﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace com.VirtuosoITech.ComplianceManagement.Portal.Penalty
{
    public partial class PenaltyDetails : System.Web.UI.Page
    {
        protected static int customerID;
        protected static string ComplianceTypeFlag;
        protected static bool IsApprover = false;
        protected string pointname;
        protected string attrubute;
        //protected int branchid;
        protected DateTime startdate;
        protected DateTime enddate;
        public static List<long> Branchlist = new List<long>();
        protected int BID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    BindLocationFilter(customerID);
                    if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                    {
                        BID = Convert.ToInt32(Request.QueryString["branchid"]);
                        Branchlist.Clear();
                        GetAllHierarchy(customerID, Convert.ToInt32(BID));
                        Branchlist.ToList();
                    }

                    if (AuthenticationHelper.Role.Equals("MGMT"))
                    {
                        IsApprover = false;
                    }
                    else
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var GetApprover = (entities.Sp_GetApproverUsers(AuthenticationHelper.UserID)).ToList();
                            if (GetApprover.Count > 0)
                            {
                                IsApprover = true;
                            }
                        }
                    }


                    BindCategories();
                    BindActList();
                    BindDetailView();

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindActList()
        {
            try
            {
                int CategoryID = -1;
                if (ddlFunctions.SelectedValue == "-1")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                    {
                        CategoryID = Convert.ToInt32(Request.QueryString["Category"]);
                        ddlFunctions.SelectedValue = Convert.ToString(CategoryID);
                    }
                }
                else
                {
                    CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                }
                ddlAct.Items.Clear();
                var ActList = ActManagement.GetAllAssignedActs(customerID, CategoryID);
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindLocationFilter(int customerID)
        {
            try
            {
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                tvFilterLocation.Nodes.Clear();
                string isstatutoryinternal = "";

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ComplianceTypeFlag == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    //BindBranchesHierarchy(node, item);
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategories()
        {
            try
            {
                ddlFunctions.DataTextField = "Name";
                ddlFunctions.DataValueField = "ID";

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    int CustBranchID = -1;
                    if (tvFilterLocation.SelectedValue != null && tvFilterLocation.SelectedValue != "")
                    {
                        CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (Convert.ToString(Request.QueryString["IsDeptHead"]) == "1")
                    {
                        ddlFunctions.DataSource = CompDeptManagement.GetNewAll(AuthenticationHelper.UserID, CustBranchID, Branchlist, "S");
                        ddlFunctions.DataBind();
                    }
                    else
                    {
                        ddlFunctions.DataSource = ComplianceCategoryManagement.GetNewAll(AuthenticationHelper.UserID, CustBranchID, Branchlist, IsApprover);
                        ddlFunctions.DataBind();
                    }
                }
                else
                {
                    int CustBranchID = -1;
                    if (tvFilterLocation.SelectedValue != null && tvFilterLocation.SelectedValue != "")
                    {
                        CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    }
                    if (Convert.ToString(Request.QueryString["IsDeptHead"]) == "1")
                    {
                        ddlFunctions.DataSource = CompDeptManagement.GetNewAll(AuthenticationHelper.UserID, CustBranchID, Branchlist, "I");
                        ddlFunctions.DataBind();
                    }
                    else
                    {
                        ddlFunctions.DataSource = ComplianceCategoryManagement.GetFunctionDetailsInternal(AuthenticationHelper.UserID, Branchlist, CustBranchID, IsApprover);
                        ddlFunctions.DataBind();
                    }
                }
                ddlFunctions.Items.Insert(0, new ListItem("Function", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void BindDetailView()
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;
                int CustomerBranchId = -1;
                int ActId = -1;
                int CategoryID = -1;
                int UserID = -1;
                int PenaltyStatus = -1;
                string internalsatutory = string.Empty;
                int CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                if (!string.IsNullOrEmpty(Request.QueryString["pointname"]))
                {
                    pointname = Request.QueryString["pointname"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["attrubute"]))
                {
                    attrubute = Request.QueryString["attrubute"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    internalsatutory = Request.QueryString["Internalsatutory"];
                }
                
                if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    BID = Convert.ToInt32(Request.QueryString["branchid"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["startdate"]))
                {
                    startdate = Convert.ToDateTime(Request.QueryString["startdate"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["enddate"]))
                {
                    enddate = Convert.ToDateTime(Request.QueryString["enddate"]);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["UserID"]))
                {
                    UserID = Convert.ToInt32(Request.QueryString["UserID"]);
                }
                if (ddlFunctions.SelectedValue == "-1")
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                    {
                        CategoryID = Convert.ToInt32(Request.QueryString["Category"]);
                        ddlFunctions.SelectedValue = Convert.ToString(CategoryID);
                    }
                }
                else
                {
                    BindActList();
                    CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                {
                    ActId = Convert.ToInt32(Request.QueryString["ActID"]);

                    if (ActId != 0)
                        ddlAct.Items.FindByValue(Request.QueryString["ActID"]).Selected = true;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Branchid"]))
                {
                    CustomerBranchId = Convert.ToInt32(Request.QueryString["Branchid"]);
                    tvFilterLocation_SelectedNodeChanged(null, null);
                    foreach (TreeNode node in tvFilterLocation.Nodes)
                    {
                        if (node.ChildNodes.Count > 0)
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                if (child.Value == Convert.ToString(CustomerBranchId))
                                {
                                    child.Selected = true;
                                }
                            }
                        }
                        else if (node.Value == Convert.ToString(CustomerBranchId))
                        {
                            node.Selected = true;
                        }
                    }
                }
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    int CustomerBranchlength = tvFilterLocation.SelectedNode.Text.Length;
                    CustomerBranchId = Convert.ToInt32(tvFilterLocation.SelectedValue);

                    if (CustomerBranchlength >= 20)
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                }
                if (ddlFunctions.SelectedValue != "-1")
                {
                    if (Convert.ToString(Request.QueryString["IsDeptHead"]) == "1")
                    {
                        int Categorylength = ddlFunctions.SelectedItem.Text.Length;
                        CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                        if (lblAdvanceSearchScrum.Text != "")
                        {
                            if (Categorylength >= 20)
                                lblAdvanceSearchScrum.Text += "     " + "<b>Department: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "     " + "<b>Department: </b>" + ddlFunctions.SelectedItem.Text;
                        }
                        else
                        {
                            if (Categorylength >= 20)
                                lblAdvanceSearchScrum.Text += "<b>Department: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "<b>Department: </b>" + ddlFunctions.SelectedItem.Text;
                        }
                    }
                    else
                    {
                        int Categorylength = ddlFunctions.SelectedItem.Text.Length;
                        CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                        if (lblAdvanceSearchScrum.Text != "")
                        {
                            if (Categorylength >= 20)
                                lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlFunctions.SelectedItem.Text;
                        }
                        else
                        {
                            if (Categorylength >= 20)
                                lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                            else
                                lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlFunctions.SelectedItem.Text;
                        }
                    }

                    //int Categorylength = ddlFunctions.SelectedItem.Text.Length;
                    //CategoryID = Convert.ToInt32(ddlFunctions.SelectedValue);
                    //if (lblAdvanceSearchScrum.Text != "")
                    //{
                    //    if (Categorylength >= 20)
                    //        lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                    //    else
                    //        lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlFunctions.SelectedItem.Text;
                    //}
                    //else
                    //{
                    //    if (Categorylength >= 20)
                    //        lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                    //    else
                    //        lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlFunctions.SelectedItem.Text;
                    //}
                }

                PenaltyStatus = Convert.ToInt32(ddlPenaltyStatus.SelectedValue);

                if (ddlAct.SelectedValue != "-1")
                {
                    int actlength = ddlAct.SelectedItem.Text.Length;
                    ActId = Convert.ToInt32(ddlAct.SelectedValue);
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (actlength >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (actlength >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }
                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }

                DivAct.Visible = true;
                GridStatutory.DataSource =null;
                GridStatutory.DataBind();
                if (internalsatutory != "Internal")
                {
                    List<SP_GetPenaltyQuarterWise_Result> detailsview = new List<SP_GetPenaltyQuarterWise_Result>();
                    List<SP_GetPenaltyQuarterWiseApprover_Result> detailsviewApprover = new List<SP_GetPenaltyQuarterWiseApprover_Result>();
                    List<SP_GetPenaltyQuarterWise_DeptHead_Result> detailsview_DeptHead = new List<SP_GetPenaltyQuarterWise_DeptHead_Result>();
                    if (Convert.ToString(Request.QueryString["IsDeptHead"]) == "1")
                    {
                        if (ddlPenaltyStatus.SelectedValue == "0")
                        {
                            detailsview_DeptHead = Business.DepartmentHeadManagement.GetPenaltyComplianceDetailsDashboard_DeptHead(customerID, Branchlist, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, PenaltyStatus, attrubute, startdate, enddate, IsApprover);
                        }
                        else
                        {
                            detailsview_DeptHead = Business.DepartmentHeadManagement.GetPenaltyComplianceDetailsDashboardPending_DeptHead(customerID, Branchlist, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, PenaltyStatus, attrubute, IsApprover);
                        }

                        Session["TotalRows"] = detailsview_DeptHead.Count;
                        GridStatutory.DataSource = detailsview_DeptHead;
                        GridStatutory.DataBind();
                    }
                    else
                    {
                        if (ddlPenaltyStatus.SelectedValue == "0")
                        {
                            if (IsApprover == false)
                            {
                                detailsview = Business.ComplianceManagement.GetPenaltyComplianceDetailsDashboard(customerID, Branchlist, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, PenaltyStatus, attrubute, startdate, enddate, IsApprover);
                                Session["TotalRows"] = detailsview.Count;
                                GridStatutory.DataSource = detailsview;
                                GridStatutory.DataBind();
                            }
                            else
                            {
                                detailsviewApprover = Business.ComplianceManagement.GetPenaltyComplianceDetailsDashboardApprover(customerID, Branchlist, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, PenaltyStatus, attrubute, startdate, enddate, IsApprover);
                                Session["TotalRows"] = detailsviewApprover.Count;
                                GridStatutory.DataSource = detailsviewApprover;
                                GridStatutory.DataBind();
                            }
                        }
                        else
                        {
                            if (IsApprover == false)
                            {
                                detailsview = Business.ComplianceManagement.GetPenaltyComplianceDetailsDashboardPending(customerID, Branchlist, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, PenaltyStatus, attrubute, IsApprover);
                                Session["TotalRows"] = detailsview.Count;
                                GridStatutory.DataSource = detailsview;
                                GridStatutory.DataBind();
                            }
                            else
                            {
                                detailsviewApprover = Business.ComplianceManagement.GetPenaltyComplianceDetailsDashboardPendingApprover(customerID, Branchlist, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, PenaltyStatus, attrubute, IsApprover);
                                Session["TotalRows"] = detailsviewApprover.Count;
                                GridStatutory.DataSource = detailsviewApprover;
                                GridStatutory.DataBind();
                            }
                        }
                    }
                    GridStatutory.Visible = true;
                    GridInternalCompliance.Visible = false;
                    UpDetailView.Update();
                    GetPageDisplaySummary();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0];
                node.Selected = true;
                ddlFunctions.ClearSelection();
                ddlAct.ClearSelection();
                lblAdvanceSearchScrum.Text = String.Empty;
                divAdvSearch.Visible = false;

                tvFilterLocation_SelectedNodeChanged(sender, e);

                var nvc = HttpUtility.ParseQueryString(Request.Url.Query);
                nvc.Remove("customerid");
                nvc.Remove("Category");
                nvc.Remove("Branchid");
                nvc.Remove("Internalsatutory");
                string url = Request.Url.AbsolutePath + "?" + nvc.ToString();
                Response.Redirect("~/Penalty/PenaltyDetails.aspx",false);
                //Rebind Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                if (ComplianceTypeFlag == "Statutory")
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                else if (ComplianceTypeFlag == "Internal")
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    Branchlist.ToList();
                }
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;
                if (Convert.ToString(Session["TotalRows"]) != null && Convert.ToString(Session["TotalRows"]) != "")
                {
                    lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                    lTotalCount.Text = GetTotalPagesCount().ToString();

                    if (lTotalCount.Text != "0")
                    {
                        if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        {
                            SelectedPageNo.Text = "1";
                            lblStartRecord.Text = "1";

                            if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                                lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                            else
                                lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                        }
                    }
                    else if (lTotalCount.Text == "0")
                    {
                        SelectedPageNo.Text = "0";
                        DivRecordsScrum.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }


        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}