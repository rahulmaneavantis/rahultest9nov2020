﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates
{
    public partial class FrmNewsLetter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["SortOrder"] = "Asc";
                ViewState["SortExpression"] = "Title";
                BindEvents();

                if(AuthenticationHelper.Role.Equals("SADMN") || AuthenticationHelper.Role.Equals("UPDT"))
                {
                    btnAddEvent.Visible = true;
                }
            }
        }

        private void BindEvents()
        {
            try
            {
                List<Business.Data.NewsLetter> EventList = null;
                //if (AuthenticationHelper.Role == "CADMN")
                //{
                //    long? customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID;
                //    List<int> CompTypeList = CustomerManagement.GetCustomerBranchTypeList(customerID);

                //    EventList = EventManagement.GetCompanyTypeWiseEvents(tbxFilter.Text, CompTypeList);
                //}
                //else
                //{
                //    EventList = EventManagement.GetAllEventsNew(tbxFilter.Text);


                EventList = DailyUpdateManagment.GetAllNewsLetter();

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        EventList = EventList.OrderBy(entry => entry.Title).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Description")
                    {
                        EventList = EventList.OrderBy(entry => entry.Description).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "NewsDate")
                    {
                        EventList = EventList.OrderBy(entry => entry.NewsDate).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "Title")
                    {
                        EventList = EventList.OrderByDescending(entry => entry.Title).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Description")
                    {
                        EventList = EventList.OrderByDescending(entry => entry.Description).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "NewsDate")
                    {
                        EventList = EventList.OrderByDescending(entry => entry.NewsDate).ToList();
                    }
                    direction = SortDirection.Ascending;
                }
                grdEventList.DataSource = EventList;
                grdEventList.DataBind();
                upEventList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int newsLetterID = Convert.ToInt32(e.CommandArgument);
                if (e.CommandName.Equals("EDIT_ACT"))
                {
                    DateTime date = DateTime.Now;
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);


                   // ViewState["Mode"] = 1;
                   // ViewState["newsLetterID"] = newsLetterID;

                   // com.VirtuosoITech.ComplianceManagement.Business.Data.NewsLetter eventData = DailyUpdateManagment.GetByNewsLetterID(newsLetterID);

                   // txtTitle.Text = eventData.Title;
                   // //txtDescription.Text = eventData.Description;
                   // lblSampleForm.Text = eventData.FileName;
                   // lblSampleDoc.Text = eventData.DocFileName;
                   // string StartDate = Convert.ToDateTime(eventData.NewsDate).ToString("dd-MM-yyyy");

                   // tbxStartDate.Text = StartDate;
                   // upEvent.Update();
                   //// Response.Redirect("~/DailyUpdates/FrmNewsLetterAdd.aspx", false);
                   // ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "initializeCombobox(); $(\"#divEventDialog\").dialog('open')", true);


                    Session["Mode"] = 1;
                    // ViewState["Mode"] = 1;
                    Session["newsLetterID"] = newsLetterID;

                    Response.Redirect("~/DailyUpdates/FrmNewsLetterAdd.aspx", false);

                }
                else if (e.CommandName.Equals("DELETE_ACT"))
                {
                    DailyUpdateManagment.DeleteNewsLetter(newsLetterID);
                    BindEvents();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEventList.PageIndex = e.NewPageIndex;
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdEventList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                var actList = DailyUpdateManagment.GetAllNewsLetter();
                if (direction == SortDirection.Ascending)
                {
                    actList = actList.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    actList = actList.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }

                foreach (DataControlField field in grdEventList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEventList.Columns.IndexOf(field);
                    }
                }

                grdEventList.DataSource = actList;
                grdEventList.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdEventList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdEventList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lbtEdit = (LinkButton)e.Row.FindControl("lbtEdit");
                    LinkButton lbtDelete = (LinkButton)e.Row.FindControl("lbtDelete");
                    lbtEdit.Visible = false;
                    lbtDelete.Visible = false;
                    if (AuthenticationHelper.Role.Equals("SADMN"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                    if (AuthenticationHelper.Role.Equals("UPDT"))
                    {
                        lbtEdit.Visible = true;
                        lbtDelete.Visible = true;
                    }
                   
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }

        protected void upEvent_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
               
                DateTime date = DateTime.MinValue;
                if (DateTime.TryParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month, date.Day), true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeDatePicker", "initializeDatePicker(null);", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddEvent_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime date = DateTime.Now;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "initializeDatePicker", string.Format("initializeDatePicker(new Date({0}, {1}, {2}));", date.Year, date.Month - 1, date.Day), true);
                //lblSampleDoc.Text = "";
                //lblSampleForm.Text = "";
                //txtTitle.Text = "";
                //tbxStartDate.Text = "";
                //ViewState["Mode"] = 0;
                //upEvent.Update();
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog1", "$(\"#divEventDialog\").dialog('open')", true);
                Session["Mode"] = 0;
                Response.Redirect("~/DailyUpdates/FrmNewsLetterAdd.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdEventList.PageIndex = 0;
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                //GetEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

    
        protected void ddlFilterCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEvents();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public string ShowFrequency(int frq)
        {
            return Enumerations.GetEnumByID<EventFrequency>(frq);
        }

        public string ShowType(int typ)
        {
            return Enumerations.GetEnumByID<EventType>(typ);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //string fileclass = "";
                //string imgflag = "";
                //if ((int)ViewState["Mode"] == 0)
                //{
                //    string strpath = System.IO.Path.GetExtension(FileUpload1.FileName);
                //    string strimage = System.IO.Path.GetExtension(fuSampleFile.FileName);
                //    if (strpath == ".pdf")
                //    {
                //        fileclass = "pdf";
                //    }
                //    if (strimage == ".jpeg" || strimage == ".JPEG" || strimage == ".gif" || strimage == ".GIF" || strimage == ".png" || strimage == ".PNG")
                //    {
                //        imgflag = "P";
                //    }
                //}
                //else
                //{
                //    fileclass = "pdf";
                //    imgflag = "P";
                //}


                //if (imgflag != "P")
                //{
                //    cvDuplicateEntry.IsValid = false;
                //    cvDuplicateEntry.ErrorMessage = "please upload only image";
                //}
                //else if (fileclass != "pdf")
                //{
                //    cvDuplicateEntry.IsValid = false;
                //    cvDuplicateEntry.ErrorMessage = "please upload only pdf Upload Document";
                //}
                //else
                //{

                    string fileName = "";
                    string filePath = "";

                    string docfileName = "";
                    string docfilePath = "";

                    if ((int)ViewState["Mode"] == 1)
                    {
                        var NewsLetter = DailyUpdateManagment.GetNewsletter(Convert.ToInt32(ViewState["newsLetterID"]));

                        fileName = NewsLetter.FileName;
                        filePath = NewsLetter.FilePath;
                        docfileName = NewsLetter.DocFileName;
                        docfilePath = NewsLetter.DocFilePath;

                    }
                    if (fuSampleFile.FileBytes != null && fuSampleFile.FileBytes.LongLength > 0)
                    {
                        fuSampleFile.SaveAs(Server.MapPath("~/NewsLetterImages/" + fuSampleFile.FileName));
                        fileName = fuSampleFile.FileName;
                        filePath = "~/NewsLetterImages/" + fuSampleFile.FileName;
                    }

                    if (FileUpload1.FileBytes != null && FileUpload1.FileBytes.LongLength > 0)
                    {
                        FileUpload1.SaveAs(Server.MapPath("~/NewsLetterImages/" + FileUpload1.FileName));
                        docfileName = FileUpload1.FileName;
                        docfilePath = "~/NewsLetterImages/" + FileUpload1.FileName;
                    }

                    com.VirtuosoITech.ComplianceManagement.Business.Data.NewsLetter eventData = new com.VirtuosoITech.ComplianceManagement.Business.Data.NewsLetter()
                    {
                        Title = txtTitle.Text,
                        // Description = txtDescription.Text,
                        FileName = fileName,
                        FilePath = filePath,
                        DocFileName = docfileName,
                        DocFilePath = docfilePath,
                        NewsDate = DateTime.ParseExact(tbxStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture),
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedDate = DateTime.Now.Date,
                        UpdatedDate = DateTime.Now.Date,
                        IsDeleted = false,
                    };

                    if ((int)ViewState["Mode"] == 1)
                    {
                        eventData.ID = Convert.ToInt32(ViewState["newsLetterID"]);
                    }

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (EventManagement.ExistsNewsLetter(eventData.Title))
                        {
                            cvDuplicateEntry.ErrorMessage = "News title already exists.";
                            cvDuplicateEntry.IsValid = false;
                            return;
                        }
                        DailyUpdateManagment.CreateNewsLetter(eventData);
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        DailyUpdateManagment.UpdateNewsLetter(eventData);
                    }

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divEventDialog\").dialog('close')", true);
                    BindEvents();
                    //Response.Redirect("~/DailyUpdates/FrmNewsLetter.aspx", false);
                //}
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }

        }


        protected void btnComplianceRepeater_Click(object sender, EventArgs e)
        {

        }

        protected void btnCompanyTypeRepeater_Click(object sender, EventArgs e)
        {

        }
    }
}