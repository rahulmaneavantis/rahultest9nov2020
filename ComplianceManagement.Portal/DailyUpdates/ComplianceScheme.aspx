﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="ComplianceScheme.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.DailyUpdates.ComplianceScheme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 45%; left: 50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upIComplianceTypeList" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td align="right" class="pagefilter">Filter :
                        <asp:TextBox runat="server" ID="tbxFilter" Width="250px" MaxLength="50" AutoPostBack="true"
                            OnTextChanged="tbxFilter_TextChanged" />
                    </td>
                    <td class="newlink" align="right">
                        <asp:LinkButton Text="Add New" runat="server" OnClick="btnAddScheme_Click" ID="btnAddScheme" OnClientClick="fopenpopup();" />
                    </td>
                </tr>
            </table>
            <asp:GridView runat="server" ID="grdAllScheme" AutoGenerateColumns="false" AllowSorting="true"
                GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" OnSorting="grdAllScheme_Sorting"
                BorderWidth="1px" CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" OnRowCreated="grdAllScheme_RowCreated"
                Width="100%" Font-Size="12px" DataKeyNames="ID" OnRowCommand="grdAllScheme_RowCommand"
                OnPageIndexChanging="grdAllScheme_PageIndexChanging">
                <Columns>

                    <asp:TemplateField HeaderText="Title" SortExpression="Description">
                        <ItemTemplate>
                             <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="ActName" HeaderText="Act" HeaderStyle-Height="20px" ItemStyle-Height="20px" SortExpression="ActName" />--%>
                    <asp:TemplateField ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_Scheme"
                                CommandArgument='<%# Eval("ID") %>'><img src="../Images/edit_icon.png" alt="Edit Scheme" title="Edit Scheme" /></asp:LinkButton>
                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="DELETE_Scheme"
                                CommandArgument='<%# Eval("ID") %>' OnClientClick="return confirm('Are you certain you want to delete this scheme?');"><img src="../Images/delete_icon.png" alt="Delete Scheme" title="Delete Scheme" /></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderTemplate>
                        </HeaderTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                <PagerSettings Position="Top" />
                <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                <AlternatingRowStyle BackColor="#E6EFF7" />
                <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                <EmptyDataTemplate>
                    No Records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divAddEditSchemeDialog">
        <asp:UpdatePanel ID="UpdateSchemes" runat="server" UpdateMode="Conditional" OnLoad="UpdateSchemes_Load"> <%--OnLoad="UpdateSchemes_Load"--%>
            <ContentTemplate>
                <div style="margin: 5px">
                    <div style="margin-bottom: 4px">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="SchemeValidationGroup" />
                        <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                            ValidationGroup="SchemeValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Act</label>
                        <%--<asp:ListBox ID="ddlAct" runat="server" CssClass="form-control" SelectionMode="Multiple" Style="height: 25px; width: 250px;"></asp:ListBox>--%>
                        <asp:TextBox runat="server" ID="txtAct" Style="padding: 0px; margin: 0px; height: 22px; width: 250px;"
                            CssClass="txtbox" />
                        <div style="margin-left: 161px; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid gray; height: 200px;" id="dvAct">

                            <asp:Repeater ID="rptAct" runat="server">
                                <HeaderTemplate>
                                    <table class="detailstable FadeOutOnEdit" id="RepeaterTable">
                                        <tr>
                                            <td style="width: 100px;">
                                                <asp:CheckBox ID="ActSelectAll" Text="Select All" runat="server" onclick="checkAll(this)" /></td>
                                            <td style="width: 282px;">
                                                <asp:Button runat="server" ID="btnRepeater" Text="Ok" Style="float: left" OnClick="btnRepeater_Click" /></td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 20px;">
                                            <asp:CheckBox ID="chkAct" runat="server" onclick="UncheckHeader();" /></td>
                                        <td style="width: 200px;">
                                            <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 250px; padding-bottom: 5px;">
                                                <asp:Label ID="lblActID" runat="server" Visible="false" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                                <asp:Label ID="lblActName" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Act can not be empty." ControlToValidate="ddlAct"
                            runat="server" ValidationGroup="SchemeValidationGroup" Display="None" />--%>
                    </div>
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 150px; display: block; float: left; font-size: 13px; color: #333;">
                            Title</label>
                        <asp:TextBox runat="server" ID="tbxTitle" Style="height: 25px; width: 250px;" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Title can not be empty." ControlToValidate="tbxTitle"
                            runat="server" ValidationGroup="SchemeValidationGroup" Display="None" />
                    </div>
                    <div style="margin-bottom: 7px; float: right; margin-right: 57px; margin-top: 10px;">
                        <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="button" OnClick="btnSave_Click"
                            ValidationGroup="SchemeValidationGroup" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divAddEditSchemeDialog').dialog('close');" />
                    </div>
                    <label runat="server" id="lblID" style="display: none;"/>
                </div>
                <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">

                    <p style="color: red;"><strong>Note:</strong> (*) Fields Are Compulsary</p>


                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript">
        $(function () {
            $('#divAddEditSchemeDialog').dialog({
                height: 350,
                width: 500,
                autoOpen: false,
                draggable: true,
                title: "Add/Edit Scheme",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function fopenpopup() {
            $("#divAddEditSchemeDialog").dialog('open');
        }

        //$(function () {
        //    $('[id*=ddlAct]').multiselect({
        //        includeSelectAllOption: true,
        //        numberDisplayed: 1
        //        //buttonWidth: '68%'
        //    });
        //});

        function initializeRadioButtonsList(controlID) {
            $(controlID).buttonset();
        }

        function checkAll(cb) {
            var ctrls = document.getElementsByTagName('input');
            for (var i = 0; i < ctrls.length; i++) {
                var cbox = ctrls[i];
                if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
                    cbox.checked = cb.checked;
                }
            }
        }

        function UncheckHeader() {
            var rowCheckBox = $("#RepeaterTable input[id*='chkAct']");
            var rowCheckBoxSelected = $("#RepeaterTable input[id*='chkAct']:checked");
            var rowCheckBoxHeader = $("#RepeaterTable input[id*='ActSelectAll']");
            if (rowCheckBox.length == rowCheckBoxSelected.length) {
                rowCheckBoxHeader[0].checked = true;
            } else {

                rowCheckBoxHeader[0].checked = false;
            }
        }

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        <%--function initializeCombobox() {
            $("#<%= ddlAct.ClientID %>").combobox();
        }--%>
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };
    </script>
</asp:Content>
