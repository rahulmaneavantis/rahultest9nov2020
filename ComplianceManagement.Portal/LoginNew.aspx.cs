﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal
{
    public partial class LoginNew : System.Web.UI.Page
    {
        
        GoogleReCaptcha.GoogleReCaptcha ctrlGoogleReCaptcha = new GoogleReCaptcha.GoogleReCaptcha();
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
            ctrlGoogleReCaptcha.PublicKey = "6LdHaw0UAAAAABbVAmqGINbZdkH9GANgNr_uMISc";
            ctrlGoogleReCaptcha.PrivateKey = "6LdHaw0UAAAAACUSQK2kgIy8RaczRGb6ws5uHn1g";
            this.Panel1.Controls.Add(ctrlGoogleReCaptcha);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                   // Submit.Click += new EventHandler(btnLogin_Click);

                    HttpCookie loginCookie = Request.Cookies.Get("LoginCookie");
                    if (null != loginCookie)
                    {

                        txtemail.Text = loginCookie.Values["Username"].ToString();

                        txtpass.Attributes.Add("value", loginCookie.Values["Password"].ToString());
                    }
                    txtemail.Focus();

                    if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                    {
                        string[] cokdata = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name.Trim().Split(';');
                        int userID = Convert.ToInt32(cokdata[0]);
                        User user = UserManagement.GetByID(userID);
                        ProcessAuthenticationInformation(user);
                    }

                    if (HttpContext.Current.Request.IsAuthenticated)
                    {
                        Response.Redirect("~/Default.aspx");
                    }

                }

                //imgCaptcha.ImageUrl = "~/CaptchaImage.aspx?" + DateTime.Now.Ticks.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void btnCaptchaRefresh_Click(object sender, EventArgs e)
        //{
        //    SetCapctha();
        //}

        //void SetCapctha()
        //{

        //    try
        //    {
        //        txtcode.Text = string.Empty;
        //        Random random = new Random();
        //        //string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        //        string combination = "123456789";
        //        StringBuilder captcha = new StringBuilder();
        //        for (int i = 0; i < 6; i++)
        //            captcha.Append(combination[random.Next(combination.Length)]);
        //        Session["captcha"] = null;
        //        Session["captcha"] = captcha.ToString();
        //        imgCaptcha.ImageUrl = "~/CaptchaImage.aspx?" + DateTime.Now.Ticks.ToString();

        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //}

        private void ProcessAuthenticationInformation(User user)
        {
            try
            {
                string name = string.Format("{0} {1}", user.FirstName, user.LastName);
                string role = RoleManagement.GetByID(user.RoleID).Code;
                int checkInternalapplicable = 0;
                if (user.CustomerID == null)
                {
                    checkInternalapplicable = 2;
                }
                else
                {
                    Customer customer = CustomerManagement.GetByID(Convert.ToInt32(user.CustomerID));
                    if (customer.IComplianceApplicable != null)
                    {
                        checkInternalapplicable = Convert.ToInt32(customer.IComplianceApplicable);
                    }
                }

                User userToUpdate = new User();
                userToUpdate.ID = user.ID;
                userToUpdate.LastLoginTime = DateTime.UtcNow;
                UserManagement.Update(userToUpdate);

                if (role.Equals("SADMN"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), true);
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4}", user.ID, role, name, checkInternalapplicable, 'C'), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);
                    Response.Redirect("~/Users/UserSummary.aspx", false);
                }
                else if (role.Equals("IMPT"))
                {
                    FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), true);
                    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4}", user.ID, role, name, checkInternalapplicable, 'C'), false);
                    TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                    Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);
                    Response.Redirect("~/Common/CompanyStructure.aspx", false);
                }
                else
                {
                    //this code is added by rahul on 7 NOV 2016
                    var productcount = UserManagement.GetByProductID(Convert.ToInt32(user.CustomerID)).Count;
                    if (productcount == 1)
                    {
                        int productid = UserManagement.GetByProduct(Convert.ToInt32(user.CustomerID));

                        if (productid == 1)
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), true);
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4}", user.ID, role, name, checkInternalapplicable, "C"), false);

                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);

                            #region Compliance

                            if (role.Equals("MGMT"))
                            {
                                Response.Redirect("~/Management/MangementDashboard.aspx", false);
                            }
                            else if (role.Equals("IMPT"))
                            {
                                Response.Redirect("~/Common/CompanyStructure.aspx", false);
                            }
                            else
                            {
                                Response.Redirect("~/Common/ComplianceDashboard.aspx", false);
                            }

                            #endregion
                        }
                        else if (productid == 3)
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), true);
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4}", user.ID, role, name, checkInternalapplicable, "A"), false);

                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);

                            if (role.Equals("MGMT"))
                            {
                                Response.Redirect("~/Management/Risk_ManagementDashboard.aspx", false);
                            }
                            else if (role.Equals("IMPT"))
                            {
                                Response.Redirect("~/Common/CompanyStructure.aspx", false);
                            }
                            else
                            {
                                Response.Redirect("~/RiskManagement/Common/AuditDashboard.aspx", false);
                            }
                        }
                        else if (productid == 4)
                        {
                            FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), true);
                            FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4}", user.ID, role, name, checkInternalapplicable, "I"), false);

                            TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                            Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);

                            if (role.Equals("MGMT"))
                            {
                                Response.Redirect("~/Management/Internal_ManagementDashboard.aspx", false);
                            }
                            else if (role.Equals("IMPT"))
                            {
                                Response.Redirect("~/Common/CompanyStructure.aspx", false);
                            }
                            else
                            {
                                Response.Redirect("~/RiskManagement/Common/InternalControlDashboard.aspx", false);
                            }
                        }

                    }
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(user.ID.ToString(), true);
                        FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4}", user.ID, role, name, checkInternalapplicable, 'C'), false);

                        TimeZoneInfo destinationTimeZone = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TimeZoneValue"]);
                        Session["LastLoginTime"] = UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime != null ? TimeZoneInfo.ConvertTimeFromUtc(UserManagement.GetByID(Convert.ToInt32(user.ID)).LastLoginTime.Value, destinationTimeZone) : TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, destinationTimeZone);

                        Response.Redirect("~/ProductMapping/ProductMappingStructure.aspx", false);
                    }
                }

                DateTime LastPasswordChangedDate = Convert.ToDateTime(UserManagement.GetByID(Convert.ToInt32(user.ID)).ChangPasswordDate);
                DateTime currentDate = DateTime.Now;
                LastPasswordChangedDate = LastPasswordChangedDate != null ? LastPasswordChangedDate : DateTime.Now;
                int noDays = Convert.ToInt32(ConfigurationManager.AppSettings["ChangedPasswordDays"]);
                int dateDifference = Convert.ToInt32((currentDate - LastPasswordChangedDate).TotalDays);

                if (dateDifference == noDays || dateDifference > noDays)
                {
                    Session["ChangePassword"] = true;
                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                }
                else
                {
                    Session["ChangePassword"] = false;
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            try
            {
                User user = null;

                if (!(UserManagement.WrongAttemptCount(txtemail.Text.Trim()) >= 3))
                {
                    //if (Session["Captcha"] != null)
                    //{
                    //    if (Session["captcha"].ToString().Trim().Equals(txtcode.Text.Trim()))
                    //    {
                    if (ctrlGoogleReCaptcha.Validate())
                    {
                        if (UserManagement.IsValidUser(txtemail.Text, Util.CalculateMD5Hash(txtpass.Text), out user))
                        {
                            if (user.IsActive)
                            {
                                Session["userID"] = user.ID;
                                if (user.LastLoginTime != null)
                                {
                                    if (UserManagement.HasUserSecurityQuestion(user.ID))
                                    {
                                        HttpCookie SecurityCookie = Request.Cookies.Get("SecurityCookie");
                                        if (SecurityCookie != null && SecurityCookie.Values["SecurityUserName"].ToString().Equals(txtemail.Text.Trim()))
                                        {
                                            ProcessAuthenticationInformation(user);
                                            UserManagement.WrongAttemptCountUpdate(user.ID);
                                            UserManagementRisk.WrongAttemptCountUpdate(user.ID);
                                        }
                                        else
                                        {

                                            Session["QuestionBank"] = false;
                                            Session["rememberme"] = false;
                                            Session["username"] = txtemail.Text;
                                            Session["password"] = txtpass.Text;
                                            Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                        }
                                    }
                                    else
                                    {

                                        Session["QuestionBank"] = true;
                                        Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
                                    }
                                }
                                else
                                {
                                    Response.Redirect("~/Account/ChangePassword.aspx", false);
                                }
                            }
                            else
                            {
                                cvLogin.IsValid = false;
                                cvLogin.ErrorMessage = "Your account is disabled.";
                                //SetCapctha();
                            }
                        }
                        else
                        {
                            UserManagement.WrongUpdate(txtemail.Text);
                            cvLogin.IsValid = false;
                            cvLogin.ErrorMessage = "Please enter a valid username or password.";
                            // SetCapctha();
                        }
                    }
                    else
                    {
                        cvLogin.IsValid = false;
                        cvLogin.ErrorMessage = "Captcha Failed!! Please try again!!";
                        // SetCapctha();
                    }
                    //}
                    //else
                    //{
                    //    //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "Confirm('Session timeout. try again.');", true);
                    //    Response.Redirect("~Login.aspx", false);
                    //}
                }
                else
                {
                    // lbtResetPassword.Visible = true;
                    cvLogin.IsValid = false;
                    cvLogin.ErrorMessage = "Your account is locked please reset password.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtResetPassword_Click(object sender, EventArgs e)
        {

        }

        //protected void btnLogin_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        User user = null;

        //        if (!(UserManagement.WrongAttemptCount(txtemail.Text.Trim()) >= 3))
        //        {
        //            //if (Session["Captcha"] != null)
        //            //{
        //            //    if (Session["captcha"].ToString().Trim().Equals(txtcode.Text.Trim()))
        //            //    {
        //            if (ctrlGoogleReCaptcha.Validate())
        //            {
        //                if (UserManagement.IsValidUser(txtemail.Text, Util.CalculateMD5Hash(txtpass.Text), out user))
        //                {
        //                    if (user.IsActive)
        //                    {
        //                        Session["userID"] = user.ID;
        //                        if (user.LastLoginTime != null)
        //                        {
        //                            if (UserManagement.HasUserSecurityQuestion(user.ID))
        //                            {
        //                                HttpCookie SecurityCookie = Request.Cookies.Get("SecurityCookie");
        //                                if (SecurityCookie != null && SecurityCookie.Values["SecurityUserName"].ToString().Equals(txtemail.Text.Trim()))
        //                                {
        //                                    ProcessAuthenticationInformation(user);
        //                                    UserManagement.WrongAttemptCountUpdate(user.ID);
        //                                    UserManagementRisk.WrongAttemptCountUpdate(user.ID);
        //                                }
        //                                else
        //                                {

        //                                    Session["QuestionBank"] = false;
        //                                    Session["rememberme"] = false;
        //                                    Session["username"] = txtemail.Text;
        //                                    Session["password"] = txtpass.Text;
        //                                    Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
        //                                }
        //                            }
        //                            else
        //                            {

        //                                Session["QuestionBank"] = true;
        //                                Response.Redirect("~/SecurityQuestion/AddSecurityQuestions.aspx", false);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            Response.Redirect("~/Account/ChangePassword.aspx", false);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        cvLogin.IsValid = false;
        //                        cvLogin.ErrorMessage = "Your account is disabled.";
        //                        //SetCapctha();
        //                    }
        //                }
        //                else
        //                {
        //                    UserManagement.WrongUpdate(txtemail.Text);
        //                    cvLogin.IsValid = false;
        //                    cvLogin.ErrorMessage = "Please enter a valid username or password.";
        //                   // SetCapctha();
        //                }
        //            }
        //            else
        //            {
        //                cvLogin.IsValid = false;
        //                cvLogin.ErrorMessage = "Captcha Failed!! Please try again!!";
        //               // SetCapctha();
        //            }
        //            //}
        //            //else
        //            //{
        //            //    //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "Confirm('Session timeout. try again.');", true);
        //            //    Response.Redirect("~Login.aspx", false);
        //            //}
        //        }
        //        else
        //        {
        //           // lbtResetPassword.Visible = true;
        //            cvLogin.IsValid = false;
        //            cvLogin.ErrorMessage = "Your account is locked please reset password.";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
    }
}