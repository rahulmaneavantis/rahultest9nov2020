﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadContractDocuments.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages.UploadContractDocuments" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="/NewCSS/bootstrap.min.css" rel="stylesheet" />
    <!-- bootstrap theme -->
    <link href="/NewCSS/bootstrap-theme.css" rel="stylesheet" />
    <!--external css-->
    <!-- font icon -->
    <link href="/NewCSS/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles -->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
        
    <script src="/Newjs/bootstrap-tagsinput.js"></script>
    <link href="/NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />

      <script type="text/javascript">
          $(document).ready(function () {

              $(window).keydown(function (event) {                  
                  if (event.keyCode == 13) {
                      event.preventDefault();
                      return false;
                  }
              });

              CheckValidation();
              $(function () {
                  $('[data-toggle="tooltip"]').tooltip()
              });
              //$('#txtDocTags').tagsInput({
              //    confirmKeys: [44,188]
              //});             

              $('input[data-role="tagsinput"]').tagsinput({              
              });
          });

          var InvalidFilesTypes = ["exe", "bat", "dll", "css", "js", "jsp",
              "php5", "pht", "asa", "cer", "asax", "swf", "xap", "aspx", "asp", "php", "reg", "rdp"];

          function CheckValidation() {              
              var fuSampleFile = $("#<%=ContractFileUpload.ClientID%>").get(0).files;
              var isValidFile = true;
              var selectedDocTypeID = $("#<%=ddlDocType.ClientID %>").val();
              $("#Labelmsg").css('display', 'none');
              if ((selectedDocTypeID != null || selectedDocTypeID != undefined) && fuSampleFile.length>0) {
                  if (selectedDocTypeID > "0" && fuSampleFile.length > 0) {
                      $("#<%= lnkDocumentUpload.ClientID %>").removeAttr('disabled');
                  }
                  else {
                      $("#<%= lnkDocumentUpload.ClientID %>").attr("disabled", "disabled");
                  }

                  for (var i = 0; i < fuSampleFile.length; i++) {
                      var fileExtension = fuSampleFile[i].name.split('.').pop();
                      if (InvalidFilesTypes.indexOf(fileExtension) != -1) {
                          isValidFile = false;
                          break;
                      }
                  }
                  if (!isValidFile) {
                      $("#Labelmsg").css('display', 'block');
                      $('#Labelmsg').text("Invalid file error. System does not support uploaded file.Please upload another file..");
                  }
                  return isValidFile;

              }
          }

        function openAddNewDocTypeModal() {
            $('#AddNewDocTypePopUp').modal('show');
            $('#DocTypeframe').attr('src', "/ContractProduct/Masters/AddDocumentType.aspx");
        }

        function CloseMe() {
            <%--alert("<%=TxtFlag.Text %>");--%>
            window.parent.CloseUploadDocumentPopup("<%=TxtFlag.Text %>");
        }

         function CloseDocTypePopup() {
            $('#AddNewDocTypePopUp').modal('hide');
            $.ajax({
                type: "POST",
                url: "/ContractProduct/Masters/AddDocumentType.aspx/getDocTypeID",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response != '') {
                        var doctypeID = response.d;
                        bindDocumentType(doctypeID);
                    }
                },
                error:
           function (XMLHttpRequest, textStatus, errorThrown) { },
            });
        }

       function bindDocumentType(doctypeID) {
            var ddlDocType = $('#<%=ddlDocType.ClientID %>');
            if (ddlDocType != null || ddlDocType != undefined) {
                ddlDocType.empty().append('<option selected="selected" value="0" disabled = "disabled">Loading.....</option>');
                ddlDocType.trigger("chosen:updated");
                ddlDocType.trigger("liszt:updated");
                $.ajax({
                    type: "POST",
                    url: "/ContractProduct/aspxPages/UploadContractDocuments.aspx/GetDocTypes",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        ddlDocType.empty();
                        $.each(response, function () {
                            for (var i = 0; i < response.d.length; i++) {
                                ddlDocType.append("<option value='" + response.d[i].Value + "'>" + response.d[i].Text + "</option>");
                            }
                        });

                        $('#ddlDocType').find('option[Value=' + doctypeID + ']').attr('selected', 'selected');

                        ddlDocType.trigger("chosen:updated");
                        ddlDocType.trigger("liszt:updated");

                        //ddlDocTypeChange();
                    },
                    failure: function (response) {
                    },
                    error: function (response) {
                    }
                });
            }
        }

      </script>

    <style type="text/css">
        .bootstrap-tagsinput {
            width: 100% !important;
            /*white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;*/
        }

        [class="bootstrap-tagsinput"] input {
            width: 100% !important;
        }
    </style>
</head>
<body class="bgColor-white">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smUploadContractDocs" runat="server"></asp:ScriptManager>

      <%--  <asp:UpdatePanel ID="upUploadContractDocs" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
                <div class="mainDiv">

                    <div class="row col-md-12 plr0">
                         <asp:ValidationSummary ID="vsContractDocument" runat="server" EnableClientScript="False"
                            ValidationGroup="ContractDocumentPopUpValidationGroup" class="alert alert-block alert-danger fade in" />
                        <asp:CustomValidator ID="cvContractDocument" runat="server" EnableClientScript="False"
                            ValidationGroup="ContractDocumentPopUpValidationGroup" Display="None" />
                         <asp:Label ID="Labelmsg" class="alert alert-block alert-danger fade in" Style="display: none;" runat="server"></asp:Label>
                    </div>

                    <div class="row col-md-12 plr0">
                        <div class="form-group required col-md-6 w50per">
                            <label for="ddlDocType" class="control-label float-left">Document Type</label>
                            <asp:DropDownListChosen runat="server" ID="ddlDocType" AllowSingleDeselect="false" DisableSearchThreshold="5"
                                DataPlaceHolder="Select" class="form-control" Width="85%"  onchange="CheckValidation()"  />
                            <img id="imgAddNewDocType" src="/Images/add_icon_new.png" alt="Add" style="padding-left: 5px;" onclick="openAddNewDocTypeModal()" 
                                 data-toggle="tooltip" title="Add another Document Type" data-placement="bottom" />
                        </div>                       
                    </div>

                    <div class="row form-group mt5">
                        <div class="col-md-12">
                            <label for="txtDocTags" class="control-label">Add upto 10 tag(s) with max 30 character(s) each</label>
                            <asp:TextBox runat="server" ID="txtDocTags" CssClass="form-control" ClientIDMode="Static" data-role="tagsinput"
                                autocomplete="off" Width="100%" Rows="1" /> <%--placeHolder="Add tag(s) (Use Tab Key to Enter)"--%> 
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <asp:FileUpload ID="ContractFileUpload" runat="server" AllowMultiple="true" CssClass="fileUploadClass"/>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12 text-center">
                            <label for="lnkDocumentUpload" class="hidden-label">&nbsp;</label>
                            <asp:LinkButton ID="lnkDocumentUpload" runat="server" CssClass="btn btn-primary"
                                Text="<i class='fa fa-upload' aria-hidden='true'></i> Upload" OnClick="btnUploadContractDoc_Click" OnClientClick="if(!CheckValidation())return false;"
                                ToolTip="Upload Selected Document(s)" data-toggle="tooltip">
                            </asp:LinkButton>
                            <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="row col-md-12">
                            <label for="lnkDocumentUpload" class="hidden-label w100per">&nbsp;</label>
                            <asp:TextBox runat="server" ID="TxtFlag" CssClass="form-control" Visible="false" />
                        </div>
                    </div>
                </div>
           <%-- </ContentTemplate>
        </asp:UpdatePanel>--%>

        <%--Document Type Popup--%>
            <div class="modal fade" id="AddNewDocTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" style="width: 100%;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseDocTypePopup()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="DocTypeframe" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <%--END--Document Type Popup--%>
    </form>
</body>
</html>
