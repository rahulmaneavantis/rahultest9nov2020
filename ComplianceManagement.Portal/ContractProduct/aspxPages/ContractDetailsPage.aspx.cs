﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using System.Web.Services;
using System.Threading.Tasks;
using System.Text;
using System.Globalization;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.aspxPages
{
    public partial class ContractDetailsPage : System.Web.UI.Page
    {
        public static string DocumentPath = "";

        protected static String[] items;

        public static List<long> Branchlist = new List<long>();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                VSContractPopup.CssClass = "alert alert-danger";
                vsTaskTab.CssClass = "alert alert-danger";
                items = new String[] { "Not Started", "In Progress", "Under Review", "Closed" };

                //Page.Header.DataBind();

                if (!IsPostBack)
                {
                    BindVendors();
                    BindCustomerBranches();
                    BindDepartments();
                    BindUsers();
                    //  BindGrid(Convert.ToInt32(ViewState["ContractInstanceID"]));
                    BindContractCategoryType();
                    BindContractStatusList(false, true);

                    ViewState["PageLink"] = "ContractLink";

                    if (!string.IsNullOrEmpty(Request.QueryString["AccessID"]))
                    {
                        var contractInstanceID = Request.QueryString["AccessID"];
                        if (contractInstanceID != "")
                        {
                            ViewState["ContractInstanceID"] = contractInstanceID;
                            Session["ContractInstanceID"] = contractInstanceID;

                            if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                            {
                                ViewState["FlagHistory"] = "1";
                            }
                            else
                            {
                                ViewState["FlagHistory"] = null;
                            }

                            if (Convert.ToInt64(contractInstanceID) == 0)
                            {
                                btnAddContract_Click(sender, e);  //Add New Detail
                            }
                            else
                            {
                                btnEditContract_Click(sender, e); //Edit Detail                                
                            }
                        }
                    }

                    ViewState["CustomefieldCount"] = null;

                    //Show Hide Grid Control - Enable/Disable Form Controls
                    if (ViewState["ContractStatusID"] != null)
                    {
                        //if (Convert.ToInt64(ViewState["ContractStatusID"]) == 9)
                        //{
                        //    showHideContractSummaryTabTopButtons(false);
                        //    enableDisableContractPopUpControls(false);
                        //}
                        //else
                        //{
                        //    showHideContractSummaryTabTopButtons(true);
                        //    enableDisableContractPopUpControls(true);
                        //}

                        showHideContractSummaryTabTopButtons(true);
                        enableDisableContractPopUpControls(true);

                        string customer = ConfigurationManager.AppSettings["ContractActionDisplay"].ToString();

                        if (customer == Convert.ToString(AuthenticationHelper.CustomerID))
                        {
                            long contractInstanceID = 0;
                            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                            {
                                contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

                                if (contractInstanceID != 0)
                                {
                                    var lstContractUserAssignment = ContractManagement.GetContractUserAssignment_All(contractInstanceID);
                                    if (lstContractUserAssignment.Count > 0)
                                    {
                                        int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                                        var query = lstContractUserAssignment.Where(x => x.UserID == UserID).ToList();
                                        if (query.Count <= 0)
                                        {
                                            btnEditContractDetail.Visible = false;
                                        }
                                        else
                                        {
                                            btnEditContractDetail.Visible = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        enableDisableContractPopUpControls(true);
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                    {
                        ViewState["FlagHistory"] = "1";

                        enableDisableContractPopUpControls(false);
                        showHideContractSummaryTabTopButtons(false);
                        ShowHide_AddNewButtons(false);
                    }
                    else
                    {
                        ViewState["FlagHistory"] = null;
                        ShowHide_AddNewButtons(true);
                    }
                }

                applyCSStoFileTag_ListItems();

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region Common

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                items = statusList.Select(row => row.StatusName).ToArray();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractCategoryType()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstContractTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

                if (lstContractTypes.Count > 0)
                    lstContractTypes = lstContractTypes.OrderBy(row => row.TypeName).ToList();

                ddlContractType.DataTextField = "TypeName";
                ddlContractType.DataValueField = "ID";

                ddlContractType.DataSource = lstContractTypes;
                ddlContractType.DataBind();

                //ddlContractType.Items.Add(new ListItem("Add New", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnRebind_Type_Click(object sender, EventArgs e)
        {
            BindContractCategoryType();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "rebindContractTypes();", true);
        }

        private bool BindContractSubType(long contractTypeID)
        {
            bool bindSuccess = false;
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);
                var lstContractSubTypes = ContractTypeMasterManagement.GetContractSubType_All(customerID, contractTypeID);
                if (lstContractSubTypes.Count > 0)
                {
                    ddlContractSubType.DataTextField = "SubTypeName";
                    ddlContractSubType.DataValueField = "ContractSubTypeID";
                    ddlContractSubType.DataSource = lstContractSubTypes;
                    ddlContractSubType.DataBind();
                    //ddlContractSubType.Items.Add(new ListItem("Add New", "0"));
                    bindSuccess = true;
                }
                return bindSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return bindSuccess;
            }
        }

        protected void lnkBtnRebind_SubType_Click(object sender, EventArgs e)
        {
            if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
            {
                BindContractSubType(Convert.ToInt64(ddlContractType.SelectedValue));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTypes", "rebindSubTypes();", true);
            }
        }

        private void BindFileTags()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    if (contractInstanceID != 0)
                    {
                        var lstTags = ContractDocumentManagement.GetDistinctFileTags(customerID, contractInstanceID);

                        lstBoxFileTags.Items.Clear();

                        if (lstTags.Count > 0)
                        {
                            for (int i = 0; i < lstTags.Count; i++)
                            {
                                lstBoxFileTags.Items.Add(new ListItem(lstTags[i], lstTags[i]));
                            }
                        }

                        applyCSStoFileTag_ListItems();
                    }
                    else
                    {
                        lstBoxFileTags.DataSource = null;
                        lstBoxFileTags.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void applyCSStoFileTag_ListItems()
        {
            foreach (ListItem eachItem in lstBoxFileTags.Items)
            {
                if (eachItem.Selected)
                    eachItem.Attributes.Add("class", "label label-info-selected");
                else
                    eachItem.Attributes.Add("class", "label label-info");
                eachItem.Attributes.Add("onclick", "fcheckcontract(this)");
            }
        }

        protected void lstBoxFileTags_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindContractDocuments_All();

                List<ListItem> lstSelectedTags = new List<ListItem>();

                foreach (ListItem eachListItem in lstBoxFileTags.Items)
                {
                    if (eachListItem.Selected)
                        lstSelectedTags.Add(eachListItem);
                }

                //Re-Arrange File Tags
                var arrangedListItems = ContractManagement.ReArrange_FileTags(lstBoxFileTags);

                lstBoxFileTags.DataSource = arrangedListItems;
                lstBoxFileTags.DataBind();

                foreach (ListItem eachListItem in lstSelectedTags)
                {
                    if (lstBoxFileTags.Items.FindByValue(eachListItem.Value) != null)
                        lstBoxFileTags.Items.FindByValue(eachListItem.Value).Selected = true;
                }

                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;

                //var branchs = CustomerBranchManagement.GetAllHierarchy(customerID);
                //var branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                List<NameValueHierarchy> branchs;
                string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                if (CacheHelper.Exists(key))
                {
                    CacheHelper.Get<List<NameValueHierarchy>>(key, out branchs);
                }
                else
                {
                    branchs = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    CacheHelper.Set<List<NameValueHierarchy>>(key, branchs);
                }
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }

                //tbxBranch.Text = "Select Entity/Location";
                tbxBranch.Text = "Click to Select";

                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        public void BindDepartments()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstDepts = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            if (lstDepts.Count > 0)
                lstDepts = lstDepts.OrderBy(row => row.Name).ToList();

            ddlDepartment.DataTextField = "Name";
            ddlDepartment.DataValueField = "ID";

            ddlDepartment.DataSource = lstDepts;
            ddlDepartment.DataBind();

            //ddlDepartment.Items.Add(new ListItem("Add New", "0"));
        }

      
        protected void lnkBtnRebind_Dept_Click(object sender, EventArgs e)
        {
            BindDepartments();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindDepts", "rebindDepartments();", true);
        }

        protected void lnkBtnRebind_ContactPersonDept_Click(object sender, EventArgs e)
        {
        
            BindUsers();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "restoreSelectedTaskUsersNew();", true);
        }


        public void BindGrid(int contractID)
        {
            try
            {
                if (contractID != 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var lstLinkedContracts = (from row in entities.Cont_SP_GetAssignedContracts_All(Convert.ToInt32(AuthenticationHelper.CustomerID))
                                                  select row).ToList();
                        if (lstLinkedContracts.Count > 0)
                        {
                            lstLinkedContracts = (from g in lstLinkedContracts
                                                  group g by new
                                                  {
                                                      g.ID, //ContractID
                                                      g.CustomerID,
                                                      g.ContractNo,
                                                      g.ContractTitle,
                                                      g.ContractDetailDesc,
                                                      g.VendorIDs,
                                                      g.VendorNames,
                                                      g.CustomerBranchID,
                                                      g.BranchName,
                                                      g.DepartmentID,
                                                      g.DeptName,
                                                      g.ContractTypeID,
                                                      g.TypeName,
                                                      g.ContractSubTypeID,
                                                      g.SubTypeName,
                                                      g.ProposalDate,
                                                      g.AgreementDate,
                                                      g.EffectiveDate,
                                                      g.ReviewDate,
                                                      g.ExpirationDate,
                                                      g.CreatedOn,
                                                      g.UpdatedOn,
                                                      g.StatusName,
                                                      g.ContractAmt,
                                                      g.ContactPersonOfDepartment,
                                                      g.PaymentType,
                                                      g.AddNewClause
                                                  } into GCS
                                                  select new Cont_SP_GetAssignedContracts_All_Result()
                                                  {
                                                      ID = GCS.Key.ID, //ContractID
                                                      CustomerID = GCS.Key.CustomerID,
                                                      ContractNo = GCS.Key.ContractNo,
                                                      ContractTitle = GCS.Key.ContractTitle,
                                                      ContractDetailDesc = GCS.Key.ContractDetailDesc,
                                                      VendorIDs = GCS.Key.VendorIDs,
                                                      VendorNames = GCS.Key.VendorNames,
                                                      CustomerBranchID = GCS.Key.CustomerBranchID,
                                                      BranchName = GCS.Key.BranchName,
                                                      DepartmentID = GCS.Key.DepartmentID,
                                                      DeptName = GCS.Key.DeptName,
                                                      ContractTypeID = GCS.Key.ContractTypeID,
                                                      TypeName = GCS.Key.TypeName,
                                                      ContractSubTypeID = GCS.Key.ContractSubTypeID,
                                                      SubTypeName = GCS.Key.SubTypeName,
                                                      ProposalDate = GCS.Key.ProposalDate,
                                                      AgreementDate = GCS.Key.AgreementDate,
                                                      EffectiveDate = GCS.Key.EffectiveDate,
                                                      ReviewDate = GCS.Key.ReviewDate,
                                                      ExpirationDate = GCS.Key.ExpirationDate,
                                                      CreatedOn = GCS.Key.CreatedOn,
                                                      UpdatedOn = GCS.Key.UpdatedOn,
                                                      StatusName = GCS.Key.StatusName,
                                                      ContractAmt = GCS.Key.ContractAmt,
                                                      ContactPersonOfDepartment = GCS.Key.ContactPersonOfDepartment,
                                                      PaymentType = GCS.Key.PaymentType,
                                                      AddNewClause = GCS.Key.AddNewClause
                                                  }).ToList();
                        }
                        lstLinkedContracts = lstLinkedContracts.Where(entry => entry.ID != contractID).ToList();

                        grdContractList_LinkContract.DataSource = lstLinkedContracts;
                        grdContractList_LinkContract.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public static void BindContractDocuments(GridView grd, ListBox lstBoxFileTags)
        {
            if (HttpContext.Current.Session["ContractInstanceID"] != null)
            {
                long ContractInstanceID = Convert.ToInt64(HttpContext.Current.Session["ContractInstanceID"]);

                List<ListItem> selectedItems = ContractManagement.GetSelectedItems(lstBoxFileTags);

                var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                List<Cont_SP_GetContractDocuments_FileTags_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_FileTags_All_Result>();

                lstContDocs = ContractDocumentManagement.Cont_SP_GetContractDocuments_FileTags_All(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID, selectedFileTags);

                grd.DataSource = lstContDocs;
                grd.DataBind();

                foreach (ListItem eachItem in lstBoxFileTags.Items)
                {
                    eachItem.Attributes.Add("class", "label label-info");
                }

                lstContDocs.Clear();
                lstContDocs = null;

                //upContractDocUploadPopup.Update();
            }
        }

        [WebMethod]
        public static List<ListItem> GetDepartments()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstDepartments = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            List<ListItem> lstDepts = new List<ListItem>();

            if (lstDepartments.Count > 0)
            {
                lstDepartments = lstDepartments.OrderBy(row => row.Name).ToList();

                for (int i = 0; i < lstDepartments.Count; i++)
                {
                    lstDepts.Add(new ListItem
                    {
                        Value = lstDepartments.ToList()[i].ID.ToString(),
                        Text = lstDepartments.ToList()[i].Name,
                    });
                }

                //lstDepts.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstDepts;
        }

        [WebMethod]
        public static List<ListItem> GetContractTypes()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstTypes = ContractTypeMasterManagement.GetContractTypes_All(customerID);

            List<ListItem> lstcontType = new List<ListItem>();

            if (lstTypes.Count > 0)
            {
                lstTypes = lstTypes.OrderBy(row => row.TypeName).ToList();

                for (int i = 0; i < lstTypes.Count; i++)
                {
                    lstcontType.Add(new ListItem
                    {
                        Value = lstTypes.ToList()[i].ID.ToString(),
                        Text = lstTypes.ToList()[i].TypeName,
                    });
                }

                //lstcontType.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstcontType;
        }

        [WebMethod]
        public static List<ListItem> GetContractSubTypes(string selectedContractType)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstContSubType = new List<ListItem>();

            long contTypeID = Convert.ToInt64(selectedContractType);
            if (contTypeID > 0)
            {
                var obj = ContractTypeMasterManagement.GetContractSubType_All(customerID, contTypeID);

                if (obj.Count > 0)
                {
                    obj = obj.OrderBy(row => row.SubTypeName).ToList();

                    for (int i = 0; i < obj.Count; i++)
                    {
                        lstContSubType.Add(new ListItem
                        {
                            Value = obj.ToList()[i].ContractSubTypeID.ToString(),
                            Text = obj.ToList()[i].SubTypeName,
                        });
                    }

                    //lstContSubType.Add(new ListItem
                    //{
                    //    Value = "0",
                    //    Text = "Add New",
                    //});
                }
            }
            return lstContSubType;
        }

        [WebMethod]
        public static List<ListItem> GetContractVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstVendors = new List<ListItem>();

            var obj = ContractMastersManagement.GetVendors_All(customerID);

            if (obj.Count > 0)
            {
                obj = obj.OrderBy(row => row.VendorName).ToList();

                for (int i = 0; i < obj.Count; i++)
                {
                    lstVendors.Add(new ListItem
                    {
                        Value = obj.ToList()[i].ID.ToString(),
                        Text = obj.ToList()[i].VendorName,
                    });
                }

                //lstVendors.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstVendors;
        }

        [WebMethod]
        public static List<ListItem> GetTaskUsers()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstInternalUsers = new List<ListItem>();

            var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

            //var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

            lstAllUsers = lstAllUsers.Where(row => row.IsActive == true).ToList();

            if (lstAllUsers.Count > 0)
            {
                for (int i = 0; i < lstAllUsers.Count; i++)
                {
                    lstInternalUsers.Add(new ListItem
                    {
                        Value = lstAllUsers.ToList()[i].ID.ToString(),
                        Text = lstAllUsers.ToList()[i].FirstName + " " + lstAllUsers.ToList()[i].LastName,
                    });
                }

                //lstInternalUsers.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstInternalUsers;
        }

        [WebMethod]
        public static List<ListItem> GetDocTypes()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            List<ListItem> lstdocumentType = new List<ListItem>();

            var lstAllDocTypes = ContractMastersManagement.GetContractDocTypes_All(customerID);
            if (lstAllDocTypes.Count > 0)
            {
                lstAllDocTypes = lstAllDocTypes.OrderBy(row => row.TypeName).ToList();

                for (int i = 0; i < lstAllDocTypes.Count; i++)
                {
                    lstdocumentType.Add(new ListItem
                    {
                        Value = lstAllDocTypes.ToList()[i].ID.ToString(),
                        Text = lstAllDocTypes.ToList()[i].TypeName,
                    });
                }

                //lstdocumentType.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstdocumentType;
        }

        [WebMethod]
        public static List<ListItem> GetCustomFields(string selectedContractType)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            long typeID = Convert.ToInt64(selectedContractType);

            List<ListItem> lstCustomFields = new List<ListItem>();

            var lstAllCustomFields = ContractMastersManagement.GetCustomFields_All(customerID, typeID);

            if (lstAllCustomFields.Count > 0)
            {
                lstAllCustomFields = lstAllCustomFields.OrderBy(row => row.Label).ToList();

                for (int i = 0; i < lstAllCustomFields.Count; i++)
                {
                    lstCustomFields.Add(new ListItem
                    {
                        Value = lstAllCustomFields.ToList()[i].ID.ToString(),
                        Text = lstAllCustomFields.ToList()[i].Label,
                    });
                }

                //lstCustomFields.Add(new ListItem
                //{
                //    Value = "0",
                //    Text = "Add New",
                //});
            }

            return lstCustomFields;
        }

        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = ContractMastersManagement.GetVendors_All(customerID);

            if (lstVendors.Count > 0)
                lstVendors = lstVendors.OrderBy(row => row.VendorName).ToList();

            //Drop-Down at Modal Pop-up
            lstBoxVendor.DataTextField = "VendorName";
            lstBoxVendor.DataValueField = "ID";

            lstBoxVendor.DataSource = lstVendors;
            lstBoxVendor.DataBind();

            //lstBoxVendor.Items.Add(new ListItem("Add New", "0"));
        }

        protected void lnkBtnRebind_Vendor_Click(object sender, EventArgs e)
        {
            BindVendors();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindVendors", "restoreSelectedVendors();", true);
        }

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var internalUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 1); //1--Internal---

                lstBoxOwner.DataValueField = "ID";
                lstBoxOwner.DataTextField = "Name";
                lstBoxOwner.DataSource = internalUsers;
                lstBoxOwner.DataBind();

                lstBoxApprover.DataValueField = "ID";
                lstBoxApprover.DataTextField = "Name";
                lstBoxApprover.DataSource = internalUsers;
                lstBoxApprover.DataBind();

               

                var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

                ddlCPDepartment.DataValueField = "ID";
                ddlCPDepartment.DataTextField = "Name";
                ddlCPDepartment.DataSource = allUsers;
                ddlCPDepartment.DataBind();

                lstBoxTaskUser.DataValueField = "ID";
                lstBoxTaskUser.DataTextField = "Name";
                lstBoxTaskUser.DataSource = allUsers;
                lstBoxTaskUser.DataBind();

                //lstBoxTaskUser.Items.Add(new ListItem("Add New", "0"));

                ddlUsers_AuditLog.DataValueField = "ID";
                ddlUsers_AuditLog.DataTextField = "Name";
                ddlUsers_AuditLog.DataSource = allUsers;
                ddlUsers_AuditLog.DataBind();

                lstAllUsers.Clear();
                lstAllUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTaskUsers()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                if (lstAllUsers.Count > 0)
                    lstAllUsers = lstAllUsers.Where(entry => entry.ContractRoleID != null).ToList();

                var allUsers = ContractUserManagement.GetRequiredUsers(lstAllUsers, 4); //4--All---Internal and External Users

                lstBoxTaskUser.DataValueField = "ID";
                lstBoxTaskUser.DataTextField = "Name";
                lstBoxTaskUser.DataSource = allUsers;
                lstBoxTaskUser.DataBind();

                //lstBoxTaskUser.Items.Add(new ListItem("Add New", "0"));

                lstAllUsers.Clear();
                lstAllUsers = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnRebind_TaskUser_Click(object sender, EventArgs e)
        {
            BindTaskUsers();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "rebindTaskUser", "restoreSelectedTaskUsers();", true);
        }

        public void BindContractAuditLogs()
        {
            try
            {
                long totalRowCount = 0;

                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    int userID = -1;
                    if (!(string.IsNullOrEmpty(ddlUsers_AuditLog.SelectedValue)))
                    {
                        userID = Convert.ToInt32(ddlUsers_AuditLog.SelectedValue);
                    }

                    string fromDate = string.Empty;
                    string toDate = string.Empty;

                    long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    if (contractInstanceID != 0)
                    {
                        var lstAuditLogs = ContractManagement.GetContractAuditLogs_All(customerID, contractInstanceID);

                        if (userID != -1 && userID != 0)
                            lstAuditLogs = lstAuditLogs.Where(row => row.CreatedBy == userID).ToList();

                        if (!(string.IsNullOrEmpty(txtFromDate_AuditLog.Text.Trim())))
                        {
                            try
                            {
                                fromDate = txtFromDate_AuditLog.Text.Trim();
                                lstAuditLogs = lstAuditLogs.Where(row => row.CreatedOn >= Convert.ToDateTime(fromDate)).ToList();
                            }
                            catch (Exception ex)
                            {
                                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                            }
                        }

                        if (!(string.IsNullOrEmpty(txtToDate_AuditLog.Text.Trim())))
                        {
                            try
                            {
                                toDate = txtToDate_AuditLog.Text.Trim();
                                lstAuditLogs = lstAuditLogs.Where(row => row.CreatedOn <= Convert.ToDateTime(toDate)).ToList();
                            }
                            catch (Exception)
                            {

                            }
                        }

                        divAuditLog_Vertical.InnerHtml = ShowAuditLog_Centered_Clickable(lstAuditLogs);
                        //divAuditLog_Vertical.InnerHtml = ShowAuditLog_Clickable(lstAuditLogs);

                        //divAuditLog.InnerHtml = ShowAuditLog_Timeline(lstAuditLogs);
                        //divAuditLog.InnerHtml = ShowAuditLog_VerticalTimeline(lstAuditLogs);

                        //int PageNumber = 1;
                        //if (!(string.IsNullOrEmpty(ddlPageNo_AuditLog.SelectedValue)))
                        //{
                        //    PageNumber = Convert.ToInt32(ddlPageNo_AuditLog.SelectedValue);
                        //}

                        //var lstAuditLogs = ContractManagement.GetContractAuditLogs_Paging(customerID, contractInstanceID, Convert.ToInt32(gvContractAuditLog.PageSize), PageNumber);

                        //gvContractAuditLog.DataSource = lstAuditLogs;
                        //gvContractAuditLog.DataBind();

                        //if (lstAuditLogs.Count > 0)
                        //{
                        //    if (lstAuditLogs[0].TotalRowCount != null)
                        //        totalRowCount = Convert.ToInt64(lstAuditLogs[0].TotalRowCount);
                        //    else
                        //    {
                        //        foreach (var item in lstAuditLogs)
                        //        {
                        //            totalRowCount = Convert.ToInt32(item.TotalRowCount);
                        //            break;
                        //        }
                        //    }
                        //}
                    }
                    else
                    {
                        gvContractAuditLog.DataSource = null;
                        gvContractAuditLog.DataBind();
                    }
                }
                else
                {
                    gvContractAuditLog.DataSource = null;
                    gvContractAuditLog.DataBind();
                }

                Session["TotalRows"] = totalRowCount;
                //lblStartRecord_AuditLog.Text = ddlPageNo_AuditLog.SelectedValue;

                if (Convert.ToInt32(ViewState["PageNumberAuditLogFlagID"]) == 0)
                {
                    bindPageNumber(3);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractHistory(int customerID, long contractInstanceID)
        {
            if (contractInstanceID != 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var lstContractHistory = (from row in entities.Cont_SP_ContractHistory(contractInstanceID, customerID)
                                              select row).ToList();

                    if (lstContractHistory != null)
                    {
                        grdContractHistory.DataSource = lstContractHistory;
                        grdContractHistory.DataBind();

                        if (lstContractHistory.Count > 0)
                            divContractHistory.Visible = true;
                        else
                            divContractHistory.Visible = false;
                    }
                }
            }
            else
                divContractHistory.Visible = false;
        }

        private void HideShowGridColumns(GridView gridView, string headerTextToMatch, bool flag)
        {
            try
            {
                if (gridView == null)
                {
                    return;
                }

                // Loop through all of the columns in the grid.
                for (int i = 0; i < gridView.Columns.Count; i++)
                {
                    String headerText = gridView.Columns[i].HeaderText;

                    //Show Hide Columns with Specific headerText
                    if (headerText == headerTextToMatch)
                        gridView.Columns[i].Visible = flag;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindContractDetails()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    var ExistingRecord = ContractManagement.ExistsContractDetails(contractID, customerID);

                    if (ExistingRecord != null)
                    {
                        txtTerms.Text = ExistingRecord.Terms.ToString();
                        txtTermination.Text = ExistingRecord.Termination.ToString();
                        txtMisc.Text = ExistingRecord.Misc.ToString();
                        txtLicense.Text = ExistingRecord.License.ToString();
                        txtWarranties.Text = ExistingRecord.Warranties.ToString();
                        txtGuarantees.Text = ExistingRecord.Guarantees.ToString();
                        txtConfidential.Text = ExistingRecord.Confidential.ToString();
                        txtOwnership.Text = ExistingRecord.Ownership.ToString();
                        txtIndemnification.Text = ExistingRecord.Indemnification.ToString();
                        txtGeneral.Text = ExistingRecord.General.ToString();
                        txtTimelines.Text = ExistingRecord.Timelines.ToString();
                        txtEscalation.Text = ExistingRecord.Escalation.ToString();
                        txtMilestones.Text = ExistingRecord.Milestone.ToString();
                        txtOthers.Text = ExistingRecord.Others.ToString();
                    }
                    else
                    {
                        clearContractDetailsControls();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void TabContract_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            liContractSummary.Attributes.Add("class", "active");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
            {
                int ContractID = Convert.ToInt32(ViewState["ContractInstanceID"]);
                BindContractListToLink(ContractID);
            }

            if (ViewState["Mode"] != null)
            {
                if (Convert.ToInt32(ViewState["Mode"]) == 0)
                {
                    enableDisableContractSummaryTabControls(true);
                    toggleTextSaveButton_ContractSummaryTab(true);

                }
                else if (Convert.ToInt32(ViewState["Mode"]) == 1)
                {
                    bool historyFlag = false;
                    if (ViewState["FlagHistory"] != null)
                    {
                        historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                    }

                    enableDisableContractSummaryTabControls(false);
                    toggleTextSaveButton_ContractSummaryTab(false);

                    if (historyFlag)
                        showHideContractSummaryTabTopButtons(false);
                    else
                        showHideContractSummaryTabTopButtons(true);
                }
            }
        }

        protected void TabContractDetail_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "active");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 1;

            clearContractDetailsControls();
            BindContractDetails();
            BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));
        }

        protected void TabDocument_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "DocumentLink";
            showHideContractSummaryTabTopButtons(false);
            ViewState["PageNumberFlagID"] = 0;
            DropDownListPageNo.ClearSelection();

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "active");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 2;

            //BindContractDocType();

            BindFileTags();
            BindContractDocuments_Paging();
            BindGrid(Convert.ToInt32(ViewState["ContractInstanceID"]));
        }

        protected void TabTask_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "active");
            liAuditLog.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 3;

            ViewState["TaskMode"] = "0";

            ViewState["TaskPageFlag"] = "0";

            if (ViewState["ContractInstanceID"] != null)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                if (contractInstanceID != 0)
                {
                    BindContractTasks_Paging(customerID, contractInstanceID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);
                }
                BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));
            }

            HideShowTaskDiv(false);
        }

        protected void TabAuditLog_Click(object sender, EventArgs e)
        {
            ViewState["PageLink"] = "ContractLink";
            showHideContractSummaryTabTopButtons(false);

            liContractSummary.Attributes.Add("class", "");
            liContractDetails.Attributes.Add("class", "");
            liDocument.Attributes.Add("class", "");
            liTask.Attributes.Add("class", "");
            liAuditLog.Attributes.Add("class", "active");
            ViewState["PageNumberAuditLogFlagID"] = 0;

            MainView.ActiveViewIndex = 4;

            if (ViewState["ContractInstanceID"] != null)
            {
                BindContractAuditLogs();
            }
            BindContractListToLink(Convert.ToInt32(ViewState["ContractInstanceID"]));
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "Select Entity/Location";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Server Error Occurred. Please try again.";
                cvContractPopUp.CssClass = "alert alert-danger";
            }
        }

        protected void ddlContractType_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bindSuccess = false;

            if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
            {
                ViewState["ddlCustomFieldFilled"] = null;
                ViewState["dataTableCustomFields"] = null;

                ViewState["CustomefieldCount"] = null;

                ViewState["ContractTypeIDUpdated"] = "false";

                bindSuccess = BindContractSubType(Convert.ToInt64(ddlContractType.SelectedValue));

                lnkAddNewContractSubTypeModal.Visible = true;

                Session["ContractTypeID"] = ddlContractType.SelectedValue;

                BindCustomFields(grdCustomField); //, grdCustomField_History
            }

            if (!bindSuccess)
            {
                if (ddlContractSubType.Items.Count > 0)
                    ddlContractSubType.Items.Clear();

                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0" && ddlContractType.SelectedValue != "")
                {
                    lnkAddNewContractSubTypeModal.Visible = true;
                    //ddlContractSubType.Items.Add(new ListItem("Add New", "0"));
                }
                else
                    lnkAddNewContractSubTypeModal.Visible = false;
            }

            upContractTypeSubType.Update();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "changeContractType", "ddlContractTypeChange(); ddlCustomFieldChange();", true);
        }

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }

        #endregion

        #region Contract Detail

        protected void btnClearContractControls_Click(object sender, EventArgs e)
        {
            try
            {
                clearContractControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearContractControls()
        {
            try
            {
                txtContractNo.Text = "";
                txtTitle.Text = "";
                tbxDescription.Text = "";

                tbxBranch.Text = "";
                tbxBranch.Text = "Select Entity/Location";
                lstBoxVendor.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlCPDepartment.ClearSelection();

                txtProposalDate.Text = "";
                txtAgreementDate.Text = "";
                txtEffectiveDate.Text = "";

                txtReviewDate.Text = "";
                txtExpirationDate.Text = "";
                txtNoticeTerm.Text = "";

                ddlContractType.ClearSelection();
                ddlContractSubType.ClearSelection();
                lstBoxOwner.ClearSelection();
                lstBoxApprover.ClearSelection();

                tbxContractAmt.Text = "";
                ddlPaymentTerm.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void clearContractDetailsControls()
        {
            txtTerms.Text = "";
            txtTermination.Text = "";
            txtMisc.Text = "";
            txtLicense.Text = "";
            txtWarranties.Text = "";
            txtGuarantees.Text = "";
            txtConfidential.Text = "";
            txtOwnership.Text = "";
            txtIndemnification.Text = "";
            txtGeneral.Text = "";
            txtTimelines.Text = "";
            txtEscalation.Text = "";
            txtMilestones.Text = "";
        }

        public void enableDisableContractSummaryTabControls(bool flag)
        {
            try
            {
                string customer = ConfigurationManager.AppSettings["Statusreviewed"].ToString();

                long customerID = AuthenticationHelper.CustomerID;
                List<string> CustomerList = customer.Split(',').ToList();
                if (CustomerList.Count > 0)
                {
                    foreach (string PList in CustomerList)
                    {
                        if (PList == customerID.ToString())
                        {
                            if (ddlContractStatus.SelectedValue == "3")
                            {
                                flag = false;
                                break;
                            }                            
                        }
                    }
                }
             
              
                ddlContractStatus.Enabled = flag;
                txtContractNo.Enabled = flag;
                txtTitle.Enabled = flag;
                tbxDescription.Enabled = flag;

                tbxBranch.Enabled = flag;
                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;
                

                ddlCPDepartment.Enabled = flag;

                tbxAddNewClause.Enabled = flag;
                txtProposalDate.Enabled = flag;
                txtAgreementDate.Enabled = flag;
                txtEffectiveDate.Enabled = flag;

                txtReviewDate.Enabled = flag;
                txtExpirationDate.Enabled = flag;

                txtNoticeTerm.Enabled = flag;
                ddlNoticeTerm.Enabled = flag;

                ddlPaymentType.Enabled = flag;

                tbxContractAmt.Enabled = flag;
                ddlPaymentTerm.Enabled = flag;

                lstBoxVendor.Enabled = flag;
                ddlDepartment.Enabled = flag;

                ddlContractType.Enabled = flag;
                ddlContractSubType.Enabled = flag;
                lstBoxOwner.Enabled = flag;
                lstBoxApprover.Enabled = flag;

                txtBoxProduct.Enabled = flag;

                //lnkShowAddNewVendorModal.Visible = flag;
                //lnkAddNewDepartmentModal.Visible = flag;
                //lnkAddNewContractTypeModal.Visible = flag;
                //lnkAddNewContractSubTypeModal.Visible = flag;

                if (flag)
                {
                    ddlDepartment.Attributes.Remove("disabled");
                    ddlCPDepartment.Attributes.Remove("disabled");
                    ddlNoticeTerm.Attributes.Remove("disabled");
                    ddlContractType.Attributes.Remove("disabled");
                    ddlContractSubType.Attributes.Remove("disabled");

                    lstBoxVendor.Attributes.Remove("disabled");
                    lstBoxOwner.Attributes.Remove("disabled");
                    lstBoxApprover.Attributes.Remove("disabled");

                    btnSaveContract.Attributes.Remove("disabled");
                    btnClearContractDetail.Attributes.Remove("disabled");

                    lnkShowAddNewVendorModal.Enabled = true;
                    lnkAddNewPDepartmentModal.Enabled = true;
                    lnkAddNewDepartmentModal.Enabled = true;
                    lnkAddNewContractTypeModal.Enabled = true;
                    lnkAddNewContractSubTypeModal.Enabled = true;
                }
                else
                {
                    ddlDepartment.Attributes.Add("disabled", "disabled");
                    ddlCPDepartment.Attributes.Add("disabled", "disabled");
                    ddlNoticeTerm.Attributes.Add("disabled", "disabled");
                    ddlContractType.Attributes.Add("disabled", "disabled");
                    ddlContractSubType.Attributes.Add("disabled", "disabled");

                    lstBoxOwner.Attributes.Add("disabled", "disabled");
                    lstBoxVendor.Attributes.Add("disabled", "disabled");
                    lstBoxApprover.Attributes.Add("disabled", "disabled");

                    btnSaveContract.Attributes.Add("disabled", "disabled");
                    btnClearContractDetail.Attributes.Add("disabled", "disabled");

                    lnkShowAddNewVendorModal.Enabled = false;
                    lnkAddNewDepartmentModal.Enabled = false;
                    lnkAddNewPDepartmentModal.Enabled = false;
                    lnkAddNewContractTypeModal.Enabled = false;
                    lnkAddNewContractSubTypeModal.Enabled = false;
                }

                if (grdCustomField != null)
                {
                    grdCustomField.Enabled = flag;
                    HideShowGridColumns(grdCustomField, "Action", flag);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showHideContractSummaryTabTopButtons(bool flag)
        {
            try
            {
                topButtons.Visible = flag;
                //divContractHistory.Visible = flag;
                btnEditContractDetail.Visible = flag;

                lnkSendMailWithDoc.Visible = flag;

                lnkLinkContract.Visible = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void showHideTabs(bool flag)
        {
            try
            {
                liContractDetails.Visible = flag;
                liDocument.Visible = flag;
                liTask.Visible = flag;
                liAuditLog.Visible = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void ShowHide_AddNewButtons(bool flag)
        {
            try
            {
                lnkBtnUploadDocument.Visible = flag;
                lnkAddNewTask.Visible = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        public void toggleTextSaveButton_ContractSummaryTab(bool flag)
        {
            try
            {
                if (flag) //Add Mode     
                {
                    btnSaveContract.Text = "Save";
                    btnClearContractDetail.Visible = flag;
                }
                else //Edit Mode
                {
                    btnSaveContract.Text = "Update";
                    btnClearContractDetail.Visible = flag;
                }

                btnSaveContract.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void toggleTextSaveButton_AddEditTask(bool flag)
        {
            try
            {
                if (flag) //Add Mode     
                {
                    rbTaskType.Enabled = flag;
                    btnTaskSave.Text = "Create & Assign";
                    btnTaskClear.Visible = flag;
                }
                else //Edit Mode
                {
                    rbTaskType.Enabled = flag;
                    btnTaskSave.Text = "Update";
                    btnTaskClear.Visible = flag;
                }

                btnTaskSave.Visible = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void enableDisableContractPopUpControls(bool flag)
        {
            try
            {
                btnEditContractDetail.Enabled = flag;
                lnkSendMailWithDoc.Enabled = flag;
                lnkLinkContract.Enabled = flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditContractControls_Click(object sender, EventArgs e)
        {
            try
            {
                enableDisableContractSummaryTabControls(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddContract_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                clearContractControls();

                showHideContractSummaryTabTopButtons(false);
                enableDisableContractPopUpControls(true);

                lblCustomField.Visible = false;
                divLinkedContracts.Visible = false;

                TabContract_Click(sender, e);

                showHideTabs(false);

                ddlContractType_SelectedIndexChanged(sender, e);

                ddlContractStatus.Enabled = false;

                if (ddlContractStatus.Items.FindByValue("1") != null) //1-Draft
                    ddlContractStatus.Items.FindByValue("1").Selected = true;

                AddSteps(items, statusBulletedList);
                SetProgress(0, statusBulletedList);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnEditContract_Click(object sender, EventArgs e)
        {
            try
            {

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                ViewState["Mode"] = 1;
                showHideContractSummaryTabTopButtons(true);
                clearContractControls();
                TabContract_Click(sender, e);
                long contractInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                {
                    contractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

                    if (contractInstanceID != 0)
                    {
                        ViewState["Mode"] = 1;

                        var contractRecord = ContractManagement.GetContractDetailsByContractID(customerID, contractInstanceID);
                        //var contractRecord = ContractManagement.GetContractByID(contractInstanceID);

                        if (contractRecord != null)
                        {
                            //SET Current Contract Status

                            if (contractRecord.ContractStatusID != 0)
                                ViewState["ContractStatusID"] = contractRecord.ContractStatusID;

                            ddlContractStatus.ClearSelection();

                            if (ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()) != null)
                                ddlContractStatus.Items.FindByValue(contractRecord.ContractStatusID.ToString()).Selected = true;

                            txtContractNo.Text = contractRecord.ContractNo;
                            txtTitle.Text = contractRecord.ContractTitle;
                            tbxDescription.Text = contractRecord.ContractDetailDesc;
                            tbxAddNewClause.Text = contractRecord.AddNewClause;

                            if (contractRecord.CustomerBranchID != 0)
                            {
                                foreach (TreeNode node in tvBranches.Nodes)
                                {
                                    if (node.Value == contractRecord.CustomerBranchID.ToString())
                                    {
                                        node.Selected = true;
                                    }
                                    foreach (TreeNode item1 in node.ChildNodes)
                                    {
                                        if (item1.Value == contractRecord.CustomerBranchID.ToString())
                                            item1.Selected = true;
                                    }
                                }
                            }

                            tvBranches_SelectedNodeChanged(null, null);

                            #region Vendor

                            var lstVendorMapping = ContractManagement.GetVendorMapping(contractInstanceID);

                            if (lstBoxVendor.Items.Count > 0)
                            {
                                lstBoxVendor.ClearSelection();
                            }

                            if (lstVendorMapping.Count > 0)
                            {
                                foreach (var VendorID in lstVendorMapping)
                                {
                                    if (lstBoxVendor.Items.FindByValue(VendorID.ToString()) != null)
                                        lstBoxVendor.Items.FindByValue(VendorID.ToString()).Selected = true;
                                }
                            }

                            #endregion

                            ddlDepartment.ClearSelection();

                            if (ddlDepartment.Items.FindByValue(contractRecord.DepartmentID.ToString()) != null)
                                ddlDepartment.SelectedValue = contractRecord.DepartmentID.ToString();

                            if (contractRecord.ProposalDate != null)
                                txtProposalDate.Text = Convert.ToDateTime(contractRecord.ProposalDate).ToString("dd-MM-yyyy");
                            else
                                txtProposalDate.Text = string.Empty;

                            if (contractRecord.AgreementDate != null)
                                txtAgreementDate.Text = Convert.ToDateTime(contractRecord.AgreementDate).ToString("dd-MM-yyyy");
                            else
                                txtAgreementDate.Text = string.Empty;

                            if (contractRecord.EffectiveDate != null)
                                txtEffectiveDate.Text = Convert.ToDateTime(contractRecord.EffectiveDate).ToString("dd-MM-yyyy");
                            else
                                txtEffectiveDate.Text = string.Empty;

                            if (contractRecord.ReviewDate != null)
                                txtReviewDate.Text = Convert.ToDateTime(contractRecord.ReviewDate).ToString("dd-MM-yyyy");
                            else
                                txtReviewDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtExpirationDate.Text = Convert.ToDateTime(contractRecord.ExpirationDate).ToString("dd-MM-yyyy");
                            else
                                txtExpirationDate.Text = string.Empty;

                            if (contractRecord.ExpirationDate != null)
                                txtNoticeTerm.Text = Convert.ToString(contractRecord.NoticeTermNumber);
                            else
                                txtNoticeTerm.Text = string.Empty;

                            if (ddlNoticeTerm.Items.FindByValue(contractRecord.NoticeTermType.ToString()) != null)
                                ddlNoticeTerm.SelectedValue = contractRecord.NoticeTermType.ToString();

                            ddlContractType.ClearSelection();

                            if (ddlContractType.Items.FindByValue(contractRecord.ContractTypeID.ToString()) != null)
                                ddlContractType.SelectedValue = contractRecord.ContractTypeID.ToString();

                            ddlContractType_SelectedIndexChanged(sender, e);

                            if (contractRecord.ContractSubTypeID != null)
                            {
                                ddlContractSubType.ClearSelection();

                                if (ddlContractSubType.Items.FindByValue(contractRecord.ContractSubTypeID.ToString()) != null)
                                    ddlContractSubType.SelectedValue = contractRecord.ContractSubTypeID.ToString();
                            }

                            #region Owner && Approvers                            

                            var lstContractUserAssignment = ContractManagement.GetContractUserAssignment_All(contractInstanceID);

                           

                            if (lstContractUserAssignment.Count > 0)
                            {
                                //int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                                //var query = lstContractUserAssignment.Where(x => x.UserID == UserID).ToList();
                                //if (query.Count <= 0)
                                //{
                                //    btnEditContractDetail.Enabled = false;
                                //}
                                
                                
                                lstBoxOwner.ClearSelection();
                                lstBoxApprover.ClearSelection();

                                foreach (var eachAssignmentRecord in lstContractUserAssignment)
                                {
                                    if (eachAssignmentRecord.RoleID == 3)
                                    {
                                        if (lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxOwner.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                    else if (eachAssignmentRecord.RoleID == 6)
                                    {
                                        if (lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()) != null)
                                            lstBoxApprover.Items.FindByValue(eachAssignmentRecord.UserID.ToString()).Selected = true;
                                    }
                                }
                            }

                            //var lstAssignedUserIDs = ContractManagement.GetAssignedUsersByRoleID(contractInstanceID, 3); //3--Owner

                            //if (lstBoxOwner.Items.Count > 0)
                            //{
                            //    lstBoxOwner.ClearSelection();
                            //}

                            //if (lstAssignedUserIDs.Count > 0)
                            //{
                            //    foreach (var userID in lstAssignedUserIDs)
                            //    {
                            //        if (lstBoxOwner.Items.FindByValue(userID.ToString()) != null)
                            //            lstBoxOwner.Items.FindByValue(userID.ToString()).Selected = true;
                            //    }
                            //}                          

                            #endregion

                            if (contractRecord.ContractAmt != null)
                                tbxContractAmt.Text = contractRecord.ContractAmt.ToString();
                            else
                                tbxContractAmt.Text = string.Empty;

                            if (contractRecord.PaymentTermID != null)
                            {
                                ddlPaymentTerm.ClearSelection();

                                if (ddlPaymentTerm.Items.FindByValue(contractRecord.PaymentTermID.ToString()) != null)
                                    ddlPaymentTerm.SelectedValue = contractRecord.PaymentTermID.ToString();
                            }


                            if (contractRecord.PaymentType != null)
                            {
                                ddlPaymentType.ClearSelection();

                                if (ddlPaymentType.Items.FindByValue(contractRecord.PaymentType.ToString()) != null)
                                    ddlPaymentType.SelectedValue = contractRecord.PaymentType.ToString();
                            }

                            if (contractRecord.ContactPersonOfDepartment != null)
                            {
                                ddlCPDepartment.ClearSelection();

                                if (ddlCPDepartment.Items.FindByValue(contractRecord.ContactPersonOfDepartment.ToString()) != null)
                                    ddlCPDepartment.SelectedValue = contractRecord.ContactPersonOfDepartment.ToString();
                            }

                            if (contractRecord.ProductItems != null)
                                txtBoxProduct.Text = contractRecord.ProductItems.ToString();
                            else
                                txtBoxProduct.Text = string.Empty;

                            if (contractRecord.AddNewClause != null)
                                tbxAddNewClause.Text = contractRecord.AddNewClause.ToString();
                            else
                                tbxAddNewClause.Text = string.Empty;

                            BindContractHistory(customerID, Convert.ToInt64(contractInstanceID));

                            BindLinkedContracts(Convert.ToInt64(contractInstanceID));
                            //Bind Case To Link
                            BindContractListToLink(Convert.ToInt64(contractInstanceID));

                            BindContractDocuments_All(grdMailDocumentList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveContract_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["Mode"] != null)
                {
                    bool formValidateSuccess = false;
                    bool saveSuccess = false;

                    List<Tuple<int, int>> lstContractUserMapping = new List<Tuple<int, int>>();
                    List<long> lstVendorMapping = new List<long>();

                    //List<int> lstOwnerMapping = new List<int>();
                    //List<int> lstApproverMapping = new List<int>();

                    #region Data Validation

                    List<string> lstErrorMsg = new List<string>();

                    //Contract Number
                    if (String.IsNullOrEmpty(txtContractNo.Text))
                        lstErrorMsg.Add("Required Contract Number");
                    else
                        formValidateSuccess = true;

                    //Contract Title
                    if (String.IsNullOrEmpty(txtTitle.Text))
                        lstErrorMsg.Add("Required Contract Title");
                    else
                        formValidateSuccess = true;

                    //Contract Description
                    if (String.IsNullOrEmpty(tbxDescription.Text))
                        lstErrorMsg.Add("Required Contract Description");
                    else
                        formValidateSuccess = true;

                    //Branch Location
                    if (tvBranches.SelectedValue != "" && tvBranches.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Entity/Branch/Location");

                    //Vendor/Contractor
                    int selectedVendorCount = 0;
                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                    {
                        if (eachVendor.Selected)
                            selectedVendorCount++;
                    }

                    if (selectedVendorCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select at least One Vendor");

                    //Department
                    if (ddlDepartment.SelectedValue != "" && ddlDepartment.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Department");
                    //Contact Person Of Department
                    if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Contact Person Of Department");

                    //Proposal Date
                    if (!string.IsNullOrEmpty(txtProposalDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtProposalDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Proposal Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Proposal Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    //Agreement Date
                    if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtAgreementDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Agreement Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Agreement Date or Date should be in DD-MM-YYYY Format");
                        }
                    }

                    if (ddlContractStatus.SelectedItem.Text != "Draft")
                    {
                        //Effective Date
                        if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                        {
                            try
                            {
                                bool check = ContractCommonMethods.CheckValidDate(txtEffectiveDate.Text);
                                if (!check)
                                {
                                    lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            catch (Exception)
                            {
                                lstErrorMsg.Add("Please Check Start Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        else
                            lstErrorMsg.Add("Required Start Date");

                        //Expiration Date
                        if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                        {
                            try
                            {
                                bool check = ContractCommonMethods.CheckValidDate(txtExpirationDate.Text);
                                if (!check)
                                {
                                    lstErrorMsg.Add("Please Check Expiration Date or Date should be in DD-MM-YYYY Format");
                                }
                            }
                            catch (Exception)
                            {
                                lstErrorMsg.Add("Please Check Expiration Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        else
                            lstErrorMsg.Add("Required End Date");
                    }

                    //Review Date
                    if (!string.IsNullOrEmpty(txtReviewDate.Text))
                    {
                        try
                        {
                            bool check = ContractCommonMethods.CheckValidDate(txtReviewDate.Text);
                            if (!check)
                            {
                                lstErrorMsg.Add("Please Check Review Date or Date should be in DD-MM-YYYY Format");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Please Check Review Date or Date should be in DD-MM-YYYY Format");
                        }
                    }


                    //Contract Type
                    if (ddlContractType.SelectedValue != "" && ddlContractType.SelectedValue != "-1")
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select Contract Type");

                    //Owner
                    int selectedOwnerCount = 0;
                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                    {
                        if (eachOwner.Selected)
                            selectedOwnerCount++;
                    }

                    if (selectedOwnerCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select at least One Contract Owner");

                    //Approver
                    //int selectedApproverCount = 0;
                    //foreach (ListItem eachApprover in lstBoxApprover.Items)
                    //{
                    //    if (eachApprover.Selected)
                    //        selectedApproverCount++;
                    //}

                    //if (selectedApproverCount > 0)
                    //    formValidateSuccess = true;
                    //else
                    //    lstErrorMsg.Add("Select at least One Contract Approver");

                    //Contract Amt
                    if (!String.IsNullOrEmpty(tbxContractAmt.Text))
                    {
                        try
                        {
                            double n;
                            var isNumeric = double.TryParse(tbxContractAmt.Text, out n);
                            if (!isNumeric)
                            {
                                lstErrorMsg.Add("Enter Valid Contract Amount, Only numbers are allowed");
                            }
                        }
                        catch (Exception)
                        {
                            lstErrorMsg.Add("Enter Valid Contract Amount, Only numbers are allowed");
                        }
                    }


                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvContractPopUp);
                    }

                    if (!cvContractPopUp.IsValid)
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUpPage();", true);

                    #endregion

                    #region Save (Add/Edit)

                    if (formValidateSuccess)
                    {
                        long newContractID = 0;

                        Cont_tbl_ContractInstance contractRecord = new Cont_tbl_ContractInstance()
                        {
                            ContractType = 1,

                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            IsDeleted = false,

                            ContractNo = txtContractNo.Text,
                            ContractTitle = txtTitle.Text,
                            ContractDetailDesc = tbxDescription.Text,
                            AddNewClause = tbxAddNewClause.Text,
                            CustomerBranchID = Convert.ToInt32(tvBranches.SelectedValue),
                            DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue),
                            ContractTypeID = Convert.ToInt64(ddlContractType.SelectedValue),
                            CreatedBy = AuthenticationHelper.UserID,
                            UpdatedBy = AuthenticationHelper.UserID,
                            ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue)
                        };

                        if (!string.IsNullOrEmpty(txtProposalDate.Text))
                            contractRecord.ProposalDate = DateTimeExtensions.GetDate(txtProposalDate.Text);

                        if (!string.IsNullOrEmpty(txtAgreementDate.Text))
                            contractRecord.AgreementDate = DateTimeExtensions.GetDate(txtAgreementDate.Text);

                        if (!string.IsNullOrEmpty(txtReviewDate.Text))
                            contractRecord.ReviewDate = DateTimeExtensions.GetDate(txtReviewDate.Text);

                        if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                            contractRecord.EffectiveDate = DateTimeExtensions.GetDate(txtEffectiveDate.Text);

                        if (!string.IsNullOrEmpty(txtExpirationDate.Text))
                            contractRecord.ExpirationDate = DateTimeExtensions.GetDate(txtExpirationDate.Text);

                        if (ddlContractSubType.SelectedValue != "" && ddlContractSubType.SelectedValue != "-1")
                            contractRecord.ContractSubTypeID = Convert.ToInt64(ddlContractSubType.SelectedValue);

                        if (ddlPaymentType.SelectedValue != "" && ddlPaymentType.SelectedValue != "-1")
                            contractRecord.PaymentType = Convert.ToInt32(ddlPaymentType.SelectedValue);

                        if (ddlCPDepartment.SelectedValue != "" && ddlCPDepartment.SelectedValue != "-1")
                            contractRecord.ContactPersonOfDepartment = Convert.ToInt32(ddlCPDepartment.SelectedValue);

                        if (!string.IsNullOrEmpty(txtNoticeTerm.Text))
                        {
                            contractRecord.NoticeTermNumber = Convert.ToInt32(txtNoticeTerm.Text.Trim());

                            if (ddlNoticeTerm.SelectedValue != "" && ddlNoticeTerm.SelectedValue != "-1")
                                contractRecord.NoticeTermType = Convert.ToInt32(ddlNoticeTerm.SelectedValue);
                        }

                        if (!string.IsNullOrEmpty(tbxContractAmt.Text))
                            contractRecord.ContractAmt = Convert.ToDecimal(tbxContractAmt.Text);
                        else
                            contractRecord.ContractAmt = 0;

                        if (ddlPaymentTerm.SelectedValue != "" && ddlPaymentTerm.SelectedValue != "-1")
                            contractRecord.PaymentTermID = Convert.ToInt32(ddlPaymentTerm.SelectedValue);

                        if (!string.IsNullOrEmpty(txtBoxProduct.Text))
                            contractRecord.ProductItems = Convert.ToString(txtBoxProduct.Text.Trim());

                        if (!string.IsNullOrEmpty(tbxAddNewClause.Text))
                            contractRecord.AddNewClause = Convert.ToString(tbxAddNewClause.Text.Trim());

                        #region ADD New Contract

                        if ((int)ViewState["Mode"] == 0)
                        {
                            bool existCaseNo = ContractManagement.ExistsContractNo(customerID, contractRecord.ContractNo, 0);
                            if (!existCaseNo)
                            {
                                //if (!ContractManagement.ExistsContractTitle(customerID, contractRecord.ContractTitle, 0))
                                if(1==1)
                                {
                                    newContractID = ContractManagement.CreateContract(contractRecord);

                                    if (newContractID > 0)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Contract Created", true);
                                        saveSuccess = true;
                                    }
                                }
                                else
                                {
                                    cvContractPopUp.IsValid = false;
                                    cvContractPopUp.ErrorMessage = "Contract with Same Title already Exists.";
                                    cvContractPopUp.CssClass = "alert alert-danger";
                                    return;
                                }
                            }
                            else
                            {
                                cvContractPopUp.IsValid = false;
                                cvContractPopUp.ErrorMessage = "Contract with Same Contract Number already exists";
                                cvContractPopUp.CssClass = "alert alert-danger";
                            }

                            if (saveSuccess)
                            {
                                #region Contract Status Transaction - Draft

                                Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                                {
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                    ContractID = newContractID,
                                    StatusID = Convert.ToInt64(ddlContractStatus.SelectedValue),
                                    StatusChangeOn = DateTime.Now,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                                #endregion

                                #region Vendor Mapping

                                if (lstBoxVendor.Items.Count > 0)
                                {
                                    foreach (ListItem eachVendor in lstBoxVendor.Items)
                                    {
                                        if (eachVendor.Selected)
                                        {
                                            if (Convert.ToInt64(eachVendor.Value) != 0)
                                                lstVendorMapping.Add(Convert.ToInt64(eachVendor.Value));
                                        }
                                    }
                                }

                                if (lstVendorMapping.Count > 0)
                                {
                                    List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                                    lstVendorMapping.ForEach(EachVendor =>
                                    {
                                        Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                        {
                                            ContractID = newContractID,
                                            VendorID = EachVendor,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                        };

                                        lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                    });

                                    saveSuccess = ContractManagement.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                    if (saveSuccess)
                                    {
                                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Mapping Created", true);
                                    }

                                    //Refresh List
                                    lstVendorMapping_ToSave.Clear();
                                    lstVendorMapping_ToSave = null;

                                    lstVendorMapping.Clear();
                                    lstVendorMapping = null;
                                }
                                #endregion

                                #region Save Custom Field

                                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                {
                                    List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                                    //Select Which Grid to Loop based on Selected Category/Type
                                    GridView gridViewToCollectData = null;
                                    gridViewToCollectData = grdCustomField;

                                    if (gridViewToCollectData != null)
                                    {
                                        for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                        {
                                            Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                            TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                            if (lblID != null && tbxLabelValue != null)
                                            {
                                                if (!string.IsNullOrEmpty(lblID.Text) || lblID.Text != "0")
                                                {
                                                    Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                                    {
                                                        ContractID = newContractID,

                                                        LabelID = Convert.ToInt64(lblID.Text),
                                                        LabelValue = tbxLabelValue.Text,

                                                        IsDeleted = false,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,
                                                        UpdatedBy = AuthenticationHelper.UserID,
                                                        UpdatedOn = DateTime.Now
                                                    };
                                                    lstObjParameters.Add(ObjParameter);
                                                    //saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);
                                                }
                                            }
                                        }//End For Each

                                        saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);

                                        if (saveSuccess)
                                        {
                                            ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Updated", true);
                                        }
                                    }
                                }

                                #endregion

                                #region User Assignment                           

                                //De-Active All Previous Assignment
                                ContractManagement.DeActiveContractUserAssignments(newContractID, AuthenticationHelper.UserID);
                                List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();

                                int assignedRoleID = 3;
                                if (lstBoxOwner.Items.Count > 0)
                                {
                                    assignedRoleID = 3;
                                    foreach (ListItem eachOwner in lstBoxOwner.Items)
                                    {
                                        if (eachOwner.Selected)
                                        {
                                            if (Convert.ToInt32(eachOwner.Value) != 0)
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachOwner.Value), assignedRoleID));
                                            //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                        }
                                    }
                                }

                                if (lstBoxApprover.Items.Count > 0)
                                {
                                    assignedRoleID = 6;
                                    foreach (ListItem eachApprover in lstBoxApprover.Items)
                                    {
                                        if (eachApprover.Selected)
                                        {
                                            if (Convert.ToInt32(eachApprover.Value) != 0)
                                                lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                            // lstApproverMapping.Add(Convert.ToInt32(eachApprover.Value));
                                        }
                                    }
                                }

                                if (lstContractUserMapping.Count > 0)
                                {
                                    lstContractUserMapping.ForEach(eachUser =>
                                    {
                                        Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                        {
                                            AssignmentType = 1,
                                            ContractID = newContractID,

                                            UserID = Convert.ToInt32(eachUser.Item1),
                                            RoleID = Convert.ToInt32(eachUser.Item2),

                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                        };

                                        lstUserAssignmentRecord_ToSave.Add(newAssignment);

                                        //saveSuccess = ContractManagement.CreateUpdateContractUserAssignment(newAssignment);
                                    });
                                }

                                if (lstUserAssignmentRecord_ToSave.Count > 0)
                                    saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                if (saveSuccess)
                                {
                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created", true);
                                }

                                lstUserAssignmentRecord_ToSave.Clear();
                                lstUserAssignmentRecord_ToSave = null;



                                #endregion
                            }

                            if (saveSuccess)
                            {
                                cvContractPopUp.IsValid = false;
                                cvContractPopUp.ErrorMessage = "Contract Created Sucessfully";
                                VSContractPopup.CssClass = "alert alert-success";

                                ViewState["Mode"] = 1;
                                ViewState["ContractInstanceID"] = newContractID;

                                toggleTextSaveButton_ContractSummaryTab(false); //false -- Switch to Edit Mode

                                showHideTabs(true);
                                showHideContractSummaryTabTopButtons(true);
                                enableDisableContractSummaryTabControls(false);

                                //Bind Case To Link
                                BindContractListToLink(newContractID);
                            }

                            if (saveSuccess)
                            {
                                #region Sent Email to Assigned Users

                                string emailTemplate = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_Assignment;

                                if (!string.IsNullOrEmpty(emailTemplate))
                                {
                                    string accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                                    ContractManagement.SendMailtoAssignedUsers(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, contractRecord, lstContractUserMapping, emailTemplate, accessURL);
                                }

                                #endregion
                            }

                            //lstApproverMapping.Clear();
                            //lstApproverMapping = null;

                            //lstOwnerMapping.Clear();
                            //lstOwnerMapping = null;
                        }

                        #endregion

                        #region Edit Contract

                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                newContractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                                if (newContractID != 0)
                                {
                                    contractRecord.ID = newContractID;

                                    bool existContractNo = ContractManagement.ExistsContractNo(customerID, contractRecord.ContractNo, newContractID);
                                    if (!existContractNo)
                                    {
                                        //if (!ContractManagement.ExistsContractTitle(customerID, contractRecord.ContractTitle, newContractID))
                                        if(1==1)
                                        {
                                            saveSuccess = ContractManagement.UpdateContract(contractRecord);

                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Detail(s) Updated", true);
                                            }
                                        }
                                        else
                                        {
                                            cvContractPopUp.IsValid = false;
                                            cvContractPopUp.ErrorMessage = "Contract with Same Title already Exists.";
                                            cvContractPopUp.CssClass = "alert alert-danger";
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        cvContractPopUp.IsValid = false;
                                        cvContractPopUp.ErrorMessage = "Contract with Same Contract Number already exists";
                                        cvContractPopUp.CssClass = "alert alert-danger";
                                    }

                                    if (saveSuccess)
                                    {
                                        bool matchSuccess = false;

                                        #region Contract Status Transaction

                                        Save_ContractStatus(newContractID);

                                        #endregion

                                        #region Vendor Mapping

                                        if (lstBoxVendor.Items.Count > 0)
                                        {
                                            foreach (ListItem eachVendor in lstBoxVendor.Items)
                                            {
                                                if (eachVendor.Selected)
                                                {
                                                    if (Convert.ToInt64(eachVendor.Value) != 0)
                                                        lstVendorMapping.Add(Convert.ToInt64(eachVendor.Value));
                                                }
                                            }
                                        }

                                        if (lstVendorMapping.Count > 0)
                                        {
                                            List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                                            lstVendorMapping.ForEach(EachVendor =>
                                            {
                                                Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                                {
                                                    ContractID = newContractID,
                                                    VendorID = EachVendor,
                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    CreatedOn = DateTime.Now,
                                                };

                                                lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                                            });

                                            var existingVendorMapping = ContractManagement.GetVendorMapping(newContractID);
                                            var assignedVendorIDs = lstVendorMapping_ToSave.Select(row => row.VendorID).ToList();

                                            if (existingVendorMapping.Count != assignedVendorIDs.Count)
                                            {
                                                matchSuccess = false;
                                            }
                                            else
                                            {
                                                matchSuccess = existingVendorMapping.Except(assignedVendorIDs).ToList().Count > 0 ? false : true;
                                            }

                                            if (!matchSuccess)
                                            {
                                                ContractManagement.DeActiveExistingVendorMapping(newContractID);

                                                saveSuccess = ContractManagement.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                                                if (saveSuccess)
                                                {
                                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Updated", true);
                                                }
                                            }
                                            else
                                                saveSuccess = false;

                                            //Refresh List
                                            lstVendorMapping_ToSave.Clear();
                                            lstVendorMapping_ToSave = null;

                                            lstVendorMapping.Clear();
                                            lstVendorMapping = null;
                                        }

                                        #endregion

                                        #region Update Custom Field                                        

                                        if (!string.IsNullOrEmpty(Convert.ToString(ViewState["CustomefieldCount"])))
                                        {
                                            List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                                            //Select Which Grid to Loop based on Selected Category/Type
                                            GridView gridViewToCollectData = null;
                                            gridViewToCollectData = grdCustomField;

                                            if (gridViewToCollectData != null)
                                            {
                                                for (int i = 0; i < gridViewToCollectData.Rows.Count; i++)
                                                {
                                                    Label lblID = (Label)gridViewToCollectData.Rows[i].FindControl("lblID");
                                                    TextBox tbxLabelValue = (TextBox)gridViewToCollectData.Rows[i].FindControl("tbxLabelValue");

                                                    if (lblID != null && tbxLabelValue != null)
                                                    {
                                                        if (!string.IsNullOrEmpty(lblID.Text) || lblID.Text != "0")
                                                        {
                                                            Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                                            {
                                                                ContractID = newContractID,

                                                                LabelID = Convert.ToInt64(lblID.Text),
                                                                LabelValue = tbxLabelValue.Text,

                                                                IsDeleted = false,
                                                                CreatedBy = AuthenticationHelper.UserID,
                                                                CreatedOn = DateTime.Now,
                                                                UpdatedBy = AuthenticationHelper.UserID,
                                                                UpdatedOn = DateTime.Now
                                                            };

                                                            lstObjParameters.Add(ObjParameter);

                                                            //saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);
                                                        }
                                                    }
                                                }//End For Each

                                                var existingCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), newContractID, Convert.ToInt64(ddlContractType.SelectedValue));

                                                var lstSavedCustomFields = existingCustomFields.Select(row => row.LableID).ToList();
                                                var assignedCustomFields = lstObjParameters.Select(row => row.LabelID).ToList();

                                                matchSuccess = false;

                                                if (lstSavedCustomFields.Count != assignedCustomFields.Count)
                                                {
                                                    matchSuccess = false;
                                                }
                                                else
                                                {
                                                    matchSuccess = lstSavedCustomFields.Except(assignedCustomFields).ToList().Count > 0 ? false : true;
                                                }

                                                if (!matchSuccess)
                                                {
                                                    saveSuccess = ContractManagement.DeActiveExistingCustomsFieldsByContractID(newContractID);
                                                    saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);
                                                }
                                                else
                                                    saveSuccess = false;

                                                if (saveSuccess)
                                                {
                                                    ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Updated", true);
                                                }
                                            }
                                        }

                                        #endregion

                                        #region User Assignment                           

                                        List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();
                                        lstContractUserMapping.Clear();

                                        int assignedRoleID = 3;
                                        if (lstBoxOwner.Items.Count > 0)
                                        {
                                            assignedRoleID = 3;
                                            foreach (ListItem eachOwner in lstBoxOwner.Items)
                                            {
                                                if (eachOwner.Selected)
                                                {
                                                    if (Convert.ToInt32(eachOwner.Value) != 0)
                                                        lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachOwner.Value), assignedRoleID));
                                                    //lstOwnerMapping.Add(Convert.ToInt32(eachOwner.Value));
                                                }
                                            }
                                        }

                                        if (lstBoxApprover.Items.Count > 0)
                                        {
                                            assignedRoleID = 6;
                                            foreach (ListItem eachApprover in lstBoxApprover.Items)
                                            {
                                                if (eachApprover.Selected)
                                                {
                                                    if (Convert.ToInt32(eachApprover.Value) != 0)
                                                        lstContractUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                                    // lstApproverMapping.Add(Convert.ToInt32(eachApprover.Value));
                                                }
                                            }
                                        }

                                        if (lstContractUserMapping.Count > 0)
                                        {
                                            lstContractUserMapping.ForEach(eachUser =>
                                            {
                                                Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                                {
                                                    AssignmentType = 1,
                                                    ContractID = newContractID,

                                                    UserID = Convert.ToInt32(eachUser.Item1),
                                                    RoleID = Convert.ToInt32(eachUser.Item2),

                                                    IsActive = true,
                                                    CreatedBy = AuthenticationHelper.UserID,
                                                    UpdatedBy = AuthenticationHelper.UserID,
                                                };

                                                lstUserAssignmentRecord_ToSave.Add(newAssignment);

                                                //saveSuccess = ContractManagement.CreateUpdateContractUserAssignment(newAssignment);
                                            });
                                        }

                                        var existingUserAssignment = ContractManagement.GetContractUserAssignment_All(newContractID);

                                        var assignedUserIDs = existingUserAssignment.Select(row => row.UserID).ToList();
                                        var currentUserIDs = lstUserAssignmentRecord_ToSave.Select(row => row.UserID).ToList();

                                        matchSuccess = false;

                                        if (assignedUserIDs.Count != currentUserIDs.Count)
                                        {
                                            matchSuccess = false;
                                        }
                                        else
                                        {
                                            matchSuccess = assignedUserIDs.Except(currentUserIDs).ToList().Count > 0 ? false : true;
                                        }

                                        if (!matchSuccess)
                                        {
                                            //De-Active All Previous Assignment
                                            ContractManagement.DeActiveContractUserAssignments(newContractID, AuthenticationHelper.UserID);

                                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                                saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Updated", true);
                                            }
                                        }
                                        else
                                            saveSuccess = true;

                                        lstUserAssignmentRecord_ToSave.Clear();
                                        lstUserAssignmentRecord_ToSave = null;

                                        #endregion
                                    }

                                    if (saveSuccess)
                                    {
                                        cvContractPopUp.IsValid = false;
                                        cvContractPopUp.ErrorMessage = "Contract Detail(s) Updated Sucessfully";
                                        VSContractPopup.CssClass = "alert alert-success";

                                        ViewState["Mode"] = 1;
                                        ViewState["ContractInstanceID"] = newContractID;

                                        toggleTextSaveButton_ContractSummaryTab(false); //false -- Switch to Edit Mode

                                        showHideTabs(true);
                                        showHideContractSummaryTabTopButtons(true);
                                        enableDisableContractSummaryTabControls(false);
                                    }
                                }
                            }
                        }

                        #endregion

                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractPopUp.IsValid = false;
                cvContractPopUp.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        #endregion

        #region Custom Field/Parameter
        private void intializeDataTableCustomField(GridView gridViewCustomField)
        {
            try
            {
                DataTable dtCustomField = new DataTable();

                DataRow drowCustomField = null;

                dtCustomField.Columns.Add(new DataColumn("LableID", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("Label", typeof(string)));
                dtCustomField.Columns.Add(new DataColumn("LabelValue", typeof(string)));

                drowCustomField = dtCustomField.NewRow();

                drowCustomField["LableID"] = string.Empty;
                drowCustomField["Label"] = string.Empty;
                drowCustomField["LabelValue"] = string.Empty;

                dtCustomField.Rows.Add(drowCustomField);

                ViewState["dataTableCustomFields"] = dtCustomField;

                gridViewCustomField.Visible = true;
                //gridViewCustomField_History.Visible = false;

                gridViewCustomField.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                gridViewCustomField.DataBind();

                //To Hide row
                gridViewCustomField.Rows[0].Visible = false;
                gridViewCustomField.Rows[0].Controls.Clear();

                //lblAddNewGround.Visible = true;               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFields(GridView gridViewCustomField) //, GridView gridViewCustomField_History
        {
            try
            {
                long contractInstanceID = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["ContractInstanceID"])))
                {
                    contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                }
                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "0")  //&& CaseInstanceID != 0
                {
                    List<Cont_SP_GetCustomFieldsValues_Result> lstCustomFields = new List<Cont_SP_GetCustomFieldsValues_Result>();

                    if (contractInstanceID != 0)
                    {
                        lstCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));

                        bool historyFlag = false;
                        if (ViewState["FlagHistory"] != null)
                        {
                            historyFlag = Convert.ToBoolean(Convert.ToInt32(ViewState["FlagHistory"]));
                        }

                        if (lstCustomFields != null && lstCustomFields.Count > 0)
                        {
                            ViewState["CustomefieldCount"] = lstCustomFields.Count;

                            gridViewCustomField.DataSource = lstCustomFields;
                            gridViewCustomField.DataBind();
                            gridViewCustomField.Visible = true;

                            if (!historyFlag)
                            {

                            }
                            else if (historyFlag)
                            {

                            }
                        }
                        else
                        {
                            Cont_SP_GetCustomFieldsValues_Result obj = new Cont_SP_GetCustomFieldsValues_Result(); //initialize empty class that may contain properties
                            lstCustomFields.Add(obj); //Add empty object to list

                            gridViewCustomField.DataSource = lstCustomFields; /*Assign datasource to create one row with default values for the class you have*//*Assign datasource to create one row with default values for the class you have*/
                            gridViewCustomField.DataBind(); //Bind that empty source     

                            //To Hide row
                            gridViewCustomField.Rows[0].Visible = false;
                            gridViewCustomField.Rows[0].Controls.Clear();

                            gridViewCustomField.Visible = true;

                            if (!historyFlag)
                            {

                            }
                            else if (historyFlag)
                            {

                            }
                        }
                    }
                    else
                    {
                        intializeDataTableCustomField(grdCustomField); //, grdCustomField_History
                    }

                    lblCustomField.Visible = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomFieldDropDown(DropDownList ddlCustomField, GridView gridViewCustomField)
        {
            try
            {
                if (ddlContractType.SelectedValue != null && ddlContractType.SelectedValue != "" && ddlContractType.SelectedValue != "0")
                {
                    var customFields = ContractManagement.GetCustomsFieldsByContractType(Convert.ToInt32(AuthenticationHelper.CustomerID), Convert.ToInt64(ddlContractType.SelectedValue));

                    if (customFields.Count > 0)
                    {
                        //lblAddNewGround.Visible = true;
                        gridViewCustomField.Visible = true;

                        if (ddlCustomField.Items.Count > 0)
                            ddlCustomField.Items.Clear();

                        ddlCustomField.DataTextField = "Label";
                        ddlCustomField.DataValueField = "ID";

                        ddlCustomField.DataSource = customFields;
                        ddlCustomField.DataBind();

                        //ddlCustomField.Items.Add(new ListItem("Add New", "0"));

                        ViewState["ddlCustomFieldFilled"] = "1";
                    }
                    else
                    {
                        //lblAddNewGround.Visible = false;
                        gridViewCustomField.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        TextBox tbxLabelValue = (TextBox)e.Row.FindControl("tbxLabelValue");

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            tbxLabelValue.Enabled = false;
                        }

                        //Hide Delete in Case of Total or Row with 0 ID
                        Label lblID = (Label)e.Row.FindControl("lblID");
                        LinkButton lnkBtnDeleteCustomField = (LinkButton)e.Row.FindControl("lnkBtnDeleteCustomField");

                        if (lblID != null && lnkBtnDeleteCustomField != null)
                        {
                            if (lblID.Text != "" && lblID.Text != "0")
                            {
                                e.Row.Enabled = true;
                                lnkBtnDeleteCustomField.Visible = true;
                            }
                            else
                            {
                                e.Row.Enabled = false;
                                lnkBtnDeleteCustomField.Visible = false;
                            }
                        }
                    }

                    if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        DropDownList ddlFieldName_Footer = (DropDownList)e.Row.FindControl("ddlFieldName_Footer");

                        if (ddlFieldName_Footer != null)
                        {
                            BindCustomFieldDropDown(ddlFieldName_Footer, gridView);

                            foreach (GridViewRow gvr in gridView.Rows)
                            {
                                Label lblID = (Label)gvr.FindControl("lblID");

                                if (lblID != null)
                                {
                                    if (lblID.Text != "")
                                    {
                                        if (ddlFieldName_Footer.Items.FindByValue(lblID.Text) != null)
                                            ddlFieldName_Footer.Items.Remove(ddlFieldName_Footer.Items.FindByValue(lblID.Text));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                GridView gridView = (GridView)sender;

                if (gridView != null)
                {
                    gridView.PageIndex = e.NewPageIndex;
                    BindCustomFields(grdCustomField); //, grdCustomField_History
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCustomField_Common_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                long contractInstanceID = 0;
                bool deleteSuccess = false;

                if (ViewState["ContractInstanceID"] != null && e.CommandName.Equals("DeleteCustomField") && e.CommandArgument != null && ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0 && ViewState["dataTableCustomFields"] != null)
                    {
                        GridViewRow gvRow = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
                        if (gvRow != null)
                        {
                            //GridViewRow gvRow = (GridViewRow)(sender).Parent.Parent;
                            int index = gvRow.RowIndex;

                            DataTable dtCustomField = ViewState["dataTableCustomFields"] as DataTable;
                            dtCustomField.Rows[index].Delete();

                            ViewState["dataTableCustomFields"] = dtCustomField;
                            ViewState["CustomefieldCount"] = dtCustomField.Rows.Count;

                            GridView gridView = (GridView)sender;

                            if (gridView != null)
                            {
                                gridView.DataSource = dtCustomField; /*Assign datasource to create one row with default values for the class you have*/
                                gridView.DataBind();
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        long LableID = Convert.ToInt64(e.CommandArgument);

                        contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        if (contractInstanceID != 0 && LableID != 0)
                        {
                            deleteSuccess = ContractManagement.DeleteCustomFieldsContractWise(contractInstanceID, LableID);
                            if (deleteSuccess)
                            {
                                LitigationManagement.CreateAuditLog("C", contractInstanceID, "Cont_tbl_CustomFieldValue", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Field Deleted", true);

                                BindCustomFields(grdCustomField); //, grdCustomField_History
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnAddCustomField_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    long lblID = 0;

                    DropDownList ddlFieldName_Footer = (DropDownList)grdCustomField.FooterRow.FindControl("ddlFieldName_Footer");
                    TextBox txtFieldValue_Footer = (TextBox)grdCustomField.FooterRow.FindControl("txtFieldValue_Footer");

                    if (ddlFieldName_Footer != null && txtFieldValue_Footer != null)
                    {
                        if ((int)ViewState["Mode"] == 0)
                        {
                            string lblName = ddlFieldName_Footer.SelectedItem.Text;
                            lblID = Convert.ToInt64(ddlFieldName_Footer.SelectedValue);

                            if (ViewState["dataTableCustomFields"] != null && lblID != 0)
                            {
                                DataTable dtcurrentTableCumtomFields = (DataTable)ViewState["dataTableCustomFields"];

                                DataRow drNewRow = dtcurrentTableCumtomFields.NewRow();

                                drNewRow["LableID"] = lblID;
                                drNewRow["Label"] = lblName;
                                drNewRow["LabelValue"] = txtFieldValue_Footer.Text;

                                //add new row to DataTable
                                dtcurrentTableCumtomFields.Rows.Add(drNewRow);

                                //Delete Rows with blank LblID (if Any)
                                dtcurrentTableCumtomFields = LitigationManagement.LoopAndDeleteBlankRows(dtcurrentTableCumtomFields);

                                ViewState["CustomefieldCount"] = dtcurrentTableCumtomFields.Rows.Count;

                                //Store the current data to ViewState
                                ViewState["CurrentTablePutCallDtls"] = dtcurrentTableCumtomFields;


                                //Rebind the Grid with the current data
                                grdCustomField.DataSource = dtcurrentTableCumtomFields;
                                grdCustomField.DataBind();

                            }
                        }//Add Mode End
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                                if (txtFieldValue_Footer != null && ddlFieldName_Footer != null && contractInstanceID != 0)
                                {
                                    bool validateData = false;
                                    bool saveSuccess = false;

                                    if (!String.IsNullOrEmpty(ddlFieldName_Footer.SelectedValue) && ddlFieldName_Footer.SelectedValue != "0")
                                    {
                                        if (txtFieldValue_Footer.Text != "")
                                        {
                                            validateData = true;
                                        }
                                    }

                                    if (validateData)
                                    {
                                        lblID = Convert.ToInt64(ddlFieldName_Footer.SelectedValue);

                                        if (lblID != 0)
                                        {
                                            Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                            {
                                                ContractID = contractInstanceID,
                                                LabelID = lblID,
                                                LabelValue = txtFieldValue_Footer.Text,
                                                IsDeleted = false,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                            };

                                            saveSuccess = ContractManagement.CreateUpdateCustomsField(ObjParameter);

                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", contractInstanceID, "Cont_tbl_CustomFieldValue", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Custom Field Added", true);

                                                if (ViewState["ContractTypeIDUpdated"] != null)
                                                {
                                                    if (ViewState["ContractTypeIDUpdated"].ToString() == "false")
                                                    {
                                                        saveSuccess = ContractManagement.UpdateContractTypeID(contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));
                                                        if (saveSuccess)
                                                        {
                                                            saveSuccess = ContractManagement.DeletePreviousCustomFields(contractInstanceID, Convert.ToInt64(ddlContractType.SelectedValue));

                                                            if (saveSuccess)
                                                                ViewState["ContractTypeIDUpdated"] = "true";
                                                        }
                                                    }
                                                }

                                                BindCustomFields(grdCustomField); //, grdCustomField_History

                                            }
                                        }
                                    }
                                }
                            }
                        }//Edit Mode End
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void TextChangedInsideGridView_TextChanged(object sender, EventArgs e)
        {
            GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
            if (currentRow != null)
            {
                if (currentRow.RowType == DataControlRowType.DataRow)
                {
                    TextBox tbxLabelValue = (TextBox)currentRow.FindControl("tbxLabelValue");
                    TextBox tbxInterestValue = (TextBox)currentRow.FindControl("tbxInterestValue");
                    TextBox tbxPenaltyValue = (TextBox)currentRow.FindControl("tbxPenaltyValue");
                    TextBox tbxRowTotalValue = (TextBox)currentRow.FindControl("tbxRowTotalValue");

                    if (tbxLabelValue != null && tbxInterestValue != null && tbxPenaltyValue != null && tbxRowTotalValue != null)
                    {
                        tbxRowTotalValue.Text = (LitigationManagement.csvToNumber(tbxLabelValue.Text) +
                            LitigationManagement.csvToNumber(tbxInterestValue.Text) +
                            LitigationManagement.csvToNumber(tbxPenaltyValue.Text)).ToString("N2");
                    }
                }
                else if (currentRow.RowType == DataControlRowType.Footer)
                {
                    TextBox txtFieldValue_Footer = (TextBox)currentRow.FindControl("txtFieldValue_Footer");
                    TextBox txtInterestValue_Footer = (TextBox)currentRow.FindControl("txtInterestValue_Footer");
                    TextBox txtPenaltyValue_Footer = (TextBox)currentRow.FindControl("txtPenaltyValue_Footer");
                    TextBox tbxRowTotalValue_Footer = (TextBox)currentRow.FindControl("tbxRowTotalValue_Footer");

                    if (txtFieldValue_Footer != null && txtInterestValue_Footer != null && txtPenaltyValue_Footer != null && tbxRowTotalValue_Footer != null)
                    {
                        tbxRowTotalValue_Footer.Text = (LitigationManagement.csvToNumber(txtFieldValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtInterestValue_Footer.Text) +
                            LitigationManagement.csvToNumber(txtPenaltyValue_Footer.Text)).ToString("N2");
                    }
                }
            }
        }

        protected void grdCustomField_History_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblIsAllowed = (Label)e.Row.FindControl("lblIsAllowed");
                    DropDownList ddlGroundResult = (DropDownList)e.Row.FindControl("ddlGroundResult");

                    if (ddlGroundResult != null && lblIsAllowed != null)
                    {
                        if (lblIsAllowed.Text != null && lblIsAllowed.Text != "")
                        {
                            string isAllowed = "0";

                            if (lblIsAllowed.Text == "True")
                                isAllowed = "1";
                            else if (lblIsAllowed.Text == "False")
                                isAllowed = "0";

                            ddlGroundResult.ClearSelection();

                            if (ddlGroundResult.Items.FindByValue(isAllowed) != null)
                                ddlGroundResult.Items.FindByValue(isAllowed).Selected = true;
                        }
                    }

                    Label lblPenalty = (Label)e.Row.FindControl("lblPenalty");

                    if (lblPenalty != null)
                    {
                        if (string.IsNullOrEmpty(lblPenalty.Text))
                            lblPenalty.Visible = false;
                    }

                    Label lblInterest = (Label)e.Row.FindControl("lblInterest");

                    if (lblInterest != null)
                    {
                        if (string.IsNullOrEmpty(lblInterest.Text))
                            lblInterest.Visible = false;
                    }

                    Label lblSettlementValue = (Label)e.Row.FindControl("lblSettlementValue");

                    if (lblSettlementValue != null)
                    {
                        if (string.IsNullOrEmpty(lblSettlementValue.Text))
                            lblSettlementValue.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Contract - Linking

        private void BindLinkedContracts(long contractID)
        {
            if (contractID != 0)
            {
                var lstLinkedContracts = ContractManagement.GetLinkedContractList_All(contractID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                grdLinkedContracts.DataSource = lstLinkedContracts;
                grdLinkedContracts.DataBind();

                if (lstLinkedContracts.Count > 0)
                    divLinkedContracts.Visible = true;
                else
                    divLinkedContracts.Visible = false;

                lstLinkedContracts.Clear();
                lstLinkedContracts = null;
                upLinkedContracts.Update();
            }
        }

        private void BindContractListToLink(long contactID)
        {

            if (contactID != 0)
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;
                List<int> branchList = new List<int>();
                var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    3, branchList, vendorID, deptID, contractStatusID, contractTypeID);

                if (lstAssignedContracts.Count > 0)
                    lstAssignedContracts = lstAssignedContracts.Where(row => row.ID != contactID).ToList();

                var lstAlreadyLinkedContracts = ContractManagement.GetLinkedContractIDs(contactID, Convert.ToInt32(customerID));

                if (lstAlreadyLinkedContracts.Count > 0)
                    lstAssignedContracts = lstAssignedContracts.Where(row => !lstAlreadyLinkedContracts.Contains(row.ID)).ToList();

                grdContractList_LinkContract.DataSource = lstAssignedContracts;
                grdContractList_LinkContract.DataBind();

                lstAssignedContracts.Clear();
                lstAssignedContracts = null;
            }


        }
        protected void grdLinkedContracts_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                bool deleteSuccess = false;
                if (e.CommandArgument != null && ViewState["ContractInstanceID"] != null)
                {
                    if (e.CommandName.Equals("ViewLinkedContract"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long contractID = Convert.ToInt64(commandArgs[0]);
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptHistoryPopUp", "OpenContractHistoryPopup(" + contractID + "," + " true" + ",'L'" + ");", true);
                    }
                    else if (e.CommandName.Equals("DeleteContractLinking"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long contractID = Convert.ToInt64(commandArgs[0]);
                        long LinkedContractID = Convert.ToInt64(commandArgs[1]);

                        if (contractID != 0 && LinkedContractID != 0)
                        {
                            deleteSuccess = ContractManagement.DeleteContractLinking(contractID, LinkedContractID, AuthenticationHelper.UserID);

                            if (deleteSuccess)
                            {
                                BindLinkedContracts(contractID);
                                ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Linking Deleted", true);

                                BindContractListToLink(contractID);

                                cvLinkedContracts.IsValid = false;
                                cvLinkedContracts.ErrorMessage = "Contract Linking Deleted Successfully";
                                vsLinkedContracts.CssClass = "alert alert-success";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdLinkedContracts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    LinkButton lnkBtnDeleteContractLinking = (LinkButton)e.Row.FindControl("lnkBtnDeleteContractLinking");
                    if (lnkBtnDeleteContractLinking != null)
                    {
                        if (ViewState["ContractStatusID"] != null)
                        {
                            if (Convert.ToInt32(ViewState["ContractStatusID"]) == 9) //9-Renewed
                                lnkBtnDeleteContractLinking.Visible = false;
                            else
                                lnkBtnDeleteContractLinking.Visible = true;
                        }

                        if (!string.IsNullOrEmpty(Request.QueryString["HistoryFlag"]))
                        {
                            lnkBtnDeleteContractLinking.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSaveLinkContract_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["PageLink"].Equals("ContractLink"))
                {
                    bool saveSuccess = false;
                    List<long> lstContractsToLink = new List<long>();
                    int totalRecordSaveCount = 0;

                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        for (int i = 0; i < grdContractList_LinkContract.Rows.Count; i++)
                        {
                            CheckBox chkRowLinkContracts = (CheckBox)grdContractList_LinkContract.Rows[i].FindControl("chkRowLinkContracts");

                            if (chkRowLinkContracts != null)
                            {
                                if (chkRowLinkContracts.Checked)
                                {
                                    Label lblContractIID = (Label)grdContractList_LinkContract.Rows[i].FindControl("lblContractIID");

                                    if (lblContractIID != null)
                                    {
                                        if (lblContractIID.Text != "")
                                            lstContractsToLink.Add(Convert.ToInt64(lblContractIID.Text));
                                    }
                                }
                            }
                        }

                        if (lstContractsToLink.Count > 0)
                        {
                            lstContractsToLink.ForEach(eachContractToLink =>
                            {
                                Cont_tbl_ContractLinking newRecord = new Cont_tbl_ContractLinking()
                                {
                                    ContractID = contractID,
                                    LinkedContractID = eachContractToLink,
                                    IsActive = true,
                                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),

                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,

                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now,
                                };

                                saveSuccess = ContractManagement.CreateUpdateContractLinking(newRecord);


                                if (saveSuccess)
                                    totalRecordSaveCount++;
                            });
                        }

                        if (saveSuccess)
                        {
                            BindLinkedContracts(contractID);
                            BindContractListToLink(contractID);
                            BindFileTags();
                            ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, totalRecordSaveCount + " Contract(s) Linked", true);

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptUnCheckAllGridCheckBox", "unCheckAll('<%=grdContractList_LinkContract.ClientID %>');", true);

                            cvLinkContract.IsValid = false;
                            cvLinkContract.ErrorMessage = totalRecordSaveCount + "\t \t Contract(s) Linked Successfully";
                            vsLinkContract.CssClass = "alert alert-success";
                        }
                    }

                }
                else
                {
                    bool saveSuccess = false;
                    List<long> lstContractsToLink = new List<long>();
                    int totalRecordSaveCount = 0;

                    if (ViewState["ContractInstanceID"] != null)
                    {
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                        for (int i = 0; i < grdContractList_LinkContract.Rows.Count; i++)
                        {
                            CheckBox chkRowLinkContracts = (CheckBox)grdContractList_LinkContract.Rows[i].FindControl("chkRowLinkContracts");

                            if (chkRowLinkContracts != null)
                            {
                                if (chkRowLinkContracts.Checked)
                                {
                                    Label lblContractIID = (Label)grdContractList_LinkContract.Rows[i].FindControl("lblContractIID");

                                    if (lblContractIID != null)
                                    {
                                        if (lblContractIID.Text != "")
                                            lstContractsToLink.Add(Convert.ToInt64(lblContractIID.Text));
                                    }
                                }
                            }
                        }


                        if (lstContractsToLink.Count > 0)
                        {
                            lstContractsToLink.ForEach(eachContractToLink =>
                            {
                                if (ViewState["ContractInstanceID"] != null)
                                {
                                    if (!string.IsNullOrEmpty(ViewState["ContractInstanceID"].ToString()))
                                    {
                                        
                                        int ContractInstanceID = Convert.ToInt32(ViewState["ContractInstanceID"]);

                                        if (ContractInstanceID > 0)
                                        {
                                            List<Cont_tbl_FileData> objFileData = new List<Cont_tbl_FileData>();
                                            using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                            {
                                                objFileData = (from row in entities.Cont_tbl_FileData
                                                               where row.IsDeleted == false
                                                               && row.ContractID == eachContractToLink
                                                               select row).ToList();

                                                #region Upload Document
                                                foreach (var item in objFileData)
                                                {
                                                    Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                                                    {
                                                        ContractID = ContractInstanceID,
                                                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                                                        DocTypeID = item.DocTypeID,
                                                        FileName = item.FileName,
                                                        FilePath = item.FilePath,
                                                        // FileTags = item.FileTags,
                                                        FileKey = item.FileKey,
                                                        Version = item.Version,
                                                        VersionDate = item.VersionDate,
                                                        CreatedBy = AuthenticationHelper.UserID,
                                                        CreatedOn = DateTime.Now,


                                                    };
                                                    entities.Cont_tbl_FileData.Add(objContDoc);
                                                    entities.SaveChanges();

                                                    List<Cont_tbl_FileDataTagsMapping> objTag = new List<Cont_tbl_FileDataTagsMapping>();
                                                    objTag = (from row in entities.Cont_tbl_FileDataTagsMapping
                                                              where row.FileID == item.ID
                                                              && row.IsActive == true
                                                              select row).ToList();

                                                    foreach (var Tagitem in objTag)
                                                    {
                                                        Cont_tbl_FileDataTagsMapping ObjFileTag = new Cont_tbl_FileDataTagsMapping
                                                        {
                                                        FileID=objContDoc.ID,
                                                        FileTag= Tagitem.FileTag,
                                                        IsActive=true,
                                                        CreatedBy=AuthenticationHelper.UserID,
                                                        CreatedOn=DateTime.Now
                                                        };
                                                        entities.Cont_tbl_FileDataTagsMapping.Add(ObjFileTag);
                                                        entities.SaveChanges();
                                                    }
                                                }
                                            }
                                            saveSuccess = true;
                                            if (saveSuccess)
                                            {
                                                ContractManagement.CreateAuditLog("C", ContractInstanceID, "Cont_tbl_FileData", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document(s) Uploaded", true);
                                                // BindContractDocuments_Paging();
                                            }

                                           /* if (saveSuccess)
                                            {
                                                cvContractDocument.IsValid = false;
                                                cvContractDocument.ErrorMessage = "Document(s) uploaded successfully";
                                                vsContractDocument.CssClass = "alert alert-success";
                                            }
                                            else
                                            {
                                                cvContractDocument.IsValid = false;
                                                cvContractDocument.ErrorMessage = "Select Document(s) to Upload";
                                            }
                                            */
                                           
                                            #endregion
                                        }
                                    }
                                    //saveSuccess = ContractManagement.CreateUpdateContractLinking(newRecord);
                                    if (saveSuccess)
                                        totalRecordSaveCount++;
                                }

                            });
                        }
                        if (saveSuccess)
                        {
                           
                            BindContractDocuments_All();
                            BindFileTags();

                            //ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractLinking", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, totalRecordSaveCount + " Contract(s) Linked", true);

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptUnCheckAllGridCheckBox", "unCheckAll('<%=grdContractList_LinkContract.ClientID %>');", true);

                            cvLinkContract.IsValid = false;
                            cvLinkContract.ErrorMessage = totalRecordSaveCount + "\t \t Document(s) Linked Successfully";
                            vsLinkContract.CssClass = "alert alert-success";
                        }
                    }
                   
                }
               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        #endregion

        #region Contract-Document
        protected void lnkBtn_RebindContractDoc_Click(object sender, EventArgs e)
        {
            BindFileTags();
            BindContractDocuments_Paging();
        }

        public void BindContractDocuments_Paging()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    long ContractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    int PageNumber = 1;
                    if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue) && DropDownListPageNo.SelectedValue != "0")
                    {
                        PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                    }

                    List<Cont_SP_GetContractDocuments_Paging_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_Paging_Result>();

                    lstContDocs = ContractDocumentManagement.GetContractDocuments_Paging(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID, Convert.ToInt32(grdContractDocuments.PageSize), PageNumber);

                    if (lstContDocs.Count > 0)
                    {
                        lstContDocs = lstContDocs.OrderBy(row => row.FileName).ThenByDescending(row => row.VersionDate).ToList();
                    }

                    grdContractDocuments.DataSource = lstContDocs;
                    grdContractDocuments.DataBind();

                    int Totalcnt = 0;
                    if (lstContDocs.Count > 0)
                    {
                        BindFileTags();

                        foreach (var item in lstContDocs)
                        {
                            Totalcnt = Convert.ToInt32(item.TotalRowCount);
                            break;
                        }
                    }

                    Session["TotalRows"] = Totalcnt;
                    //lblStartRecord.Text = DropDownListPageNo.SelectedValue;
                    if (Convert.ToInt32(ViewState["PageNumberFlagID"]) == 0)
                    {
                        bindPageNumber(1);
                    }

                    upContractDocUploadPopup.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDocument.IsValid = false;
                cvContractDocument.ErrorMessage = "Server Error Occurred. Please try again.";
                cvContractDocument.CssClass = "alert alert-danger";
            }
        }
        protected void lnkBtn_RebindContractTaskDoc_Click(object sender, EventArgs e)
        {
            BindContractDocuments_All(grdTaskContractDocuments);
        }
        public void BindContractDocuments_All(GridView grdtoBindDocuments)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    long ContractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    List<Cont_SP_GetContractDocuments_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_All_Result>();

                    lstContDocs = ContractDocumentManagement.GetContractDocuments_All(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID);

                    grdtoBindDocuments.DataSource = lstContDocs;
                    grdtoBindDocuments.DataBind();
                    lstContDocs.Clear();
                    lstContDocs = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindContractDocuments_All()
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    long ContractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    List<ListItem> selectedItems = ContractManagement.GetSelectedItems(lstBoxFileTags);

                    var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                    List<Cont_SP_GetContractDocuments_FileTags_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_FileTags_All_Result>();

                    lstContDocs = ContractDocumentManagement.Cont_SP_GetContractDocuments_FileTags_All(Convert.ToInt32(AuthenticationHelper.CustomerID), ContractInstanceID, selectedFileTags);

                    grdContractDocuments.DataSource = lstContDocs;
                    grdContractDocuments.DataBind();

                    Session["TotalRows"] = lstContDocs.Count;
                    bindPageNumber(1);

                    lstContDocs.Clear();
                    lstContDocs = null;

                    upContractDocUploadPopup.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindTaskDocuments_All(GridView grdtoBindDocuments, long contractID, long taskID)
        {
            try
            {
                if (contractID != 0 && taskID != 0)
                {
                    List<Cont_SP_GetContractDocuments_All_Result> lstContDocs = new List<Cont_SP_GetContractDocuments_All_Result>();

                    lstContDocs = ContractDocumentManagement.GetContractDocuments_All(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID);

                    grdtoBindDocuments.DataSource = lstContDocs;
                    grdtoBindDocuments.DataBind();

                    if (lstContDocs.Count > 0)
                    {
                        var prevTaskDocumentMapping = ContractTaskManagement.GetTaskDocumentMapping(contractID, taskID);

                        if (prevTaskDocumentMapping != null)
                        {
                            if (prevTaskDocumentMapping.Count > 0)
                            {
                                long fileID = 0;

                                for (int i = 0; i < grdtoBindDocuments.Rows.Count; i++)
                                {
                                    if (grdtoBindDocuments.Rows[i].RowType == DataControlRowType.DataRow)
                                    {
                                        Label lblFileID = (Label)grdtoBindDocuments.Rows[i].FindControl("lblFileID");

                                        if (lblFileID != null)
                                        {
                                            fileID = Convert.ToInt64(lblFileID.Text);

                                            if (fileID != 0)
                                            {
                                                if (prevTaskDocumentMapping.Contains(fileID))
                                                {
                                                    CheckBox chkRowTaskDocument = (CheckBox)grdtoBindDocuments.Rows[i].FindControl("chkRowTaskDocument");

                                                    if (chkRowTaskDocument != null)
                                                    {
                                                        chkRowTaskDocument.Checked = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } //END FOR
                            }
                        }

                        prevTaskDocumentMapping.Clear();
                        prevTaskDocumentMapping = null;

                    }

                    lstContDocs.Clear();
                    lstContDocs = null;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected bool uploadDocuments(long contractID, long docTypeID, HttpFileCollection fileCollection, string fileUploadControlName, long? taskID, long? taskResponseID, string fileTags)
        {
            bool uploadSuccess = false;
            try
            {
                #region Upload Document

                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string directoryPath = string.Empty;
                string fileName = string.Empty;

                Cont_tbl_FileData objContDoc = new Cont_tbl_FileData()
                {
                    ContractID = contractID,
                    DocTypeID = docTypeID,
                    TaskID = taskID,
                    TaskResponseID = taskResponseID,

                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false,

                };


                if (fileCollection.Count > 0)
                {
                    List<KeyValuePair<string, Byte[]>> fileList = new List<KeyValuePair<string, Byte[]>>();

                    if (contractID > 0)
                    {
                        for (int i = 0; i < fileCollection.Count; i++)
                        {
                            HttpPostedFile uploadedFile = fileCollection[i];

                            if (uploadedFile.ContentLength > 0)
                            {
                                string[] keys1 = fileCollection.Keys[i].Split('$');
                                if (keys1[keys1.Count() - 1].Equals(fileUploadControlName))
                                {
                                    fileName = uploadedFile.FileName;
                                }
                                objContDoc.FileName = fileName;                                
                                if (taskID == null && taskResponseID == null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt64(contractID) + "/" + docTypeID + "/" + objContDoc.Version);
                                else if (taskID != null && taskResponseID == null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt64(contractID) + "/" + taskID + "/" + docTypeID + "/" + objContDoc.Version);
                                else if (taskID != null && taskResponseID != null)
                                    directoryPath = Server.MapPath("~/ContractDocuments/" + customerID + "/" + Convert.ToInt64(contractID) + "/" + taskID + "/" + taskResponseID + "/" + docTypeID + "/" + objContDoc.Version);

                               
                                Guid fileKey1 = Guid.NewGuid();
                                string finalPath1 = Path.Combine(directoryPath, fileKey1 + Path.GetExtension(uploadedFile.FileName));
                                Stream fs = uploadedFile.InputStream;
                                BinaryReader br = new BinaryReader(fs);
                                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                                fileList.Add(new KeyValuePair<string, Byte[]>(finalPath1, bytes));

                                objContDoc.FilePath = directoryPath.Substring(System.Web.Hosting.HostingEnvironment.MapPath("~/").Length).Replace('\\', '/').Insert(0, "~/");
                                objContDoc.FileKey = fileKey1.ToString();
                                objContDoc.VersionDate = DateTime.Now;
                                objContDoc.CreatedOn = DateTime.Now;
                                objContDoc.FileSize = uploadedFile.ContentLength;

                                DocumentManagement.Contract_SaveDocFiles(fileList);
                                long newFileID = ContractDocumentManagement.CreateContractDocumentMapping(objContDoc);
                                
                                if (newFileID > 0 & !string.IsNullOrEmpty(fileTags))
                                {
                                    string[] arrFileTags = fileTags.Trim().Split(',');

                                    if (arrFileTags.Length > 0)
                                    {
                                        List<Cont_tbl_FileDataTagsMapping> lstFileTagMapping = new List<Cont_tbl_FileDataTagsMapping>();

                                        for (int j = 0; j < arrFileTags.Length; j++)
                                        {
                                            Cont_tbl_FileDataTagsMapping objFileTagMapping = new Cont_tbl_FileDataTagsMapping()
                                            {
                                                FileID = newFileID,
                                                FileTag = arrFileTags[j].Trim(),
                                                IsActive = true,
                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now,
                                            };

                                            lstFileTagMapping.Add(objFileTagMapping);
                                        }

                                        if (lstFileTagMapping.Count > 0)
                                        {
                                            ContractDocumentManagement.CreateUpdate_FileTagsMapping(lstFileTagMapping);
                                        }
                                    }
                                }

                                fileList.Clear();
                            }

                        }//End For Each
                    }
                }

                #endregion

                return uploadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return uploadSuccess;
            }
        }

        protected bool shareDocuments(GridView grdDocuments, long contractID, long taskID)
        {
            bool uploadSuccess = false;
            try
            {
                List<long> lstFileIDstoShare = new List<long>();
                List<Cont_tbl_TaskDocumentMapping> lstFilestoShare = new List<Cont_tbl_TaskDocumentMapping>();

                for (int i = 0; i < grdDocuments.Rows.Count; i++)
                {
                    CheckBox chkRowTaskDocument = (CheckBox)grdDocuments.Rows[i].FindControl("chkRowTaskDocument");

                    if (chkRowTaskDocument != null)
                    {
                        if (chkRowTaskDocument.Checked)
                        {
                            Label lblFileID = (Label)grdDocuments.Rows[i].FindControl("lblFileID");

                            if (lblFileID != null)
                            {
                                if (lblFileID.Text != "")
                                    lstFileIDstoShare.Add(Convert.ToInt64(lblFileID.Text));
                            }
                        }
                    }
                }

                if (lstFileIDstoShare.Count > 0)
                {
                    lstFileIDstoShare.ForEach(eachFile =>
                    {
                        Cont_tbl_TaskDocumentMapping objTaskDocShare = new Cont_tbl_TaskDocumentMapping()
                        {
                            ContractID = contractID,
                            TaskID = Convert.ToInt64(taskID),
                            FileID = eachFile,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            UpdatedBy = AuthenticationHelper.UserID,
                            IsActive = true,
                        };

                        lstFilestoShare.Add(objTaskDocShare);
                    });

                    var exisingSharedFileIDs = ContractTaskManagement.GetTaskDocumentMapping(contractID, taskID);
                    var currentFileIDs = lstFilestoShare.Select(row => row.FileID).ToList();

                    bool matchSuccess = ContractCommonMethods.matchLists(exisingSharedFileIDs, currentFileIDs);

                    if (!matchSuccess)
                    {
                        ContractTaskManagement.DeActive_TaskDocumentMapping(contractID, taskID, AuthenticationHelper.UserID);

                        if (lstFilestoShare.Count > 0)
                            uploadSuccess = ContractTaskManagement.CreateUpdate_TaskDocumentMapping(lstFilestoShare);
                        else
                            uploadSuccess = false;
                    }
                    else
                        uploadSuccess = true;
                }

                return uploadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return uploadSuccess;
            }
        }

        protected void rptDocmentVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                // var AllinOneDocumentList=null;
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                if (e.CommandName.Equals("View"))
                {
                    if (e.CommandArgument != null)
                    {
                        long fileID = Convert.ToInt64(e.CommandArgument);
                        if (fileID != 0)
                            ViewContractDocument(fileID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                // cvDuplicateEntry.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void rptDocmentVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }

        protected void grdContDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    GridView senderGridView = sender as GridView;

                    if (senderGridView != null)
                    {
                        CustomValidator cvtoShowErrorMsg = null;

                        if (senderGridView.ID == "grdTaskContractDocuments")
                            cvtoShowErrorMsg = cvTaskTab;
                        else
                            cvtoShowErrorMsg = cvContractDocument;

                        if (e.CommandName.Equals("DownloadContDoc"))
                        {
                            bool downloadSuccess = DownloadContractDocument(Convert.ToInt64(e.CommandArgument));

                            if (!downloadSuccess)
                            {
                                cvContractDocument.IsValid = false;
                                cvContractDocument.ErrorMessage = "Sorry!No File To Download";
                            }
                        }
                        else if (e.CommandName.Equals("DeleteContDoc"))
                        {
                            DeleteContractDocument(Convert.ToInt64(e.CommandArgument));

                            //Bind Contract Related Documents
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                BindFileTags();
                                BindContractDocuments_Paging();
                            }
                        }
                        else if (e.CommandName.Equals("Info_Doc"))
                        {
                            if (ViewState["ContractInstanceID"] != null)
                            {
                                long fileID = Convert.ToInt64(e.CommandArgument);
                                long contractID = Convert.ToInt64(e.CommandArgument);

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "showDocInfo", "OpenDocInfoPopup('" + fileID + "','" + contractID + "');", true);
                            }
                        }
                        else if (e.CommandName.Equals("View_Doc"))
                        {
                            var AllinOneDocumentList = ContractDocumentManagement.GetContractDocumentByID(Convert.ToInt64(e.CommandArgument));

                            if (AllinOneDocumentList != null)
                            {
                                DocumentPath = string.Empty;

                                string filePath = Path.Combine(Server.MapPath(AllinOneDocumentList.FilePath), AllinOneDocumentList.FileKey + Path.GetExtension(AllinOneDocumentList.FileName));
                                if (AllinOneDocumentList.FilePath != null && File.Exists(filePath))
                                {
                                    string fileExtension = System.IO.Path.GetExtension(filePath);

                                    if (fileExtension.ToUpper() == ".ZIP" || fileExtension.ToUpper() == ".7Z" || fileExtension.ToUpper() == ".RAR")
                                    {
                                        lblMessage.Text = "";
                                        lblMessage.Text = "Compressed file(s) can not be preview, Please try to download file(s)";
                                    }
                                    else
                                    {
                                        string Folder = "~/TempFiles";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                        string DateFolder = Folder + "/" + File;
                                        string extension = System.IO.Path.GetExtension(filePath);
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }
                                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                        string FileName = DateFolder + "/" + User + "" + extension;
                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (AllinOneDocumentList.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();
                                        DocumentPath = FileName;
                                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);
                                    }

                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void grdContDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    grdContractDocuments.PageIndex = e.NewPageIndex;
                    BindContractDocuments_Paging();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public bool DownloadContractDocument(long contFileID)
        {
            bool downloadSuccess = false;
            try
            {
                var file = ContractDocumentManagement.GetContractDocumentByID(contFileID);

                if (file != null)
                {
                    if (file.FilePath != null)
                    {
                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                        if (filePath != null && File.Exists(filePath))
                        {
                            Response.Buffer = true;
                            Response.Clear();
                            Response.ClearContent();
                            Response.ClearHeaders();
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("content-disposition", "attachment; filename=" + DocumentManagement.MakeValidFileName(file.FileName));
                            if (file.EnType == "M")
                            {
                                Response.BinaryWrite(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            else
                            {
                                Response.BinaryWrite(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath))); // create the file
                            }
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                            ContractManagement.CreateAuditLog("CT", Convert.ToInt32(ViewState["ContractInstanceID"]), "Cont_tbl_FileData", "Download", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document Downloaded", true);

                            downloadSuccess = true;
                        }
                    }
                }

                return downloadSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return downloadSuccess;
            }
        }

        public void DeleteContractDocument(long contFileID)
        {
            try
            {
                if (contFileID != 0)
                {
                    bool deleteSuccess = false;
                    deleteSuccess = ContractDocumentManagement.DeleteContDocument(contFileID, AuthenticationHelper.UserID);
                    if (deleteSuccess)
                    {
                        ContractDocumentManagement.Delete_FileTagsMapping(contFileID, AuthenticationHelper.UserID);

                        BindFileTags();

                        cvContractDocument.IsValid = false;
                        cvContractDocument.ErrorMessage = "Document Deleted Successfully";
                        vsContractDocument.CssClass = "alert alert-success";

                        if (ViewState["ContractInstanceID"] != null)
                        {
                            ContractManagement.CreateAuditLog("CT", Convert.ToInt32(ViewState["ContractInstanceID"]), "Cont_tbl_FileData", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Document Deleted", true);
                        }
                    }
                    else
                    {
                        cvContractDocument.IsValid = false;
                        cvContractDocument.ErrorMessage = "Something went wrong, Please try again.";
                        vsContractDocument.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractDocument.IsValid = false;
                cvContractDocument.ErrorMessage = "Server Error Occurred. Please try again.";
                vsContractDocument.CssClass = "alert alert-danger";
            }
        }

        public bool ViewContractDocument(long contFileID)
        {
            bool viewSuccess = false;
            try
            {
                var file = ContractDocumentManagement.GetContractDocumentByID(contFileID);

                if (file != null)
                {
                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                    if (file.FilePath != null && File.Exists(filePath))
                    {
                        string Folder = "~/TempFiles";
                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                        string DateFolder = Folder + "/" + File;

                        string extension = System.IO.Path.GetExtension(filePath);

                        Directory.CreateDirectory(Server.MapPath(DateFolder));

                        if (!Directory.Exists(DateFolder))
                        {
                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                        }

                        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                        string FileName = DateFolder + "/" + User + "" + extension;

                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                        BinaryWriter bw = new BinaryWriter(fs);
                        if (file.EnType == "M")
                        {
                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        else
                        {
                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                        }
                        bw.Close();
                        DocumentPath = FileName;

                        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                        lblMessage.Text = "";


                    }

                }

                return viewSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return viewSuccess;
            }
        }

        public void BindTaskResponses(long contractID, long taskID)
        {
            try
            {
                List<Cont_SP_GetTaskResponses_All_Result> lstTaskResponses = new List<Cont_SP_GetTaskResponses_All_Result>();

                lstTaskResponses = ContractTaskManagement.GetTaskResponses(contractID, taskID);

                if (lstTaskResponses != null && lstTaskResponses.Count > 0)
                {
                    lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.StatusChangeOn).ToList();

                    //lstTaskResponses = lstTaskResponses.OrderByDescending(entry => entry.UpdatedOn).ThenByDescending(entry => entry.CreatedOn).ToList();
                    grdTaskResponseLog.DataSource = lstTaskResponses;
                    grdTaskResponseLog.DataBind();

                    divTaskResponseLog.Visible = true;
                }
                else
                {
                    clearTaskResponseLog();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Something went wrong, Please try again";
                vsContractDocument.CssClass = "alert alert-danger";
            }
        }
        public void clearTaskResponseLog()
        {
            divTaskResponseLog.Visible = false;
            grdTaskResponseLog.DataSource = null;
            grdTaskResponseLog.DataBind();
        }

        protected void grdTaskResponseLog_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null && ViewState["ContractInstanceID"] != null)
                {
                    long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    if (e.CommandName.Equals("DownloadTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long taskResponseID = Convert.ToInt64(commandArgs[0]);
                        long taskID = Convert.ToInt64(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0 && contractID != 0)
                        {
                            var lstTaskResponseDocument = ContractTaskManagement.GetTaskResponseDocuments(contractID, taskID, taskResponseID);

                            if (lstTaskResponseDocument.Count > 0)
                            {
                                using (ZipFile responseDocZip = new ZipFile())
                                {
                                    int i = 0;
                                    foreach (var file in lstTaskResponseDocument)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                        if (file.FilePath != null && File.Exists(filePath))
                                        {                                            
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + file.Version + "." + file.FileName.Substring(idx + 1);

                                            if (!responseDocZip.ContainsEntry(file.DocTypeID + "/" + str))
                                            {
                                                if (file.EnType == "M")
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    responseDocZip.AddEntry(file.DocTypeID + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    var zipMs = new MemoryStream();
                                    responseDocZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=TaskResponseDocuments-" + DateTime.Now.ToString("dd-MM-yyyy") + ".zip");
                                    Response.BinaryWrite(Filedata);
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                                }
                            }
                            else
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "No Document Available for Download.";
                                cvTaskTab.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskResponseDoc"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long taskResponseID = Convert.ToInt64(commandArgs[0]);
                        long taskID = Convert.ToInt64(commandArgs[1]);

                        if (taskResponseID != 0 && taskID != 0 && contractID != 0)
                        {
                            var lstTaskDocument = ContractTaskManagement.GetTaskResponseDocuments(contractID, taskID, taskResponseID);

                            if (lstTaskDocument.Count > 0)
                            {
                                List<Cont_tbl_FileData> entitiesData = lstTaskDocument.Where(entry => entry.Version != null).ToList();

                                if (lstTaskDocument.Where(entry => entry.Version == null).ToList().Count > 0)
                                {
                                    Cont_tbl_FileData entityData = new Cont_tbl_FileData();
                                    entityData.Version = "1.0";
                                    entityData.ContractID = contractID;
                                    entityData.TaskID = taskID;
                                    entityData.TaskResponseID = taskResponseID;
                                    entitiesData.Add(entityData);
                                }

                                if (entitiesData.Count > 0)
                                {
                                    rptDocmentVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                    rptDocmentVersionView.DataBind();

                                    //ViewContractDocument(entitiesData[0].ID);

                                    bool downloadSuccess = ViewContractDocument(entitiesData[0].ID);

                                    if (!downloadSuccess)
                                    {
                                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideShowTask_Script", "HideShowTaskDiv('true');", true);
                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                    }

                                    //foreach (var file in entitiesData)
                                    //{
                                    //    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    //    if (file.FilePath != null && File.Exists(filePath))
                                    //    {
                                    //        string Folder = "~/TempFiles";
                                    //        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    //        string DateFolder = Folder + "/" + File;

                                    //        string extension = System.IO.Path.GetExtension(filePath);

                                    //        Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    //        if (!Directory.Exists(DateFolder))
                                    //        {
                                    //            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    //        }

                                    //        string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    //        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    //        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    //        string FileName = DateFolder + "/" + User + "" + extension;

                                    //        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    //        BinaryWriter bw = new BinaryWriter(fs);
                                    //        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    //        bw.Close();
                                    //        DocumentPath = FileName;

                                    //        DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                    //        lblMessage.Text = "";

                                    //        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                                    //    }
                                    //    else
                                    //    {
                                    //        lblMessage.Text = "There is no document available to preview";
                                    //    }
                                    //    break;
                                    //}
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                            }
                            else
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "No Document Available for View.";
                                cvTaskTab.CssClass = "alert alert-danger";
                                return;
                            }
                        }
                    }
                    else if (e.CommandName.Equals("TaskReminder"))
                    {
                        bool sendSuccess = false;

                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                        long taskID = Convert.ToInt64(commandArgs[0]);
                        int userID = Convert.ToInt32(commandArgs[1]);
                        int roleID = Convert.ToInt32(commandArgs[2]);

                        if (contractID != 0 && taskID != 0 && userID != 0 && roleID != 0)
                        {
                            var newTaskRecord = ContractTaskManagement.GetTaskDetailsByTaskID(contractID, taskID);

                            if (newTaskRecord != null)
                            {
                                List<Tuple<int, int>> lstTaskUserMapping = new List<Tuple<int, int>>();

                                lstTaskUserMapping.Add(new Tuple<int, int>(userID, roleID));

                                if (lstTaskUserMapping.Count > 0)
                                {
                                    sendSuccess = SendEmail_TaskAssignedUsers(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, newTaskRecord, lstTaskUserMapping, "Task Reminder");

                                    if (sendSuccess)
                                        ContractManagement.CreateAuditLog("C", Convert.ToInt64(contractID), "Cont_tbl_FileData", "Mail", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Email Sent with Contract Document(s)", true);
                                }
                            }
                        }

                        if (sendSuccess)
                        {
                            cvTaskTab.IsValid = false;
                            cvTaskTab.ErrorMessage = "An Email containing Task detail and access URL to provide response sent to assignee.";
                            vsTaskTab.CssClass = "alert alert-success";

                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUp_Script", "scrollUp();", true);
                        }
                        else
                        {
                            cvTaskTab.IsValid = false;
                            cvTaskTab.ErrorMessage = "Something went wrong, Please try again.";
                            vsTaskTab.CssClass = "alert alert-danger";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Something went wrong, Please try again";
            }
        }


        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            if (ViewState["ContractInstanceID"] != null)
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                string messageAll = string.Empty;
                long contractID = 0;

                bool formValidateSuccess = false;
                bool sendSuccess = false;

                List<string> lstTO = new List<string>();
                List<string> lstCc = new List<string>();
                List<string> lstBcc = new List<string>();

                List<string> lstErrorMsg = new List<string>();
                List<int> lstTaskUserMapping = new List<int>();
                string strReceiver = string.Empty;

                #region Data Validation

                if (String.IsNullOrEmpty(tbxMailTo.Text))
                    lstErrorMsg.Add("Provide at least One Email-ID to Send this Message");
                else
                {
                    var lstAllUsers = ContractUserManagement.GetAllUsers_Contract(customerID);

                    if (lstAllUsers.Count > 0)
                    {
                        string eachMail = string.Empty;
                        strReceiver = tbxMailTo.Text;
                        string[] arrEmails = strReceiver.Split(',');

                        for (int i = 0; i < arrEmails.Length; i++)
                        {
                            eachMail = arrEmails[i].Trim();
                            if (!string.IsNullOrEmpty(eachMail))
                            {
                                if (ZohoCRMAPI.IsValidEmail(eachMail))
                                {
                                    var userEmailExists = lstAllUsers.Where(row => row.Email == eachMail).FirstOrDefault();
                                    if (userEmailExists != null)
                                        lstTO.Add(arrEmails[i]);
                                    else
                                    {
                                        lstErrorMsg.Add("Please Check Email - '" + eachMail + "', You can able to Send Email only to Contract Product User(s) of Your Organization");
                                    }
                                }
                                else
                                {
                                    lstErrorMsg.Add("Invalid Email - '" + eachMail + "'");
                                }
                            }
                        }

                        if (lstTO.Count > 0)
                            formValidateSuccess = true;
                    }
                }

                if (String.IsNullOrEmpty(tbxMailMsg.Text))
                    lstErrorMsg.Add("Provide Message to Send");
                else
                    formValidateSuccess = true;

                int selectedDocumentCount = 0;
                foreach (GridViewRow eachGridRow in grdMailDocumentList.Rows)
                {
                    if (eachGridRow.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRowMailDocument = (CheckBox)eachGridRow.FindControl("chkRowMailDocument");

                        if (chkRowMailDocument != null)
                        {
                            if (chkRowMailDocument.Checked)
                                selectedDocumentCount++;
                        }
                    }
                }

                if (selectedDocumentCount == 0)
                    lstErrorMsg.Add("Provide Select at lease One Document to Send");
                else if (selectedDocumentCount > 0)
                    formValidateSuccess = true;

                if (lstErrorMsg.Count > 0)
                {
                    formValidateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvMailDocument);
                }

                #endregion

                #region Send Mail

                if (formValidateSuccess)
                {
                    string mailMessageToSend = string.Empty;
                    contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    mailMessageToSend = getMailMessage(contractID, tbxMailMsg.Text);

                    if (!string.IsNullOrEmpty(mailMessageToSend))
                    {
                        List<Tuple<string, string>> attachmentwithPath = GetDocumentsToAttach();

                        string folderPath = string.Empty;
                        if (attachmentwithPath.Count > 0)
                        {
                            try
                            {
                                SendGridEmailManager.SendGridMailwithAttachment(ConfigurationManager.AppSettings["SenderEmailAddress"], Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]), lstTO, lstCc, lstBcc, "Litigation Case Summary with Documents", messageAll, attachmentwithPath);
                                sendSuccess = true;
                            }
                            catch (Exception ex)
                            {
                                sendSuccess = false;
                            }
                        }
                    }
                }

                if (sendSuccess)
                {
                    cvMailDocument.IsValid = false;
                    cvMailDocument.ErrorMessage = "E-Mail Sent Successfully";
                    //vsMailDocument.CssClass = "alert alert-success";
                    cvMailDocument.CssClass = "alert alert-success";

                    tbxMailMsg.Text = "";
                    tbxMailTo.Text = "";

                    ContractManagement.CreateAuditLog("C", Convert.ToInt64(contractID), "Cont_tbl_FileData", "Mail", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Email Sent with Contract Document(s)", true);
                }
                else
                {
                    cvMailDocument.IsValid = false;
                    cvMailDocument.ErrorMessage = "Something went wrong there, Please try again later";
                    cvMailDocument.CssClass = "alert alert-danger";
                }

                #endregion
            }
        }

        private string getMailMessage(long contractID, string msgText)
        {
            string message = string.Empty;
            var contractDetailRecord = ContractManagement.GetContractByID(contractID);

            if (contractDetailRecord != null)
            {
                message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_DocumentWithSummary
                                    .Replace("@ContractNo", Convert.ToString(contractDetailRecord.ContractNo))
                                    .Replace("@ContractTitle", contractDetailRecord.ContractTitle)
                                    .Replace("@message", msgText)
                                    .Replace("@From", Convert.ToString(AuthenticationHelper.User))
                                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

            }

            return message;
        }


        private List<Tuple<string, string>> GetDocumentsToAttach()
        {
            List<Tuple<string, string>> lstDocsToAttach = new List<Tuple<string, string>>();

            try
            {
                ArrayList DocumentList = new ArrayList();
                ArrayList DocumentFileNameList = new ArrayList();

                foreach (GridViewRow eachGridRow in grdMailDocumentList.Rows)
                {
                    if (eachGridRow.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRowMailDocument = (CheckBox)eachGridRow.FindControl("chkRowMailDocument");

                        if (chkRowMailDocument != null)
                        {
                            if (chkRowMailDocument.Checked)
                            {
                                Label lblID = (Label)eachGridRow.FindControl("lblID");
                                Label lblFileName = (Label)eachGridRow.FindControl("lblFileName");
                                Label lblFilePath = (Label)eachGridRow.FindControl("lblFilePath");

                                if (lblID != null)
                                {
                                    if (!string.IsNullOrEmpty(lblID.Text))
                                    {
                                        var documentRecord = ContractDocumentManagement.GetContractDocumentByID(Convert.ToInt64(lblID.Text));
                                        if (documentRecord != null)
                                        {
                                            string finalFilePath = string.Empty;
                                            string filePath = Path.Combine(Server.MapPath(documentRecord.FilePath), documentRecord.FileKey + Path.GetExtension(documentRecord.FileName));
                                            if (documentRecord.FilePath != null && File.Exists(filePath))
                                            {
                                                string folderPath = "~/TempFiles/" + DateTime.Now.ToString("ddMMyyyy");
                                                string extension = System.IO.Path.GetExtension(filePath);
                                                if (!Directory.Exists(folderPath))
                                                    Directory.CreateDirectory(Server.MapPath(folderPath));
                                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                                string userWiseFileName = documentRecord.FileName + "" + FileDate;
                                                finalFilePath = folderPath + "/" + userWiseFileName + "" + extension;
                                                FileStream fs = new FileStream(Server.MapPath(finalFilePath), FileMode.Create, FileAccess.ReadWrite);
                                                BinaryWriter bw = new BinaryWriter(fs);
                                                if (documentRecord.EnType == "M")
                                                {
                                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                else
                                                {
                                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                                }
                                                bw.Close();

                                                DocumentPath = finalFilePath;
                                                DocumentPath = DocumentPath.Substring(2, DocumentPath.Length - 2);

                                                //DocumentList.Add(lblID.Text);
                                                //DocumentFileNameList.Add(lblFilePath.Text);

                                                lstDocsToAttach.Add(new Tuple<string, string>(DocumentPath, finalFilePath));

                                                //ViewState["ListofFile"] = DocumentList;
                                                //ViewState["docsToAttach"] = lstDocsToAttach;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

                return lstDocsToAttach;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return lstDocsToAttach;
            }
        }

        public string ShowPriority(string priorityID)
        {
            try
            {
                if (!string.IsNullOrEmpty(priorityID))
                {
                    if (priorityID == "1")
                        return "High";
                    else if (priorityID == "2")
                        return "Medium";
                    else if (priorityID == "3")
                        return "Low";
                    else
                        return string.Empty;
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }


        #endregion

        private int GetTotalPagesCount(int tabIndex) //TabIndex ---Bind Page No to which Drop down Page No
        {
            try
            {
                int totalPages = 0;
                int pageSize = 0;
                TotalRows.Value = Session["TotalRows"].ToString();

                if (tabIndex == 1) //Document Tab
                {
                    pageSize = Convert.ToInt32(grdContractDocuments.PageSize);
                }
                else if (tabIndex == 2) //Task Tab
                {
                    pageSize = Convert.ToInt32(grdTaskActivity.PageSize);
                }
                else if (tabIndex == 3) //Audit Log Tab
                {
                    pageSize = Convert.ToInt32(gvContractAuditLog.PageSize);
                }

                if (pageSize != 0)
                {
                    totalPages = Convert.ToInt32(TotalRows.Value) / pageSize;

                    // total page item to be displyed
                    int pageItemRemain = Convert.ToInt32(TotalRows.Value) % pageSize;

                    // remaing no of pages
                    if (pageItemRemain > 0)// set total No of pages
                    {
                        totalPages = totalPages + 1;
                    }
                    else
                    {
                        totalPages = totalPages + 0;
                    }
                }

                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        private void bindPageNumber(int tabIndex) //TabIndex ---Bind Page No to which Drop down Page No
        {
            try
            {
                if (tabIndex != 0)
                {
                    int count = Convert.ToInt32(GetTotalPagesCount(tabIndex));


                    //Select Which Grid to Loop based on Selected Category/Type
                    DropDownListChosen.DropDownListChosen ddlPageNo = null;

                    if (tabIndex == 1) //Document Tab
                    {
                        ddlPageNo = DropDownListPageNo;
                    }
                    else if (tabIndex == 2) //Task Tab
                    {
                        ddlPageNo = ddlPageNo_Task;
                    }
                    else if (tabIndex == 3) //Audit Log Tab
                    {
                        ddlPageNo = ddlPageNo_AuditLog;
                    }

                    if (ddlPageNo.Items.Count > 0)
                    {
                        ddlPageNo.Items.Clear();
                    }

                    ddlPageNo.DataTextField = "ID";
                    ddlPageNo.DataValueField = "ID";

                    ddlPageNo.DataBind();
                    for (int i = 1; i <= count; i++)
                    {
                        string chkPageID = i.ToString();
                        ddlPageNo.Items.Add(chkPageID);
                    }
                    if (count > 0)
                    {
                        ddlPageNo.SelectedValue = ("1").ToString();
                    }
                    else if (count == 0)
                    {
                        ddlPageNo.Items.Add("0");
                        ddlPageNo.SelectedValue = ("0").ToString();
                    }

                    SetShowingRecords(tabIndex);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SetShowingRecords(int tabIndex)//TabIndex ---Bind Page No to which Drop down Page No
        {
            try
            {
                if (tabIndex != 0)
                {
                    int PageSize = 0;
                    int PageNumber = 0;

                    int EndRecord = 0;
                    int TotalRecord = 0;
                    int TotalValue = 0;

                    if (tabIndex == 1) //Document Tab
                    {
                        PageSize = grdContractDocuments.PageSize;
                        PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                    }
                    else if (tabIndex == 2) //Task Tab
                    {
                        PageSize = grdTaskActivity.PageSize;
                        PageNumber = Convert.ToInt32(ddlPageNo_Task.SelectedValue);
                    }
                    else if (tabIndex == 3) //Audit Log Tab
                    {
                        PageSize = gvContractAuditLog.PageSize;
                        PageNumber = Convert.ToInt32(ddlPageNo_AuditLog.SelectedValue);
                    }

                    TotalValue = PageSize * PageNumber;

                    if (Session["TotalRows"] != null)
                    {
                        TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                        if (TotalRecord < TotalValue)
                        {
                            EndRecord = TotalRecord;
                        }
                        else
                        {
                            EndRecord = TotalValue;
                        }
                    }

                    if (tabIndex == 1)
                    {
                        if (TotalRecord != 0)
                            lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                        else
                            lblStartRecord.Text = "0";

                        lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                        lblTotalRecord.Text = TotalRecord.ToString();
                    }
                    else if (tabIndex == 2)
                    {
                        if (TotalRecord != 0)
                            lblStartRecord_Task.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                        else
                            lblStartRecord_Task.Text = "0";

                        lblEndRecord_Task.Text = Convert.ToString(EndRecord) + " ";
                        lblTotalRecord_Task.Text = TotalRecord.ToString();
                    }
                    else if (tabIndex == 3)
                    {
                        if (TotalRecord != 0)
                            lblStartRecord_AuditLog.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                        else
                            lblStartRecord_AuditLog.Text = "0";

                        lblEndRecord_AuditLog.Text = Convert.ToString(EndRecord) + " ";
                        lblTotalRecord_AuditLog.Text = TotalRecord.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }



        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownListChosen.DropDownListChosen ddlSender = sender as DropDownListChosen.DropDownListChosen;

                if (ddlSender != null)
                {
                    int tabIndex = 0;
                    int chkSelectedPage = Convert.ToInt32(ddlSender.SelectedItem.ToString());

                    if (ddlSender.ID == "DropDownListPageNo")
                    {
                        ViewState["PageNumberFlagID"] = 1;

                        grdContractDocuments.PageIndex = chkSelectedPage - 1;
                        BindContractDocuments_Paging();

                        tabIndex = 1;
                    }
                    else if (ddlSender.ID == "ddlPageNo_Task")
                    {
                        if (ViewState["ContractInstanceID"] != null)
                        {
                            int customerID = -1;
                            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                            long contractInstanceID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                            if (contractInstanceID != 0)
                            {
                                ViewState["TaskPageFlag"] = 1;
                                grdTaskActivity.PageIndex = chkSelectedPage - 1;
                                BindContractTasks_Paging(customerID, contractInstanceID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);
                            }
                        }

                        tabIndex = 2;
                    }
                    else if (ddlSender.ID == "ddlPageNo_AuditLog")
                    {
                        ViewState["PageNumberAuditLogFlagID"] = 1;
                        gvContractAuditLog.PageIndex = chkSelectedPage - 1;
                        BindContractAuditLogs();

                        tabIndex = 3;
                    }

                    SetShowingRecords(tabIndex);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string ShowAuditLog_Timeline(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    strTimeLineHTML.Append("<ul class=\"timeline timeline-horizontal\">");

                    lstAuditLogs.ForEach(eachAuditLog =>
                    {
                        strTimeLineHTML.Append("<li class=\"timeline-item\">" +
                                "<div class=\"timeline-badge primary\"><i class=\"fa fa-check\"></i></div>" +
                                "<div class=\"timeline-panel\">" +
                                    "<div class='timeline-heading'>" +
                                       " <h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                       " <p><small class=\"text-muted\"><i class=\"fa fa-clock-o\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</small></p>" +
                                   " </div>" +
                                    "<div class=\"timeline-body\">" +
                                        "<p>" + eachAuditLog.Remark + "</p>" +
                                   " </div>" +
                                "</div>" +
                            "</li>");
                    });

                    strTimeLineHTML.Append("</ul>");

                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public string ShowAuditLog_VerticalTimeline(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    strTimeLineHTML.Append("<ul class=\"timeline\">");

                    lstAuditLogs.ForEach(eachAuditLog =>
                    {
                        strTimeLineHTML.Append("<li class=\"timeline-item\">" +
                                "<div class=\"timeline-badge primary\"><i class=\"fa fa-check\"></i></div>" +
                                "<div class=\"timeline-panel\">" +
                                    "<div class='timeline-heading'>" +
                                       " <h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                       " <p><small class=\"text-muted\"><i class=\"fa fa-clock-o\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</small></p>" +
                                   " </div>" +
                                    "<div class=\"timeline-body\">" +
                                        "<p>" + eachAuditLog.Remark + "</p>" +
                                   " </div>" +
                                "</div>" +
                            "</li>");
                    });

                    strTimeLineHTML.Append("</ul>");

                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public string ShowAuditLog_Centered_Clickable(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    var minDate = lstAuditLogs.Select(row => row.CreatedOn).Min();
                    var maxDate = lstAuditLogs.Select(row => row.CreatedOn).Max();

                    if (minDate != null && maxDate != null)
                    {
                        strTimeLineHTML.Append("<ul class=\"timeline\">");

                        int accordionCount = 0;
                        var startDate = maxDate;
                        do
                        {
                            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(startDate);
                            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(startDate);

                            var auditLogsMonthWise = lstAuditLogs.Where(row => row.CreatedOn >= MonthStartDate && row.CreatedOn <= MonthEndDate).OrderByDescending(row => row.CreatedOn).ToList();

                            if (auditLogsMonthWise.Count > 0)
                            {
                                accordionCount++;

                                string accordionCollapseClass = "panel-collapse collapse ";//change by Ruchi on 12th sep 2018

                                string faIcon = "fa fa-minus";
                                if (accordionCount > 1)
                                {
                                    accordionCollapseClass = "panel-collapse collapse";
                                    faIcon = "fa fa-minus";
                                }

                                if (accordionCount % 2 == 0)
                                    strTimeLineHTML.Append("<li class=\"timeline-inverted\">");
                                else
                                    strTimeLineHTML.Append("<li>");

                                strTimeLineHTML.Append("<div class=\"timeline-badge\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\"" +
                                    "data-toggle=\"tooltip\" data-placement=\"bottom\" ToolTip=\"View Detail\"><i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></div>");

                                strTimeLineHTML.Append("<div class=\"timeline-panel\">");

                                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<h2 class=\"panel-title new-products-title\">" +
                                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>" +
                                    "</a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style =\"max-height: 300px; overflow-y: auto; margin-top: 5px;\">" +
                                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                                                                "<ul class=newProductsList>");

                                auditLogsMonthWise.ForEach(eachAuditLog =>
                                {
                                    strTimeLineHTML.Append("<li>" +
                                         "<div class=\"timeline-body\">" +
                                          "<div class=\"col-md-3 pl0\">" +
                                                "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy h:mm tt") + "</p>" +
                                            "</div>" +
                                             "<div class=\"col-md-2 pl0\">" +
                                                "<p class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</p>" +
                                             "</div>" +
                                            "<div class=\"col-md-7 pl0\">" +
                                                "<p>" + eachAuditLog.Remark + "</p>" +
                                            "</div>" +
                                         "</div>" +
                                   "</li>");
                                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                                    //<small class=\"text-muted\"></small>
                                });

                                strTimeLineHTML.Append("</ul></div>");
                                strTimeLineHTML.Append("</div>");

                                startDate = MonthStartDate.AddDays(-1);
                            }
                        } while (startDate >= minDate);

                        strTimeLineHTML.Append("</ul>");
                    }
                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        public string ShowAuditLog_Clickable(List<Cont_SP_AuditLogs_All_Result> lstAuditLogs)
        {
            try
            {
                StringBuilder strTimeLineHTML = new StringBuilder();

                if (lstAuditLogs.Count > 0)
                {
                    var minDate = lstAuditLogs.Select(row => row.CreatedOn).Min();
                    var maxDate = lstAuditLogs.Select(row => row.CreatedOn).Max();

                    if (minDate != null && maxDate != null)
                    {
                        strTimeLineHTML.Append("<div class=\"timeline\">" +
                                                "<div class=\"line text-muted\"></div>");

                        int accordionCount = 0;
                        var startDate = maxDate;
                        do
                        {
                            DateTime MonthStartDate = ContractCommonMethods.GetFirstDayOfMonth(startDate);
                            DateTime MonthEndDate = ContractCommonMethods.GetLastDayOfMonth(startDate);

                            var auditLogsMonthWise = lstAuditLogs.Where(row => row.CreatedOn >= MonthStartDate && row.CreatedOn <= MonthEndDate).OrderByDescending(row => row.CreatedOn).ToList();

                            if (auditLogsMonthWise.Count > 0)
                            {
                                accordionCount++;

                                string accordionCollapseClass = "panel-collapse collapse in";//added by Ruchi on 9th sep 2018
                                string faIcon = "fa fa-minus";

                                if (accordionCount > 1)
                                {
                                    accordionCollapseClass = "panel-collapse collapse in";
                                    faIcon = "fa fa-minus";
                                }

                                strTimeLineHTML.Append("<article class=\"panel panel-primary product-panel\" style=\" margin-top: 20px;\">");

                                strTimeLineHTML.Append("<div class=\"panel-heading icon\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLinkIcon\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<i id=\"i" + accordionCount + "\" class=\"" + faIcon + "\"></i></a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div class=\"panel-heading clickable newProducts\" onclick=\"showHideAuditLog(divCollapse" + accordionCount + ",i" + accordionCount + ")\">" +
                                    "<a class=\"newProductsLink\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse" + accordionCount + "\">" +
                                    "<h2 class=\"panel-title new-products-title\">" +
                                    CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(startDate.Month) + "-" + startDate.Year + "</h2>" +
                                    "</a>" +
                                "</div>");

                                strTimeLineHTML.Append("<div id=\"divCollapse" + accordionCount + "\" class=\"panel-body in\" style=\"max-height: 300px; overflow-y: auto; border: 1px solid #c7c7cc; margin-top: 5px; border-radius: 5px\">" +
                                                        "<div id=\"collapse" + accordionCount + "\" class=" + accordionCollapseClass + "\">" +
                                                                "<ul class=newProductsList>");

                                auditLogsMonthWise.ForEach(eachAuditLog =>
                                {
                                    strTimeLineHTML.Append("<li>" +
                                         "<div class=\"timeline-body\">" +
                                             "<div class=\"col-md-2\">" +
                                                "<h4 class=\"timeline-title\">" + eachAuditLog.CreatedByUser + "</h4>" +
                                             "</div>" +
                                            "<div class=\"col-md-3\">" +
                                                "<p><i class=\"fa fa-clock-o mlr5\"></i>" + eachAuditLog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss tt") + "</p>" +
                                            "</div>" +
                                            "<div class=\"col-md-7\">" +
                                                "<p>" + eachAuditLog.Remark + "</p>" +
                                            "</div>" +
                                         "</div>" +
                                   "</li>");
                                    strTimeLineHTML.Append("<div class=\"clearfix\"></div>");

                                    //<small class=\"text-muted\"></small>
                                });

                                strTimeLineHTML.Append("</ul></div>");
                                strTimeLineHTML.Append("</article>");

                                startDate = MonthStartDate.AddDays(-1);
                            }
                        } while (startDate >= minDate);

                        strTimeLineHTML.Append("</div>");
                    }
                }

                return strTimeLineHTML.ToString();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return string.Empty;
            }
        }

        #region Contract-Task

        public void BindContractTasks_Paging(int customerID, long contractID, int pageSize, int pageNumber, GridView grd)
        {
            try
            {
                var lstTasks = ContractTaskManagement.GetContractTasks_Paging(customerID, contractID, pageSize, pageNumber);

                if (lstTasks.Count > 0)
                    lstTasks = lstTasks.OrderByDescending(row => row.UpdatedOn).ToList();

                grd.DataSource = lstTasks;
                grd.DataBind();

                long totalRowCount = 0;
                if (lstTasks.Count > 0)
                {
                    if (lstTasks[0].TotalRowCount != null)
                        totalRowCount = Convert.ToInt64(lstTasks[0].TotalRowCount);
                    else
                    {
                        foreach (var item in lstTasks)
                        {
                            totalRowCount = Convert.ToInt32(item.TotalRowCount);
                            break;
                        }
                    }
                }

                Session["TotalRows"] = totalRowCount;

                lstTasks.Clear();
                lstTasks = null;

                //lblStartRecord_Task.Text = DropDownListPageNo.SelectedValue;
                if (Convert.ToInt32(ViewState["TaskPageFlag"]) == 0)
                {
                    bindPageNumber(2);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnCloseTask_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskControls();
                HideShowTaskDiv(false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearTask_Click(object sender, EventArgs e)
        {
            try
            {
                clearTaskControls();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void clearTaskControls()
        {
            try
            {
                tbxTaskTitle.Text = "";
                tbxTaskDesc.Text = "";
                tbxTaskAssignDate.Text = "";
                tbxTaskDueDate.Text = "";
                tbxExpOutcome.Text = "";
                tbxTaskRemark.Text = "";

                lstBoxTaskUser.ClearSelection();
                ddlTaskPriority.ClearSelection();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        public void enableDisableTaskControls(bool flag)
        {
            try
            {
                tbxTaskTitle.Enabled = flag;
                tbxTaskDesc.Enabled = flag;
                tbxTaskDueDate.Enabled = flag;
                tbxExpOutcome.Enabled = flag;
                tbxTaskRemark.Enabled = flag;

                if (flag)
                {
                    lstBoxTaskUser.Attributes.Remove("disabled");
                    ddlTaskPriority.Attributes.Remove("disabled");
                }
                else
                {
                    lstBoxTaskUser.Attributes.Add("disabled", "disabled");
                    ddlTaskPriority.Attributes.Add("disabled", "disabled");
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void btnSaveTask_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    long taskID = 0;
                    long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    bool formValidateSuccess = false;
                    bool saveSuccess = false;

                    List<string> lstErrorMsg = new List<string>();
                    List<Tuple<int, int>> lstTaskUserMapping = new List<Tuple<int, int>>();

                    #region Data Validation

                    if (String.IsNullOrEmpty(tbxTaskTitle.Text))
                        lstErrorMsg.Add("Provide Task Title");
                    else
                        formValidateSuccess = true;

                    if (String.IsNullOrEmpty(tbxTaskDesc.Text))
                        lstErrorMsg.Add("Provide Task Description");
                    else
                        formValidateSuccess = true;

                    if (rbTaskType.SelectedValue == "" || rbTaskType.SelectedValue == null)
                        lstErrorMsg.Add("Select Task Type");
                    else
                        formValidateSuccess = true;

                    int selectedUserCount = 0;

                    if (rbTaskType.SelectedValue != null || rbTaskType.SelectedValue != "")
                    {
                        if (rbTaskType.SelectedValue == "6")
                        {
                            foreach (ListItem eachUser in lstBoxTaskUser.Items)
                            {
                                if (eachUser.Selected)
                                    selectedUserCount++;
                            }
                        }
                        else
                        {
                            foreach (ListItem eachUser in lstBoxTaskUser.Items)
                            {
                                if (eachUser.Selected)
                                    selectedUserCount++;
                            }
                        }
                    }

                    if (selectedUserCount > 0)
                        formValidateSuccess = true;
                    else
                        lstErrorMsg.Add("Select at least One User to Assign");

                    if (String.IsNullOrEmpty(tbxTaskAssignDate.Text))
                        lstErrorMsg.Add("Provide Task Assign On, If not available set is to Current Date");
                    else
                    {
                        bool check = ContractCommonMethods.CheckDate(tbxTaskAssignDate.Text.Trim());
                        if (!check)
                        {
                            lstErrorMsg.Add("Invalid Task Assign On Date or Date should be in DD-MM-YYYY Format");
                        }
                        else
                            formValidateSuccess = true;
                    }

                    if (String.IsNullOrEmpty(tbxTaskDueDate.Text))
                        lstErrorMsg.Add("Provide Task Due Date");
                    else
                    {
                        bool check = ContractCommonMethods.CheckDate(tbxTaskDueDate.Text.Trim());
                        if (!check)
                        {
                            lstErrorMsg.Add("Invalid Task Due Date or Date should be in DD-MM-YYYY Format");
                        }
                        else
                            formValidateSuccess = true;
                    }

                    if (!String.IsNullOrEmpty(tbxTaskAssignDate.Text) && !String.IsNullOrEmpty(tbxTaskDueDate.Text))
                    {
                        bool check = ContractCommonMethods.CheckDate(tbxTaskDueDate.Text.Trim());
                        bool check1 = ContractCommonMethods.CheckDate(tbxTaskAssignDate.Text.Trim());

                        if (check && check1)
                        {
                            if (Convert.ToDateTime(tbxTaskAssignDate.Text).Date <= Convert.ToDateTime(tbxTaskAssignDate.Text).Date)
                                formValidateSuccess = true;
                            else
                                lstErrorMsg.Add("Task Due Date should be greater than Task Assign On Date");
                        }
                    }

                    if (String.IsNullOrEmpty(ddlTaskPriority.SelectedValue) || ddlTaskPriority.SelectedValue == "-1" || ddlTaskPriority.SelectedValue == "0")
                        lstErrorMsg.Add("Select Task Priority");
                    else
                        formValidateSuccess = true;

                    if (lstErrorMsg.Count > 0)
                    {
                        formValidateSuccess = false;
                        showErrorMessages(lstErrorMsg, cvTaskTab);
                    }

                    #endregion                    

                    if (formValidateSuccess)
                    {
                        Cont_tbl_TaskInstance newTaskRecord = new Cont_tbl_TaskInstance();

                        newTaskRecord.ContractID = contractID;
                        newTaskRecord.IsActive = true;
                        newTaskRecord.TaskType = Convert.ToInt32(rbTaskType.SelectedValue);

                        newTaskRecord.TaskTitle = tbxTaskTitle.Text.Trim();
                        newTaskRecord.TaskDesc = tbxTaskDesc.Text.Trim();
                        newTaskRecord.AssignOn = DateTimeExtensions.GetDate(tbxTaskAssignDate.Text);
                        newTaskRecord.DueDate = DateTimeExtensions.GetDate(tbxTaskDueDate.Text);

                        if (!String.IsNullOrEmpty(ddlTaskPriority.SelectedValue))
                            newTaskRecord.PriorityID = Convert.ToInt32(ddlTaskPriority.SelectedValue);

                        if (tbxExpOutcome.Text != "")
                            newTaskRecord.ExpectedOutcome = tbxExpOutcome.Text;

                        if (tbxTaskRemark.Text != "")
                            newTaskRecord.Remark = tbxTaskRemark.Text.Trim();

                        newTaskRecord.CustomerID = customerID;
                        newTaskRecord.CreatedBy = AuthenticationHelper.UserID;
                        newTaskRecord.CreatedOn = DateTime.Now;
                        newTaskRecord.UpdatedBy = AuthenticationHelper.UserID;
                        newTaskRecord.UpdatedOn = DateTime.Now;

                        if (Convert.ToString(ViewState["TaskMode"]) == "0")
                        {
                            if (!ContractTaskManagement.ExistTaskTitle(tbxTaskTitle.Text.Trim(), contractID, 0, customerID))
                            {
                                taskID = ContractTaskManagement.CreateTask(newTaskRecord);
                                if (taskID > 0)
                                {
                                    saveSuccess = true;
                                    ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "New Task Created", true);
                                }
                            }
                            else
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Task with same title already exists";

                                tbxTaskTitle.Focus();
                                saveSuccess = false;
                                return;
                            }
                        }
                        else if (Convert.ToString(ViewState["TaskMode"]) == "1")
                        {
                            taskID = Convert.ToInt64(ViewState["TaskID"]);

                            if (!ContractTaskManagement.ExistTaskTitle(tbxTaskTitle.Text.Trim(), contractID, taskID, customerID))
                            {
                                newTaskRecord.ID = taskID;
                                newTaskRecord.ContractID = contractID;

                                saveSuccess = ContractTaskManagement.UpdateTask(newTaskRecord);

                                if (saveSuccess)
                                {
                                    ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskInstance", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Detail(s) Updated", true);
                                }
                            }
                            else
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Task with same title already exists";
                                cvTaskTab.CssClass = "alert alert-danger";
                                tbxTaskTitle.Focus();
                                saveSuccess = false;
                                return;
                            }
                        }

                        if (saveSuccess)
                        {
                            #region Share Documents
                            //Save Task Related Uploaded Documents
                            if (grdTaskContractDocuments.Rows.Count > 0)
                            {
                                saveSuccess = shareDocuments(grdTaskContractDocuments, contractID, taskID);
                            }
                            #endregion

                            #region Task-User-Assignment

                            if (lstBoxTaskUser.Items.Count > 0)
                            {
                                if (rbTaskType.SelectedValue != null || rbTaskType.SelectedValue != "")
                                {
                                    int assignedRoleID = 4;
                                    if (rbTaskType.SelectedValue == "6")
                                    {
                                        assignedRoleID = 6;
                                        foreach (ListItem eachApprover in lstBoxTaskUser.Items) //lstBoxApprover
                                        {
                                            if (eachApprover.Selected)
                                            {
                                                if (Convert.ToInt32(eachApprover.Value) != 0)
                                                    lstTaskUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachApprover.Value), assignedRoleID));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (ListItem eachUser in lstBoxTaskUser.Items)
                                        {
                                            if (eachUser.Selected)
                                            {
                                                if (Convert.ToInt32(eachUser.Value) != 0)
                                                    lstTaskUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(eachUser.Value), assignedRoleID));
                                            }
                                        }
                                    }

                                    if (lstTaskUserMapping.Count > 0)
                                    {
                                        //Add Logged-in User As Performer
                                        //lstTaskUserMapping.Add(new Tuple<int, int>(Convert.ToInt32(AuthenticationHelper.UserID), 3));

                                        List<Cont_tbl_TaskUserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_TaskUserAssignment>();

                                        lstTaskUserMapping.ForEach(eachUser =>
                                        {
                                            Cont_tbl_TaskUserAssignment newAssignment = new Cont_tbl_TaskUserAssignment()
                                            {
                                                CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),

                                                ContractID = contractID,
                                                TaskID = taskID,

                                                UserID = Convert.ToInt32(eachUser.Item1),
                                                RoleID = Convert.ToInt32(eachUser.Item2),

                                                IsActive = true,
                                                LinkCreatedOn = DateTime.Now,
                                                URLExpired = false,

                                                CreatedBy = AuthenticationHelper.UserID,
                                                CreatedOn = DateTime.Now,
                                                UpdatedBy = AuthenticationHelper.UserID,
                                                UpdatedOn = DateTime.Now
                                            };

                                            string checkSum = Util.CalculateMD5Hash(contractID.ToString() + taskID.ToString() + newAssignment.UserID.ToString() + assignedRoleID.ToString());

                                            newAssignment.AccessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]) +
                                                                      "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?" +
                                                                      "A=" + CryptographyManagement.Encrypt(contractID.ToString()) +
                                                                      "&B=" + CryptographyManagement.Encrypt(taskID.ToString()) +
                                                                      "&C=" + CryptographyManagement.Encrypt(newAssignment.UserID.ToString()) +
                                                                      "&D=" + CryptographyManagement.Encrypt(assignedRoleID.ToString()) +
                                                                      "&Checksum=" + checkSum;

                                            lstUserAssignmentRecord_ToSave.Add(newAssignment);
                                        });

                                        var existingTaskUserAssignments = ContractTaskManagement.GetContractTaskUserAssignment_RoleWise(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, taskID, assignedRoleID);

                                        var assignedUserIDs = existingTaskUserAssignments.Select(row => row.UserID).ToList();
                                        var CurrentUserIDs = lstUserAssignmentRecord_ToSave.Select(row => row.UserID).ToList();

                                        bool matchSuccess = false;

                                        if (assignedUserIDs.Count != CurrentUserIDs.Count)
                                        {
                                            matchSuccess = false;
                                        }
                                        else
                                        {
                                            matchSuccess = assignedUserIDs.Except(CurrentUserIDs).ToList().Count > 0 ? false : true;
                                        }

                                        if (!matchSuccess)
                                        {
                                            ContractTaskManagement.DeActive_TaskUserAssignments(taskID, contractID, AuthenticationHelper.UserID);

                                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                            {
                                                saveSuccess = ContractTaskManagement.CreateUpdate_TaskUserAssignments(lstUserAssignmentRecord_ToSave);

                                                if (saveSuccess)
                                                {
                                                    if (Convert.ToString(ViewState["TaskMode"]) == "0")
                                                        ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskUserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task User Assignment(s) Created", true);
                                                    else if (Convert.ToString(ViewState["TaskMode"]) == "1")
                                                        ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskUserAssignment", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task User Assignment(s) Updated", true);
                                                }
                                            }

                                            if (saveSuccess)
                                            {
                                                int statusID = 11; //Task-Open
                                                string taskComments = string.Empty;

                                                if (tbxTaskRemark.Text != "")
                                                    taskComments = tbxTaskRemark.Text.Trim();
                                                else
                                                    taskComments = "New Task Assigned";

                                                saveSuccess = ContractTaskManagement.CreateUpdate_TaskTrasactions(lstUserAssignmentRecord_ToSave, AuthenticationHelper.UserID, statusID, taskComments);

                                                //Move Contract Status 
                                                if (saveSuccess)
                                                {
                                                    int newStatusID = 0;

                                                    if (newTaskRecord.TaskType == 4) //Review Task
                                                    {
                                                        newStatusID = 2; //Pending Review                                       
                                                    }
                                                    else if (newTaskRecord.TaskType == 6) //Approval Task
                                                    {
                                                        newStatusID = 4;  //Pending Approval                                        
                                                    }

                                                    if (newStatusID != 0)
                                                        saveSuccess = ContractTaskManagement.UpdateContractStatus(customerID, contractID, taskID, "Open", newStatusID);

                                                    if (saveSuccess)
                                                    {
                                                        ddlContractStatus.ClearSelection();

                                                        if (ddlContractStatus.Items.FindByValue(newStatusID.ToString()) != null)
                                                            ddlContractStatus.Items.FindByValue(newStatusID.ToString()).Selected = true;
                                                    }
                                                }

                                                #region Send Email to Assigned User(s)

                                                int userID = AuthenticationHelper.UserID;

                                                if (saveSuccess)
                                                {
                                                    bool sendSuccess = false;
                                                    sendSuccess = SendEmail_TaskAssignedUsers(customerID, contractID, newTaskRecord, lstTaskUserMapping, "Task Assignment ");

                                                    //Task<bool> task = new Task<bool>(() => SendEmail_TaskAssignedUsers(customerID, userID, contractID, newTaskRecord, lstTaskUserMapping, "Task Assignment"));
                                                    //task.Start();

                                                    //sendSuccess = await task;

                                                    //if (sendSuccess)
                                                    //{
                                                    //    cvTaskTab.IsValid = false;
                                                    //    cvTaskTab.ErrorMessage = "An Email containing task detail and access URL to provide response sent to assignee.";
                                                    //    vsTaskTab.CssClass = "alert alert-success";
                                                    //}
                                                }
                                                #endregion
                                            }
                                        }
                                        else
                                            saveSuccess = true;

                                        lstUserAssignmentRecord_ToSave.Clear();
                                        lstUserAssignmentRecord_ToSave = null;
                                    }
                                }
                            }

                            #endregion                            
                        }

                        if (saveSuccess)
                        {
                            clearTaskControls();
                            BindContractTasks_Paging(customerID, contractID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);

                            HideShowTaskDiv(false);

                            cvTaskTab.IsValid = false;
                            cvTaskTab.ErrorMessage = "Task Save Successfully. An Email containing task detail and access URL to provide response sent to assignee.";
                            vsTaskTab.CssClass = "alert alert-success";
                        }
                        else
                        {
                            cvTaskTab.IsValid = false;
                            cvTaskTab.ErrorMessage = "Something went wrong, Please try again.";
                            vsTaskTab.CssClass = "alert alert-danger";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        async Task<bool> SendEmail_TaskAssignedUsers_Async(int customerID, long ContractID, Cont_tbl_TaskInstance newTaskRecord, List<Tuple<int, int>> lstTaskUserMapping, string reminderType)
        {
            long TaskID = newTaskRecord.ID;
            bool saveSuccess = false;
            string accessURL = string.Empty;

            if (newTaskRecord != null)
            {
                string taskAssignedBy = ContractUserManagement.GetUserNameByUserID(newTaskRecord.CreatedBy);

                List<Cont_tbl_ReminderLog> lstReminderLogs = new List<Cont_tbl_ReminderLog>();

                foreach (var eachAssignedUser in lstTaskUserMapping)
                {
                    if (eachAssignedUser.Item2 != 3)
                    {
                        accessURL = string.Empty;

                        if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(eachAssignedUser.Item1), customerID))
                        {
                            accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                                + "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?TaskID=" + CryptographyManagement.Encrypt(TaskID.ToString())
                                + "&CID=" + CryptographyManagement.Encrypt(ContractID.ToString());
                        }
                        else
                        {
                            accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        }

                        saveSuccess = SendTaskAssignmentMail(newTaskRecord, accessURL, customerID, ContractID, Convert.ToInt32(eachAssignedUser.Item1), taskAssignedBy);

                        if (saveSuccess)
                        {
                            bool sendSuccess = ContractTaskManagement.UpdateTaskAccessURL_Contract(ContractID, TaskID, Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(eachAssignedUser.Item1), Convert.ToInt32(eachAssignedUser.Item2), accessURL);

                            if (sendSuccess)
                            {
                                //Save Reminder Log
                                Cont_tbl_ReminderLog objRemind = new Cont_tbl_ReminderLog()
                                {
                                    UserID = Convert.ToInt32(eachAssignedUser.Item1),
                                    Role = Convert.ToInt32(eachAssignedUser.Item2),
                                    TriggerType = reminderType,
                                    TriggerDate = DateTime.Now
                                };

                                lstReminderLogs.Add(objRemind);
                            }
                        }
                    }
                }

                if (lstReminderLogs.Count > 0)
                {
                    if (ContractManagement.Save_ContractReminderLogs(lstReminderLogs))
                    {
                        ContractManagement.CreateAuditLog("C", ContractID, "Cont_tbl_ReminderLog", "Sent", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, reminderType + "Email sent to " + lstReminderLogs.Count + " User(s)", true);
                    }
                }
            }

            return saveSuccess;
        }

        private bool SendEmail_TaskAssignedUsers(int customerID, long ContractID, Cont_tbl_TaskInstance newTaskRecord, List<Tuple<int, int>> lstTaskUserMapping, string reminderType)
        {
            long TaskID = newTaskRecord.ID;
            bool saveSuccess = false;
            string accessURL = string.Empty;

            if (newTaskRecord != null)
            {
                string taskAssignedBy = ContractUserManagement.GetUserNameByUserID(newTaskRecord.CreatedBy);

                List<Cont_tbl_ReminderLog> lstReminderLogs = new List<Cont_tbl_ReminderLog>();

                foreach (var eachAssignedUser in lstTaskUserMapping)
                {
                    if (eachAssignedUser.Item2 != 3)
                    {
                        accessURL = string.Empty;

                        //if (UserManagement.GetUserTypeInternalExternalByUserID(Convert.ToInt32(eachAssignedUser.Item1), customerID))
                        //{
                        //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"])
                        //        + "/ContractProduct/aspxPages/ContractTaskDetailsPage.aspx?TaskID=" + CryptographyManagement.Encrypt(TaskID.ToString())
                        //        + "&CID=" + CryptographyManagement.Encrypt(ContractID.ToString());
                        //}
                        //else
                        //{
                        //    accessURL = Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]);
                        //}

                        saveSuccess = SendTaskAssignmentMail(newTaskRecord, accessURL, customerID, ContractID, Convert.ToInt32(eachAssignedUser.Item1), taskAssignedBy);

                        if (saveSuccess)
                        {
                            //bool sendSuccess = ContractTaskManagement.UpdateTaskAccessURL_Contract(ContractID, TaskID, Convert.ToInt32(AuthenticationHelper.UserID), Convert.ToInt32(eachAssignedUser.Item1), Convert.ToInt32(eachAssignedUser.Item2), accessURL);

                            //if (sendSuccess)
                            //{
                            //Save Reminder Log
                            Cont_tbl_ReminderLog objRemind = new Cont_tbl_ReminderLog()
                            {
                                UserID = Convert.ToInt32(eachAssignedUser.Item1),
                                Role = Convert.ToInt32(eachAssignedUser.Item2),
                                TriggerType = reminderType,
                                TriggerDate = DateTime.Now
                            };

                            lstReminderLogs.Add(objRemind);
                            //}
                        }
                    }
                }

                if (lstReminderLogs.Count > 0)
                {
                    if (ContractManagement.Save_ContractReminderLogs(lstReminderLogs))
                    {
                        ContractManagement.CreateAuditLog("C", ContractID, "Cont_tbl_ReminderLog", "Sent", customerID, AuthenticationHelper.UserID, reminderType + "Email sent to " + lstReminderLogs.Count + " User(s)", true);
                    }
                }
            }

            return saveSuccess;
        }

        public bool SendTaskAssignmentMail(Cont_tbl_TaskInstance taskRecord, string accessURL, int customerID, long ContractID, int assignedTo, string taskAssignedBy)
        {
            try
            {
                if (taskRecord != null)
                {
                    User User = UserManagement.GetByID(Convert.ToInt32(assignedTo));

                    if (User != null)
                    {
                        if (User.Email != null && User.Email != "")
                        {
                            string taskPriority = string.Empty;

                            if (taskRecord.PriorityID != 0)
                            {
                                if (taskRecord.PriorityID == 1)
                                    taskPriority = "High";
                                else if (taskRecord.PriorityID == 2)
                                    taskPriority = "Medium";
                                else if (taskRecord.PriorityID == 3)
                                    taskPriority = "Low";
                            }

                            string username = string.Format("{0} {1}", User.FirstName, User.LastName);

                            string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_Contract_TaskAssignment
                                                    .Replace("@User", username)
                                                    .Replace("@TaskTitle", taskRecord.TaskTitle)
                                                    .Replace("@TaskDesc", taskRecord.TaskDesc)
                                                    .Replace("@Priority", taskPriority)
                                                    .Replace("@DueDate", taskRecord.DueDate.ToString("dd-MM-yyyy"))
                                                    .Replace("@AssignedBy", taskAssignedBy.ToString())
                                                    .Replace("@Remark", taskRecord.Remark)
                                                    .Replace("@AccessURL", accessURL)
                                                    .Replace("@From", Convert.ToString(ConfigurationManager.AppSettings["ReplyEmailAddressName"]))
                                                    .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { User.Email }), null, null, "Contract Notification-Task Assigned", message);

                            return true;
                        }
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

        protected void grdTaskActivity_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (ViewState["ContractInstanceID"] != null)
                {
                    grdTaskActivity.PageIndex = e.NewPageIndex;
                    BindContractTasks_Paging(customerID, Convert.ToInt64(ViewState["CaseInstanceID"]), Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);

                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "gridPageIndexChanged", "gridPageIndexChanged();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdTaskActivity_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (e.CommandArgument != null)
                {
                    if (ViewState["ContractInstanceID"] != null)
                    {
                        bool txnSuccess = false;
                        long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                        long taskID = Convert.ToInt64(e.CommandArgument);

                        //Update Task Status to Closed and Expire URL
                        if (e.CommandName.Equals("CloseTask"))
                        {
                            var closeSuccess = ContractTaskManagement.UpdateTaskStatus(customerID, AuthenticationHelper.UserID, contractID, taskID, "Closed");

                            if (closeSuccess)
                            {
                                //Update URL Status to De-Active
                                ContractTaskManagement.UpdateURLExpired(customerID, contractID, taskID, false);

                                ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskTransaction", "Update", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Closed", true);

                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Task Closed Successfully";
                                vsTaskTab.CssClass = "alert alert-success";
                            }
                        }

                        else if (e.CommandName.Equals("EditTask"))
                        {
                            ViewState["TaskMode"] = "1";

                            ViewState["TaskID"] = taskID;

                            if (taskID != 0 && contractID != 0)
                            {
                                var taskDetailRecord = ContractTaskManagement.GetTaskDetailsByTaskID(contractID, taskID);
                                if (taskDetailRecord != null)
                                {
                                    tbxTaskTitle.Text = taskDetailRecord.TaskTitle;
                                    tbxTaskDesc.Text = taskDetailRecord.TaskDesc;

                                    if (taskDetailRecord.AssignOn != null)
                                        tbxTaskAssignDate.Text = taskDetailRecord.AssignOn.ToString("dd-MM-yyyy");

                                    if (taskDetailRecord.DueDate != null)
                                        tbxTaskDueDate.Text = taskDetailRecord.DueDate.ToString("dd-MM-yyyy");

                                    ddlTaskPriority.ClearSelection();
                                    if (ddlTaskPriority.Items.FindByValue(taskDetailRecord.PriorityID.ToString()) != null)
                                        ddlTaskPriority.Items.FindByValue(taskDetailRecord.PriorityID.ToString()).Selected = true;

                                    tbxExpOutcome.Text = taskDetailRecord.ExpectedOutcome;
                                    tbxTaskRemark.Text = taskDetailRecord.Remark;

                                    var lstTaskAssignedUsers = ContractTaskManagement.GetContractTaskUserAssignment(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, taskID);

                                    if (lstTaskAssignedUsers.Count > 0)
                                    {
                                        if (lstBoxTaskUser.Items.Count > 0)
                                        {
                                            lstBoxTaskUser.ClearSelection();
                                        }

                                        if (lstTaskAssignedUsers.Count > 0)
                                        {
                                            foreach (var userID in lstTaskAssignedUsers)
                                            {
                                                if (lstBoxTaskUser.Items.FindByValue(userID.UserID.ToString()) != null)
                                                    lstBoxTaskUser.Items.FindByValue(userID.UserID.ToString()).Selected = true;
                                            }
                                        }
                                    }

                                    BindTaskDocuments_All(grdTaskContractDocuments, contractID, taskID);

                                    BindTaskResponses(contractID, taskID);
                                    HideShowTaskDiv(true);
                                    toggleTextSaveButton_AddEditTask(false);

                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideShowTask_Script", "HideShowTaskDiv('true');", true);
                                }
                            }
                        }
                        else if (e.CommandName.Equals("DeleteTask"))
                        {
                            txnSuccess = ContractTaskManagement.DeleteTask(contractID, taskID, AuthenticationHelper.UserID, Convert.ToInt32(AuthenticationHelper.CustomerID));

                            if (txnSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_TaskInstance", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Task Deleted", true);

                                //Re-Bind Task
                                BindContractTasks_Paging(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, Convert.ToInt32(grdTaskActivity.PageSize), Convert.ToInt32(grdTaskActivity.PageIndex) + 1, grdTaskActivity);

                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Task Deleted Successfully";
                                vsTaskTab.CssClass = "alert alert-success";
                            }
                        }
                        else if (e.CommandName.Equals("TaskReminder"))
                        {
                            bool sendSuccess = false;

                            var newTaskRecord = ContractTaskManagement.GetTaskDetailsByTaskID(contractID, taskID);

                            if (newTaskRecord != null)
                            {
                                var TaskUserAssignment = ContractTaskManagement.GetContractTaskUserAssignment(Convert.ToInt32(AuthenticationHelper.CustomerID), contractID, taskID).ToList();

                                if (TaskUserAssignment.Count > 0)
                                {
                                    //Update URL Status to Active
                                    ContractTaskManagement.UpdateURLExpired(customerID, contractID, taskID, true);

                                    List<Tuple<int, int>> lstTaskUserMapping = new List<Tuple<int, int>>();
                                    foreach (var eachAssignedUser in TaskUserAssignment)
                                    {
                                        lstTaskUserMapping.Add(new Tuple<int, int>(eachAssignedUser.UserID, eachAssignedUser.RoleID));
                                    }

                                    sendSuccess = SendEmail_TaskAssignedUsers(customerID, contractID, newTaskRecord, lstTaskUserMapping, "Task Reminder");
                                }

                                if (sendSuccess)
                                {
                                    cvTaskTab.IsValid = false;
                                    cvTaskTab.ErrorMessage = "An Email containing Task detail and access URL to provide response sent to assignee.";
                                    vsTaskTab.CssClass = "alert alert-success";
                                }
                                else
                                {
                                    cvTaskTab.IsValid = false;
                                    cvTaskTab.ErrorMessage = "Something went wrong, Please try again.";
                                    vsTaskTab.CssClass = "alert alert-danger";
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdEditTaskDocuments_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long contFileID = 0;

                    if (e.CommandName.Equals("DownloadTaskEditDocument"))
                    {
                        contFileID = Convert.ToInt64(e.CommandArgument);

                        if (contFileID != 0)
                        {
                            bool downloadSuccess = DownloadContractDocument(contFileID);

                            if (!downloadSuccess)
                            {
                                cvTaskTab.IsValid = false;
                                cvTaskTab.ErrorMessage = "Something went wrong, Please try again.";
                                vsTaskTab.CssClass = "alert alert-danger";
                            }
                        }
                    }
                    else if (e.CommandName.Equals("ViewTaskEditDocument"))
                    {
                        contFileID = Convert.ToInt64(e.CommandArgument);

                        if (contFileID != 0)
                        {
                            bool downloadSuccess = ViewContractDocument(contFileID);

                            if (!downloadSuccess)
                            {
                                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "HideShowTask_Script", "HideShowTaskDiv('true');", true);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenDocviewer('" + DocumentPath + "');", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                cvTaskTab.IsValid = false;
                cvTaskTab.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void HideShowTaskDiv(bool flag)
        {
            if (flag)
            {
                pnlAddNewTask.Visible = true;
                pnlTaskList.Visible = false;
            }
            else if (!flag)
            {
                pnlAddNewTask.Visible = false;
                pnlTaskList.Visible = true;
            }

            //upContractTaskActivity.Update();
            //upAddEditTask.Update();
        }

        protected void lnkAddNewTask_Click(object sender, EventArgs e)
        {
            try
            {
                HideShowTaskDiv(true);
                clearTaskControls();
                clearTaskResponseLog();

                toggleTextSaveButton_AddEditTask(true);

                tbxTaskAssignDate.Text = DateTime.Now.ToString("dd-MM-yyyy");

                BindContractDocuments_All(grdTaskContractDocuments);

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Task_CheckAllDocument", "DefaultSelectAllTaskDocument();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion

        #region Contract Audit Log   

        protected void gvContractAuditLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    gvContractAuditLog.PageIndex = e.NewPageIndex;

                    BindContractAuditLogs();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #endregion


        protected void lnkBtnUploadDocument_Click(object sender, EventArgs e)
        {
            var contractID = CryptographyManagement.Encrypt(ViewState["ContractInstanceID"].ToString());
            var FlagID = 1;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenUploadDocumentPopup('" + contractID + "','" + FlagID + "');", true);
        }

        protected void lnkBtnAddNewTaskDoc_Click(object sender, EventArgs e)
        {
            var contractID = CryptographyManagement.Encrypt(ViewState["ContractInstanceID"].ToString());
            var FlagID = 2;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenUploadDocumentPopup('" + contractID + "','" + FlagID + "');", true);
        }

        protected void btnSaveContDetail_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long contractID = Convert.ToInt64(ViewState["ContractInstanceID"]);

                var ExistingRecord = ContractManagement.ExistsContractDetails(contractID, customerID);

                if (ExistingRecord != null)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        Cont_tbl_ContractDetail prevRecord = (from row in entities.Cont_tbl_ContractDetail
                                                              where row.ID == ExistingRecord.ID
                                                              select row).FirstOrDefault();
                        if (prevRecord != null)
                        {
                            prevRecord.IsActive = true;
                            prevRecord.Terms = txtTerms.Text.ToString();
                            prevRecord.Termination = txtTermination.Text.ToString();
                            prevRecord.Misc = txtMisc.Text.ToString();
                            prevRecord.License = txtLicense.Text.ToString();
                            prevRecord.Warranties = txtWarranties.Text.ToString();
                            prevRecord.Guarantees = txtGuarantees.Text.ToString();
                            prevRecord.Confidential = txtConfidential.Text.ToString();
                            prevRecord.Ownership = txtOwnership.Text.ToString();
                            prevRecord.Indemnification = txtIndemnification.Text.ToString();
                            prevRecord.General = txtGeneral.Text.ToString();
                            prevRecord.Timelines = txtTimelines.Text.ToString();
                            prevRecord.Escalation = txtEscalation.Text.ToString();
                            prevRecord.Milestone = txtMilestones.Text.ToString();
                            prevRecord.Others = txtOthers.Text.ToString();
                            prevRecord.UpdatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                            prevRecord.UpdatedOn = DateTime.Now;

                            entities.SaveChanges();

                            saveSuccess = true;
                        }
                    }
                }
                else
                {
                    Cont_tbl_ContractDetail newContRecord = new Cont_tbl_ContractDetail();

                    newContRecord.IsActive = true;
                    newContRecord.CustomerID = customerID;
                    newContRecord.ContractID = contractID;
                    newContRecord.Terms = txtTerms.Text.ToString();
                    newContRecord.Termination = txtTermination.Text.ToString();
                    newContRecord.Misc = txtMisc.Text.ToString();
                    newContRecord.License = txtLicense.Text.ToString();
                    newContRecord.Warranties = txtWarranties.Text.ToString();
                    newContRecord.Guarantees = txtGuarantees.Text.ToString();
                    newContRecord.Confidential = txtConfidential.Text.ToString();
                    newContRecord.Ownership = txtOwnership.Text.ToString();
                    newContRecord.Indemnification = txtIndemnification.Text.ToString();
                    newContRecord.General = txtGeneral.Text.ToString();
                    newContRecord.Timelines = txtTimelines.Text.ToString();
                    newContRecord.Escalation = txtEscalation.Text.ToString();
                    newContRecord.Milestone = txtMilestones.Text.ToString();
                    newContRecord.Others = txtOthers.Text.ToString();

                    newContRecord.CreatedBy = Convert.ToInt32(AuthenticationHelper.UserID);
                    newContRecord.CreatedOn = DateTime.Now;

                    saveSuccess = ContractManagement.CreateContractDetails(newContRecord);
                }

                if (saveSuccess)
                {
                    cvContractDetails.IsValid = false;
                    cvContractDetails.ErrorMessage = "Contract Detail(s) Save Successfully";
                    vsContractDetails.CssClass = "alert alert-success";
                }
                else
                {
                    cvContractDetails.IsValid = false;
                    cvContractDetails.ErrorMessage = "Something went wrong, Please try again";
                    vsContractDetails.CssClass = "alert alert-danger";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSteps(string[] items, System.Web.UI.WebControls.BulletedList ListName)
        {
            ListName.Items.Clear();
            ListName.Attributes["data-progtrckr-steps"] = items.Length.ToString();

            for (int i = 0; i < items.Length; i++)
            {
                ListName.Items.Add(new ListItem(items[i]));
            }
        }

        protected void SetProgress(long current, System.Web.UI.WebControls.BulletedList ListName)
        {
            for (int i = 0; i < ListName.Items.Count; i++)
            {
                ListName.Items[i].Attributes["class"] =
                    (i < current) ? "progtrckr-done" : (i == current) ? "progtrckr-current" : "progtrckr-todo";

                if (i == 3 && i > current)
                    ListName.Items[i].Attributes["class"] = "progtrckr-todo-closed";
            }
            if (current == 4)
                ListName.Items[Convert.ToInt32(current) - 1].Attributes["class"] = "progtrckr-closed";
        }

        public bool Save_ContractStatus(long contractID)
        {
            bool saveSuccess = false;
            try
            {
                #region Contract Status Transaction

                long newStatusID = 0;

                if (ViewState["ContractStatusID"] != null)
                {
                    long prevStatusID = Convert.ToInt64(ViewState["ContractStatusID"]);

                    if (prevStatusID != Convert.ToInt64(ddlContractStatus.SelectedValue))
                    {
                        newStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                    }
                }
                else
                {
                    newStatusID = Convert.ToInt64(ddlContractStatus.SelectedValue);
                }

                if (newStatusID != 0)
                {
                    Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                    {
                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        ContractID = contractID,
                        StatusID = newStatusID,
                        StatusChangeOn = DateTime.Now,
                        IsActive = true,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        UpdatedBy = AuthenticationHelper.UserID,
                        UpdatedOn = DateTime.Now,
                    };

                    saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                    if (saveSuccess)
                    {
                        ViewState["ContractStatusID"] = Convert.ToInt64(ddlContractStatus.SelectedValue);
                        ContractManagement.CreateAuditLog("C", contractID, "Cont_tbl_ContractStatusTransaction", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Status Changed", true);
                    }
                }

                return saveSuccess;

                #endregion
            }
            catch (Exception ex)
            {
                return saveSuccess;
            }
        }

        protected void btnRenewContract_Click(object sender, EventArgs e)
        {
            bool renewSuccess = false;
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long oldContractID = Convert.ToInt64(ViewState["ContractInstanceID"]);
                    long newContractID = 0;

                    #region Contract Status Transaction - Renewed - Current Contract 

                    renewSuccess = Save_ContractStatus(oldContractID);

                    #endregion

                    if (renewSuccess)
                    {
                        renewSuccess = Save_ContractToContract_Renew(oldContractID, out newContractID);
                    }

                    if (renewSuccess)
                    {
                        cvRenewContract.IsValid = false;
                        cvRenewContract.ErrorMessage = "Contract Renewed Successfully";
                        vsRenewContract.CssClass = "alert alert-success";
                    }
                    else
                    {
                        cvRenewContract.IsValid = false;
                        cvRenewContract.ErrorMessage = "Something went wrong, Please try again";
                        vsRenewContract.CssClass = "alert alert-danger";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool Save_ContractToContract_Renew(long oldContractID, out long newContractID_Out)
        {
            bool saveSuccess = false;
            newContractID_Out = 0;
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var oldContractRecord = ContractManagement.GetContractDetailsByContractID(customerID, oldContractID);

                if (oldContractRecord != null)
                {
                    long newContractID = 0;

                    #region Save New Contract Record

                    Cont_tbl_ContractInstance newContractRecord = new Cont_tbl_ContractInstance()
                    {
                        ContractType = oldContractRecord.ContractType,

                        CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                        IsDeleted = false,

                        ContractNo = "Renewed-" + oldContractRecord.ContractNo,
                        ContractTitle = "Renewed-" + oldContractRecord.ContractTitle,
                        ContractDetailDesc = oldContractRecord.ContractDetailDesc,
                        CustomerBranchID = oldContractRecord.CustomerBranchID,
                        DepartmentID = oldContractRecord.DepartmentID,
                        ContractTypeID = oldContractRecord.ContractTypeID,
                        ContractSubTypeID = oldContractRecord.ContractSubTypeID,
                        //ProposalDate = oldContractRecord.ProposalDate,
                        //AgreementDate= oldContractRecord.AgreementDate,
                        EffectiveDate = oldContractRecord.EffectiveDate,
                        //ReviewDate = oldContractRecord.ReviewDate,
                        ExpirationDate = oldContractRecord.ExpirationDate,

                        NoticeTermNumber = oldContractRecord.NoticeTermNumber,
                        NoticeTermType = oldContractRecord.NoticeTermType,
                        PaymentTermID = oldContractRecord.PaymentTermID,
                        ContractAmt = oldContractRecord.ContractAmt,
                        AddNewClause = oldContractRecord.AddNewClause,
                        CreatedBy = AuthenticationHelper.UserID,
                        UpdatedBy = AuthenticationHelper.UserID
                    };

                    newContractID = ContractManagement.CreateContract(newContractRecord);

                    if (newContractID > 0)
                    {
                        ContractManagement.CreateAuditLog("C", oldContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Renewed and New Contract Created", true);
                        ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_ContractInstance", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Created from Existing Contract", true);
                        saveSuccess = true;
                    }

                    #endregion

                    if (saveSuccess)
                    {
                        newContractID_Out = newContractID;

                        #region Contract Mapping
                        Cont_tbl_ContractToContractMapping objContractMapping = new Cont_tbl_ContractToContractMapping()
                        {
                            OldContractID = oldContractID,
                            NewContractID = newContractID,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,

                            UpdatedBy = AuthenticationHelper.UserID,
                            UpdatedOn = DateTime.Now,
                        };

                        saveSuccess = ContractManagement.CreateUpdate_ContractToContractMapping(objContractMapping);

                        #endregion

                        #region Contract Status Transaction - Draft

                        Cont_tbl_ContractStatusTransaction newStatusRecord = new Cont_tbl_ContractStatusTransaction()
                        {
                            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID),
                            ContractID = newContractID,
                            StatusID = Convert.ToInt64(1), //Draft
                            StatusChangeOn = DateTime.Now,
                            IsActive = true,
                            CreatedBy = AuthenticationHelper.UserID,
                            CreatedOn = DateTime.Now,
                            UpdatedBy = AuthenticationHelper.UserID,
                            UpdatedOn = DateTime.Now,
                        };

                        if (!ContractManagement.Exist_ContractStatusTransaction(newStatusRecord))
                            saveSuccess = ContractManagement.CreateContractStatusTransaction(newStatusRecord);

                        #endregion

                        #region Vendor Mapping

                        var lstVendorMapping = ContractManagement.GetVendorMapping(oldContractID);

                        if (lstVendorMapping.Count > 0)
                        {
                            List<Cont_tbl_VendorMapping> lstVendorMapping_ToSave = new List<Cont_tbl_VendorMapping>();

                            lstVendorMapping.ForEach(EachVendor =>
                            {
                                Cont_tbl_VendorMapping _vendorMappingRecord = new Cont_tbl_VendorMapping()
                                {
                                    ContractID = newContractID,
                                    VendorID = EachVendor,
                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                };

                                lstVendorMapping_ToSave.Add(_vendorMappingRecord);
                            });

                            saveSuccess = ContractManagement.CreateUpdate_VendorMapping(lstVendorMapping_ToSave);
                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_VendorMapping", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Vendor Mapping Created", true);
                            }

                            //Refresh List
                            lstVendorMapping_ToSave.Clear();
                            lstVendorMapping_ToSave = null;

                            lstVendorMapping.Clear();
                            lstVendorMapping = null;
                        }

                        #endregion

                        #region Custom Field(s)

                        var existingCustomFields = ContractManagement.GetCustomsFieldsContractWise_All(Convert.ToInt32(AuthenticationHelper.CustomerID), oldContractID, oldContractRecord.ContractTypeID);

                        if (existingCustomFields.Count > 0)
                        {
                            List<Cont_tbl_CustomFieldValue> lstObjParameters = new List<Cont_tbl_CustomFieldValue>();

                            existingCustomFields.ForEach(EachCustomField =>
                            {
                                Cont_tbl_CustomFieldValue ObjParameter = new Cont_tbl_CustomFieldValue()
                                {
                                    ContractID = newContractID,

                                    LabelID = EachCustomField.LableID,
                                    LabelValue = EachCustomField.LabelValue,

                                    IsDeleted = false,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    CreatedOn = DateTime.Now,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                    UpdatedOn = DateTime.Now
                                };

                                lstObjParameters.Add(ObjParameter);
                            });

                            saveSuccess = ContractManagement.CreateUpdate_CustomsFields(lstObjParameters);

                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_CustomFieldValue", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Custom Parameter(s) Added", true);
                            }
                        }

                        #endregion

                        #region User Assignment                           

                        var lstContractUserMapping = ContractManagement.GetContractUserAssignment_All(oldContractID);

                        if (lstContractUserMapping.Count > 0)
                        {
                            List<Cont_tbl_UserAssignment> lstUserAssignmentRecord_ToSave = new List<Cont_tbl_UserAssignment>();

                            lstContractUserMapping.ForEach(eachUser =>
                            {
                                Cont_tbl_UserAssignment newAssignment = new Cont_tbl_UserAssignment()
                                {
                                    AssignmentType = 1,
                                    ContractID = newContractID,

                                    UserID = Convert.ToInt32(eachUser.UserID),
                                    RoleID = Convert.ToInt32(eachUser.RoleID),

                                    IsActive = true,
                                    CreatedBy = AuthenticationHelper.UserID,
                                    UpdatedBy = AuthenticationHelper.UserID,
                                };

                                lstUserAssignmentRecord_ToSave.Add(newAssignment);
                            });

                            if (lstUserAssignmentRecord_ToSave.Count > 0)
                                saveSuccess = ContractManagement.CreateUpdate_ContractUserAssignments(lstUserAssignmentRecord_ToSave);

                            if (saveSuccess)
                            {
                                ContractManagement.CreateAuditLog("C", newContractID, "Cont_tbl_UserAssignment", "Add", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Contract Owner(s) Created", true);
                            }

                            lstUserAssignmentRecord_ToSave.Clear();
                            lstUserAssignmentRecord_ToSave = null;
                        }

                        #endregion
                    }
                }
                return saveSuccess;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return saveSuccess;
            }
        }

        protected void btnApplyFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                BindContractAuditLogs();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearFilter_AuditLog_Click(object sender, EventArgs e)
        {
            try
            {
                ddlUsers_AuditLog.ClearSelection();

                txtFromDate_AuditLog.Text = string.Empty;
                txtToDate_AuditLog.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void grdContractHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("ViewContractPopup"))
                    {
                        long contractID = Convert.ToInt64(e.CommandArgument);

                        if (contractID != 0)
                        {
                            string HistoryFlag = " true";
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "OpenContractHistoryPopup(" + contractID + "," + HistoryFlag + ");", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (ViewState["ContractInstanceID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    grdContractHistory.PageIndex = e.NewPageIndex;

                    BindContractHistory(customerID, Convert.ToInt64(ViewState["ContractInstanceID"]));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void grdTaskContractDocuments_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdTaskContractDocuments.PageIndex = e.NewPageIndex;
            BindContractDocuments_All(grdTaskContractDocuments);
        }
    }
}