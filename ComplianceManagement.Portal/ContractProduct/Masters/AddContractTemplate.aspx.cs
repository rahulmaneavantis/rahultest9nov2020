﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddContractTemplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                divsuccessmsgaCTem.Visible = false;
                if (!IsPostBack)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    long templateID = 0;

                    if (!string.IsNullOrEmpty(Request.QueryString["accessID"]))
                    {
                        templateID = Convert.ToInt64(Request.QueryString["accessID"]);

                        var objRecord = ContractTemplateManagement.GetTemplateDetailsByID(templateID, customerID);

                        if (objRecord != null)
                        {
                            tbxTemplateName.Text = objRecord.TemplateName;
                            tbxVersion.Text = objRecord.Version;

                            ViewState["Mode"] = 1;
                            ViewState["TemplateID"] = templateID;
                        }
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                        templateID = 0;
                    }

                    lnkBtnTemplate_Click(sender, e);

                    BindSections_All(grdTemplateSection, templateID);
                }

                if (ViewState["TemplateID"] != null)
                {
                    btnExportWord1.Enabled = true;
                    btnExportWord2.Enabled = true;
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', true);", lnkBtnImportSection.ClientID), true);
                }
                else
                {
                    btnExportWord1.Enabled = false;
                    btnExportWord2.Enabled = false;
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', false);", lnkBtnImportSection.ClientID), true);
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public static void DisableLinkButton(LinkButton linkButton)
        {
            linkButton.Attributes.Remove("href");
            linkButton.Attributes.CssStyle[HtmlTextWriterStyle.Color] = "gray";
            linkButton.Attributes.CssStyle[HtmlTextWriterStyle.Cursor] = "default";
            if (linkButton.Enabled != false)
            {
                linkButton.Enabled = false;
            }

            if (linkButton.OnClientClick != null)
            {
                linkButton.OnClientClick = null;
            }
        }
        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string sectionID = null;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal('" + sectionID + "','0');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnClearTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                tbxTemplateName.Text = string.Empty;
                tbxVersion.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnExportWord_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TemplateID"] != null)
                {
                    long templateID = Convert.ToInt64(ViewState["TemplateID"]);

                    if (templateID != 0)
                    {
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var lstSections = (from row in entities.Cont_SP_GetTemplateSectionDetails(customerID, templateID)
                                               select row).ToList();
                            if (lstSections.Count > 0)
                            {
                                //GenerateTemplateDocx(lstSections);
                                ExporttoWord(lstSections);
                            }
                            else
                            {
                                cvAddEditTemplate.IsValid = false;
                                cvAddEditTemplate.ErrorMessage = "No Section(s) Mapped to Selected Template";
                            }
                        }
                    }
                }
                else
                {
                    cvAddEditTemplate.IsValid = false;
                    cvAddEditTemplate.ErrorMessage = "No Section Mapped to Selected Template";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClearSections_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                {
                    if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        TextBox txtOrder = (TextBox)grdTemplateSection.Rows[i].FindControl("txtOrder");
                        if (txtOrder != null)
                            txtOrder.Text = string.Empty;
                        //txtOrder.Text = "0";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                bool validateSuccess = false;
                bool saveSuccess = false;
                List<string> lstErrorMsg = new List<string>();

                #region Validation

                if (!string.IsNullOrEmpty(tbxTemplateName.Text))
                    validateSuccess = true;
                else
                    lstErrorMsg.Add("Please Enter Template Name");

                //if (!string.IsNullOrEmpty(tbxVersion.Text))
                //    validateSuccess = true;
                //else
                //    lstErrorMsg.Add("Required Version");

                int selectedSectionCount = 0;
                foreach (GridViewRow eachGridRow in grdTemplateSection.Rows)
                {
                    if (eachGridRow.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (CheckBox)eachGridRow.FindControl("chkRow");

                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                                selectedSectionCount++;
                        }
                    }
                }

                if (selectedSectionCount == 0)
                    lstErrorMsg.Add("Provide Select at lease one section to include in the template");
                else if (selectedSectionCount > 0)
                    validateSuccess = true;

                if (lstErrorMsg.Count > 0)
                {
                    validateSuccess = false;
                    ShowErrorMessages(lstErrorMsg, cvAddEditTemplate);
                }

                #endregion

                if (validateSuccess)
                {
                    Cont_tbl_TemplateMaster _objTemplate = new Cont_tbl_TemplateMaster()
                    {
                        TemplateName = tbxTemplateName.Text,
                        Version = tbxVersion.Text,

                        CustomerID = customerID,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsDeleted = false
                    };

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (ContractTemplateManagement.ExistsTemplate(_objTemplate, customerID, 0))
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Template with Same Name and Version already exists";
                        }
                        else
                        {
                            long newTemplateID = 0;

                            newTemplateID = ContractTemplateManagement.CreateTemplate(_objTemplate);

                            if (newTemplateID > 0)
                            {
                                saveSuccess = true;

                                //Save Mapping Template Section
                                saveSuccess = Save_TemplateSectionMapping(newTemplateID);

                                //clearControls();
                                ViewState["TemplateID"] = newTemplateID;
                            }

                            if (saveSuccess)
                            {
                                //cvAddEditTemplate.IsValid = false;
                                //cvAddEditTemplate.ErrorMessage = "Template Save Successfully.";
                                //vsAddEditTemplate.CssClass = "alert alert-success";
                                if (cvAddEditTemplate.IsValid)
                                {
                                    divsuccessmsgaCTem.Visible = true;
                                    successmsgaCTem.Text = "Template Save Successfully.";
                              ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                                }

                            }
                            else
                            {
                                divsuccessmsgaCTem.Visible = false;
                                cvAddEditTemplate.IsValid = false;
                                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        _objTemplate.ID = Convert.ToInt64(ViewState["TemplateID"]);

                        if (ContractTemplateManagement.ExistsTemplate(_objTemplate, customerID, _objTemplate.ID))
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Template with Same Name and Version already exists";
                        }
                        else
                        {
                            _objTemplate.UpdatedBy = AuthenticationHelper.UserID;
                            _objTemplate.UpdatedOn = DateTime.Now;

                            saveSuccess = ContractTemplateManagement.UpdateTemplate(_objTemplate);

                            if (saveSuccess)
                            {
                                saveSuccess = Save_TemplateSectionMapping(_objTemplate.ID);
                            }

                            if (saveSuccess)
                            {
                                //cvAddEditTemplate.IsValid = false;
                                //cvAddEditTemplate.ErrorMessage = "Template Updated Successfully.";
                                //vsAddEditTemplate.CssClass = "alert alert-success";
                                if (cvAddEditTemplate.IsValid)
                                {
                                    divsuccessmsgaCTem.Visible = true;
                                    successmsgaCTem.Text = "Template Updated Successfully.";
                              ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                                }


                            }
                            else
                            {
                                cvAddEditTemplate.IsValid = false;
                                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                                divsuccessmsgaCTem.Visible = false;
                            }
                        }
                    }
                }

                upAddEditTemplate.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                divsuccessmsgaCTem.Visible = false;
            }
        }

        protected void btnSaveTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                SaveTemplate(true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = SaveTemplate(false);

                if (saveSuccess)
                {
                    lnkBtnImportSection_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSaveSections_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["TemplateID"] != null)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    bool validateSuccess = false;
                    bool saveSuccess = false;
                    List<string> lstErrorMsg = new List<string>();

                    #region Validation

                    lstErrorMsg.Clear();

                    int selectedSectionCount = 0;
                    foreach (GridViewRow eachGridRow in grdTemplateSection.Rows)
                    {
                        if (eachGridRow.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox chkRow = (CheckBox)eachGridRow.FindControl("chkRow");

                            if (chkRow != null)
                            {
                                if (chkRow.Checked)
                                    selectedSectionCount++;
                            }
                        }
                    }

                    if (selectedSectionCount == 0)
                        lstErrorMsg.Add("Provide Select at least one section to include in the template");
                    else if (selectedSectionCount > 0)
                        validateSuccess = true;

                    if (lstErrorMsg.Count > 0)
                    {
                        validateSuccess = false;
                        ShowErrorMessages(lstErrorMsg, cvAddEditTemplate);
                    }

                    #endregion

                    if (validateSuccess)
                    {
                        long newTemplateID = Convert.ToInt64(ViewState["TemplateID"]);

                        //Save Mapping Template Section
                        saveSuccess = Save_TemplateSectionMapping(newTemplateID);

                        if (saveSuccess)
                        {
                            if (cvAddEditTemplate.IsValid)
                            {
                                //cvAddEditTemplate.IsValid = false;
                                //cvAddEditTemplate.ErrorMessage = "Template Save Successfully.";
                                //vsAddEditTemplate.CssClass = "alert alert-success";
                                divsuccessmsgaCTem.Visible = true;
                                successmsgaCTem.Text = "Template Save Successfully.";
                              ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                            }
                        }
                        else
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                        }
                    }

                    upAddEditTemplate.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public bool Save_TemplateSectionMapping(long templateID)
        {
            bool saveSuccess = false;

            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            if (grdTemplateSection != null)
            {
                List<Cont_tbl_TemplateSectionMapping> lstObjMapping_ToSave = new List<Cont_tbl_TemplateSectionMapping>();

                for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                {
                    if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow = (CheckBox)grdTemplateSection.Rows[i].FindControl("chkRow");
                        if (chkRow != null)
                        {
                            if (chkRow.Checked)
                            {
                                Label lblSectionID = (Label)grdTemplateSection.Rows[i].FindControl("lblSectionID");

                                if (lblSectionID != null)
                                {
                                    if (!string.IsNullOrEmpty(lblSectionID.Text) || lblSectionID.Text != "0")
                                    {
                                        Cont_tbl_TemplateSectionMapping _objMapping = new Cont_tbl_TemplateSectionMapping()
                                        {
                                            TemplateID = templateID,
                                            SectionID = Convert.ToInt64(lblSectionID.Text),
                                            CustomerID = customerID,
                                            IsActive = true,
                                            CreatedBy = AuthenticationHelper.UserID,
                                            CreatedOn = DateTime.Now,
                                            UpdatedBy = AuthenticationHelper.UserID,
                                            UpdatedOn = DateTime.Now
                                        };

                                        TextBox txtOrder = (TextBox)grdTemplateSection.Rows[i].FindControl("txtOrder");
                                        if (!string.IsNullOrEmpty(txtOrder.Text.Trim()))
                                        {
                                            //if (Regex.Match(txtOrder.Text.Trim(), @"!^[a-zA-Z0-9\x20]+$").Success)
                                            //{
                                            //    cvAddEditTemplate.IsValid = false;
                                            //    cvAddEditTemplate.ErrorMessage = "Please enter number only.";
                                            //}
                                            //else
                                            //{
                                            //}
                                            if (!string.IsNullOrEmpty(txtOrder.Text))
                                                _objMapping.SectionOrder = Convert.ToInt32(txtOrder.Text);

                                        }
                                        lstObjMapping_ToSave.Add(_objMapping);
                                    }
                                }
                            }
                        }
                    }
                }//End For Each

                bool MakeFalseAllTemplate = ContractTemplateManagement.GetTemplateSectionMapping(templateID, AuthenticationHelper.UserID);
                if (MakeFalseAllTemplate)
                {
                    saveSuccess = ContractTemplateManagement.CreateUpdate_TemplateSectionMapping(lstObjMapping_ToSave);
                }

                //var assignedTemplateSectionIDs = lstObjMapping_ToSave.Select(row => row.SectionID).ToList();

                //bool matchSuccess = false;
                //if (existingTemplateSectionMapping.Count != assignedTemplateSectionIDs.Count)
                //{
                //    matchSuccess = false;
                //}
                //else
                //{
                //    matchSuccess = existingTemplateSectionMapping.Except(assignedTemplateSectionIDs).ToList().Count > 0 ? false : true;
                //}

                //if (!matchSuccess && lstObjMapping_ToSave.Count > 0)
                //{
                //    saveSuccess = ContractTemplateManagement.CreateUpdate_TemplateSectionMapping(lstObjMapping_ToSave);
                //}
            }

            return saveSuccess;
        }

        public bool SaveTemplate(bool showMsg)
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            bool validateSuccess = false;
            bool saveSuccess = false;
            List<string> lstErrorMsg = new List<string>();

            #region Validation

            if (!string.IsNullOrEmpty(tbxTemplateName.Text.Trim()))
                validateSuccess = true;
            else
                lstErrorMsg.Add("Required Template Name");

            //if (!string.IsNullOrEmpty(tbxVersion.Text.Trim()))
            //    validateSuccess = true;
            //else
            //    lstErrorMsg.Add("Required Version");

            if (lstErrorMsg.Count > 0)
            {
                validateSuccess = false;
                ShowErrorMessages(lstErrorMsg, cvAddEditTemplate);
            }

            #endregion

            if (validateSuccess)
            {
                Cont_tbl_TemplateMaster _objTemplate = new Cont_tbl_TemplateMaster()
                {
                    TemplateName = tbxTemplateName.Text,
                    Version = tbxVersion.Text,

                    CustomerID = customerID,
                    CreatedBy = AuthenticationHelper.UserID,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false
                };

                if ((int)ViewState["Mode"] == 0)
                {
                    if (ContractTemplateManagement.ExistsTemplate(_objTemplate, customerID, 0))
                    {
                        cvAddEditTemplate.IsValid = false;
                        cvAddEditTemplate.ErrorMessage = "Template with Same Name and Version already exists";
                    }
                    else
                    {
                        long newTemplateID = 0;

                        newTemplateID = ContractTemplateManagement.CreateTemplate(_objTemplate);

                        if (newTemplateID > 0)
                        {
                            saveSuccess = true;

                            ViewState["Mode"] = 1;
                            //clearControls();
                            ViewState["TemplateID"] = newTemplateID;

                            if (ViewState["TemplateID"] != null)
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', true);", lnkBtnImportSection.ClientID), true);
                            else
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "enableDisablelnkbtn", string.Format("EnableLinkButton('{0}', false);", lnkBtnImportSection.ClientID), true);
                        }

                        if (saveSuccess)
                        {
                            if (showMsg)
                            {
                                //cvAddEditTemplate.IsValid = false;
                                //cvAddEditTemplate.ErrorMessage = "Template Save Successfully.";
                                //vsAddEditTemplate.CssClass = "alert alert-success";
                                if (cvAddEditTemplate.IsValid)
                                {
                                    divsuccessmsgaCTem.Visible = true;
                                    successmsgaCTem.Text = "Template Save Successfully.";
                                   ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                                }
                            }
                        }
                        else
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                            divsuccessmsgaCTem.Visible = false;
                        }
                    }
                }
                else if ((int)ViewState["Mode"] == 1)
                {
                    if (ViewState["TemplateID"] != null)
                    {
                        _objTemplate.ID = Convert.ToInt64(ViewState["TemplateID"]);

                        if (ContractTemplateManagement.ExistsTemplate(_objTemplate, customerID, _objTemplate.ID))
                        {
                            cvAddEditTemplate.IsValid = false;
                            cvAddEditTemplate.ErrorMessage = "Template with Same Name and Version already exists";
                            divsuccessmsgaCTem.Visible = false;
                        }
                        else
                        {
                            _objTemplate.UpdatedBy = AuthenticationHelper.UserID;
                            _objTemplate.UpdatedOn = DateTime.Now;

                            saveSuccess = ContractTemplateManagement.UpdateTemplate(_objTemplate);

                            if (saveSuccess)
                            {
                                if (showMsg)
                                {
                                    //cvAddEditTemplate.IsValid = false;
                                    //cvAddEditTemplate.ErrorMessage = "Template Updated Successfully.";
                                    //vsAddEditTemplate.CssClass = "alert alert-success";
                                    if (cvAddEditTemplate.IsValid)
                                    {
                                        divsuccessmsgaCTem.Visible = true;
                                        successmsgaCTem.Text = "Template Updated Successfully.";
                                        ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                                    }
                                }
                            }
                            else
                            {
                                cvAddEditTemplate.IsValid = false;
                                cvAddEditTemplate.ErrorMessage = "Something went wrong, Please try again";
                                divsuccessmsgaCTem.Visible = false;
                            }
                        }
                    }
                }
            }
            upAddEditTemplate.Update();
            return saveSuccess;
        }

        public void ShowErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }

        public void BindSections_All(GridView grdTemplateSection, long templateID)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                long sectionID = 0;

                grdTemplateSection.DataSource = ContractTemplateManagement.GetSections_All(customerID);
                grdTemplateSection.DataBind();

                if (templateID != 0)
                {
                    var prevTemplateSectionMapping = ContractTemplateManagement.GetExistingTemplateSectionMapping(templateID);

                    if (prevTemplateSectionMapping != null)
                    {
                        if (prevTemplateSectionMapping.Count > 0)
                        {
                            for (int i = 0; i < grdTemplateSection.Rows.Count; i++)
                            {
                                if (grdTemplateSection.Rows[i].RowType == DataControlRowType.DataRow)
                                {
                                    Label lblSectionID = (Label)grdTemplateSection.Rows[i].FindControl("lblSectionID");

                                    if (lblSectionID != null)
                                    {
                                        sectionID = Convert.ToInt64(lblSectionID.Text);

                                        if (sectionID != 0)
                                        {
                                            if (prevTemplateSectionMapping.Select(row => row.SectionID).Contains(sectionID))
                                            {
                                                CheckBox chkRow = (CheckBox)grdTemplateSection.Rows[i].FindControl("chkRow");

                                                if (chkRow != null)
                                                {
                                                    chkRow.Checked = true;
                                                }

                                                TextBox txtOrder = (TextBox)grdTemplateSection.Rows[i].FindControl("txtOrder");

                                                if (txtOrder != null)
                                                {
                                                    var secTemplateMappingRecord = prevTemplateSectionMapping.Where(row => row.SectionID == sectionID).FirstOrDefault();

                                                    if (secTemplateMappingRecord != null)
                                                    {
                                                        txtOrder.Text = secTemplateMappingRecord.SectionOrder.ToString();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } //END FOR
                        }
                    }

                    prevTemplateSectionMapping.Clear();
                    prevTemplateSectionMapping = null;

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnTemplate_Click(object sender, EventArgs e)
        {
            liTemplate.Attributes.Add("class", "active");
            liImportSection.Attributes.Add("class", "");

            MainView.ActiveViewIndex = 0;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myModal", "pageLoad();", true);

        }

        protected void lnkBtnImportSection_Click(object sender, EventArgs e)
        {
            liTemplate.Attributes.Add("class", "");
            liImportSection.Attributes.Add("class", "active");

            MainView.ActiveViewIndex = 1;

            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "countSelected", "checkUncheckRow();", true);
        }

        protected void lnkBtnRebind_Sections_Click(object sender, EventArgs e)
        {
            if (ViewState["TemplateID"] != null)
            {
                long templateID = Convert.ToInt64(ViewState["TemplateID"]);

                BindSections_All(grdTemplateSection, templateID);
            }
        }

        protected void grdTemplateSections_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (e.CommandName.Equals("View_Section"))
                {
                    long sectionID = Convert.ToInt64(e.CommandArgument);

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenSectionModal('" + sectionID + "','1');", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvAddEditTemplate.IsValid = false;
                cvAddEditTemplate.ErrorMessage = "Something went wrong. Please try again";
            }
        }

        public void ExporttoWord(List<Cont_SP_GetTemplateSectionDetails_Result> lstSections)
        {
            if (lstSections.Count > 0)
            {
                System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                string fileName = "Template";
                string templateName = "Template";

                if (lstSections[0] != null)
                {
                    templateName = lstSections[0].TemplateName;
                    fileName = templateName + "-" + lstSections[0].TemplateVersion;
                }

                strExporttoWord.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header: h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }

                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                .break { page-break-before: always; }
                -->
                </style></head>");

                strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                //strExporttoWord.Append(@"<table style='width:100%; font-family:Calibri;border:none;'>");

                lstSections = lstSections.OrderBy(row => row.SectionOrder).ToList();

                strExporttoWord.Append(@"<div style='text-align:center;'>");
                strExporttoWord.Append(@"<p><b>" + templateName + "</b></p>");
                strExporttoWord.Append(@"</div>");

                lstSections.ForEach(eachSection =>
                {
                    strExporttoWord.Append(@"<div>");
                    strExporttoWord.Append(@"<p><b>" + eachSection.Header + "</b></p>");
                    strExporttoWord.Append(@"</div>");

                    strExporttoWord.Append(@"<div>");
                    strExporttoWord.Append(@"<p>" + eachSection.BodyContent + "</p>");
                    strExporttoWord.Append(@"</div>");

                    //strExporttoWord.Append(@"<tr>");
                    //strExporttoWord.Append(@"<td><b>" + eachSection.Header + "</b></td>");
                    //strExporttoWord.Append(@"</tr>");

                    //strExporttoWord.Append(@"<tr>");
                    //strExporttoWord.Append(@"<td>" + eachSection.BodyContent + "</td>");
                    //strExporttoWord.Append(@"</tr>");
                });

                //strExporttoWord.Append(@"</table>");

                strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr>
                    <td>
                        <div style='mso-element:header' id=h1 >
                            <p class=MsoHeader style='text-align:left'>
                        </div>
                    </td>
                    <td>
                        <div style='mso-element:footer' id=f1>
                            <p class=MsoFooter>
                            <span style=mso-tab-count:2'></span>
                            <span style='mso-field-code:"" PAGE ""'></span>
                            of <span style='mso-field-code:"" NUMPAGES ""'></span>
                            </p>
                        </div>
                    </td>
                </tr>
                </table>
                </body>
                </html>");

                fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                Response.AppendHeader("Content-Type", "application/msword");
                Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
                Response.Write(strExporttoWord);
            }
        }
    }
}