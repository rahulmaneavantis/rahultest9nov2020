﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddContractTemplate.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddContractTemplate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- font icon -->
    <link href="../../NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <link href="../../NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="../../NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <%--<link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <%--<script src="https://code.jquery.com/jquery-1.11.3.js"></script>--%>
    <script type="text/javascript" src="../../Newjs/jquery.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../../Newjs/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="../../Newjs/jquery-1.8.3.min.js"></script>--%>
    <!-- Bootstrap CSS -->
    <link href="../../NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="../../NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../../NewCSS/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="../../Newjs/bootstrap-multiselect.js" type="text/javascript"></script>
    <script src="../../Newjs/bootstrap-tagsinput.js"></script>
    <link href="../../NewCSS/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../../NewCSS/timeline.css" rel="stylesheet" />
    <!-- nice scroll -->
    <script type="text/javascript" src="../../Newjs/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="../../Newjs/jquery.nicescroll.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <script type="text/javascript">
     
              

        function CloseMe() {
            window.parent.CloseContractModal();
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function OpenSectionModal(templateID, viewOnly) {
            debugger;
            $('#TemplateSectionPopup').modal('show');
            $('#<%= IframeTemplateSection.ClientID%>').attr('src', "/ContractProduct/Masters/AddTemplateSection.aspx?accessID=" + templateID + "&ViewOnly=" + viewOnly);
        }

        function CloseSectionModal() {
            $('#TemplateSectionPopup').modal('hide');
          <%--  document.getElementById('<%= lnkBtnRebind_Sections.ClientID %>').click();
            var grid = document.getElementById("<%= grdTemplateSection.ClientID%>");  
            for (var i = 0; i < grid.rows.length - 1; i++) {
                var txtOrder = $("input[id*=txtOrder]")
                if (txtOrder[i].value == '') {
                   // alert(txtOrder[i].value);
                    txtOrder = '';
        }
            }--%>
        }

        function EnableLinkButton(ID, flag) {
            document.getElementById(ID).onclick = function () { return flag; };
            if (!flag) {
                document.getElementById(ID).setAttribute("disabled", "disabled");
                document.getElementById(ID).className = "LnkDisabled";
            }
            else {
                document.getElementById(ID).setAttribute("disabled", "");
                document.getElementById(ID).className = "LnkEnabled";
            }
        }

        function CheckNumber(event) {
            if (event.keyCode == 46 || event.keyCode == 8) {
            } else {
                if (event.keyCode < 95) {
                    if (event.keyCode < 48 || event.keyCode > 57) {
                        event.preventDefault();
                    }
                } else {
                    if (event.keyCode < 96 || event.keyCode > 105) {
                        event.preventDefault();
                    }
                }
            }
        }
        function pageLoad() {

            $('[data-toggle="tooltip"]').tooltip()
        };
    </script>
    <style>
       #upAddEditTemplate
       {
           width:95%;
       }
    </style>
    <script type="text/javascript">
        function checkAll(chkHeader) {
            var selectedRowCount = 0;
            var grid = document.getElementById("<%=grdTemplateSection.ClientID %>");
            if (grid != null) {
                if (chkHeader != null) {
                    //Get all input elements in Gridview
                    var inputList = grid.getElementsByTagName("input");

                    for (var i = 1; i < inputList.length; i++) {
                        if (inputList[i].type == "checkbox") {
                            if (chkHeader.checked) {
                                inputList[i].checked = true;
                                selectedRowCount++;
                            }
                            else if (!chkHeader.checked)
                                inputList[i].checked = false;
                        }
                    }
                }

                var btnSave = document.getElementById("<%=btnSaveSections.ClientID %>");
                var lblTotalSectionsSelected = document.getElementById("<%=lblTotalSectionsSelected.ClientID %>");
                if ((btnSave != null || btnSave != undefined) && (lblTotalSectionsSelected != null || lblTotalSectionsSelected != undefined)) {
                    if (selectedRowCount > 0) {
                        lblTotalSectionsSelected.innerHTML = selectedRowCount + " Selected";
                        divCountSelectedSections.style.display = "block";
                    }
                    else {
                        lblTotalSectionsSelected.innerHTML = "";;
                        divCountSelectedSections.style.display = "none";
                    }
                }
            }
        }
       
        function checkUncheckRow() {

            var selectedRowCount = 0;
            //Get the reference of GridView
            var grid = document.getElementById("<%=grdTemplateSection.ClientID %>");

            //Get all input elements in Gridview
            var inputList = grid.getElementsByTagName("input");

            //The First element is the Header Checkbox
            var headerCheckBox = inputList[0];
            var checked = true;
            for (var i = 0; i < inputList.length; i++) {
                //Based on all or none checkboxes are checked check/uncheck Header Checkbox                
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                    }
                    if (inputList[i].checked) {
                        selectedRowCount++;
                    }
                }
            }

            headerCheckBox.checked = checked;

            var btnSave = document.getElementById("<%=btnSaveSections.ClientID %>");
            var lblTotalSectionsSelected = document.getElementById("<%=lblTotalSectionsSelected.ClientID %>");
            if ((btnSave != null || btnSave != undefined) && (lblTotalSectionsSelected != null || lblTotalSectionsSelected != undefined)) {
                if (selectedRowCount > 0) {
                    lblTotalSectionsSelected.innerHTML = selectedRowCount + " Selected";
                    divCountSelectedSections.style.display = "block";
                }
                else {
                    lblTotalSectionsSelected.innerHTML = "";;
                    divCountSelectedSections.style.display = "none";
                }
            }
        }

        function unCheckAll(gridName) {
            var grid = document.getElementById(gridName);
            if (grid != null) {
                //Get all input elements in Gridview
                var inputList = grid.getElementsByTagName("input");

                for (var i = 1; i < inputList.length; i++) {
                    if (inputList[i].type == "checkbox") {
                        inputList[i].checked = false;
                    }
                }
            }
        }
    </script>
</head>
<body class="bgColor-white">
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="smAddEditTemplate" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upAddEditTemplate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="row">
                        <ul class="nav nav-tabs">
                            <li class="active" id="liTemplate" runat="server">
                                <asp:LinkButton ID="lnkBtnTemplate" OnClick="lnkBtnTemplate_Click" runat="server">Step 1-Template</asp:LinkButton>
                            </li>

                            <li class="" id="liImportSection" runat="server">
                                <asp:LinkButton ID="lnkBtnImportSection" OnClick="lnkBtnImportSection_Click" runat="server">Step 2-Import Section</asp:LinkButton>
                            </li>
                        </ul>

                        <%--  <header class="panel-heading tab-bg-primary" style="background: none !important;">
                            <ul class="nav nav-tabs">
                                <li class="active" id="liTemplate" runat="server">
                                    <asp:LinkButton ID="lnkBtnTemplate" OnClick="lnkBtnTemplate_Click" runat="server">Step 1-Template</asp:LinkButton>
                                </li>

                                <li class="" id="liImportSection" runat="server">
                                    <asp:LinkButton ID="lnkBtnImportSection" OnClick="lnkBtnImportSection_Click" runat="server">Step 2-Import Section</asp:LinkButton>
                                </li>
                            </ul>
                        </header>--%>
                    </div>
                   <%-- divsuccessmsgaCTem--%>
                    <div class="row form-group">
                       <div class="col-md-12 alert alert-success" runat="server" visible="false" id="divsuccessmsgaCTem">
                            <asp:Label runat="server" ID="successmsgaCTem" ></asp:Label>
                        </div>
                        <asp:ValidationSummary ID="vsAddEditTemplate" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="ContractTemplateValidationGroup" />
                        <asp:CustomValidator ID="cvAddEditTemplate" runat="server" EnableClientScript="False"
                            ValidationGroup="ContractTemplateValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>
                    <div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="TemplateView" runat="server">
                                <div class="row form-group">
                                    <div class="form-group required col-md-4">
                                        <label for="tbxTemplateName" class="control-label">Template Name</label>
                                        <asp:TextBox runat="server" ID="tbxTemplateName" CssClass="form-control" autocomplete="off" />
                                        <asp:RequiredFieldValidator ID="rfvTemplateName" ErrorMessage="Please Enter Template Name" ControlToValidate="tbxTemplateName"
                                            runat="server" ValidationGroup="ContractTemplateValidationGroup" Display="None" />
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="tbxVersion" class="control-label">Version</label>
                                        <asp:TextBox runat="server" ID="tbxVersion" CssClass="form-control" autocomplete="off" onkeypress="return isNumberKey(event)" />
                                        <asp:RequiredFieldValidator ID="rfvVersion" ErrorMessage="Please Enter Version" ControlToValidate="tbxVersion"
                                            runat="server" ValidationGroup="ContractTemplateValidationGroup" Display="None" />
                                    </div>
                                </div>
                                <div class="row form-group text-right">
                                    <div class="col-md-12">
                                        <asp:Button Text="Save" runat="server" ID="btnSaveTemplate" CssClass="btn btn-primary"
                                            ValidationGroup="ContractTemplateValidationGroup" OnClick="btnSaveTemplate_Click" />
                                        <asp:Button Text="Clear" runat="server" ID="btnClear" CssClass="btn btn-primary"
                                            OnClick="btnClearTemplate_Click" Visible="false" />
                                        <asp:Button Text="Export as Word" runat="server" ID="btnExportWord1" CssClass="btn btn-primary"
                                            OnClick="btnExportWord_Click" />
                                        <asp:Button Text="Save & Next" runat="server" ID="btnSaveNext" CssClass="btn btn-primary"
                                            ValidationGroup="ContractTemplateValidationGroup" OnClick="btnSaveNext_Click" />
                                    </div>
                                </div>
                            </asp:View>

                            <asp:View ID="SectionBrowserView" runat="server">
                                <div id="divOuterGridTemplateSection" class="row col-md-12" runat="server">
                                    <div class="w100per">
                                        <div class="float-left col-md-11 colpadding0 w95per">
                                            <label for="grdTemplateSection" class="control-label">Section Browser</label>
                                            <i style="color: #c1c1c1;">(Select (<i class="fa fa-check" aria-hidden="true"></i>) one or more section(s) to add in the template)</i>
                                        </div>
                                        <div class="float-left col-md-1 colpadding0 w5per text-right">
                                            <asp:LinkButton runat="server" ID="btnAddNew" OnClick="btnAddNew_Click"
                                                data-toggle="tooltip" data-placement="top" title="Add a New Section">
                                                <img src='/Images/add_icon_new.png' alt="Section" />
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-md-12 mt5 mb5" style="min-height: 50px; max-height: 300px; overflow-y: auto; overflow-x:hidden">
                                        <asp:GridView runat="server" ID="grdTemplateSection" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                                            AllowSorting="true" GridLines="none" Width="100%" AllowPaging="false" AutoPostBack="true" CssClass="table" DataKeyNames="ID"
                                            OnRowCommand="grdTemplateSections_RowCommand">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeader" runat="server" onclick="javascript:checkAll(this)" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRow" runat="server" onclick="javascript:checkUncheckRow(this)" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Order" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSectionID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                                                        <asp:TextBox ID="txtOrder" runat="server" CssClass="form-control text-center" autocomplete="off" onkeydown="CheckNumber(event)"></asp:TextBox>
                                                        <%--onkeydown="return (!(event.keyCode >47 && event.keyCode <58) || (event.keyCode >95 && event.keyCode >106) && event.keyCode!=32);"
                                                        onkeyup="ValidateText(this);"
                                                            <asp:CompareValidator ID="valQtyNumeric" runat="server" ControlToValidate="txtOrder"  Display="Dynamic" SetFocusOnError="true"
                                                            Text="" ErrorMessage="Error: Order must be a number!" ForeColor="Red" Operator="DataTypeCheck" Type="Integer">   </asp:CompareValidator>  --%>
                                                        <%--onkeydown="return (!(event.keyCode>=65 && event.keyCode <=95) && event.keyCode!=32);"--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Header" HeaderText="Header" ItemStyle-Width="60%" />
                                                <asp:BoundField DataField="Version" HeaderText="Version" ItemStyle-Width="10%"
                                                    HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" />
                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Justify" ItemStyle-Width="10%"
                                                    HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkViewSection" runat="server" CommandName="View_Section"
                                                            ToolTip="View" data-toggle="tooltip" CommandArgument='<%# Eval("ID") %>'>
                                                            <img src='<%# ResolveUrl("/Images/Eye.png")%>' alt="View" />
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="clsROWgrid" />
                                            <HeaderStyle CssClass="clsheadergrid" />
                                            <PagerTemplate>
                                                <table style="display: none">
                                                    <tr>
                                                        <td>
                                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                No Record Found
                                            </EmptyDataTemplate>
                                        </asp:GridView>

                                        <asp:LinkButton ID="lnkBtnRebind_Sections" OnClick="lnkBtnRebind_Sections_Click"
                                            Style="float: right; display: none;" Width="100%" runat="server"> 
                                        </asp:LinkButton>
                                    </div>
                                </div>

                                <div id="divCountSelectedSections" class="row col-md-12 pl0" style="display: none;">
                                    <div class="col-md-12 text-left">
                                        <asp:Label runat="server" ID="lblTotalSectionsSelected" Text="" CssClass="control-label"></asp:Label>
                                    </div>
                                </div>

                                <div class="row form-group text-right">
                                    <div class="col-md-12">
                                        <asp:Button Text="Save" runat="server" ID="btnSaveSections" CssClass="btn btn-primary"
                                            ValidationGroup="ContractTemplateValidationGroup" OnClick="btnSaveSections_Click" />
                                        <asp:Button Text="Export as Word" runat="server" ID="btnExportWord2" CssClass="btn btn-primary"
                                            OnClick="btnExportWord_Click" />
                                        <asp:Button Text="Clear" runat="server" ID="btnClearSections"
                                            CssClass="btn btn-primary" data-dismiss="modal" OnClick="btnClearSections_Click" />
                                        <asp:Button Text="Close" runat="server" ID="Button2"
                                            CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" />
                                    </div>
                                </div>
                            </asp:View>
                        </asp:MultiView>

                        <div class="row">
                            <div class="col-md-12">
                                <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportWord1" />
                    <asp:PostBackTrigger ControlID="btnExportWord2" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="modal fade" id="TemplateSectionPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="margin-bottom: 10px;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="CloseSectionModal()">&times;</button>
                        </div>
                        <div class="modal-body" style="width: 100%;">
                            <iframe src="about:blank" id="IframeTemplateSection" runat="server" frameborder="0" width="100%" height="450px"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
   
</html>
