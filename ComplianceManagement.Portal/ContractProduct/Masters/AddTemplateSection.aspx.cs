﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class AddTemplateSection : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["accessID"]))
                    {
                        int customerID = -1;
                        customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                        long sectionID = Convert.ToInt64(Request.QueryString["accessID"]);

                        var objRecord = ContractTemplateManagement.GetSectionDetailsByID(sectionID, customerID);

                        if (objRecord != null)
                        {
                            tbxHeader.Text = objRecord.Header;
                            tbxVersion.Text = objRecord.Version;
                            tbxContent.Text = objRecord.BodyContent;

                            ViewState["Mode"] = 1;
                            ViewState["SectionID"] = sectionID;
                        }
                    }
                    else
                    {
                        ViewState["Mode"] = 0;
                    }

                    if (!string.IsNullOrEmpty(Request.QueryString["ViewOnly"]))
                    {
                        if (Convert.ToInt32(Request.QueryString["ViewOnly"]) == 0)
                            enableDisableControls(true);
                        else
                            enableDisableControls(false);
                    }
                    else
                        enableDisableControls(true);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvTemplateSection.IsValid = false;
                    cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                bool validateSuccess = false;
                List<string> lstErrorMsg = new List<string>();

                #region Validation

                if (!string.IsNullOrEmpty(tbxHeader.Text))
                    validateSuccess = true;
                else
                    lstErrorMsg.Add("Required Header");

                //if (!string.IsNullOrEmpty(tbxVersion.Text))
                //    validateSuccess = true;
                //else
                //    lstErrorMsg.Add("Required Version");

                if (!string.IsNullOrEmpty(tbxContent.Text))
                    validateSuccess = true;
                else
                    lstErrorMsg.Add("Required Content");

                if (lstErrorMsg.Count > 0)
                {
                    validateSuccess = false;
                    showErrorMessages(lstErrorMsg, cvTemplateSection);
                }

                #endregion

                if (validateSuccess)
                {
                    Cont_tbl_SectionMaster _objSection = new Cont_tbl_SectionMaster()
                    {
                        Header = tbxHeader.Text,
                        Version = tbxVersion.Text,
                        BodyContent = tbxContent.Text,
                        CustomerID = customerID,
                        CreatedBy = AuthenticationHelper.UserID,
                        CreatedOn = DateTime.Now,
                        IsActive = true
                    };

                    if ((int)ViewState["Mode"] == 0)
                    {
                        if (ContractTemplateManagement.ExistsSection(_objSection, customerID, 0))
                        {
                            cvTemplateSection.IsValid = false;
                            cvTemplateSection.ErrorMessage = "Section with Same Header and Version already exists";
                        }
                        else
                        {
                            long newSectionID = 0;

                            newSectionID = ContractTemplateManagement.CreateSection(_objSection);

                            if (newSectionID > 0)
                            {
                                if (cvTemplateSection.IsValid)
                                {
                                    divsuccessmsgaCTemSec.Visible = true;
                                    successmsgaCTemSec.Text = "Section Save Successfully.";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);
                                }

                                clearControls();

                                Session["SectionID"] = newSectionID;
                            }
                            else
                            {
                                cvTemplateSection.IsValid = false;
                                cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                    else if ((int)ViewState["Mode"] == 1)
                    {
                        _objSection.ID = Convert.ToInt64(ViewState["SectionID"]);

                        if (ContractTemplateManagement.ExistsSection(_objSection, customerID, _objSection.ID))
                        {
                            cvTemplateSection.IsValid = false;
                            cvTemplateSection.ErrorMessage = "Section with Same Header and Version already exists";
                        }
                        else
                        {
                            _objSection.UpdatedBy = AuthenticationHelper.UserID;
                            _objSection.UpdatedOn = DateTime.Now;

                            bool saveSuccess = ContractTemplateManagement.UpdateSection(_objSection);

                            if (saveSuccess)
                            {
                                if (cvTemplateSection.IsValid)
                                {
                                    divsuccessmsgaCTemSec.Visible = true;
                                    successmsgaCTemSec.Text = "Section Updated Successfully";
                                    ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divsuccessmsgaCTemSec.ClientID + "').style.display = 'none' },2000);", true);

                                }
                            }
                            else
                            {
                                cvTemplateSection.IsValid = false;
                                cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
                            }
                        }
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "bindTinyMCE", "bind_tinyMCE();", true);

                if (!cvTemplateSection.IsValid)
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scrollUpPageScript", "scrollUp();", true);

                //upTemplateSection.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvTemplateSection.IsValid = false;
                cvTemplateSection.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void clearControls()
        {
            tbxHeader.Text = string.Empty;
            tbxVersion.Text = string.Empty;
            tbxContent.Text = string.Empty;
        }

        private void enableDisableControls(bool flag)
        {
            tbxHeader.Enabled = flag;
            tbxVersion.Enabled = flag;
            tbxContent.Enabled = flag;

            btnSave.Enabled = flag;
        }

        public void showErrorMessages(List<string> lstErrMsgs, CustomValidator cvtoShowErrorMsg)
        {
            //string finalErrMsg = string.Join("<br/>", emsg.ToArray());

            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (lstErrMsgs.Count > 0)
            {
                lstErrMsgs.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvtoShowErrorMsg.IsValid = false;
            cvtoShowErrorMsg.ErrorMessage = finalErrMsg;
        }
    }
}