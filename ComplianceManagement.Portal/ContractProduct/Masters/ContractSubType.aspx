﻿<%@ Page Title="Contract Sub-Type:: Contract" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="ContractSubType.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.ContractSubType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
       
        function OpenSubConTypePopup(ContractSubTypeID) {
            $('#AddContSubTypePopUp').modal('show');
            $('#ContentPlaceHolder1_Iframesubconttype').attr('b')
            $('#ContentPlaceHolder1_Iframesubconttype').attr('src', "../../ContractProduct/Masters/AddContSubType.aspx?ContractSubTypeId=" + ContractSubTypeID);
        }

        function CloseContractSubTypePopUp() {
            $('#AddContSubTypePopUp').modal('hide');
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //location.reload();
        }

        function RefreshParent() {
            document.getElementById('<%= lnkBtn_RebindGrid.ClientID %>').click();
            //window.location.href = window.location.href;
        }
    </script>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { null };

        $(document).ready(function () {
            fhead('Masters/ Contract Sub-Type');
        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upSubType" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard container">
                    <div class="row">
                        <asp:ValidationSummary ID="vsSubTypePage" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                            ValidationGroup="SubTypePageValidationGroup" />
                        <asp:CustomValidator ID="cvSubTypePage" runat="server" EnableClientScript="False"
                            ValidationGroup="SubTypePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        <div class="col-md-6 colpadding0">
                            <asp:TextBox runat="server" ID="tbxtypeTofilter" placeholder="Type to Search" AutoComplete="off" CssClass="form-control" AutoPostBack="true"
                         OnTextChanged="tbxFilter_TextChanged" Width="70%"/>
                        </div>
                        <div class="col-md-1 colpadding0">
                            <asp:LinkButton ID="lnkBtn_RebindGrid" OnClick="lnkBtn_RebindGrid_Click" Style="display: none;" runat="server"></asp:LinkButton>
                        </div>
                        <div class="col-md-5  colpadding0">
                            <div class="col-md-8"></div>
                            <div class="col-md-2" style="padding-left:0">
                            <asp:LinkButton Text="Back" runat="server" ID="lnkBackToContractType" CssClass="btn btn-primary" OnClick="lnkBackToContractType_Click" 
                                ToolTip="Go Back to Contract Type Master" data-toggle="tooltip" data-placement="bottom" Width="100%"/>
                                   </div>  
                             <div class="col-md-2 colpadding0">             
                            <asp:LinkButton runat="server" ID="btnAddSubType" OnClick="btnAddSubType_Click" CssClass="btn btn-primary"
                                data-toggle="tooltip" data-placement="bottom" ToolTip="Add a New Contract Sub-Type" Width="100%">
                                <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New
                            </asp:LinkButton>  
                                 </div>  
                        </div>
                            </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        <asp:GridView runat="server" ID="grdSubConType" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true"
                            AllowSorting="true" PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%"
                            DataKeyNames="ContractSubTypeID" OnRowCommand="grdSubConType_RowCommand" OnSorting="grdSubConType_Sorting" OnRowCreated="grdSubConType_RowCreated">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                         <%#Container.DataItemIndex+1 %>
                                        <%--<asp:Label ID="lblRowID" runat="server" Text='<%# Eval("RowID") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="TypeName" HeaderText="Contract Type" HeaderStyle-Width="40%" ItemStyle-Width="40%" />
                                <asp:BoundField DataField="SubTypeName" HeaderText="Sub-Type" SortExpression="SubTypeName" HeaderStyle-Width="45%" ItemStyle-Width="45%" />

                                <asp:TemplateField HeaderText="Action" HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="EDIT_SubContype" CommandArgument='<%# Eval("ContractSubTypeID") %>' 
                                            ToolTip="Edit" data-toggle="tooltip">                                           
                                            <img src="/Images/edit_icon_new.png" alt="Edit"  />
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2" runat="server" Visible="false" CommandName="DELETE_SubContype" CommandArgument='<%# Eval("ContractSubTypeID") %>'
                                            OnClientClick="return confirm('Are you certain you want to delete this sub type?');"  ToolTip="Delete" data-toggle="tooltip">
                                            <img src="/Images/delete_icon_new.png" alt="Delete"/>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="clsROWgrid" />
                            <HeaderStyle CssClass="clsheadergrid" />
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                No Record Found
                            </EmptyDataTemplate>
                        </asp:GridView>
                            </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="col-md-10 colpadding0">
                                <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                                    <p class="pr0">                                        
                                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-1 colpadding0">
                                <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width:100%; float: right; margin-right: 6%;"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Text="5" />
                                    <asp:ListItem Text="10" Selected="True" />
                                    <asp:ListItem Text="20" />
                                    <asp:ListItem Text="50" />
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-1 colpadding0 text-right">
                                <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                                    OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                                </asp:DropDownListChosen>
                            </div>
                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                        </div>
                    </div>

                    <%--<div class="row">
                        <div class="col-md-12 colpadding0" style="margin-top: 1%;">
                            <div class="col-md-6 colpadding0">
                                <div class="col-md-1 colpadding0">
                                    <asp:LinkButton Text="Previous" CssClass="btn btn-primary" runat="server" ID="LnkbtnPrevious" />
                                </div>
                                <div class="col-md-11 colpadding0">
                                </div>
                            </div>
                            <div class="col-md-6 colpadding0">
                                <div class="col-md-11 colpadding0">
                                </div>
                                <div class="col-md-1 colpadding0">
                                    <asp:LinkButton Text="Next" CssClass="btn btn-primary" runat="server" ID="LnkbtnNext" Style="float: right;" />
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="AddContSubTypePopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 35%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="RefreshParent()">&times;</button>
                </div>
                <div class="modal-body" style="width: 100%;">
                    <iframe src="about:blank" id="Iframesubconttype" runat="server" frameborder="0" width="100%" height="250px"></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
