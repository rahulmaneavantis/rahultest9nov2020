﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddVendor.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters.AddVendor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!--external css-->
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>
    
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />

    <script type="text/javascript">

     function OpenAddCityPopup() {
            $('#AddCityPopUp').modal('show');

        }
        function CloseMe() {
            window.parent.ClosePopVendor();
        }
    </script>    
    
</head>
<body style="background-color: white">
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnSelectedCity" runat="server" />
        <div>
            <asp:ScriptManager ID="LitigationAddDept" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="upDepartment" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div>
                        <div style="margin-bottom: 7px">
                             <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCvndr">
                            <asp:Label runat="server" ID="successmsgaCvndr" ></asp:Label>
                        </div>

                            <div class="col-md-12 alert alert-block alert-success fade in" runat="server" visible="false" id="divsuccessmsgaCcity">
                            <asp:Label runat="server" ID="successmsgaCcity" ></asp:Label>
                        </div>
                            <asp:ValidationSummary ID="vsAddVendor" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                                ValidationGroup="NewAddLaywerValidate" />
                            <asp:CustomValidator ID="cvAddVendor" runat="server" EnableClientScript="False"
                                ValidationGroup="NewAddLaywerValidate" Display="none" class="alert alert-block alert-danger fade in" />                           
                        </div>
                        <div>
                            <div class="row">
                                <div class="form-group required col-md-4">
                                    <label for="rbPartyType" class="control-label">Vendor Type</label>
                                    <asp:RadioButtonList ID="rbPartyType" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem class="radio-inline" Text="Individual" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem class="radio-inline" Text="Corporate" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="form-group required col-md-8">
                                    <label for="tbxName" class="control-label">Vendor Name</label>
                                    <asp:TextBox runat="server" ID="tbxName" CssClass="form-control" autocomplete="off" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Please Enter Vendor Name"
                                        ControlToValidate="tbxName" runat="server" ValidationGroup="NewAddLaywerValidate"
                                        Display="None" />
                                    <asp:RegularExpressionValidator ID="revVendorName" ControlToValidate="tbxName" ValidationExpression="[^,@]+"
                                        runat="server" ValidationGroup="NewAddLaywerValidate" ErrorMessage="Name can not be contain comma(,)." Display="None"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="tbxAddress" class="control-label">Address</label>
                                    <asp:TextBox runat="server" ID="tbxAddress" Style="width: 100%;" MaxLength="200" CssClass="form-control" TextMode="MultiLine" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="ddlCountry" class="control-label">Country</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlCountry" class="form-control m-bot15" DataPlaceHolder="Select Country" Width="100%"
                                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="ddlState" class="control-label">State</label>
                                    <asp:DropDownListChosen runat="server" ID="ddlState" class="form-control m-bot15" DataPlaceHolder="Select State" Width="100%"
                                        OnSelectedIndexChanged="ddlState_SelectedIndexChanged" AutoPostBack="true" AllowSingleDeselect="false" DisableSearchThreshold="5" />
                                </div>
                                
                               <div class="form-group col-md-4">
                                    <label for="ddlCity" class="control-label">City</label>
                                   <div style="width:100%;">
                                   <div style="float:left;width:90%">
                                    <asp:DropDownListChosen runat="server" ID="ddlCity" class="form-control m-bot15" DataPlaceHolder="Select City" Width="100%" onchange="ddlCityChange()"
                                        AllowSingleDeselect="false" DisableSearchThreshold="5" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Please Select City or Select 'Not Applicable'"
                                        ControlToValidate="ddlCity" runat="server" ValidationGroup="CasePopUpValidationGroup"
                                        Display="None" />
                                       </div>
                                   <div style="float: right; text-align: center; width: 10%; margin-top: 1%;">
                                    <img id="lnkShowAddNewCityModal" style="float: right; display: none;" src="../../Images/add_icon_new.png" onclick="OpenAddCityPopup()" alt="Add New Opponent" title="Add New Opponent" />
                                       </div>
                                </div>
                                   <asp:Button ID="btnRebindddlCity" Visible="false" runat="server" OnClick="btnRebindddlCity_Click" /> 
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="tbxContactPerson" class="control-label">Contact Person</label>
                                    <asp:TextBox runat="server" ID="tbxContactPerson" CssClass="form-control" autocomplete="off"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="tbxEmail" class="control-label">Email</label>
                                    <asp:TextBox runat="server" ID="tbxEmail" CssClass="form-control" autocomplete="off"/>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="None" runat="server"
                                        ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid email."
                                        ControlToValidate="tbxEmail" ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="tbxContactNo" class="control-label">Contact No</label>
                                    <asp:TextBox runat="server" ID="tbxContactNo" CssClass="form-control"
                                        MaxLength="10" autocomplete="off"/>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" runat="server"
                                        ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter a valid contact number."
                                        ControlToValidate="tbxContactNo" ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxContactNo" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                                        ValidationGroup="NewAddLaywerValidate" ErrorMessage="Please enter only 10 digit."
                                        ControlToValidate="tbxContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label for="tbxVAT" class="control-label">VAT</label>
                                    <asp:TextBox runat="server" ID="tbxVAT" CssClass="form-control" autocomplete="off"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="tbxTIN" class="control-label">TIN</label>
                                    <asp:TextBox runat="server" ID="tbxTIN" CssClass="form-control" autocomplete="off"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="tbxGSTIN" class="control-label">GSTIN</label>
                                    <asp:TextBox runat="server" ID="tbxGSTIN" CssClass="form-control" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-md-12">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-primary" OnClick="btnSave_Click"
                                        ValidationGroup="NewAddLaywerValidate" />
                                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMe();" /> <%--RefreshParent();--%>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="modal fade" id="AddCityPopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" style="width: 40%; height: 30%">
                <div class="modal-content">
                    <div class="modal-header">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label class="modal-header-custom">
                            Add New City</label>
                    </div>

                    <div class="modal-body" style="width: 100%;">
                        <asp:UpdatePanel ID="UpdatePanelCity" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="margin-bottom: 7px">
                                    <asp:ValidationSummary ID="cityValidationSummary" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="CityAddPopupValidationGroup" />
                                    <asp:CustomValidator ID="CityValidator" runat="server" EnableClientScript="False"
                                        ValidationGroup="CityAddPopupValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
                                </div>
                                <div style="margin-bottom: 7px; margin-top: 20px; align-content: center">
                                    <label style="width: 3%; display: block; float: left; margin-top: 5px; font-size: 13px; color: red;">*</label>
                                    <label style="width: 27%; display: block; float: left; margin-top: 5px; font-size: 13px; color: #333;">
                                        City</label>
                                    <asp:TextBox runat="server" ID="tbxCity" CssClass="form-control" Style="width: 68%;" MaxLength="100" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="City can not be empty." ControlToValidate="tbxCity"
                                        runat="server" ValidationGroup="CityAddPopupValidationGroup" Display="None" />
                                </div>

                                <div style="margin-bottom: 7px; margin-left: 140px; margin-top: 10px">
                                    <div style="margin-bottom: 7px; text-align: center">
                                        <asp:Button Text="Save" runat="server" ID="lnkBtnCity" OnClick="lnkBtnCity_Click" CssClass="btn btn-primary" ValidationGroup="CityAddPopupValidationGroup" />
                                        <asp:Button Text="Close" runat="server" ID="btnCancelCityPopUp" CssClass="btn btn-primary" data-dismiss="modal" OnClientClick="CloseMeCity();" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="color: red;"><strong>Note:</strong> (*) fields are Compulsory</p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>


    </form>
    <script type="text/javascript">

        function ddlCityChange() {
            var selectedCityID = $("#<%=ddlCity.ClientID %>").val();
             if (selectedCityID != null) {
                if (selectedCityID == "0") {
                    $("#lnkShowAddNewCityModal").show();
                }
                else {
                    $("#lnkShowAddNewCityModal").hide();
                }
            }
        }
       
        function restoreSelectedCity() {
            debugger;

            var selectedStr = $('#<% =hdnSelectedCity.ClientID %>').val();

            //alert(selectedStr);

            $('[id*=ddlCity]').multiselect('rebuild');

            var value = selectedStr.split(",");
            for (var i = 0; i < value.length; i++) {
                $("#ddlCity option[value=" + value[i] + "]").prop('selected', true);
            }
          
            $('[id*=ddlCity]').multiselect('refresh');
        }
        function CloseMeCity() {
            debugger;
	        $('#tbxCity').val('');
	        $('#AddCityPopUp').modal('hide');
	        document.getElementById('<%=btnRebindddlCity.ClientID %>').click();
            restoreSelectedCity();
        }
    </script>
</body>
</html>
