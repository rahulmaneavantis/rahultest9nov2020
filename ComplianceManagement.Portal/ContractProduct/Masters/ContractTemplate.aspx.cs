﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using com.VirtuosoITech.ComplianceManagement.Portal.RiskManagement.Process;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Masters
{
    public partial class ContractTemplate : System.Web.UI.Page
    {
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            divMsuccessmsgaCTem.Visible = false;
            if (!IsPostBack)
            {
                flag = false;
                BindTemplates();
                bindPageNumber();
                SetShowingRecords();
            }
        }

        private void BindTemplates()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstTemplates = ContractTemplateManagement.GetTemplates_Paging(customerID, tbxFilter.Text.Trim().ToString());

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstTemplates = lstTemplates.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstTemplates = lstTemplates.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (lstTemplates.Count > 0)
                {
                    Session["TotalRows"] = lstTemplates.Count;
                    grdContractTemplates.DataSource = lstTemplates;
                    grdContractTemplates.DataBind();
                }
                else
                {
                    Session["TotalRows"] = lstTemplates.Count;
                    grdContractTemplates.DataSource = null;
                    grdContractTemplates.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTemplatePage.IsValid = false;
                cvContractTemplatePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractTemplates.PageIndex = 0;
                BindTemplates();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTemplatePage.IsValid = false;
                cvContractTemplatePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = 0;
                string templateID = null;

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenContractModal('" + templateID + "');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTemplatePage.IsValid = false;
                cvContractTemplatePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnSection_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/TemplateSectionMaster.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractTemplates.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindTemplates();

                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContractTemplates.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();

                }
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);

                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                long CustomerID = AuthenticationHelper.CustomerID;
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displayed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void lnkBtn_RebindGrid_Click(object sender, EventArgs e)
        {
            BindTemplates(); bindPageNumber();
        }

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContractTemplates.PageIndex = chkSelectedPage - 1;
            grdContractTemplates.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
            BindTemplates(); SetShowingRecords();
        }

        protected void grdContractTemplates_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                if (e.CommandName.Equals("EDIT_Template"))
                {
                    long templateID = Convert.ToInt64(e.CommandArgument);

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "OpenContractModal('" + templateID + "');", true);

                }
                else if (e.CommandName.Equals("DELETE_Template"))
                {
                    long templateID = Convert.ToInt64(e.CommandArgument);

                    bool deleteSuccess = ContractTemplateManagement.DeleteTemplate(templateID, customerID, AuthenticationHelper.UserID);

                    if (deleteSuccess)
                    {
                        BindTemplates();
                        bindPageNumber();
                        SetShowingRecords();
                        if(cvContractTemplatePage.IsValid)
                        {
                            divMsuccessmsgaCTem.Visible = true;
                            MsuccessmsgaCTem.Text = "Template Deleted Successfully;";
                            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), "script", "window.setTimeout(function() { document.getElementById('" + divMsuccessmsgaCTem.ClientID + "').style.display = 'none' },2000);", true);
                        }
                    }
                }
                else if (e.CommandName.Equals("EXPORT_Template"))
                {
                    long templateID = Convert.ToInt64(e.CommandArgument);

                    if (templateID != 0)
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            var lstSections = (from row in entities.Cont_SP_GetTemplateSectionDetails(customerID, templateID)
                                               select row).ToList();
                            if (lstSections.Count > 0)
                            {
                                //GenerateTemplateDocx(lstSections);
                                ExporttoWord(lstSections);
                            }
                            else
                            {
                                cvContractTemplatePage.IsValid = false;
                                cvContractTemplatePage.ErrorMessage = "No Section Mapped to Selected Template";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTemplatePage.IsValid = false;
                cvContractTemplatePage.ErrorMessage = "Something went wrong. Please try again";
            }
        }

        public void ExporttoWord(List<Cont_SP_GetTemplateSectionDetails_Result> lstSections)
        {
            if (lstSections.Count > 0)
            {
                System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();

                string fileName = "Template";
                string templateName = "Template";

                if (lstSections[0] != null)
                {
                    templateName = lstSections[0].TemplateName;
                    fileName = templateName + "-" + lstSections[0].TemplateVersion;
                }

                strExporttoWord.Append(@"
                <html 
                xmlns:o='urn:schemas-microsoft-com:office:office' 
                xmlns:w='urn:schemas-microsoft-com:office:word'
                xmlns='http://www.w3.org/TR/REC-html40'>
                <head><title></title>

                <!--[if gte mso 9]>
                <xml>
                <w:WordDocument>
                <w:View>Print</w:View>
                <w:Zoom>90</w:Zoom>
                <w:DoNotOptimizeForBrowser/>
                </w:WordDocument>
                </xml>
                <![endif]-->

                <style>
                p.MsoFooter, li.MsoFooter, div.MsoFooter
                {
                margin:0in;
                margin-bottom:.0001pt;
                mso-pagination:widow-orphan;
                tab-stops:center 3.0in right 6.0in;
                font-size:12.0pt;
                }
                <style>

                <!-- /* Style Definitions */

                @page Section1
                {
                size:8.5in 11.0in; 
                margin:1.0in 1.25in 1.0in 1.25in ;
                mso-header-margin:.5in;
                mso-header: h1;
                mso-footer: f1; 
                mso-footer-margin:.5in;
                 font-family:Arial;
                }

                div.Section1
                {
                page:Section1;
                }
                table#hrdftrtbl
                {
                    margin:0in 0in 0in 9in;
                }

                .break { page-break-before: always; }
                -->
                </style></head>");

                strExporttoWord.Append(@"<body lang=EN-US style='tab-interval:.5in'> <div class=Section1>");

                //strExporttoWord.Append(@"<table style='width:100%; font-family:Calibri;border:none;'>");

                lstSections = lstSections.OrderBy(row => row.SectionOrder).ToList();

                strExporttoWord.Append(@"<div style='text-align:center;'>");
                strExporttoWord.Append(@"<p><b>" + templateName + "</b></p>");
                strExporttoWord.Append(@"</div>");

                lstSections.ForEach(eachSection =>
                {
                    strExporttoWord.Append(@"<div>");
                    strExporttoWord.Append(@"<p><b>" + eachSection.Header + "</b></p>");
                    strExporttoWord.Append(@"</div>");

                    strExporttoWord.Append(@"<div>");
                    strExporttoWord.Append(@"<p>" + eachSection.BodyContent + "</p>");
                    strExporttoWord.Append(@"</div>");

                    //strExporttoWord.Append(@"<tr>");
                    //strExporttoWord.Append(@"<td><b>" + eachSection.Header + "</b></td>");
                    //strExporttoWord.Append(@"</tr>");

                    //strExporttoWord.Append(@"<tr>");
                    //strExporttoWord.Append(@"<td>" + eachSection.BodyContent + "</td>");
                    //strExporttoWord.Append(@"</tr>");
                });

                //strExporttoWord.Append(@"</table>");

                strExporttoWord.Append(@"<table id='hrdftrtbl' border='1' cellspacing='0' cellpadding='0'>
                <tr>
                    <td>
                        <div style='mso-element:header' id=h1 >
                            <p class=MsoHeader style='text-align:left'>
                        </div>
                    </td>
                    <td>
                        <div style='mso-element:footer' id=f1>
                            <p class=MsoFooter>
                            <span style=mso-tab-count:2'></span>
                            <span style='mso-field-code:"" PAGE ""'></span>
                            of <span style='mso-field-code:"" NUMPAGES ""'></span>
                            </p>
                        </div>
                    </td>
                </tr>
                </table>
                </body>
                </html>");

                fileName = fileName + "-" + DateTime.Now.ToString("ddMMyyyy");

                Response.AppendHeader("Content-Type", "application/msword");
                Response.AppendHeader("Content-disposition", "attachment; filename=" + fileName + ".doc");
                Response.Write(strExporttoWord);
            }
        }

        public void GenerateTemplateDocx(List<Cont_SP_GetTemplateSectionDetails_Result> lstSections)
        {
            //string fileName = @"D:\DocxExample\exempleWord.docx";
            //var doc = DocX.Create(fileName);

            //System.Text.StringBuilder strExporttoWord = new System.Text.StringBuilder();
            //strExporttoWord.Append(@"<p><b>Ankit Jain</b></p><p><u>Narendra Sonone</u></p><p><span style='background - color: rgb(255, 255, 0);'>Rahul Mane</span></p><p><span style='background - color: rgb(255, 255, 0);'><br></span></p><p><u><br></u></p>");

            //doc.InsertParagraph(strExporttoWord.ToString());
            //doc.Save();            
        }

        protected void lnkPreviousSwitch_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/ContractProduct/Masters/ContCustomFieldMasters.aspx", false);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractTemplates_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstTemplates = ContractTemplateManagement.GetTemplates_Paging(customerID, tbxFilter.Text.Trim().ToString());

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstTemplates = lstTemplates.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstTemplates = lstTemplates.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdContractTemplates.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdContractTemplates.Columns.IndexOf(field);
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (lstTemplates.Count > 0)
                {
                    Session["TotalRows"] = lstTemplates.Count;
                    grdContractTemplates.DataSource = lstTemplates;
                    grdContractTemplates.DataBind();
                }
                else
                {
                    Session["TotalRows"] = lstTemplates.Count;
                    grdContractTemplates.DataSource = null;
                    grdContractTemplates.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvContractTemplatePage.IsValid = false;
                cvContractTemplatePage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection) ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void grdContractTemplates_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }
    }
}