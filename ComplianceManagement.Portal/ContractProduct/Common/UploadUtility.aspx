﻿<%@ Page Title="Upload Utility :: Contract" Language="C#" MasterPageFile="~/ContractProduct.Master" AutoEventWireup="true" CodeBehind="UploadUtility.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Common.UploadUtility" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftuploadmenu');
            fhead('Upload Utility');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="upUploadUtility">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.3;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 30%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upUploadUtility" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget">
                <div class="dashboard">

                    <div class="col-lg-12 col-md-12" style="min-height: 0px; max-height: 250px; overflow-y: auto;">
                        <asp:Panel ID="vdpanel" runat="server" ScrollBars="Auto">
                            <asp:ValidationSummary ID="vsUploadUtility" runat="server"
                                class="alert alert-block alert-danger fade in" DisplayMode="BulletList" ValidationGroup="uploadUtilityValidationGroup" />
                            <asp:CustomValidator ID="cvUploadUtilityPage" runat="server" EnableClientScript="False"
                                ValidationGroup="uploadUtilityValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                        </asp:Panel>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-lg-12 col-md-12">
                        <div runat="server" id="divCase">
                            <div class="col-md-12 colpadding0 entrycount" style="margin-top: 5px;">
                                <ul id="rblRole1" class="nav nav-tabs">
                                    <li class="active" id="liNotDone" runat="server">
                                        <asp:LinkButton ID="Tab1" class="active" runat="server">Import</asp:LinkButton>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix" style="clear: both; height: 15px;"></div>
                            <div class="clearfix" style="clear: both;"></div>

                            <div class="col-md-12 plr0">
                                <div class="col-md-2 pl0">
                                    <asp:RadioButton ID="rdoBtn_Contract" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Contract" OnCheckedChanged="rdoBtn_CheckedChanged" GroupName="uploadContentGroup" />
                                </div>
                                <div class="col-md-2">
                                    <asp:RadioButton ID="rdoBtn_Vendor" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Vendor Master" OnCheckedChanged="rdoBtn_CheckedChanged" GroupName="uploadContentGroup" />
                                </div>

                                <div class="col-md-2">
                                    <asp:RadioButton ID="rdoBtn_ContractType" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Contract Type" OnCheckedChanged="rdoBtn_CheckedChanged" GroupName="uploadContentGroup" />
                                </div>
                                <div class="col-md-2">
                                    <asp:RadioButton ID="rdoBtn_ContractSubType" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Contract Sub Type" OnCheckedChanged="rdoBtn_CheckedChanged" GroupName="uploadContentGroup" />
                                </div>
                                <div class="col-md-2">
                                    <asp:RadioButton ID="rdoBtn_DocType" runat="server" AutoPostBack="True" Style="display: block; font-size: 13px; color: #333;" Text="Document Type" OnCheckedChanged="rdoBtn_CheckedChanged" GroupName="uploadContentGroup" />
                                </div>
                                <div class="col-md-2"></div>
                            </div>

                            <div class="clearfix" style="clear: both; height: 15px;"></div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-4 colpadding0">
                                    <asp:FileUpload ID="MasterFileUpload" runat="server" Style="display: block; font-size: 13px; color: #333;" />
                                    <asp:RequiredFieldValidator ID="rfvFileUpload" ErrorMessage="Please Select File" ControlToValidate="MasterFileUpload"
                                        runat="server" Display="None" ValidationGroup="uploadUtilityValidationGroup" />
                                </div>

                                <div class="col-md-2 colpadding0 text-right">
                                    <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="uploadUtilityValidationGroup"
                                        class="btn btn-primary" OnClick="btnUploadFile_Click" OnClientClick="showProgress()" />
                                </div>

                                <div class="col-md-6 colpadding0 text-right">
                                    <asp:LinkButton ID="lnkSampleFormat" class="newlink" Font-Underline="True" OnClick="lnkSampleFormat_Click"
                                        data-toggle="tooltip" data-placement="bottom" runat="server" Text="Sample Format(Excel)"
                                        ToolTip="Download Sample Excel Document Format for Contract/ Vendor/ Contract Type/ Contract Sub-Type Upload"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUploadFile" />
            <asp:PostBackTrigger ControlID="lnkSampleFormat" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
