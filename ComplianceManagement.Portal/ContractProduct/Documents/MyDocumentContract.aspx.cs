﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace com.VirtuosoITech.ComplianceManagement.Portal.ContractProduct.Documents
{
    public partial class MyDocumentContract : System.Web.UI.Page
    {
        public static string ContractnoDocPath = "";
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    BindVendors();

                    //Bind Tree Views
                    //var branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                    List<NameValueHierarchy> branchList;
                    string key = "LocationHierarchy" + AuthenticationHelper.CustomerID;
                    if (CacheHelper.Exists(key))
                    {
                        CacheHelper.Get<List<NameValueHierarchy>>(key, out branchList);
                    }
                    else
                    {
                        branchList = CustomerBranchManagement.GetAllHierarchyManagementSatutory(Convert.ToInt32(customerID));
                        CacheHelper.Set<List<NameValueHierarchy>>(key, branchList);
                    }
                    //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);

                    BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);

                    BindDepartment();
                    BindContractStatusList(false, true);
                   
                    BindGrid();
                   
                    BindFileTags();

                    applyCSStoFileTag_ListItems();

                    bindPageNumber();
                    SetShowingRecords();
                }

                //applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        {
            try
            {
                treetoBind.Nodes.Clear();

                NameValueHierarchy branch = null;

                if (branchList.Count > 0)
                {
                    branch = branchList[0];
                }

                treeTxtBox.Text = "Select Entity/Branch/Location";

                List<TreeNode> nodes = new List<TreeNode>();

                BindBranchesHierarchy(null, branch, nodes);

                foreach (TreeNode item in nodes)
                {
                    treetoBind.Nodes.Add(item);
                }

                treetoBind.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown
            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindVendors()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var lstVendors = ContractMastersManagement.GetVendors_All(customerID);

            ddlVendorPage.DataTextField = "VendorName";
            ddlVendorPage.DataValueField = "ID";

            ddlVendorPage.DataSource = lstVendors;
            ddlVendorPage.DataBind();

            ddlVendorPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        private void BindContractStatusList(bool isForTask, bool isVisibleToUser)
        {
            try
            {
                ddlContractStatus.DataSource = null;
                ddlContractStatus.DataBind();
                ddlContractStatus.ClearSelection();

                ddlContractStatus.DataTextField = "StatusName";
                ddlContractStatus.DataValueField = "ID";

                var statusList = ContractTaskManagement.GetStatusList_All(isForTask, isVisibleToUser);

                ddlContractStatus.DataSource = statusList;
                ddlContractStatus.DataBind();

                ddlContractStatus.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        public void BindGrid()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;
                int pageSize = -1;
                int pageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedValue))
                {
                    pageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                }

                pageNumber = grdContractList.PageIndex + 1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt32(ddlContractStatus.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                List<ListItem> selectedItems = ContractManagement.GetSelectedItems(lstBoxFileTags);

                var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                var lstAssignedContracts = ContractDocumentManagement.GetAssigned_MyDocuments(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    3, branchList, vendorID, deptID, contractStatusID, contractTypeID, selectedFileTags);


                #region Filter Code
                if (!string.IsNullOrEmpty(tbxFilter.Text))
                {
                    if (CheckInt(tbxFilter.Text))
                    {
                        int a = Convert.ToInt32(tbxFilter.Text.ToUpper());
                        lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ContractID == a || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                        //&& entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())
                    }
                    else
                    {
                        lstAssignedContracts = lstAssignedContracts.Where(entry => entry.ContractTitle.ToUpper().Contains(tbxFilter.Text.ToUpper()) || entry.ContractNo.ToUpper().Contains(tbxFilter.Text.ToUpper())).ToList();
                    }
                }
                #endregion

                //var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                //    3, branchList, vendorID, deptID, contractStatusID, contractTypeID, pageSize, pageNumber);

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstAssignedContracts = lstAssignedContracts.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstAssignedContracts = lstAssignedContracts.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (lstAssignedContracts.Count > 0)
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = lstAssignedContracts.Count;
                }
                else
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = null;
                }
                lstAssignedContracts.Clear();
                lstAssignedContracts = null;
                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Something went wrong, Please try again.";
            }
        }

        private void BindFileTags()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                var lstTags = ContractDocumentManagement.GetDistinctFileTags(customerID);

                lstBoxFileTags.Items.Clear();

                if (lstTags.Count > 0)
                {
                    for (int i = 0; i < lstTags.Count; i++)
                    {
                        lstBoxFileTags.Items.Add(new ListItem(lstTags[i], lstTags[i]));
                    }
                }

                if (lstTags.Count > 0)
                    outerDivFileTags.Visible = true;
                else
                    outerDivFileTags.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void applyCSStoFileTag_ListItems()
        {
            foreach (ListItem eachItem in lstBoxFileTags.Items)
            {
                if (eachItem.Selected)
                    eachItem.Attributes.Add("class", "item label label-info-selected");
                else
                    eachItem.Attributes.Add("class", "item label label-info");
            }
        }

        protected void lstBoxFileTags_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();

                List<ListItem> lstSelectedTags = new List<ListItem>();

                foreach (ListItem eachListItem in lstBoxFileTags.Items)
                {
                    if (eachListItem.Selected)
                        lstSelectedTags.Add(eachListItem);
                }

                //Re-Arrange File Tags
                var arrangedListItems = ContractManagement.ReArrange_FileTags(lstBoxFileTags);

                lstBoxFileTags.DataSource = arrangedListItems;
                lstBoxFileTags.DataBind();

                foreach (ListItem eachListItem in lstSelectedTags)
                {
                    if (lstBoxFileTags.Items.FindByValue(eachListItem.Value) != null)
                        lstBoxFileTags.Items.FindByValue(eachListItem.Value).Selected = true;
                }

                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SetShowingRecords()
        {
            if (Session["TotalRows"] != null)
            {
                int PageSize = 0;
                int PageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedItem.Text))
                    PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);

                if (!string.IsNullOrEmpty(DropDownListPageNo.SelectedValue))
                    PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }

                if (TotalRecord != 0)
                    lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                else
                    lblStartRecord.Text = "0";

                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();
                bindPageNumber();
                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdContractList.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }

                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void btnAddContract_Click(object sender, EventArgs e)
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + 0 + ");", true);
        //}

        //protected void lnkEditContract_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        LinkButton btn = (LinkButton) (sender);
        //        if (btn != null)
        //        {
        //            int contractInstanceID = Convert.ToInt32(btn.CommandArgument);

        //            if (contractInstanceID != 0)
        //            {
        //                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + contractInstanceID + ");", true);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvErrorContractListPage.IsValid = false;
        //        cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                var lstdocTypes = ContractMastersManagement.GetContractDocTypes_All(customerID);
                DropDownList ddlDocType = (DropDownList)e.Row.FindControl("ddlDocType");

                if (ddlDocType != null)
                {
                    ddlDocType.DataTextField = "TypeName";
                    ddlDocType.DataValueField = "ID";

                    ddlDocType.DataSource = lstdocTypes;
                    ddlDocType.DataBind();

                    ddlDocType.Items.Insert(0, new ListItem("All", "0"));
                }
            }
        }
        protected void grdContractList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    long customerID = -1;
                    customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                    int userID = AuthenticationHelper.UserID;

                    if (e.CommandName.Equals("Download"))
                    {
                        long docTypeID = 0;
                        var TypeName = string.Empty;
                        GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                        int RowIndex = gvr.RowIndex;
                        DropDownList ddlDocType = (DropDownList)grdContractList.Rows[RowIndex].FindControl("ddlDocType");

                        if (!string.IsNullOrEmpty(ddlDocType.SelectedValue))
                        {
                            docTypeID = Convert.ToInt64(ddlDocType.SelectedValue);
                            TypeName = ddlDocType.SelectedItem.Text;
                            ViewState["DocTypeID"] = docTypeID;
                        }
                        long contractID = Convert.ToInt64(e.CommandArgument);
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        List<Cont_tbl_FileData> CMPDocuments = new List<Cont_tbl_FileData>();

                        if (CMPDocuments != null)
                        {
                            using (ZipFile ContractZip = new ZipFile())
                            {
                                CMPDocuments = ContractDocumentManagement.GetContractDocumentsByDocTypeID(Convert.ToInt64(contractID), docTypeID);

                                if (CMPDocuments.Count > 0)
                                {
                                    int i = 0;
                                    foreach (var file in CMPDocuments)
                                    {
                                        string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            int idx = file.FileName.LastIndexOf('.');
                                            string str = file.FileName.Substring(0, idx) + "_" + i + "." + file.FileName.Substring(idx + 1);
                                            string Dates = DateTime.Now.ToString("dd/mm/yyyy");
                                            if (file.EnType == "M")
                                            {
                                                ContractZip.AddEntry("Contract Document" + "/" + TypeName + "_" + Dates + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ContractZip.AddEntry("Contract Document" + "/" + TypeName + "_" + Dates + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }

                                            i++;
                                        }
                                    }
                                    var zipMs = new MemoryStream();
                                    ContractZip.Save(zipMs);
                                    zipMs.Position = 0;
                                    byte[] Filedata = zipMs.ToArray();
                                    Response.Buffer = true;
                                    Response.ClearContent();
                                    Response.ClearHeaders();
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=ContractDocument.zip");
                                    Response.BinaryWrite(Filedata);
                                    Response.Flush();
                                    //Response.End();
                                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                                }
                                else
                                {
                                    cvErrorContractListPage.IsValid = false;
                                    cvErrorContractListPage.ErrorMessage = "No Document available to Download.";
                                }
                            }
                        }
                    }
                    else if (e.CommandName == "View")
                    {
                        long docTypeID = 0;
                        var TypeName = string.Empty;
                        GridViewRow gvr = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                        int RowIndex = gvr.RowIndex;
                        DropDownList ddlDocType = (DropDownList)grdContractList.Rows[RowIndex].FindControl("ddlDocType");

                        if (!string.IsNullOrEmpty(ddlDocType.SelectedValue))
                        {
                            docTypeID = Convert.ToInt64(ddlDocType.SelectedValue);
                            TypeName = ddlDocType.SelectedItem.Text;
                            ViewState["DocTypeID"] = docTypeID;
                        }
                        long contractID = Convert.ToInt64(e.CommandArgument);
                        string[] commandArg = e.CommandArgument.ToString().Split(',');
                        Session["ContractInstanceID"] = commandArg[0];
                        Session["DocTypes"] = docTypeID;

                        List<Cont_tbl_FileData> CMPDocuments = ContractDocumentManagement.GetContractDocumentsByDocTypeID(Convert.ToInt64(contractID), docTypeID);

                        if (CMPDocuments != null)
                        {
                            List<Cont_tbl_FileData> entitiesData = CMPDocuments.Where(entry => entry.Version != null).ToList();
                            if (CMPDocuments.Where(entry => entry.Version == null).ToList().Count > 0)
                            {
                                Cont_tbl_FileData entityData = new Cont_tbl_FileData();
                                entityData.Version = "1.0";
                                entityData.ContractID = Convert.ToInt64(commandArg[0]);
                                entitiesData.Add(entityData);
                            }

                            if (entitiesData.Count > 0)
                            {
                                rptLitigationVersionView.DataSource = entitiesData.OrderBy(entry => entry.Version);
                                rptLitigationVersionView.DataBind();

                                bool docViewSuccess = false;
                                foreach (var file in CMPDocuments)
                                {
                                    string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string Folder = "~/TempFiles/Contract";
                                        string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                        string DateFolder = Folder + "/" + File;

                                        string extension = System.IO.Path.GetExtension(filePath);

                                        if (!Directory.Exists(DateFolder))
                                        {
                                            Directory.CreateDirectory(Server.MapPath(DateFolder));
                                        }

                                        string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                        string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                        string FileName = DateFolder + "/" + User + "" + extension;

                                        FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                        BinaryWriter bw = new BinaryWriter(fs);
                                        if (file.EnType == "M")
                                        {
                                            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        bw.Close();

                                        ContractnoDocPath = FileName;
                                        ContractnoDocPath = ContractnoDocPath.Substring(2, ContractnoDocPath.Length - 2);
                                        docViewSuccess = true;

                                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileContract('" + ContractnoDocPath + "');", true);
                                        lblMessage.Text = "";
                                        applyCSStoFileTag_ListItems();
                                        break;
                                    }
                                }

                                if (!docViewSuccess)
                                {
                                    cvErrorContractListPage.IsValid = false;
                                    cvErrorContractListPage.ErrorMessage = "There is no file to preview";
                                    applyCSStoFileTag_ListItems();
                                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptLitigationVersionView_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                long ContractInstanceID = Convert.ToInt64(Session["ContractInstanceID"]);

                long docTypeID = 0;
                var CheckType = string.Empty;
                var NameOfDocu = string.Empty;

                if (ViewState["LitstOfddlValue"] != null)
                {
                    docTypeID = Convert.ToInt32(ViewState["DocTypeID"]);
                }

                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<Cont_tbl_FileData> ContractFileData = new List<Cont_tbl_FileData>();
                List<Cont_tbl_FileData> ContractDocument = new List<Cont_tbl_FileData>();
                ContractDocument = ContractDocumentManagement.GetContractDocumentsByDocTypeID(Convert.ToInt64(ContractInstanceID), docTypeID);

                if (commandArgs[1].Equals("1.0"))
                {
                    ContractFileData = ContractDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ContractFileData.Count <= 0)
                    {
                        ContractFileData = ContractDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ContractFileData = ContractDocument.Where(entry => entry.FileName == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("View"))
                {
                    if (ContractFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ContractFileData)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;
                                ContractnoDocPath = FileName;

                                ContractnoDocPath = ContractnoDocPath.Substring(2, ContractnoDocPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileContract('" + ContractnoDocPath + "');", true);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorContractListPage.IsValid = false;
                cvErrorContractListPage.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void rptLitigationVersionView_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                LinkButton lblIDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblIDocumentVersionView);

            }
        }
        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdContractList.PageIndex = chkSelectedPage - 1;

            grdContractList.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid();
        }

     
        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
                SetShowingRecords();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnClearFilter_Click(object sender, EventArgs e)
        {
            try
            {
                ddlDeptPage.ClearSelection();
                ddlVendorPage.ClearSelection();
                ddlContractStatus.ClearSelection();
                tbxFilter.Text = string.Empty;
                //tbxFilterLocation.Text = "";
                ClearTreeViewSelection(tvFilterLocation);

                tvFilterLocation_SelectedNodeChanged(sender, e);
                lnkBtnApplyFilter_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void ClearTreeViewSelection(TreeView tree)
        {
            if (tree.SelectedNode != null)
            {
                tree.SelectedNode.Selected = false;
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode != null ? tvFilterLocation.SelectedNode.Text : "All";
                //tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;

                int branchID = -1;
                int vendorID = -1;
                int deptID = -1;
                long contractStatusID = -1;
                long contractTypeID = -1;
                int pageSize = -1;
                int pageNumber = 0;

                if (!string.IsNullOrEmpty(ddlPageSize.SelectedValue))
                {
                    pageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                }

                pageNumber = grdContractList.PageIndex + 1;

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlVendorPage.SelectedValue))
                {
                    vendorID = Convert.ToInt32(ddlVendorPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlContractStatus.SelectedValue))
                {
                    contractStatusID = Convert.ToInt32(ddlContractStatus.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                List<ListItem> selectedItems = ContractManagement.GetSelectedItems(lstBoxFileTags);

                var selectedFileTags = selectedItems.Select(row => row.Text).ToList();

                var lstAssignedContracts = ContractDocumentManagement.GetAssigned_MyDocuments(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                    3, branchList, vendorID, deptID, contractStatusID, contractTypeID, selectedFileTags);

                //var lstAssignedContracts = ContractManagement.GetAssignedContractsList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.Role,
                //    3, branchList, vendorID, deptID, contractStatusID, contractTypeID, pageSize, pageNumber);

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstAssignedContracts = lstAssignedContracts.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstAssignedContracts = lstAssignedContracts.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                }

                ViewState["SortExpression"] = e.SortExpression;

                foreach (DataControlField field in grdContractList.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdContractList.Columns.IndexOf(field);
                    }
                }
                flag = true;
                Session["TotalRows"] = null;
                if (lstAssignedContracts.Count > 0)
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = lstAssignedContracts.Count;
                }
                else
                {
                    grdContractList.DataSource = lstAssignedContracts;
                    grdContractList.DataBind();
                    Session["TotalRows"] = null;
                }
                applyCSStoFileTag_ListItems();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdContractList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

       #region Filter code
        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            BindGrid();
            bindPageNumber();
            SetShowingRecords();
        }
        #endregion
    }
}