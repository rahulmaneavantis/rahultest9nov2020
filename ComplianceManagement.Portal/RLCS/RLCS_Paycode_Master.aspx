﻿<%@ Page Title="Masters :: Paycode Master" Language="C#" AutoEventWireup="true" MasterPageFile="~/HRPlusCompliance.Master" CodeBehind="RLCS_Paycode_Master.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_Paycode_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <title></title>

     <style>
        .k-grid-content {
            min-height: auto;
        }

        .k-grid td {
            line-height: 2.0em;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/javascript">

        function checkDeductionType(value) {
            if (value.localeCompare("D")) {
                return "Deduction";
            } else if (value.localeCompare("E")) {
                return "Earning";
            }
        }

        function checkYesOrNO(value) {
            if (value.localeCompare("Y")) {
                return "Yes";
            } else if (value.localeCompare("N")) {
                return "No";
            }
        }

        function BindFirstGrid() {
            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 50,
                    transport: {
                        read: {
                            url: '<% =Path%>GetAll_PayCodeMaster?CustID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 50,
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                        total: function (response) {
                            return response.Result.length;
                        }
                    }
                },
                //toolbar: kendo.template($("#template").html()),
                //height: 471,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    // pageSizes: true,
                    //pageSizes: [ 10 , 25, 50 ],
                    change: function (e) {
                        //$('#chkAll').removeAttr('checked');
                        //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: "5%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "PEM_Pay_Code", title: 'Pay Code',
                        width: "10%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "PEM_Pay_Code_Description", title: 'Description',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "PEM_Deduction_Type", title: 'Deduction Type',
                        width: "15%",
                        template: '#:checkDeductionType(PEM_Deduction_Type)#',
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "PEM_appl_ESI", title: 'ESI Applicability',
                        width: "10%",
                        template: '#:checkYesOrNO(PEM_appl_ESI)#',
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "PEM_Appl_PF", title: 'PF Applicability',
                        width: "10%",
                        template: '#:checkYesOrNO(PEM_Appl_PF)#',
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    contains: "Contains",
                                    eq: "Is equal to",
                                    neq: "Is not equal to"
                                }
                            }
                        }
                    },

                     {
                         field: "PEM_Appl_PT", title: 'PT Applicability',
                         width: "10%",
                         template: '#:checkYesOrNO(PEM_Appl_PT)#',
                         filterable: {
                             extra: false,
                             operators: {
                                 string: {
                                     contains: "Contains",
                                     eq: "Is equal to",
                                     neq: "Is not equal to"
                                 }
                             }
                         }
                     },

                      {
                          field: "PEM_Appl_LWF", title: 'LWF Applicability',
                          width: "10%",
                          template: '#:checkYesOrNO(PEM_Appl_LWF)#',
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      contains: "Contains",
                                      eq: "Is equal to",
                                      neq: "Is not equal to"
                                  }
                              }
                          }
                      },
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });
        }

        $(document).ready(function () {
            BindFirstGrid();

            $("#btnSearch").click(function () {
                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#grid").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 50,
                        filter: {
                            logic: "or",
                            filters: [
                              { field: "PEM_Pay_Code", operator: "contains", value: $x },
                              { field: "PEM_Pay_Code_Description", operator: "contains", value: $x },
                              { field: "PEM_Deduction_Type", operator: "contains", value: $x }
                            ]
                        }
                    });
                   
                    return false;
                }
                else {
                    BindFirstGrid();
                }
                // return false;
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
        </div>
    </div>

    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-6 colpadding0">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 70%" placeholder="Type Text to Search..." />
                <button id="btnSearch" class="btn btn-primary">Search</button>
            </div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid" style="border: none;">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

