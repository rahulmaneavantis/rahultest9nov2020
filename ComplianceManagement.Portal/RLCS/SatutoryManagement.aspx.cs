﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class SatutoryManagement : System.Web.UI.Page
    {
        protected string pointname;
        protected string attrubute;
        protected int customerid;
        protected int branchid;
        protected DateTime FromDate;
        protected DateTime Enddate;
        protected string Filter;
        protected int functionid;
        protected string satutoryinternal;
        protected string chartname;
        protected string listcategoryid;
        protected static string CompDocReviewPath = "";
        protected static int Userid;
        protected static bool IsApprover = false;
        public static List<long> Branchlist = new List<long>();

        protected string Performername;
        protected string Reviewername;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID,AuthenticationHelper.ProfileID)).ToList();

                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                
                                BindLocationFilter(assignedbranchIDs);
                                BindStatus();

                                BindDetailView();

                                btnDownload.Visible = false;

                                GetPageDisplaySummary();

                                if (tvFilterLocation.SelectedValue != "-1")
                                    Session["LocationName"] = tvFilterLocation.SelectedNode.Text;
                                else
                                    Session["LocationName"] = tvFilterLocation.Nodes[1].Text;

                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                            }
                        }                    
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }                
            }
        }
        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                Reviewername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);         
                return "";
            }
        }
        protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
                return "";
            }

        }


        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;              
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                tvFilterLocation.Nodes.Clear();
                string isstatutoryinternal = "";
                string stateid = "";
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    satutoryinternal = Request.QueryString["Internalsatutory"];
                }
                if (satutoryinternal == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (satutoryinternal == "Internal")
                {
                    isstatutoryinternal = "I";
                }
                if (!string.IsNullOrEmpty(Request.QueryString["stateid"]))
                {
                    stateid = Request.QueryString["stateid"];
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var state = from s in entities.RLCS_State_Mapping where s.SM_PanIndia_ID == stateid select s.AVACOM_StateID;
                    }
                }

                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;                    
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation.Nodes.Clear();

                    string isstatutoryinternal = "";
                    //if (ddlStatus.SelectedValue == "Statutory")
                    //{
                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        satutoryinternal = Request.QueryString["Internalsatutory"];
                    }
                    if (satutoryinternal == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (satutoryinternal == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindStatus()
        {
            try
            {
                int Count = 0;
                foreach (ManagementFilterNewStatus r in Enum.GetValues(typeof(ManagementFilterNewStatus)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(ManagementFilterNewStatus), r), r.ToString().Trim());
                    ddlStatus.Items.Add(item);
                    Count++;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }        

        protected bool ViewDownload(int statusid)
        {
            if (statusid != 1)
                return true;
            else
                return false;
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindDetailView(string TwelveMonth = "")
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int CustBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                if (CustBranchID != -1)
                {
                    branchid = CustBranchID;
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                {
                    branchid = Convert.ToInt32(Request.QueryString["branchid"]);
                }

                Branchlist.Clear();
                GetAllHierarchy(customerID, Convert.ToInt32(branchid));
                Branchlist.ToList();

                if (!string.IsNullOrEmpty(Request.QueryString["pointname"]))
                {
                    pointname = Request.QueryString["pointname"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["attrubute"]))
                {
                    attrubute = Request.QueryString["attrubute"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
               
                if (!string.IsNullOrEmpty(Request.QueryString["FromDate"]))
                {
                    FromDate = Convert.ToDateTime(Request.QueryString["FromDate"]);
                }
                else
                {
                    FromDate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Enddate"]))
                {
                    Enddate = Convert.ToDateTime(Request.QueryString["Enddate"]);
                }
                else
                {
                    Enddate = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Filter"]))
                {
                    Filter = Request.QueryString["Filter"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["functionid"]))
                {
                    functionid = Convert.ToInt32(Request.QueryString["functionid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    satutoryinternal = Request.QueryString["Internalsatutory"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["ChartName"]))
                {
                    chartname = Request.QueryString["ChartName"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["listcategoryid"]))
                {
                    listcategoryid = Request.QueryString["listcategoryid"];
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Userid"]))
                {
                    Userid = Convert.ToInt32(Request.QueryString["Userid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }                
                if (satutoryinternal == "Statutory")
                {
                    List<int?> CategoryIds = new List<int?>();
                    if (!string.IsNullOrEmpty(listcategoryid))
                    {
                        if (listcategoryid != "0")
                        {
                            string[] val = listcategoryid.Split(',');
                            if (val.Length > 0)
                            {
                                for (int i = 0; i < val.Length; i++)
                                {
                                    CategoryIds.Add(Convert.ToInt32(val[i]));
                                }
                            }
                        }
                    }
                    List<int> statusIds = new List<int>();
                    string lable = string.Empty;
                    int riskid = 0;

                    #region Function Bar Chart
                    if (chartname == "Functionbarchart")
                    {
                        if (attrubute.Trim().Equals("High"))
                        {
                            riskid = 0;
                        }
                        else if (attrubute.Trim().Equals("Medium"))
                        {
                            riskid = 1;
                        }
                        else if (attrubute.Trim().Equals("Low"))
                        {
                            riskid = 2;
                        }

                        if (pointname.Trim().Equals("After due date"))
                        {
                            statusIds.Add(5);
                            statusIds.Add(9);
                        }
                        else if (pointname.Trim().Equals("In Time"))
                        {
                            statusIds.Add(4);
                            statusIds.Add(7);
                        }
                        else if (pointname.Trim().Equals("Not completed"))
                        {
                            //statusIds.Add(1);
                            //statusIds.Add(10);
                            //statusIds.Add(6);
                            //statusIds.Add(2);
                            //statusIds.Add(3);

                            statusIds.Add(1);
                            statusIds.Add(10);
                            statusIds.Add(8);
                            statusIds.Add(6);
                            statusIds.Add(2);
                            statusIds.Add(3);
                        }
                        else
                        {
                            statusIds.Add(1);
                            statusIds.Add(6);
                            statusIds.Add(10);
                            statusIds.Add(4);
                            statusIds.Add(5);
                            statusIds.Add(7);
                            statusIds.Add(9);
                            statusIds.Add(8);
                            statusIds.Add(2);
                            statusIds.Add(3);
                        }

                        grdSummaryDetails.DataSource = null;
                        grdSummaryDetails.DataBind();

                        if (customerid != -1 && branchid != -1 || ddlMonth.SelectedValue != null && ddlMonth.SelectedValue != "-1" || ddlYear.SelectedValue != null && ddlYear.SelectedValue != "-1" || ddlrisk.SelectedValue != null && ddlrisk.SelectedValue != "-1")
                        {
                            List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                            detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerid, Branchlist, branchid, statusIds, Filter, CategoryIds, riskid, attrubute, ddlMonth.SelectedValue, ddlYear.SelectedValue, ddlrisk.SelectedValue, FromDate, Enddate, AuthenticationHelper.UserID, IsApprover);

                            lblDetailViewTitle.Text = lable;                            

                            //if (CustBranchID != -1)
                            //    detailView = detailView.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                            ManagementFilterNewStatus status = (ManagementFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                            switch (status)
                            {
                                case ManagementFilterNewStatus.Overdue:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedTimely:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 4).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedDelayed:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 5).ToList();
                                    break;

                                case ManagementFilterNewStatus.PendingForReview:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                                    break;

                                case ManagementFilterNewStatus.Rejected:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 6).ToList();
                                    break;
                            }
                            Session["TotalRows"] = detailView.Count;
                            grdSummaryDetails.DataSource = detailView;
                            grdSummaryDetails.DataBind();
                        }
                        else
                        {
                            List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                            detailView = RLCSManagement.FunctionBARGetManagementDetailView(customerid, Branchlist, statusIds, Filter, Convert.ToInt32(functionid), riskid, pointname, FromDate, Enddate, AuthenticationHelper.UserID);                            
                             
                            lblDetailViewTitle.Text = lable;
                          
                            //if (CustBranchID != -1)
                            //    detailView = detailView.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                            ManagementFilterNewStatus status = (ManagementFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);

                            switch (status)
                            {
                                case ManagementFilterNewStatus.Overdue:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedTimely:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 4).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedDelayed:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 5).ToList();
                                    break;

                                case ManagementFilterNewStatus.PendingForReview:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                                    break;

                                case ManagementFilterNewStatus.Rejected:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 6).ToList();
                                    break;
                            }

                            Session["TotalRows"] = detailView.Count;

                            grdSummaryDetails.DataSource = detailView;
                            grdSummaryDetails.DataBind();
                        }

                        UpDetailView.Update();
                        Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<SP_GetCannedReportCompliancesSummary_Result>).ToDataTable();
                    }
                    #endregion

                    #region Function Pie Chart
                    else if (chartname == "FunctionPiechart")
                    {
                        if (pointname.Trim().Equals("High"))
                        {
                            riskid = 0;
                        }
                        else if (pointname.Trim().Equals("Medium"))
                        {
                            riskid = 1;
                        }
                        else if (pointname.Trim().Equals("Low"))
                        {
                            riskid = 2;
                        }
                        if (!string.IsNullOrEmpty(attrubute))
                        {
                            if (attrubute.Trim().Equals("After due date"))
                            {
                                statusIds.Add(5);
                                statusIds.Add(9);
                            }
                            else if (attrubute.Trim().Equals("In Time"))
                            {
                                statusIds.Add(4);
                                statusIds.Add(7);
                            }
                            else if (attrubute.Trim().Equals("Not completed"))
                            {
                                statusIds.Add(1);
                                statusIds.Add(10);
                                statusIds.Add(8);
                                statusIds.Add(6);
                                statusIds.Add(2);
                                statusIds.Add(3);
                            }
                            //Comment by Rhul on 19 April 2018
                            //else if (attrubute.Trim().Equals("Overdue"))
                            //{
                            //    statusIds.Add(1);
                            //    statusIds.Add(10);
                            //    statusIds.Add(6);
                            //    statusIds.Add(2);
                            //    statusIds.Add(3);
                            //}
                            //else if (attrubute.Trim().Equals("Upcoming"))
                            //{
                            //    statusIds.Add(1);
                            //    statusIds.Add(10);
                            //    statusIds.Add(6);
                            //    statusIds.Add(2);
                            //    statusIds.Add(3);
                            //}
                            else
                            {
                                statusIds.Add(1);
                                statusIds.Add(6);
                                statusIds.Add(10);
                                statusIds.Add(4);
                                statusIds.Add(5);
                                statusIds.Add(7);
                                statusIds.Add(9);
                                statusIds.Add(8);
                                statusIds.Add(2);
                                statusIds.Add(3);
                            }
                        }

                        if (customerid != -1 && branchid != -1 || ddlMonth.SelectedValue != null && ddlMonth.SelectedValue != "-1" || ddlYear.SelectedValue != null && ddlYear.SelectedValue != "-1" || ddlrisk.SelectedValue != null && ddlrisk.SelectedValue != "-1")
                        {
                            List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                            detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerid, Branchlist, branchid, statusIds, Filter, CategoryIds, riskid,attrubute,ddlMonth.SelectedValue,ddlYear.SelectedValue,ddlrisk.SelectedValue,FromDate, Enddate, AuthenticationHelper.UserID, IsApprover);
                            
                            lblDetailViewTitle.Text = lable;   
                                                
                            //if (CustBranchID != -1)
                            //    detailView = detailView.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                            ManagementFilterNewStatus status = (ManagementFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                            switch (status)
                            {
                                case ManagementFilterNewStatus.Overdue:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedTimely:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 4).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedDelayed:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 5).ToList();
                                    break;

                                case ManagementFilterNewStatus.PendingForReview:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                                    break;

                                case ManagementFilterNewStatus.Rejected:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 6).ToList();
                                    break;
                            }
                            Session["TotalRows"] = detailView.Count;
                            grdSummaryDetails.DataSource = detailView;
                            grdSummaryDetails.DataBind();
                        }
                        else
                        {
                            List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                            detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerid, Branchlist, statusIds, Filter, CategoryIds, riskid, attrubute, FromDate, Enddate, AuthenticationHelper.UserID, IsApprover);
                            
                            lblDetailViewTitle.Text = lable;                          
                            //if (CustBranchID != -1)
                            //    detailView = detailView.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                            ManagementFilterNewStatus status = (ManagementFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                            switch (status)
                            {
                                case ManagementFilterNewStatus.Overdue:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedTimely:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 4).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedDelayed:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 5).ToList();
                                    break;

                                case ManagementFilterNewStatus.PendingForReview:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                                    break;

                                case ManagementFilterNewStatus.Rejected:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 6).ToList();
                                    break;
                            }
                            Session["TotalRows"] = detailView.Count;
                            grdSummaryDetails.DataSource = detailView;
                            grdSummaryDetails.DataBind();
                        }

                        UpDetailView.Update();
                        Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<SP_GetCannedReportCompliancesSummary_Result>).ToDataTable();

                    }
                    #endregion

                    #region Risk Bar Chart
                    else if (chartname == "RiskBARchart")
                    {

                        if (!string.IsNullOrEmpty(pointname))
                        {
                            if (pointname.Trim().Equals("High"))
                            {
                                riskid = 0;
                            }
                            else if (pointname.Trim().Equals("Medium"))
                            {
                                riskid = 1;
                            }
                            else if (pointname.Trim().Equals("Low"))
                            {
                                riskid = 2;
                            }
                        }
                        if (!string.IsNullOrEmpty(attrubute))
                        {
                            if (attrubute.Trim().Equals("After due date"))
                            {
                                statusIds.Add(5);
                                statusIds.Add(9);
                            }
                            else if (attrubute.Trim().Equals("In Time"))
                            {
                                statusIds.Add(4);
                                statusIds.Add(7);
                            }
                            else if (attrubute.Trim().Equals("Not completed"))
                            {
                                statusIds.Add(1);
                                statusIds.Add(10);
                                statusIds.Add(8);
                                statusIds.Add(6);
                                statusIds.Add(2);
                                statusIds.Add(3);
                            }
                            else
                            {
                                statusIds.Add(1);
                                statusIds.Add(6);
                                statusIds.Add(10);
                                statusIds.Add(4);
                                statusIds.Add(5);
                                statusIds.Add(7);
                                statusIds.Add(9);
                                statusIds.Add(8);
                                statusIds.Add(2);
                                statusIds.Add(3);
                            }
                        }
                        if (customerid != -1 && branchid != -1 || ddlMonth.SelectedValue != "-1" && ddlMonth.SelectedValue != null || ddlYear.SelectedValue != "-1" && ddlYear.SelectedValue != null || ddlrisk.SelectedValue != "-1" && ddlrisk.SelectedValue != null)
                        {
                            List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                            detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerid, Branchlist, branchid, statusIds, Filter, CategoryIds, riskid, attrubute, ddlMonth.SelectedValue, ddlYear.SelectedValue, ddlrisk.SelectedValue, FromDate, Enddate, AuthenticationHelper.UserID, IsApprover);
                            
                            lblDetailViewTitle.Text = lable;
                                                     
                            //if (CustBranchID != -1)
                            //    detailView = detailView.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                            ManagementFilterNewStatus status = (ManagementFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);

                            switch (status)
                            {
                                case ManagementFilterNewStatus.Overdue:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedTimely:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 4).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedDelayed:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 5).ToList();
                                    break;

                                case ManagementFilterNewStatus.PendingForReview:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                                    break;

                                case ManagementFilterNewStatus.Rejected:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 6).ToList();
                                    break;
                            }

                            Session["TotalRows"] = detailView.Count;
                            grdSummaryDetails.DataSource = detailView;
                            grdSummaryDetails.DataBind();

                          }
                        else
                        {
                            List<SP_GetCannedReportCompliancesSummary_Result> detailView = new List<SP_GetCannedReportCompliancesSummary_Result>();

                            detailView = RLCSManagement.FunctionPIEGetManagementDetailView(customerid, Branchlist, statusIds, Filter, CategoryIds, riskid, attrubute, FromDate, Enddate, AuthenticationHelper.UserID, IsApprover);
                            
                            lblDetailViewTitle.Text = lable;
                            
                            //if (CustBranchID != -1)
                            //    detailView = detailView.Where(Entry => Entry.CustomerBranchID == CustBranchID).ToList();

                            ManagementFilterNewStatus status = (ManagementFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                            switch (status)
                            {
                                case ManagementFilterNewStatus.Overdue:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 1 || entry.ComplianceStatusID == 10)).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedTimely:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 4).ToList();
                                    break;

                                case ManagementFilterNewStatus.ClosedDelayed:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 5).ToList();
                                    break;

                                case ManagementFilterNewStatus.PendingForReview:
                                    detailView = detailView.Where(entry => (entry.ComplianceStatusID == 2 || entry.ComplianceStatusID == 3 || entry.ComplianceStatusID == 11)).ToList();
                                    break;

                                case ManagementFilterNewStatus.Rejected:
                                    detailView = detailView.Where(entry => entry.ComplianceStatusID == 6).ToList();
                                    break;
                            }
                            Session["TotalRows"] = detailView.Count;
                            grdSummaryDetails.DataSource = detailView;
                            grdSummaryDetails.DataBind();
                        }

                        UpDetailView.Update();
                        Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<SP_GetCannedReportCompliancesSummary_Result>).ToDataTable();
                    }
                    #endregion
                }

                GetPageDisplaySummary();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Script1","HideLoaderDiv();",true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Script2", "HideLoaderDiv();",true);
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }                    
        }
    
        private void CmpSaveCheckedValues()
        {
            try
            {
                ArrayList userdetails = new ArrayList();
                int index = -1;
                foreach (GridViewRow gvrow in grdSummaryDetails.Rows)
                {
                    ImageButton lblDownLoadfile = (ImageButton)gvrow.FindControl("lblDownLoadfile");
                    index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                    bool result = ((CheckBox)gvrow.FindControl("chkCompliances")).Checked;

                    // Check in the Session
                    if (ViewState["checkedCompliances"] != null)
                        userdetails = (ArrayList)ViewState["checkedCompliances"];
                    if (result)
                    {
                        if (!userdetails.Contains(index))
                            userdetails.Add(index);
                    }
                    else
                        userdetails.Remove(index);
                }
                if (userdetails != null && userdetails.Count > 0)
                    ViewState["checkedCompliances"] = userdetails;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }          
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                using (ZipFile ComplianceZip = new ZipFile())
                {
                    CmpSaveCheckedValues();

                    if (ViewState["checkedCompliances"] != null)
                    {
                        foreach (var gvrow in (ArrayList)ViewState["checkedCompliances"])
                        {

                            int ScheduledOnID = Convert.ToInt32(gvrow);
                            if (ScheduledOnID != 0)
                            {
                                var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                                List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                                //craeted subdirectory                                
                                string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                int i = 0;
                                foreach (var file in fileData)
                                {
                                    string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                    var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                    if (dictionary == null)
                                        ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                    string filePath = string.Empty;
                                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    else
                                    {
                                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                         
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = file.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }

                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;
                        
                        byte[] data = zipMs.ToArray();                      
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=ComplianceDocument.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.

                    }
                }
                //BindComplianceDocument(DateTime.Now,DateTime.Now);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);               
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                ViewState["checkedCompliances"] = null;

                SelectedPageNo.Text = "1";

                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;

                int location = 0;
                int Risk = 0;
                int Status = 0;

                if (tvFilterLocation.SelectedValue != "-1")
                {
                    location = tvFilterLocation.SelectedNode.Text.Length;

                    if (location >= 50)
                        lblAdvanceSearchScrum.Text = "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 30) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                }

                if (ddlStatus.SelectedValue != "Status")
                {
                    Status = ddlStatus.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Status >= 50)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Status: </b>" + ddlStatus.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Status: </b>" + ddlStatus.SelectedItem.Text;
                    }
                    else
                    {
                        if (Status >= 50)
                            lblAdvanceSearchScrum.Text += "<b>Status: </b>" + ddlStatus.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Status: </b>" + ddlStatus.SelectedItem.Text;
                    }
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    BindDetailView();
                }
                else
                {
                    divAdvSearch.Visible = false;
                    BindDetailView();
                }

                CmpPopulateCheckedValues();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0]; 
                node.Selected = true;

                ddlStatus.ClearSelection();

                lblAdvanceSearchScrum.Text = String.Empty;
                divAdvSearch.Visible = false;

                tvFilterLocation_SelectedNodeChanged(sender, e);

                //Rebind Grid
                BindDetailView();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                        String LocationName = String.Empty;

                        DateTime CurrentDate = DateTime.Today.Date;

                        if (Session["LocationName"].ToString() != "")
                            LocationName = Session["LocationName"].ToString();
                        DataView view = new System.Data.DataView((DataTable) Session["grdDetailData"]);
                        ExcelData = view.ToTable("Selected", false, "Branch","ActName", "ShortForm","RequiredForms", "TextFrequency","ScheduledOn", "ForMonth", "Status", "RiskCategory", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");//"User", "ReviewerUser",

                    //foreach (DataRow item in ExcelData.Rows)
                    //{
                    //    item["User"] = GetPerformer(Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                    //    item["ReviewerUser"] = GetReviewer(Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                    //}
                    ExcelData.Columns.Remove("ComplianceStatusID");
                        ExcelData.Columns.Remove("ScheduledOnID");
                        ExcelData.Columns.Remove("ComplianceInstanceID");

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";

                        exWorkSheet.Cells["B2:C2"].Merge = true;

                        
                       exWorkSheet.Cells["B2"].Value = "Report of Compliances";

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);

                       
                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].Value = "Entity / Branch";
                        exWorkSheet.Cells["A5"].AutoFitColumns(20);

                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].Value = "Act Name";
                        exWorkSheet.Cells["B5"].AutoFitColumns(75);

                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C5"].Value = "Compliance";
                        exWorkSheet.Cells["C5"].AutoFitColumns(40);

                        //exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        //exWorkSheet.Cells["D5"].Value = "Performer";
                        //exWorkSheet.Cells["D5"].AutoFitColumns(30);

                        //exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        //exWorkSheet.Cells["E5"].Value = "Reviewer";
                        //exWorkSheet.Cells["E5"].AutoFitColumns(30);
                       
                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D5"].Value = "Form";
                        exWorkSheet.Cells["D5"].AutoFitColumns(10);
                        
                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E5"].Value = "Frequency";
                        exWorkSheet.Cells["E5"].AutoFitColumns(10);

                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F5"].Value = "Due Date";
                        exWorkSheet.Cells["F5"].AutoFitColumns(12);

                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G5"].Value = "Period";
                        exWorkSheet.Cells["G5"].AutoFitColumns(8);

                    exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["H5"].AutoFitColumns(8);

                    exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                    exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                    exWorkSheet.Cells["I5"].Value = "Risk";
                    exWorkSheet.Cells["I5"].AutoFitColumns(8);

                    using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 9])
                        {
                            col.Style.WrapText = true;
                            col.Style.Numberformat.Format = "dd/MMM/yyyy";
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            //col.AutoFitColumns();

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                            string filename = "_Report.xlsx";
                    if (Request.QueryString["attrubute"] == "Not completed") {
                        filename = "Compliance_Overdue" + filename;
                       }
                    else if(Request.QueryString["attrubute"] == "In Time")
                        {
                        filename = "Closed_Timely" + filename;
                     }
                    else if(Request.QueryString["attrubute"] == "After due date")
                    {
                        filename = "Closed_Delayed" + filename;
                    }

                     
                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename="+filename);
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void chkCompliancesHeader_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)grdSummaryDetails.HeaderRow.FindControl("chkCompliancesHeader");

                foreach (GridViewRow row in grdSummaryDetails.Rows)
                {
                    CheckBox chkCompliances = (CheckBox)row.FindControl("chkCompliances");

                    if (ChkBoxHeader.Checked)
                        chkCompliances.Checked = true;
                    else
                        chkCompliances.Checked = false;
                }

                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

           
        }

        protected void chkCompliances_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int Count = 0;

                CheckBox ChkBoxHeader = (CheckBox)grdSummaryDetails.HeaderRow.FindControl("chkCompliancesHeader");

                foreach (GridViewRow row in grdSummaryDetails.Rows)
                {
                    CheckBox chkCompliances = (CheckBox)row.FindControl("chkCompliances");

                    if (chkCompliances.Checked)
                        Count++;
                }

                if (Count == grdSummaryDetails.Rows.Count)
                    ChkBoxHeader.Checked = true;
                else
                    ChkBoxHeader.Checked = false;

                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

           
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                
                //Reload the Grid
                BindDetailView();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }                        
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            try
            {
                lblTotalSelected.Text = "";

                CmpSaveCheckedValues();

                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
           
        }

        protected void grdSummaryDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Download")
                {                    
                    using (ZipFile ComplianceZip = new ZipFile())
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int CompID = Convert.ToInt32(commandArgs[1]);
                        if (ScheduledOnID != 0)
                        {
                            var complianceTypeID = DocumentManagement.GetCompliancetypeID(CompID);
                            if (complianceTypeID == 1)
                            {
                                var ComplianceData = DocumentManagement.GetForMonthChecklist(ScheduledOnID);
                                if (ComplianceData !=null)
                                {
                                    List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                                    string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                    int i = 0;
                                    foreach (var file in fileData)
                                    {
                                        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                        var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                        if (dictionary == null)
                                            ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                        string filePath = string.Empty;
                                        //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                        {
                                            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        else
                                        {
                                            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                        }
                                        if (file.FilePath != null && File.Exists(filePath))
                                        {
                                            string[] filename = file.FileName.Split('.');
                                            string str = filename[0] + i + "." + filename[1];
                                            if (file.EnType == "M")
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            else
                                            {
                                                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                            }
                                            i++;
                                        }
                                    }
                                }                               
                            }
                            else
                            {
                                var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                                List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                                string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                                int i = 0;
                                foreach (var file in fileData)
                                {
                                    string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                    var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                                    if (dictionary == null)
                                        ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                                    string filePath = string.Empty;
                                    //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                                    if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                    {
                                        filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    else
                                    {
                                        filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                    }
                                    if (file.FilePath != null && File.Exists(filePath))
                                    {
                                        string[] filename = file.FileName.Split('.');
                                        string str = filename[0] + i + "." + filename[1];
                                        if (file.EnType == "M")
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        else
                                        {
                                            ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                        }
                                        i++;
                                    }
                                }
                            }
                        }                      
                        var zipMs = new MemoryStream();
                        ComplianceZip.Save(zipMs);
                        zipMs.Position = zipMs.Length;
                        byte[] data = zipMs.ToArray();
                        Response.Buffer = true;
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=Document.zip");
                        Response.BinaryWrite(data);
                        Response.Flush();
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }

                    #region Previous code
                    //using (ZipFile ComplianceZip = new ZipFile())
                    //{                        
                    //    string[] commandArg = e.CommandArgument.ToString().Split(',');
                    //    int ScheduledOnID = Convert.ToInt32(e.CommandArgument);
                    //    if (ScheduledOnID != 0)
                    //    {
                    //        var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                    //        List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                    //        //craeted subdirectory
                    //        //string directoryName = fileData[0].ID + "_" + fileData[0].Branch + "_" + fileData[0].ScheduledOn.Value.ToString("dd-MM-yyyy");
                    //        string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                    //        int i = 0;
                    //        foreach (var file in fileData)
                    //        {
                    //            string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                    //            var dictionary = ComplianceZip.EntryFileNames.Where(x => x.Contains(directoryName + "/" + version)).FirstOrDefault();
                    //            if (dictionary == null)
                    //                ComplianceZip.AddDirectoryByName(directoryName + "/" + version);
                    //            string filePath = string.Empty;
                    //            //Change by rahul on 21 JAN 2017 for Azure Drive or Local Drive
                    //            if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    //            {
                    //                filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                    //            }
                    //            else
                    //            {
                    //                filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    //            }
                    //            if (file.FilePath != null && File.Exists(filePath))
                    //            {
                    //                string[] filename = file.FileName.Split('.');
                    //                string str = filename[0] + i + "." + filename[1];
                    //                ComplianceZip.AddEntry(directoryName + "/" + version + "/" + str, CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    //                i++;
                    //            }
                    //        }
                    //    }
                    //    //PerformanceSummaryForPerformer filter = (PerformanceSummaryForPerformer)Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]);
                    //    var zipMs = new MemoryStream();
                    //    ComplianceZip.Save(zipMs);
                    //    zipMs.Position = zipMs.Length;
                    //    byte[] data = zipMs.ToArray();
                    //    Response.Buffer = true;
                    //    Response.ClearContent();
                    //    Response.ClearHeaders();
                    //    Response.Clear();
                    //    Response.ContentType = "application/zip";
                    //    Response.AddHeader("content-disposition", "attachment; filename=Document.zip");
                    //    Response.BinaryWrite(data);
                    //    Response.Flush();
                    //    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    //    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    //    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    //}
                    #endregion

                }
                else if (e.CommandName == "View")
                {

                    string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                    int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                    int CompID = Convert.ToInt32(commandArgs[1]);
                    if (ScheduledOnID != 0)
                    {
                        var complianceTypeID = DocumentManagement.GetCompliancetypeID(CompID);
                        if (complianceTypeID == 1)
                        {
                            var ComplianceData = DocumentManagement.GetForMonthChecklist(ScheduledOnID);
                            List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                            string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                            int i = 0;
                            rptComplianceVersionView.DataSource = fileData.OrderBy(entry => entry.Version);
                            rptComplianceVersionView.DataBind();
                            foreach (var file in fileData)
                            {
                                string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                string filePath = string.Empty;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                }
                                else
                                {
                                    filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                }
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));
                                    string DateFolder = Folder + "/" + File;
                                    string extension = System.IO.Path.GetExtension(filePath);
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }
                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);
                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));
                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;
                                    string FileName = DateFolder + "/" + User + "" + extension;
                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();
                                    CompDocReviewPath = FileName;
                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                }
                                break;
                            }
                        }
                        else
                        {
                            var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                            List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                            string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                            int i = 0;

                            rptComplianceVersionView.DataSource = fileData.OrderBy(entry => entry.Version);
                            rptComplianceVersionView.DataBind();
                            foreach (var file in fileData)
                            {
                                string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                                string filePath = string.Empty;
                                if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                                {
                                    filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                                }
                                else
                                {
                                    filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                                }
                                if (file.FilePath != null && File.Exists(filePath))
                                {
                                    string Folder = "~/TempFiles";
                                    string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                    string DateFolder = Folder + "/" + File;

                                    string extension = System.IO.Path.GetExtension(filePath);

                                    Directory.CreateDirectory(Server.MapPath(DateFolder));

                                    if (!Directory.Exists(DateFolder))
                                    {
                                        Directory.CreateDirectory(Server.MapPath(DateFolder));
                                    }

                                    string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                    string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                    string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                    string FileName = DateFolder + "/" + User + "" + extension;

                                    FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                    BinaryWriter bw = new BinaryWriter(fs);
                                    if (file.EnType == "M")
                                    {
                                        bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    else
                                    {
                                        bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                    }
                                    bw.Close();

                                    CompDocReviewPath = FileName;
                                    CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                                    lblMessage.Text = "";
                                }
                                else
                                {
                                    lblMessage.Text = "There is no file to preview";
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                                }
                                break;
                            }
                        }
                    }

                    #region previous code
                    //string[] commandArg = e.CommandArgument.ToString().Split(',');
                    //int ScheduledOnID = Convert.ToInt32(e.CommandArgument);
                    //if (ScheduledOnID != 0)
                    //{
                    //    var ComplianceData = DocumentManagement.GetForMonth(ScheduledOnID);
                    //    List<GetComplianceDocumentsView> fileData = DocumentManagement.GetFileData1(ScheduledOnID);
                    //    string directoryName = ComplianceData.ShortDescription + "/" + ComplianceData.ForMonth;
                    //    int i = 0;

                    //    rptComplianceVersionView.DataSource = fileData.OrderBy(entry => entry.Version);
                    //    rptComplianceVersionView.DataBind();
                    //    foreach (var file in fileData)
                    //    {
                    //        string version = string.IsNullOrEmpty(file.Version) ? "1.0" : file.Version;
                    //        string filePath = string.Empty;
                    //        if (ConfigurationManager.AppSettings["ISAzureDrive"] == "1")
                    //        {
                    //            filePath = Path.Combine(file.FilePath, file.FileKey + Path.GetExtension(file.FileName));
                    //        }
                    //        else
                    //        {
                    //            filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));
                    //        }
                    //        if (file.FilePath != null && File.Exists(filePath))
                    //        {
                    //            string Folder = "~/TempFiles";
                    //            string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                    //            string DateFolder = Folder + "/" + File;

                    //            string extension = System.IO.Path.GetExtension(filePath);

                    //            Directory.CreateDirectory(Server.MapPath(DateFolder));

                    //            if (!Directory.Exists(DateFolder))
                    //            {
                    //                Directory.CreateDirectory(Server.MapPath(DateFolder));
                    //            }

                    //            string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                    //            string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                    //            string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                    //            string FileName = DateFolder + "/" + User + "" + extension;

                    //            FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                    //            BinaryWriter bw = new BinaryWriter(fs);
                    //            bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                    //            bw.Close();

                    //            CompDocReviewPath = FileName;
                    //            CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                    //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                    //            lblMessage.Text = "";
                    //        }
                    //        else
                    //        {
                    //            lblMessage.Text = "There is no file to preview";
                    //            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowViewDocument();", true);

                    //        }
                    //        break;
                    //    }
                    //}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                //throw;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        protected void rptComplianceVersionView_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                // LinkButton btnComplinceVersionDocView = (LinkButton)e.Item.FindControl("btnComplinceVersionDocView");
                var scriptManager = ScriptManager.GetCurrent(this.Page);
                //scriptManager.RegisterPostBackControl(btnComplinceVersionDocView);

                LinkButton lblDocumentVersionView = (LinkButton)e.Item.FindControl("lblDocumentVersionView");
                scriptManager.RegisterAsyncPostBackControl(lblDocumentVersionView);
            }
        }


        protected void rptComplianceVersion_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                List<GetComplianceDocumentsView> ComplianceFileData = new List<GetComplianceDocumentsView>();
                List<GetComplianceDocumentsView> ComplianceDocument = new List<GetComplianceDocumentsView>();
                ComplianceDocument = DocumentManagement.GetFileData1(Convert.ToInt32(commandArgs[0])).ToList();

                if (commandArgs[1].Equals("1.0"))
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                    if (ComplianceFileData.Count <= 0)
                    {
                        ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == null).ToList();
                    }
                }
                else
                {
                    ComplianceFileData = ComplianceDocument.Where(entry => entry.Version == commandArgs[1]).ToList();
                }

                if (e.CommandName.Equals("View"))
                {
                    if (ComplianceFileData.Count > 0)
                    {
                        int i = 0;
                        foreach (var file in ComplianceFileData)
                        {
                            string filePath = Path.Combine(Server.MapPath(file.FilePath), file.FileKey + Path.GetExtension(file.FileName));

                            if (file.FilePath != null && File.Exists(filePath))
                            {
                                string Folder = "~/TempFiles";
                                string File = Convert.ToString(DateTime.Now.ToString("ddMMyy"));

                                string DateFolder = Folder + "/" + File;

                                string extension = System.IO.Path.GetExtension(filePath);

                                Directory.CreateDirectory(Server.MapPath(DateFolder));

                                if (!Directory.Exists(DateFolder))
                                {
                                    Directory.CreateDirectory(Server.MapPath(DateFolder));
                                }

                                string customerID = Convert.ToString(UserManagement.GetByID(Convert.ToInt32(AuthenticationHelper.UserID)).CustomerID ?? 0);

                                string FileDate = Convert.ToString(DateTime.Now.ToString("HHmmss"));

                                string User = AuthenticationHelper.UserID + "" + customerID + "" + FileDate;

                                string FileName = DateFolder + "/" + User + "" + extension;

                                FileStream fs = new FileStream(Server.MapPath(FileName), FileMode.Create, FileAccess.ReadWrite);
                                BinaryWriter bw = new BinaryWriter(fs);
                                if (file.EnType == "M")
                                {
                                    bw.Write(CryptographyHandler.Decrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                else
                                {
                                    bw.Write(CryptographyHandler.AESDecrypt(DocumentManagement.ReadDocFiles(filePath)));
                                }
                                bw.Close();
                                i++;

                                CompDocReviewPath = FileName;

                                CompDocReviewPath = CompDocReviewPath.Substring(2, CompDocReviewPath.Length - 2);
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "fopendocfileReview('" + CompDocReviewPath + "');", true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdSummaryDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton lblDownLoadfile = (ImageButton)e.Row.FindControl("lblDownLoadfile");
                    var scriptManager = ScriptManager.GetCurrent(this.Page);
                    if (scriptManager != null)
                    {
                        scriptManager.RegisterPostBackControl(lblDownLoadfile);
                    }


                    // String GridStatus = e.Row.Cells[7].Text;

                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    if (lblStatus != null)
                    {
                        if (lblScheduledOn != null)
                        {
                            DateTime CurrentDate = DateTime.Today.Date;
                            if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                            else if (lblStatus.Text.Trim() == "Complied but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "Complied Delayed but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //cvDuplicateEntry.IsValid = false;
                //cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void CmpPopulateCheckedValues()
        {
            try
            {

                ArrayList complianceDetails = (ArrayList)ViewState["checkedCompliances"];

                if (complianceDetails != null && complianceDetails.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdSummaryDetails.Rows)
                    {
                        ImageButton lblDownLoadfile = (ImageButton)gvrow.FindControl("lblDownLoadfile");
                        int index = Convert.ToInt32(lblDownLoadfile.CommandArgument.ToString().Split(',')[0]);
                        if (complianceDetails.Contains(index))
                        {
                            CheckBox myCheckBox = (CheckBox)gvrow.FindControl("chkCompliances");
                            myCheckBox.Checked = true;
                        }
                    }

                    if (complianceDetails.Count > 0)
                    {
                        lblTotalSelected.Text = complianceDetails.Count + " Selected";
                        btnDownload.Visible = true;
                    }
                    else if (complianceDetails.Count == 0)
                    {
                        lblTotalSelected.Text = "";
                        btnDownload.Visible = false;
                    }
                }
                else
                {
                    lblTotalSelected.Text = "";
                    btnDownload.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void grdSummaryDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                CmpSaveCheckedValues();
                grdSummaryDetails.PageIndex = e.NewPageIndex;

                if (string.IsNullOrEmpty(Convert.ToString(Session["TwelveMonth"])))
                {
                    BindDetailView();
                    //BindDetailView(Convert.ToString(Session["Arguments"]), Convert.ToString(Session["Filter"]));
                }
                else
                {
                    BindDetailView();
                    //BindDetailView(Convert.ToString(Session["Arguments"]), Convert.ToString(Session["Filter"]), Convert.ToString(Session["TwelveMonth"]));
                }
                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);             
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                CmpSaveCheckedValues();

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
               
                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindDetailView();

                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {     
                CmpSaveCheckedValues();

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindDetailView();

                CmpPopulateCheckedValues();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}