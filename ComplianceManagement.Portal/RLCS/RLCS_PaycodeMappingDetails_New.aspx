﻿<%@ Page Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_PaycodeMappingDetails_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_PaycodeMappingDetails_New" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <style type="text/css">
        .k-grid-content {
            min-height: auto;
        }

        html {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }
        /*.k-widget k-window {
            margin-left: 3%;
            margin-top: 2%;
        }*/
        .k-checkbox-label, .k-radio-label {
            display: inline;
        }

        .k-header .k-grid-toolbar {
            background: white;
            float: left;
            width: 100%;
        }

        .k-grid td {
            line-height: 1.0em;
        }

        .k-i-more-vertical:before {
            content: "\e006";
        }

        .k-grid td.k-state-selected:hover, .k-grid tr.k-state-selected:hover {
            background-color: #1fd9e1;
            background-image: none;
        }

        k-draghandle.k-state-selected:hover, .k-ghost-splitbar-horizontal, .k-ghost-splitbar-vertical, .k-list > .k-state-highlight, .k-list > .k-state-selected, .k-marquee-color, .k-panel > .k-state-selected, .k-scheduler .k-scheduler-toolbar .k-state-selected, .k-scheduler .k-today.k-state-selected, .k-state-selected, .k-state-selected:link, .k-state-selected:visited, .k-tool.k-state-selected {
            color: #000000;
            /*background-color: #1fd9e1;*/
            border-color: #1fd9e1;
            background-color: #f6f6f6;
        }

        #grid .k-grid-toolbar {
            background: white;
        }


        .k-pager-wrap > .k-link > .k-icon {
            margin-top: 5px;
            color: inherit;
        }

        .toolbar {
            float: left;
        }

        html .k-grid tr:hover {
            background: #E4F7FB;
        }

        html .k-grid tr.k-alt:hover {
            background: #E4F7FB;
        }

        .k-grid tbody .k-button {
            min-width: 30px;
            min-height: 30px;
            border-radius: 35px;
        }

        .k-button-icontext .k-icon, .k-button-icontext .k-image, .k-button-icontext .k-sprite {
            margin-right: 0px;
            margin-right: 0px;
            margin-left: 0px;
            margin-left: 0px;
        }

        .k-auto-scrollable {
            overflow: hidden;
        }

        .k-grid-header {
            padding-right: 0px !important;
        }

        .k-multicheck-wrap .k-item {
            line-height: 1.2em;
            font-size: 14px;
            margin-bottom: 5px;
        }

        label {
            display: inline-block;
            margin-bottom: 0px;
        }

        .k-filter-menu .k-button {
            width: 27%;
        }

        .k-label input[type="checkbox"] {
            margin: 0px 5px 0 !important;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            /* border-width: 0 0 0px 0px; */
            font-size: 15px;
            background: #f8f8f8;
            font-family: 'Roboto',sans-serif;
            color: #2b2b2b;
            font-weight: 400;
        }

        .k-primary {
            border-color: #1fd9e1;
            background-color: #1fd9e1;
        }

        .k-pager-wrap {
            background-color: #f8f8f8;
            color: #2b2b2b;
        }

        td.k-command-cell {
            border-width: 0 1px 0 1px;
        }

        .k-grid-pager {
            border-width: 1px 1px 1px 1px;
        }

        span.k-icon.k-i-calendar {
            margin-top: 6px;
        }

        .k-filter-row > th:first-child, .k-grid tbody td:first-child, .k-grid tfoot td:first-child, .k-grid-header th.k-header:first-child {
            border-left-width: 1px;
        }

        .k-icon-plus {
            background-position: -48px -64px;
        }

        .k-i-foo {
            background-color: #f00;
            background-image: url(".place.your.own.icon.here.");
        }

        .k-grid tbody button.k-button {
            min-width: 32px;
            min-height: 32px;
        }

        .k-icon, .k-tool-icon {
            position: relative;
            display: inline-block;
            overflow: hidden;
            width: 1em;
            height: 1em;
            text-align: center;
            vertical-align: middle;
            background-image: none;
            font: 16px/1 WebComponentsIcons;
            speak: none;
            font-variant: normal;
            text-transform: none;
            text-indent: 0;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            color: inherit;
        }
    </style>

    <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
            }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }

                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }
    .alert-danger {	
        color: #5c9566;	
        background-color: #dff8e3;	
        border-color: #d6e9c6;	
        }	
    .alert {	
        /*padding: 15px;	
        margin-bottom: 20px;*/	
        border: 1px solid transparent;	
        border-radius: 4px;	
    }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#divRLCSPaycodeUploadDialog').hide();
            $('#divRLCSPaycodeAddDialog').hide();

        });
        function onClose() {
            debugger;
            document.getElementById('<%= lnkbtnRefresh.ClientID %>').click();
        }
        function OpenAddEmployeePopup() {
            debugger;
            var Client_ = $("#ContentPlaceHolder1_hdnClientID").val();
            var Client = '<%= ClientID%>';

            if (Client_ != undefined || Client_ != null) {
                $('#divRLCSPaycodeAddDialog').show();
                var myWindowAdv = $("#divRLCSPaycodeAddDialog");
                myWindowAdv.kendoWindow({
                    width: "65%",
                    height: '89%',
                    title: "Paycode Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/Setup/CreateClientPaycodeMappingDetails?ClientID=" + Client_);
            }
            return false;
        }

           function EditEmployeePopup(obj) {
            debugger;
            var clientid = $(obj).attr('data-clId');
            var id = $(obj).attr('data-parentId');

            //alert(clientid);
            //alert(id);

             //var Client_ = $("#BodyContent_hdnClientID").val();
             var Client = '<%= ClientID%>';

               if (Client != undefined || Client != null) {
                $('#divRLCSPaycodeAddDialog').show();
                var myWindowAdv = $("#divRLCSPaycodeAddDialog");

                myWindowAdv.kendoWindow({
                    width: "65%",
                    height: '89%',
                    title: "Paycode Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/Setup/CreateClientPaycodeMappingDetails?ClientID=" + clientid +"&ID="+id+"&Edit=YES");
            }
            return false;
        }

        function OpenUploadWindow() {
            debugger;
            var Client_ = $("#ContentPlaceHolder1_hdnClientID").val();
            Client = '<%=ClientID%>';
            $('#divRLCSPaycodeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeUpload').attr('src', "/Setup/UploadPaycodeFile?Client=" + Client_);
            var myWindowAdv = $("#divRLCSPaycodeUploadDialog");
            myWindowAdv.kendoWindow({
                width: "50%",
                height: "50%",
                title: "Upload Paycode Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:LinkButton Text="Refresh" runat="server" ID="lnkbtnRefresh" OnClick="btnRefresh_Click" CssClass="k-hidden" />
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton>
                        <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <asp:UpdatePanel ID="upEmpMasterCADMN" runat="server" UpdateMode="Conditional" OnLoad="upEmpMasterCADMN_Load">
        <ContentTemplate>
            <asp:HiddenField ID="hdnClientID" runat="server" Value="" />
            <div style="width: 100%; margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                  <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />
                                       
                                        <asp:CustomValidator ID="cvDuplicate" runat="server" EnableClientScript="False"
                                            ValidationGroup="DocGenValidationGroup" Display="None" />
            </div>
            <div style="width: 100%">
                <table width="100%">
                    <tr>
                        <td style="width: 20%; display:none;">
                            <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                                RepeatLayout="Flow" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                        runat="server" Style="text-decoration: none; color: Black" />
                                </ItemTemplate>
                                <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                                <SeparatorStyle Font-Size="12" />
                                <SeparatorTemplate>
                                    &gt;
                                </SeparatorTemplate>
                            </asp:DataList>
                        </td>
                        <td style="width: 30%;">
                            <div id="divCustomerfilter" runat="server">
                                <div style="width: 80%; float: left;">
                                    <asp:DropDownListChosen runat="server" ID="ddlClientList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlClientList_SelectedIndexChanged" NoResultsText="No results match." Width="350px"
                                         AllowSingleDeselect="true" />
                                </div>
                            </div>
                        </td>
                        <td style="width: 30%;">
                            <div style="width: 80%; float: left;">
                                <asp:TextBox runat="server" ID="tbxFilter" Width="100%" MaxLength="50" placeholder="Filter" AutoPostBack="true" CssClass="form-control"
                                    OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                        </td>
                        <td style="width: 10%;">
                            <asp:LinkButton Text="Add New" runat="server" ID="btnAddEmployee" CssClass="k-button" ToolTip="Paycode Add" OnClientClick="return OpenAddEmployeePopup()" />
                        </td>
                        <td style="width: 10%;">
                            <asp:LinkButton Text="Upload" runat="server" ID="btnUploadEmployee" OnClientClick="return OpenUploadWindow()" CssClass="k-button" />
                        </td>
                    </tr>

                </table>

                <asp:Panel ID="Panel2" Width="100%" Height="400px" ScrollBars="Vertical" runat="server">
                    <asp:HiddenField ID="TotalRows" runat="server" Value="0" />

                    <div class="tab-content">
                        <asp:GridView runat="server" ID="grdPaycodeMappingDetails" CssClass="table" AutoGenerateColumns="false" AllowSorting="true" GridLines="None"
                            CellPadding="4" AllowPaging="True" PageSize="10" Width="100%" Font-Size="12px" OnSorting="grdPaycodeMappingDetails_Sorting" ShowHeaderWhenEmpty="true" OnRowCreated="grdPaycodeMappingDetails_RowCreated"
                            OnPageIndexChanging="grdPaycodeMappingDetails_PageIndexChanging" OnRowEditing="grdPaycodeMappingDetails_RowEditing" OnRowCommand="grdPaycodeMappingDetails_RowCommand" DataKeyNames="CPMD_ClientID">
                            <Columns>

                                <asp:TemplateField HeaderText="Sr" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                        <asp:HiddenField ID="CPMD_ClientID" runat="server" Value='<%# Eval("CPMD_ClientID") %>' />
                                        <asp:HiddenField ID="Header" runat="server" Value='<%# Eval("CPMD_Header") %>' />
                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# Eval("CPMD_Status") %>' />
                                    <asp:HiddenField ID="hdnPayCode" runat="server" Value='<%# Eval("CPMD_PayCode") %>' />
                                    <asp:HiddenField ID="hdnStandard_Column" runat="server" Value='<%# Eval("CPMD_Standard_Column") %>' />
                                    <asp:HiddenField ID="hdnPayGroup" runat="server" Value='<%# Eval("CPMD_PayGroup") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="CPMD_Header" HeaderText="Header Name" SortExpression="CPMD_Header" />
                                <asp:BoundField DataField="CPMD_PayGroup" HeaderText="Paycode Group" SortExpression="CPMD_PayGroup" />
                                <asp:BoundField DataField="CPMD_PayCode" HeaderText="Paycode" SortExpression="CPMD_PayCode" />
                                <asp:BoundField DataField="CPMD_Deduction_Type" HeaderText="PaycodeType" SortExpression="CPMD_Deduction_Type" />
                                <asp:BoundField DataField="CPMD_Sequence_Order" HeaderText="Sequence Order" SortExpression="CPMD_Sequence_Order" />
                                <asp:BoundField DataField="CPMD_appl_ESI" HeaderText="ESI Applicable" SortExpression="CPMD_appl_ESI" />
                                <asp:BoundField DataField="CPMD_Appl_PT" HeaderText="PT Applicable" SortExpression="CPMD_Appl_PT" />
                                <asp:BoundField DataField="CPMD_Appl_LWF" HeaderText="LWF Applicable" SortExpression="CPMD_Appl_LWF" />
                                <asp:BoundField DataField="CPMD_appl_PF" HeaderText="PF Applicable" SortExpression="CPMD_appl_PF" />
                                <asp:BoundField DataField="PS_Status" HeaderText="Status" SortExpression="PS_Status" />
                                <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                         <asp:LinkButton ID="lnkBtnEdit" runat="server" data-clId='<%# Eval("CPMD_ClientID")%>' data-parentId='<%# Eval("ID")%>' OnClientClick="return EditEmployeePopup(this);">
                                                <img src="../../Images/edit_icon.png" alt="Edit" title="Edit Details" />
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="btnActive" runat="server" CommandName="Delete_Employee" CommandArgument='<%# Eval("ID") %>' Visible='<%# Eval("CPMD_Status").ToString() == "I" ? true : false %>'
                                            OnClientClick="return confirm('Are you certain you want to Activate this Paycode?');"><img src="../../Images/delete_icon.png" alt="Delete" title="Delete Paycode" /></asp:LinkButton>

                                        <asp:LinkButton ID="lnkDeleteEmployee" runat="server" CommandName="Delete_Employee" CommandArgument='<%# Eval("ID") %>' Visible='<%# Eval("CPMD_Status").ToString() == "A" ? true : false %>'
                                            OnClientClick="return confirm('Are you certain you want to delete this Paycode?');"><img src="../../Images/delete_icon.png" alt="Delete" title="Delete Paycode" /></asp:LinkButton>

                                    </ItemTemplate>
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Right" />
                            <PagerTemplate>
                                <table style="display: none">
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </PagerTemplate>

                            <EmptyDataTemplate>
                                No Paycode to display.                                       
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                    <div class="col-md-12 colpadding0" >
                        <div class="col-md-6 colpadding0">
                            <div runat="server" id="DivRecordsScrum">
                                <p style="padding-right: 0px !Important; color: #666; text-align: -webkit-left;">
                                    <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                    <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                                            <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                                            <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 colpadding0">
                            <div class="table-paging" runat="server" id="DivnextScrum" style="margin-bottom: 20px">
                                <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                <div class="table-paging-text">
                                    <p>
                                        <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                                <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1" style="padding-left: 0px;">
                        <label for="ddlPageSize" class="hidden-label">Show</label>
                        <asp:DropDownList runat="server" Visible="false" ID="ddlPageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"
                            class="form-control">
                            <asp:ListItem Text="5" />
                            <asp:ListItem Text="10" Selected="True" />
                            <asp:ListItem Text="20" />
                            <asp:ListItem Text="50" />
                        </asp:DropDownList>
                    </div>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="divRLCSPaycodeUploadDialog">
        <div class="chart-loading colpadding0" id="loader"></div>
        <iframe id="iframeUpload" style="width: 100%; height: 300px; border: none"></iframe>
    </div>
    <div id="divRLCSPaycodeAddDialog">
        <div class="chart-loading colpadding0" id="loader1"></div>
        <iframe id="iframeAdd" style="width: 96%; height: 500px; border: none"></iframe>
    </div>

       
</asp:Content>
