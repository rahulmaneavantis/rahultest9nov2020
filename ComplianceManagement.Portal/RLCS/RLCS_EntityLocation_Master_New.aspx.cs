﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_EntityLocation_Master_New : System.Web.UI.Page
    {
        protected static int CustId, UserId;
        protected static string avacomRLCSAPI_URL;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {

                CustId = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["CustID"]))
                {
                    CustId = Convert.ToInt32(Request.QueryString["CustID"]);
                    UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                    ViewState["CustomerID"] = CustId;
                }

                //CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                avacomRLCSAPI_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        protected void btnPaycode_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/RLCS/RLCS_PaycodeMappingDetails_New.aspx?CustomerID=" + Convert.ToInt32(ViewState["CustomerID"]));
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string masterpage = Convert.ToString(Session["masterpage"]);
            if (masterpage == "HRPlusSPOCMGR")
            {
                this.Page.MasterPageFile = "~/HRPlusSPOCMGR.Master";
            }
            else if (masterpage == "HRPlusCompliance")
            {
                this.Page.MasterPageFile = "~/HRPlusCompliance.Master";
            }
        }
    }
}