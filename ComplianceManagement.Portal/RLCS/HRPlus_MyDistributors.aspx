﻿<%@ Page Title="My Distributors" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_MyDistributors.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_MyDistributors" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .k-grid tbody .k-button {
            min-width: 30px !important;
            min-height: 30px !important;
            border-radius: 35px !important;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            fhead('My Distributors');

            BindGrid();
            SetGridTooltip();

            $('#divKendoWindow').hide();
        });

        function BindGrid() {
            $("#gridDistributor").kendoTreeList({
                dataSource: {
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetMyDistributors?parentDistID=<%=loggedInUserCustID%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            if (response.Result != null)
                                return response.Result;
                        },
                        model: {
                            id: "ID",
                            fields: {
                                Id: { type: "number", nullable: false },
                                parentId: { field: "ParentID", nullable: true }
                            },
                            expanded: true
                        }   
                    }
                },
                pageable: {
                    pageSize: 5,
                    pageSizes: true
                },
                editable: "true",
                sortable: true,
                filterable: true,
                columnMenu: true,
                columns: [
                    { field: "Name", expandable: true, title: "Name", width: "75%" },
                    { field: "StatusName", title: "Status", width: "15%" },
                    {
                        title: "Action",
                        command:
                            [
                                { name: "defineSharing", text: " ", imageClass: "k-i-edit", iconClass: ".k-icon k-i-eye", className: "k-grid-edit", click: showDetails },
                                //{ name: "defineSharing", text: " ", imageClass: ".k-i-edit", className: ".k-grid-edit", click: showDetails },
                                { name: "proceed", text: " ", imageClass: "k-i-arrow-right", iconClass: ".k-icon .k-i-arrow-right", className: "k-grid-arrow-right", click: showPaymentHistory },
                            ],
                        lock: true,
                        width: "10%",
                    }
                ],
                pageable: {
                    pageSize: 5,
                    pageSizes: true
                },
                dataBound: function () {
                    var grid = $("#gridDistributor").data("kendoTreeList");
                    var gridData = grid.dataSource.view();

                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        if (gridData[i].ActualParentID != null) {
                            if (gridData[i].ActualParentID != 95) {
                                var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                                var editButton = $(currenRow).find(".k-grid-edit");
                                editButton.hide();
                            }
                        }
                    }
                }
            });
        }

        function SetGridTooltip() {
            $("#gridDistributor").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "View/Edit Sharing";
                }
            });

            $("#gridDistributor").kendoTooltip({
                filter: ".k-grid-arrow-right",
                content: function (e) {
                    return "View Payment History";
                }
            });
        }

        function OpenKendowWindow(ID) {
            $('#divKendoWindow').show();

            var myWindowAdv = $("#divKendoWindow");

            myWindowAdv.kendoWindow({
                width: "50%",
                height: '80%',
                title: "Sharing",
                visible: false,
                actions: ["Close"],                
            });

            $('#iframeWindow').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeWindow').attr('src', "/RLCS/HRPlus_SharingPercentage.aspx?CustID=" + ID);
            return false;
        }
        
        function showDetails(e) {
            e.preventDefault();
            debugger;
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            var custID = dataItem.ID;
            OpenKendowWindow(custID);
            return false;
        }

        function showPaymentHistory(e) {
            e.preventDefault();
            debugger;
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            var custID = dataItem.ID;

            window.location.href = "HRPlus_MyPayments.aspx?CustID=" + custID;
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0" style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="gridDistributor" style="border: none;">
            </div>
        </div>
    </div>

    <div id="divKendoWindow">
        <iframe id="iframeWindow" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
</asp:Content>
