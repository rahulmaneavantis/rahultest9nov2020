﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Logger;
using System.Reflection;
using System.Collections;
using OfficeOpenXml;
using System.Data;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_HRCompliance_Assign_New : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();
        public static List<string> lstStatesToFilter = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            vsHRCompAssign.CssClass = "alert alert-danger";
            if (!IsPostBack)
            {
                Panel1.Visible = false;

                BindCustomers();
                BindLocationFilter();
                BindStates();

                BindUsers(ddlFilterPerformer);
                BindUsers(ddlFilterReviewer);

                if (SelectedPageNo.Text == "")
                {
                    SelectedPageNo.Text = "1";
                }

                if (Session["TotalComplianceAssign"] != null)
                    TotalRows.Value = Session["TotalComplianceAssign"].ToString();

                //AddFilter();
                tbxFilterLocation.Text = "Entity/Branch";
                txtactList.Text = "< Select >";

                btnReAssign.Enabled = false;
                btnUploadAssignment.Enabled = false;
            }
            ForceCloseFilterBranchesTreeView();
        }

        protected void AddFilter(int pageIndex = 0)
        {
            try
            {
                ViewState["pagefilter"] = Convert.ToString(Request.QueryString["Param"]);
                if (ViewState["pagefilter"] != null)
                {
                    if (Convert.ToString(ViewState["pagefilter"]).Equals("location"))
                    {
                        //divFilterUsers.Visible = false;
                        //FilterLocationdiv.Visible = true;
                    }
                    else if (Convert.ToString(ViewState["pagefilter"]).Equals("user"))
                    {
                        //divFilterUsers.Visible = true;
                        //FilterLocationdiv.Visible = false;
                    }
                    else
                    {
                        //divFilterUsers.Visible = false;
                        //FilterLocationdiv.Visible = false;
                        //if (AuthenticationHelper.Role == "EXCT")
                        //{
                        //    btnAddComplianceType.Visible = false;
                        //}

                        //BindComplianceInstances();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public IEnumerable<TreeNode> GetChildren(TreeNode Parent)
        {
            return Parent.ChildNodes.Cast<TreeNode>().Concat(
                   Parent.ChildNodes.Cast<TreeNode>().SelectMany(GetChildren));
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                //ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2);
                ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(AuthenticationHelper.UserID, customerID, serviceProviderID, distributorID, AuthenticationHelper.Role, false)
                                            .Where(x => x.HRProductMappingID > 0).ToList();

                ddlCustomer.DataBind();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomer.Items.FindByValue(customerID.ToString()) != null)
                    {
                        ddlCustomer.ClearSelection();
                        ddlCustomer.Items.FindByValue(customerID.ToString()).Selected = true;

                        if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                        {
                            btnReAssign.Enabled = true;
                            btnUploadAssignment.Enabled = true;
                        }
                        else
                        {
                            btnReAssign.Enabled = false;
                            btnUploadAssignment.Enabled = false;
                        }
                    }
                }

                ddlCustomer.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                {
                    btnReAssign.Enabled = true;
                    btnUploadAssignment.Enabled = true;
                }
                else
                {
                    btnReAssign.Enabled = false;
                    btnUploadAssignment.Enabled = false;
                }

                BindLocationFilter();

                branchList.Clear();

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    branchList.Add(Convert.ToInt32(tvFilterLocation.SelectedValue));
                    branchList.ToList();
                }

                BindUsers(ddlFilterPerformer);
                BindUsers(ddlFilterReviewer);

                BindGrid();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                {

                }

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }

                if (customerID != -1)
                {
                    tvFilterLocation.Nodes.Clear();

                    //var bracnhes = RLCS_Master_Management.GetAll_Entities(customerID);
                    var bracnhes = RLCS_ClientsManagement.GetAllHierarchy(customerID);

                    //TreeNode node = new TreeNode("< All >", "-1");
                    //node.Selected = true;
                    //tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        BindBranchesHierarchy(node, item);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    //tvFilterLocation_SelectedNodeChanged(null, null);     
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindStates()
        {
            try
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;
                int customerBranchID = 0;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}

                if (customerID != -1)
                {
                    ddlState.DataTextField = "SM_Name";
                    ddlState.DataValueField = "SM_Code";

                    if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue) && branchList.Count == 0)
                    {
                        customerBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        branchList.Clear();
                        //GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        //GetAll_SubBranches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        branchList.Add(Convert.ToInt32(tvFilterLocation.SelectedValue));
                        branchList.ToList();
                    }

                    if (ddlScopeType.SelectedValue.Trim().Equals("SOW13"))
                    {
                        int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                        if (custBranchDetails != null)
                        {
                            //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);
                            string clientID = custBranchDetails.CM_ClientID;
                            //string ptState = custBranchDetails.CL_PT_State;

                            if (!string.IsNullOrEmpty(clientID))
                            {
                                lstStatesToFilter = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "EPF");
                            }
                        }
                    }
                    else if (ddlScopeType.SelectedValue.Trim().Equals("SOW14"))
                    {
                        int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                        if (custBranchDetails != null)
                        {
                            //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);
                            string clientID = custBranchDetails.CM_ClientID;
                            //string ptState = custBranchDetails.CL_PT_State;

                            if (!string.IsNullOrEmpty(clientID))
                            {
                                lstStatesToFilter = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "ESI");
                            }
                        }
                    }
                    else if (ddlScopeType.SelectedValue.Trim().Equals("SOW17"))
                    {
                        int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                        //var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);
                        var custBranchDetails = RLCS_Master_Management.GetEntityOrBranchDetails(custBranchID);

                        if (custBranchDetails != null)
                        {
                            //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);
                            string clientID = custBranchDetails.CM_ClientID;
                            //string ptState = custBranchDetails.CL_PT_State;

                            if (!string.IsNullOrEmpty(clientID))
                            {
                                //lstStatesToFilter = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "PT");
                                lstStatesToFilter = RLCS_Master_Management.GetPTStates(custBranchID, clientID, "PT");
                            }
                        }
                    }
                    else
                    {
                        lstStatesToFilter = RLCS_Master_Management.GetBranchStateDetails(branchList);
                    }

                    var lstStates = RLCS_ClientsManagement.GetAllStates();

                    if (lstStatesToFilter.Count > 0 && lstStates.Count > 0)
                    {
                        lstStates = lstStates.Where(row => lstStatesToFilter.Contains(row.SM_Code)).ToList();
                    }

                    ddlState.DataSource = lstStates;
                    ddlState.DataBind();

                    //ddlState.Items.Insert(0, new ListItem("Select", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}                

                if (customerID != -1)
                {
                    var branch = CustomerBranchManagement.GetByID(Convert.ToInt32(tvFilterLocation.SelectedValue));

                    ddlScopeType.Items.Clear();
                    if (branch != null && branch.ParentID != null)
                    {
                        ddlScopeType.Items.Add(new ListItem { Text = "Register", Value = "SOW03" });
                        ddlScopeType.Items.Add(new ListItem { Text = "ESI Challan", Value = "SOW14" });
                        ddlScopeType.Items.Add(new ListItem { Text = "PT Challan", Value = "SOW17" });
                        ddlScopeType.Items.Add(new ListItem { Text = "Return", Value = "SOW05" });
                    }
                    else if (branch != null && branch.ParentID == null)
                    {
                        ddlScopeType.Items.Add(new ListItem { Text = "PF Challan", Value = "SOW13" });
                    }
                    
                    branchList.Clear();
                    //GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    //GetAll_SubBranches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    branchList.Add(Convert.ToInt32(tvFilterLocation.SelectedValue));
                    branchList.ToList();
                }
            }

            if (branchList.Count > 0)
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

                //BindStates();
                //BindGrid();
                ddlScopeType_SelectedIndexChanged(sender, e);
                GetPageDisplaySummary();
            }
            else
            {
                tvFilterLocation.SelectedNode.Selected = false;

                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "No Branch available for Selected Entity, Please Create Location/Branch before Compliance Assignment.";
            }
        }

        private void BindUsers(DropDownList ddlUserList, List<long> ids = null)
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    distributorID = RLCS_Master_Management.Get_DistributorID(customerID);
                    //serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);                    
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                if (customerID != -1)
                {
                    ddlUserList.DataTextField = "Name";
                    ddlUserList.DataValueField = "ID";
                    ddlUserList.Items.Clear();

                    var users = UserCustomerMappingManagement.GetAllUser_ServiceProviderDistributorCustomer(customerID, serviceProviderID, distributorID);
                    //var users = RLCS_Master_Management.GetAll_RLCSUsers_IncludingServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2);
                    //var users = UserManagement.GetAllNVP(customerID, ids: ids, Flags: true);

                    ddlUserList.DataSource = users;
                    ddlUserList.DataBind();

                    //ddlUserList.Items.Insert(0, new ListItem("< Select >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void ForceCloseFilterBranchesTreeView()
        {
            //ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide();", true);
        }

        private void BindComplianceCategories()
        {
            try
            {
                ddlComplianceCatagory.DataTextField = "Name";
                ddlComplianceCatagory.DataValueField = "ID";
                ddlComplianceCatagory.DataSource = ComplianceCategoryManagement.GetAll();
                ddlComplianceCatagory.DataBind();
                ddlComplianceCatagory.Items.Insert(0, new ListItem("< Select >", "-1"));

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlComplianceCatagory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindActList();
            grdComplianceRoleMatrix.PageIndex = 0;
            BindGrid();
            GetPageDisplaySummary();
        }

        private void BindGrid()
        {
            int customerID = -1;

            if (AuthenticationHelper.Role == "CADMN")
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                }
            }
            //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
            //{
            //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            //    {
            //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
            //    }
            //}

            string scopeType = Convert.ToString(ddlScopeType.SelectedValue.Trim());
            string stateCode = Convert.ToString(ddlState.SelectedValue);

            string establishmentType = string.Empty;
            string branchState = string.Empty;

            if (scopeType.Trim().ToUpper().Equals("SOW03"))
            {
                if (branchList.Count == 1)
                {
                    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(branchList[0]);

                    if (custBranchDetails != null)
                    {
                        if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                        {
                            establishmentType = custBranchDetails.CM_EstablishmentType;
                            branchState = custBranchDetails.CM_State;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(establishmentType) && !string.IsNullOrEmpty(branchState))
                {
                    var list = RLCS_ComplianceManagement.GetHRComplianceList_Assignment(customerID, scopeType, branchList, lstStatesToFilter, stateCode, establishmentType, branchState, false);
                    grdComplianceRoleMatrix.DataSource = list;
                    Session["TotalComplianceAssign"] = 0;
                    Session["TotalComplianceAssign"] = list.Count();
                    grdComplianceRoleMatrix.DataBind();
                }
                else
                {
                    var list = RLCS_ComplianceManagement.GetHRComplianceList_Assignment(customerID, scopeType, branchList, lstStatesToFilter, stateCode, establishmentType, branchState, false);
                    grdComplianceRoleMatrix.DataSource = null;
                    Session["TotalComplianceAssign"] = 0;
                    Session["TotalComplianceAssign"] = list.Count();
                    grdComplianceRoleMatrix.DataBind();

                    if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please specify Establishment Type and State, prior to Register Compliance Assignment";
                    }
                }
            }
            else if (scopeType.Trim().ToUpper().Equals("SOW17"))
            {
                if (lstStatesToFilter.Count > 0)
                {
                    var list = RLCS_ComplianceManagement.GetHRComplianceList_Assignment(customerID, scopeType, branchList, lstStatesToFilter, stateCode, establishmentType, branchState, false);
                    grdComplianceRoleMatrix.DataSource = list;
                    Session["TotalComplianceAssign"] = 0;
                    Session["TotalComplianceAssign"] = list.Count();
                    grdComplianceRoleMatrix.DataBind();
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please specify PT State for Employee(s) prior to PT Compliance Assignment";
                }
            }
            else
            {
                var list = RLCS_ComplianceManagement.GetHRComplianceList_Assignment(customerID, scopeType, branchList, lstStatesToFilter, stateCode, establishmentType, branchState, false);
                grdComplianceRoleMatrix.DataSource = list;
                Session["TotalComplianceAssign"] = 0;
                Session["TotalComplianceAssign"] = list.Count();
                grdComplianceRoleMatrix.DataBind();
            }

            if (grdComplianceRoleMatrix.Rows.Count > 0)
            {
                btnSaveAssignment.Visible = true;
            }
            else
                btnSaveAssignment.Visible = false;

            Panel1.Visible = true;
        }

        private void BindActList()
        {
            try
            {
                //int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}

                int complianceTypeID = Convert.ToInt32(ddlScopeType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);

                List<ActView> ActList = ActManagement.GetAll(complianceCatagoryID, complianceTypeID);
                if (complianceTypeID != -1)
                {
                    if (ComplianceTypeManagement.GetByID(complianceTypeID).Name.ToLower().Equals("state"))
                    {
                        //int branchID = tvBranches.SelectedNode != null ? Convert.ToInt32(tvBranches.SelectedNode.Value) : -1;
                        int branchID = -1;
                        if (!(tbxFilterLocation.Text.Trim().Equals("< Select >")))
                        {
                            branchID = CustomerBranchManagement.GetByName(tbxFilterLocation.Text.Trim(), customerID).ID;
                        }
                        if (branchID != -1)
                        {
                            int StateId = CustomerBranchManagement.GetByID(branchID).StateID;
                            ActList = ActList.Where(entry => entry.StateID == StateId).ToList();
                        }
                    }
                }

                rptActList.DataSource = ActList;
                rptActList.DataBind();

                if (complianceCatagoryID != -1 || complianceTypeID != -1)
                {

                    foreach (RepeaterItem aItem in rptActList.Items)
                    {
                        CheckBox chkAct = (CheckBox)aItem.FindControl("chkAct");

                        if (!chkAct.Checked)
                        {
                            chkAct.Checked = true;
                        }
                    }
                    CheckBox actSelectAll = (CheckBox)rptActList.Controls[0].Controls[0].FindControl("actSelectAll");
                    actSelectAll.Checked = true;
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceRoleMatrix_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["MatrixSortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddMatrixSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void grdComplianceRoleMatrix_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int complianceTypeID = Convert.ToInt32(ddlScopeType.SelectedValue);
                int complianceCatagoryID = Convert.ToInt32(ddlComplianceCatagory.SelectedValue);

                tbxFilterLocation.Text = "< Select >";
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void AddMatrixSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected void grdComplianceRoleMatrix__RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void grdComplianceRoleMatrix_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                SaveCheckedValues();

                grdComplianceRoleMatrix.PageIndex = e.NewPageIndex;
                grdComplianceRoleMatrix.DataBind();

                BindGrid();

                PopulateCheckedValues();
                //GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void ddlScopeType_SelectedIndexChanged(object sender, EventArgs e)
        { 
            if (!string.IsNullOrEmpty(ddlScopeType.SelectedValue))
            {
                Panel1.Visible = true;
                ViewState["CHECKED_ITEMS"] = null;
                if (ddlScopeType.SelectedValue.Trim().Equals("SOW03") || ddlScopeType.SelectedValue.Trim().Equals("SOW17"))
                {
                    //ddlState.ClearSelection();
                    // lblState.Visible = true;
                    ddlState.Visible = true;
                    BindStates();
                }
                else
                {
                    ddlState.ClearSelection();
                    ddlState.Items.Insert(0, new ListItem("Select", "-1"));
                    //  lblState.Visible = false;
                    ddlState.Visible = false;
                }

                BindGrid();
                GetPageDisplaySummary();
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlState.SelectedValue))
            {
                if (ddlScopeType.SelectedValue.Trim().Equals("SOW03") || ddlScopeType.SelectedValue.Trim().Equals("SOW17"))
                {
                    //ddlState.ClearSelection();
                    // lblState.Visible = true;
                    ddlState.Visible = true;
                }
                else
                {
                    ddlState.ClearSelection();
                    //lblState.Visible = false;
                    ddlState.Visible = false;
                }

                BindGrid();
                GetPageDisplaySummary();
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //BindComplianceMatrix("N", "N");

        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {

        }

        protected void chkEvent_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void chkAssignSelectAll_CheckedChanged(object sender, EventArgs e)
        {

            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            foreach (GridViewRow row in grdComplianceRoleMatrix.Rows)
            {
                CheckBox chkAssign = (CheckBox)row.FindControl("chkAssign");
                if (chkAssignSelectAll.Checked == true)
                {
                    chkAssign.Checked = true;
                }
                else
                {
                    chkAssign.Checked = false;
                }
            }

        }

        protected void chkAssign_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkAssignSelectAll = (CheckBox)grdComplianceRoleMatrix.HeaderRow.FindControl("chkAssignSelectAll");
            int countCheckedCheckbox = 0;
            for (int i = 0; i < grdComplianceRoleMatrix.Rows.Count; i++)
            {
                GridViewRow row = grdComplianceRoleMatrix.Rows[i];
                if (((CheckBox)row.FindControl("chkAssign")).Checked)
                {

                    countCheckedCheckbox = countCheckedCheckbox + 1;

                }

            }
            if (countCheckedCheckbox == grdComplianceRoleMatrix.Rows.Count)
            {
                chkAssignSelectAll.Checked = true;
            }
            else
            {
                chkAssignSelectAll.Checked = false;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool assignSuccess = false;
                List<string> lstCodes = new List<string>();

                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }
                //else if (AuthenticationHelper.Role == "SPADM" || AuthenticationHelper.Role == "DADMN")
                //{
                //    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                //    {
                //        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                //    }
                //}

                if (customerID != -1)
                {
                    if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                    {
                        if ((!String.IsNullOrEmpty(ddlFilterPerformer.SelectedValue) && ddlFilterPerformer.SelectedValue != "-1")&& (!String.IsNullOrEmpty(ddlFilterReviewer.SelectedValue) && ddlFilterReviewer.SelectedValue != "-1"))
                        {
                            string scopeType = Convert.ToString(ddlScopeType.SelectedValue.Trim());

                            var complianceList = new List<ComplianceAsignmentProperties>();
                            SaveCheckedValues();
                            complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                            if (complianceList.Count > 0)
                            {
                                List<TempAssignmentTable> Tempassignments = new List<TempAssignmentTable>();

                                if (scopeType.Trim().ToUpper().Equals("SOW03"))
                                {
                                    RLCS_CustomerBranch_ClientsLocation_Mapping customerBranchDetails = null;

                                    branchList.Clear();
                                    //GetAll_Branches(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                                    branchList.Add(Convert.ToInt32(tvFilterLocation.SelectedValue));
                                    branchList.ToList();

                                    if (branchList.Count > 0)
                                    {
                                        branchList.ForEach(eachBranch =>
                                        {
                                            //Get CustomerBranchID, StartDate from RLCS_CustomerBranch_ClientsLocation_Mapping
                                            customerBranchDetails = RLCS_Master_Management.GetClientLocationDetails(eachBranch);

                                            if (customerBranchDetails != null && customerBranchDetails.CM_EstablishmentType != null)
                                            {
                                                complianceList.ForEach(eachSelectedCompliance =>
                                                {
                                                    if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                                    {
                                                        var mapCompliance = RLCS_ComplianceManagement.Check_Register_Compliance_Map(eachSelectedCompliance.ComplianceId,
                                                            customerBranchDetails.CM_State, customerBranchDetails.CM_EstablishmentType.Trim().ToUpper());

                                                        if (mapCompliance)
                                                        {
                                                            //Performer
                                                            TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                            {
                                                                ComplianceId = eachSelectedCompliance.ComplianceId,
                                                                CustomerBranchID = eachBranch,
                                                                RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                                UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                                IsActive = true,
                                                                CreatedOn = DateTime.Now
                                                            };

                                                            Tempassignments.Add(TempAssP);

                                                            //Reviewer
                                                            TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                            {
                                                                ComplianceId = eachSelectedCompliance.ComplianceId,
                                                                CustomerBranchID = eachBranch,
                                                                RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                                UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                                IsActive = true,
                                                                CreatedOn = DateTime.Now
                                                            };

                                                            Tempassignments.Add(TempAssR);
                                                        }
                                                    }
                                                });
                                            }
                                            else
                                                LoggerMessage.InsertErrorMsg_DBLog("customerBranchDetails=null",
                                                    MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                        });
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "No Branch Available for Selected Entity/Client";
                                    }
                                }
                                else if (scopeType.Trim().ToUpper().Equals("SOW13")) //PF Challan
                                {
                                    int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                    var custBranchDetails = RLCS_Master_Management.GetEntityDetails(custBranchID);
                                    //var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                                    if (custBranchDetails != null)
                                    {
                                        string clientID = custBranchDetails.CM_ClientID;

                                        if (!string.IsNullOrEmpty(clientID))
                                        {
                                            lstCodes = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "EPF");

                                            if (lstCodes.Count > 0)
                                            {
                                                lstCodes.ForEach(eachpfCode =>
                                                {
                                                    //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);

                                                    if (!string.IsNullOrEmpty(eachpfCode))
                                                    {
                                                        var lstBranches = RLCSManagement.GetCustomerBranchesByPFOrESICOrPTOrLWFCode(clientID, "EPF", eachpfCode, "E");

                                                        if (lstBranches.Count > 0)
                                                        {
                                                            lstBranches.ForEach(eachCustBranch =>
                                                            {
                                                                complianceList.ForEach(eachSelectedCompliance =>
                                                                {
                                                                    if (eachCustBranch.AVACOM_BranchID != null)
                                                                    {
                                                                        if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                                                        {
                                                                            //Performer
                                                                            TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                                            {
                                                                                ComplianceId = eachSelectedCompliance.ComplianceId,
                                                                                CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                                RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                                                UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                                                IsActive = true,
                                                                                CreatedOn = DateTime.Now
                                                                            };

                                                                            Tempassignments.Add(TempAssP);

                                                                            //Reviewer
                                                                            TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                                            {
                                                                                ComplianceId = eachSelectedCompliance.ComplianceId,
                                                                                CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                                RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                                                UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                                                IsActive = true,
                                                                                CreatedOn = DateTime.Now
                                                                            };

                                                                            Tempassignments.Add(TempAssR);
                                                                        }
                                                                    }
                                                                });
                                                            });
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "NO Entity/Client Assigned with EPF Code-" + eachpfCode;
                                                        }
                                                    }
                                                });
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "No PF Code Assigned for Selected Entity/Client";
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            //cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                            cvDuplicateEntry.ErrorMessage = "Entity/Branch Details not found, or Select Entity for PF Compliance Assignment";
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        //cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                        cvDuplicateEntry.ErrorMessage = "Entity/Branch Details not found, or Select Entity for PF Compliance Assignment";
                                    }
                                }
                                else if (scopeType.Trim().ToUpper().Equals("SOW14"))
                                {
                                    int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                                    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                                    if (custBranchDetails != null)
                                    {
                                        string clientID = custBranchDetails.CM_ClientID;
                                        //string esiCode = custBranchDetails.CL_PF_Code;

                                        //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);

                                        if (!string.IsNullOrEmpty(clientID))
                                        {
                                            lstCodes = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "ESI");

                                            if (lstCodes.Count > 0)
                                            {
                                                lstCodes.ForEach(eachesiCode =>
                                                {
                                                    var lstBranches = RLCSManagement.GetCustomerBranchesByPFOrESICOrPTOrLWFCode(clientID, "ESI", eachesiCode, "B");

                                                    if (lstBranches.Count > 0)
                                                    {
                                                        lstBranches.ForEach(eachCustBranch =>
                                                        {
                                                            complianceList.ForEach(eachSelectedCompliance =>
                                                            {
                                                                if (eachCustBranch.AVACOM_BranchID != null)
                                                                {
                                                                    if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                                                    {
                                                                        //Performer
                                                                        TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                                        {
                                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                                            CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                            RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                                            UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                                            IsActive = true,
                                                                            CreatedOn = DateTime.Now
                                                                        };

                                                                        Tempassignments.Add(TempAssP);

                                                                        //Reviewer
                                                                        TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                                        {
                                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                                            CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                            RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                                            UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                                            IsActive = true,
                                                                            CreatedOn = DateTime.Now
                                                                        };

                                                                        Tempassignments.Add(TempAssR);
                                                                    }
                                                                }
                                                            });
                                                        });
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "NO Employee Assigned with ESI Code-" + eachesiCode;
                                                    }
                                                });
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "No ESI Code Assigned to Employee";
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                    }
                                }
                                else if (scopeType.Trim().ToUpper().Equals("SOW15")) //LWF Challan
                                {


                                }
                                else if (scopeType.Trim().ToUpper().Equals("SOW17")) //PT Challan
                                {
                                    int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                                    var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                                    if (custBranchDetails != null)
                                    {
                                        //var clientID = RLCS_Master_Management.GetClientIDByCustBranchID(custBranchID);
                                        string clientID = custBranchDetails.CM_ClientID;
                                        //string ptState = custBranchDetails.CL_PT_State;

                                        if (!string.IsNullOrEmpty(clientID))
                                        {
                                            lstCodes = RLCS_Master_Management.GetPTStates(custBranchID, clientID, "PT");
                                            //lstCodes = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "PT");

                                            if (lstCodes.Count > 0)
                                            {
                                                lstCodes.ForEach(eachptState =>
                                                {
                                                    var lstBranches = RLCSManagement.GetCustomerBranchesByPFOrESICOrPTOrLWFCode(clientID, "PT", eachptState, "B");
                                                    //var lstBranches = RLCSManagement.GetCustomerBranchesByPTState(customerID, clientID, eachptState, "B");
                                                    if (lstBranches.Count > 0)
                                                    {
                                                        complianceList.ForEach(eachSelectedCompliance =>
                                                        {
                                                            if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                                            {
                                                                if (!string.IsNullOrEmpty(eachSelectedCompliance.StateCode))
                                                                {
                                                                    //var lstBranches = RLCSManagement.GetCustomerBranchesByPTState(customerID, clientID, eachSelectedCompliance.StateCode, "B");

                                                                    lstBranches.ForEach(eachCustBranch =>
                                                                    {
                                                                        //Performer
                                                                        TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                                        {
                                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                                            CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                            RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                                            UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                                            IsActive = true,
                                                                            CreatedOn = DateTime.Now
                                                                        };

                                                                        Tempassignments.Add(TempAssP);

                                                                        //Reviewer
                                                                        TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                                        {
                                                                            ComplianceId = eachSelectedCompliance.ComplianceId,
                                                                            CustomerBranchID = Convert.ToInt32(eachCustBranch.AVACOM_BranchID),
                                                                            RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                                            UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                                            IsActive = true,
                                                                            CreatedOn = DateTime.Now
                                                                        };

                                                                        Tempassignments.Add(TempAssR);
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntry.IsValid = false;
                                                        cvDuplicateEntry.ErrorMessage = "No PT State mapped to Selected Branch";
                                                    }
                                                });
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "No PT State Assigned for Selected Entity/Client";
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Branch Details not found, Please try again";
                                        }
                                    }
                                }
                                else if (scopeType.Trim().ToUpper().Equals("SOW05")) //Return
                                {
                                    int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                                    if (custBranchID != -1)
                                    {
                                        var custBranchDetails = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                                        if (custBranchDetails != null)
                                        {
                                            complianceList.ForEach(eachSelectedCompliance =>
                                            {
                                                if (eachSelectedCompliance.Performer) //Performer - means Compliance Selected or Not
                                                {
                                                    //Performer
                                                    TempAssignmentTable TempAssP = new TempAssignmentTable()
                                                    {
                                                        ComplianceId = eachSelectedCompliance.ComplianceId,
                                                        CustomerBranchID = custBranchID,
                                                        RoleID = 3, //RoleManagement.GetByCode("PERF").ID,
                                                        UserID = Convert.ToInt32(ddlFilterPerformer.SelectedValue),
                                                        IsActive = true,
                                                        CreatedOn = DateTime.Now
                                                    };

                                                    Tempassignments.Add(TempAssP);

                                                    //Reviewer
                                                    TempAssignmentTable TempAssR = new TempAssignmentTable()
                                                    {
                                                        ComplianceId = eachSelectedCompliance.ComplianceId,
                                                        CustomerBranchID = custBranchID,
                                                        RoleID = 4, //RoleManagement.GetByCode("PERF").ID,
                                                        UserID = Convert.ToInt32(ddlFilterReviewer.SelectedValue),
                                                        IsActive = true,
                                                        CreatedOn = DateTime.Now
                                                    };

                                                    Tempassignments.Add(TempAssR);
                                                }
                                            });
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Select Branch";
                                        }
                                    }
                                }
                                if (Tempassignments.Count != 0)
                                {
                                    RLCS_ComplianceManagement.SaveTempAssignments(Tempassignments);
                                    ClearSelection();
                                    assignSuccess = true;
                                }

                                if (assignSuccess)
                                {
                                    tvFilterLocation_SelectedNodeChanged(null, null);
                                    //BindGrid();
                                    ViewState["CHECKED_ITEMS"] = null;

                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Selected Details Save Successfully";
                                    vsHRCompAssign.CssClass = "alert alert-success";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Select one or more compliance to assign";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please select User to Assign";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please select at least one Entity/Location/Branch";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please select Customer.";

                }
            }
            catch (Exception ex)
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "";
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void ClearSelection()
        {
            tbxFilterLocation.Text = "< Select >";
            ddlFilterPerformer.ClearSelection();
            //ddlFilterPerformer.SelectedValue = "-1";
            ddlFilterReviewer.ClearSelection();
            //ddlFilterReviewer.SelectedValue = "-1";
            ddlFilterApprover.ClearSelection();
            //ddlFilterApprover.SelectedValue = "-1";
            ddlComplianceCatagory.SelectedValue = "-1";
            //ddlScopeType.SelectedValue = "-1";
        }

        private void SaveCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = new List<ComplianceAsignmentProperties>();
                // Check in the Session
                if (ViewState["CHECKED_ITEMS"] != null)
                    complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                {
                    ComplianceAsignmentProperties complianceProperties = new ComplianceAsignmentProperties();
                    complianceProperties.ComplianceId = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                    complianceProperties.Performer = ((CheckBox)gvrow.FindControl("chkAssign")).Checked;
                    complianceProperties.StateCode = ((Label)gvrow.FindControl("lblStateCode")).Text;

                    if (complianceProperties.Performer)
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                        {
                            complianceList.Remove(rmdata);
                            complianceList.Add(complianceProperties);
                        }
                        else
                        {
                            complianceList.Add(complianceProperties);
                        }
                    }
                    else
                    {
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == complianceProperties.ComplianceId).FirstOrDefault();
                        if (rmdata != null)
                            complianceList.Remove(rmdata);
                    }
                }

                if (complianceList != null && complianceList.Count > 0)
                    ViewState["CHECKED_ITEMS"] = complianceList;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void PopulateCheckedValues()
        {
            try
            {
                List<ComplianceAsignmentProperties> complianceList = ViewState["CHECKED_ITEMS"] as List<ComplianceAsignmentProperties>;

                if (complianceList != null && complianceList.Count > 0)
                {
                    foreach (GridViewRow gvrow in grdComplianceRoleMatrix.Rows)
                    {
                        int index = Convert.ToInt32(grdComplianceRoleMatrix.DataKeys[gvrow.RowIndex].Value);
                        ComplianceAsignmentProperties rmdata = complianceList.Where(t => t.ComplianceId == index).FirstOrDefault();
                        if (rmdata != null)
                        {
                            CheckBox chkPerformer = (CheckBox)gvrow.FindControl("chkAssign");
                            chkPerformer.Checked = rmdata.Performer;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                if (Session["TotalComplianceAssign"] != null)
                    TotalRows.Value = Session["TotalComplianceAssign"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;
                DivnextScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalComplianceAssign"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalComplianceAssign"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalComplianceAssign"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                    DivnextScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalComplianceAssign"]))
                    EndRecord = Convert.ToInt32(Session["TotalComplianceAssign"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalComplianceAssign"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalComplianceAssign"]))
                    EndRecord = Convert.ToInt32(Session["TotalComplianceAssign"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindGrid();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdComplianceRoleMatrix.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceRoleMatrix.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindGrid();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        #region To Find Branches not Entities and Sub-Entities

        public static List<NameValueHierarchy> GetAll_Branches(int customerID, int selectedEntityID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedEntityID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    LoadChildBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadChildBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                && row.Type != 1
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadChildBranches(customerid, item, false, entities);
            }
        }

        #endregion

        #region To Find Child Branches

        public static List<NameValueHierarchy> GetAll_SubBranches(int customerID, int selectedBranchID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedBranchID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchList.Add(item.ID);
                    LoadSubBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadSubBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadSubBranches(customerid, item, false, entities);
            }
        }

        #endregion

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string masterpage = Convert.ToString(Session["masterpage"]);
            if (masterpage == "HRPlusSPOCMGR")
            {
                this.Page.MasterPageFile = "~/HRPlusSPOCMGR.Master";
            }
            else if (masterpage == "HRPlusCompliance")
            {
                this.Page.MasterPageFile = "~/HRPlusCompliance.Master";
            }
        }
    }
}