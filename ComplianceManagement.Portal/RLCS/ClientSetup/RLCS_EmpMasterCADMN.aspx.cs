﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    public partial class RLCS_EmpMasterCADMN : System.Web.UI.Page
    {
        protected static int CustID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = "Name";

                    BindCustomers();
                    BindEmployees();

                    Session["CurrentRole"] = AuthenticationHelper.Role;
                    Session["CurrentUserId"] = AuthenticationHelper.UserID;

                    btnAddEmployee.Visible = false;
                    btnUploadEmployee.Visible = false;
                }
                else
                { 
                    FormsAuthentication.SignOut();
                    Session.Abandon();
                    FormsAuthentication.RedirectToLoginPage();
                }
            }            
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                int customerID = -1;
                

                if (AuthenticationHelper.Role == "CADMN")
                {   
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);                    
                }

                int serviceProviderID = 95;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlCustomerList.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(customerID, serviceProviderID, 2);
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("< Select Customer >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindEmployees()
        {
            try
            {
                int customerID = -1;
                
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);                  
                }

                grdEmployee.DataSource = RLCS_Master_Management.GetAll_EmployeeMaster(customerID);
                grdEmployee.DataBind();

                upEmpMasterCADMN.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (ddlCustomerList.SelectedItem.Text == "< Select Customer >")
                    {
                        btnAddEmployee.Visible = false;
                        btnUploadEmployee.Visible = false;
                    }
                    else
                    {
                        btnAddEmployee.Visible = true;
                        btnUploadEmployee.Visible = true;
                    }
                }

                if(!String.IsNullOrEmpty(ddlCustomerList.SelectedValue) && ddlCustomerList.SelectedItem.Text != "< Select Customer >")
                {
                    CustID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                    Session["CustID"] = CustID;

                    btnAddEmployee.Visible = true;
                    btnUploadEmployee.Visible = true;
                }
                else
                {
                    btnAddEmployee.Visible = false;
                    btnUploadEmployee.Visible = false;
                }
                
                BindEmployees();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdEmployee_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }

                var employees = RLCS_Master_Management.GetAll_EmployeeMaster(customerID);

                if (direction == SortDirection.Ascending)
                {
                    employees = employees.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    employees = employees.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdEmployee.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdEmployee.Columns.IndexOf(field);
                    }
                }

                grdEmployee.DataSource = employees;
                grdEmployee.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdEmployee.PageIndex = e.NewPageIndex;

                BindEmployees();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdEmployee_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                bool deleteSuccess = false;
                if (e.CommandArgument != null)
                {
                    if (e.CommandName.Equals("Delete_Employee"))
                    {
                        int recordID = Convert.ToInt32(e.CommandArgument);

                        if (recordID != 0)
                            deleteSuccess = RLCS_Master_Management.DeleteEmployee(recordID, "I");

                        if (deleteSuccess)
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Employee Record Deleted Successfully";

                            BindEmployees();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdEmployee.PageIndex = 0;
                BindEmployees();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upEmpMasterCADMN_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "initializeCombobox", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnRefreshEmployee_Click(object sender, EventArgs e)
        {
            try
            {                
                BindEmployees();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
    }
}