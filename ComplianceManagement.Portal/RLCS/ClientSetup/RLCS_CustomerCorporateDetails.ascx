﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RLCS_CustomerCorporateDetails.ascx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_CustomerCorporateDetails" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<style>
     .m-10{
            margin-left: 10px;
        }
</style>
<style type="text/css">

    .icon-th-Logo {  
    height: 33px;
    width: 41px;
    float: left;
    margin-right: 4px;
}
</style>
<div id="divCustomersDialog">
    <asp:UpdatePanel ID="upCustomers" runat="server" UpdateMode="Conditional" OnLoad="upCustomers_Load">
        <ContentTemplate>
            <div style="margin: 5px">
                <div style="margin-bottom: 4px">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vdsummary" ValidationGroup="CustomerValidationGroup" />
                    <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                        ValidationGroup="CustomerValidationGroup" Display="None" />
                    <asp:Label runat="server" ID="lblErrorMassage" Style="color: Red"></asp:Label>
                </div>

                <div style="margin-bottom: 7px; display: none ">
                    <asp:Label Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">&nbsp;</asp:Label>
                    <asp:Label Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                       Is Service Provider:</asp:Label>
                    <asp:CheckBox ID="chkSp" runat="server" />
                </div>

                <div style="margin-bottom: 7px">
                    <asp:Label ID="lblCustDistComp" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label ID="lblCustDist" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                        Select:</asp:Label>
                    <asp:RadioButtonList ID="rblCustomerDistributor" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblCustomerDistributor_CheckedChanged" AutoPostBack="true">
                        <asp:ListItem Text="Customer" Value="C" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Professional Firm" Value="D"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>

                <div style="margin-bottom: 7px" id="divSubDist" runat="server">
                    <asp:Label ID="Label3" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label ID="Label4" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">Can Create Sub-Distributor(s)?</asp:Label>

                    <asp:RadioButtonList ID="rblSubDistributor" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="0"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>

                <div style="margin-bottom: 7px; display: none" runat="server" id="divSP">
                    <asp:Label ID="lblast" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label ID="lblsp" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                      Service Provider:</asp:Label>
                    <asp:DropDownList runat="server" ID="ddlSPName" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">                       
                    </asp:DropDownList>
                </div>  
                
                <div style="margin-bottom: 7px;" runat="server" id="divDistributor">
                    <asp:Label ID="Label1" Style="width: 10px; display: block; float: left; font-size: 13px; color: red;" runat="server">*</asp:Label>
                    <asp:Label ID="Label2" Style="width: 200px; display: block; float: left; font-size: 13px; color: #333;" runat="server">
                      Distributor:</asp:Label>
                    <asp:DropDownList runat="server" ID="ddlDistributor" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">                      
                    </asp:DropDownList>
                </div>                

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Name</label>
                    <asp:TextBox runat="server" ID="tbxName" Style="height: 16px; width: 390px;" autocomplete="off" MaxLength="50" OnTextChanged="tbxName_TextChanged" AutoPostBack="true" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Name can not be empty."
                        ControlToValidate="tbxName" runat="server" ValidationGroup="CustomerValidationGroup" Display="None" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid name" ControlToValidate="tbxName" ValidationExpression="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"></asp:RegularExpressionValidator>
                <%--="^[a-zA-Z_&]+[a-zA-Z0-9&_ .-]*$"--%>  
                </div>

                <div id="divCorpID" runat="server">
                    <div style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            CorporateID</label>
                        <asp:TextBox runat="server" ID="txtCorpID" Style="height: 16px; width: 325px;" MaxLength="10" autoComplete="off" />
                        <asp:RequiredFieldValidator ErrorMessage="CorporateID can not be empty" ControlToValidate="txtCorpID"
                            runat="server" ValidationGroup="CustomerValidationGroup" Display="None" />
                        <asp:Button Text="Check" runat="server" ID="btnCheckCorpID" CssClass="button" Style="width: 60px;"
                            OnClick="btnCheckCorpID_Click" />
                    </div>
                    <div id="divlblCorpIDMsg" runat="server" style="margin-bottom: 7px">
                        <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                        <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                            &nbsp;</label>
                        <asp:Label runat="server" ID="lblCheckCorporate" ForeColor="red" CssClass="m-10"
                            Text="Please click on Check button to verify CorporateID"></asp:Label>
                    </div>
                </div>

                <div style="margin-bottom: 7px; display:none">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Location Type:</label>
                    <asp:DropDownList runat="server" ID="ddlLocationType" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;" />
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Name</label>
                    <asp:TextBox runat="server" ID="tbxBuyerName" Style="height: 16px; width: 390px;" autocomplete="off" 
                        MaxLength="50" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Buyer Name can not be empty."
                        ControlToValidate="tbxBuyerName" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Contact No</label>
                    <asp:TextBox runat="server" ID="tbxBuyerContactNo" Style="height: 16px; width: 390px;" autocomplete="off" 
                        MaxLength="15" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Buyer Contact Number can not be empty"
                        ControlToValidate="tbxBuyerContactNo" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid contact number." ControlToValidate="tbxBuyerContactNo"
                        ValidationExpression="^[\+\d]+(?:[\d-.\s()]*)$"></asp:RegularExpressionValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers" TargetControlID="tbxBuyerContactNo" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" Display="None" runat="server"
                        ValidationGroup="CustomerValidationGroup" ErrorMessage="Buyer Contact No can be of 10 digits"
                        ControlToValidate="tbxBuyerContactNo" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">*</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Buyer Email</label>
                    <asp:TextBox runat="server" ID="tbxBuyerEmail" Style="height: 16px; width: 390px;" autocomplete="off" 
                        MaxLength="200" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Buyer Email can not be empty."
                        ControlToValidate="tbxBuyerEmail" runat="server" ValidationGroup="CustomerValidationGroup"
                        Display="None" />
                    <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                        ErrorMessage="Please enter a valid email." ControlToValidate="tbxBuyerEmail"
                        ValidationExpression="^([\w-_]+\.)*[\w-_]+@([\w-_]+\.)*[\w-_]+\.[\w-_]+$"></asp:RegularExpressionValidator>
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Customer Status</label>
                    <asp:DropDownList runat="server" ID="ddlCustomerStatus" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;" />
                </div>

                <div style="margin-bottom: 7px;display:none">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <asp:label runat="server" id="lblstartdate" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Start Date</asp:label>
                    <asp:TextBox runat="server" ID="txtStartDate" Style="height: 16px; width: 390px;" ReadOnly="true" autocomplete="off" 
                        MaxLength="200" />
                </div>

                <div style="margin-bottom: 7px;display:none">
                    <label  style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <asp:label id="lblenddate" runat="server" style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        End Date</asp:label>
                    <asp:TextBox runat="server" ID="txtEndDate" Style="height: 16px; width: 390px;" ReadOnly="true" autocomplete="off" 
                        MaxLength="200" />
                </div>

                <div style="margin-bottom: 7px;display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Disk Space</label>
                    <asp:TextBox runat="server" ID="txtDiskSpace" Style="height: 16px; width: 390px;" autocomplete="off" 
                        MaxLength="200" />
                </div>

                <div style="margin-bottom: 7px; display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Internal Compliance Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlComplianceApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <asp:ListItem Text="No" Value="0"  />
                        <asp:ListItem Text="Yes" Value="1" Selected="True" />
                    </asp:DropDownList>
                </div>

               <div style="margin-bottom: 7px; display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Task Management Applicable</label>
                    <asp:DropDownList runat="server" ID="ddlTaskApplicable" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox">
                        <asp:ListItem Text="No" Value="0" />
                        <asp:ListItem Text="Yes" Value="1" Selected="True" />
                    </asp:DropDownList>
                </div>

                <div style="margin-bottom: 7px; display:none;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Compliance Product Type</label>
                    <asp:DropDownList runat="server" ID="ddlComplianceProductType" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                        CssClass="custom-combobox" OnSelectedIndexChanged="ddlComplianceProductType_SelectedIndexChanged" AutoPostBack="true">
                        <%--<asp:ListItem Text="Compliance" Value="0" />
                        <asp:ListItem Text="HR Compliance" Value="1" />--%>
                        <asp:ListItem Text="HR+Compliance" Value="2" Selected="True" />
                    </asp:DropDownList>
                </div>

                
                <div style="margin-bottom: 7px" id="divPAN" runat="server">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        PAN</label>
                    <asp:TextBox runat="server" ID="txtPAN" Style="height: 16px; width: 390px;" MaxLength="10" autoComplete="off" />
                    <asp:RegularExpressionValidator ID="revPAN" ControlToValidate="txtPAN" runat="server" ErrorMessage="Invalid PAN"
                        ValidationGroup="CustomerValidationGroup" ValidationExpression="^[\w]{5}[\d]{4}[\w]$" Display="None"></asp:RegularExpressionValidator>
                </div>

                <div style="margin-bottom: 7px" id="divTAN" runat="server">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        TAN</label>
                    <asp:TextBox runat="server" ID="txtTAN" Style="height: 16px; width: 390px;" autoComplete="off" MaxLength="10" />
                    <asp:RegularExpressionValidator ID="revTAN" ControlToValidate="txtTAN" runat="server" ErrorMessage="Invalid TAN"
                        ValidationGroup="CustomerValidationGroup" ValidationExpression="^[\w]{4}[\d]{5}[\w]$" Display="None"></asp:RegularExpressionValidator>
                </div>

                <div style="margin-bottom: 7px">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Address</label>
                    <asp:TextBox runat="server" ID="tbxAddress" Style="height: 50px; width: 390px;" MaxLength="500"
                        TextMode="MultiLine" autocomplete="off"  />
                </div>

                <div style="margin-bottom: 7px" id="divCountry" runat="server">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        Country</label>
                    <asp:DropDownList runat="server" ID="ddlCountry" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true" />
                </div>

                <div style="margin-bottom: 7px" id="divState" runat="server">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        State</label>
                    <asp:DropDownList runat="server" ID="ddlState" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;"
                        OnSelectedIndexChanged="ddlState_SelectedIndexChanged" AutoPostBack="true" />
                </div>

                <div style="margin-bottom: 7px" id="divCity" runat="server">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                        City</label>
                    <asp:DropDownList runat="server" ID="ddlCity" Style="padding: 0px; margin: 0px; height: 22px; width: 200px;" />
                </div>

                <table>
                    <tr>
                        <td>
                            <div style="margin-bottom: 7px">
                                <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                                <label style="width: 200px; display: block; float: left; font-size: 13px; color: #333;">
                                    Logo</label>
                                <asp:FileUpload ID="fuCustLogoUpload" runat="server" />

                                <asp:RegularExpressionValidator Display="None" runat="server" ValidationGroup="CustomerValidationGroup"
                                    ErrorMessage="Unsupported Logo File. Supported files are (*.gif,*.jpeg,*.jpeg,*.png)" ControlToValidate="fuCustLogoUpload"
                                    ValidationExpression="^([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.jpeg|.gif)$"></asp:RegularExpressionValidator>

                            </div>
                        </td>
                        <td><span visible="false" id="imgDiv" runat="server">
                            <asp:Image ID="ImgLogo" runat="server" class="icon-th-Logo" /></span>
                        </td>
                    </tr>
                </table>

                <div style="text-align: center; margin-top: 10px;">
                    <asp:Button Text="Save" runat="server" ID="btnSave" OnClick="btnSave_Click" CssClass="button"
                        ValidationGroup="CustomerValidationGroup" />
                    <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="button" OnClientClick="$('#divCustomersDialog').dialog('close');" />
                </div>
            </div>

            <div style="margin-bottom: 7px; float: left; margin-left: 10px; margin-top: 10px;">
                <p style="color: red;"><strong>Note:</strong> (*) fields are compulsory</p>
            </div>
        </ContentTemplate>
         <Triggers>
           <asp:PostBackTrigger ControlID="btnsave" />
          </Triggers>
    </asp:UpdatePanel>
</div>

<script type="text/javascript">
    $(function () {
        $('#divCustomersDialog').dialog({
            height: 580,
            width: 700,
            autoOpen: false,
            draggable: true,
            title: "Customer Information",
            open: function (type, data) {
                $(this).parent().appendTo("form");
            }
        });    
    });
    
    function initializeDatePicker(date) {
       
        var startDate = new Date();
       
        $("#<%= txtStartDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "minDate", startDate);
            }
        });

            $("#<%= txtEndDate.ClientID %>").datepicker({
            dateFormat: 'dd-mm-yy',
            defaultDate: startDate,
            numberOfMonths: 1,
            minDate: startDate,
            onClose: function (startDate) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "maxDate", startDate);
            }
            });


            if (date != null) {
                $("#<%= txtStartDate.ClientID %>").datepicker("option", "defaultDate", date);
                $("#<%= txtEndDate.ClientID %>").datepicker("option", "defaultDate", date);
            }
        }
   
    function initializeCombobox() {
        $("#<%= ddlCustomerStatus.ClientID %>").combobox();
        $("#<%= ddlLocationType.ClientID %>").combobox();
        $("#<%= ddlTaskApplicable.ClientID %>").combobox();
        $("#<%= ddlComplianceApplicable.ClientID %>").combobox();

        $("#<%= ddlSPName.ClientID %>").combobox();
        $("#<%= ddlDistributor.ClientID %>").combobox();

        $("#<%= ddlComplianceProductType.ClientID %>").combobox();
        $("#<%= ddlCountry.ClientID %>").combobox();
        $("#<%= ddlState.ClientID %>").combobox();
        $("#<%= ddlCity.ClientID %>").combobox();
    }

</script>