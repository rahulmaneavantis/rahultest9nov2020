﻿<%@ Page Title="Employee Master:: Setup HR+" Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_EmpMasterCADMN.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_EmpMasterCADMN" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>

    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />

    <style type="text/css">
        *.disabled {
            cursor: not-allowed;
        }

        .aspNetDisabled {
            cursor: not-allowed;
        }

        .m-10 {
            margin-left: 10px;
        }

        .m-20 {
            margin-left: 20px;
        }
    </style>

     <script type="text/javascript">
        function initializeCombobox() {            
            $("#<%= ddlCustomerList.ClientID %>").combobox();
        }

        $(document).ready(function () {
            initializeCombobox();
        });
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">
        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity/ Location/ Branch"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmpMasterCADMN.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity/Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upEmpMasterCADMN" runat="server" UpdateMode="Conditional" OnLoad="upEmpMasterCADMN_Load">
        <ContentTemplate>
            <div style="width: 100%; margin-bottom: 4px">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
            </div>
            <div style="width: 100%">
                <table width="100%">
                    <tr>
                        <td style="width: 40%;">
                            <div id="divCustomerfilter" runat="server">
                                <div style="width: 25%; float: left;">
                                    <label style="display: block; float: right; font-size: 13px; color: #333; font-weight: bold; margin-top: 5px;">
                                        Customer :</label>
                                </div>
                                <div style="width: 75%; float: left;">
                                    <asp:DropDownList runat="server" ID="ddlCustomerList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerList_SelectedIndexChanged" />
                                </div>
                            </div>
                        </td>
                        <td style="width: 40%;">
                            <div style="width: 30%; float: left;">
                                <label style="display: block; float: right; font-size: 13px; color: #333; font-weight: bold; margin-top: 5px;">Filter :</label>
                            </div>
                            <div style="width: 70%; float: left;">
                                <asp:TextBox runat="server" ID="tbxFilter" Width="100%" MaxLength="50" AutoPostBack="true" />
                                <%--OnTextChanged="tbxFilter_TextChanged"--%>
                            </div>
                        </td>
                        <td style="width: 20%; padding-left: 30px;">
                            <asp:LinkButton Text="Add New" runat="server" ID="btnAddEmployee" CssClass="button" OnClientClick="return OpenAddEmployeePopup(null)" />
                            <%-- OnClick="btnAddUser_Click"--%>
                            <asp:LinkButton Text="Upload" runat="server" ID="btnUploadEmployee" OnClientClick="return OpenUploadWindow()" CssClass="button m-20" />
                            <asp:LinkButton Text="Refresh" runat="server" ID="lnkBtnRefresh" CssClass="button m-20" OnClick="btnRefreshEmployee_Click" />
                        </td>
                    </tr>
                </table>

                <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                    <asp:GridView runat="server" ID="grdEmployee" AutoGenerateColumns="false" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" Font-Size="12px" OnSorting="grdEmployee_Sorting"
                        OnPageIndexChanging="grdEmployee_PageIndexChanging" OnRowCommand="grdEmployee_RowCommand">
                        <Columns>
                             <asp:TemplateField HeaderText="Sr" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <asp:BoundField DataField="EM_EMPID" HeaderText="EMPID" SortExpression="EM_EMPID" />
                            <asp:BoundField DataField="EM_EmpName" HeaderText="Name" SortExpression="EM_EmpName" />
                            <asp:BoundField DataField="EM_ClientID" HeaderText="ClientID" SortExpression="EM_ClientID" />
                            <asp:BoundField DataField="EM_Branch" HeaderText="Location/Branch" SortExpression="EM_Branch" />
                            <asp:BoundField DataField="EM_ClientID" HeaderText="Client" SortExpression="EM_ClientID" Visible="false" />
                            <asp:BoundField DataField="AVACOM_CustomerID" HeaderText="customer" SortExpression="AVACOM_CustomerID" Visible="false" />
                            <asp:TemplateField HeaderText="Date of Birth" ItemStyle-HorizontalAlign="Center" SortExpression="EM_DOB">
                                <ItemTemplate>
                                    <%# Eval("EM_DOB") != null ? Convert.ToDateTime(Eval("EM_DOB")).ToString("dd-MM-yyyy") : "" %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date of Joining" ItemStyle-HorizontalAlign="Center" SortExpression="EM_DOJ">
                                <ItemTemplate>
                                    <%# Eval("EM_DOJ") != null ? Convert.ToDateTime(Eval("EM_DOJ")).ToString("dd-MM-yyyy") : "" %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Date of Leaving" ItemStyle-HorizontalAlign="Center" SortExpression="EM_DOL">
                                <ItemTemplate>
                                    <%# Eval("EM_DOL") != null ? Convert.ToDateTime(Eval("EM_DOL")).ToString("dd-MM-yyyy") : "" %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="EM_Client_ESI_Number" HeaderText="Client ESI" ItemStyle-HorizontalAlign="Center" SortExpression="EM_Client_ESI_Number" />
                            <asp:BoundField DataField="EM_Client_PT_State" HeaderText="PT State" ItemStyle-HorizontalAlign="Center" SortExpression="EM_Client_PT_State" />

                            <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" SortExpression="EM_Status">
                                <ItemTemplate>
                                    <%# Eval("EM_Status") != null ? (Convert.ToString(Eval("EM_Status"))=="A")?"Active":"In-Active" : "" %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEditEmployee" runat="server" CommandName="RLCS_Employee" data-clId='<%# Eval("EM_EMPID")%>' data-custId='<%# Eval("AVACOM_CustomerID")%>' data-client='<%# Eval("EM_ClientID")%>' CommandArgument='<%# Eval("EM_EMPID") %>' OnClientClick="return EmployeeSetup(this)">                                                                      
                                        <img src="../../Images/edit_icon.png" alt="Edit" title="View/Edit Employee Details" /></asp:LinkButton>
                                    <asp:LinkButton ID="lnkDeleteEmployee" runat="server" CommandName="Delete_Employee" CommandArgument='<%# Eval("EM_ID") %>'
                                        OnClientClick="return confirm('Are you certain you want to delete this User?');"><img src="../../Images/delete_icon.png" alt="Delete" title="Delete Employee" /></asp:LinkButton>
                                     <%--CommandArgument='<%# Eval("EM_EMPID") + ","+ Eval("EM_ClientID") + ","+ Eval("AVACOM_CustomerID") %>'--%>
                                </ItemTemplate>
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        $(function () {
            $('#divRLCSEmployeeUploadDialog').hide();
            $('#divRLCSEmployeeAddDialog').hide();
            $('#divCustomerBranchesDialog').dialog({
                height: 650,
                width: 720,
                autoOpen: false,
                draggable: true,
                title: "Branch Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });

        function EmployeeSetup(obj) {
            debugger;
            
            var EmpID = $(obj).attr('data-clId');
            var client = $(obj).attr('data-client');
            var cust = $(obj).attr('data-custId');

            alert(EmpID + '' + client + '' + cust);

            OpenAddEmployeePopup(EmpID, cust, client);
            return false;
        }

        function OpenUploadWindow() {
            debugger;
             cust=<%=CustID%>
            $('#divRLCSEmployeeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeUpload').attr('src', "/Setup/UploadEmployeeFiles?CustID="+ cust);
            var myWindowAdv = $("#divRLCSEmployeeUploadDialog");

            function onClose() {
            }

            myWindowAdv.kendoWindow({
                width: "50%",
                height: "50%",
                title: "Upload Employee Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;
        }

        function OpenAddEmployeePopup(ID, cust, client) {
            debugger;
            if (cust === undefined || cust == null)            
                cust =<%=CustID%>;
            alert(cust);
            $('#divRLCSEmployeeAddDialog').show();
            var myWindowAdv = $("#divRLCSEmployeeAddDialog");

            function onClose() {
            }

            myWindowAdv.kendoWindow({
                width: "85%",
                height: '89%',
                title: "Employee Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeAdd').attr('src', "/Setup/CreateEmplodyeeMaster?EmpId=" + ID + "&CustID=" + cust + "&Client=" + client);

            return false;
        }
    </script>

    <div id="divRLCSEmployeeUploadDialog">
        <iframe id="iframeUpload" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeAddDialog">
        <iframe id="iframeAdd" style="width: 1165px; height: 100%; border: none"></iframe>
    </div>
</asp:Content>
