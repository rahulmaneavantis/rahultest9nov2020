﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    public partial class RLCS_UserCustomerMapping : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divFilterUsers.Visible = true;
                btnAddNew.Visible = true;

                BindUsers();

                //BindUsers(ddlUsers);
                //BindUsers(ddlFilterUsers);

                BindCustomers();

                BindUserCustomerMapping();
            }
        }
             
        private void BindUserCustomerMapping()
        {
            try
            {
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int selectedUserID = string.IsNullOrEmpty(ddlFilterUsers.SelectedValue) ? -1 : Convert.ToInt32(ddlFilterUsers.SelectedValue);
                int selectedMgrID = string.IsNullOrEmpty(ddlMgrPage.SelectedValue) ? -1 : Convert.ToInt32(ddlMgrPage.SelectedValue);

                int selectedCustID = -1;

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    selectedCustID = string.IsNullOrEmpty(ddlCustomer.SelectedValue) ? -1 : Convert.ToInt32(ddlCustomer.SelectedValue);
                }

                var lstUserCustomerMapping = UserCustomerMappingManagement.GetUserCustomerMappingList(selectedUserID, selectedCustID, serviceProviderID, distributorID, selectedMgrID);

                if (lstUserCustomerMapping.Count > 0)
                {
                    //if (custID != -1)
                    //    lstUserCustomerMapping = lstUserCustomerMapping.Where(row => row.CustomerID == custID).ToList();

                    //if (userID != -1)
                    //    lstUserCustomerMapping = lstUserCustomerMapping.Where(row => row.UserID == userID).ToList();
                    
                    grdUserCustomerMapping.DataSource = lstUserCustomerMapping;
                    grdUserCustomerMapping.DataBind();

                    upUserCustomerList.Update();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upComplianceTypeList_Load(object sender, EventArgs e)
        {
            try
            {
                //ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void upMapUser_Load(object sender, EventArgs e)
        {
            try
            {
             //   ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranches", string.Format("initializeJQueryUI('{0}', 'divBranches');", tbxBranch.ClientID), true);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeCombobox();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
              
        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomers()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int serviceProviderID = -1;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int distributorID = -1;

                var customerList = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, false);                //var customerList = CustomerManagement.GetAll_HRComplianceCustomers_IncludesServiceProvider(customerID, serviceProviderID, 2);
                //var customerList = CustomerManagement.GetAll_HRComplianceCustomers(customerID, serviceProviderID, 2);

                #region Page Drop Down

                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                ddlCustomer.DataSource = customerList;
                ddlCustomer.DataBind();
                                
                ddlCustomer.Items.Insert(0, new ListItem("Select", "-1"));

                #endregion

                #region Pop-Up DropDown

                lblCustomerPopUP.DataTextField = "Name";
                lblCustomerPopUP.DataValueField = "ID";

                lblCustomerPopUP.DataSource = customerList;
                lblCustomerPopUP.DataBind();

                lblCustomerPopUP.Items.Insert(0, new ListItem("< Select Customer >", "-1"));

                #endregion
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        //private void BindCustomersPopUp()
        //{
        //    try
        //    {
        //        lblCustomerPopUP.DataTextField = "Name";
        //        lblCustomerPopUP.DataValueField = "ID";

        //        int customerID = -1;
        //        if (AuthenticationHelper.Role == "CADMN")
        //        {   
        //            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        }

        //        int serviceProviderID = 95;
        //        if (AuthenticationHelper.Role == "SPADM")
        //        {
        //            serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        }

        //        lblCustomerPopUP.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(customerID, serviceProviderID, 2);                
        //        lblCustomerPopUP.DataBind();

        //        lblCustomerPopUP.Items.Insert(0, new ListItem("< Select Customer >", "-1"));                
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvDuplicateEntry.IsValid = false;
        //        cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
        //    }
        //}

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlFilterUsers.ClearSelection();
                ddlMgrPage.ClearSelection();

                BindUserCustomerMapping();

                //BindUsers(ddlUsers);
                //BindUsers(ddlFilterUsers);

                if (ddlCustomer.SelectedIndex != -1)
                    btnAddNew.Visible = true;
                else
                    btnAddNew.Visible = false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void ForceCloseFilterBranchesTreeView()
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideFilterTreeView", "$(\"#divFilterLocation\").hide(\"blind\", null, 5, function () { });", true);
        }

        private void BindUsers()
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    customerID = -1;
                }

                int distID = -1;
                if (AuthenticationHelper.Role == "DADMN")
                {
                    distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int serviceProviderID = -1;
                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                var lstUsers = UserCustomerMappingManagement.GetAll_Users(customerID, serviceProviderID, distID);

                BindUserToDropDown(lstUsers, ddlFilterUsers);
                BindUserToDropDown(lstUsers, ddlMgrPage, "HMGR");
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindUserToDropDown(List<SP_RLCS_GetAllUser_ServiceProviderDistributor_Result> lstUsers, DropDownList ddltoBind, string roleCode = "")
        {
            try
            {
                if (lstUsers.Count > 0 && !string.IsNullOrEmpty(roleCode))
                    lstUsers = lstUsers.Where(row => row.RoleCode == roleCode).ToList();

                List<object> users = new List<object>();
                users = (from row in lstUsers
                         select new { ID = row.ID, Name = row.FirstName + " " + row.LastName, RoleCode = row.RoleCode }).Distinct().OrderBy(row => row.Name).ToList<object>();

                ddltoBind.Items.Clear();

                ddltoBind.DataTextField = "Name";
                ddltoBind.DataValueField = "ID";

                users.Insert(0, new { ID = -1, Name = "Select" });

                ddltoBind.DataSource = users;
                ddltoBind.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlFilterUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomer.ClearSelection();
            ddlMgrPage.ClearSelection();

            BindUserCustomerMapping();
        }

        protected void ddlMgrPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCustomer.ClearSelection();
            ddlFilterUsers.ClearSelection();

            BindUserCustomerMapping();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool saveSuccess = false;
                if (!string.IsNullOrEmpty(lblCustomerPopUP.SelectedValue) && lblCustomerPopUP.SelectedValue != "-1")
                {
                    if (!string.IsNullOrEmpty(ddlUsers.SelectedValue) && ddlUsers.SelectedValue != "-1")
                    {
                        int userID = Convert.ToInt32(ddlUsers.SelectedValue);   

                        List<UserCustomerMapping> lstUserCustmerMapping = new List<UserCustomerMapping>();

                        foreach (ListItem eachCustomer in lblCustomerPopUP.Items)
                        {
                            if (eachCustomer.Selected)
                            {
                                UserCustomerMapping objUserCustMapping = new UserCustomerMapping()
                                {
                                    UserID = userID,
                                    CustomerID = Convert.ToInt32(eachCustomer.Value),
                                    IsActive = true
                                };

                                lstUserCustmerMapping.Add(objUserCustMapping);
                            }
                        }

                        if (lstUserCustmerMapping.Count > 0)
                        {
                            saveSuccess = UserCustomerMappingManagement.CreateUpdate_UserCustomerMapping_Multiple(lstUserCustmerMapping);

                            if (saveSuccess)
                            {
                                BindUserCustomerMapping();

                                ddlUsers.ClearSelection();
                                lblCustomerPopUP.ClearSelection();
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Something went wrong, please try again";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Select Customer/User to Assign";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Select User to Assign";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select Customer to Assign";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = -1;
                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                    {
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                    }
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divAssignUserCustomerDialog\").dialog('open');", true);

               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUserCustomerMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdUserCustomerMapping.PageIndex = e.NewPageIndex;
            BindUserCustomerMapping();
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdUserCustomerMapping_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                int selectedUserID = string.IsNullOrEmpty(ddlFilterUsers.SelectedValue) ? -1 : Convert.ToInt32(ddlFilterUsers.SelectedValue);
                int selectedMgrID = string.IsNullOrEmpty(ddlMgrPage.SelectedValue) ? -1 : Convert.ToInt32(ddlMgrPage.SelectedValue);

                int selectedCustID = -1;

                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    selectedCustID = string.IsNullOrEmpty(ddlCustomer.SelectedValue) ? -1 : Convert.ToInt32(ddlCustomer.SelectedValue);
                }

                var lstUserCustomerMapping = UserCustomerMappingManagement.GetUserCustomerMappingList(selectedUserID, selectedCustID, serviceProviderID, distributorID, selectedMgrID);

                if (lstUserCustomerMapping.Count > 0)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        lstUserCustomerMapping = lstUserCustomerMapping.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Descending;
                    }
                    else
                    {
                        lstUserCustomerMapping = lstUserCustomerMapping.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                        direction = SortDirection.Ascending;
                    }

                    foreach (DataControlField field in grdUserCustomerMapping.Columns)
                    {
                        if (field.SortExpression == e.SortExpression)
                        {
                            ViewState["SortIndex"] = grdUserCustomerMapping.Columns.IndexOf(field);
                        }
                    }

                    grdUserCustomerMapping.DataSource = lstUserCustomerMapping;
                    grdUserCustomerMapping.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void grdUserCustomerMapping_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }
        
        protected void grdUserCustomerMapping_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null)
                {
                    long recordID = Convert.ToInt64(e.CommandArgument);
                    
                    if (e.CommandName.Equals("DELETE_USER"))
                    {
                        if (UserCustomerMappingManagement.Delete_UserCustomerMapping(recordID))
                        {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User Customer Mapping Deleted Successfully.')", true);

                        }
                        else
	                    {
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User Customer Mapping Not Deleted.')", true);
                        }
                        
                        BindUserCustomerMapping();
                    }
                    else if (e.CommandName.Equals("EDIT_USER"))
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "OpenPopupUCM(" + recordID + ");", true);
                    }
                }               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
    }
}