﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Data;
using System.ComponentModel;
using System.Collections;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using System.Web;
using System.Web.Security;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup
{
    public partial class RLCS_UserMaster : System.Web.UI.Page
    {
        public int customerID = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                if (!IsPostBack)
                {
                    if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "EXCT")
                    {
                        //if (HttpContext.Current.Request.IsAuthenticated && AuthenticationHelper.Role != "MGMT")
                        if (HttpContext.Current.Request.IsAuthenticated)
                        {
                            //if (HttpContext.Current.Request.IsAuthenticated)
                            //{
                            //    if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                            //    {

                            if (AuthenticationHelper.Role != "SADMN" && AuthenticationHelper.Role != "IMPT")
                                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                            ViewState["SortOrder"] = "Asc";
                            ViewState["SortExpression"] = "Name";
                            BindCustomers();
                            //BindRoles();
                            BindUsers();
                            Session["CurrentRole"] = AuthenticationHelper.Role;
                            Session["CurrentUserId"] = AuthenticationHelper.UserID;
                            if (AuthenticationHelper.Role == "CADMN")
                            {
                                divCustomerfilter.Visible = false;
                            }
                            else
                            {
                                divCustomerfilter.Visible = true;
                            }

                            if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                            {
                                btnAddUser.Visible = false;
                                btnUserCustomerMapping.Visible = true;
                            }
                            else
                                btnUserCustomerMapping.Visible = false;

                            //if (AuthenticationHelper.Role == "SPADM")
                            //    btnUserCustomerMapping.Visible = true;
                            //else
                            //    btnUserCustomerMapping.Visible = false;                       
                        }
                        else
                        {
                            //added by rahul on 12 June 2018 Url Sequrity
                            FormsAuthentication.SignOut();
                            Session.Abandon();
                            FormsAuthentication.RedirectToLoginPage();
                        }
                    }
                    else
                    {
                        //added by rahul on 12 June 2018 Url Sequrity
                        FormsAuthentication.SignOut();
                        Session.Abandon();
                        FormsAuthentication.RedirectToLoginPage();
                    }
                }
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
            udcInputForm.OnSaved += (inputForm, args) => { BindUsers(); };
        }

        #region user Detail

        public void BindUsers()
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }

                //var uselist = UserManagement.GetAllUser(customerID, tbxFilter.Text);
                var userlist = CustomerManagement.GetAllUser(customerID, serviceProviderID, distributorID, 2, tbxFilter.Text);

                if (userlist != null)
                {
                    if (userlist.Count > 0)
                    {
                        List<string> lstRoles = new List<string>();
                        lstRoles.Add("Vendor Auditor");
                        lstRoles.Add("VendorAuditAdmin");
                        lstRoles.Add("HR_VendorAuditor");
                        lstRoles.Add("HR_Vendor");

                        userlist = userlist.Where(row => !lstRoles.Contains(row.HR_Role)).ToList();
                    }   
                }

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "FirstName")
                    {
                        userlist = userlist.OrderBy(entry => entry.FirstName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "LastName")
                    {
                        userlist = userlist.OrderBy(entry => entry.LastName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Email")
                    {
                        userlist = userlist.OrderBy(entry => entry.Email).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ContactNumber")
                    {
                        userlist = userlist.OrderBy(entry => entry.ContactNumber).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "HR_Role")
                    {
                        userlist = userlist.OrderBy(entry => entry.HR_Role).ToList();
                    }

                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "FirstName")
                    {
                        userlist = userlist.OrderByDescending(entry => entry.FirstName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "LastName")
                    {
                        userlist = userlist.OrderByDescending(entry => entry.LastName).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "Email")
                    {
                        userlist = userlist.OrderByDescending(entry => entry.Email).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ContactNumber")
                    {
                        userlist = userlist.OrderByDescending(entry => entry.ContactNumber).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "HR_Role")
                    {
                        userlist = userlist.OrderByDescending(entry => entry.HR_Role).ToList();
                    }
                    direction = SortDirection.Ascending;
                }

                grdUser.DataSource = userlist;
                grdUser.DataBind();

                upUserList.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        protected void grdUser_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument != null && !string.IsNullOrEmpty(e.CommandName))
                {
                    int userID = Convert.ToInt32(e.CommandArgument);
                    if (e.CommandName.Equals("EDIT_USER"))
                    {
                        udcInputForm.EditUserInformation(userID);
                    }
                    else if (e.CommandName.Equals("DELETE_USER"))
                    {
                        if (UserManagement.HasCompliancesAssigned(userID))
                        {
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deleted. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                        }
                        else if (EventManagement.GetAllAssignedInstancesByUser(userID).Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "EventInform", "alert('Account can not be deleted. One or more Event are assigned to user, please re-assign to other user.');", true);
                        }
                        else
                        {
                            UserManagement.Delete(userID);
                            UserManagementRisk.Delete(userID);
                        }

                        BindUsers();
                    }
                    else if (e.CommandName.Equals("CHANGE_STATUS"))
                    {
                        if (UserManagement.IsActive(userID) && UserManagement.HasCompliancesAssigned(userID))
                        {
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Account can not be deactivated. One or more Compliances are assigned to user, please re-assign to other user.');", true);
                        }
                        else
                        {
                            UserManagement.ToggleStatus(userID);
                            UserManagementRisk.ToggleStatus(userID);

                        }
                        BindUsers();
                    }
                    else if (e.CommandName.Equals("RESET_PASSWORD"))
                    {
                        User user = UserManagement.GetByID(userID);
                        mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                        string passwordText = Util.CreateRandomPassword(10);
                        user.Password = Util.CalculateAESHash(passwordText);
                        mstuser.Password = Util.CalculateAESHash(passwordText);

                        // added by sudarshan for comman notification
                        int customerID = -1;
                        string ReplyEmailAddressName = "";
                        if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                        {
                            ReplyEmailAddressName = "Avantis";
                        }
                        else
                        {
                            customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                            ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                        }

                        string message = EmailNotations.SendPasswordResetNotificationEmail(user, passwordText, Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]), ReplyEmailAddressName);

                        bool result = UserManagement.ChangePassword(user);
                        bool result1 = UserManagementRisk.ChangePassword(mstuser);
                        if (result && result1)
                        {
                            EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<string>(new string[] { user.Email }), null, null, "AVACOM account password changed", message);
                            ScriptManager.RegisterStartupScript(this, Page.GetType(), "InformUser", "alert('Password reset successfully.');", true);
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
                        }
                    }
                    else if (e.CommandName.Equals("UNLOCK_USER"))
                    {
                        UserManagement.WrongAttemptCountUpdate(userID);
                        UserManagementRisk.WrongAttemptCountUpdate(userID);
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "Unloack", "alert('User unlocked successfully.');", true);
                        BindUsers();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindNewUserList(int customerID, DropDownList ddllist)
        {
            try
            {
                ddllist.DataTextField = "Name";
                ddllist.DataValueField = "ID";

                ddllist.DataSource = UserManagement.GetAllByCustomerID(customerID, null);
                ddllist.DataBind();

                ddllist.Items.Insert(0, new ListItem("< Select user >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUser.PageIndex = e.NewPageIndex;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        protected void grdUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                if (ddlCustomerList.SelectedValue != "-1")
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }

                //var users = UserManagement.GetAllUser(customerID, tbxFilter.Text);
                var users = CustomerManagement.GetAllUser(customerID, serviceProviderID, distributorID, 2, tbxFilter.Text);
                
                //List<object> dataSource = new List<object>();
                //foreach (User user in users)
                //{
                //    dataSource.Add(new
                //    {
                //        user.ID,
                //        user.FirstName,
                //        user.LastName,
                //        user.Email,
                //        user.ContactNumber,
                //        Role = RoleManagement.GetByID(user.RoleID).Name,
                //        user.IsActive

                //    });
                //}

                if (direction == SortDirection.Ascending)
                {
                    users = users.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                }
                else
                {
                    users = users.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdUser.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdUser.Columns.IndexOf(field);
                    }
                }

                grdUser.DataSource = users;
                grdUser.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int id = Convert.ToInt32(((GridView)sender).DataKeys[e.Row.RowIndex].Value);

                    LinkButton lbtnDelete = (LinkButton)e.Row.FindControl("lbtnDelete");
                    LinkButton lbtnModifyAssignment = (LinkButton)e.Row.FindControl("lbtnModifyAssignment");
                    LinkButton lbtnChangeStatus = (LinkButton)e.Row.FindControl("lbtnChangeStatus");
                    LinkButton lbtnEventAssignment = (LinkButton)e.Row.FindControl("lbtnEventAssignment");

                    if (UserManagement.GetByID(id).RoleID == 1)
                    {
                        lbtnDelete.Visible = false;
                        lbtnModifyAssignment.Visible = false;
                        lbtnChangeStatus.Enabled = false;
                        lbtnChangeStatus.Attributes.Add("href", "#");
                        lbtnChangeStatus.OnClientClick = string.Empty;
                    }

                    if (id == AuthenticationHelper.UserID)
                    {
                        lbtnChangeStatus.Enabled = false;
                        lbtnChangeStatus.Attributes.Add("href", "#");
                        lbtnChangeStatus.OnClientClick = string.Empty;
                    }

                    //if (Business.ComplianceManagement.GetAllAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count <= 0)
                    //{
                    //    lbtnModifyAssignment.Visible = false;
                    //}

                    //if (Business.InternalComplianceManagement.GetAllAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count <= 0)
                    //{
                    //    lbtnModifyAssignment.Visible = false;
                    //}

                    int a = 0;
                    int b = 0;
                    int c = 0;
                    int d = 0;

                    a = Business.ComplianceManagement.GetAllAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count;
                    b = Business.InternalComplianceManagement.GetAllAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count;
                    c = Business.ComplianceManagement.GetAllAssignedInstancesCheckList(userID: id).OrderBy(entry => entry.Branch).ToList().Count;
                    d = Business.TaskManagment.GetAllTaskAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count;


                    if (a <= 0 && b <= 0 && c <= 0 && d <= 0)
                    {
                        lbtnModifyAssignment.Visible = false;
                    }
                    //if ((Business.ComplianceManagement.GetAllAssignedInstances( userID: id).OrderBy(entry => entry.Branch).ToList().Count <= 0)
                    //        || (Business.InternalComplianceManagement.GetAllAssignedInstances(userID: id).OrderBy(entry => entry.Branch).ToList().Count <= 0)
                    //        || (Business.ComplianceManagement.GetAllAssignedInstancesCheckList(userID: id).OrderBy(entry => entry.Branch).ToList().Count <= 0))
                    //    {

                    //        lbtnModifyAssignment.Visible = false;
                    //    }

                    if (EventManagement.GetAllAssignedInstancesByUser(id).Count <= 0)
                    {
                        lbtnEventAssignment.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grdUser_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (direction == SortDirection.Ascending)
            {
                sortImage.ImageUrl = "../../Images/SortAsc.gif";
                sortImage.AlternateText = "Ascending Order";
            }
            else
            {
                sortImage.ImageUrl = "../../Images/SortDesc.gif";
                sortImage.AlternateText = "Descending Order";
            }
            headerRow.Cells[columnIndex].Controls.Add(sortImage);
        }

        protected bool IsLocked(string emailID)
        {
            try
            {
                if (UserManagement.WrongAttemptCount(emailID.Trim()) >= 3)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
            return false;
        }

        #endregion
        
        protected void btnAddUser_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
            {
                int customerid = -1;
                if (ddlCustomerList.SelectedValue != "-1")
                {
                    udcInputForm.AddNewUser(Convert.ToInt32(ddlCustomerList.SelectedValue));
                }
                else
                {
                    udcInputForm.AddNewUser(customerid);
                }
            }
        }

        protected void tbxFilter_TextChanged(object sender, EventArgs e)
        {
            try
            {
                grdUser.PageIndex = 0;
                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        protected void ddlCustomerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "SPADM")
                {
                    if (ddlCustomerList.SelectedValue == "-1")
                    {
                        btnAddUser.Visible = false;
                    }
                    else
                    {
                        btnAddUser.Visible = true;
                    }
                }

                BindUsers();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                //ddlCustomerList.DataSource = CustomerManagement.GetAll_HRComplianceCustomers_IncludesServiceProvider(customerID, serviceProviderID, 2);
                ddlCustomerList.DataSource = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, true);
                ddlCustomerList.DataBind();

                ddlCustomerList.Items.Insert(0, new ListItem("Select", "-1"));

                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerList.Items.FindByValue(customerID.ToString()) != null)
                        ddlCustomerList.Items.FindByValue(customerID.ToString()).Selected = true;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        
        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindUsers();
            //BindCustomers();
           // ddlSPList_SelectedIndexChanged(sender, e);
        }



















    }
}