﻿<%@ Page Language="C#" MasterPageFile="~/Compliance.Master" AutoEventWireup="true" CodeBehind="RLCS_PaycodeMappingDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.ClientSetup.RLCS_PaycodeMappingDetails" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../../Newjs/kendo.all.min.js"></script>

    <link href="/NewCSS/arrow-Progressbar.css" rel="stylesheet" />

    <style type="text/css">
        *.disabled {
            cursor: not-allowed;
        }

        .aspNetDisabled {
            cursor: not-allowed;
        }

        .m-10 {
            margin-left: 10px;
        }

        .m-20 {
            margin-left: 20px;
        }
    
        .alert-danger {	
        color: #5c9566;	
        background-color: #dff8e3;	
        border-color: #d6e9c6;	
        }	
    .alert {	
        /*padding: 15px;	
        margin-bottom: 20px;*/	
        border: 1px solid transparent;	
        border-radius: 4px;	
    }
    </style>

    <script type="text/javascript">
      
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <asp:LinkButton Text="Refresh" runat="server" ID="lnkbtnRefresh" OnClick="btnRefresh_Click" CssClass="k-hidden" />
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/processing.gif"
                    AlternateText="Processing..." ToolTip="Processing ..." Style="padding: 10px; position: fixed; top: 25%; left: 40%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="container">

        <div style="margin-left: 1%;">
            <div class="arrow-steps clearfix">
                <div class="step done">
                    <asp:LinkButton ID="lnkBtnCustomer" PostBackUrl="~/RLCS/ClientSetup/RLCS_CustomerCorporateList.aspx" runat="server" Text="Customer"></asp:LinkButton>
                </div>
                <div class="step current">
                    <asp:LinkButton ID="lnkBtnEntityCustomerBranch" runat="server" Text="Entity-Branch"></asp:LinkButton>
                    <%--PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityBranchList.aspx"--%>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnUser" PostBackUrl="~/RLCS/ClientSetup/RLCS_UserMaster.aspx" runat="server" Text="User"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEmployee" PostBackUrl="~/RLCS/ClientSetup/RLCS_EmployeeMaster.aspx" runat="server" Text="Employee"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_EntityAssignment.aspx" runat="server" Text="Entity-Branch Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAssign" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Assign.aspx" runat="server" Text="Compliance Assignment"></asp:LinkButton>
                </div>
                <div class="step">
                    <asp:LinkButton ID="lnkBtnComAct" PostBackUrl="~/RLCS/ClientSetup/RLCS_HRCompliance_Activate.aspx" runat="server" Text="Compliance Activation"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="upEmpMasterCADMN" runat="server" UpdateMode="Conditional" OnLoad="upEmpMasterCADMN_Load">
        <ContentTemplate>
            <asp:HiddenField ID="hdnClientID" runat="server" Value="" />
            <div style="width: 90%; margin-bottom: 4px;margin-left:20px;">
                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                    ValidationGroup="ComplianceInstanceValidationGroup" Display="None" />
                    <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="DocGenValidationGroup" />
                                       
                                        <asp:CustomValidator ID="cvDuplicate" runat="server" EnableClientScript="False"
                                            ValidationGroup="DocGenValidationGroup" Display="None" />
            </div>
            <div style="width: 100%">
                <table width="100%">
                    <tr>
                        <td style="width: 30%;">
                            <div id="divCustomerfilter" runat="server">
                                <div style="width: 25%; float: left;">
                                    <label style="display: block; float: right; font-size: 13px; color: #333; font-weight: bold; margin-top: 5px;">
                                        Entity/Client :</label>
                                </div>
                                <div style="width: 75%; float: left;">
                                    <asp:DropDownListChosen runat="server" ID="ddlClientList" Style="padding: 0px; margin: 0px; height: 22px; width: 390px;"
                                        CssClass="txtbox" AutoPostBack="true" OnSelectedIndexChanged="ddlClientList_SelectedIndexChanged" NoResultsText="No results match." Width="350px"
                                         AllowSingleDeselect="true" />
                                </div>
                            </div>
                        </td>
                        <td style="width: 30%;">
                            <div style="width: 30%; float: left;">
                                <label style="display: block; float: right; font-size: 13px; color: #333; font-weight: bold; margin-top: 5px;">Filter :</label>
                            </div>
                            <div style="width: 70%; float: left;">
                                <asp:TextBox runat="server" ID="tbxFilter" Width="100%" MaxLength="50" AutoPostBack="true"
                                    OnTextChanged="tbxFilter_TextChanged" />
                            </div>
                        </td>
                        <td style="width: 20%; padding-left: 30px;">
                            <asp:LinkButton Text="Add New" runat="server" ID="btnAddEmployee" CssClass="k-button" OnClientClick="return OpenAddEmployeePopup()" />
                            <%-- OnClick="btnAddUser_Click"--%>
                            <asp:LinkButton Text="Upload" runat="server" ID="btnUploadEmployee" OnClientClick="return OpenUploadWindow()" CssClass="k-button" />
                            <%--                            <asp:LinkButton Text="Refresh" runat="server" ID="lnkBtnRefresh" CssClass="button m-20" OnClick="btnRefreshEmployee_Click" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><b style="margin-left: 32px;">Customer: </b>
                            <asp:DataList runat="server" ID="dlBreadcrumb" OnItemCommand="dlBreadcrumb_ItemCommand"
                                RepeatLayout="Flow" RepeatDirection="Horizontal">
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Eval("Name") %>' CommandArgument='<%# Eval("ID") %>' CommandName="ITEM_CLICKED"
                                        runat="server" Style="text-decoration: none; color: Black" />
                                </ItemTemplate>
                                <ItemStyle Font-Size="12" ForeColor="Black" Font-Underline="true" />
                                <SeparatorStyle Font-Size="12" />
                                <SeparatorTemplate>
                                    &gt;
                                </SeparatorTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>

                <asp:Panel ID="Panel2" Width="100%" Height="450px" ScrollBars="Vertical" runat="server">
                    <asp:GridView runat="server" ID="grdPaycodeMappingDetails" AutoGenerateColumns="false" GridLines="Vertical" BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="1px" AllowSorting="true"
                        CellPadding="4" ForeColor="Black" AllowPaging="True" PageSize="12" Width="100%" Font-Size="12px" OnSorting="grdPaycodeMappingDetails_Sorting"
                        OnPageIndexChanging="grdPaycodeMappingDetails_PageIndexChanging" OnRowEditing="grdPaycodeMappingDetails_RowEditing" OnRowCommand="grdPaycodeMappingDetails_RowCommand" DataKeyNames="CPMD_ClientID">
                        <Columns>
                            <asp:TemplateField HeaderText="Sr" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %>
                                    <asp:HiddenField ID="CPMD_ClientID" runat="server" Value='<%# Eval("CPMD_ClientID") %>' />
                                    <asp:HiddenField ID="Header" runat="server" Value='<%# Eval("CPMD_Header") %>' />
                                    <asp:HiddenField ID="hdnStatus" runat="server" Value='<%# Eval("CPMD_Status") %>' />
                                    <asp:HiddenField ID="hdnPayCode" runat="server" Value='<%# Eval("CPMD_PayCode") %>' />
                                    <asp:HiddenField ID="hdnStandard_Column" runat="server" Value='<%# Eval("CPMD_Standard_Column") %>' />
                                    <asp:HiddenField ID="hdnPayGroup" runat="server" Value='<%# Eval("CPMD_PayGroup") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="CPMD_Header" HeaderText="Header Name" SortExpression="CPMD_Header" />
                             <asp:BoundField DataField="CPMD_PayGroup" HeaderText="Paycode Group" SortExpression="CPMD_PayGroup" />
                            <asp:BoundField DataField="CPMD_PayCode" HeaderText="Paycode" SortExpression="CPMD_PayCode" />
                            <asp:BoundField DataField="CPMD_Deduction_Type" HeaderText="PaycodeType" SortExpression="CPMD_Deduction_Type" />
                            <asp:BoundField DataField="CPMD_Sequence_Order" HeaderText="Sequence Order" SortExpression="CPMD_Sequence_Order" />
                            <asp:BoundField DataField="CPMD_appl_ESI" HeaderText="ESI Applicable" SortExpression="CPMD_appl_ESI"/>
                            <asp:BoundField DataField="CPMD_Appl_PT" HeaderText="PT Applicable" SortExpression="CPMD_Appl_PT" />
                            <asp:BoundField DataField="CPMD_Appl_LWF" HeaderText="LWF Applicable" SortExpression="CPMD_Appl_LWF"/>
                            <asp:BoundField DataField="CPMD_appl_PF" HeaderText="PF Applicable" SortExpression="CPMD_appl_PF" />
                            <asp:BoundField DataField="PS_Status" HeaderText="Status" SortExpression="PS_Status" />
                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <asp:LinkButton ID="lnkBtnEdit" runat="server" data-clId='<%# Eval("CPMD_ClientID")%>' data-parentId='<%# Eval("ID")%>' OnClientClick="return EditEmployeePopup(this);">
                                        <img src="../../Images/edit_icon.png" alt="Edit" title="Edit Details" />
                                      </asp:LinkButton>

                                    <asp:LinkButton ID="btnActive" runat="server" CommandName="Delete_Employee" CommandArgument='<%# Eval("ID") %>' Visible='<%# Eval("CPMD_Status").ToString() == "I" ? true : false %>'
                                        OnClientClick="return confirm('Are you certain you want to Activate this Paycode?');"><img src="../../Images/delete_icon.png" alt="Delete" title="Delete Paycode" /></asp:LinkButton>

                                    
                                    <asp:LinkButton ID="lnkDeleteEmployee" runat="server" CommandName="Delete_Employee" CommandArgument='<%# Eval("ID") %>' Visible='<%# Eval("CPMD_Status").ToString() == "A" ? true : false %>'
                                        OnClientClick="return confirm('Are you certain you want to delete this Paycode?');"><img src="../../Images/delete_icon.png" alt="Delete" title="Delete Paycode" /></asp:LinkButton>

                                </ItemTemplate>
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#CCCC99" />
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
                        <PagerSettings Position="Top" />
                        <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
                        <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
                        <AlternatingRowStyle BackColor="#E6EFF7" />
                        <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
                        <EmptyDataTemplate>
                            No Records Found.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        $(function () {
            $('#divRLCSEmployeeUploadDialog').hide();
            $('#divRLCSEmployeeAddDialog').hide();
            $('#divCustomerBranchesDialog').dialog({
                height: 650,
                width: 720,
                autoOpen: false,
                draggable: true,
                title: "Branch Details",
                open: function (type, data) {
                    $(this).parent().appendTo("form");
                }
            });
        });



       <%-- function OpenUploadWindow() {
            debugger; cust=<%=CustID%>
            $('#divRLCSEmployeeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeUpload').attr('src', "/Setup/UploadEmployeeFiles?CustID="+ cust);
            var myWindowAdv = $("#divRLCSEmployeeUploadDialog");

            function onClose() {
            }

            myWindowAdv.kendoWindow({
                width: "50%",
                height: "50%",
                title: "Upload Employee Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;
        }--%>
        function onClose() {
            debugger;
            document.getElementById('<%= lnkbtnRefresh.ClientID %>').click();
          
        }
        function OpenAddEmployeePopup() {
            debugger;
            var Client_ = $("#BodyContent_hdnClientID").val();
            var Client = '<%= ClientID%>';

            if (Client_ != undefined || Client_ != null) {
                $('#divRLCSEmployeeAddDialog').show();
                var myWindowAdv = $("#divRLCSEmployeeAddDialog");

                myWindowAdv.kendoWindow({
                    width: "65%",
                    height: '89%',
                    title: "Paycode Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/Setup/CreateClientPaycodeMappingDetails?ClientID=" + Client_);
            }
            return false;
        }

        function EditEmployeePopup(obj) {
            debugger;
            var clientid = $(obj).attr('data-clId');
            var id = $(obj).attr('data-parentId');

            //alert(clientid);
            //alert(id);

             var Client_ = $("#BodyContent_hdnClientID").val();
             var Client = '<%= ClientID%>';

            if (Client_ != undefined || Client_ != null) {
                $('#divRLCSEmployeeAddDialog').show();
                var myWindowAdv = $("#divRLCSEmployeeAddDialog");

                myWindowAdv.kendoWindow({
                    width: "65%",
                    height: '89%',
                    title: "Paycode Details",
                    visible: false,
                    actions: [
                        //"Pin",
                        "Close"
                    ],
                    close: onClose
                });

                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeAdd').attr('src', "/Setup/CreateClientPaycodeMappingDetails?ClientID=" + clientid +"&ID="+id+"&Edit=YES");
            }
            return false;
        }



        function OpenUploadWindow() {
            debugger;
            var Client_ = $("#BodyContent_hdnClientID").val();
            Client = '<%=ClientID%>';
            $('#divRLCSEmployeeUploadDialog').show();
            kendo.ui.progress($(".chart-loading"), true);
            $('#iframeUpload').attr('src', "/Setup/UploadPaycodeFile?Client=" + Client_);
            var myWindowAdv = $("#divRLCSEmployeeUploadDialog");

            myWindowAdv.kendoWindow({
                width: "50%",
                height: "50%",
                title: "Upload Paycode Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose
            });

            myWindowAdv.data("kendoWindow").center().open();
            $('#loader').hide();

            return false;
        }


    </script>

    <div id="divRLCSEmployeeUploadDialog">
        <iframe id="iframeUpload" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
    <div id="divRLCSEmployeeAddDialog">
        <iframe id="iframeAdd" style="width: 96%; height: 100%; border: none"></iframe>
    </div>
</asp:Content>
