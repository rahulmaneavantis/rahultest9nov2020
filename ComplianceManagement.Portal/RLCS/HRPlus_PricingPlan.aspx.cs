﻿using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_PricingPlan : System.Web.UI.Page
    {
        protected static string AVACOM_RLCS_API_URL;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                AVACOM_RLCS_API_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                if (!IsPostBack)
                {
                    txtEffectiveDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");
                    BindGrid(DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindGrid(DateTime? checkOnDate)
        {
            try
            {
                DataTable result = new DataTable();
                using (ComplianceDBEntities context = new ComplianceDBEntities())
                {
                    using (SqlConnection sqlcon = new SqlConnection(context.Database.Connection.ConnectionString))
                    {
                        string spexec = String.Format(@"USP_RLCS_PricingPlan");
                        using (SqlCommand cmd = new SqlCommand(spexec, sqlcon))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            var idParam = new SqlParameter
                            {
                                ParameterName = "CheckDate",
                                Value = checkOnDate
                            };

                            cmd.Parameters.Add(idParam);

                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                da.Fill(result);
                            }
                        }
                    }
                }

                if (result.Rows.Count > 0)
                {
                    gridCompliances.DataSource = result;
                    gridCompliances.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void txtEffectiveDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtEffectiveDate.Text))
                {
                    DateTime? filterDate = null;
                    try
                    {
                        filterDate = Convert.ToDateTime(txtEffectiveDate.Text);
                    }
                    catch(Exception ex)
                    {
                        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    }

                    if (filterDate != null)
                        BindGrid(filterDate);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }
    }
}