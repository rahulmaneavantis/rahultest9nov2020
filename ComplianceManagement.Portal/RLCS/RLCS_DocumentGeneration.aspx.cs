﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_DocumentGeneration : System.Web.UI.Page
    {
        public static List<int> branchList = new List<int>();
       public int customerID, custBranchID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                                lnkTabRegister_Click(sender, e);

                                BindLocationFilter(assignedbranchIDs);
                                //BindRegisterDropDown();

                                BindEntityClientFilter(assignedbranchIDs);

                                ReturnBranch.Visible = false;
                                ReturnEntity.Visible = true;
                               DivCode.Visible = true;
                            }
                        }
                    }
                }

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "initializeJQueryUIDeptDDL", "initializeJQueryUIDeptDDL();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkTabRegister_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 0;

            liRegister.Attributes.Add("class", "active");
            liChallan.Attributes.Add("class", "");
            liReturn.Attributes.Add("class", "");
        }

        protected void lnkTabChallan_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 1;

            liRegister.Attributes.Add("class", "");
            liChallan.Attributes.Add("class", "active");
            liReturn.Attributes.Add("class", "");
        }

        protected void lnkTabReturn_Click(object sender, EventArgs e)
        {
            MainView.ActiveViewIndex = 2;

            liRegister.Attributes.Add("class", "");
            liChallan.Attributes.Add("class", "");
            liReturn.Attributes.Add("class", "active");
        }

        protected void ddlRegType_SelectedIndexChanged(object sender, EventArgs e)
        {
            tvFilterLocation_SelectedNodeChanged(sender, e);
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

            BindRegisterDropDown();
            BindMonthYearDropDown();

        }

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    //var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    var branches = RLCS_ClientsManagement.GetAllHierarchy(customerID);

                    tvFilterLocation.Nodes.Clear();

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in branches)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);

                    BindLocationFilter_Return(assignedBranchList, branches);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter_Return(List<int> assignedBranchList, List<NameValueHierarchy> branches)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);
                    //var branches = RLCS_ClientsManagement.GetAllHierarchy(customerID);

                    tvFilterLocation_Return.Nodes.Clear();

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation_Return.Nodes.Add(node);

                    foreach (var item in branches)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation_Return.Nodes.Add(node);
                    }

                    tvFilterLocation_Return.CollapseAll();
                    tvFilterLocation_Return_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindRegisterDropDown()
        {
            try
            {
                if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                    var clientLocationDetailRecord = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                    if (clientLocationDetailRecord != null)
                    {
                        var registersMasterRecords = RLCS_ComplianceManagement.GetAll_Registers_StateWise(AuthenticationHelper.UserID, clientLocationDetailRecord.CM_State, clientLocationDetailRecord.CM_EstablishmentType, ddlRegType.SelectedValue);
                        ddlRegisterName.Items.Clear();

                        ddlRegisterName.DataTextField = "Register_Name";
                        ddlRegisterName.DataValueField = "RegisterID";

                        ddlRegisterName.DataSource = registersMasterRecords;
                        ddlRegisterName.DataBind();

                        ddlRegisterName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlRegisterName.Items.Clear();

                        ddlRegisterName.DataSource = null;
                        ddlRegisterName.DataBind();

                        ddlRegisterName.Items.Insert(0, new ListItem("All", "0"));

                        //cvDuplicateEntry.IsValid = false;
                        //cvDuplicateEntry.ErrorMessage = "Please Select Branch.";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select at least Branch.";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindMonthYearDropDown()
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int custBranchID = 0;

                if (MainView.ActiveViewIndex == 0)
                    custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                else if (MainView.ActiveViewIndex == 1)
                {
                    if (!string.IsNullOrEmpty(ddlEntityClient.SelectedValue))
                    {
                        var entityClient = ddlEntityClient.SelectedValue.Split('|');

                        if (entityClient.Length > 0)
                        {
                            string branchID = entityClient[0];
                            custBranchID = Convert.ToInt32(branchID);
                        }
                    }
                }

                branchList.Clear();
                GetAll_Branches(customerID, custBranchID);
                branchList.ToList();

                if (branchList.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        List<long> lstComplianceIDs = new List<long>();

                        if (MainView.ActiveViewIndex == 0)
                        {
                            lstComplianceIDs = (from row in entities.RLCS_Register_Compliance_Mapping
                                                where row.AVACOM_ComplianceID != null
                                                && row.RegisterID != null
                                                && row.Register_Status == "A"
                                                select (long)row.AVACOM_ComplianceID).ToList();
                        }
                        else if (MainView.ActiveViewIndex == 1)
                        {
                            var ChallanCompliances = (from row in entities.RLCS_Challan_Compliance_Mapping
                                                      where row.AVACOM_ComplianceID != null
                                                      && row.IsActive == true
                                                      select row).ToList();

                            if (ChallanCompliances.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(ddlChallanType.SelectedValue))
                                {
                                    string challanType = string.Empty;

                                    if (!ddlChallanType.SelectedValue.Trim().Equals("PF_Working_New"))
                                        challanType = "EPF";
                                    else
                                        challanType = ddlChallanType.SelectedValue.Trim();

                                    lstComplianceIDs = ChallanCompliances.Where(row => row.ChallanType.Equals(challanType))
                                        .Select(row => (long)row.AVACOM_ComplianceID).ToList();
                                }
                                else
                                {
                                    lstComplianceIDs = ChallanCompliances.Select(row => (long)row.AVACOM_ComplianceID).ToList();
                                }
                            }
                        }

                        var lstInstances = (from row in entities.ComplianceInstances
                                            where branchList.Contains(row.CustomerBranchID)
                                            && lstComplianceIDs.Contains(row.ComplianceId)
                                            && row.IsDeleted == false
                                            select row.ID).ToList();

                        if (lstInstances.Count > 0)
                        {
                            var lstSchedules = (from row in entities.ComplianceScheduleOns
                                                where lstInstances.Contains((long)row.ComplianceInstanceID)
                                                && row.ComplianceInstanceID != null
                                                && row.IsActive == true
                                                && row.RLCS_PayrollMonth != null
                                                && row.RLCS_PayrollYear != null
                                                select new
                                                {
                                                    row.RLCS_PayrollMonth,
                                                    //MonthName1 = new DateTime(1900, Convert.ToInt32(row.RLCS_PayrollMonth), 01).ToString("MMMM"),
                                                    //MonthName2 = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(row.RLCS_PayrollMonth)),
                                                    row.RLCS_PayrollYear,
                                                }).Distinct().ToList();

                            if (lstSchedules.Count > 0)
                            {
                                var lstMonths = (from row in lstSchedules.AsEnumerable()
                                                 select new
                                                 {
                                                     row.RLCS_PayrollMonth,
                                                     //MonthName1 = new DateTime(1900, Convert.ToInt32(row.RLCS_PayrollMonth), 01).ToString("MMMM"),
                                                     MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(row.RLCS_PayrollMonth)),
                                                     //row.RLCS_PayrollYear,
                                                 }).Distinct().ToList();

                                if (MainView.ActiveViewIndex == 0)
                                {
                                    ddlMonthRegister.DataTextField = "MonthName";
                                    ddlMonthRegister.DataValueField = "RLCS_PayrollMonth";

                                    ddlMonthRegister.DataSource = lstMonths;
                                    ddlMonthRegister.DataBind();

                                    ddlYearRegister.DataSource = lstSchedules.Select(row => row.RLCS_PayrollYear).OrderByDescending(row => row).Distinct().ToList();
                                    ddlYearRegister.DataBind();
                                }
                                else if (MainView.ActiveViewIndex == 1)
                                {
                                    ddlMonthChallan.DataTextField = "MonthName";
                                    ddlMonthChallan.DataValueField = "RLCS_PayrollMonth";

                                    ddlMonthChallan.DataSource = lstMonths;
                                    ddlMonthChallan.DataBind();

                                    ddlYearChallan.DataSource = lstSchedules.Select(row => row.RLCS_PayrollYear).OrderByDescending(row => row).Distinct().ToList();
                                    ddlYearChallan.DataBind();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        #region To Find Branches 

        public static List<NameValueHierarchy> GetAll_Branches(int customerID, int selectedEntityID)
        {
            List<NameValueHierarchy> hierarchy = null;

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false
                             && row.CustomerID == customerID
                             && row.ID == selectedEntityID
                             select row);

                hierarchy = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

                foreach (var item in hierarchy)
                {
                    branchList.Add(item.ID);
                    LoadChildBranches(customerID, item, true, entities);
                }
            }

            return hierarchy;
        }

        public static void LoadChildBranches(int customerid, NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {
            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false
                                                && row.CustomerID == customerid
                                                && row.ParentID == nvp.ID
                                                //&& row.Type != 1
                                                select row);

            var subEntities = query.ToList().Select(entry => new NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();

            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadChildBranches(customerid, item, false, entities);
            }
        }

        #endregion

        protected void btnGenerateRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(tvFilterLocation.SelectedValue))
                {
                    if (!String.IsNullOrEmpty(ddlMonthRegister.SelectedValue) && !String.IsNullOrEmpty(ddlYearRegister.SelectedValue))
                    {
                        bool saveSuccess = false;

                        string requestUrl = string.Empty;
                        string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];

                        requestUrl = rlcsAPIURL + "ReportingRegisters/DownloadRegisters";

                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        int custBranchID = Convert.ToInt32(tvFilterLocation.SelectedValue);

                        var clientLocationDetailRecord = RLCS_Master_Management.GetClientLocationDetails(custBranchID);

                        if (clientLocationDetailRecord != null)
                        {
                            string clientID = clientLocationDetailRecord.CM_ClientID;
                            string stateID = clientLocationDetailRecord.CM_State;
                            string locID = clientLocationDetailRecord.CM_City;
                            string branchID = clientLocationDetailRecord.CM_ClientName;

                            RLCS_Request_DownloadRegister APIRequest = new RLCS_Request_DownloadRegister()
                            {
                                CM_ClientID = clientID,
                                StateID = stateID,
                                LocationID = locID,
                                BranchID = branchID,
                                MonthID = ddlMonthRegister.SelectedValue,
                                YearID = ddlYearRegister.SelectedValue,
                                RegisterID = ddlRegisterName.SelectedItem.Text,
                                DownloadFormatType = "excel",
                                status = ddlRegType.SelectedValue,
                                IsAventisAPICall = 1,
                            };

                            long recordID = RLCS_DocumentManagement.RLCS_GenerateDocumentRequest_Register(customerID, custBranchID, "REG", AuthenticationHelper.UserID, APIRequest);

                            //string responseData = WebAPIUtility.POST_Corporate(requestUrl, corpRecord);
                            string responseData = WebAPIUtility.POST_RLCS_Common<RLCS_Request_DownloadRegister>(requestUrl, APIRequest);

                            if (!string.IsNullOrEmpty(responseData))
                            {
                                var response_DownloadRegister = JsonConvert.DeserializeObject<RLCS_Response_DownloadRegister>(responseData);

                                if (response_DownloadRegister != null)
                                {
                                    if (response_DownloadRegister.StatusCode != 0)
                                        saveSuccess = Convert.ToBoolean(response_DownloadRegister.StatusCode);

                                    if (saveSuccess)
                                    {
                                        int index = 0;
                                        var lstGeneratedRegisters = response_DownloadRegister.RegistersId;
                                        var lstDocumentPath = response_DownloadRegister.DocumentPath;

                                        if (lstGeneratedRegisters.Count > 0 && lstDocumentPath.Count > 0 && lstDocumentPath.Count == lstGeneratedRegisters.Count)
                                        {
                                            lstGeneratedRegisters.ForEach(eachRegisterID =>
                                            {
                                                if (!string.IsNullOrEmpty(eachRegisterID))
                                                {
                                                    string documentPath = lstDocumentPath[index];

                                                    int registerID = Convert.ToInt32(eachRegisterID);
                                                    var lstRegisterComplianceRecord = RLCS_ComplianceManagement.Get_RegistersComplianceRecordsByRegisterID(registerID);
                                                    //Download Generated Documents
                                                    if (lstRegisterComplianceRecord != null)
                                                        saveSuccess = RLCS_ComplianceManagement.Save_GeneratedRegisters(customerID, "SOW03", ddlMonthRegister.SelectedValue, ddlYearRegister.SelectedValue, clientID, stateID, locID, branchID, lstRegisterComplianceRecord, documentPath);


                                                    // saveSuccess = RLCS_ComplianceManagement.Save_GeneratedRegisters(customerID, "SOW03", ddlMonthRegister.SelectedValue, ddlYearRegister.SelectedValue, clientID, stateID, locID, branchID, registerID, documentPath);

                                                    index++;
                                                }
                                            });
                                        }
                                    }

                                    if (saveSuccess)
                                    {
                                        RLCS_DocumentManagement.UpdateStatus_RLCS_DocumentGeneration(recordID, AuthenticationHelper.UserID, saveSuccess);
                                        cvDuplicateEntrysucess.IsValid = false;
                                        cvDuplicateEntrysucess.ErrorMessage = "Requested Documents generated successfully, Please check Under My Documents";
                                        cvDuplicateEntrysucess.ValidationGroup = "success";
                                        cvDuplicateEntry.Visible = false;
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        if (response_DownloadRegister.ErrorMessage != "")
                                            cvDuplicateEntry.ErrorMessage = response_DownloadRegister.ErrorMessage;
                                        else
                                            cvDuplicateEntry.ErrorMessage = "There were No Registers Downloaded For the Selected Criteria";
                                        cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                                        cvDuplicateEntrysucess.Visible = false;
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again.";
                                    cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                                    cvDuplicateEntrysucess.Visible = false;
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "No Register(s) generated for Current Selection, Please check values or try again after some time";
                                cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                                cvDuplicateEntrysucess.Visible = false;
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Register(s) generated for Current Selection, Please check values or try again after some time";
                            cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                            cvDuplicateEntrysucess.Visible = false;
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Select Month and Year.";
                        cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                        cvDuplicateEntrysucess.Visible = false;
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select at least one Branch.";
                    cvDuplicateEntrysucess.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDocGenPopup_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter_Return", string.Format("initializeJQueryUI('{0}', 'divFilterLocation_Return');", tbxFilterLocation.ClientID), true);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindEntityClientFilter(List<int> assignedBranchList)
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                //ddlEntityClient.DataValueField = "AVACOM_BranchID";
                ddlEntityClient.DataValueField = "branchIDclientID";
                ddlEntityClient.DataTextField = "AVACOM_BranchName";
                //Return Entity
                ddlreturnEntity.DataValueField = "branchIDclientID";
                ddlreturnEntity.DataTextField = "AVACOM_BranchName";

                var lstEntityClients = RLCS_ClientsManagement.GetAll_EntityClient(customerID, assignedBranchList);

                if (lstEntityClients.Count > 0)
                {
                    var clients = (from row in lstEntityClients
                                   select new
                                   {
                                       branchIDclientID = row.AVACOM_BranchID + "|" + row.CM_ClientID,
                                       row.AVACOM_BranchName
                                   });

                    ddlEntityClient.DataSource = clients;
                    ddlEntityClient.DataBind();

                    ddlreturnEntity.DataSource = clients;
                    ddlreturnEntity.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlChallanType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindMonthYearDropDown();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<string> lstListItems = new List<string>();

                    if (!string.IsNullOrEmpty(ddlEntityClient.SelectedValue) && ddlEntityClient.SelectedValue != "-1")
                    {
                        var entityClient = ddlEntityClient.SelectedValue.Split('|');

                        if (entityClient.Length > 0)
                        {
                            string clientID = entityClient[1];

                            if (!string.IsNullOrEmpty(ddlChallanType.SelectedValue) && !string.IsNullOrEmpty(clientID))
                            {
                                if (ddlChallanType.SelectedValue.Trim().Equals("PF_Working_New"))
                                {
                                    lstListItems = (from RCB in entities.RLCS_Client_BasicDetails
                                                    join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                    on RCB.CB_ClientID equals RCCM.CM_ClientID
                                                    where RCB.CB_PF_Code != null
                                                    && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                    && RCB.IsProcessed == true
                                                    && RCCM.CM_Status == "A"
                                                    && RCCM.BranchType == "E"
                                                    select RCB.CB_PF_Code).Distinct().ToList();
                                }
                                else if (ddlChallanType.SelectedValue.Trim().ToUpper().Equals("ESI"))
                                {
                                    lstListItems = (from row in entities.RLCS_Employee_Master
                                                    where row.EM_Client_ESI_Number != null
                                                    && row.IsProcessed == true
                                                    && row.EM_Status == "A"
                                                     && row.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                    select row.EM_Client_ESI_Number).Distinct().ToList();
                                }
                                else if (ddlChallanType.SelectedValue.Trim().ToUpper().Equals("PT"))
                                {
                                    lstListItems = (from REM in entities.RLCS_Employee_Master
                                                    join RSM in entities.RLCS_State_Mapping
                                                    on REM.EM_Client_PT_State equals RSM.SM_Code
                                                    where REM.EM_Client_PT_State != null
                                                    && REM.IsProcessed == true
                                                    && REM.EM_Status == "A"
                                                    && RSM.PT_Applicability == "Y"
                                                    && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                    select REM.EM_Client_PT_State + "-" + RSM.SM_Name).Distinct().ToList();
                                }
                            }
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Entity/Client";
                    }

                    if (lstListItems.Count > 0)
                    {
                        ddlChallanCode.DataSource = lstListItems;
                        ddlChallanCode.DataBind();
                    }
                    else
                    {
                        ddlChallanCode.Items.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlReturnChallanType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string clientID = string.Empty;
                BindMonthYearDropDown();
                Bind_ReturnChallanList();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<string> lstListItems = new List<string>();

                    if (!string.IsNullOrEmpty(ddlReturnChallanType.SelectedValue))
                    {
                        // ddlFrequency.Items.Clear();
                        if (ddlReturnChallanType.SelectedValue.Trim().ToUpper() == "PT")
                        {
                            ReturnBranch.Visible = true;
                            ReturnEntity.Visible = false;
                            DivCode.Visible = true;
                        }

                        else if (ddlReturnChallanType.SelectedValue.Trim().ToUpper() == "OTHER")
                        {
                            ReturnBranch.Visible = true;
                            ReturnEntity.Visible = false;
                            DivCode.Visible = false;
                        }
                        else
                        {
                            ReturnBranch.Visible = false;
                            ReturnEntity.Visible = true;
                            DivCode.Visible = true;

                        }
                        if (!string.IsNullOrEmpty(ddlreturnEntity.SelectedValue) && ddlreturnEntity.SelectedValue != "-1")
                        {
                            var entityClient = ddlreturnEntity.SelectedValue.Split('|');

                            if (entityClient.Length > 0)
                            {
                                clientID = entityClient[1];

                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Select Entity/Client";
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tvFilterLocation_Return.SelectedValue))
                            {
                                clientID = RLCS_ClientsManagement.GetClientIDByName(tvFilterLocation_Return.SelectedNode.Text);
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Select Entity/Client";
                            }
                        }
                        if (!string.IsNullOrEmpty(ddlReturnChallanType.SelectedValue) && !string.IsNullOrEmpty(clientID))
                        {
                            if (ddlReturnChallanType.SelectedValue.Trim().Equals("EPF"))
                            {
                                ReturnBranch.Visible = false;
                                ReturnEntity.Visible = true;
                                DivCode.Visible = true;
                                lstListItems = (from RCB in entities.RLCS_Client_BasicDetails
                                                join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                on RCB.CB_ClientID equals RCCM.CM_ClientID
                                                where RCB.CB_PF_Code != null
                                                && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                && RCB.IsProcessed == true
                                                && RCCM.CM_Status == "A"
                                                && RCCM.BranchType == "E"
                                                select RCB.CB_PF_Code).Distinct().ToList();
                            }
                            else if (ddlReturnChallanType.SelectedValue.Trim().ToUpper().Equals("ESI"))
                            {
                                ReturnBranch.Visible = false;
                                ReturnEntity.Visible = true;
                                DivCode.Visible = true;

                                lstListItems = (from row in entities.RLCS_Employee_Master
                                                where row.EM_Client_ESI_Number != null
                                                && row.IsProcessed == true
                                                && row.EM_Status == "A"
                                                 && row.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                select row.EM_Client_ESI_Number
                                                ).Distinct().ToList();
                            }
                            else if (ddlReturnChallanType.SelectedValue.Trim().ToUpper().Equals("PT"))
                            {
                                ReturnBranch.Visible = true;
                                ReturnEntity.Visible = false;
                                DivCode.Visible = true;

                                lstListItems = (from REM in entities.RLCS_Employee_Master
                                                join RSM in entities.RLCS_State_Mapping
                                                on REM.EM_Client_PT_State equals RSM.SM_Code
                                                where REM.EM_Client_PT_State != null
                                                && REM.IsProcessed == true
                                                && REM.EM_Status == "A"
                                                && RSM.PT_Applicability == "Y"
                                                && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
                                                select REM.EM_Client_PT_State + "-" + RSM.SM_Name).Distinct().ToList();
                            }
                            else
                            {
                                ReturnBranch.Visible = true;
                                ReturnEntity.Visible = false;
                                DivCode.Visible = false;
                            }
                        }

                    }


                    if (lstListItems.Count > 0)
                    {
                        //ddlReturnCode.DataSource = lstListItems;
                        rptCode.DataSource = lstListItems.Select(x => new { Name = x }).ToList();
                        rptCode.DataBind();
                    }
                    else
                    {
                        // ddlReturnCode.Items.Clear();
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void ddlReturnChallanType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BindMonthYearDropDown();
        //        Bind_ReturnChallanList();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            List<string> lstListItems = new List<string>();

        //            if (!string.IsNullOrEmpty(ddlReturnChallanType.SelectedValue))
        //            {
        //                // ddlFrequency.Items.Clear();
        //                if (ddlReturnChallanType.SelectedValue.Trim().ToUpper() == "PT")
        //                {
        //                    ReturnBranch.Visible = true;
        //                    ReturnEntity.Visible = false;
        //                    DivCode.Visible = true;
        //                }

        //                else if (ddlReturnChallanType.SelectedValue.Trim().ToUpper() == "OTHER")
        //                {
        //                    ReturnBranch.Visible = true;
        //                    ReturnEntity.Visible = false;
        //                    DivCode.Visible = false;
        //                }
        //                else
        //                {
        //                    ReturnBranch.Visible = false;
        //                    ReturnEntity.Visible = true;
        //                    DivCode.Visible = true;

        //                }
        //                if (!string.IsNullOrEmpty(ddlreturnEntity.SelectedValue) && ddlreturnEntity.SelectedValue != "-1")
        //                {
        //                    var entityClient = ddlreturnEntity.SelectedValue.Split('|');

        //                    if (entityClient.Length > 0)
        //                    {
        //                        string clientID = entityClient[1];

        //                        if (!string.IsNullOrEmpty(ddlReturnChallanType.SelectedValue) && !string.IsNullOrEmpty(clientID))
        //                        {
        //                            if (ddlReturnChallanType.SelectedValue.Trim().Equals("EPF"))
        //                            {
        //                                ReturnBranch.Visible = false;
        //                                ReturnEntity.Visible = true;
        //                                DivCode.Visible = true;
        //                                lstListItems = (from RCB in entities.RLCS_Client_BasicDetails
        //                                                join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
        //                                                on RCB.CB_ClientID equals RCCM.CM_ClientID
        //                                                where RCB.CB_PF_Code != null
        //                                                && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
        //                                                && RCB.IsProcessed == true
        //                                                && RCCM.CM_Status == "A"
        //                                                && RCCM.BranchType == "B"
        //                                                select RCB.CB_PF_Code).Distinct().ToList();
        //                            }
        //                            else if (ddlReturnChallanType.SelectedValue.Trim().ToUpper().Equals("ESI"))
        //                            {
        //                                ReturnBranch.Visible = false;
        //                                ReturnEntity.Visible = true;
        //                                DivCode.Visible = true;

        //                                lstListItems = (from row in entities.RLCS_Employee_Master
        //                                                where row.EM_Client_ESI_Number != null
        //                                                && row.IsProcessed == true
        //                                                && row.EM_Status == "A"
        //                                                 && row.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
        //                                                select row.EM_Client_ESI_Number
        //                                                ).Distinct().ToList();
        //                            }
        //                            else if (ddlReturnChallanType.SelectedValue.Trim().ToUpper().Equals("PT"))
        //                            {
        //                                ReturnBranch.Visible = true;
        //                                ReturnEntity.Visible = false;
        //                                DivCode.Visible = true;

        //                                lstListItems = (from REM in entities.RLCS_Employee_Master
        //                                                join RSM in entities.RLCS_State_Mapping
        //                                                on REM.EM_Client_PT_State equals RSM.SM_Code
        //                                                where REM.EM_Client_PT_State != null
        //                                                && REM.IsProcessed == true
        //                                                && REM.EM_Status == "A"
        //                                                && RSM.PT_Applicability == "Y"
        //                                                && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
        //                                                select REM.EM_Client_PT_State + "-" + RSM.SM_Name).Distinct().ToList();
        //                            }
        //                            else
        //                            {
        //                                ReturnBranch.Visible = true;
        //                                ReturnEntity.Visible = false;
        //                                DivCode.Visible = false;
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    cvDuplicateEntry.IsValid = false;
        //                    cvDuplicateEntry.ErrorMessage = "Please Select Entity/Client";
        //                }

        //                if (lstListItems.Count > 0)
        //                {
        //                    //ddlReturnCode.DataSource = lstListItems;
        //                    rptCode.DataSource = lstListItems.Select(x => new { Name = x }).ToList();
        //                    rptCode.DataBind();
        //                }
        //                else
        //                {
        //                    // ddlReturnCode.Items.Clear();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //protected void ddlReturnChallanType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        BindMonthYearDropDown();
        //        Bind_ReturnChallanList();
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            List<string> lstListItems = new List<string>();

        //            if (!string.IsNullOrEmpty(ddlReturnChallanType.SelectedValue))
        //            {
        //                if (ddlReturnChallanType.SelectedValue.Trim().ToUpper() == "PT")
        //                {
        //                    ReturnBranch.Visible = true;
        //                    ReturnEntity.Visible = false;
        //                    DivCode.Visible = true;
        //                }

        //              else if( ddlReturnChallanType.SelectedValue.Trim().ToUpper() == "OTHER")
        //                {
        //                    ReturnBranch.Visible = true;
        //                    ReturnEntity.Visible = false;
        //                    DivCode.Visible = false;
        //                }
        //                else
        //                {
        //                    ReturnBranch.Visible = false;
        //                    ReturnEntity.Visible = true;
        //                    DivCode.Visible = true;

        //                }
        //                if (!string.IsNullOrEmpty(ddlreturnEntity.SelectedValue) && ddlreturnEntity.SelectedValue != "-1")
        //                {
        //                    var entityClient = ddlreturnEntity.SelectedValue.Split('|');

        //                    if (entityClient.Length > 0)
        //                    {
        //                        string clientID = entityClient[1];

        //                        if (!string.IsNullOrEmpty(ddlReturnChallanType.SelectedValue) && !string.IsNullOrEmpty(clientID))
        //                        {
        //                            if (ddlReturnChallanType.SelectedValue.Trim().Equals("EPF"))
        //                            {
        //                                ReturnBranch.Visible = false;
        //                                ReturnEntity.Visible = true;
        //                                DivCode.Visible = true;
        //                                lstListItems = (from RCB in entities.RLCS_Client_BasicDetails
        //                                                join RCCM in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
        //                                                on RCB.CB_ClientID equals RCCM.CM_ClientID
        //                                                where RCB.CB_PF_Code != null
        //                                                && RCCM.CM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
        //                                                && RCB.IsProcessed == true
        //                                                && RCCM.CM_Status == "A"
        //                                                && RCCM.BranchType == "B"
        //                                                select RCB.CB_PF_Code).Distinct().ToList();
        //                            }
        //                            else if (ddlReturnChallanType.SelectedValue.Trim().ToUpper().Equals("ESI"))
        //                            {
        //                                ReturnBranch.Visible = false;
        //                                ReturnEntity.Visible = true;
        //                                DivCode.Visible = true;

        //                                lstListItems = (from row in entities.RLCS_Employee_Master
        //                                                where row.EM_Client_ESI_Number != null
        //                                                && row.IsProcessed == true
        //                                                && row.EM_Status == "A"
        //                                                 && row.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
        //                                                select row.EM_Client_ESI_Number
        //                                                ).Distinct().ToList();
        //                            }
        //                            else if (ddlReturnChallanType.SelectedValue.Trim().ToUpper().Equals("PT"))
        //                            {
        //                                ReturnBranch.Visible = false;
        //                                ReturnEntity.Visible = true;
        //                                DivCode.Visible = true;

        //                                lstListItems = (from REM in entities.RLCS_Employee_Master
        //                                                join RSM in entities.RLCS_State_Mapping
        //                                                on REM.EM_Client_PT_State equals RSM.SM_Code
        //                                                where REM.EM_Client_PT_State != null
        //                                                && REM.IsProcessed == true
        //                                                && REM.EM_Status == "A"
        //                                                && RSM.PT_Applicability == "Y"
        //                                                && REM.EM_ClientID.Trim().ToUpper().Equals(clientID.Trim().ToUpper())
        //                                                select REM.EM_Client_PT_State + "-" + RSM.SM_Name).Distinct().ToList();
        //                            }
        //                            else
        //                            {
        //                                ReturnBranch.Visible = true;
        //                                ReturnEntity.Visible = false;
        //                                DivCode.Visible = false;
        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    cvDuplicateEntry.IsValid = false;
        //                    cvDuplicateEntry.ErrorMessage = "Please Select Entity/Client";
        //                }

        //                if (lstListItems.Count > 0)
        //                {
        //                    //ddlReturnCode.DataSource = lstListItems;
        //                    rptCode.DataSource = lstListItems.Select(x => new { Name = x }).ToList();
        //                    rptCode.DataBind();
        //                }
        //                else
        //                {
        //                    // ddlReturnCode.Items.Clear();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void btnGenerateChallan_Click(object sender, EventArgs e)
        {
            try
            {
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                if (!string.IsNullOrEmpty(ddlChallanType.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(ddlEntityClient.SelectedValue))
                    {
                        var entityClient = ddlEntityClient.SelectedValue.Split('|');

                        if (entityClient.Length > 0)
                        {
                            int custBranchID = Convert.ToInt32(entityClient[0]);
                            string clientID = entityClient[1];

                            if (!string.IsNullOrEmpty(ddlMonthChallan.SelectedValue) && !string.IsNullOrEmpty(ddlYearChallan.SelectedValue))
                            {
                                if (!string.IsNullOrEmpty(ddlChallanCode.SelectedValue))
                                {
                                    string codeValue = string.Empty;
                                    if (ddlChallanType.SelectedValue == "PT")
                                    {
                                        codeValue = ddlChallanCode.SelectedValue.Split('-').Count() > 0 ? ddlChallanCode.SelectedValue.Split('-')[0].ToString() : "";
                                    }
                                    else
                                    {
                                        codeValue = ddlChallanCode.SelectedValue;
                                    }

                                    bool generateDocumentSuccess = false;

                                    string requestUrl = string.Empty;
                                    string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];

                                    requestUrl = rlcsAPIURL + "ReportingChallan/DownloadChallan";

                                    RLCS_Request_DownloadChallan APIRequest = new RLCS_Request_DownloadChallan()
                                    {
                                        ClientID = clientID,
                                        Month = ddlMonthChallan.SelectedValue,
                                        Year = ddlYearChallan.SelectedValue,
                                        ChallanType = ddlChallanType.SelectedValue,
                                        Code = codeValue,

                                        IsAventisAPICall = 1
                                    };

                                    long recordID = RLCS_DocumentManagement.RLCS_GenerateDocumentRequest_Challan(customerID, custBranchID, "CHALLAN", AuthenticationHelper.UserID, APIRequest);

                                    //string responseData = WebAPIUtility.POST_Corporate(requestUrl, corpRecord);
                                    string responseData = WebAPIUtility.POST_RLCS_Common<RLCS_Request_DownloadChallan>(requestUrl, APIRequest);

                                    if (!string.IsNullOrEmpty(responseData))
                                    {
                                        var response_DownloadChallan = JsonConvert.DeserializeObject<RLCS_Response_DownloadRegister>(responseData);

                                        if (response_DownloadChallan != null)
                                        {
                                            if (response_DownloadChallan.StatusCode != 0)
                                                generateDocumentSuccess = Convert.ToBoolean(response_DownloadChallan.StatusCode);

                                            if (generateDocumentSuccess)
                                            {
                                                var lstDocumentPath = response_DownloadChallan.DocumentPath;

                                                if (lstDocumentPath.Count > 0)
                                                {
                                                    string message;
                                                    //Download Generated Documents
                                                    generateDocumentSuccess = RLCS_ComplianceManagement.Generate_Challans(customerID, ddlChallanType.SelectedValue, ddlMonthChallan.SelectedValue, ddlYearChallan.SelectedValue, clientID, codeValue, lstDocumentPath, out message);

                                                    if (!generateDocumentSuccess)
                                                    {
                                                        if (!string.IsNullOrEmpty(message))
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = message;
                                                        }
                                                        else
                                                            cvDuplicateEntry.ErrorMessage = "Something went wrong, Please try again.";
                                                    }
                                                    else
                                                    {
                                                        cvDuplicateEntrysucess.IsValid = false;
                                                        cvDuplicateEntrysucess.ErrorMessage = "Requested Documents generated successfully, Please check Under My Documents";
                                                        cvDuplicateEntrysucess.ValidationGroup = "success";
                                                        cvDuplicateEntry.Visible = false;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                cvDuplicateEntry.IsValid = false;
                                                cvDuplicateEntry.ErrorMessage = "There were No Challans Downloaded For the Selected Criteria";
                                                cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                                                cvDuplicateEntrysucess.Visible = false;
                                            }
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "There were No Challans Downloaded For the Selected Criteria";
                                            cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                                            cvDuplicateEntrysucess.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        cvDuplicateEntry.IsValid = false;
                                        cvDuplicateEntry.ErrorMessage = "There were No Challans Downloaded For the Selected Criteria";
                                        cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                                        cvDuplicateEntrysucess.Visible = false;
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Please Select Challan Code";
                                    cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                                    cvDuplicateEntrysucess.Visible = false;
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Please Select Month-Year";
                                cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                                cvDuplicateEntrysucess.Visible = false;
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Please Select Entity/Client";
                            cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                            cvDuplicateEntrysucess.Visible = false;
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Please Select Entity/Client";
                        cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                        cvDuplicateEntrysucess.Visible = false;
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Please Select Challan Type";
                    cvDuplicateEntry.ValidationGroup = "DocGenValidationGroup";
                    cvDuplicateEntrysucess.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void ddlReturnType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(ddlReturnType.SelectedValue))
        //        {
        //            if (tvFilterLocation_Return.SelectedValue != "-1" && !string.IsNullOrEmpty(tvFilterLocation_Return.SelectedValue))
        //            {
        //                List<SP_RLCS_ReturnMaster_Result> lstReturnMaster = RLCS_ComplianceManagement.GetAll_ReturnMaster();

        //                if (lstReturnMaster != null)
        //                {
        //                    if (lstReturnMaster.Count > 0)
        //                    {
        //                        BindActList_Return(Convert.ToInt32(tvFilterLocation_Return.SelectedValue), lstReturnMaster, ddlReturnType.SelectedValue);

        //                        ddlFrequency.Items.Clear();
        //                        // ddlPeriod.Items.Clear();
        //                        //ddlYearReturn.ClearSelection();
        //                        //ddlReturnList.Items.Clear();
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                cvDuplicateEntry.IsValid = false;
        //                cvDuplicateEntry.ErrorMessage = "Select Entity/Location/Branch";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //protected void ddlAct_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(tvFilterLocation_Return.SelectedValue) && tvFilterLocation_Return.SelectedValue != "-1")
        //        {
        //            if (!string.IsNullOrEmpty(ddlAct.SelectedValue))
        //            {
        //                List<SP_RLCS_ReturnMaster_Result> lstReturnMaster = RLCS_ComplianceManagement.GetAll_ReturnMaster();

        //                if (lstReturnMaster != null)
        //                {
        //                    if (lstReturnMaster.Count > 0)
        //                    {
        //                        //BindFrequency_Return(Convert.ToInt32(tvFilterLocation_Return.SelectedValue), lstReturnMaster, ddlReturnType.SelectedValue, ddlAct.SelectedValue);

        //                        //ddlPeriod.Items.Clear();
        //                        ddlYearReturn.ClearSelection();
        //                        //ddlReturnList.Items.Clear();
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            cvDuplicateEntry.IsValid = false;
        //            cvDuplicateEntry.ErrorMessage = "Select Entity/Location/Branch";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        //private void BindActList_Return(int custBranchID, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string selectedReturnType)
        //{
        //    try
        //    {
        //        ddlAct.DataValueField = "ID";
        //        ddlAct.DataTextField = "Name";

        //        var lstItems = RLCS_ComplianceManagement.GetReturn_Act_Frequency(custBranchID, "Act", lstReturnMaster, selectedReturnType, "", "");

        //        if (lstItems.Count > 0)
        //        {
        //            ddlAct.DataSource = lstItems;
        //            ddlAct.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        private void BindFrequency_Return(int custBranchID, List<SP_RLCS_ReturnMaster_Result> lstReturnMaster, string selectedReturnType, string selectedActType)
        {
            try
            {
                ddlFrequency.DataValueField = "ID";
                ddlFrequency.DataTextField = "Name";

                var lstItems = RLCS_ComplianceManagement.GetReturn_Act_Frequency(custBranchID, "Frequency", lstReturnMaster, selectedReturnType, selectedActType, "");

                if (lstItems.Count > 0)
                {
                    ddlFrequency.DataSource = lstItems;
                    ddlFrequency.DataBind();
                }

                if (lstItems.Count == 1)
                {
                    ddlFrequency_SelectedIndexChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlFrequency_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
                {
                    BindPeriod_Return(ddlFrequency.SelectedValue);
                    ddlYearReturn.ClearSelection();

                    if (ddlReturnChallanType.SelectedValue == "Other")
                    {
                        Bind_ReturnList();
                    }
                    //else if (ddlReturnChallanType.SelectedValue == "PT")
                    //{
                    //    Bind_ReturnChallanList();
                    //}
                    else
                    {
                        BindPTPFESIReturnList();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindPeriod_Return(string selectedFreq)
        {
            try
            {
                if (!string.IsNullOrEmpty(selectedFreq))
                {
                    List<Tuple<string, string, string>> tupleList = new List<Tuple<string, string, string>>();

                    tupleList.Add(new Tuple<string, string, string>("Annual", "Annual", "Annual"));
                    tupleList.Add(new Tuple<string, string, string>("BiAnnual", "BiAnnual", "BiAnnual"));

                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "First Half", "HY1"));
                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "Second Half", "HY2"));

                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q1", "Q1"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q2", "Q2"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q3", "Q3"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q4", "Q4"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "January", "01"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "February", "02"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "March", "03"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "April", "04"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "May", "05"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "June", "06"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "July", "07"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "August", "08"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "September", "09"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "Octomber", "10"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "November", "11"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "December", "12"));

                    DataTable dtReturnPeriods = new DataTable();
                    dtReturnPeriods.Columns.Add("ID");
                    dtReturnPeriods.Columns.Add("Name");

                    var lst = tupleList.FindAll(m => m.Item1 == selectedFreq);
                    if (lst.Count > 0)
                    {
                        foreach (var item in lst)
                        {
                            DataRow drReturnPeriods = dtReturnPeriods.NewRow();
                            drReturnPeriods["ID"] = item.Item3;
                            drReturnPeriods["Name"] = item.Item2;
                            dtReturnPeriods.Rows.Add(drReturnPeriods);
                        }

                        rptPeriodList.DataSource = dtReturnPeriods;
                        rptPeriodList.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
 
        private void Bind_ReturnList()
        {
            try
            {
                List<SP_RLCS_ReturnMaster_Result> lstReturnMaster = RLCS_ComplianceManagement.GetAll_ReturnMaster();

                if (lstReturnMaster != null)
                {
                    if (lstReturnMaster.Count > 0)
                    {                        
                        var myAssignedCompliances = RLCS_ComplianceManagement.GetAll_AssignedCompliances(AuthenticationHelper.UserID);

                        if (myAssignedCompliances != null)
                            if (myAssignedCompliances.Count > 0)
                                lstReturnMaster = lstReturnMaster.Where(row => myAssignedCompliances.Contains((long)row.AVACOM_ComplianceID)).ToList();

                        var lstItems = RLCS_ComplianceManagement.GetReturnList(Convert.ToInt32(tvFilterLocation_Return.SelectedValue), lstReturnMaster, ddlFrequency.SelectedValue);
                        if (lstItems.Count > 0)
                        {
                            if (ddlFrequency.Items.Count <= 0)
                            {
                                var frequencies = (from frequency in lstReturnMaster
                                                   select new { Frequency = frequency.RM_Frequency }).Distinct().ToList();

                                if (frequencies.Count > 0)
                                {                                    
                                    ddlFrequency.DataValueField = "Frequency";
                                    ddlFrequency.DataTextField = "Frequency";

                                    ddlFrequency.DataSource = frequencies;
                                    ddlFrequency.DataBind();
                                    ddlFrequency.Items.Insert(-1, new ListItem("Select", "-1"));
                                    //ddlFrequency_SelectedIndexChanged(null, null);
                                }
                            }

                            if (ddlFrequency.SelectedValue != "-1")
                            {
                                rptReturnList.DataSource = lstItems;
                                rptReturnList.DataBind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        private void Bind_ReturnChallanList()
        {
            try
            {
                List<SP_RLCS_ReturnMaster_Result> lstReturnMaster = new List<SP_RLCS_ReturnMaster_Result>();

                if (ddlReturnChallanType.SelectedValue == "Other")
                {
                    lstReturnMaster = RLCS_ComplianceManagement.GetAll_ReturnMaster();
                }
                else
                {
                    lstReturnMaster = RLCS_ComplianceManagement.GetAll_ReturnMasterByType(ddlReturnChallanType.SelectedValue);
                }

                if (lstReturnMaster != null)
                {
                    if (lstReturnMaster.Count > 0)
                    {
                        ddlFrequency.Items.Clear();
                        if (ddlFrequency.Items.Count <= 0)
                        {
                            List<string> frequencies = new List<string>();
                            if (ddlReturnChallanType.SelectedValue.ToUpper() == "ESI")
                            {
                                frequencies = lstReturnMaster.Where(x => x.RM_Act == "ESI").Select(x => x.RM_Frequency).Distinct().ToList();

                            }
                            else if (ddlReturnChallanType.SelectedValue.ToUpper() == "EPF")
                            {
                                frequencies = lstReturnMaster.Where(x => x.RM_Act == "EPF").Select(x => x.RM_Frequency).Distinct().ToList();
                            }
                            //else if (ddlReturnChallanType.SelectedValue.ToUpper() == "PT")
                            //{
                            //    frequencies = lstReturnMaster.Where(x => x.RM_Act == "PT").Select(x => x.RM_Frequency).Distinct().ToList();

                            //}
                            else
                            {
                                frequencies = lstReturnMaster.Select(x => x.RM_Frequency).Distinct().ToList();
                            }
                            if (frequencies.Count > 0)
                            {
                                ddlFrequency.DataSource = frequencies;
                                ddlFrequency.DataBind();
                                ddlFrequency.Items.Insert(0, new ListItem("Select", "0"));
                                //ddlFrequency_SelectedIndexChanged(null, null);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlreturnEntity_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindMonthYearDropDown();
            ddlReturnChallanType_SelectedIndexChanged(sender, e);
        }

        protected void BindPTPFESIReturnList()
        {
            List<SP_RLCS_ReturnMaster_Result> lstReturnMaster = new List<SP_RLCS_ReturnMaster_Result>();

            if (ddlReturnChallanType.SelectedValue == "Other")
            {
                lstReturnMaster = RLCS_ComplianceManagement.GetAll_ReturnMaster();
            }
            else
            {
                lstReturnMaster = RLCS_ComplianceManagement.GetAll_ReturnMasterByType(ddlReturnChallanType.SelectedValue);
            }

            if (lstReturnMaster != null && lstReturnMaster.Count > 0)
            {
                var myAssignedCompliances = RLCS_ComplianceManagement.GetAll_AssignedCompliances(AuthenticationHelper.UserID);

                if (myAssignedCompliances != null)
                    if (myAssignedCompliances.Count > 0)
                        lstReturnMaster = lstReturnMaster.Where(row => myAssignedCompliances.Contains((long)row.AVACOM_ComplianceID)).ToList();

                if (!string.IsNullOrEmpty(ddlreturnEntity.SelectedValue))
                {
                    var entityClient = ddlreturnEntity.SelectedValue.Split('|');

                    if (entityClient.Length > 0)
                    {
                        string branchID = entityClient[0];
                        custBranchID = Convert.ToInt32(branchID);

                        var lstItems = RLCS_ComplianceManagement.GetReturnChallanList(custBranchID, lstReturnMaster, ddlFrequency.SelectedValue);

                        if (lstItems.Count > 0)
                        {
                            if (ddlFrequency.SelectedValue != "-1")
                            {
                                rptReturnList.DataSource = lstItems;
                                rptReturnList.DataBind();
                            }
                        }
                    }
                }
            }
        }

        protected void tvFilterLocation_Return_SelectedNodeChanged(object sender, EventArgs e)
        {
            tbxFilterLocation_Return.Text = tvFilterLocation_Return.SelectedNode.Text;

            if (!string.IsNullOrEmpty(tvFilterLocation_Return.SelectedValue))
            {
                if (tvFilterLocation_Return.SelectedValue != "-1")
                {
                    var existReturnComplianceMapping = Check_ReturnComplianceMapping(Convert.ToInt32(tvFilterLocation_Return.SelectedValue));
                    if (!existReturnComplianceMapping)
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "No Return Compliance Mapped to Selected Entity/Branch/Location";
                    }
                    else
                    {
                        if (ddlReturnChallanType.SelectedValue.ToUpper() == "PT")
                        {
                            //Bind_ReturnChallanList();
                            ddlReturnChallanType_SelectedIndexChanged(sender, e);
                        }
                            
                        else
                            Bind_ReturnList();
                    }
                }
            }
        }

        private bool Check_ReturnComplianceMapping(int branchID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    List<long> complianceIDs = new List<long>();

                    complianceIDs = RLCS_ComplianceManagement.GetAll_ReturnsRelatedCompliance_SectionWise(string.Empty);

                    if (complianceIDs.Count > 0)
                    {
                        var complianceInstances = (from row in entities.ComplianceInstances
                                                   where row.CustomerBranchID == branchID
                                                   && complianceIDs.Contains(row.ComplianceId)
                                                   && row.IsDeleted == false
                                                   select row.ID).ToList();
                        if (complianceInstances.Count > 0)
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return true;
            }
        }

        protected void btnGenerateReturn_Click(object sender, EventArgs e)
        {
            try
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                List<RLCS_Returns_Compliance_Mapping> record = new List<RLCS_Returns_Compliance_Mapping>();
                if (ddlReturnChallanType.SelectedValue.ToUpper() == "EPF" || ddlReturnChallanType.SelectedValue.ToUpper() == "ESI")
                {
                    ReturnChallanGenerate();
                }
                else
                {
                    if (!string.IsNullOrEmpty(tvFilterLocation_Return.SelectedValue))
                    {
                        if (tvFilterLocation_Return.SelectedValue != "-1")
                        {
                            if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
                            {
                                var clientLocationDetailRecord = RLCS_Master_Management.GetClientLocationDetails(Convert.ToInt32(tvFilterLocation_Return.SelectedValue));

                                if (clientLocationDetailRecord != null)
                                {
                                    if (clientLocationDetailRecord.AVACOM_BranchID != null)
                                    {
                                        custBranchID = Convert.ToInt32(clientLocationDetailRecord.AVACOM_BranchID);
                                        var returns = RLCS_ComplianceManagement.GetAllReturnsDetail(Convert.ToInt64(custBranchID));
                                        RLCS_ReturnRecords_Post returnPost = new RLCS_ReturnRecords_Post();

                                        int iSelectedPerids = 0;
                                        int iSelectedReturns = 0;
                                        foreach (RepeaterItem aItem in rptPeriodList.Items)
                                        {
                                            try
                                            {
                                                CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
                                                if (chkPeriod != null && chkPeriod.Checked)
                                                {
                                                    string Period = string.Empty;
                                                    Label lblPeriodID = (Label)aItem.FindControl("lblPeriodID");
                                                    if (lblPeriodID != null && !string.IsNullOrEmpty(lblPeriodID.Text))
                                                    {
                                                        Period = lblPeriodID.Text;
                                                    }

                                                    foreach (RepeaterItem returnItem in rptReturnList.Items)
                                                    {
                                                        try
                                                        {
                                                            CheckBox chkReturn = (CheckBox)returnItem.FindControl("chkReturn");
                                                            if (chkReturn != null && chkReturn.Checked)
                                                            {
                                                                Label lblReturnID = (Label)returnItem.FindControl("lblReturnID");
                                                                if (lblReturnID != null && !string.IsNullOrEmpty(lblReturnID.Text))
                                                                {
                                                                    int? returnID = Convert.ToInt32(lblReturnID.Text);
                                                                    var selectedreturn = (from row in returns
                                                                                          where row.ReturnID == returnID
                                                                                          select row).FirstOrDefault();

                                                                    if (selectedreturn != null)
                                                                    {
                                                                        RLCS_Request_DownloadReturn APIRequest = new RLCS_Request_DownloadReturn();

                                                                        APIRequest.ClientId = clientLocationDetailRecord.CM_ClientID;
                                                                        APIRequest.SM_Code = clientLocationDetailRecord.CM_State;
                                                                        APIRequest.SM_Name = RLCS_ClientsManagement.GetStateName(clientLocationDetailRecord.CM_State);
                                                                        APIRequest.LocationID = clientLocationDetailRecord.CM_City;
                                                                        APIRequest.LM_Name = RLCS_ClientsManagement.GetLocationName(clientLocationDetailRecord.CM_City);
                                                                        APIRequest.BranchID = clientLocationDetailRecord.CM_ClientName;
                                                                        APIRequest.Month = Period;
                                                                        APIRequest.Year = ddlYearReturn.SelectedItem.Text;
                                                                        APIRequest.FormID = Convert.ToString(selectedreturn.ReturnID);
                                                                        APIRequest.ActId = selectedreturn.RM_Act;
                                                                        APIRequest.Frequency_ID = selectedreturn.RM_Frequency;
                                                                        APIRequest.FormName = selectedreturn.RM_Name;
                                                                        APIRequest.Return_DownloadType = selectedreturn.RM_Doc_Format;
                                                                        APIRequest.R_Type = selectedreturn.RM_Return_Category;
                                                                        APIRequest.PF_ESI_Code = "";
                                                                        APIRequest.EMPID = "";
                                                                        APIRequest.Employee = "";

                                                                        if (selectedreturn.RM_Act == "PT")
                                                                        {
                                                                            APIRequest.Add_parameter = clientLocationDetailRecord.CL_PT_State;
                                                                        }
                                                                        else
                                                                        {
                                                                            APIRequest.Add_parameter = ClientPFESICode(clientLocationDetailRecord.CM_ClientID, selectedreturn.RM_Act);
                                                                        }

                                                                        APIRequest.Sectionid = "";
                                                                        APIRequest.Return_Path = selectedreturn.RM_Download_Path;
                                                                        APIRequest.Type = "";
                                                                        returnPost.lst_ReturnFormats.Add(APIRequest);
                                                                    }
                                                                }

                                                                iSelectedReturns += 1;
                                                            }
                                                            else
                                                            {
                                                                cvDuplicateEntry.IsValid = false;
                                                                cvDuplicateEntry.ErrorMessage = "Select Return";
                                                            }
                                                        }
                                                        catch (Exception)
                                                        {

                                                        }
                                                    }

                                                    iSelectedPerids += 1;
                                                }
                                                else
                                                {
                                                    cvDuplicateEntry.IsValid = false;
                                                    cvDuplicateEntry.ErrorMessage = "Select Period";
                                                }
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }

                                        if (returnPost.lst_ReturnFormats.Count() > 0)
                                        {
                                            DownloadReturns(returnPost, clientLocationDetailRecord);
                                        }
                                        //else
                                        //{
                                        //    cvDuplicateEntry.IsValid = false;
                                        //    cvDuplicateEntry.ErrorMessage = "Select Return";
                                        //}
                                    }
                                }
                                else
                                {
                                    cvDuplicateEntry.IsValid = false;
                                    cvDuplicateEntry.ErrorMessage = "Branch Details not Found, Please complete the details Master->Entity/Location/Branch";
                                }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Select Frequency";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Select Entity/Location/Branch";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Select Entity/Location/Branch";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        //private void ReturnChallanGenerate()
        //{
        //    try
        //    {
        //        customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        List<RLCS_Returns_Compliance_Mapping> record = new List<RLCS_Returns_Compliance_Mapping>();
        //        if (!string.IsNullOrEmpty(ddlReturnChallanType.SelectedItem.Text))
        //        {

        //            if (tvFilterLocation_Return.SelectedValue != "-1")
        //            {
        //                if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
        //                {
        //                    var entityClient = ddlreturnEntity.SelectedValue.Split('|');

        //                    if (entityClient.Length > 0)
        //                    {
        //                        string branchID = entityClient[0];
        //                        custBranchID = Convert.ToInt32(branchID);
        //                        var clientLocationDetailRecord = RLCS_Master_Management.GetEntityDetails(custBranchID);
        //                        var branches = RLCS_ComplianceManagement.GetBranchesForEntity(custBranchID);
        //                        if (branches != null)
        //                        {
        //                            //chage this later
        //                            if (branches != null)
        //                            {
        //                                //custBranchID = Convert.ToInt32(clientLocationDetailRecord.AVACOM_BranchID);

        //                                var returns = RLCS_ComplianceManagement.GetAllReturnsDetailForEntity(branches);
        //                                RLCS_ReturnRecords_Post returnPost = new RLCS_ReturnRecords_Post();

        //                                int iSelectedPerids = 0;
        //                                int iSelectedReturns = 0;
        //                                foreach (RepeaterItem aItem in rptPeriodList.Items)
        //                                {
        //                                    try
        //                                    {
        //                                        CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
        //                                        if (chkPeriod != null && chkPeriod.Checked)
        //                                        {
        //                                            string Period = string.Empty;
        //                                            Label lblPeriodID = (Label)aItem.FindControl("lblPeriodID");
        //                                            if (lblPeriodID != null && !string.IsNullOrEmpty(lblPeriodID.Text))
        //                                            {
        //                                                Period = lblPeriodID.Text;
        //                                            }

        //                                            foreach (RepeaterItem returnItem in rptReturnList.Items)
        //                                            {
        //                                                try
        //                                                {
        //                                                    CheckBox chkReturn = (CheckBox)returnItem.FindControl("chkReturn");
        //                                                    if (chkReturn != null && chkReturn.Checked)
        //                                                    {
        //                                                        Label lblReturnID = (Label)returnItem.FindControl("lblReturnID");
        //                                                        if (lblReturnID != null && !string.IsNullOrEmpty(lblReturnID.Text))
        //                                                        {
        //                                                            ///Code list
        //                                                            foreach (RepeaterItem codeitem in rptCode.Items)
        //                                                            {
        //                                                                try
        //                                                                {
        //                                                                    CheckBox chkReturnCode = (CheckBox)codeitem.FindControl("chkCode");
        //                                                                    if (chkReturnCode != null && chkReturnCode.Checked)
        //                                                                    {
        //                                                                        string code = string.Empty;
        //                                                                        Label lblcode = (Label)codeitem.FindControl("lblcodeName");
        //                                                                        if (lblcode != null && !string.IsNullOrEmpty(lblcode.Text))
        //                                                                        {
        //                                                                            code = lblcode.Text;
        //                                                                        }


        //                                                                        int? returnID = Convert.ToInt32(lblReturnID.Text);
        //                                                                        var selectedreturn = (from row in returns
        //                                                                                              where row.ReturnID == returnID
        //                                                                                              select row).FirstOrDefault();

        //                                                                        if (selectedreturn != null)
        //                                                                        {
        //                                                                            RLCS_Request_DownloadReturn APIRequest = new RLCS_Request_DownloadReturn();

        //                                                                            APIRequest.ClientId = clientLocationDetailRecord.CM_ClientID;
        //                                                                            APIRequest.SM_Code = "";
        //                                                                            APIRequest.SM_Name = "";
        //                                                                            APIRequest.LocationID = "";
        //                                                                            APIRequest.LM_Name = "";
        //                                                                            APIRequest.BranchID = "";
        //                                                                            APIRequest.Month = Period;
        //                                                                            APIRequest.Year = ddlYearReturn.SelectedItem.Text;
        //                                                                            APIRequest.FormID = Convert.ToString(selectedreturn.ReturnID);
        //                                                                            APIRequest.ActId = selectedreturn.RM_Act;
        //                                                                            APIRequest.Frequency_ID = selectedreturn.RM_Frequency;
        //                                                                            APIRequest.FormName = selectedreturn.RM_Name;
        //                                                                            APIRequest.Return_DownloadType = selectedreturn.RM_Doc_Format;
        //                                                                            APIRequest.R_Type = selectedreturn.RM_Return_Category;
        //                                                                            APIRequest.PF_ESI_Code = "";
        //                                                                            APIRequest.EMPID = "";
        //                                                                            APIRequest.Employee = "";

        //                                                                            //if (selectedreturn.RM_Act == "PT")
        //                                                                            //{
        //                                                                                APIRequest.Add_parameter = code;
        //                                                                            //}
        //                                                                            //else
        //                                                                            //{
        //                                                                            //    APIRequest.Add_parameter = ClientPFESICode(clientLocationDetailRecord.CM_ClientID, selectedreturn.RM_Act);
        //                                                                            //}

        //                                                                            APIRequest.Sectionid = "";
        //                                                                            APIRequest.Return_Path = selectedreturn.RM_Download_Path;
        //                                                                            APIRequest.Type = "";
        //                                                                            returnPost.lst_ReturnFormats.Add(APIRequest);
        //                                                                        }
        //                                                                    }

        //                                                                }
        //                                                                catch (Exception)
        //                                                                {

        //                                                                }
        //                                                                iSelectedReturns += 1;
        //                                                            }
        //                                                        }
        //                                                        else
        //                                                        {
        //                                                            cvDuplicateEntry.IsValid = false;
        //                                                            cvDuplicateEntry.ErrorMessage = "Select Return";
        //                                                        }
        //                                                    }
        //                                                }
        //                                                catch (Exception)
        //                                                {

        //                                                }
        //                                            }

        //                                            iSelectedPerids += 1;
        //                                        }
        //                                        else
        //                                        {
        //                                            cvDuplicateEntry.IsValid = false;
        //                                            cvDuplicateEntry.ErrorMessage = "Select Period";
        //                                        }
        //                                    }
        //                                    catch (Exception)
        //                                    {

        //                                    }
        //                                }

        //                                if (returnPost.lst_ReturnFormats.Count() > 0)
        //                                {
        //                                    DownloadReturns(returnPost, clientLocationDetailRecord);
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            cvDuplicateEntry.IsValid = false;
        //                            cvDuplicateEntry.ErrorMessage = "Branch Details not Found, Please complete the details Master->Entity/Location/Branch";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        cvDuplicateEntry.IsValid = false;
        //                        cvDuplicateEntry.ErrorMessage = "Select Frequency";
        //                    }
        //                }
        //                else
        //                {
        //                    cvDuplicateEntry.IsValid = false;
        //                    cvDuplicateEntry.ErrorMessage = "Select Entity/Location/Branch";
        //                }
        //            }
        //            else
        //            {
        //                cvDuplicateEntry.IsValid = false;
        //                cvDuplicateEntry.ErrorMessage = "Select Entity/Location/Branch";
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}


        private void ReturnChallanGenerate()
        {
            try
            {
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                List<RLCS_Returns_Compliance_Mapping> record = new List<RLCS_Returns_Compliance_Mapping>();
                RLCS_CustomerBranch_ClientsLocation_Mapping clientLocationDetailRecord = new RLCS_CustomerBranch_ClientsLocation_Mapping();
                if (!string.IsNullOrEmpty(ddlReturnChallanType.SelectedItem.Text))
                {

                    if (tvFilterLocation_Return.SelectedValue != "-1")
                    {
                        if (!string.IsNullOrEmpty(ddlFrequency.SelectedValue))
                        {
                            var entityClient = ddlreturnEntity.SelectedValue.Split('|');

                            if (entityClient.Length > 0)
                            {
                                string branchID = entityClient[0];
                                custBranchID = Convert.ToInt32(branchID);
                                clientLocationDetailRecord = RLCS_Master_Management.GetEntityDetails(custBranchID);
                                var branches = RLCS_ComplianceManagement.GetBranchesForEntity(custBranchID);
                                record = RLCS_ComplianceManagement.GetAllReturnsDetailForEntity(branches);


                            }
                            else
                            {
                                List<int> Branch = new List<int>();
                                clientLocationDetailRecord = RLCS_Master_Management.GetClientLocationDetails(Convert.ToInt32(tvFilterLocation_Return.SelectedValue));
                                if (clientLocationDetailRecord.AVACOM_BranchID > 0)
                                {
                                    Branch.Add(Convert.ToInt32(clientLocationDetailRecord.AVACOM_BranchID));
                                    record = RLCS_ComplianceManagement.GetAllReturnsDetailForEntity(Branch);
                                }

                            }
                            if (record != null && clientLocationDetailRecord != null)
                            {
                                RLCS_ReturnRecords_Post returnPost = new RLCS_ReturnRecords_Post();

                                int iSelectedPerids = 0;
                                int iSelectedReturns = 0;
                                foreach (RepeaterItem aItem in rptPeriodList.Items)
                                {
                                    try
                                    {
                                        CheckBox chkPeriod = (CheckBox)aItem.FindControl("chkPeriod");
                                        if (chkPeriod != null && chkPeriod.Checked)
                                        {
                                            string Period = string.Empty;
                                            Label lblPeriodID = (Label)aItem.FindControl("lblPeriodID");
                                            if (lblPeriodID != null && !string.IsNullOrEmpty(lblPeriodID.Text))
                                            {
                                                Period = lblPeriodID.Text;
                                            }

                                            foreach (RepeaterItem returnItem in rptReturnList.Items)
                                            {
                                                try
                                                {
                                                    CheckBox chkReturn = (CheckBox)returnItem.FindControl("chkReturn");
                                                    if (chkReturn != null && chkReturn.Checked)
                                                    {
                                                        Label lblReturnID = (Label)returnItem.FindControl("lblReturnID");
                                                        if (lblReturnID != null && !string.IsNullOrEmpty(lblReturnID.Text))
                                                        {
                                                            ///Code list
                                                            foreach (RepeaterItem codeitem in rptCode.Items)
                                                            {
                                                                try
                                                                {
                                                                    CheckBox chkReturnCode = (CheckBox)codeitem.FindControl("chkReturnCode");
                                                                    if (chkReturnCode != null && chkReturnCode.Checked)
                                                                    {
                                                                        string code = string.Empty;
                                                                        Label lblcode = (Label)codeitem.FindControl("lblcodeName");
                                                                        if (lblcode != null && !string.IsNullOrEmpty(lblcode.Text))
                                                                        {
                                                                            code = lblcode.Text;
                                                                        }


                                                                        int? returnID = Convert.ToInt32(lblReturnID.Text);
                                                                        var selectedreturn = (from row in record
                                                                                              where row.ReturnID == returnID
                                                                                              select row).FirstOrDefault();

                                                                        if (selectedreturn != null)
                                                                        {
                                                                            RLCS_Request_DownloadReturn APIRequest = new RLCS_Request_DownloadReturn();

                                                                            APIRequest.ClientId = clientLocationDetailRecord.CM_ClientID;
                                                                            APIRequest.SM_Code = "";
                                                                            APIRequest.SM_Name = "";
                                                                            APIRequest.LocationID = "";
                                                                            APIRequest.LM_Name = "";
                                                                            APIRequest.BranchID = "";
                                                                            APIRequest.Month = Period;
                                                                            APIRequest.Year = ddlYearReturn.SelectedItem.Text;
                                                                            APIRequest.FormID = Convert.ToString(selectedreturn.ReturnID);
                                                                            APIRequest.ActId = selectedreturn.RM_Act;
                                                                            APIRequest.Frequency_ID = selectedreturn.RM_Frequency;
                                                                            APIRequest.FormName = selectedreturn.RM_Name;
                                                                            APIRequest.Return_DownloadType = selectedreturn.RM_Doc_Format;
                                                                            APIRequest.R_Type = selectedreturn.RM_Return_Category;
                                                                            APIRequest.PF_ESI_Code = "";
                                                                            APIRequest.EMPID = "";
                                                                            APIRequest.Employee = "";

                                                                            if (selectedreturn.RM_Act == "PT")
                                                                            {
                                                                                APIRequest.Add_parameter = clientLocationDetailRecord.CL_PT_State;
                                                                            }
                                                                            else
                                                                            {
                                                                                APIRequest.Add_parameter = code;
                                                                            }

                                                                            APIRequest.Sectionid = "";
                                                                            APIRequest.Return_Path = selectedreturn.RM_Download_Path;
                                                                            APIRequest.Type = "";
                                                                            returnPost.lst_ReturnFormats.Add(APIRequest);
                                                                        }
                                                                    }

                                                                }
                                                                catch (Exception)
                                                                {

                                                                }
                                                                iSelectedReturns += 1;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            cvDuplicateEntry.IsValid = false;
                                                            cvDuplicateEntry.ErrorMessage = "Select Return";
                                                        }
                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                }
                                            }

                                            iSelectedPerids += 1;
                                        }
                                        else
                                        {
                                            cvDuplicateEntry.IsValid = false;
                                            cvDuplicateEntry.ErrorMessage = "Select Period";
                                        }
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }

                                if (returnPost.lst_ReturnFormats.Count() > 0)
                                {
                                    DownloadReturns(returnPost, clientLocationDetailRecord);
                                }
                                // }
                            }
                            else
                            {
                                cvDuplicateEntry.IsValid = false;
                                cvDuplicateEntry.ErrorMessage = "Branch Details not Found, Please complete the details Master->Entity/Location/Branch";
                            }
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "Select Frequency";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "Select Entity/Location/Branch";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "Select Return Type";
                }

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void DownloadReturns(RLCS_ReturnRecords_Post returnPost, RLCS_CustomerBranch_ClientsLocation_Mapping clientLocationDetailRecord)
        {
            //RLCS_ReturnRecords_Post returnPost = new RLCS_ReturnRecords_Post();
            string requestUrl = string.Empty;

            string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
           
            bool generateDocumentSuccess = false;

            requestUrl = rlcsAPIURL + "ReportingReturn/DownloadReturns";
            if (returnPost != null)
            {
                long recordID = RLCS_DocumentManagement.RLCS_GenerateDocumentRequest_Return(customerID,custBranchID, "RETURN", AuthenticationHelper.UserID, returnPost);

                string responseData = WebAPIUtility.POST_RLCS_Common<RLCS_ReturnRecords_Post>(requestUrl, returnPost);

                if (!string.IsNullOrEmpty(responseData))
                {
                    var response_DownloadReturn = JsonConvert.DeserializeObject<RLCS_Response_DownloadReturn>(responseData);

                    if (response_DownloadReturn != null)
                    {
                        if (response_DownloadReturn.StatusCode != 0)
                            generateDocumentSuccess = Convert.ToBoolean(response_DownloadReturn.StatusCode);

                        List<Tuple<string, bool>> lstDocumentGenerateReport = new List<Tuple<string, bool>>();
                        int docuGenerateCounter = 0;

                        if (generateDocumentSuccess)
                        {
                            var lstGeneratedReturns = response_DownloadReturn.Returns;

                            if (lstGeneratedReturns != null)
                            {
                                if (lstGeneratedReturns.Count > 0)
                                {
                                    lstGeneratedReturns.ForEach(eachReturn =>
                                    {
                                        if (!string.IsNullOrEmpty(eachReturn.ReturnId))
                                        {
                                            string documentPath = eachReturn.DocumentPath;

                                            int returnID = Convert.ToInt32(eachReturn.ReturnId);
                                            var lstReturnComplianceRecord = RLCS_ComplianceManagement.Get_ReturnsComplianceRecordsByReturnID(returnID);

                                            if (lstReturnComplianceRecord != null)
                                            {
                                                string month = string.Empty;
                                                string year = string.Empty;

                                                var tuple = Business.ComplianceManagement.Get_RLCSMonthYear(eachReturn.Frequency_ID, eachReturn.Month, eachReturn.Year);

                                                if (tuple != null)
                                                {
                                                    month = tuple.Item1;
                                                    year = tuple.Item2;
                                                }

                                                //Download Generated Documents
                                                generateDocumentSuccess = RLCS_ComplianceManagement.Save_GeneratedReturns(customerID, custBranchID, month, year, lstReturnComplianceRecord, documentPath);

                                                if (generateDocumentSuccess)
                                                    docuGenerateCounter++;

                                                lstDocumentGenerateReport.Add(new Tuple<string, bool>(lstReturnComplianceRecord.RM_Name, generateDocumentSuccess));
                                            }
                                        }
                                    });
                                }
                            }
                        }

                        if (docuGenerateCounter > 0)
                        {
                            int saveSuccessCount = lstDocumentGenerateReport.Where(row => row.Item2 == true).Count();
                            int saveFailedCount = lstDocumentGenerateReport.Where(row => row.Item2 == false).Count();

                            string message = string.Empty;

                            if (saveSuccessCount > 0)
                                message += saveSuccessCount + "-Document(" + string.Join(", ", lstDocumentGenerateReport.Where(row => row.Item2 == true).Select(row => row.Item1).ToList()) + ") generated successfully;";

                            if (saveFailedCount > 0)
                                message += saveFailedCount + "-Document(" + string.Join(", ", lstDocumentGenerateReport.Where(row => row.Item2 == false).Select(row => row.Item1).ToList()) + ") generation failed;";

                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = message;
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "There were No Returns Downloaded For the Selected Criteria";
                        }
                    }
                    else
                    {
                        cvDuplicateEntry.IsValid = false;
                        cvDuplicateEntry.ErrorMessage = "There were No Returns Downloaded For the Selected Criteria";
                    }
                }
                else
                {
                    cvDuplicateEntry.IsValid = false;
                    cvDuplicateEntry.ErrorMessage = "There were No Returns Downloaded For the Selected Criteria";
                }

            }
            else
            {
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "There were No Returns Downloaded For the Selected Criteria";
            }
           // return generateDocumentSuccess;
        }

        private string ClientPFESICode(string clientid, string actType)
        {
            if (actType == "ESI" || actType == "EPF")
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var basicdetails = (from row in entities.RLCS_Client_BasicDetails
                                        where row.CB_ClientID == clientid
                                        select row).SingleOrDefault();

                    if (basicdetails != null)
                    {
                        if (actType == "ESI")
                        {
                            return basicdetails.CM_ESICCode;
                        }
                        else if (actType == "EPF")
                        {
                            return basicdetails.CB_PF_Code;
                        }
                    }
                }
            }

            return string.Empty; 
        }
    }
}
 