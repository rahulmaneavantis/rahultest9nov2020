﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.UI;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_AbstractMaster : System.Web.UI.Page
    {
        protected int CustId;
        protected int UserId;
        protected static string Path;
        protected static string ProfileID;
        public static string RLCSRegistrationDocPath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
            if (!string.IsNullOrEmpty(Request.QueryString["RLCSDownloadPath"]))
            {
                string DocType = Request.QueryString["RLCSDownloadPath"].ToString();
                bool _objDownloadval = RLCS_DocumentDownload(DocType);
                if (!_objDownloadval)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document to Download')", true);
                }
            }
        }
        private bool RLCS_DocumentDownload(string docType)
        {
            try
            {
                if (!string.IsNullOrEmpty(docType))
                {
                    if (File.Exists(docType))
                    {
                        string filename = docType.Substring(docType.LastIndexOf('\\') + 1);
                        Response.Buffer = true;
                        Response.Clear();
                        Response.ClearContent();
                        Response.ContentType = "application/octet-stream";
                        Response.AddHeader("content-disposition", "attachment; filename=" + filename);
                        Response.BinaryWrite(DocumentManagement.ReadDocFiles(docType)); // create the file
                        Response.Flush(); // send it to the client to download
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "NoDcoumentAlert();", true);
                }
                return true;
            }

            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }

       
    }
}