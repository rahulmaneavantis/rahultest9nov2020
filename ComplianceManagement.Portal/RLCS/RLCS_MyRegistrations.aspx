﻿<%@ Page Title="Licenses & Renewals :: My Registrations" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_MyRegistrations.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyRegistrations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

<%--    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.common.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.rtl.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.silver.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.2.620/styles/kendo.mobile.all.min.css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://kendo.cdn.telerik.com/2018.2.620/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.min.js"></script>--%>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />
    
    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <style>
        .k-grid-content { min-height:50px; }
    </style>

    <script type="text/javascript">
        function change_label() {      
            document.getElementById("ContentPlaceHolder1_lblregDocumenterror").innerText = 'This is updated value';           
        }   
    </script>
    <script type="text/jscript">
        $(document).ready(function () {
            setactivemenu('leftnoticesmenu');
            fhead('My New Registrations/Licenses & Renewals');
        });

        $(document).on("click", "#grid tbody tr .ob-download", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDownloadOverviewpup(item.RecordID,"REN","Download")            
            return true;
        });

        $(document).on("click", "#grid tbody tr .ob-overview", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            OpenDocumentOverviewpup(item.RecordID,"REN","View");
            return true;
        });
       
        function OpenDownloadOverviewpup(RecordID,DocType,Event) {
            $('#DownloadViews').attr('src', "RLCS_MyRegistrations.aspx?RLCSRecordID=" + RecordID + "&RLCSDockType=" + DocType+"&Event="+Event);
        }

        function OpenDocumentOverviewpup(RecordID,DocType,Event) {
            debugger;
            $('#divViewDocument').modal('show');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViews').attr('src', "RLCS_DocumentOverview.aspx?RecordID=" + RecordID + "&DocType="+DocType+"&Event="+Event)
        }

        function CloseClearDV() {
            $('#DownloadViews').attr('src', "../Common/blank.html");           
        }

        function fcloseStory(obj) {
            debugger;
            var DataId = $(obj).attr('data-Id');
            var dataKId = $(obj).attr('data-K-Id');
            var seq = $(obj).attr('data-seq');
            var deepspan = $('#' + DataId + ' > li > span > span.k-i-close')[seq];
            $(deepspan).trigger('click');
            var upperli = $('#' + dataKId);            
            $(upperli).remove();
            fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc');
            //fCreateStoryBoard('ddlMonth', 'filtermonth', 'mon');
            //fCreateStoryBoard('ddlYear', 'filteryear', 'yer');
            //CheckFilterClearorNot();
            CheckFilterClearorNotMain();
        };

        $(document).ready(function(){
            $("#dropdowntree").kendoDropDownTree({ 
                placeholder: "Entity/State/Location/Branch",
                checkboxes: {
                    checkChildren: true
                },
                checkAll: true,
                autoWidth: true,
                checkAllTemplate: "Select All",
                dataTextField: "Name",
                dataValueField: "ID",
                //optionLabel: "All",
                change: function (e) {
                    var filter = { logic: "or", filters: [] };
                    //  values is an array containing values to be searched
                    var values = this.value();
                    $.each(values, function (i, v) {
                        filter.filters.push({
                            field: "CustomerBranchID", operator: "eq", value: parseInt(v)
                        });
                    });
                    fCreateStoryBoard('dropdowntree', 'filtersstoryboard', 'loc')
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(filter);
                },
                dataSource: {
                    //severFiltering: true,
                    transport: {
                        read: {
                            url: "<% =Path%>GetAssignedEntitiesLocationsList?customerId=<% =CustId%>&userId=<% =UserId%>&profileID=<% =ProfileID%>",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }               
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        },
                       
                        model: {
                            children: "Children"
                        }                        
                    }
                }
            });
        });

        $(document).ready(function()
        {
            var customerID =<% =CustId%>;
            $("#grid").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: "<% =Path%>GetAll_NewRegistrationLicenseDetails?customerID=<% =CustId%>&userID=<% =UserId%>&profileID=<% =ProfileID%>",
                            type: 'GET',
                            //data: function () {
                            //    return {
                            //        customerID: customerID
                            //    };
                            //},
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },

                            dataType: 'json'
                        }                       
                    },
                    pageSize:10,
                    schema: {
                        data: function (response) {                           
                            return response.Result;
                        },
                        total: function (response) {
                            return response.Result.length;
                        },
                    }
                },
                height: 380,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: true,
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,

                pageable: {
                    refresh: true,
                    //pageSizes: true,
                    buttonCount: 3
                },
                columns: [
                    //
                      {
                          title: "Sr.No",
                          field: "rowNumber",
                          template: "<span class='row-number'></span>",
                          width: 80,
                          attributes: {
                              style: 'white-space: nowrap;'

                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      }, 
                          {
                              field: "OA_ClientID",
                              title: "Entity",
                              width: 100,
                              attributes: {
                                  style: 'white-space: nowrap;'

                              },
                              filterable: {
                                  extra: false,
                                  operators: {
                                      string: {
                                          eq: "Is equal to",
                                          neq: "Is not equal to",
                                          contains: "Contains"
                                      }
                                  }
                              }
                          },
                      {
                          hidden:true,
                          title: "OA_ActivityID",
                          field: "OA_ActivityID",
                          //template: "<span class='row-number'></span>",
                          width: 70,
                          attributes: {
                              style: 'white-space: nowrap;'

                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      }, 
                      {
                          field: "SM_Name",
                          title: "State",
                          width: 100,
                          attributes: {
                              style: 'white-space: nowrap;'

                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                      {
                          field: "LM_Name",
                          title: "Location",
                          width: 120,
                          attributes: {
                              style: 'white-space: nowrap;'

                          },
                          filterable: {
                              extra: false,
                              operators: {
                                  string: {
                                      eq: "Is equal to",
                                      neq: "Is not equal to",
                                      contains: "Contains"
                                  }
                              }
                          }
                      },
                    {
                        field: "OA_Branch",
                        title: "Branch",
                        width: 100,
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                  
                    {
                        field: "CM_ActivityName",
                        title: "Type",
                        width: 100,
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                     {
                         field: "OA_ActivityDate",
                         title: "Request Receive Date",
                         width: 130,
                         type: "date",
                         format: "{0:dd-MMM-yyyy}",
                         attributes: {
                             style: 'white-space: nowrap;'

                         },
                         filterable: {
                             extra: false,
                             operators: {
                                 string: {
                                     eq: "Is equal to",
                                     neq: "Is not equal to",
                                     contains: "Contains"
                                 }
                             }
                         }
                     },
                    {
                        field: "OA_ClosedDate",
                        title: "Close Date",
                        width: 130,
                        type: "date",
                        format: "{0:dd-MMM-yyyy}",
                    },
                    {
                        field: "StatusName",
                        title: "Status",
                        width: 100,
                        attributes: {
                            style: 'white-space: nowrap;'

                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                     {// template: kendo.template('<span class="file-icon img-file"></span>').html()
                         command: [
                         { name:"editLable", text: "No Document" },
                             { name: "edit1", text: "", iconClass: "k-icon k-i-download", className: "ob-download" },
                              { name: "edit2", text: "", iconClass: "k-icon k-i-eye", className: "ob-overview" }
                         ], title: "Action", lock: true,// width: 150,

                     }
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1 
                        + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });


                         //Selects all delete buttons
                    $("#grid tbody tr .k-grid-edit1").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }                    
                    });
                    $("#grid tbody tr .k-grid-edit2").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Not Available') {
                            $(this).remove();
                        }                    
                    });


                      $("#grid tbody tr .k-grid-editLable").each(function () {
                        var currentDataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                        //Check in the current dataItem if the row is deletable
                        if (currentDataItem.DocStatus == 'Document Available') {
                            $(this).remove();
                        }                    
                    });
                }
            });
            $("#grid").kendoTooltip({
              
                filter: "td:nth-child(2)", //this filter selects the second column's cells
                position: "down",
                width:100,
               
                content: function (e) {
                    var content = e.target.context.textContent;
                    return content;
                }
            }).data("kendoTooltip");
        });
        $(document).ready(function()
        {
            $(document).ready(function() {
                $("#dropdownActivity").kendoDropDownList({
                    dataTextField: "CM_ActivityName",
                    dataValueField: "CM_ID",
                    optionLabel: "All Activities",
                    change: function (e) {
                        debugger;
                        //var filter = { logic: "or", filters: [] };
                        //  values is an array containing values to be searched
                        var values = this.value();
                        if (values!="" && values!=null) {
                            var filter = { logic: "or", filters: [] };                      
                            var values = this.value();    
                      
                            filter.filters.push({
                                field: "OA_ActivityID", operator: "eq", value: values
                              
                            });
                            var dataSource = $("#grid").data("kendoGrid").dataSource;
                            dataSource.filter(filter);
                        }
                        else
                        {
                            $("#grid").data("kendoGrid").dataSource.filter({});
                        }
                    },
                    dataSource: {
                        transport: {
                            read: {
                                url: "<% =Path%>GetAll_OneTimeActivityMaster",
                                beforeSend: function (request) {
                                    request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    request.setRequestHeader('Content-Type', 'application/json');
                                },
                                dataType: 'json',
                            }                           
                        },
                        schema: {
                            data: function (response) {                      
                                return response.Result;
                            }
                        }
                    }
                });
            });
        });
        function ClearAllFilterMain(e) {
            $("#dropdowntree").data("kendoDropDownTree").value([]);
           
            $('#ClearfilterMain').css('display', 'none');            
        }
        function CheckFilterClearorNotMain() {
            if (($($($('#dropdowntree').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#ddlMonth').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) &&
                ($($($('#ddlYear').parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0)) {
                $('#ClearfilterMain').css('display', 'none');
            }
        }
        function fCreateStoryBoard(Id, div, filtername) {
            var IdKReset = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('id');
            $('#' + div).html('');
            debugger;
            $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '28px');
            $('#' + div).css('display', 'block');

            $('#Clearfilter').css('display', 'none');

            if (div == 'filtersstoryboard') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');//Dashboard
                $('#ClearfilterMain').css('display', 'block');
            }
            else if (div == 'filtertype') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
                //  $('#' + div).append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
            }
            else if (div == 'filterrisk') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterstatus') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');//Dashboard
            }
            else if (div == 'filterpstData1') {
                $('#' + div).append('Time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCategory') {
                $('#' + div).append('Category&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterAct') {
                $('#' + div).append('ACT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompSubType') {
                $('#' + div).append('SubType&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterCompType') {
                $('#' + div).append('type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtersstoryboard1') {
                $('#' + div).append('Location&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filtertype1') {
                $('#' + div).append('Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');                
            }
            else if (div == 'filterrisk1') {
                $('#' + div).append('Risk&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterFY') {
                $('#' + div).append('FY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }
            else if (div == 'filterUser') {
                $('#' + div).append('User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }                
            else if (div == 'filterstatus1') {
                $('#' + div).append('Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:');
                $('#Clearfilter').css('display', 'block');
            }

            for (var i = 0; i < $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length; i++) {
                var button = $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button')[i];
                $(button).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).attr('placeholder', 'Shree');
                var buttontest = $($(button).find('span')[0]).text();
                $('#' + div).append('<li class="k-button " unselectable="on" Id="ddl' + filtername + [i] + '" role="option" style="background-color:#1fd9e1; height: 20px;Color:white;white-space: nowrap;"><span unselectable="on">' + buttontest + '</span><span data-K-Id="ddl' + filtername + [i] + '" data-Id="' + IdKReset + '" data-seq="' + i + '" onclick="fcloseStory(this)" title="delete" aria-label="delete" class="k-select"><span data-Id="' + IdKReset + '" data-seq="' + i + '"  onclick="fcloseStory(this)"  class="k-icon k-i-close"></span></span></li>');
            }
            if ($($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).find('.k-button').length == 0) {
                // debugger;
                $('#' + div).css('display', 'none');
                $($($('#' + Id).parent('.k-widget.k-dropdowntree.k-dropdowntree-clearable')).find('.k-reset')).css('height', '0px');
            }
            CheckFilterClearorNotMain();
        }
        //});
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">

        <div class="col-md-12 alert alert-success" runat="server" id="divErrormsgReg" visible="false">
            <asp:Label runat="server" ID="lblerroemsg" Text="<% =msg%>"></asp:Label>
        </div>
    </div>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-3 colpadding0">
                <input id="dropdowntree" data-placeholder="Entity/Sub-Entity/Location" style="width: 98%;" />
            </div>

            <div class="col-md-3 colpadding0">
                <input id="dropdownActivity" style="width: 98%" />
            </div>
       
        <div class="col-md-4">
        </div>
        <div class="col-md-2 colpadding0">
             <button id="ClearfilterMain" style="float: right; margin-right: 1%; display:none" onclick="ClearAllFilterMain(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear Filter</button>
        </div>
    </div>
        </div>
    <div id="grid" style="border: none; margin-top: 5px">
        <div class="k-header k-grid-toolbar">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none;" id="filtersstoryboard">&nbsp;</div>
        </div>
    </div>

    <%--DownLoad ModalPOPup--%>
    <div class="modal fade" id="divDownloadView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 360px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearDV();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="DownloadViews" src="about:blank" width="300px" height="150px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%--View Modalpopup--%>
    <div>
        <div class="modal fade" id="divViewDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog" style="width: 70%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="height: 570px;">
                        <div style="width: 100%;">
                            <div class="col-md-12">
                                <asp:Label runat="server" ID="lblMessage" Style="color: red;"></asp:Label>
                                <fieldset style="height: 550px; width: 100%;">
                                    <iframe src="about:blank" id="OverViews" width="100%" height="100%" frameborder="0"></iframe>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
