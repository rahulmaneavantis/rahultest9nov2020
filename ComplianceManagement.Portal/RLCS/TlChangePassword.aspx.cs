﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Threading;
using Logger;
using System.Reflection;
using System.Web.Security;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class TlChangePassword : System.Web.UI.Page
    {
        protected static string CId;
        protected static string UId;
        protected static string avacomAPIUrl;
        protected static string TLConnectAPIUrl;

        protected static string userProfileID;
        protected static string userProfileID_Encrypted;
        protected static string authKey;
        protected static string ProfileID;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                    TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                    hdnApiPath.Value = TLConnectAPIUrl;

                    CId = Convert.ToString(AuthenticationHelper.CustomerID);
                    UId = Convert.ToString(AuthenticationHelper.UserID);
                    ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
                
                    string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];

                    if (!string.IsNullOrEmpty(TLConnectKey))
                    {
                        userProfileID = string.Empty;
                        userProfileID = AuthenticationHelper.ProfileID;

                        if (!string.IsNullOrEmpty(userProfileID))
                        {
                            userProfileID_Encrypted = CryptographyHandler.encrypt(userProfileID.Trim(), TLConnectKey);
                        }
                         //hdnProfileID.Value = userProfileID_Encrypted;
                        hdnProfileID.Value = ProfileID;
                        authKey = AuthenticationHelper.AuthKey;
                        hdnAuthKey.Value = authKey;
                    }
                }
            }
        }

        protected void btnSavePassword_Click(object sender, EventArgs e)
        {
            try
            {
                int userID = -1;
                if (AuthenticationHelper.UserID != -1)
                {
                    userID = AuthenticationHelper.UserID;
                }
                else
                {
                    if (Convert.ToString(Session["userID"]) != null)
                    {
                        if (Convert.ToString(Session["userID"]) != "")
                        {
                            userID = Convert.ToInt32(Session["userID"]);
                        }
                    }
                }
                if (userID != -1)
                {
                    if (hdnOldPasswrdmatch.Value == "1")
                    {
                        #region If EmailID 
                        RLCS_User_Mapping User = new RLCS_User_Mapping();                        
                        string passwordText = txtNewPassword.Text.Trim();
                        User.Password = Util.CalculateAESHash(passwordText);
                        User.AVACOM_UserID = userID;
                        bool result = RLCSManagement.ChangePassword(User);

                        if (result)
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Password reset successfully.";
                            FormsAuthentication.SignOut();
                            Session.Abandon();

                            Response.Redirect("https://tlconnect.teamlease.com", false);
                            //FormsAuthentication.RedirectToLoginPage();
                        }
                        else
                        {
                            CustomValidator1.IsValid = false;
                            CustomValidator1.ErrorMessage = "Something went wrong, Please try again.";
                        }

                        btnClear_Click(null, null);

                        #endregion
                    }
                }
                else
                {
                    btnClear_Click(null, null);

                    CustomValidator1.IsValid = false;
                    CustomValidator1.ErrorMessage = "New Password can not be same as Old Password. Please enter different password.";                
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Something went wrong. Please try again.";
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtOldPassword.Text = string.Empty;
                txtNewPassword.Text = string.Empty;
                txtConfirmPassword.Text = string.Empty;
                txtanswer.Text = string.Empty;
                // lblmsg.Text = string.Empty;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void lnklogin_Click(object sender, EventArgs e)
        {
            try
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                Response.Redirect("https://tlconnect.teamlease.com", false);
                //FormsAuthentication.RedirectToLoginPage();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                CustomValidator1.IsValid = false;
                CustomValidator1.ErrorMessage = "Something went wrong. Please try again.";
            }
        }
    }
}