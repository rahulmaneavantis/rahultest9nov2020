﻿<%@ Page Title="My Notices" Language="C#" MasterPageFile="~/RLCSCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_NoticeList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_NoticeList" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">

        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 10);
        window.onunload = function () { };

        $(document).ready(function () {
            setactivemenu('leftnoticesmenu');
            fhead('My Notices');
        });

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tvFilterLocation.ClientID %>').unbind('click');

                $('<%= tvFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });


        //$(document).on.click(function (event) {
        //    if (event.target.id == "") {
        //        var idvid = $(event.target).closest('div');
        //        if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('tvFilterLocation') > -1) {
        //            $("#divFilterLocation").show();
        //        } else {
        //            $("#divFilterLocation").hide();
        //        }
        //    }
        //    else if (event.target.id != "ContentPlaceHolder1_tbxFilterLocation") {
        //        $("#divFilterLocation").hide();
        //    } else if (event.target.id != "" && event.target.id.indexOf('tvFilterLocation') > -1) {
        //        $("#divFilterLocation").show();
        //    } else if (event.target.id == "ContentPlaceHolder1_tbxFilterLocation") {
        //        $("#ContentPlaceHolder1_tbxFilterLocation").unbind('click');

        //        $("#ContentPlaceHolder1_tbxFilterLocation").click(function () {
        //            $("#divFilterLocation").toggle("blind", null, 500, function () { });
        //        });

        //    }
        //});

        <%--$(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                    $("#divFilterLocation").show();
                } else {
                    $("#divFilterLocation").hide();
                }
            } else if (event.target.id != '<%= tbxFilterLocation.ClientID %>') {
                $("#divFilterLocation").hide();
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocation.ClientID %>') > -1) {
                $("#divFilterLocation").show();
            } else if (event.target.id == '<%= tbxFilterLocation.ClientID %>') {
                $('<%= tbxFilterLocation.ClientID %>').unbind('click');

                $('<%= tbxFilterLocation.ClientID %>').click(function () {
                    $("#divFilterLocation").toggle("blind", null, 500, function () { });
                });
            }
        });--%>

function hideDivBranch() {
    $('#divBranches').hide("blind", null, 500, function () { });
}

function openNoticeModal() {
    $('#divAddNoticeModal').modal('show');
}

function closeNoticeModal() {
    document.getElementById('<%= lnkBtnBindGrid.ClientID %>').click();
}

function openAddNewActModal() {
    $('#divAddNewActModal').modal('show');
}

function ShowDialog(NoticeInstanceID) {
    $('#divShowDialog').modal('show');
    $('.modal-dialog').css('width', '95%');
    $('#showdetails').attr('width', '100%');
    $('#showdetails').attr('height', '550px');
    //$('#showdetails').attr('src', "/Litigation/aspxPages/NoticeDetailPage.aspx?AccessID=" + NoticeInstanceID);
    $('#showdetails').attr('src', "/RLCS/RLCS_NoticeDetailPage.aspx?AccessID=" + NoticeInstanceID);
};

function ClosePopNoticeDetialPage() {
    $('#divShowDialog').modal('hide');
}
    </script>

    <style type="text/css">
        .customDropDownCheckBoxCSS {
            height: 32px !important;
            width: 70%;
        }
            .AddNewspan1 {
    padding: 5px;
    border: 2px solid #f4f0f0;
    border-radius: 51px;
    display: inline-block;
    height: 26px;
    width: 26px;
}
    </style>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row Dashboard-white-widget">
        <div class="dashboard">

            <div class="col-md-12 colpadding0">
                <asp:ValidationSummary ID="ValidationSummary2" runat="server" Display="none" class="alert alert-block alert-danger fade in" ValidationGroup="DepartmentPageValidationGroup" />
                <asp:CustomValidator ID="cvErrorNoticePage" runat="server" EnableClientScript="False"
                    ValidationGroup="NoticePageValidationGroup" Display="none" class="alert alert-block alert-danger fade in" />
            </div>

            <div class="col-md-12 colpadding0">
                <%--<div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 10%;">
                    <div class="col-md-2 colpadding0" style="width: 33%;">
                        <p style="color: #999; margin-top: 5px;">Show </p>
                    </div>                    
                </div>--%>

                <asp:UpdatePanel ID="upDivLocation" runat="server" UpdateMode="Conditional" OnLoad="upDivLocation_Load">
                    <ContentTemplate>
                        <div class="col-md-4 colpadding0">
                            <asp:TextBox runat="server" ID="tbxFilterLocation" PlaceHolder="Select Entity/State/Location/Branch" autocomplete="off" 
                                CssClass="clsDropDownTextBox" />
                            <div style="margin-left: 1px; position: absolute; z-index: 10; overflow-y: auto; height: 200px; width: 95%" id="divFilterLocation">
                                <asp:TreeView runat="server" ID="tvFilterLocation" SelectedNodeStyle-Font-Bold="true" NodeStyle-ForeColor="#8e8e93"
                                    Style="margin-top: -5%; overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                    OnSelectedNodeChanged="tvFilterLocation_SelectedNodeChanged">
                                </asp:TreeView>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="col-md-3 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlStatus" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Status" class="form-control" Width="90%" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                        <asp:ListItem Text="All" Value="A"></asp:ListItem>
                        <asp:ListItem Text="Pending" Value="P"></asp:ListItem>
                        <asp:ListItem Text="Completed" Value="C"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0">
                    <asp:DropDownListChosen runat="server" ID="ddlNoticeTypePage" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        DataPlaceHolder="Select Type" class="form-control" Width="90%">
                        <asp:ListItem Text="All" Value="B"></asp:ListItem>
                        <asp:ListItem Text="Inward/Defendant" Value="I"></asp:ListItem>
                        <asp:ListItem Text="Outward/Plaintiff" Value="O"></asp:ListItem>
                    </asp:DropDownListChosen>
                </div>

                <div class="col-md-3 colpadding0" style="display:none; margin-top: 5px; width: 15%;">
                    <asp:DropDownListChosen runat="server" ID="ddlPartyPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Opponent" class="form-control" Width="90%" />
                </div>

                <div class="col-md-3 colpadding0" style="display:none; margin-top: 5px; width: 16%;">
                    <asp:DropDownListChosen runat="server" ID="ddlDeptPage" AllowSingleDeselect="false" DisableSearchThreshold="3"
                        DataPlaceHolder="Select Department" class="form-control" Width="90%" />
                </div>               

                <div class="col-md-2 colpadding0">
                    <asp:LinkButton Text="Apply" CssClass="btn btn-primary" runat="server" ID="lnkBtnApplyFilter" OnClick="lnkBtnApplyFilter_Click"
                         Style="float: right;" />
                </div>

                <div class="col-md-3 colpadding0 entrycount" style="display:none; margin-top: 5px; width: 18%;">
                    <asp:DropDownListChosen runat="server" ID="ddlType" DataPlaceHolder="Select Type" AllowSingleDeselect="false"
                        class="form-control m-bot15" Width="100%" Height="30px">
                    </asp:DropDownListChosen>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0" style="display:none; margin-top: 5px; width: 17%;">
                    <asp:DropDownListChosen runat="server" ID="ddlOwnerlist" DataPlaceHolder="Select Owner" AllowSingleDeselect="false" DisableSearchThreshold="5"
                        CssClass="form-control" Width="100%" />
                </div>

                <div class="col-md-3 colpadding0" style="display:none; margin-top: 5px; width: 23.5%; margin-left: 1%;">
                    <asp:TextBox runat="server" ID="tbxtypeTofilter" AutoComplete="off" placeholder="Type to Search" CssClass="form-control" />
                </div>                

                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 7%; float: right">
                    <asp:LinkButton Text="Add New" CssClass="btn btn-primary" runat="server" ID="btnAddNotice" Style="float: right"
                         OnClick="btnAddNotice_Click" data-toggle="tooltip" ToolTip="Add New Case" Visible="false">
                       
                         <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;New
                    </asp:LinkButton>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-3 colpadding0 entrycount" style="margin-top: 5px; width: 20%;">
                    <asp:LinkButton ID="lnkBtnBindGrid" OnClick="lnkBtnBindGrid_Click" Style="float: right; display: none;" Width="100%" runat="server">
                    </asp:LinkButton>
                </div>
            </div>

            <div class="col-md-12 colpadding0">
                <asp:GridView runat="server" ID="grdNoticeDetails" AutoGenerateColumns="false" AllowSorting="true" ShowHeaderWhenEmpty="true"
                    PageSize="10" AllowPaging="true" AutoPostBack="true" CssClass="table" GridLines="none" Width="100%" DataKeyNames="AVACOM_NoticeID"
                    OnRowCommand="grdNoticeDetails_RowCommand" OnRowCreated="grdNoticeDetails_RowCreated"> <%--OnSorting="grdNoticeDetails_Sorting"--%> 
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Sr" ItemStyle-Width="2%">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Entity" ItemStyle-Width="10%"> 
                            <ItemTemplate> 
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Entity") %>' ToolTip='<%# Eval("Entity") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="State" ItemStyle-Width="10%"> 
                            <ItemTemplate> 
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("SM_Name") %>' ToolTip='<%# Eval("SM_Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Location" ItemStyle-Width="10%"> 
                            <ItemTemplate> 
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("LM_Name") %>' ToolTip='<%# Eval("LM_Name") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Branch" ItemStyle-Width="10%"> 
                            <ItemTemplate> 
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("IN_Branch") %>' ToolTip='<%# Eval("IN_Branch") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Type" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 150px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("NoticeTitle") %>' ToolTip='<%# Eval("NoticeTitle") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Received Date" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom"
                                         Text='<%# Eval("IN_ReceivedDate") != null ? Convert.ToDateTime(Eval("IN_ReceivedDate")).ToString("dd-MM-yyyy") : "" %>'
                                        ToolTip='<%# Eval("IN_ReceivedDate") != null ? Convert.ToDateTime(Eval("IN_ReceivedDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Close Date" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" 
                                        Text='<%# Eval("IN_ClosedDate") != null ? Convert.ToDateTime(Eval("IN_ClosedDate")).ToString("dd-MM-yyyy") : "" %>'
                                        ToolTip='<%# Eval("IN_ClosedDate") != null ? Convert.ToDateTime(Eval("IN_ClosedDate")).ToString("dd-MM-yyyy") : "" %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Status" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 75px">
                                    <asp:Label runat="server" data-toggle="tooltip" data-placement="bottom" 
                                        Text='<%# Eval("NoticeStatus") %>' ToolTip='<%# Eval("NoticeStatus") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEditNotice" runat="server" OnClick="btnChangeStatus_Click" ToolTip="View Notice Details" data-toggle="tooltip"
                                    CommandArgument='<%# Eval("AVACOM_NoticeID") %>'>
                                     <img src='<%# ResolveUrl("~/Images/edit_icon_new.png")%>' alt="View" /></asp:LinkButton>
                                <%--CommandName="EDIT_Notice" OnClientClick="openNoticeModal()"--%>
                                <asp:LinkButton ID="lnkDeleteNotice" runat="server" CommandName="DELETE_Notice" ToolTip="Delete Notice" data-toggle="tooltip" Visible="false"
                                    CommandArgument='<%# Eval("AVACOM_NoticeID") %>' OnClientClick="return confirm('Are you certain you want to Delete this Notice Details?');">
                                    <img src='<%# ResolveUrl("~/Images/delete_icon_new.png")%>' alt="Delete Notice" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="clsROWgrid" />
                    <HeaderStyle CssClass="clsheadergrid" />
                    <PagerSettings Visible="false" />
                    <PagerTemplate>
                    </PagerTemplate>
                    <EmptyDataTemplate>
                        No Record Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>

            <div class="col-md-12 colpadding0">
                <div class="col-md-8 colpadding0">
                    <div runat="server" id="DivRecordsScrum" style="float: left; margin-top: 5px; color: #999">
                        <p style="padding-right: 0px !Important;">
                            <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                            <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                </div>

                <div class="col-md-2 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlPageSize" class="form-control m-bot15" Style="width: 37%; float: right; height: 32px !important"
                        AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                        <asp:ListItem Text="5"/>
                        <asp:ListItem Text="10" Selected="True" />
                        <asp:ListItem Text="20" />
                        <asp:ListItem Text="50" />
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 colpadding0" style="float: right;">
                    <div style="float: left; width: 60%">
                        <p class="clsPageNo">Page</p>
                    </div>
                    <div style="float: left; width: 40%">
                        <asp:DropDownListChosen runat="server" ID="DropDownListPageNo" AutoPostBack="true" DataPlaceHolder="No" AllowSingleDeselect="false"
                            OnSelectedIndexChanged="DropDownListPageNo_SelectedIndexChanged" class="form-control m-bot15" Width="100%" Height="30px">
                        </asp:DropDownListChosen>
                    </div>
                </div>

                <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
            </div>
        </div>
        <%--</section>--%>
    </div>

    <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>

    <div class="modal fade" id="divShowDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #f7f7f7; height: 30px;">
                    <label style="width: 10px; display: block; float: left; font-size: 13px; color: red;">&nbsp;</label>
                    <label style="width: 210px; display: block; float: left; font-size: 20px; color: #1fd9e1;">
                        Add/Edit Notice Details</label>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeNoticeModal();">&times;</button>
                </div>

                <div class="modal-body" style="background-color: #f7f7f7;">
                    <iframe id="showdetails" src="about:blank" width="95%" height="80%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
