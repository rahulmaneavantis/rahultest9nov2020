﻿<%@ Page Title="My Compliance Reports" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_MyComplianceReports.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_MyComplianceReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {

            function initializeRadioButtonsList(controlID) {
                $(controlID).buttonset();
            }

            var startDate = new Date();
            $(function () {
                $('input[id*=txtStartDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                        maxDate: startDate,
                    });
            });

            $(function () {
                $('input[id*=txtEndDate]').datepicker(
                    {
                        dateFormat: 'dd-mm-yy',
                    });
            });


        });

    </script>

    <style type="text/css">
        .btnss {
            background-image: url(../Images/edit_icon_new.png);
            border: 0px;
            width: 24px;
            height: 24px;
            background-color: transparent;
        }

        .table > thead > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > tbody > tr > th {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table > thead > tr > th > a {
            vertical-align: bottom;
            border-bottom: 2px solid #dddddd;
        }

        .table tr th {
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
        }

        .clsheadergrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: bottom !important;
            border-bottom: 2px solid #dddddd !important;
            color: #666666;
            font-size: 15px;
            font-weight: normal;
            font-family: 'Roboto',sans-serif;
            text-align: left;
        }

        .clsROWgrid {
            padding: 8px !important;
            line-height: 1.428571429 !important;
            vertical-align: top !important;
            border-top: 1px solid #dddddd !important;
            color: #666666 !important;
            font-size: 14px !important;
            font-family: 'Roboto', sans-serif !important;
        }
    </style>

    <script type="text/javascript">
        function setTabActive(id) {
            $('.dummyval').removeClass("active");
            $('#' + id).addClass("active");
        };

        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function hidetree() {
            //if (event.target.value != "Location") {
            //    $('#ContentPlaceHolder1_udcCannedReportPerformer_divFilterLocationPerformer').hide();
            //} 
        }
    </script>

    <script type="text/javascript">

        function OpenOverViewpupPerformer(scheduledonid, instanceid) {
            $('#divOverViewPerformer').modal('show');
            $('#OverViewsPerformer').attr('width', '1150px');
            $('#OverViewsPerformer').attr('height', '600px');
            $('.modal-dialog').css('width', '1200px');
            $('#OverViewsPerformer').attr('src', "../RLCS/RLCS_ComplianceOverview.aspx?ComplianceScheduleID=" + scheduledonid + "&ComplainceInstatnceID=" + instanceid);
        }

        function ClosePopup() {
            $('#divOverViewPerformer').modal('hide');
        }

        function hideDivBranch() {
            $('#divFilterLocationPerformer').hide("blind", null, 500, function () { });
        }
    </script>

    <script type="text/javascript">

        function fComplianceOverviewPerformer(obj) {
            OpenOverViewpupPerformer($(obj).attr('scheduledonid'), $(obj).attr('instanceid'));
        }

        function hidediv() {
            var div = document.getElementById('AdvanceSearch');
            div.style.display == "none" ? "block" : "none";
            $('.modal-backdrop').hide();
            return true;
        }

        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvFilterLocationPerformer.ClientID %>') > -1) {
                    $("#divFilterLocationPerformer").show();
                } else {
                    $("#divFilterLocationPerformer").hide();
                }<%-- event.target.id != '<%= tbxFilterLocation.ClientID %>'--%>
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvFilterLocationPerformer.ClientID %>') > -1) {
                $("#divFilterLocationPerformer").show();
            } else if (event.target.id != '<%= tvFilterLocationPerformer.ClientID %>') {
                $("#divFilterLocationPerformer").hide();
            } else if (event.target.id == '<%= tvFilterLocationPerformer.ClientID %>') {
                $('<%= tvFilterLocationPerformer.ClientID %>').unbind('click');

                $('<%= tvFilterLocationPerformer.ClientID %>').click(function () {
                    $("#divFilterLocationPerformer").toggle("blind", null, 500, function () { });
                });
            }
        });




function myFunction() {
    $('#divFilterLocationPerformer').show();
}

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upCaseDocUploadPopup" runat="server">
        <ContentTemplate>
            <div class="row Dashboard-white-widget" style="padding: 5px 10px 10px;">
                <div class="panel-body" style="padding: 0px;">
                    <div class="tab-content">
                        <div runat="server" id="performerdocuments" class="tab-pane active">
                            <div style="float: right; margin-right: 10px; margin-top: -35px; display: none">
                                <asp:DataList runat="server" ID="dlFilters" RepeatDirection="Horizontal" OnSelectedIndexChanged="dlFilters_SelectedIndexChanged"
                                    DataKeyField="ID">
                                    <SeparatorTemplate>
                                        <span style="margin: 0 5px 0 5px">|</span>
                                    </SeparatorTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select" Style="text-decoration: none; color: Black">  
                                    <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Font-Names="Tahoma" Font-Size="13px" VerticalAlign="Middle" />
                                    <SelectedItemStyle Font-Names="Tahoma" Font-Size="13px" Font-Bold="True" VerticalAlign="Middle"
                                        Font-Underline="true" />
                                </asp:DataList>
                                <%--<asp:Button ID="btnCheck" OnClick="btnCheck_Click" Visible ="false" runat="server" Text="" />--%>
                            </div>

                            <div class="col-md-12 colpadding0">
                                <asp:ValidationSummary ID="vsDocGen" runat="server" class="alert alert-block alert-danger fade in" ValidationGroup="comReportValidationGroup" />
                                <asp:CustomValidator ID="cvDuplicateEntry" runat="server" EnableClientScript="False"
                                    ValidationGroup="comReportValidationGroup" Display="None" />
                            </div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-1 colpadding0">
                                    <div class="col-md-3 colpadding0" style="float: left;">
                                        <asp:DropDownList runat="server" ID="ddlpageSize" AutoPostBack="true" OnSelectedIndexChanged="ddlpageSize_SelectedIndexChanged" class="form-control m-bot15" Style="width: 70px; float: left">
                                            <asp:ListItem Text="5" Selected="True" />
                                            <asp:ListItem Text="10" />
                                            <asp:ListItem Text="20" />
                                            <asp:ListItem Text="50" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-4 colpadding0" id="divloc">
                                    <asp:TextBox runat="server" AutoComplete="off" ID="tbxFilterLocationPerformer" onfocus="myFunction()"
                                        Style="padding: 0px; padding-left: 10px; margin: 0px; height: 35px; width: 95%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93"
                                        CssClass="txtbox" />
                                    <div style="margin-left: 1px; position: absolute; z-index: 10; width: 90%; margin-top: -20px;" id="divFilterLocationPerformer">
                                        <asp:TreeView runat="server" ID="tvFilterLocationPerformer" SelectedNodeStyle-Font-Bold="true" Width="100%" NodeStyle-ForeColor="#8e8e93"
                                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important;" ShowLines="true"
                                            OnSelectedNodeChanged="tvFilterLocationPerformer_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </div>

                                <div class="col-md-2 colpadding0">
                                    <asp:DropDownListChosen runat="server" ID="ddlRiskType" class="form-control" Width="90%">
                                        <asp:ListItem Text="Risk" Value="-1" />
                                        <asp:ListItem Text="High" Value="0" />
                                        <asp:ListItem Text="Medium" Value="1" />
                                        <asp:ListItem Text="Low" Value="2" />
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0">
                                    <asp:DropDownListChosen runat="server" ID="ddlStatus" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                                        class="form-control" Width="90%">
                                    </asp:DropDownListChosen>
                                </div>

                                <div class="col-md-2 colpadding0" style="display: none">
                                    <asp:DropDownListChosen runat="server" ID="ddlComplianceType" Width="90%"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlComplianceType_SelectedIndexChanged" class="form-control">
                                        <asp:ListItem Text="Statutory" Value="-1" />
                                        <%-- <asp:ListItem Text="Internal" Value="0" />
                                             <asp:ListItem Text="Event Based" Value="1" />--%>
                                        <asp:ListItem Text="Statutory CheckList" Value="2" />
                                        <%--<asp:ListItem Text="Internal CheckList" Value="3" />--%>
                                    </asp:DropDownListChosen>
                                </div>
                                <%-- <div class="col-md-2 colpadding0"></div>--%>

                                <div class="col-md-1 colpadding0">
                                    <asp:Button ID="btnSearch" CausesValidation="false" class="btn btn-primary" OnClick="btnSearch_Click" runat="server" Text="Apply" />
                                </div>

                                <div class="col-md-2 colpadding0">
                                    <a class="btn btn-advanceSearch" data-toggle="modal" href="#AdvanceSearch" title="Search">Advanced Search</a>
                                    <asp:LinkButton runat="server" CssClass="btn" ID="lbtnExportExcel"
                                        Style="background: url('../images/Excel _icon.png'); float: right; width: 47px; height: 33px;"
                                        toggle="tooltip" data-placement="bottom" Title="Export All to Excel" ToolTip="Export All to Excel" OnClick="lbtnExportExcel_Click">          
                                    </asp:LinkButton>
                                </div>

                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12 colpadding0" style="text-align: right; float: right">
                                <div id="divEvent" class="col-md-10 colpadding0" style="text-align: right; float: right" runat="server" visible="false">
                                    <div style="float: left; margin-right: 2%;">
                                        <asp:DropDownList runat="server" ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" AutoPostBack="true" class="form-control m-bot15" Style="width: 255px;">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="float: left; margin-right: 2%;">
                                        <asp:DropDownList runat="server" ID="ddlEventNature" class="form-control m-bot15" Style="width: 255px;">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 AdvanceSearchScrum">
                                <div id="divAdvSearch" runat="server" visible="false">
                                    <p>
                                        <asp:Label ID="lblAdvanceSearchScrum" runat="server" Text=""></asp:Label>
                                    </p>
                                    <p>
                                        <asp:LinkButton ID="lnkClearAdvanceList" OnClick="lnkClearAdvanceSearch_Click" runat="server">Clear Advanced Search</asp:LinkButton>
                                    </p>
                                </div>

                                <div runat="server" id="DivRecordsScrum" style="float: right;">
                                    <p style="padding-right: 0px !Important;">
                                        <asp:Label ID="Label4" runat="server" Text="Showing "></asp:Label>
                                        <asp:Label ID="lblStartRecord" Font-Bold="true" runat="server" Text=""></asp:Label>- 
                        <asp:Label ID="lblEndRecord" Font-Bold="true" runat="server" Text=""></asp:Label>of 
                        <asp:Label ID="lblTotalRecord" Font-Bold="true" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-12 colpadding0">
                                <asp:GridView runat="server" ID="grdComplianceTransactions" AutoGenerateColumns="false" CssClass="table" ShowHeaderWhenEmpty="true"
                                    OnRowCreated="grdComplianceTransactions_RowCreated" GridLines="none" OnRowCommand="grdComplianceTransactions_RowCommand"
                                    AllowPaging="True" PageSize="5" OnSorting="grdComplianceTransactions_Sorting" BorderWidth="0px" OnRowDataBound="grdComplianceTransactions_RowDataBound"
                                    DataKeyNames="ComplianceInstanceID" OnPageIndexChanging="grdComplianceTransactions_PageIndexChanging">
                                    <HeaderStyle CssClass="clsheadergrid" />
                                    <RowStyle CssClass="clsROWgrid" />
                                    <Columns>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Sr">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblBranch" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("Branch") %>' ToolTip='<%# Eval("Branch") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Compliance">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 300px;">
                                                    <asp:Label ID="lblShortDesc" runat="server" data-toggle="tooltip" data-placement="bottom" Text='<%# Eval("ShortForm") %>' ToolTip='<%# Eval("ShortDescription") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Form">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 80px;">
                                                    <asp:Label ID="lblRequiredForms" runat="server" data-toggle="tooltip"
                                                        data-placement="bottom" Text='<%# Eval("RequiredForms") %>' ToolTip='<%# Eval("RequiredForms") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Frequency">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 90px;">
                                                    <asp:Label ID="lblFrequency" runat="server" data-toggle="tooltip"
                                                        data-placement="bottom" Text='<%# Eval("TextFrequency") %>' ToolTip='<%# Eval("TextFrequency") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reviewer" Visible="false">
                                            <ItemTemplate>
                                                <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100px;">
                                                    <asp:Label ID="lblReviewer" runat="server" Text='<%# GetReviewer((long)Eval("ComplianceInstanceID")) %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="ForMonth" HeaderText="Period" />

                                        <asp:TemplateField HeaderText="Due Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScheduledOn" runat="server" Text='<%# Convert.ToDateTime(Eval("ScheduledOn")).ToString("dd-MMM-yyyy") %>'></asp:Label>
                                                <asp:Label ID="lblScheduledOnID" runat="server" Text='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="Status" HeaderText="Status" />

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkbtnHistory" runat="server" CommandName="View_History" Text="View_History" CommandArgument='<%# Eval("ScheduledOnID") %>' Visible="false"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:BoundField DataField="Name" Visible="false" />
                <asp:BoundField DataField="Risk" Visible="false" />
                <asp:BoundField DataField="ComplianceStatusID" Visible="false" />
                <asp:BoundField DataField="ScheduledOnID" Visible="false" />
                <asp:BoundField DataField="ComplianceInstanceID" Visible="false" />
                <asp:BoundField DataField="EventName" Visible="false"/>
                <asp:BoundField DataField="EventNature" Visible="false"/>--%>
                                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:UpdatePanel ID="upDownloadFileperformer" runat="server">
                                                    <ContentTemplate>
                                                        <asp:ImageButton ID="lblOverView3" runat="server" ImageUrl="~/Images/Eye.png" ScheduledOnID='<%# Eval("ScheduledOnID")%>' instanceId='<%#Eval("ComplianceInstanceID")  %>'
                                                            OnClientClick='fComplianceOverviewPerformer(this)' ToolTip="Overview"></asp:ImageButton>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="lblOverView3" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" />
                                    <PagerTemplate>
                                        <table style="display: none">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="ph" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </PagerTemplate>
                                    <EmptyDataTemplate>
                                        No Records Found
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div class="col-md-12 colpadding0">
                                <div class="col-md-6 colpadding0">
                                </div>
                                <div class="col-md-6 colpadding0">
                                    <div class="table-paging">
                                        <asp:ImageButton ID="lBPrevious" CssClass="table-paging-left" runat="server" ImageUrl="~/img/paging-left-active.png" OnClick="Previous_Click" />

                                        <div class="table-paging-text">
                                            <p>
                                                <asp:Label ID="SelectedPageNo" runat="server" Text=""></asp:Label>/
                        <asp:Label ID="lTotalCount" runat="server" Text=""></asp:Label>
                                            </p>
                                        </div>
                                        <asp:ImageButton ID="lBNext" CssClass="table-paging-right" runat="server" ImageUrl="~/img/paging-right-active.png" OnClick="Next_Click" />
                                    </div>
                                </div>
                            </div>

                            <asp:HiddenField ID="TotalRows" runat="server" Value="0" />
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExportExcel" />
        </Triggers>
    </asp:UpdatePanel>

    <!--advance search starts-->
    <div class="modal fade" id="AdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" style="width: 70%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <h3 style="text-align: center; margin-top: 10px; font-weight: bold;">Advanced Search</h3>

                    <div class="col-md-12 colpadding0">
                        <div class="col-md-3" style="margin-bottom: 10px;">
                            <asp:DropDownListChosen runat="server" ID="ddlType" class="form-control" Width="100%" DataPlaceHolder="Type">
                            </asp:DropDownListChosen>
                        </div>

                        <div class="col-md-3">
                            <asp:DropDownListChosen runat="server" ID="ddlCategory" class="form-control" Width="100%" DataPlaceHolder="Category">
                            </asp:DropDownListChosen>
                        </div>

                        <asp:Panel ID="PanelAct" runat="server" CssClass="col-md-6">
                            <div id="DivAct" runat="server" class="table-advanceSearch-selectOpt">
                                <asp:DropDownListChosen runat="server" ID="ddlAct" class="form-control" Width="210%" DataPlaceHolder="Act">
                                </asp:DropDownListChosen>
                            </div>
                        </asp:Panel>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-12 colpadding0">
                        <asp:Panel ID="Panelsubtype" runat="server" CssClass="col-md-3">
                            <div id="DivComplianceSubTypeList" runat="server">
                                <asp:DropDownListChosen runat="server" ID="ddlComplianceSubType" class="form-control" Width="100%" DataPlaceHolder="Sub Type">
                                </asp:DropDownListChosen>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="PanelSearchType" runat="server" CssClass="col-md-3">
                            <div id="Div2" runat="server">
                                <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="Type to Filter"
                                    class="form-group form-control" ID="txtSearchType"
                                    CssClass="form-control" onkeydown="return (event.keyCode!=13);" />
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="Panel1" runat="server" CssClass="col-md-3">
                            <div id="Div3" runat="server">
                                <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="From Date"
                                    ID="txtStartDate" CssClass="StartDate form-group form-control" />
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="Panel2" runat="server" CssClass="col-md-3">
                            <div id="Div4" runat="server">
                                <asp:TextBox runat="server" Style="padding-left: 7px;" placeholder="To Date"
                                    CssClass="StartDate form-group form-control" ID="txtEndDate" />
                            </div>
                        </asp:Panel>
                    </div>

                    <div class="clearfix" style="clear: both;"></div>

                    <div class="text-center">
                        <asp:Button ID="Button1" Text="Search" class="btn btn-primary"
                            OnClick="btnAdvSearch_Click" runat="server" OnClientClick="return hidediv();" />
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                    <br />

                </div>

            </div>
        </div>
    </div>
    <!--advance search ends-->

    <div id="divActDialog111">
        <%--<asp:UpdatePanel ID="UpDetailView" runat="server">
        <ContentTemplate>--%>

        <asp:GridView runat="server" ID="grdTransactionHistory" AutoGenerateColumns="false"
            GridLines="Both" CssClass="table"
            AllowPaging="True" PageSize="50"
            DataKeyNames="ComplianceTransactionID">
            <Columns>
                <asp:BoundField DataField="CreatedByText" HeaderText="Changed By" SortExpression="CreatedByText" />
                <asp:TemplateField HeaderText="Date" SortExpression="StatusChangedOn">
                    <ItemTemplate>
                        <%# Eval("Dated") != null ? Convert.ToDateTime(Eval("Dated")).ToString() : ""%>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks" />
                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" Font-Size="Medium" />
            <PagerSettings Position="Top" />
            <SelectedRowStyle BackColor="#EBC79E" Font-Bold="True" ForeColor="#603311" />
            <HeaderStyle CssClass="ui-widget-header" ForeColor="White"></HeaderStyle>
            <AlternatingRowStyle BackColor="#E6EFF7" />
            <EmptyDataRowStyle Font-Size="13px" HorizontalAlign="Center" />
            <EmptyDataTemplate>
                No Records Found.
            </EmptyDataTemplate>
        </asp:GridView>
        <%--   </ContentTemplate>
    </asp:UpdatePanel>--%>
    </div>

    <!--Overview Statutory Performer starts-->
    <div class="modal fade" id="divOverViewPerformer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog pt10 pb10">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -9px;" onclick="ClosePopup();">&times;</button>
                </div>

                <div class="modal-body">
                    <iframe id="OverViewsPerformer" src="about:blank" width="95%" height="auto" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--Overview Statutory Performer ends-->

    <script type="text/javascript">
        $(document).ready(function () {
            setactivemenu('leftreportsmenu');
            fhead('My Reports / Compliance Reports');
            if ($('.dummyval').length == 0) {

                $('#ContentPlaceHolder1_btnAdvSearch').css('margin-top', '0px');
                $('#ContentPlaceHolder1_btnAdvSearch').css('margin-bottom', '5px');
            }
        });
    </script>
</asp:Content>
