<%@ Page Title="Generate Document List" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="GenerateDocumentList.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.GenerateDocumentList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <script>
        $(document).ready(function (e) {

            $(document).click(function (evt) {
                if (evt.target.id != "ContentPlaceHolder1_txtActNameList") {
                    if (evt.target.type == "checkbox" && $(evt.target).closest("table").attr("id") == "RepeaterTableAct") {

                        $("#dvActName").show();
                    }
                    else {
                        $("#dvActName").hide();
                    }
                }
                if (evt.target.id != "ContentPlaceHolder1_txtPeriod1List") {
                    if (evt.target.type == "checkbox" && $(evt.target).closest("table").attr("id") == "RepeaterTableP") {
                        $("#dvPeriod1").show();
                    }
                    else {
                        $("#dvPeriod1").hide();
                    }
                }
            });

            var ScheduleIds = "";
            fhead('Generate Document List');

            $('#GenrateDocumentPageModal').on('shown.bs.modal', function () {
                ScheduleIds = $("#lblScheduleIds").text();
                $('#iframePopupPage').attr('src', "RLCS_MyComplianceDocumentsPopup.aspx?scid=" + ScheduleIds);
            });

            $("#btnViewDocument").click(function () {
                $("#GenrateDocumentPageModal").modal("show");
            });
            $("#ContentPlaceHolder1_ddlOutYear").change(function () {
                $("#dvPeriod1").hide();
            });
            $('#divFilterLocation').addClass("hidden");
            $("#ContentPlaceHolder1_tbxFilterLocation").click(function (e) {
                $('#divFilterLocation').removeClass("hidden");
            });
            $('<%= tvOutputBranch.ClientID %>').click(function () {
                $("#divFilterLocationOutput").toggle("blind", null, 100, function () { });
            });
            $("#<%= txtActNameList.ClientID %>").unbind('click');
            $("#<%= txtActNameList.ClientID %>").click(function () {
                $("#dvActName").toggle("blind", null, 100, function () { });
            });
            $("#<%= txtPeriod1List.ClientID %>").unbind('click');
            $("#<%= txtPeriod1List.ClientID %>").click(function () {
                $("#dvPeriod1").toggle("blind", null, 100, function () { });
            });

        });
        $(document).on("click", function (event) {
            if (event.target.id == "") {
                var idvid = $(event.target).closest('div');
                if ($(idvid).attr('id') != undefined && $(idvid).attr('id') != '' && $(idvid).attr('id').indexOf('<%= tvOutputBranch.ClientID %>') > -1) {
                    $("#divFilterLocationOutput").show();
                } else {
                    $("#divFilterLocationOutput").hide();
                }
            } else if (event.target.id != "" && event.target.id.indexOf('<%= tvOutputBranch.ClientID %>') > -1) {
                $("#divFilterLocationOutput").show();
            } else if (event.target.id != '<%= tbxOutputBranch.ClientID %>') {
                $("#divFilterLocationOutput").hide();
            } else if (event.target.id == '<%= tbxOutputBranch.ClientID %>') {
                $("#divFilterLocationOutput").show();
            } else if (event.target.id == '<%= tvOutputBranch.ClientID %>') {
                $('<%= tvOutputBranch.ClientID %>').unbind('click');
            }

        });
      
function GetDocumentGenerateListData(CustomerID, BranchID, ComplianceType, ComplianceID, MonthId, Year, LoginUserID) {
    //Get FilterDATA

    
    var CustomerID = $('#ContentPlaceHolder1_hdnOutCustID').val();
    var BranchID = $('#ContentPlaceHolder1_hdnOutBranchId').val();
    var ComplianceType = $('#ContentPlaceHolder1_hdnOutComplianceType').val();
    var ComplianceID = $('#ContentPlaceHolder1_hdnOutComplianceID').val();
    var Month = $('#ContentPlaceHolder1_hdnOutMonth').val();
    var Year = $('#ContentPlaceHolder1_hdnOutYear').val();
    var LoginUserID = $('#ContentPlaceHolder1_hdnLoginUserID').val();
    var btnApply = $('#ContentPlaceHolder1_hdnSubmitbtn').val();
    if (ComplianceID == "") {
        ComplianceID = 0;
    }
    BindGridGenerateDocument(CustomerID, BranchID, ComplianceType, ComplianceID, Month, Year, LoginUserID);

}
function BindGridGenerateDocument(CustomerID, BranchID, ComplianceType, ComplianceID, MonthId, Year, LoginUserID) {
    //alert("Call btn");
    var IsGenerate = $("#ContentPlaceHolder1_ddlIsGenerateStatus").val();
    kendo.ui.progress($("#gridGenerateDocument"), true);
    $.ajax({
        type: "GET",
        url: '<% =avacomRLCSAPI_URL%>RLCS_Get_DIYPendingForGenerationDocumentList?CustomerID=' + CustomerID + '&BranchID=' + BranchID + '&ComplianceType=' + ComplianceType + '&ComplianceID=' + ComplianceID + '&MonthId=' + MonthId + '&Year=' + Year + '&LoginUserID=' + LoginUserID + '&IsGenerate=' + IsGenerate,
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
            request.setRequestHeader('Content-Type', 'application/json');
        },
        success: function (data) {
          
            kendo.ui.progress($("#gridGenerateDocument"), false);
          
            $("#gridGenerateDocument").kendoGrid({

                dataSource: {
                    data: data.Result,
                    schema: {
                        model: {

                        }
                    },
                    pageSize: 10,

                    total: function (data) {
                        if (data.Result != null && data.Result != undefined) {
                            if (data.Result.length > 0) {

                            }
                            return data.Result.length;
                        }
                    }
                },
                //height: 400,
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                   
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [

                    {
                        field: "IsSelected", filterable: false, sortable: false, columnMenu: false, title: "", template: "<input type='checkbox' class='clsCheck' style='margin-left:4px;' />", width: "5%;"
       , headerTemplate: "<input type='checkbox' id='chkAllMain' class='clsHeaderCheck'>", attributes: { style: "vertical-align: top;" }
                    },


                    { hidden: true, field: "ComplianceID" },
                    { hidden: true, field: "ScheduleOnID" },
                    { hidden: true, field: "ReturnRegisterChallanID" },
                    { field: "ShortForm", title: "Compliance", width: "35%", attributes: { style: 'white-space: nowrap;' } },
                    { field: "FormName", title: "Form Name", width: "20%" },
                    { field: "BranchName", title: "Branch Name", width: "15%" },
                    { field: "RLCS_PayrollMonth", title: "Period", width: "15%" },
                    {
                        title: "Action", lock: true, width: "10%;",
                        attributes: {
                            style: 'text-align: center;'
                        },
                        command:
                            [
                                { name: "edit", text: "", iconClass: ".k-icon k-i-copy k-icon k-edit", className: "ob-download"},
                                 { name: "edit2", text: "", iconClass: "k-icon k-i-hyperlink-open", className: "ob-open" }
                            ],
                    }
                ],
                dataBound: onDataBound
            });
            $("#gridGenerateDocument").kendoTooltip({
                filter: ".k-grid-edit",
                content: function (e) {
                    return "Generate Document";
                }
            });
            $("#gridGenerateDocument").kendoTooltip({
                filter: ".k-grid-edit2",
                content: function (e) {
                    return "View Document";
                }
            });
        },
        failure: function (data) {
          
            kendo.ui.progress($("#gridGenerateDocument"), false);
        }
    });


    function onDataBound(e) {
        var data = this.dataSource.view();
        for (var i = 0; i < data.length; i++) {
            var uid = data[i].uid;
            var row = this.table.find("tr[data-uid='" + uid + "']");
            if (data[i].IsGenerate == 0) {
                row.find(".ob-download").show();
                row.find(".ob-open").hide();
                row.find(".clsCheck").addClass("chkEnabled");

            }
            else {
                row.find(".ob-download").hide();
                row.find(".ob-open").show();
                row.find(".clsCheck").attr("disabled", true);
            }

        }

    }
    $(document).on("click", "#gridGenerateDocument tbody tr .ob-open", function (e) {
        kendo.ui.progress($("#gridGenerateDocument"), true);
        
        var item = $("#gridGenerateDocument").data("kendoGrid").dataItem($(this).closest("tr"));
        OpenDocumentOverviewpup(item.ScheduleOnID)
        return true;
    });

    $('#gridGenerateDocument').on('click', '#chkAllMain', function () {
        var checked = $(this).is(':checked');
        if (checked) {
            $('.chkEnabled').prop('checked', true);

            var countchecked = $("table input[type=checkbox]:not(.clsHeaderCheck):checked").length;
            if (countchecked == 0)
                $("#btnProceed").hide();
            else
                $("#btnProceed").show();
        }
        else {
            $('.chkEnabled').prop('checked', false);
            $("#btnProceed").hide();
        }

    });

    $('#gridGenerateDocument').on('click', '.chkEnabled', function () {
        var countchecked = $("table input[type=checkbox]:not(.clsHeaderCheck):checked").length;
        if (countchecked == 0)
            $("#btnProceed").hide();
        else
            $("#btnProceed").show();
    });
}

function OpenDocumentOverviewpup(ScheduleonID) {
    $('#divOverView').modal('show');
    $('#OverViews').attr('width', '1150px');
    $('#OverViews').attr('height', '500px');
    $('.modal-dialog').css('width', '1200px');
    kendo.ui.progress($("#gridGenerateDocument"), false);
  
    $('#OverViews').attr('src', "../Common/DocumentOverview.aspx?ComplianceScheduleID=" + ScheduleonID + "&ComplainceTransactionID=0&ISStatutoryflag=-1");
}

function CloseClearOV() {
    $('#OverViews').attr('src', "../Common/blank.html");
}
$(document).on("click", "#gridGenerateDocument tbody tr .ob-download", function (e) {
    kendo.ui.progress($("#gridGenerateDocument"), true);

    var item = $("#gridGenerateDocument").data("kendoGrid").dataItem($(this).closest("tr"));
    var GenerateJSON = {};
    var arrGenerate = [];
    var Complianceid = item.ComplianceID;
    var ScheduleOnID = item.ScheduleOnID;
    var ReturnRegisterChallanID = item.ReturnRegisterChallanID;
    GenerateJSON = {
        ScheduleOnID: ScheduleOnID,
        ReturnRegisterChallanID: ReturnRegisterChallanID
    }
    arrGenerate.push(GenerateJSON);
    var DocumentJSON = JSON.stringify(arrGenerate);
    CallGenerateDocumentAPI(Complianceid, DocumentJSON);
    
});


function CallGenerateDocumentAPI(Complianceid, IOJSON) {
    var ComplianceID = Complianceid;
    $.ajax({
        type: "POST",
        url: '<% =avacomRLCSAPI_URL%>GetDIYDocumentDetailsForGeneration?customerid=<% =CustomerId%>&userid=<% =UserId%>&complianceids=' + ComplianceID,
        data: IOJSON,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        beforeSend: function (request) {
            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
            request.setRequestHeader('Content-Type', 'application/json');
        },
        success: function (data) {
            if (data.Result.StatusCode = 200) {
                ScheduleIds = data.Result.Result;
                kendo.ui.progress($("#gridGenerateDocument"), false);
              

                $("#lblScheduleIds").text("");
                $("#lblScheduleIds").text(ScheduleIds);
                $("#lbldocumentMessageAlert").text(data.Result.Message);
                $("#AlertModal").modal("show");


                var CustomerID = $('#ContentPlaceHolder1_hdnOutCustID').val();
                var BranchID = $('#ContentPlaceHolder1_hdnOutBranchId').val();
                var ComplianceType = $('#ContentPlaceHolder1_hdnOutComplianceType').val();
                var ComplianceID = $('#ContentPlaceHolder1_hdnOutComplianceID').val();
                var Month = $('#ContentPlaceHolder1_hdnOutMonth').val();
                var Year = $('#ContentPlaceHolder1_hdnOutYear').val();
                var LoginUserID = $('#ContentPlaceHolder1_hdnLoginUserID').val();
                var btnApply = $('#ContentPlaceHolder1_hdnSubmitbtn').val();
                if (ComplianceID == "") {
                    ComplianceID = 0;
                }
                var grid = $("#gridGenerateDocument").data("kendoGrid");
                BindGridGenerateDocument(CustomerID, BranchID, ComplianceType, ComplianceID, Month, Year, LoginUserID);
                grid.refresh();
                return true;
            }
        },
        failure: function (data) {
            kendo.ui.progress($("#gridGenerateDocument"), false);
          

        }
    });
}
function checkAllAct(cb) {
    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkAct") > -1) {
            cbox.checked = cb.checked;
        }
    }
}
function UncheckActHeader() {
    var rowCheckBox = $("#RepeaterTableAct input[id*='chkAct']");
    var rowCheckBoxSelected = $("#RepeaterTableAct input[id*='chkAct']:checked");
    var rowCheckBoxHeader = $("#RepeaterTableAct input[id*='ActSelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {

        rowCheckBoxHeader[0].checked = false;
    }
}
function checkAllPeriods1(cb) {
    var ctrls = document.getElementsByTagName('input');
    for (var i = 0; i < ctrls.length; i++) {
        var cbox = ctrls[i];
        if (cbox.type == "checkbox" && cbox.id.indexOf("chkPeriod1") > -1) {
            cbox.checked = cb.checked;
        }
    }
}
function UncheckPeriodHeader1() {
    var rowCheckBox = $("#RepeaterTableP input[id*='chkPeriod1']");
    var rowCheckBoxSelected = $("#RepeaterTableP input[id*='chkPeriod1']:checked");
    var rowCheckBoxHeader = $("#RepeaterTableP input[id*='Period1SelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {
        rowCheckBoxHeader[0].checked = false;
    }
}

function DivClick1(e, id) {
}

function UncheckFileInputHeader() {
    var rowCheckBox = $("#RepeaterTable1 input[id*='chkFileInput']");
    var rowCheckBoxSelected = $("#RepeaterTable1 input[id*='chkFileInput']:checked");
    var rowCheckBoxHeader = $("#RepeaterTable1 input[id*='FileInputSelectAll']");
    if (rowCheckBox.length == rowCheckBoxSelected.length) {
        rowCheckBoxHeader[0].checked = true;
    } else {

        rowCheckBoxHeader[0].checked = false;
    }
}

function CallbtnCompliance() {
    $('#ContentPlaceHolder1_btnComplianceGet').click();
}

function hideDivBranch() {
    $('#divFilterLocationOutput').hide("blind", null, 0, function () { });
    $("#divFilterLocationOutput").hide();
}


function GenerateClick() {
    var gridMapping = $('#gridGenerateDocument').data().kendoGrid;
    var gridData = gridMapping.dataSource.data();

    var GenerateJSON = {};
    var arrGenerate = [];

    $("#gridGenerateDocument input[type=checkbox]:not(.clsHeaderCheck):checked").each(function () {
        var row = $(this).closest("tr")[0];

        var ScheduleOnID = row.cells[2].innerHTML;
        var ReturnRegisterChallanID = row.cells[3].innerHTML;

        GenerateJSON = {
            ScheduleOnID: ScheduleOnID,
            ReturnRegisterChallanID: ReturnRegisterChallanID
        }
        arrGenerate.push(GenerateJSON);
    });


    if (arrGenerate.length > 0) {
        var DocumentJSON = JSON.stringify(arrGenerate);
        CallGenerateDocumentAPI(null, DocumentJSON);
    }

}

    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            debugger;
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {

                $("#<%= txtActNameList.ClientID %>").unbind('click');
                $("#<%= txtActNameList.ClientID %>").click(function () {
                    $("#dvActName").toggle("blind", null, 100, function () { });
                });
                $("#<%= txtPeriod1List.ClientID %>").unbind('click');
                $("#<%= txtPeriod1List.ClientID %>").click(function () {
                    $("#dvPeriod1").toggle("blind", null, 100, function () { });
                });
                
            });
        });
    </script>

    <style>
        #loadingImage {
            position: fixed;
            margin-left: 38%;
            margin-top: 15%;
            width: 50px;
            height: 50px;
            background-image: url("../NewCSS/Default/loading-image.gif");
        }

        #tblScheduleList_length {
            display: none;
        }

        .col-md-half, .col-lg-half, .col-sm-half {
            width: 6.0%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-md-1, .col-lg-1, .col-sm-1 {
            width: 9.2%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-md-1F, .col-lg-1F, .col-sm-1F {
            width: 11.0%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-md-1half, .col-lg-1half, .col-sm-1half {
            width: 13.499999995%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-2half, .col-md-2half, .col-sm-2half {
            width: 20.93333333166666%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-3half, .col-md-3half, .col-sm-3half {
            width: 29.1666666665%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-4half, .col-md-4half, .col-sm-4half {
            width: 37.49%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-5half, .col-md-5half, .col-sm-5half {
            width: 45.83333333166666%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-6half, .col-md-6half, .col-sm-6half {
            width: 54.16666666665%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-7half, .col-md-7half, .col-sm-7half {
            width: 62.4999999998333%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-8half, .col-md-8half, .col-sm-8half {
            width: 70.83333333333166333%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-9half, .col-md-9half, .col-sm-9half {
            width: 79.1666666666499333%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-10half, .col-md-10half, .col-sm-10half {
            width: 87.499999999%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-11half, .col-md-11half, .col-sm-11half {
            width: 95.83333%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .col-lg-12half, .col-md-12half, .col-sm-12half {
            width: 104.16666666333%;
            float: left;
            position: relative;
            min-height:50px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .Top-M15 {
            margin-top: 15px;
        }

        #gridGenerateDocument {
            width: 96%;
        }

        .chosen-results {
            max-height: 200px !important;
            margin-top: 0px !important;
        }

        .chosen-container .chosen-drop {
            width: 175% !important;
        }

        .chosen-container-single .chosen-default {
            color: #666 !important;
        }

        td.k-command-cell {
            border-width: 0 0px 0 1px;
        }

        .k-filter-row th, .k-grid-header th.k-header {
            border-width: 0px 0px 1px 0px;
        }

        .k-grid td {
            border-width: 0px 1px 0px 0px;
        }

        .k-grid-pager {
            border-width: 1px 0px 0px 0px;
        }
         .k-grid-content {
            min-height: 50px !important;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="row">
        
        <asp:UpdatePanel ID="updatepanelDocList" runat="server" OnLoad="upDocGenPopup_Load">
            <ContentTemplate>
               
              <asp:Button ID="btnComplianceGet" runat="server" Text="" OnClick="btnComplianceGet_Click" CssClass="hidden" />
                <asp:HiddenField ID="hdnOutBranchId" runat="server" Value="" />
                <asp:HiddenField ID="hdnOutComplianceType" runat="server" Value="" />
                <asp:HiddenField ID="hdnOutComplianceID" runat="server" Value="0" />
                <asp:HiddenField ID="hdnOutMonth" runat="server" Value="" />
                <asp:HiddenField ID="hdnOutYear" runat="server" Value="0" />
                <asp:HiddenField ID="hdnOutCustID" runat="server" Value="" />
                <asp:HiddenField ID="hdnLoginUserID" runat="server" Value="" />
                <asp:HiddenField ID="hdnSubmitbtn" runat="server" Value="" />
                <div class="col-lg-1half col-md-1half col-sm-1half colpadding0">
                    <asp:DropDownList ID="ddlOutputComplianceType" Width="100%" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlOutputComplianceType_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Text="Compliance Type"></asp:ListItem>
                        <asp:ListItem Text="Register"></asp:ListItem>
                        <asp:ListItem Text="Challan"></asp:ListItem>
                        <asp:ListItem Text="Return"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-lg-1half col-md-1half col-sm-1half colpadding0">
                    <asp:TextBox runat="server" ID="txtActNameList" Width="95%" Style="padding: 0px; margin: 0px;" CssClass="txtbox form-control" AutoComplete="off" placeholder="Act Name" />
                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: 350%;" id="dvActName" class="dvDeptHideshow form-control">
                      <div style="overflow-y:auto;height:192px;">
                         <asp:Repeater ID="rptAct" runat="server">
                            <HeaderTemplate>
                                <table class="detailstable FadeOutOnEdit" id="RepeaterTableAct">
                                    <tr>
                                        <td style="width: 100px;" colspan="2">
                                            <div id="divSelectAll">
                                                <asp:CheckBox ID="ActSelectAll" Text=" Select All" runat="server" onclick="checkAllAct(this);CallbtnCompliance();" />
                                            </div>
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 20px">
                                        <asp:CheckBox ID="chkAct" runat="server" onclick=" UncheckActHeader();CallbtnCompliance();" /></td>
                                    <td style="width: 95%">
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 95%; padding-bottom: 5px;">
                                            <asp:Label ID="lblActID" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                            <asp:Label ID="lblActName" runat="server" Style="display: none" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                            <div id="divAct" style="margin-top: 3px;" onclick="return DivClick1(this,<%# Eval("ID")%>)"><%# Eval("Name")%></div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2half col-md-2half col-sm-2half colpadding0">
                    <asp:DropDownListChosen ID="ddlComplianceName" Width="100%" runat="server" DataPlaceHolder="Compliance" AllowSingleDeselect="false" CssClass="form-control" OnSelectedIndexChanged="ddlComplianceName_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownListChosen>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <asp:TextBox runat="server" ID="txtPeriod1List" Width="100%" Style="padding: 0px; margin: 0px;" autocomplete="off" CssClass="txtbox form-control" placeholder="     Periods" />
                    <div style="display: none; position: absolute; z-index: 50; overflow-y: auto; background: white; border: 1px solid #c3b9b9; height: 200px; width: 130%;" id="dvPeriod1" class="dvDeptHideshow form-control">
                       <div style="overflow-y:auto;height:192px;">
                         <asp:Repeater ID="rptPeriod1" runat="server">
                            <HeaderTemplate>
                                <table class="detailstable FadeOutOnEdit" id="RepeaterTableP">
                                    <tr>
                                        <td style="width: 100px;" colspan="2">
                                            <asp:CheckBox ID="Period1SelectAll" Text="Select All" runat="server" onclick="checkAllPeriods1(this)" />
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 20px;">
                                        <asp:CheckBox ID="chkPeriod1" runat="server" onclick="UncheckPeriodHeader1();" /></td>
                                    <td style="width: 100%">
                                        <div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%; padding-bottom: 5px;">
                                            <asp:Label ID="lblPeriodID1" runat="server" Style="display: none" Text='<%# Eval("ID")%>' ToolTip='<%# Eval("ID") %>'></asp:Label>
                                            <asp:Label ID="lblPeriodName1" runat="server" Text='<%# Eval("Name")%>' ToolTip='<%# Eval("Name") %>'></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                            </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 colpadding0">
                    <asp:DropDownList runat="server" ID="ddlOutYear" Width="100%" class="form-control">
                        <asp:ListItem Text="Year" Value="0" />
                        <asp:ListItem Text="2020" Value="2020" />
                        <asp:ListItem Text="2019" Value="2019" />
                        <asp:ListItem Text="2018" Value="2018" />
                        <asp:ListItem Text="2017" Value="2017" />
                        <asp:ListItem Text="2016" Value="2016" />
                        <asp:ListItem Text="2015" Value="2015" />
                    </asp:DropDownList>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 colpadding0">
                    <asp:TextBox runat="server" Width="100%" AutoComplete="off" ID="tbxOutputBranch"
                        Style="padding: 0px; padding-left: 10px; margin: 0px; width: 100%; border: 1px solid #c7c7cc; border-radius: 4px; color: #8e8e93" placeholder="Entity/State/Location"
                        CssClass="txtbox form-control" />
                    <div style="margin-left: 1px; position: absolute; z-index: 10; width: unset; margin-top: -20px; height:300px;" id="divFilterLocationOutput">
                        <asp:TreeView runat="server" ID="tvOutputBranch" SelectedNodeStyle-Font-Bold="true" Width="110%" NodeStyle-ForeColor="#8e8e93" ShowCheckBoxes="All" onclick="OnTreeClick(event)"
                            Style="overflow: auto; border-left: 1px solid #c7c7cc; border-right: 1px solid #c7c7cc; border-bottom: 1px solid #c7c7cc; background-color: #ffffff; color: #8e8e93 !important; max-height: 300px;" ShowLines="true">
                        </asp:TreeView>
                        <div id="bindelement" style="background: white; height: 292px; display: none; width: 390px; border: 1px solid; overflow: auto;"></div>
                        <asp:Button ID="btnlocation" runat="server" Text="select" CssClass="hidden" />
                        <asp:Button ID="btnClear" Visible="true" runat="server" OnClick="btnClear_Click" Text="Clear" CssClass="hidden" />
                    </div>
                </div>
                <div class="col-lg-1F col-md-1F col-sm-1F colpadding0">
                    <asp:DropDownList runat="server" ID="ddlIsGenerateStatus" Width="100%" class="form-control">
                        <asp:ListItem Text="Pending" Value="0" />
                        <asp:ListItem Text="Generated" Value="1" />
                        <asp:ListItem Text="All" Value="2" />


                    </asp:DropDownList>
                </div>
                <asp:UpdatePanel ID="btnupdate" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-half col-md-half col-sm-half">
                            <asp:Button ID="btnOutSubmit" Text="Apply" runat="server" CssClass="btn btn-primary pull-right" OnClick="btnOutSubmit_Click" />
                              
                        </div>
                        

                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnOutSubmit" />
                        
                    </Triggers>
                </asp:UpdatePanel>
                <br />
                <br />

            </ContentTemplate>
            <Triggers>
                    
            </Triggers>
        </asp:UpdatePanel>
    </div>

            <div class="row colpadding0">
             
                <div class="col-lg-12 col-md-12half col-sm-12 colpadding0">
                    <div id="gridGenerateDocument"></div>
                </div>
                <div class="row" style="width: 99%;">
                    <input id="btnProceed" type="button" value="Generate" onclick="return GenerateClick();" style="margin-top: 10px; margin-bottom: 10px; display: none;" class="btn btn-primary pull-right">
                </div>

            </div>
 
    


    <!-- Modal -->
    <div class="modal fade" id="AlertModal" role="dialog" style="margin-top: 181px;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <label id="lbldocumentMessageAlert" style="color: black; font-size: large;"></label>
                            <label id="lblScheduleIds" class="hidden"></label>

                        </div>
                    </div>
                    <div class="row Top-M15">
                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <button id="btnViewDocument" type="button" class="btn btn-primary">View Document</button>
                        </div>
                    </div>
                </div>
                <%-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>--%>
            </div>

        </div>
    </div>

    <div class="modal  modal-fullscreen" id="GenrateDocumentPageModal" role="dialog">
        <div class="modal-dialog modal-full" style="width: 100%;">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <iframe id="iframePopupPage" class="col-md-12" style="height: 595px"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="divOverView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog" style="width: 1150px;">
            <div class="modal-content" style="width: 100%;">
                <div class="modal-header" style="border-bottom: none;">
                    <button type="button" class="close" data-dismiss="modal" onclick="CloseClearOV();" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <iframe id="OverViews" src="about:blank" width="1200px" height="100%" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>




     <script>
        function initializeJQueryUI(textBoxID, divID) {
            $("#" + textBoxID).unbind('click');

            $("#" + textBoxID).click(function () {
                $("#" + divID).toggle("blind", null, 500, function () { });
            });
        }
        function fCheckTree(obj) {
            var id = $(obj).attr('data-attr');
            var elm = $("#" + id);
            $(elm).trigger('click');
        }
        function FnSearch1() {

            var tree = document.getElementById('BodyContent_tvFilterLocation');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tvFilterLocation').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement1').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement1').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement1').show();
            } else {
                $('#bindelement1').html('');
                $('#bindelement1').hide();
                $(tree).show();
            }

        }
        function FnSearch() {

            var tree = document.getElementById('BodyContent_tvOutputBranch');
            var links = tree.getElementsByTagName('a');
            var keysrch = document.getElementById('BodyContent_tvOutputBranch').value.toLowerCase();
            var keysrchlen = keysrch.length
            if (keysrchlen > 2) {
                $('#bindelement').html('');
                for (var i = 0; i < links.length; i++) {

                    var anch = $(links[i]);
                    var twoletter = $(anch).html().toLowerCase().indexOf(keysrch);
                    var getId = $(anch).attr('id');
                    var parendNode = '#' + getId + 'Nodes';
                    var childanchor = $(parendNode).find('a');
                    if (childanchor.length == 0) {
                        if (twoletter > -1) {

                            var idchild = $($(anch).siblings('input')).attr('name');
                            // var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  name="' + getId + 'CheckBox" id="' + getId + 'CheckBox"><a id="' + getId + '" href="' + $(anch).attr('href') + '" onclick="' + $(anch).attr('onclick') + '" >' + anch.html() + '</a></br>';
                            var createanchor = '<input type="checkbox" onclick="fCheckTree(this)"  data-attr="' + idchild + '" ><a  >' + anch.html() + '</a></br>';
                            $('#bindelement').append(createanchor);
                        }
                    }

                }
                $(tree).hide();
                $('#bindelement').show();
            } else {
                $('#bindelement').html('');
                $('#bindelement').hide();
                $(tree).show();
            }

        }
        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }
        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }
        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }
        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
    </script>
</asp:Content>
