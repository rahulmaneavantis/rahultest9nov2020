﻿<%@ Page Title="Applicability Assessment :: Avantis" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_CheckApplicability.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_CheckApplicability" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <title></title>

    <style>
        .k-grid-content {
            min-height: 200px;
            max-height: 350px;
            overflow-y: visible;
        }

        .k-btn {
            /*// float: right;*/
            margin-right: 10px;
        }

        .k-btnUpload {
            float: right;
            margin-right: 10px;
        }

        div.k-window-content {
            padding: 0;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
           <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            fhead('Masters/Applicability Assessment');
            $('#divShowApplicability').hide();
            BindGrid();
        });

        function BindGrid() {            
            var gridview = $("#gridListing").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetCheckApplicabilityCustomers?custID=' + <%=custID%>+'&SPID=' + <%=spID%>+'&distID=' + <%=distID%>,                            
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {  
                            debugger;
                            if (response != null && response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null && response != undefined)
                                if (response.Result != null && response.Result != undefined)
                                    return response.Result.length;
                        },                        
                    }
                },

                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    change: function (e) {

                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ID" },
                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: "5%;",
                        attributes: {
                            style: 'white-space: nowrap;text-align: center;'
                        },
                    },
                    {
                        field: "Name", title: 'Company',
                        width: "20%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "BuyerName", title: 'Contact Person',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "BuyerEmail", title: 'Email',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "BuyerContactNumber", title: 'Contact Number',
                        width: "10%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        title: "Action", lock: true, width: "5%;",
                        attributes: {
                            style: 'text-align: center;'
                        },
                        command:
                            [
                               { name: "edit", text: "", iconClass: ".k-icon k-i-pencil", className: "ob-download", click: Edit },
                            ],
                    }
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index()
                            + 1
                            + ($("#gridListing").data("kendoGrid").dataSource.pageSize() * ($("#gridListing").data("kendoGrid").dataSource.page() - 1));

                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });

                    $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                }
            });
        }

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function onClose() {
            //$(this.element).empty();
            $('#gridListing').data('kendoGrid').dataSource.read();
        }

        function onRowBound(e) {
            $(".k-grid-edit").find("span").addClass("k-icon k-edit");
            $(".k-grid-delete").find("span").addClass("k-icon k-delete");
            $(".k-grid-add").find("span").addClass("k-i-plus-outline");
        }

        function OpenCheckApplicabilityPopup(ID) {            
            $('#divShowApplicability').show();

            var myWindowAdv = $("#divShowApplicability");
            
            myWindowAdv.kendoWindow({
                width: "95%",
                height: '95%',
                title: "Applicability Assessment",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                //open: onOpen
            });
            
            $('#iframeApplicability').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeApplicability').attr('src', "/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=" + ID+"&Source=Home");            
            return false;
        }

        function Edit(e) {            
            var d = e.data;
            var grid = $("#gridListing").data("kendoGrid");

            var tr = $(e.target).closest("tr");    // get the current table row (tr)
            var item = this.dataItem(tr);
            var recordID = item.ID;

            $('#divShowApplicability').show();
            var myWindowAdv = $("#divShowApplicability");

            myWindowAdv.kendoWindow({                
                width: "95%",
                height: '95%',
                //content: '/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=' + recordID,
                title: "Applicability Assessment",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                //open: onOpen
            });

            $('#iframeApplicability').attr('src', 'about:blank');
            var windowWidget = $("#divShowApplicability").data("kendoWindow");
            
            myWindowAdv.data("kendoWindow").center().open();

            $('#iframeApplicability').attr('src', "/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=" + recordID+"&Source=Home");            
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar text-right">
            <asp:LinkButton CssClass="btn btn-primary" runat="server" ID="btnAddContract" OnClientClick="return OpenCheckApplicabilityPopup(0);"
                data-toggle="tooltip" data-placement="bottom" ToolTip="">
                    <span class="AddNewspan1"><i class='fa fa-plus'></i></span>&nbsp;Check Now
            </asp:LinkButton>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="gridListing" style="border: none;">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>

    <div id="divShowApplicability">
        <iframe id="iframeApplicability" style="width: 100%; height: 98%; border: none;"></iframe>
    </div>
</asp:Content>
