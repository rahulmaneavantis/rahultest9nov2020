﻿<%@ Page Title="Customer :: Setup/Onboarding" Language="C#" AutoEventWireup="true" MasterPageFile="~/HRPlusSPOCMGR.Master" Culture="en-GB"
    CodeBehind="RLCS_CustomerCorporateList_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_CustomerCorporateList_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>

    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />
    <%--<link href="../NewCSS/kendo.bootstrap.min.css" rel="stylesheet" />--%>

    <%--  <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>--%>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <style>
        .k-grid-content {
            min-height: 200px;
        }

        .k-btn {
            /*// float: right;*/
            margin-right: 10px;
        }

        .k-btnUpload {
            /*float: right;*/
            margin-right: 10px;
        }

        .my-custom-edit {
            content: "\e10b";
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }

        .panel-heading .nav > li > a {
            font-size: 17px;
        }
    </style>

    <style>
        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }


                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }
                div.k-confirm
                {
                    width:411px;
                    left:500.5px;
                }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
            <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/javascript">
        function Bind_ServiceProvider() {
            $("#ddlServiceProvider").kendoDropDownList({
                placeholder: "Select",
                autoWidth: true,                
                dataTextField: "Name",
                dataValueField: "ID",
                index: 0,
                change: function (e) {
                    BindGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetSPDist?IsSPDist=' +<%=IsSPDist%> +'&SPID='+<%=spID%>+'&distID='+<%=distID%>+'&custID=' + <%=custID%>,
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {                           
                            return response.Result;
                        }
                    }
                }
            });
        }

        $(document).ready(function () {
            fhead('Masters/Customer');
            Bind_ServiceProvider();
        });

        function checkCustomerActive(value) {
            if (value == 1) {
                return "Active";
            } else if (value == 0) {
                return "InActive";
            }
        }

        function BindGrid() {                       
            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetHRCustomers?userID=<%=userID%>&custID=<%=custID%>&SPID=<%=spID%>&distID=<%=distID%>&roleCode=<%=RoleCode%>&prodType=2',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {                            
                            if (response != null || response != undefined)
                                return response.Result;
                        },
                        total: function (response) {                            
                            if (response != null || response != undefined)
                                return response.Result.length;
                        }
                    }
                },
                // editable: "inline",
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    change: function (e) {
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ID", title: "ID" },
                    { hidden: true, field: "HRProductMappingID", title: "HRProductMappingID" },
                    {
                        title: "Sr.",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: "5%;",
                        attributes: {
                            style: 'white-space: nowrap;text-align: center;'
                        },
                    },
                    {
                        title: "Type",
                        field: "DistributorOrCustomer",
                        width: "10%;",
                        attributes: {
                            style: 'white-space: nowrap;text-align: center;'
                        },
                    },
                    {
                        field: "CorporateID", title: 'CorporateID',
                        width: "10%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Name", title: 'Name',
                        width: "20%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    //{
                    //    field: "BuyerName", title: 'Buyer Name',
                    //    width: "15%",
                    //    filterable: {
                    //        extra: false,
                    //        operators: {
                    //            string: {
                    //                eq: "Is equal to",
                    //                neq: "Is not equal to",
                    //                contains: "Contains"
                    //            }
                    //        }
                    //    }
                    //},
                    {
                        field: "BuyerContactNumber", title: 'Buyer Contact',
                        width: "10%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "BuyerEmail", title: 'Buyer Email',
                        width: "15%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Status", title: 'Status',
                        width: "10%",
                        //template: '#:checkCustomerActive(Status)#',
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },                   
                    {
                        command:
                            [
                          { name: "edit", text: "", iconClass: ".k-icon k-i-pencil", className: "k-grid-edit", visible:"showHideEditDeleteButton" },
                          { name: "delete", text: "", iconClass: ".k-icon k-i-delete", className: "k-grid-delete", visible:"showHideEditDeleteButton" }, 
                          //{ name: "checkApplicability", text: "", iconClass: "k-icon k-i-zoom", className: "k-grid-zoom", }, 
                                //visible:"showHideApplicabilityButton"
                          { name: "proceed", text: "", iconClass: ".k-icon .k-i-arrow-right", className: "k-grid-arrow-right", visible:"showHideEditDeleteButton"},
                            ],
                        title: "Action",
                        
                        lock: true, width: "20%;"
                    }
                ],               
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });

                    $(".k-grid-arrow-right").find("span").addClass("k-icon k-i-arrow-right");
                    $(".k-grid-edit").find("span").addClass("k-icon k-edit");
                    $(".k-grid-delete").find("span").addClass("k-icon k-i-delete");
                }
            });
            }

            function OpenCheckApplicabilityPopup(ID) {
                $('#divCheckApplicabilityDialog').show();

                var myWindowAdv = $("#divCheckApplicabilityDialog");
            
                myWindowAdv.kendoWindow({
                    width: "95%",
                    height: '95%',
                    title: "Applicability Assessment",
                    visible: false,
                    actions: ["Close"],
                    close: onClose
                });

                $('#iframeCheck').attr('src', 'about:blank');
                myWindowAdv.data("kendoWindow").center().open();
                $('#iframeCheck').attr('src', "/RLCS/HRPlus_ApplicabilityDetails.aspx?ID=" + ID+"&Source=Master");
                return false;
            }

            function showHideApplicabilityButton(data) {                
                var showHide = true;               
                if (data.DistributorOrCustomer != null && data.DistributorOrCustomer != undefined) {
                    if(data.DistributorOrCustomer==="Customer")
                        showHide=true;
                    else
                        showHide=false;
                }

                return showHide;
            }

            function showHideEditDeleteButton(data) {                
                var myCustID=<%=loggedInUserCustID%>;
                var showHide = true; 

                if(myCustID!=null && myCustID!=undefined){
                    if (data.ID != null && data.ID != undefined) {
                        if(data.ID===myCustID){
                            showHide=false;
                        }
                    }
                }

                return showHide;
            }

            $(document).on("click", "#grid tbody tr .k-grid-zoom", function (e) {                
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                var custID = item.ID;
            
                OpenCheckApplicabilityPopup(custID);
            });

            $(document).on("click", "#grid tbody tr .k-grid-arrow-right", function (e) {    
                debugger;
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                var HRProductMappingID = item.HRProductMappingID;
                var CustID = item.ID;

                if(HRProductMappingID > 0)
                {
                    window.location.href="RLCS_EntityLocation_Master_New.aspx?CustID="+CustID;
                }
                else
                {
                    alert("Access to Labour Product not yet given for this Customer, Go to Masters -> Customer and Assign Product to Customer");
                }                
            });

            //function Edit(e) {
            $(document).on("click", "#grid tbody tr .k-grid-edit", function (e) {                
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
                
                var CustID = item.ID;
                
                OpenAddCustomerPopup(CustID);

                return false;
            });

            $(document).on("click", "#grid tbody tr .k-grid-delete", function (e) {
                var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));

                if(item!=null && item!=undefined){
                    var cust = item.ID;
                    if(cust!=null && cust!=undefined){
                        window.kendoCustomConfirm("This will also delete all the sub-entities/branch associated with customer. Are you certain you want to delete this customer?","Confirm").then(function () {
                            
                            $.ajax({
                                type: 'POST',
                                url: "<%=avacomRLCSAPIURL%>DeleteCustomer?CustomerID=" + cust,
                                dataType: "json",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    xhr.setRequestHeader('Content-Type', 'application/json');
                                },
                                success: function (result) {
                                    $("#grid").data("kendoGrid").dataSource.read();
                                    var popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
                                    popupNotification.show("Deleted Successfully", "error");
                                },
                                error: function (result) {
                   
                                }
                            });
                        }, function () {
                            // For Cancel
                        });  
                    }
                }

                return false;
            });

            function SetGridTooltip(){
                $("#grid").kendoTooltip({
                    filter: ".k-grid-edit",
                    content: function(e){
                        return "View/Edit";
                    }
                });

                $("#grid").kendoTooltip({
                    filter: ".k-grid-delete",
                    content: function(e){
                        return "Delete";
                    }
                });

                $("#grid").kendoTooltip({
                    filter: ".k-grid-arrow-right",
                    content: function(e){
                        return "View Entity/Branch";
                    }
                });

                $("#grid").kendoTooltip({
                    filter: ".k-grid-zoom",
                    content: function(e){
                        return "Applicability Assessment";
                    }
                });
            }


            $(document).ready(function () {  
                
                $('#txtSearch').on('keypress', function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) { 
                        e.preventDefault();
                        return false;
                    }
                });
                $('#divAddEditCustomerDialog').hide();
                $('#divCheckApplicabilityDialog').hide();

                BindGrid();
                
                SetGridTooltip();

                $("#btnSearch").click(function (e) {

                    e.preventDefault();
                    
                    var filter = [];
                    $x = $("#txtSearch").val();
                    if ($x) {
                        var gridview = $("#grid").data("kendoGrid");
                        gridview.dataSource.query({
                            page: 1,
                            pageSize: 50,
                            filter: {
                                logic: "or",
                                filters: [
                                  { field: "Name", operator: "contains", value: $x },
                                ]
                            }
                        });

                        return false;
                    }
                    else {
                        var dataSource = $("#grid").data("kendoGrid").dataSource;
                        dataSource.filter({});
                        //BindGrid();
                    }
                    // return false;
                });
            });
    </script>

    <script>
         ///GG ADD 29July2020
        function CustomerValidateCheck()
         {
            debugger;
            var CustomerID =  $('#ddlServiceProvider').val();
            var  NewRecord=1;  ///ADD
            event.preventDefault();
            if (CustomerID != undefined) {

                $.ajax({
                    url: "/Setup/CheckForCustomerValidation",
                    data: { NewRecord,CustomerID},
                    type: "POST",
                    content: 'application/json;charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        debugger;
                        if(data === true)
                        {
                            OpenAddCustomerPopup(0);   ///ADD
                        }
                        else
                        {
                            alert("Facility to create more customers is not available in the Free version. Kindly contact Avantis to activate the same.");
                        }
                    }

                });
            }
        }
         ///END
        function OpenAddCustomerPopup(CustID) {
            $('#divAddEditCustomerDialog').show();
            var myWindowAdv = $("#divAddEditCustomerDialog");

            myWindowAdv.kendoWindow({
                width: "60%",
                height: '90%',                
                iframe: true,
                title: "Customer Details",
                visible: false,
                actions: ["Close"],
                close: onClose
            });

            $('#iframeAdd').attr('src', 'about:blank');
            myWindowAdv.data("kendoWindow").center().open();
            $('#iframeAdd').attr('src', "/RLCS/RLCS_CustomerDetails.aspx?CustomerID=" + CustID);
            return false;
        }

        function onClose() {
            $('#grid').data('kendoGrid').dataSource.read();
        }
        
        $(document).ready(function () {
            // function BindLocationFilter() {
            function ClearAllFilterMain(e) {

                e.preventDefault();

                $("#dropdowntree").data("kendoDropDownTree").value([]);
                //$("#ddlMonth").data("kendoDropDownList").value([]);
                //$("#ddlYear").data("kendoDropDownList").value([]);
                $('#ClearfilterMain').css('display', 'none');

                $("#grid").data("kendoGrid").dataSource.filter({});

                $('#chkAll').removeAttr('checked');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <span id="popupNotification"></span>
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton>
                        <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>

        <div class="col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
            <div class="col-md-4" style="padding-left: 0px;">
                <input id="ddlServiceProvider" style="width: 100%" />
            </div>
            <div class="col-md-4 colpadding0">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 80%" placeholder="Type to search..." autocomplete="off" />
                <button id="btnSearch" class="k-button">Search</button>
            </div>
            <div class="col-md-2">
            </div>

    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role != null)
        { %>
    <%if (com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.Role != "CADMN")
        { %>
    <div class="col-md-2 colpadding0" style="text-align: right;">
        <button id="btnAdd" class="k-button k-btnUpload" onclick="return CustomerValidateCheck()"><span class="k-icon k-i-plus-outline"></span>Add New</button>
    </div>
    <% } %>
    <% } %>

        </div>
    </div>

    <div class="row colpadding0">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="row" style="padding-bottom: 4px; font-size: 12px; display: none; color: black;" id="filtersstoryboard">&nbsp;</div>
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid" style="border: none;">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>

    <div id="divAddEditCustomerDialog">
        <iframe id="iframeAdd" style="width: 100%; height: 100%; border: none"></iframe>
    </div>

    <div id="divCheckApplicabilityDialog">
        <iframe id="iframeCheck" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
</asp:Content>
