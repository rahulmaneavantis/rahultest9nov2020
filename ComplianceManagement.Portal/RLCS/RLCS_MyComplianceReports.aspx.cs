﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using OfficeOpenXml;
using System.IO;
using System.Globalization;
using OfficeOpenXml.Style;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyComplianceReports : System.Web.UI.Page
    {
        static int CustomerID;
        static int UserRoleID;
        protected static string ClickChangeflag = "P";
        public static List<long> branchList = new List<long>();

        protected void Page_Load(object sender, EventArgs e)
        {
            CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserRoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);

            if (!IsPostBack)
            {
                Session["Status"] = "";
                Session["ComplianceType"] = "Statutory";
                Session["ComplianceTypeReviewer"] = "Statutory";

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(CustomerID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            List<int> assignedbranchIDs = new List<int>();
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                            BindCategoriesAll();
                            BindTypesAll();
                            BindActListAll();
                            BindComplianceSubTypeListAll();

                            BindEvents();
                            BindStatus();

                            BindLocationFilter(assignedbranchIDs);

                            Ffilldata();
                            GetPageDisplaySummary();

                            if (SelectedPageNo.Text == "")
                            {
                                SelectedPageNo.Text = "1";
                            }
                            if (tvFilterLocationPerformer.SelectedValue != "-1")
                                Session["LocationName"] = tvFilterLocationPerformer.SelectedNode.Text;
                            else
                                Session["LocationName"] = tvFilterLocationPerformer.Nodes[1].Text;
                        }
                        else
                        {
                            cvDuplicateEntry.IsValid = false;
                            cvDuplicateEntry.ErrorMessage = "No Branch Assigned to User.So, My Reports page will not be accessible";
                        }
                    }
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocationPerformer\").hide(\"blind\", null, 500, function () { });", true);

            }
        }

        private void BindComplianceSubTypeListAll()
        {
            try
            {
                ddlComplianceSubType.Items.Clear();

                List<SP_GetComplianceSubTypeIDAll_Result> ComplianceSubTypeList = ActManagement.GetSubTypeByUserIDAll(CustomerID);
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Insert(0, new ListItem("Compliance Subtype", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindComplianceSubTypeList()
        {
            try
            {
                ddlComplianceSubType.Items.Clear();

                List<SP_GetComplianceSubTypeID_Result> ComplianceSubTypeList = ActManagement.GetSubTypeByUserID(AuthenticationHelper.UserID);
                ddlComplianceSubType.DataTextField = "Name";
                ddlComplianceSubType.DataValueField = "ID";
                ddlComplianceSubType.DataSource = ComplianceSubTypeList;
                ddlComplianceSubType.DataBind();

                ddlComplianceSubType.Items.Insert(0, new ListItem("Compliance Subtype", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindEventNature();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEvents()
        {
            try
            {
                ddlEvent.DataSource = null;
                ddlEvent.DataBind();
                ddlEvent.DataTextField = "EventName";
                ddlEvent.DataValueField = "eventid";
                var TypeList = ActManagement.GetActiveEvents(10, CustomerID, AuthenticationHelper.UserID, UserRoleID);
                ddlEvent.DataSource = TypeList;
                ddlEvent.DataBind();

                ddlEvent.Items.Insert(0, new ListItem("Event Name", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindEventNature()
        {
            try
            {
                ddlEventNature.DataSource = null;
                ddlEventNature.DataBind();
                ddlEventNature.DataTextField = "EventNature";
                ddlEventNature.DataValueField = "EventScheduleOnid";
                var TypeList = ActManagement.GetEventNature(10, CustomerID, AuthenticationHelper.UserID, Convert.ToInt32(ddlEvent.SelectedValue), UserRoleID);
                ddlEventNature.DataSource = TypeList;
                ddlEventNature.DataBind();

                ddlEventNature.Items.Insert(0, new ListItem("Event Nature", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(customerID);
                tvFilterLocationPerformer.Nodes.Clear();
                string isstatutoryinternal = "";
                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    isstatutoryinternal = "S";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Internal")
                {
                    isstatutoryinternal = "I";
                }

                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                node.Selected = true;
                tvFilterLocationPerformer.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocationPerformer.Nodes.Add(node);
                }

                tvFilterLocationPerformer.CollapseAll();
                // divFilterLocationPerformer.Style.Add("display", "none");
                tvFilterLocationPerformer_SelectedNodeChanged(null, null);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocationPerformer\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocationPerformer.Nodes.Clear();

                    string isstatutoryinternal = "";
                    //if (ddlStatus.SelectedValue == "Statutory")
                    //{
                    if (ddlStatus.SelectedValue == "0")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlStatus.SelectedValue == "1")
                    {
                        isstatutoryinternal = "I";
                    }

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocationPerformer.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocationPerformer.Nodes.Add(node);
                    }

                    tvFilterLocationPerformer.CollapseAll();
                    tvFilterLocationPerformer_SelectedNodeChanged(null, null);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp)
        {
            try
            {
                foreach (var item in nvp.Children)
                {
                    TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                    BindBranchesHierarchy(node, item);
                    parent.ChildNodes.Add(node);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindActListAll()
        {
            try
            {
                ddlAct.Items.Clear();
                var ActList = ActManagement.GetAllAssignedActs_RLCS(CustomerID, AuthenticationHelper.UserID, -1, AuthenticationHelper.ProfileID, branchList,false);  ///0
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCategoriesAll()
        {
            try
            {
                ddlCategory.DataSource = null;
                ddlCategory.DataBind();

                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "ID";

                var lstCategories = ComplianceCategoryManagement.GetAll();

                if (lstCategories.Count > 0)
                    lstCategories = lstCategories.Where(row => row.ID == 2 || row.ID == 5).ToList();

                ddlCategory.DataSource = lstCategories;
                ddlCategory.DataBind();

                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindTypesAll()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var TypeList = ActManagement.GetAllAssignedTypeAll(CustomerID);
                ddlType.DataSource = TypeList;
                ddlType.DataBind();

                ddlType.Items.Insert(0, new ListItem("Select Type", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //private void BindStatus()
        //{
        //    try
        //    {
        //        foreach (CannedReportFilterNewStatus r in Enum.GetValues(typeof(CannedReportFilterNewStatus)))
        //        {
        //            ListItem item = new ListItem(Enum.GetName(typeof(CannedReportFilterNewStatus), r), r.ToString());
        //            ddlStatus.Items.Add(item);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}
        private void BindStatus()
        {
            try
            {
                ddlStatus.Items.Clear();

                foreach (RLCS_CannedReportFilterStatus r in Enum.GetValues(typeof(RLCS_CannedReportFilterStatus)))
                {
                    string description = GetDescription(r);

                    //ListItem item = new ListItem(Enum.GetName(typeof(RLCSReviewerStatus), r), r.ToString());
                    ListItem item = new ListItem(description, r.ToString());
                    ddlStatus.Items.Add(item);
                }

                //ddlStatus.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string GetDescription(Enum GenericEnum)
        {
            Type genericEnumType = GenericEnum.GetType();
            MemberInfo[] memberInfo = genericEnumType.GetMember(GenericEnum.ToString());
            if ((memberInfo != null && memberInfo.Length > 0))
            {
                try
                {
                    return memberInfo[0].GetCustomAttributesData()[0].ConstructorArguments[0].Value.ToString();
                }
                catch (Exception)
                {
                    return GenericEnum.ToString();
                }
                //var _Attribs = memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
                //if ((_Attribs != null && _Attribs.Count() > 0))
                //{
                //    return ((System.ComponentModel.DescriptionAttribute)_Attribs.ElementAt(0)).Description;
                //}
            }
            return GenericEnum.ToString();
        }

        protected void dlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                CannedReportFilterNewStatus status = (CannedReportFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int type = Convert.ToInt32(ddlType.SelectedValue);
                int category = Convert.ToInt32(ddlCategory.SelectedValue);
                int ActID = Convert.ToInt32(ddlAct.SelectedValue);
                int SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                string StringType = "";
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;

                int EventID = Convert.ToInt32(ddlEvent.SelectedValue);

                int EventScheduleID;
                if (ddlEventNature.SelectedValue.ToString() != "")
                {
                    EventScheduleID = Convert.ToInt32(ddlEventNature.SelectedValue);
                }
                else
                {
                    EventScheduleID = -1;
                }
                int complianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);

                if (txtStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (txtEndDate.Text == "")
                {
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }

                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    var GridData = CannedReportManagement.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);
                    grdComplianceTransactions.DataSource = GridData;
                    grdComplianceTransactions.DataBind();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    var GridData = CannedReportManagement.GetCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);
                    grdComplianceTransactions.DataSource = GridData;
                    grdComplianceTransactions.DataBind();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                {
                    var GridData = CannedReportManagement.GetChecklistCannedReportDataForPerformer(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType);
                    grdComplianceTransactions.DataSource = GridData;
                    grdComplianceTransactions.DataBind();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;
                dlFilters_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public DataTable GetGrid()
        {
            dlFilters_SelectedIndexChanged(null, null);

            if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based")
            {
                return (grdComplianceTransactions.DataSource as List<SP_GetCannedReportCompliancesSummary_Result>).ToDataTable();
            }
            else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
            {
                return (grdComplianceTransactions.DataSource as List<CheckListInstanceTransactionView>).ToDataTable();
            }
            else //Statutory and Statutory CheckList
            {
                return (grdComplianceTransactions.DataSource as List<SP_GetCannedReportCompliancesSummary_Result>).ToDataTable();
            }
        }


        public string GetFilter()
        {
            return Enumerations.GetEnumByID<CannedReportFilterNewStatus>(Convert.ToInt16(dlFilters.DataKeys[dlFilters.SelectedIndex]));
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }


        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(e.CommandArgument.ToString()))
                {
                    int ScheduledOnID = Convert.ToInt32(e.CommandArgument);
                    if (e.CommandName.Equals("View_History"))
                    {
                        BindTransactions(ScheduledOnID);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DateTime CurrentDate = DateTime.Today.Date;

                int filtervalue = Convert.ToInt32(dlFilters.SelectedIndex);

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    String GridStatus = e.Row.Cells[6].Text;

                    LinkButton lnkbtnHistory = (LinkButton)e.Row.FindControl("lnkbtnHistory");
                    Label lblScheduledOnID = (Label)e.Row.FindControl("lblScheduledOnID");

                    Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");

                    //BindTransactions(Convert.ToInt32(lblScheduledOnID.Text));
                    if (filtervalue == 4)
                    {
                        lnkbtnHistory.Visible = true;
                        //BindTransactions(Convert.ToInt32(lblScheduledOnID.Text));
                    }
                    else
                    {
                        lnkbtnHistory.Visible = false;
                    }

                    if (lblScheduledOn != null)
                    {
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            e.Row.Cells[6].Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            e.Row.Cells[6].Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdComplianceTransactionsInt_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                DateTime CurrentDate = DateTime.Today.Date;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    String GridStatus = e.Row.Cells[6].Text;
                    Label lblScheduledOn = (Label)e.Row.FindControl("lblInternalScheduledOn");

                    if (lblScheduledOn != null)
                    {
                        if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date >= CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "Open" && Convert.ToDateTime(lblScheduledOn.Text).Date < CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                        else if (GridStatus == "Complied but pending review")
                            e.Row.Cells[6].Text = "Pending For Review";
                        else if (GridStatus == "Complied Delayed but pending review")
                            e.Row.Cells[6].Text = "Pending For Review";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date > CurrentDate)
                            e.Row.Cells[6].Text = "Upcoming";
                        else if (GridStatus == "In Progress" && Convert.ToDateTime(lblScheduledOn.Text).Date <= CurrentDate)
                            e.Row.Cells[6].Text = "Overdue";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindTransactions(int ScheduledOnID)
        {
            try
            {
                if (ScheduledOnID != 0)
                {
                    grdTransactionHistory.DataSource = Business.ComplianceManagement.GetAllTransactionLog(ScheduledOnID);
                    grdTransactionHistory.DataBind();
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "OpenDialog", "$(\"#divActDialog111\").dialog('open')", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdComplianceTransactions_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
        }
        protected void grdComplianceTransactions_RowCreated(object sender, GridViewRowEventArgs e)
        {
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (tvFilterLocationPerformer.SelectedValue != "-1")
                    Session["LocationName"] = tvFilterLocationPerformer.SelectedNode.Text;
                else
                    Session["LocationName"] = tvFilterLocationPerformer.Nodes[1].Text;

                SelectedPageNo.Text = "1";

                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                Ffilldata();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void Ffilldata()
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                RLCS_CannedReportFilterStatus status = (RLCS_CannedReportFilterStatus)Convert.ToInt16(ddlStatus.SelectedIndex);

                //RLCS_CannedReportFilterStatus status; 
                //Enum.TryParse(ddlStatus.SelectedItem.Text, out status);

                int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                int type = -1;
                int category = -1;
                int ActID = -1;
                int SubTypeID = -1;

                if (!string.IsNullOrEmpty(ddlType.SelectedValue) || ddlType.SelectedValue != "")
                    type = Convert.ToInt32(ddlType.SelectedValue);

                if (!string.IsNullOrEmpty(ddlCategory.SelectedValue) || ddlCategory.SelectedValue != "")
                    category = Convert.ToInt32(ddlCategory.SelectedValue);

                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) || ddlAct.SelectedValue != "")
                    ActID = Convert.ToInt32(ddlAct.SelectedValue);

                if (!string.IsNullOrEmpty(ddlComplianceSubType.SelectedValue) || ddlComplianceSubType.SelectedValue != "")
                    SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);
                
                DateTime dtfrom = DateTime.Now;
                DateTime dtTo = DateTime.Now;
                string StringType = "";

                int EventID = Convert.ToInt32(ddlEvent.SelectedValue);

                int EventScheduleID;
                if (ddlEventNature.SelectedValue.ToString() != "")
                {
                    EventScheduleID = Convert.ToInt32(ddlEventNature.SelectedValue);
                }
                else
                {
                    EventScheduleID = -1;
                }

                int complianceType = -1;

                if (!string.IsNullOrEmpty(ddlComplianceType.SelectedValue) || ddlComplianceType.SelectedValue != "")
                    complianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);

                if (txtStartDate.Text == "")
                {
                    dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                if (txtEndDate.Text == "")
                {
                    //dtTo = DateTime.Today.Date;
                    dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                }
                StringType = txtSearchType.Text;

                if (location != -1)
                {
                    branchList.Clear();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    GetAllHierarchy(customerid, Convert.ToInt32(location));
                    branchList.ToList();
                }

                if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    var GridData = RLCSManagement.GetCannedReportData_RLCS(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType, branchList);

                    if (GridData.Count > 0)
                    {
                        var maxDate = GridData.Max(r => r.ScheduledOn);
                        var minDate = GridData.Min(r => r.ScheduledOn);
                    }

                    grdComplianceTransactions.DataSource = null;
                    grdComplianceTransactions.DataBind();

                    grdComplianceTransactions.DataSource = GridData;
                    Session["TotalRowsPerformer"] = GridData.Count;
                    grdComplianceTransactions.DataBind();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    var GridData = RLCSManagement.GetCannedReportData_RLCS(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType, branchList);

                    if (GridData.Count > 0)
                    {
                        var maxDate = GridData.Max(r => r.ScheduledOn);
                        var minDate = GridData.Min(r => r.ScheduledOn);
                    }

                    grdComplianceTransactions.DataSource = null;
                    grdComplianceTransactions.DataBind();

                    grdComplianceTransactions.DataSource = GridData;
                    Session["TotalRowsPerformer"] = GridData.Count;
                    grdComplianceTransactions.DataBind();
                }
                else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                {
                    var GridData = RLCSManagement.GetCannedReportData_RLCS(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType, branchList);

                    if (GridData.Count > 0)
                    {
                        var maxDate = GridData.Max(r => r.ScheduledOn);
                        var minDate = GridData.Min(r => r.ScheduledOn);
                    }

                    grdComplianceTransactions.DataSource = null;
                    grdComplianceTransactions.DataBind();

                    grdComplianceTransactions.DataSource = GridData;
                    Session["TotalRowsPerformer"] = GridData.Count;
                    grdComplianceTransactions.DataBind();

                }

                GetPageDisplaySummary();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        static public void EnumToListBox(Type EnumType, ListControl TheListBox)
        {
            Array Values = System.Enum.GetValues(EnumType);

            foreach (long Value in Values)
            {
                string Display = Enum.GetName(EnumType, Value);
                ListItem Item = new ListItem(Display, Value.ToString());
                TheListBox.Items.Add(Item);
            }
        }

        protected void ddlpageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!IsValid()) { return; };
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                {
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                Ffilldata();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                if (!(StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRowsPerformer"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRowsPerformer"]))
                    EndRecord = Convert.ToInt32(Session["TotalRowsPerformer"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                Paging_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void Paging_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                {
                    grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                Ffilldata();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlpageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlpageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRowsPerformer"]))
                    EndRecord = Convert.ToInt32(Session["TotalRowsPerformer"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                Paging_Click(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private bool IsValid()
        {
            try
            {
                if (String.IsNullOrEmpty(SelectedPageNo.Text.Trim()) || (SelectedPageNo.Text == "0"))
                {
                    SelectedPageNo.Text = "1";
                    return false;
                }
                else if (!IsNumeric(SelectedPageNo.Text))
                {
                    //ShowGridViewPagingErrorMessage("Please Insert Valid Page No.");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private bool IsNumeric(string PageNo)
        {
            try
            {
                int i = Convert.ToInt32(PageNo);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }



        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRowsPerformer"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlpageSize.SelectedValue) > Convert.ToInt32(Session["TotalRowsPerformer"].ToString())))
                            lblEndRecord.Text = ddlpageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRowsPerformer"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {

                TotalRows.Value = Session["TotalRowsPerformer"].ToString();


                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlpageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlpageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlEventNature.SelectedValue = "-1";
                ddlEvent.SelectedValue = "-1";
                divEvent.Visible = false;

                if (ddlComplianceType.SelectedItem.Text == "Event Based")
                {
                    divEvent.Visible = true;
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    DivComplianceSubTypeList.Attributes.Remove("disabled");

                    Session["ComplianceType"] = "EventBased";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Statutory")
                {
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    DivComplianceSubTypeList.Attributes.Remove("disabled");

                    Session["ComplianceType"] = "Statutory";
                }
                else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                {
                    ddlAct.ClearSelection();
                    PanelAct.Enabled = true;
                    Panelsubtype.Enabled = true;
                    DivAct.Attributes.Remove("disabled");
                    DivComplianceSubTypeList.Attributes.Add("disabled", "true");

                    Session["ComplianceType"] = "Statutory CheckList";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //protected void ddlComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //        int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
        //        CannedReportFilterNewStatus status = (CannedReportFilterNewStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
        //        int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
        //        ddlEventNature.SelectedValue = "-1";
        //        ddlEvent.SelectedValue = "-1";
        //        divEvent.Visible = false;
        //        if (ddlComplianceType.SelectedItem.Text == "Event Based")
        //        {
        //            divEvent.Visible = true;
        //            ddlAct.ClearSelection();
        //            PanelAct.Enabled = true;
        //            Panelsubtype.Enabled = true;
        //            DivAct.Attributes.Remove("disabled");
        //            DivComplianceSubTypeList.Attributes.Remove("disabled");

        //            grdComplianceTransactions.Visible = true;

        //            SelectedPageNo.Text = "1";

        //            grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
        //            grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

        //            Ffilldata();
        //            GetPageDisplaySummary();


        //                BindTypesAll();
        //                BindCategoriesAll();


        //            Session["ComplianceType"] = "EventBased";
        //        }
        //        else if (ddlComplianceType.SelectedItem.Text == "Statutory")
        //        {
        //            ddlAct.ClearSelection();
        //            PanelAct.Enabled = true;
        //            Panelsubtype.Enabled = true;
        //            DivAct.Attributes.Remove("disabled");
        //            DivComplianceSubTypeList.Attributes.Remove("disabled");

        //            grdComplianceTransactions.Visible = true;

        //            SelectedPageNo.Text = "1";

        //            grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
        //            grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

        //            Ffilldata();
        //            GetPageDisplaySummary();


        //                BindTypesAll();
        //                BindCategoriesAll();


        //            Session["ComplianceType"] = "Statutory";
        //        }
        //        else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
        //        {
        //            ddlAct.ClearSelection();
        //            PanelAct.Enabled = true;
        //            Panelsubtype.Enabled = true;
        //            DivAct.Attributes.Remove("disabled");
        //            DivComplianceSubTypeList.Attributes.Add("disabled", "true");

        //            grdComplianceTransactions.Visible = true;

        //            SelectedPageNo.Text = "1";

        //            grdComplianceTransactions.PageSize = Convert.ToInt32(ddlpageSize.SelectedValue);
        //            grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

        //            Ffilldata();
        //            GetPageDisplaySummary();


        //                BindTypesAll();
        //                BindCategoriesAll();


        //            Session["ComplianceType"] = "Statutory CheckList";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //    }
        //}

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ddlComplianceSubType.SelectedValue = "-1";
                ddlType.SelectedValue = "-1";
                ddlCategory.SelectedValue = "-1";
                ddlAct.SelectedValue = "-1";
                txtStartDate.Text = "";
                txtEndDate.Text = "";
                txtSearchType.Text = string.Empty;
                divAdvSearch.Visible = false;

                SelectedPageNo.Text = "1";

                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                Ffilldata();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAdvSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                if (ddlComplianceType.SelectedItem.Text == "Statutory" || ddlComplianceType.SelectedItem.Text == "Event Based" || ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                    grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;

                int type = 0;
                int Category = 0;
                int Act = 0;
                int subCategory = 0;

                if (txtStartDate.Text != "" && txtEndDate.Text == "" || txtStartDate.Text == "" && txtEndDate.Text != "")
                {
                    if (txtStartDate.Text == "")
                        txtStartDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");
                    else if (txtEndDate.Text == "")
                        txtEndDate.BorderColor = System.Drawing.ColorTranslator.FromHtml("red");

                    return;
                }

                if (!string.IsNullOrEmpty(ddlType.SelectedValue) && ddlType.SelectedValue != "-1")
                {
                    type = ddlType.SelectedItem.Text.Length;

                    if (type >= 20)
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Type: </b>" + ddlType.SelectedItem.Text;
                }

                if (!string.IsNullOrEmpty(ddlCategory.SelectedValue) && ddlCategory.SelectedValue != "-1")
                {
                    Category = ddlCategory.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlCategory.SelectedItem.Text;
                    }
                }

                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && ddlAct.SelectedValue != "-1")
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }


                if (!string.IsNullOrEmpty(ddlComplianceSubType.SelectedValue) && ddlComplianceSubType.SelectedValue != "-1")
                {
                    subCategory = ddlComplianceSubType.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (subCategory >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                    }
                    else
                    {
                        if (subCategory >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Sub Category: </b>" + ddlComplianceSubType.SelectedItem.Text;
                    }
                }

                if (txtStartDate.Text != "" && txtEndDate.Text != "")
                {
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        lblAdvanceSearchScrum.Text += "     " + "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "     " + "<b>End Date: </b>" + txtEndDate.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Start Date: </b>" + txtStartDate.Text;
                        lblAdvanceSearchScrum.Text += "<b> End Date: </b>" + txtEndDate.Text;
                    }
                }

                if (txtSearchType.Text != "")
                {
                    lblAdvanceSearchScrum.Text += "<b>Type to Search: </b>" + txtSearchType.Text;
                }

                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    Ffilldata();
                }
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocationPerformer\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Session["Status"] = ddlStatus.SelectedItem.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void tvFilterLocationPerformer_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocationPerformer.Text = tvFilterLocationPerformer.SelectedNode.Text;
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationPerformer');", tbxFilterLocationPerformer.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocationPerformer\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                    RLCS_CannedReportFilterStatus status = (RLCS_CannedReportFilterStatus)Convert.ToInt16(ddlStatus.SelectedIndex);
                    int location = Convert.ToInt32(tvFilterLocationPerformer.SelectedValue);
                    int type = -1;
                    int category = -1;
                    int ActID = -1;
                    int SubTypeID = -1;

                    if (!string.IsNullOrEmpty(ddlType.SelectedValue) || ddlType.SelectedValue != "")
                        type = Convert.ToInt32(ddlType.SelectedValue);

                    if (!string.IsNullOrEmpty(ddlCategory.SelectedValue) || ddlCategory.SelectedValue != "")
                        category = Convert.ToInt32(ddlCategory.SelectedValue);

                    if (!string.IsNullOrEmpty(ddlAct.SelectedValue) || ddlAct.SelectedValue != "")
                        ActID = Convert.ToInt32(ddlAct.SelectedValue);

                    if (!string.IsNullOrEmpty(ddlComplianceSubType.SelectedValue) || ddlComplianceSubType.SelectedValue != "")
                        SubTypeID = Convert.ToInt32(ddlComplianceSubType.SelectedValue);

                    DateTime dtfrom = DateTime.Now;
                    DateTime dtTo = DateTime.Now;
                    string StringType = "";

                    int EventID = Convert.ToInt32(ddlEvent.SelectedValue);

                    int EventScheduleID;
                    if (ddlEventNature.SelectedValue.ToString() != "")
                    {
                        EventScheduleID = Convert.ToInt32(ddlEventNature.SelectedValue);
                    }
                    else
                    {
                        EventScheduleID = -1;
                    }

                    int complianceType = -1;

                    if (!string.IsNullOrEmpty(ddlComplianceType.SelectedValue) || ddlComplianceType.SelectedValue != "")
                        complianceType = Convert.ToInt32(ddlComplianceType.SelectedValue);

                    if (txtStartDate.Text == "")
                    {
                        dtfrom = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtfrom = DateTime.ParseExact(txtStartDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    if (txtEndDate.Text == "")
                    {
                        //dtTo = DateTime.Today.Date;
                        dtTo = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        dtTo = DateTime.ParseExact(txtEndDate.Text.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    StringType = txtSearchType.Text;

                    if (location != -1)
                    {
                        branchList.Clear();
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        GetAllHierarchy(customerid, Convert.ToInt32(location));
                        branchList.ToList();
                    }

                    List<SP_GetCannedReportCompliancesSummary_Result> masterlstComAssignCustomerWise = new List<SP_GetCannedReportCompliancesSummary_Result>();

                    if (ddlComplianceType.SelectedItem.Text == "Statutory")
                    {
                        masterlstComAssignCustomerWise = RLCSManagement.GetCannedReportData_RLCS(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType, branchList);

                        if (masterlstComAssignCustomerWise.Count > 0)
                        {
                            var maxDate = masterlstComAssignCustomerWise.Max(r => r.ScheduledOn);
                            var minDate = masterlstComAssignCustomerWise.Min(r => r.ScheduledOn);
                        }
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Event Based")
                    {
                         masterlstComAssignCustomerWise = RLCSManagement.GetCannedReportData_RLCS(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType,branchList);

                        if (masterlstComAssignCustomerWise.Count > 0)
                        {
                            var maxDate = masterlstComAssignCustomerWise.Max(r => r.ScheduledOn);
                            var minDate = masterlstComAssignCustomerWise.Min(r => r.ScheduledOn);
                        }
                    }
                    else if (ddlComplianceType.SelectedItem.Text == "Statutory CheckList")
                    {
                         masterlstComAssignCustomerWise = RLCSManagement.GetCannedReportData_RLCS(customerid, AuthenticationHelper.UserID, risk, status, location, type, category, ActID, dtfrom, dtTo, complianceType, EventID, EventScheduleID, SubTypeID, StringType,branchList);

                        if (masterlstComAssignCustomerWise.Count > 0)
                        {
                            var maxDate = masterlstComAssignCustomerWise.Max(r => r.ScheduledOn);
                            var minDate = masterlstComAssignCustomerWise.Min(r => r.ScheduledOn);
                        }
                    }

                    ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("CannedReport");
                    DataTable ExcelData = null;
                    string filter = "";
                    String LocationName = String.Empty;

                    DateTime CurrentDate = DateTime.Today.Date;

                    if (Session["LocationName"].ToString() != "")
                        LocationName = Session["LocationName"].ToString();

                    // var masterlstComAssignCustomerWise = RLCSManagement.GetlstComplianceAssignments(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID);

                    //if (AuthenticationHelper.Role.Equals("HMGMT")
                    //    || AuthenticationHelper.Role.Equals("HMGR")
                    //    || AuthenticationHelper.Role.Equals("LSPOC")
                    //    || AuthenticationHelper.Role.Equals("HAPPR"))
                    //{
                        #region Management
                        if (ClickChangeflag == "P")
                        {
                            #region Managment Performer
                            // DataView view = new System.Data.DataView(GetGrid());
                            DataView view = new System.Data.DataView(masterlstComAssignCustomerWise.ToDataTable());

                            if (Session["ComplianceType"].ToString() == "Statutory")
                            {
                                ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name","ShortForm", "RequiredForms","TextFrequency", "ForMonth", "ScheduledOn","Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                                //ExcelData.Columns.Add("RiskName");
                                //ExcelData.Columns.Add("Performer");
                                //ExcelData.Columns.Add("Reviewer");
                                //ExcelData.Columns.Add("Approver");


                                foreach (DataRow item in ExcelData.Rows)
                                {
                                    //item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    //item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                                    //item["Approver"] = GetApprover(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                                    //if (item["Risk"].ToString() == "0")
                                    //    item["RiskName"] = "High";
                                    //else if (item["Risk"].ToString() == "1")
                                    //    item["RiskName"] = "Medium";
                                    //else if (item["Risk"].ToString() == "2")
                                    //    item["RiskName"] = "Low";

                                    if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Complied but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "Complied Delayed but pending review")
                                        item["Status"] = "Pending For Review";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                                        item["Status"] = "Upcoming";
                                    else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                                        item["Status"] = "Overdue";
                                    else if (item["Status"].ToString() == "Not Complied")
                                        item["Status"] = "Rejected";
                                }

                                ExcelData.Columns.Remove("ComplianceStatusID");
                                ExcelData.Columns.Remove("ScheduledOnID");
                                ExcelData.Columns.Remove("ComplianceInstanceID");
                                //ExcelData.Columns.Remove("Risk");
                            }
                        #region startComment
                        //else if (Session["ComplianceType"].ToString() == "EventBased")
                        //{
                        //    ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortForm", "RequiredForms", "TextFrequency", "EventName", "EventNature", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                        //   // ExcelData.Columns.Add("RiskName");
                        //    //ExcelData.Columns.Add("Performer");
                        //    //ExcelData.Columns.Add("Reviewer");
                        //    //ExcelData.Columns.Add("Approver");

                        //    foreach (DataRow item in ExcelData.Rows)
                        //    {
                        //        //item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                        //        //item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                        //        //item["Approver"] = GetApprover(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                        //        if (item["Risk"].ToString() == "0")
                        //            item["RiskName"] = "High";
                        //        else if (item["Risk"].ToString() == "1")
                        //            item["RiskName"] = "Medium";
                        //        else if (item["Risk"].ToString() == "2")
                        //            item["RiskName"] = "Low";

                        //        if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                        //            item["Status"] = "Upcoming";
                        //        else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                        //            item["Status"] = "Overdue";
                        //        else if (item["Status"].ToString() == "Complied but pending review")
                        //            item["Status"] = "Pending For Review";
                        //        else if (item["Status"].ToString() == "Complied Delayed but pending review")
                        //            item["Status"] = "Pending For Review";
                        //        else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                        //            item["Status"] = "Upcoming";
                        //        else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                        //            item["Status"] = "Overdue";
                        //        else if (item["Status"].ToString() == "Not Complied")
                        //            item["Status"] = "Rejected";
                        //    }

                        //    ExcelData.Columns.Remove("ComplianceStatusID");
                        //    ExcelData.Columns.Remove("ScheduledOnID");
                        //    ExcelData.Columns.Remove("ComplianceInstanceID");
                        //    ExcelData.Columns.Remove("Risk");
                        //}
                        //else if (Session["ComplianceType"].ToString() == "Statutory CheckList")
                        //{

                        //    ExcelData = view.ToTable("Selected", false, "ComplianceID", "Branch", "Name", "ComCategoryName", "ComSubTypeName", "ShortForm", "RequiredForms", "TextFrequency", "ForMonth", "ScheduledOn", "Risk", "CloseDate", "Status", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID");
                        //    ExcelData.Columns.Add("RiskName");

                        //    //ExcelData.Columns.Add("Performer");
                        //    //ExcelData.Columns.Add("Reviewer");
                        //    ////ExcelData.Columns.Add("Approver");

                        //    foreach (DataRow item in ExcelData.Rows)
                        //    {
                        //        //item["Performer"] = GetPerformer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                        //        //item["Reviewer"] = GetReviewer(masterlstComAssignCustomerWise, Convert.ToInt32(item["ComplianceInstanceID"].ToString()));

                        //        if (item["Risk"].ToString() == "0")
                        //            item["RiskName"] = "High";
                        //        else if (item["Risk"].ToString() == "1")
                        //            item["RiskName"] = "Medium";
                        //        else if (item["Risk"].ToString() == "2")
                        //            item["RiskName"] = "Low";

                        //        if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                        //            item["Status"] = "Upcoming";
                        //        else if (item["Status"].ToString() == "Open" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                        //            item["Status"] = "Overdue";
                        //        else if (item["Status"].ToString() == "Complied but pending review")
                        //            item["Status"] = "Pending For Review";
                        //        else if (item["Status"].ToString() == "Complied Delayed but pending review")
                        //            item["Status"] = "Pending For Review";
                        //        else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date > CurrentDate)
                        //            item["Status"] = "Upcoming";
                        //        else if (item["Status"].ToString() == "In Progress" && Convert.ToDateTime(item["ScheduledOn"]).Date <= CurrentDate)
                        //            item["Status"] = "Overdue";
                        //        else if (item["Status"].ToString() == "Not Complied")
                        //            item["Status"] = "Rejected";
                        //    }

                        //    ExcelData.Columns.Remove("ComplianceStatusID");
                        //    ExcelData.Columns.Remove("ComSubTypeName");
                        //    ExcelData.Columns.Remove("ScheduledOnID");
                        //    ExcelData.Columns.Remove("ComplianceInstanceID");
                        //    ExcelData.Columns.Remove("Risk");
                        //}
                        #endregion

                        #endregion
                    }
                    #endregion
                    //}

                    if (ExcelData.Rows.Count > 0)
                    {
                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";

                        exWorkSheet.Cells["B2:C2"].Merge = true;

                        exWorkSheet.Cells["B2"].Value = "Report of " + Session["ComplianceTypeReviewer"].ToString() + " Compliances";

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";
                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        //Load Data From Excel
                        exWorkSheet.Cells["A6"].LoadFromDataTable(ExcelData, false);
                    }

                    //if (AuthenticationHelper.Role.Equals("HMGMT")
                    //    || AuthenticationHelper.Role.Equals("HMGR")
                    //    || AuthenticationHelper.Role.Equals("LSPOC")
                    //    || AuthenticationHelper.Role.Equals("HAPPR"))
                    //{
                        #region Managment
                        if (Session["ComplianceType"].ToString() == "Statutory")
                        {
                            exWorkSheet.Cells["A5"].Value = "ComplianceID";
                            exWorkSheet.Cells["B5"].Value = "Entity / Branch"; 
                            exWorkSheet.Cells["C5"].Value = "Act";
                            exWorkSheet.Cells["D5"].Value = "Compliance";
                            exWorkSheet.Cells["E5"].Value = "Forms";
                            exWorkSheet.Cells["F5"].Value = "Frequency";
                            exWorkSheet.Cells["G5"].Value = "Period";
                            exWorkSheet.Cells["H5"].Value = "Due Date";
                        
                            exWorkSheet.Cells["I5"].Value = "Status";
                           
                        exWorkSheet.Cells.AutoFitColumns();
                    }
                    #endregion
                    // }

                    //Heading
                    #region  Heading
                    if (!(filter.Equals("CategoryByEntity") || filter.Equals("RiskByEntity")))
                    {
                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                      
                        exWorkSheet.Cells.AutoFitColumns();
                        int colIdx = 0;

                        if (ClickChangeflag == "P")
                        {
                            if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased" || Session["ComplianceType"].ToString() == "Statutory CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("ComplianceID") + 1;
                              
                            }
                            else if (Session["ComplianceType"].ToString() == "Internal" || Session["ComplianceType"].ToString() == "Internal CheckList")
                            {
                                colIdx = ExcelData.Columns.IndexOf("InternalComplianceID") + 1;
                              
                            }
                        }

                        colIdx = ExcelData.Columns.IndexOf("Branch") + 1;
                        colIdx = ExcelData.Columns.IndexOf("Name") + 1;
                        exWorkSheet.Column(colIdx).Width = 75;
                        colIdx = ExcelData.Columns.IndexOf("ShortForm") + 1;
                        colIdx = ExcelData.Columns.IndexOf("RequiredForms") + 1;
                        colIdx = ExcelData.Columns.IndexOf("TextFrequency") + 1;
                        colIdx = ExcelData.Columns.IndexOf("ForMonth") + 1;
                        
                        colIdx = ExcelData.Columns.IndexOf("ScheduledOn") + 1;
                        colIdx = ExcelData.Columns.IndexOf("InternalScheduledOn") + 1;
                        colIdx = ExcelData.Columns.IndexOf("Status") + 1;
                        
                        exWorkSheet.Column(colIdx).AutoFit();
                        //if (AuthenticationHelper.Role.Equals("HMGMT")
                        //|| AuthenticationHelper.Role.Equals("HMGR")
                        //|| AuthenticationHelper.Role.Equals("LSPOC")
                        //|| AuthenticationHelper.Role.Equals("HAPPR"))
                        //{
                        #region Managment
                        if (Session["ComplianceType"].ToString() == "Statutory")
                            {
                                using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 9])
                                {
                                    col.Style.WrapText = true;
                                    // col.Style.Numberformat.Format = "dd/MMM/yyyy";
                                    col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                                    //col.AutoFitColumns();

                                    // Assign borders
                                    col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    col[5, 2, 5 + ExcelData.Rows.Count, 9].Style.Numberformat.Format = "dd/MMM/yyyy";
                                }
                            }
                            #endregion
                        //}

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        if (ClickChangeflag == "P")
                            Response.AddHeader("content-disposition", "attachment;filename=" + Session["ComplianceType"].ToString() + "_CannedReport.xlsx");
                        else
                            Response.AddHeader("content-disposition", "attachment;filename=" + Session["ComplianceTypeReviewer"].ToString() + "_CannedReport.xlsx");

                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string GetPerformer(List<SP_GetCannedReportCompliancesSummary_Result> masterListComAssign, long complianceinstanceid)
        {
            try
            {
                string result = "";
                if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased")
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                   // result = RLCSManagement.GetUserName(masterListComAssign, complianceinstanceid, 3);
                }
                else
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                    //result = RLCSManagement.GetUserName(masterListComAssign, complianceinstanceid, 3);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        protected string GetReviewer(List<SP_GetCannedReportCompliancesSummary_Result> masterListComAssign, long complianceinstanceid)
        {
            try
            {
                string result = "";

                if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased")
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                    //result = RLCSManagement.GetUserName(masterListComAssign, complianceinstanceid, 4);
                }
                else
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                    //result = RLCSManagement.GetUserName(masterListComAssign, complianceinstanceid, 4);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetReviewer(long complianceinstanceid)
        {
            try
            {
                string result = "";

                if (Session["ComplianceType"].ToString() == "Statutory" || Session["ComplianceType"].ToString() == "EventBased")
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                }
                else
                {
                    result = DashboardManagement.GetUserName(complianceinstanceid, 4);
                }
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        protected string GetApprover(List<SP_GetCannedReportCompliancesSummary_Result> masterListComAssign, long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 6);
                //result = RLCSManagement.GetUserName(masterListComAssign, complianceinstanceid, 6);
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    branchList.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                branchList.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }
    }
}