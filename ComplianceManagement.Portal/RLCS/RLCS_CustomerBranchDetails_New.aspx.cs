﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Models;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_CustomerBranchDetails_New : System.Web.UI.Page
    {
        protected static int CustomerID;
        protected static string CustomerName;
        protected static string establishmenttype;
        protected static string acttype;
        protected static int CustomerBranchID;

        protected void Page_Load(object sender, EventArgs e)
        {
            vsEntityBranchPage.CssClass = "alert alert-danger";
            if (!IsPostBack)
            {
                try
                {
                    if (!String.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                    {
                        CustomerID = Convert.ToInt32(Request.QueryString["CustomerID"]);
                    }

                    if (!String.IsNullOrEmpty(Request.QueryString["Mode"]))
                    {
                        ViewState["Mode"] = Convert.ToInt32(Request.QueryString["Mode"]);

                        if ((int)ViewState["Mode"] == 0)
                        {
                            if (CustomerID > 0)
                            {
                                GetCustomerDetails(CustomerID);
                            }

                            btnSave.Enabled = false;
                            lblCheckClient.Visible = true;
                        }

                        if ((int)ViewState["Mode"] == 1)
                        {
                            txtClientID.Enabled = false;
                            lblCheckClient.Visible = false;
                            btnCheck.Enabled = false;

                            btnSave.Enabled = true;
                        }
                    }

                    if (CustomerID > 0)
                    {
                        ViewState["CorpID"] = RLCS_Master_Management.GetCorporateIDByCustID(CustomerID);

                        int complianceProductType = 0;
                        complianceProductType = RLCS_Master_Management.GetComplianceProductType(CustomerID);
                        ViewState["ComplianceProductType"] = complianceProductType;

                        //GetCustomerDetails(CustomerID);
                    }

                    BindCustomerStatus();
                    BindLegalRelationShips(false);
                    BindIndustry();
                    BindStates();

                    BindLegalEntityType();
                    BindCompanyTypeType();

                    BindWagePeriodfrom();
                    BindWagePeriodTo();
                    BindPaymentDay();

                    if (!string.IsNullOrEmpty(Request.QueryString["BranchID"]))
                    {
                        CustomerBranchID = Convert.ToInt32(Request.QueryString["BranchID"]);

                        if (CustomerBranchID > 0)
                            EditBranch();
                    }

                    divLegalEntityType.Visible = false;
                    divLegalRelationship.Visible = false;
                    txtServicedate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvEntityBranchPage.IsValid = false;
                    cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }

        public void EditBranch()
        {
            try
            {
                CustomerBranch customerBranch = CustomerBranchManagement.GetByID(CustomerBranchID);

                if (customerBranch != null)
                {
                    if (!string.IsNullOrEmpty(customerBranch.Name))
                    {
                        tbxName.Text = customerBranch.Name;
                        tbxName.Enabled = false;
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Type)))
                    {
                        if (customerBranch.Type != -1)
                        {
                            ddlType.SelectedValue = customerBranch.Type.ToString();
                            ddlType_SelectedIndexChanged(null, null);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalRelationShipID)))
                    {
                        if (customerBranch.LegalRelationShipID != -1 && customerBranch.LegalRelationShipID != 0)
                        {
                            ddlLegalRelationShip.SelectedValue = (customerBranch.LegalRelationShipID ?? -1).ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.LegalEntityTypeID)))
                    {
                        if (customerBranch.LegalEntityTypeID != -1 && customerBranch.LegalEntityTypeID != 0)
                        {
                            ddlLegalEntityType.SelectedValue = (customerBranch.LegalEntityTypeID ?? -1).ToString();
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.ComType)))
                    {
                        if (customerBranch.ComType != 0 && customerBranch.ComType != -1)
                        {
                            ddlCompanyType.SelectedValue = (customerBranch.ComType).ToString();
                        }
                    }

                    tbxAddressLine1.Text = customerBranch.AddressLine1;
                    tbxAddressLine2.Text = customerBranch.AddressLine2;

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.StateID)))
                    {
                        if (customerBranch.StateID != -1)
                        {
                            ddlState.SelectedValue = customerBranch.StateID.ToString();
                            ddlState_SelectedIndexChanged(null, null);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.CityID)))
                    {
                        if (customerBranch.CityID != -1)
                        {
                            ddlCity.SelectedValue = customerBranch.CityID.ToString();
                            ddlCity_SelectedIndexChanged(null, null);
                        }
                    }

                    tbxOther.Text = customerBranch.Others;
                    tbxPinCode.Text = customerBranch.PinCode;
                    tbxContactPerson.Text = customerBranch.ContactPerson;
                    tbxLandline.Text = customerBranch.Landline;
                    tbxMobile.Text = customerBranch.Mobile;
                    tbxEmail.Text = customerBranch.EmailID;

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.Status)))
                    {
                        if (customerBranch.Status != -1)
                        {
                            ddlCustomerStatus.SelectedValue = Convert.ToString(customerBranch.Status);
                        }
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(customerBranch.AuditPR)))
                    {
                        if (customerBranch.AuditPR == false)
                        {
                            ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(0);
                        }
                        else
                        {
                            ddlPersonResponsibleApplicable.SelectedValue = Convert.ToString(1);
                        }
                    }
                }

                RLCS_CustomerBranch_ClientsLocation_Mapping RLCS_CustBranch = RLCS_ClientsManagement.GetClientInfoByID(CustomerBranchID, "E");
                if (RLCS_CustBranch != null)
                {
                    txtClientID.Text = RLCS_CustBranch.CM_ClientID;

                    txtBonusP.Text = Convert.ToString(RLCS_CustBranch.CM_BonusPercentage);
                    txtBonusP.Attributes.Add("Initialvalue", Convert.ToString(RLCS_CustBranch.CM_BonusPercentage));

                    //txtServicedate.Text = Convert.ToString(RLCS_CustBranch.CM_ServiceStartDate);
                    if (RLCS_CustBranch.CM_ServiceStartDate != null)
                    {
                        var dt = Convert.ToDateTime(RLCS_CustBranch.CM_ServiceStartDate);
                        txtServicedate.Text = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(RLCS_CustBranch.CM_EstablishmentType)))
                    {
                        ddlestablishmentType.SelectedValue = (RLCS_CustBranch.CM_EstablishmentType).ToString();
                    }

                    if (RLCS_CustBranch.CM_Excemption!=null)
                    {
                       if(RLCS_CustBranch.CM_Excemption == true)
                            ddlBonusExempted.SelectedValue = "Y";
                       else
                            ddlBonusExempted.SelectedValue = "N";
                    }

                    if (RLCS_CustBranch.CM_IsPOApplicable != null)
                    {
                        if (RLCS_CustBranch.CM_IsPOApplicable == true)
                            ddlPoApplicable.SelectedValue = "Y";
                        else
                            ddlPoApplicable.SelectedValue = "N";
                    }

                    var RLCS_clientBasic = RLCS_ClientsManagement.GetClientBasicByID(Convert.ToInt32(CustomerBranchID), RLCS_CustBranch.CM_ClientID);
                    if (RLCS_clientBasic != null)
                    {
                        //txtcommencementdt.Text = Convert.ToString(RLCS_clientBasic.CB_DateOfCommencement);
                        if (RLCS_clientBasic.CB_DateOfCommencement != null)
                        {
                            var dt = Convert.ToDateTime(RLCS_clientBasic.CB_DateOfCommencement);
                            txtcommencementdt.Text = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }

                        txtPfCode.Text = Convert.ToString(RLCS_clientBasic.CM_PFCode);

                        if (!string.IsNullOrEmpty(Convert.ToString(RLCS_clientBasic.CB_ActType)))
                        {
                            ddlActApplicability.SelectedValue = (RLCS_clientBasic.CB_ActType).ToString();
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(RLCS_clientBasic.CB_WagePeriodFrom)))
                        {
                            ddlWagePFrom.SelectedValue = (RLCS_clientBasic.CB_WagePeriodFrom).ToString();
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(RLCS_clientBasic.CB_WagePeriodTo)))
                        {
                            ddlWagePTo.SelectedValue = (RLCS_clientBasic.CB_WagePeriodTo).ToString();
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(RLCS_clientBasic.CB_PaymentDate)))
                        {
                            ddlPaymentDay.SelectedValue = (RLCS_clientBasic.CB_PaymentDate).ToString();
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(RLCS_clientBasic.CB_ServiceTaxExmpted)))
                        {
                            ddlServiceTax.SelectedValue = (RLCS_clientBasic.CB_ServiceTaxExmpted).ToString();
                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(RLCS_clientBasic.CB_EDLIExemption)))
                        {
                            ddlEdliExcemption.SelectedValue = (RLCS_clientBasic.CB_EDLIExemption).ToString();
                        }
                    }
                }
                else
                    txtClientID.Text = RLCS_Master_Management.GetClientIDByBranchID(CustomerBranchID);



                //ddlIndustry.Enabled = false;
                ddlType.Enabled = false;
                //ddlLegalRelationShipOrStatus.Enabled = false;
                ddlLegalRelationShip.Enabled = false;

                if (ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 1)
                    {
                        txtClientID.Enabled = false;
                        btnCheck.Enabled = false;
                        btnSave.Enabled = true;
                    }
                }

                upCustomerBranches.Update();
                // ScriptManager.RegisterStartupScript(this, Page.GetType(), "OpenDialog", "$(\"#divCustomerBranchesDialog\").dialog('open');", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public void GetCustomerDetails(int CustomerID)
        {
            var cust = CustomerManagement.GetByID(CustomerID);
            if (cust != null)
            {
                tbxAddressLine1.Text = cust.Address;
                tbxEmail.Text = cust.BuyerEmail;
                tbxLandline.Text = cust.BuyerContactNumber;
                tbxContactPerson.Text = cust.BuyerName;
                litCustomer.Text = cust.Name;
                CustomerName = cust.Name;
            }
        }

        private void BindLegalRelationShips(bool parent)
        {
            try
            {
                ddlLegalRelationShip.DataTextField = "Name";
                ddlLegalRelationShip.DataValueField = "ID";

                var legalRelationShips = Enumerations.GetAll<LegalRelationship>();
                if (!parent)
                {
                    legalRelationShips.RemoveAt(0);
                }
                ddlLegalRelationShip.DataSource = legalRelationShips;
                ddlLegalRelationShip.DataBind();

                ddlLegalRelationShip.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindBranchTypes()
        {
            try
            {
                ddlType.DataSource = null;
                ddlType.DataBind();
                ddlType.ClearSelection();

                ddlType.DataTextField = "Name";
                ddlType.DataValueField = "ID";

                var dataSource = CustomerBranchManagement.GetAllNodeTypes();
                if (ViewState["ParentID"] == null)
                {
                    dataSource = dataSource.Where(entry => entry.ID == 1).ToList();
                }
                else
                {
                    dataSource = dataSource.Where(entry => entry.ID != 1).ToList();
                }

                ddlType.DataSource = dataSource;

                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindIndustry()
        {
            try
            {
                rptIndustry.DataSource = CustomerBranchManagement.GetAllIndustry();
                rptIndustry.DataBind();

                foreach (RepeaterItem aItem in rptIndustry.Items)
                {
                    CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");

                    if (!chkIndustry.Checked)
                    {
                        //chkIndustry.Checked = true;
                    }
                }

                //CheckBox IndustrySelectAll = (CheckBox)rptIndustry.Controls[0].Controls[0].FindControl("IndustrySelectAll");
                //IndustrySelectAll.Checked = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindCities()
        {
            try
            {
                ddlCity.Items.Clear();

                if (ddlState.SelectedValue != "" || !string.IsNullOrEmpty(ddlState.SelectedValue) || ddlState.SelectedValue != "-1")
                {
                    //ddlCity.DataTextField = "Name";
                    //ddlCity.DataValueField = "ID";

                    //ddlCity.DataSource = AddressManagement.GetAllCitiesByState(Convert.ToInt32(ddlState.SelectedValue));
                    //ddlCity.DataBind();

                    ddlCity.DataTextField = "Name";
                    ddlCity.DataValueField = "ID";

                    ddlCity.DataSource = RLCS_Master_Management.FillLocationCityByStateID(Convert.ToInt32(ddlState.SelectedValue));
                    ddlCity.DataBind();

                    ddlCity.Items.Insert(0, new ListItem("Other", "0"));
                    ddlCity.Items.Insert(0, new ListItem("Select", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindStates()
        {
            try
            {
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "ID";

                //ddlState.DataSource = AddressManagement.GetAllStates();
                //ddlState.DataBind();

                ddlState.DataSource = RLCS_Master_Management.FillStates();
                ddlState.DataBind();

                ddlState.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindWagePeriodfrom()
        {
            try
            {
                ListItemCollection list = BindWagePeriods("");
                ddlWagePFrom.DataSource = list;
                ddlWagePFrom.DataBind();
                ddlWagePFrom.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindWagePeriodTo()
        {
            try
            {
                ListItemCollection list = BindWagePeriods("");
                ddlWagePTo.DataSource = list;
                ddlWagePTo.DataBind();
                ddlWagePTo.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindPaymentDay()
        {
            try
            {
                ListItemCollection list = BindWagePeriods("P");
                ddlPaymentDay.DataSource = list;
                ddlPaymentDay.DataTextField = "text";
                ddlPaymentDay.DataValueField = "value";
                ddlPaymentDay.DataBind();
                ddlPaymentDay.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public static ListItemCollection BindWagePeriods(string param)
        {
            ListItemCollection Period = new ListItemCollection();
           
            Period.Add(new ListItem("1", "1"));
            Period.Add(new ListItem("2", "2"));
            Period.Add(new ListItem("3", "3"));
            Period.Add(new ListItem("4", "4"));
            Period.Add(new ListItem("5", "5"));
            Period.Add(new ListItem("6", "6"));
            Period.Add(new ListItem("7", "7"));
            Period.Add(new ListItem("8", "8"));
            Period.Add(new ListItem("9", "9"));
            Period.Add(new ListItem("10", "10"));
            Period.Add(new ListItem("11", "11"));
            Period.Add(new ListItem("12", "12"));
            Period.Add(new ListItem("13", "13"));
            Period.Add(new ListItem("14", "14"));
            Period.Add(new ListItem("15", "15"));
            Period.Add(new ListItem("16", "16"));
            Period.Add(new ListItem("17", "17"));
            Period.Add(new ListItem("18", "18"));
            Period.Add(new ListItem("19", "19"));
            Period.Add(new ListItem("20", "20"));
            Period.Add(new ListItem("21", "21"));
            Period.Add(new ListItem("22", "22"));
            Period.Add(new ListItem("23", "23"));
            Period.Add(new ListItem("24", "24"));
            Period.Add(new ListItem("25", "25"));
            Period.Add(new ListItem("26", "26"));
            Period.Add(new ListItem("27", "27"));
            Period.Add(new ListItem("28", "28"));
           
            if (param == "P")//coming from payment date
            {
                Period.Add(new ListItem("Last day of month", "31"));
            }
            else
            {
                Period.Add(new ListItem("29", "29"));
                Period.Add(new ListItem("30", "30"));
                Period.Add(new ListItem("31", "31"));
            }

            return Period;
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindCities();
            ddlCity.SelectedValue = "-1";
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCity.SelectedValue == "0" && divOther.Visible == false)
                {
                    divOther.Visible = true;
                    tbxOther.Text = string.Empty;
                }
                else if (ddlCity.SelectedValue != "0" && string.IsNullOrEmpty(tbxPinCode.Text.Trim()))
                {
                    if (ddlCity.SelectedItem.Text.Contains("-"))
                    {
                        string[] city = ddlCity.SelectedItem.Text.Split('-');

                        if (city != null)
                        {
                            if (city.Length == 2)
                                tbxPinCode.Text = city[1];
                        }
                    }

                    divOther.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public void BindCustomerStatus()
        {
            try
            {
                ddlCustomerStatus.DataTextField = "Name";
                ddlCustomerStatus.DataValueField = "ID";

                ddlCustomerStatus.DataSource = Enumerations.GetAll<CustomerStatus>();
                ddlCustomerStatus.DataBind();

                if (ddlCustomerStatus.Items.FindByText("Active") != null)
                    ddlCustomerStatus.Items.FindByText("Active").Selected = true;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void tbxName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0 && ViewState["ParentID"] == null)
                    {
                        txtClientID.Text = GetClientID(tbxName.Text.Trim());
                        btnSave.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void txtClientID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["Mode"] != null)
                {
                    if ((int)ViewState["Mode"] == 0 && ViewState["ParentID"] == null)
                    {                       
                        btnSave.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        private void BindLegalEntityType()
        {
            try
            {
                ddlLegalEntityType.DataTextField = "EntityTypeName";
                ddlLegalEntityType.DataValueField = "ID";
                ddlLegalEntityType.DataSource = CustomerBranchManagement.GetAllLegalEntityType();
                ddlLegalEntityType.DataBind();
                //ddlLegalEntityType.Items.Insert(0, new ListItem("< Select >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        private void BindCompanyTypeType()
        {
            try
            {
                ddlCompanyType.DataTextField = "Name";
                ddlCompanyType.DataValueField = "ID";
                ddlCompanyType.DataSource = CustomerBranchManagement.GetAllComanyTypeCustomerBranch();
                ddlCompanyType.DataBind();
                ddlCompanyType.Items.Insert(0, new ListItem("Select", "-1"));
                ddlCompanyType.SelectedValue = "-1";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divLegalRelationship.Visible = ddlType.SelectedValue == "1";
                string industryId = string.Empty;

                //if (ViewState["IndustryId"] != null)
                //{
                //    BindLegalRelationShipOrStatus(Convert.ToInt32(ViewState["IndustryId"].ToString()));
                //    //ddlIndustry.SelectedValue = ViewState["IndustryId"].ToString();
                //}
                //if (!divIndustry.Visible)
                //{
                //    //    ddlIndustry.SelectedIndex = -1;
                //    //    if (ViewState["IndustryId"] == null)
                //    //BindLegalRelationShipOrStatus(0);
                //}
                if (!divLegalRelationship.Visible)
                {
                    ddlLegalRelationShip.SelectedIndex = -1;
                }

                if (ddlType.SelectedValue == "1")
                {
                    divLegalEntityType.Visible = true;
                    divCompanyType.Visible = true;

                    //int complianceProductType = 0;
                    //if (ViewState["ComplianceProductType"] != null)
                    //{
                    //    complianceProductType = Convert.ToInt32(ViewState["ComplianceProductType"]);
                    //}

                    //if (complianceProductType > 0)
                    //{
                    //    divClientID.Visible = true;
                    //    if (ViewState["Mode"].ToString() == "1")
                    //    {
                    //        lblCheckClient.Visible = false;
                    //    }
                    //    else
                    //        lblCheckClient.Visible = true;
                    //}
                    //else
                    //{
                    //    divClientID.Visible = false;
                    //    lblCheckClient.Visible = false;
                    //    btnSave.Enabled = true;
                    //}
                }
                else
                {
                    divLegalEntityType.Visible = false;
                    divCompanyType.Visible = false;

                    //divClientID.Visible = false;
                    //lblCheckClient.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void upCustomerBranches_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "Script", "initializeDatePicker();", true);

                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeIndustryList", string.Format("initializeJQueryUI('{0}', 'dvIndustry');", txtIndustry.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideIndustryList", "$(\"#dvIndustry\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public string GetClientID(string clientName)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                foreach (char c in clientName)
                {
                    if (result.Length < 5)
                    {
                        if (Char.IsLetterOrDigit(c))
                            result.Append(c);
                    }
                }

                return "AVA" + result.ToString().ToUpper();
            }
            catch (Exception ex)
            {
                return result.ToString();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showLoader;", "showLoader();", true);

                    bool saveSuccess = false;
                    bool apiSuccess = false;
                    int avacom_CustomerID = 0;
                    int type = 0;
                    if (!string.IsNullOrEmpty(txtClientID.Text))
                    {
                        if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                        {
                            if (ddlType.SelectedValue == "1")
                            {
                                if (ddlCompanyType.SelectedValue == "-1")
                                {
                                    type = 1;
                                    cvEntityBranchPage.ErrorMessage = "Please select Type.";
                                    cvEntityBranchPage.IsValid = false;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            divLegalRelationship.Visible = ddlType.SelectedValue == "1";
                            type = 1;
                        }
                        if (ddlCompanyType.SelectedValue == "-1")
                        {
                            cvEntityBranchPage.ErrorMessage = "Please select Type.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }
                        if (string.IsNullOrEmpty(txtBonusP.Text))
                        {
                            cvEntityBranchPage.ErrorMessage = "Please enter bonus percentage.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }
                        if (string.IsNullOrEmpty(txtPfCode.Text))
                        {
                            cvEntityBranchPage.ErrorMessage = "Please enter pf code.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }
                        if (string.IsNullOrEmpty(txtServicedate.Text))
                        {
                            cvEntityBranchPage.ErrorMessage = "Please select service date.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }
                        if (string.IsNullOrEmpty(txtcommencementdt.Text))
                        {
                            cvEntityBranchPage.ErrorMessage = "Please select commencement date.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }
                        int comType = 0;
                        if (!string.IsNullOrEmpty(ddlCompanyType.SelectedValue))
                        {
                            if (ddlCompanyType.SelectedValue.ToString() == "-1")
                            {
                                comType = 0;
                            }
                            else
                            {
                                comType = Convert.ToInt32(ddlCompanyType.SelectedValue);
                            }
                        }
                        if (ddlWagePFrom.SelectedIndex <= 0)
                        {
                            cvEntityBranchPage.ErrorMessage = "Please select wage period from.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }
                        if (ddlWagePTo.SelectedIndex <= 0)
                        {
                            cvEntityBranchPage.ErrorMessage = "Please select wage period to.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }

                        if (ddlPaymentDay.SelectedIndex <= 0)
                        {
                            cvEntityBranchPage.ErrorMessage = "Please select payment day.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }
                        if (ddlestablishmentType.SelectedIndex > 0)
                        {
                            establishmenttype = ddlestablishmentType.SelectedValue;
                        }
                        else
                        {
                            cvEntityBranchPage.ErrorMessage = "Please select establishment type.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }

                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide&showscript", "myShow();", true);
                        // if (ddlActApplicability.SelectedIndex > 0)
                        // {
                        acttype = ddlActApplicability.SelectedValue;
                        // }
                        // else
                        // {
                        //   cvEntityBranchPage.ErrorMessage = "Please select act applicability type.";
                        //   cvEntityBranchPage.IsValid = false;
                        //   return;
                        // }

                        CustomerBranch customerBranch = new CustomerBranch()
                        {
                            Name = tbxName.Text.Trim(),
                            Type = Convert.ToByte(type),
                            ComType = Convert.ToByte(comType),
                            AddressLine1 = tbxAddressLine1.Text.Trim(),
                            AddressLine2 = tbxAddressLine2.Text.Trim(),
                            StateID = Convert.ToInt32(ddlState.SelectedValue),
                            CityID = Convert.ToInt32(ddlCity.SelectedValue),
                            Others = tbxOther.Text.Trim(),
                            PinCode = tbxPinCode.Text.Trim(),
                            //Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                            ContactPerson = tbxContactPerson.Text.Trim(),
                            Landline = tbxLandline.Text.Trim(),
                            Mobile = tbxMobile.Text.Trim(),
                            EmailID = tbxEmail.Text.Trim(),
                            CustomerID = Convert.ToInt32(CustomerID),
                            ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                            Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                        };

                        avacom_CustomerID = customerBranch.CustomerID;

                        if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                        {
                            customerBranch.AuditPR = true;
                        }
                        if (ddlType.SelectedValue == "1")
                        {
                            if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                            {
                                customerBranch.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                            }
                            if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                            {
                                customerBranch.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                            }
                        }
                        else
                        {
                            customerBranch.LegalRelationShipID = null;
                            customerBranch.LegalEntityTypeID = null;
                        }

                        if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                        {
                            if (ddlLegalRelationShip.SelectedValue != "-1")
                                customerBranch.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShip.SelectedValue);
                            else
                                customerBranch.LegalRelationShipOrStatus = 0;
                        }
                        else
                        {
                            customerBranch.LegalRelationShipOrStatus = 0;
                        }

                        if ((int)ViewState["Mode"] == 1)
                        {
                            //customerBranch.ID = Convert.ToInt32(ViewState["CustomerBranchID"]);
                            customerBranch.ID = CustomerBranchID;
                        }

                        if (CustomerBranchManagement.Exists(customerBranch, Convert.ToInt32(ViewState["CustomerID"])))
                        {
                            cvEntityBranchPage.ErrorMessage = "Customer branch name already exists.";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }
                        if ((int)ViewState["Mode"] == 0)
                        {
                            ////GG ADD 29July2020
                            bool Valid = false;
                            int NewRecord = 1;
                            Valid = ICIAManagement.CheckForEntityValidation(NewRecord, CustomerID);
                            if (Valid)
                            {
                                int resultData = 0;
                                resultData = CustomerBranchManagement.Create(customerBranch);

                                if (resultData > 0)
                                {
                                    saveSuccess = true;
                                    // Added by SACHIN 28 April 2016
                                    string ReplyEmailAddressName = "Avantis";
                                    string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EMailTemplate_NewCustomerBranchCreaded
                                                        .Replace("@NewCustomer", CustomerName)
                                                        .Replace("@BranchName", tbxName.Text)
                                                        .Replace("@LoginUser", AuthenticationHelper.User)
                                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                                        .Replace("@From", ReplyEmailAddressName)
                                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));

                                    string SenderEmailAddress = ConfigurationManager.AppSettings["SenderEmailAddress"].ToString();
                                    string CustomerCreatedEmail = ConfigurationManager.AppSettings["CustomerCreatedEmail"].ToString();

                                    //EmailManager.SendMail(SenderEmailAddress, new List<String>(new String[] { CustomerCreatedEmail }), null, null, "AVACOM-New Branch Added", message);
                                }
                            }
                            else
                            {
                                cvEntityBranchPage.ErrorMessage = "Facility to create more entities is not available in the Free version. Kindly contact Avantis to activate the same.";
                                cvEntityBranchPage.IsValid = false;
                                return;
                            }
                            ////END
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            saveSuccess = CustomerBranchManagement.Update(customerBranch);
                        }

                        com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch customerBranch1 = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_CustomerBranch()
                        {
                            Name = tbxName.Text.Trim(),
                            Type = Convert.ToByte(type),
                            ComType = Convert.ToByte(comType),
                            AddressLine1 = tbxAddressLine1.Text.Trim(),
                            AddressLine2 = tbxAddressLine2.Text.Trim(),
                            StateID = Convert.ToInt32(ddlState.SelectedValue),
                            CityID = Convert.ToInt32(ddlCity.SelectedValue),
                            Others = tbxOther.Text.Trim(),
                            PinCode = tbxPinCode.Text.Trim(),
                            //Industry = Convert.ToInt32(ddlIndustry.SelectedValue),
                            ContactPerson = tbxContactPerson.Text.Trim(),
                            Landline = tbxLandline.Text.Trim(),
                            Mobile = tbxMobile.Text.Trim(),
                            EmailID = tbxEmail.Text.Trim(),
                            CustomerID = Convert.ToInt32(CustomerID),
                            ParentID = ViewState["ParentID"] == null ? (int?)null : Convert.ToInt32(ViewState["ParentID"]),
                            Status = Convert.ToInt32(ddlCustomerStatus.SelectedValue)
                        };

                        if (ddlPersonResponsibleApplicable.SelectedItem.Text == "Yes")
                        {
                            customerBranch1.AuditPR = true;
                        }

                        if (ddlType.SelectedValue == "1")
                        {
                            if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                            {
                                customerBranch1.LegalRelationShipID = Convert.ToInt32(ddlLegalRelationShip.SelectedValue);
                            }
                            if (!string.IsNullOrEmpty(ddlLegalEntityType.SelectedValue))
                            {
                                customerBranch1.LegalEntityTypeID = Convert.ToInt32(ddlLegalEntityType.SelectedValue);
                            }
                        }
                        else
                        {
                            customerBranch1.LegalRelationShipID = null;
                            customerBranch1.LegalEntityTypeID = null;
                        }

                        //if (!string.IsNullOrEmpty(ddlLegalRelationShip.SelectedValue))
                        //{
                        //    if (ddlLegalRelationShipOrStatus.SelectedValue != "-1")
                        //        customerBranch1.LegalRelationShipOrStatus = Convert.ToByte(ddlLegalRelationShipOrStatus.SelectedValue);
                        //    else
                        //        customerBranch1.LegalRelationShipOrStatus = 0;
                        //}
                        //else
                        //{
                        //    customerBranch1.LegalRelationShipOrStatus = 0;
                        //}

                        if ((int)ViewState["Mode"] == 1)
                        {
                            customerBranch1.ID = CustomerBranchID;
                            //Convert.ToInt32(ViewState["CustomerBranchID"]);
                        }

                        if (CustomerBranchManagement.Exists1(customerBranch1, Convert.ToInt32(ViewState["CustomerID"])))
                        {
                            cvEntityBranchPage.ErrorMessage = "Branch with same name already exists";
                            cvEntityBranchPage.IsValid = false;
                            return;
                        }

                        if ((int)ViewState["Mode"] == 0)
                        {
                            saveSuccess = CustomerBranchManagement.CreateRLCS(customerBranch1);

                            //---------add Industry--------------------------------------------
                            List<int> IndustryIds = new List<int>();
                            foreach (RepeaterItem aItem in rptIndustry.Items)
                            {
                                CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                                if (chkIndustry.Checked)
                                {
                                    IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));

                                    CustomerBranchIndustryMapping IndustryMapping = new CustomerBranchIndustryMapping()
                                    {
                                        CustomerBranchID = customerBranch1.ID,
                                        IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                        IsActive = true,
                                        EditedDate = DateTime.UtcNow,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                    };
                                    Business.ComplianceManagement.CreateCustomerBranchIndustryMapping(IndustryMapping);
                                }
                            }
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            saveSuccess = CustomerBranchManagement.UpdateRLCS(customerBranch1);

                            //---------add Industry--------------------------------------------
                            List<int> IndustryIds = new List<int>();
                            Business.ComplianceManagement.UpdateCustomerBranchIndustryMappingMappedID(customerBranch1.ID);
                            foreach (RepeaterItem aItem in rptIndustry.Items)
                            {
                                CheckBox chkIndustry = (CheckBox)aItem.FindControl("chkIndustry");
                                if (chkIndustry.Checked)
                                {
                                    IndustryIds.Add(Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()));

                                    CustomerBranchIndustryMapping IndustryMapping = new CustomerBranchIndustryMapping()
                                    {
                                        CustomerBranchID = customerBranch1.ID,
                                        IndustryID = Convert.ToInt32(((Label)aItem.FindControl("lblIndustryID")).Text.Trim()),
                                        IsActive = true,
                                        EditedDate = DateTime.UtcNow,
                                        EditedBy = Convert.ToInt32(Session["userID"]),
                                    };

                                    Business.ComplianceManagement.CreateCustomerBranchIndustryMapping(IndustryMapping);
                                }
                            }
                        }

                        //Create or Update CustomerBranch in RLCS DB
                        int complianceProductType = 0;
                        if (ViewState["ComplianceProductType"] != null)
                        {
                            complianceProductType = Convert.ToInt32(ViewState["ComplianceProductType"]);
                        }

                        if (complianceProductType >= 0)
                        {
                            string stateCode = string.Empty, cityCode = string.Empty;
                            string corpID = string.Empty;
                            string clientID = string.Empty;

                            if (ViewState["CorpID"] != null)
                                corpID = Convert.ToString(ViewState["CorpID"]);

                            if (ViewState["ParentID"] == null)
                                clientID = txtClientID.Text.Trim();
                            else
                            {
                                if (ViewState["EntityClientID"] != null)
                                    clientID = Convert.ToString(ViewState["EntityClientID"]);
                            }

                            if (saveSuccess && !string.IsNullOrEmpty(corpID) && !string.IsNullOrEmpty(clientID) && avacom_CustomerID != 0)
                            {
                                RLCS_CustomerBranch_ClientsLocation_Mapping RLCS_CustomerBranch = new RLCS_CustomerBranch_ClientsLocation_Mapping()
                                {
                                    AVACOM_CustomerID = avacom_CustomerID,
                                    AVACOM_BranchID = customerBranch.ID,
                                    AVACOM_BranchName = customerBranch.Name,
                                    BranchType = "E",
                                    CM_Address = customerBranch.AddressLine1,
                                    CO_CorporateID = corpID,
                                    CM_ClientID = clientID,
                                    CM_ClientName = customerBranch.Name,
                                    CM_IsAventisClientOrBranch= complianceProductType,
                                };

                                if (ViewState["ParentID"] == null)
                                {
                                    RLCS_CustomerBranch.BranchType = "E"; 

                                    if (ddlBonusExempted.SelectedValue == "Y")
                                        RLCS_CustomerBranch.CM_Excemption = true;
                                    else if (ddlBonusExempted.SelectedValue == "N")
                                        RLCS_CustomerBranch.CM_Excemption = false;
                                    else
                                        RLCS_CustomerBranch.CM_Excemption = null;

                                    if (ddlPoApplicable.SelectedValue == "Y")
                                        RLCS_CustomerBranch.CM_IsPOApplicable = true;
                                    else if (ddlPoApplicable.SelectedValue == "N")
                                        RLCS_CustomerBranch.CM_IsPOApplicable = false;
                                    else
                                        RLCS_CustomerBranch.CM_IsPOApplicable = null;

                                }
                                else
                                {
                                    RLCS_CustomerBranch.BranchType = "B";
                                }

                                stateCode = RLCS_Master_Management.GetStateCodeByStateID(customerBranch.StateID);

                                if (!string.IsNullOrEmpty(stateCode))
                                    RLCS_CustomerBranch.CM_State = stateCode;

                                cityCode = RLCS_Master_Management.GetCityCodeByCityID(customerBranch.CityID);

                                if (!string.IsNullOrEmpty(cityCode))
                                {
                                    RLCS_CustomerBranch.CM_City = cityCode;
                                    RLCS_CustomerBranch.CM_Pincode = cityCode;
                                    RLCS_CustomerBranch.CL_Pincode = cityCode;
                                }

                                if (ddlCustomerStatus.SelectedItem.Text.Equals("Active"))
                                    RLCS_CustomerBranch.CM_Status = "A";
                                else
                                    RLCS_CustomerBranch.CM_Status = "I";

                                RLCS_CustomerBranch.CM_EstablishmentType = establishmenttype;
                                RLCS_CustomerBranch.CM_ActType = acttype;

                                if (!string.IsNullOrEmpty(txtBonusP.Text))
                                    RLCS_CustomerBranch.CM_BonusPercentage = Convert.ToDecimal(txtBonusP.Text);

                                //if (chkexcemption.Checked)
                                //    RLCS_CustomerBranch.CM_Excemption = true;
                                //else
                                //    RLCS_CustomerBranch.CM_Excemption = false;

                                if (!string.IsNullOrEmpty(txtServicedate.Text))
                                    RLCS_CustomerBranch.CM_ServiceStartDate = DateTimeExtensions.GetDate(txtServicedate.Text);

                                bool save = RLCSManagement.CreateUpdate_CustomerBranch_ClientsOrLocation_Mapping(RLCS_CustomerBranch);
                                if (save)
                                {
                                    RLCS_Client_BasicDetails Client_BasicDetails = new RLCS_Client_BasicDetails()
                                    {
                                        CB_ClientID = clientID,
                                        AVACOM_BranchID = customerBranch.ID,
                                        CB_ActType = RLCS_CustomerBranch.CM_ActType
                                    };

                                    if (ddlWagePFrom.SelectedIndex > 0)
                                        Client_BasicDetails.CB_WagePeriodFrom = ddlWagePFrom.SelectedItem.Value;

                                    if (ddlWagePTo.SelectedIndex > 0)
                                        Client_BasicDetails.CB_WagePeriodTo = ddlWagePTo.SelectedItem.Value;

                                    if (ddlPaymentDay.SelectedIndex > 0)
                                        Client_BasicDetails.CB_PaymentDate = ddlPaymentDay.SelectedItem.Value;

                                    if (!string.IsNullOrEmpty(txtcommencementdt.Text))
                                        Client_BasicDetails.CB_DateOfCommencement = DateTimeExtensions.GetDate(txtcommencementdt.Text);

                                    if (!string.IsNullOrEmpty(txtPfCode.Text))
                                    {
                                        Client_BasicDetails.CM_PFCode = txtPfCode.Text;
                                        Client_BasicDetails.CB_PF_Code = txtPfCode.Text;
                                    }

                                    if (ddlServiceTax.SelectedIndex != -1 && ddlServiceTax.SelectedIndex > 0)
                                        Client_BasicDetails.CB_ServiceTaxExmpted = ddlServiceTax.SelectedItem.Value;

                                    if (ddlEdliExcemption.SelectedIndex != -1 && ddlEdliExcemption.SelectedIndex > 0)
                                        Client_BasicDetails.CB_EDLIExemption = ddlEdliExcemption.SelectedItem.Value;

                                    var update = RLCS_ClientsManagement.UpdateClientBasicInfo(Client_BasicDetails);

                                    if (update)
                                    {
                                        jsonClientDetails jsonClientDetails = new jsonClientDetails()
                                        {
                                            ClientId = clientID,
                                            State = stateCode,
                                            CorporateId = corpID,
                                            City = RLCS_CustomerBranch.CM_City,
                                            Mandate = "",
                                            ClientName = customerBranch.Name,
                                            AgreementID = "",
                                            ActApplicabilty = Client_BasicDetails.CB_ActType,
                                            DateOfCommencement = Client_BasicDetails.CB_DateOfCommencement,
                                            EDLIExcemption = Client_BasicDetails.CB_EDLIExemption,
                                            PaymentDate = Client_BasicDetails.CB_PaymentDate,
                                            EstablishmentType = establishmenttype,
                                            WagePeriodFrom = Client_BasicDetails.CB_WagePeriodFrom,
                                            WagePeriodTo = Client_BasicDetails.CB_WagePeriodTo,
                                            Address = customerBranch.AddressLine1,
                                            BonusPercentage = (decimal)RLCS_CustomerBranch.CM_BonusPercentage,
                                            //Excemption = RLCS_CustomerBranch.CM_Excemption.Value,
                                            ServiceTaxExcempted = Client_BasicDetails.CB_ServiceTaxExmpted,
                                            ServiceStartDate = RLCS_CustomerBranch.CM_ServiceStartDate,
                                            Status = RLCS_CustomerBranch.CM_Status,
                                            PFCode = Client_BasicDetails.CM_PFCode,
                                            ClientFlag=RLCS_CustomerBranch.CM_IsAventisClientOrBranch,
                                            POApplicabilty= RLCS_CustomerBranch.CM_IsPOApplicable,
                                            excemption = RLCS_CustomerBranch.CM_Excemption,
                                            CreatedBy = "Avantis",
                                            ModifiedBy = "Avantis",
                                        };
                                        SetupController setupcontroller = new SetupController();
                                        apiSuccess = setupcontroller.ClientBasicApiCall(jsonClientDetails);
                                        if (apiSuccess)
                                            RLCS_ClientsManagement.Update_ProcessedStatus_ClientBasicDetail(Client_BasicDetails.AVACOM_BranchID, Client_BasicDetails.CB_ClientID, saveSuccess);

                                    }

                                    if (update || apiSuccess)
                                    {
                                        cvEntityBranchPage.ErrorMessage = "Details Save Successfully";
                                        cvEntityBranchPage.IsValid = false;
                                        vsEntityBranchPage.CssClass = "alert alert-success";
                                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hide&showscript2", "Hideshow();", true);
                                    }
                                }
                                //RLCSManagement.AddRLCSCustBranch(RLCS_CustomerBranch);
                            }
                        }

                        //ScriptManager.RegisterStartupScript(this, Page.GetType(), "CloseDialog", "$(\"#divCustomerBranchesDialog\").dialog('close')", true);
                        //f BindCustomerBranches();
                        upCustomerBranches.Update();
                    }
                    else
                    {
                        cvEntityBranchPage.ErrorMessage = "Required ClientID";
                        cvEntityBranchPage.IsValid = false;
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEntityBranchPage.IsValid = false;
                cvEntityBranchPage.ErrorMessage = "Something went wrong, Please try again";
            }
            finally
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hideLoader", "hideLoader();", true);
            }
        }
  
        protected void btnCheck_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtClientID.Text))
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    bool clientExists = false;
                    bool entityExists = false;

                    string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                    rlcsAPIURL += "AventisIntegration/CheckClientIdExists?ClientId=" + txtClientID.Text.Trim();

                    string responseData = RLCSAPIClasses.Invoke("GET", rlcsAPIURL, "");

                    if (responseData != null)
                    {
                        string data = responseData;
                        if (!string.IsNullOrWhiteSpace(data))
                            clientExists = Convert.ToBoolean(data);
                        else
                            clientExists = false;
                        if (clientExists)
                        {
                            cvEntityBranchPage.IsValid = false;
                            cvEntityBranchPage.ErrorMessage = "Entity/Client with Same ClientID already exists, Please choose other";
                            btnSave.Enabled = false;
                        }
                        else
                        {
                            entityExists = RLCS_Master_Management.Exists_CorporateID(txtClientID.Text.Trim());
                            if (entityExists)
                            {
                                cvEntityBranchPage.IsValid = false;
                                cvEntityBranchPage.ErrorMessage = "Entity with this ClientID already exists, Please choose other";
                                btnSave.Enabled = false;
                            }
                            else
                            {
                                clientExists = RLCS_Master_Management.Exists_ClientID(txtClientID.Text.Trim());
                                if (clientExists)
                                {
                                    cvEntityBranchPage.IsValid = false;
                                    cvEntityBranchPage.ErrorMessage = "Entity/Client with Same ClientID already exists, Please choose other";
                                    btnSave.Enabled = false;
                                }
                                else
                                {
                                    cvEntityBranchPage.IsValid = false;
                                    cvEntityBranchPage.ErrorMessage = "ClientID Available, Please proceed further";
                                    vsEntityBranchPage.CssClass = "alert alert-success";
                                    btnSave.Enabled = true;
                                }
                                    
                            }
                            
                            lblCheckClient.Visible = false;
                        }
                    }
                    else
                        btnSave.Enabled = false;
                }
            }
            else
                btnSave.Enabled = false;
        }


    }

}