﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_SharingPercentage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCustomers(); 

                if (!string.IsNullOrEmpty(Request.QueryString["CustID"]))
                {
                    if (ddlCustomer.Items.FindByValue(Request.QueryString["CustID"]) != null)
                    {
                        ddlCustomer.ClearSelection();
                        ddlCustomer.Items.FindByValue(Request.QueryString["CustID"]).Selected = true;

                        if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                            BindGrid_With_Data(Convert.ToInt32(ddlCustomer.SelectedValue));
                    }
                }
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                BindGrid_With_Data(Convert.ToInt32(ddlCustomer.SelectedValue));
            else
                BindGrid_With_Data(0);
        }

        public void BindGrid_With_Data(int customerID)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var cust = (from row in entities.RLCS_SharingPercentage
                                where row.CustomerID == customerID
                                && row.IsActive == true
                                select row).ToList();

                    if (cust.Count > 0)
                    {
                        grd_details.DataSource = cust;
                        grd_details.DataBind();
                    }
                    else
                    {
                        grd_details.DataSource = null;
                        grd_details.EmptyDataText = "No Record(s) to Display";
                        grd_details.ShowFooter = true;
                        grd_details.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);                
            }
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                {
                    int customerID = Convert.ToInt32(ddlCustomer.SelectedValue);

                    // GET THE ACTIVE GRIDVIEW ROW.
                    Button bt = (Button)sender;
                    GridViewRow grdRow = (GridViewRow)bt.Parent.Parent;

                    // NOW GET VALUES FROM FIELDS FROM THE ACTIVE ROW.
                    TextBox txtfromdate = (TextBox)grdRow.Cells[0].FindControl("txt_fdate");
                    TextBox txttodate = (TextBox)grdRow.Cells[0].FindControl("txt_tdate");
                    TextBox txtsharepercentage = (TextBox)grdRow.Cells[0].FindControl("txt_SharingPercentage");
                    TextBox txtdiscountpercentage = (TextBox)grdRow.Cells[0].FindControl("txt_DiscountPercentage");

                    if (txtfromdate.Text != "" && txttodate.Text != "" && txtsharepercentage.Text != "")
                    {
                        using (ComplianceDBEntities entities = new ComplianceDBEntities())
                        {
                            RLCS_SharingPercentage sharing = new RLCS_SharingPercentage();

                            //price.CustName = tbBookName.Text;
                            sharing.FromDate = Convert.ToDateTime(txtfromdate.Text);
                            sharing.ToDate = Convert.ToDateTime(txttodate.Text);
                            sharing.DiscountPercentage = Convert.ToDecimal(txtdiscountpercentage.Text);
                            sharing.SharingPercentage = Convert.ToDecimal(txtsharepercentage.Text);
                            sharing.CustomerID = customerID;
                            sharing.IsActive = true;
                            sharing.CreatedOn = DateTime.Now;
                            sharing.CreatedBy = AuthenticationHelper.UserID;

                            // sharing.CreatedBy = Convert.ToInt32(ddlCustomer.SelectedValue);
                            entities.RLCS_SharingPercentage.Add(sharing);
                            entities.SaveChanges();
                        }

                        // REFRESH THE GRIDVIEW CONTROL TO SHOW THE NEWLY INSERTED ROW.
                        BindGrid_With_Data(customerID);
                    }
                    else
                    {
                        cvCustomError.IsValid = false;
                        cvCustomError.ErrorMessage = "Required Date(From), Date(To), Sharing(%)";
                    }
                }
            }
            catch(Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);

                cvCustomError.IsValid = false;
                cvCustomError.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void grd_details_RowEditing(object sender, GridViewEditEventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
            {
                int customerID = Convert.ToInt32(ddlCustomer.SelectedValue);

                grd_details.EditIndex = e.NewEditIndex;
                BindGrid_With_Data(customerID);
            }
        }

        protected void grd_details_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
            {
                int customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                grd_details.EditIndex = -1;

                BindGrid_With_Data(customerID);
            }
        }

        protected void grd_details_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
            {
                int customerID = Convert.ToInt32(ddlCustomer.SelectedValue);

                GridViewRow row = grd_details.Rows[e.RowIndex];
                TextBox fromdate = row.FindControl("txtfdate") as TextBox;
                TextBox todate = row.FindControl("txttdate") as TextBox;
                TextBox shareingpercentage = row.FindControl("txtSharingPercentage") as TextBox;
                TextBox discpercentage = row.FindControl("txtDiscountPercentage") as TextBox;

                if (fromdate.Text != "" && shareingpercentage.Text != "" && discpercentage.Text != "")
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int recordID = Convert.ToInt32(grd_details.DataKeys[e.RowIndex].Value);

                        RLCS_SharingPercentage prevRecord = entities.RLCS_SharingPercentage.First(x => x.ID == recordID);

                        if (prevRecord != null)
                        {
                            prevRecord.FromDate = Convert.ToDateTime(fromdate.Text);

                            if (!string.IsNullOrEmpty(todate.Text))
                                prevRecord.ToDate = Convert.ToDateTime(todate.Text);
                            else
                                prevRecord.ToDate = null;

                            prevRecord.SharingPercentage = Convert.ToDecimal(shareingpercentage.Text);
                            prevRecord.DiscountPercentage = Convert.ToDecimal(discpercentage.Text);

                            prevRecord.UpdatedOn = DateTime.Now;
                            prevRecord.UpdatedBy = customerID;
                            entities.SaveChanges();
                        }

                        grd_details.EditIndex = -1;

                        BindGrid_With_Data(customerID);
                    }
                }
                else
                {
                    cvCustomError.IsValid = false;
                    cvCustomError.ErrorMessage = "Required Date(From), Date(To), Sharing(%)";
                }
            }
        }

        private void BindCustomers()
        {
            try
            {
                int customerID = -1;
                int serviceProviderID = 95;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                //ddlCustomerList.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(customerID, serviceProviderID, 2);
                ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, true);
                ddlCustomer.DataBind();

                ddlCustomer.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grd_details_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
            {
                int customerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                int recordID = Convert.ToInt32(grd_details.DataKeys[e.RowIndex].Value);

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var obj = entities.RLCS_SharingPercentage.FirstOrDefault(x => x.ID == recordID);

                    if (obj != null)
                    {
                        obj.IsActive = false;
                        entities.SaveChanges();

                        BindGrid_With_Data(customerID);
                    }
                }
            }
        }
    }
}