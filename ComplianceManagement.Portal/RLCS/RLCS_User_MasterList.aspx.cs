﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.DataRisk;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_User_MasterList : System.Web.UI.Page
    {
        protected static int CustId;
        protected int UserId;
        protected static string Path;
        protected void Page_Load(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);

            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
        }
    }
}