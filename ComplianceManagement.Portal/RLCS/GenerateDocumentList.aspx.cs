﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class GenerateDocumentList : System.Web.UI.Page
    {
        protected static int CustomerId;
        protected static int UserId;
        protected static string avacomRLCSAPI_URL;
        public int LoginUserID;
        public string BranchID_;
        public static List<int> locationList = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {
            avacomRLCSAPI_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            CustomerId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            hdnOutCustID.Value = Convert.ToString(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            hdnLoginUserID.Value = Convert.ToString(AuthenticationHelper.UserID);
            
            if (!IsPostBack)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {

                    LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                    if (userAssignedBranchList != null)
                    {
                        if (userAssignedBranchList.Count > 0)
                        {
                            List<int> assignedbranchIDs = new List<int>();
                            assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                            BindLocationFilterOutput(assignedbranchIDs);

                        }
                    }
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "GetDocumentGenerateListData('" + CustomerId + "','','','0','','0','" + LoginUserID + "');", true);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideDivTree", "hideDivBranch();", true);
        }
        protected void upDocGenPopup_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocationOutput');", tbxOutputBranch.ClientID), true);
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
              
            }
        }
        protected void ddlOutputComplianceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                List<int> userAssignedBranchListIds = new List<int>();
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                    foreach (var item in userAssignedBranchList)
                    {
                        userAssignedBranchListIds.Add(item.ID);
                    }
                }

                #region MyRegion InputOutput Page File
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                    // txtActNameList.Text = "";
                    if (ddlOutputComplianceType.SelectedIndex != 0)
                    {
                        if (ddlOutputComplianceType.SelectedValue == "Register")
                        {
                            var listActNameOutput = (from A in entities.Acts
                                                     join C in entities.Compliances on A.ID equals C.ActID
                                                     join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
                                                     join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
                                                     join RR in entities.RLCS_Register_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
                                                     join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
                                                     where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)UserId
                                                     select new
                                                     {
                                                         Id = A.ID,
                                                         Name = A.Name
                                                     }).Distinct().ToList();
                            if (listActNameOutput.Count > 0)
                            {

                                rptAct.DataSource = listActNameOutput;
                                rptAct.DataBind();
                            }
                            else
                            {
                                rptAct.DataSource = null;
                                rptAct.DataBind();


                            }
                        }
                        if (ddlOutputComplianceType.SelectedValue == "Challan")
                        {
                            var listActNameOutput = (from A in entities.Acts
                                                     join C in entities.Compliances on A.ID equals C.ActID
                                                     join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
                                                     join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
                                                     join RR in entities.RLCS_Challan_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
                                                     join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
                                                     where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)UserId
                                                     select new 
                                                     {
                                                         Id = A.ID,
                                                         Name = A.Name
                                                     }).Distinct().ToList();
                            if (listActNameOutput.Count > 0)
                            {

                                rptAct.DataSource = listActNameOutput;
                                rptAct.DataBind();
                            }
                            else
                            {
                                rptAct.DataSource = null;
                                rptAct.DataBind();


                            }
                        }
                        if (ddlOutputComplianceType.SelectedValue == "Return")
                        {
                            var listActNameOutput = (from A in entities.Acts
                                                     join C in entities.Compliances on A.ID equals C.ActID
                                                     join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
                                                     join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
                                                     join RR in entities.RLCS_Returns_Compliance_Mapping on CI.ComplianceId equals RR.AVACOM_ComplianceID
                                                     join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
                                                     where userAssignedBranchListIds.Contains((int)CB.ID) && CA.UserID == (long)UserId
                                                     select new
                                                     {
                                                         Id = A.ID,
                                                         Name = A.Name
                                                     }).Distinct().ToList();
                            if (listActNameOutput.Count > 0)
                            {

                                rptAct.DataSource = listActNameOutput;
                                rptAct.DataBind();
                            }
                            else
                            {
                                rptAct.DataSource = null;
                                rptAct.DataBind();


                            }
                        }
                    }
                    else
                    {
                        rptAct.DataSource = null;
                        rptAct.DataBind();


                    }
                }
                ddlComplianceName.DataSource = null;
                ddlComplianceName.DataBind();
               
            }
            catch (Exception ex)
            {
            }
            #endregion
            
        }

        protected void ddlComplianceName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlComplianceName.SelectedIndex != 0  && ddlComplianceName.SelectedItem.Text!= "Compliance")
            {
                int ComplianceID = Convert.ToInt32(ddlComplianceName.SelectedValue);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Frequency_ID = (from C in entities.Compliances
                                        where C.ID == ComplianceID
                                        select C.Frequency).Distinct().FirstOrDefault();
                    if (Frequency_ID != null)
                    {
                        List<Tuple<int, string>> FreqTList = new List<Tuple<int, string>>();
                        FreqTList.Add(new Tuple<int, string>(0, "Monthly"));
                        FreqTList.Add(new Tuple<int, string>(1, "Quarterly"));
                        FreqTList.Add(new Tuple<int, string>(2, "Half Yearly"));
                        FreqTList.Add(new Tuple<int, string>(3, "Annual"));
                        FreqTList.Add(new Tuple<int, string>(4, "Four Monthly"));
                        FreqTList.Add(new Tuple<int, string>(5, "BiAnnual"));
                        var lst = FreqTList.FindAll(m => m.Item1 == Frequency_ID);
                        if (lst.Count > 0)
                        {
                            BindPeriod_Return(Convert.ToString(lst[0].Item2), "O");
                        }

                    }
                }
            }
            
        }

        private void BindPeriod_Return(string selectedFreq, string Type)
        {
            try
            {
                if (!string.IsNullOrEmpty(selectedFreq))
                {
                    List<Tuple<string, string, string>> tupleList = new List<Tuple<string, string, string>>();

                    tupleList.Add(new Tuple<string, string, string>("Annual", "Annual", "12"));
                    tupleList.Add(new Tuple<string, string, string>("BiAnnual", "BiAnnual", "BiAnnual"));

                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "First Half", "06"));
                    tupleList.Add(new Tuple<string, string, string>("Half Yearly", "Second Half", "12"));

                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q1", "03"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q2", "06"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q3", "09"));
                    tupleList.Add(new Tuple<string, string, string>("Quarterly", "Q4", "12"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "January", "01"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "February", "02"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "March", "03"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "April", "04"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "May", "05"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "June", "06"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "July", "07"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "August", "08"));

                    tupleList.Add(new Tuple<string, string, string>("Monthly", "September", "09"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "Octomber", "10"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "November", "11"));
                    tupleList.Add(new Tuple<string, string, string>("Monthly", "December", "12"));

                    DataTable dtReturnPeriods = new DataTable();
                    dtReturnPeriods.Columns.Add("ID");
                    dtReturnPeriods.Columns.Add("Name");

                    var lst = tupleList.FindAll(m => m.Item1 == selectedFreq);
                    if (lst.Count > 0)
                    {
                        foreach (var item in lst)
                        {
                            DataRow drReturnPeriods = dtReturnPeriods.NewRow();
                            drReturnPeriods["ID"] = item.Item3;
                            drReturnPeriods["Name"] = item.Item2;
                            dtReturnPeriods.Rows.Add(drReturnPeriods);
                        }
                        if (Type == "I")
                        {
                            //rptPeriodList.DataSource = dtReturnPeriods;
                            //  rptPeriodList.DataBind();

                        }
                        else if (Type == "O")
                        {
                            rptPeriod1.DataSource = dtReturnPeriods;
                            rptPeriod1.DataBind();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.tvOutputBranch.Nodes.Count; i++)
            {
                ChkBoxClear(this.tvOutputBranch.Nodes[i]);
            }
        }

        protected void ChkBoxClear(TreeNode node)
        {

            if (node.Checked)
            {
                node.Checked = false;
            }
            foreach (TreeNode tn in node.ChildNodes)
            {

                if (tn.Checked)
                {
                    tn.Checked = false;
                }

                if (tn.ChildNodes.Count != 0)
                {
                    for (int i = 0; i < tn.ChildNodes.Count; i++)
                    {
                        ChkBoxClear(tn.ChildNodes[i]);
                    }
                }
            }
        }

        protected void btnComplianceGet_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtCompliance = new DataTable();
                dtCompliance.Columns.Add("ID");
                dtCompliance.Columns.Add("Name");
                List<int> userAssignedBranchListIds = new List<int>();
                List<int> lstActIds = new List<int>();
                
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                    foreach (var item in userAssignedBranchList)
                    {
                        userAssignedBranchListIds.Add(item.ID);
                    }
                }
                foreach (RepeaterItem Item in rptAct.Items)
                {
                    CheckBox chkAct = (CheckBox)Item.FindControl("chkAct");

                    if (chkAct != null && chkAct.Checked)
                    {
                        Label lblActID = (Label)Item.FindControl("lblActID");
                        Label lblActName = (Label)Item.FindControl("lblActName");

                        lstActIds.Add(Convert.ToInt32(lblActID.Text));

                        // txtActNameList.Text += lblActName.Text + ",";
                    }

                }
                if (lstActIds.Count > 0)
                {
                    UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var listComplianceName = (from A in entities.Acts
                                                  join C in entities.Compliances on A.ID equals C.ActID
                                                  join CM in entities.ComplianceInputMappings on C.ID equals (long)CM.ComplianceID
                                                  join CI in entities.ComplianceInstances on C.ID equals CI.ComplianceId
                                                  join CB in entities.CustomerBranches on CI.CustomerBranchID equals CB.ID
                                                  join CA in entities.ComplianceAssignments on CI.ID equals CA.ComplianceInstanceID
                                                  where userAssignedBranchListIds.Contains((int)CB.ID) && lstActIds.Contains((int)A.ID) && CA.UserID == (long)UserId && C.IsDeleted == false && CM.DocumentType == ddlOutputComplianceType.SelectedValue
                                                  select new
                                                  {
                                                      Id = C.ID,
                                                      Name = C.ShortForm
                                                  }).Distinct().ToList();
                        if (listComplianceName.Count > 0)
                        {
                            foreach (var item in listComplianceName)
                            {
                                DataRow drCompliance = dtCompliance.NewRow();
                                drCompliance["ID"] = item.Id;
                                drCompliance["Name"] = item.Name;
                                dtCompliance.Rows.Add(drCompliance);
                            }
                            ddlComplianceName.DataValueField = "Id";
                            ddlComplianceName.DataTextField = "Name";

                            ddlComplianceName.DataSource = dtCompliance;
                            ddlComplianceName.DataBind();
                            ddlComplianceName.Items.Insert(0, "Compliance");
                        }
                        else
                        {
                            ddlComplianceName.DataSource = null;
                            ddlComplianceName.DataBind();
                            ddlComplianceName.Items.Insert(0, "Compliance");
                        }
                    }
                }
                else
                {
                    ddlComplianceName.DataSource = null;
                    ddlComplianceName.DataBind();

                }
            }
            catch (Exception ex)
            {
            }
            
        }

        private void BindLocationFilterOutput(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    var branches = RLCS_ClientsManagement.GetAllHierarchy(customerID);
                    LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
                    tvOutputBranch.Nodes.Clear();
                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvOutputBranch.Nodes.Add(node);
                    foreach (var item in branches)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvOutputBranch.Nodes.Add(node);
                    }
                    tvOutputBranch.CollapseAll();
                    // tvOutputBranch_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnOutSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                hdnSubmitbtn.Value = "";
                string ActGroup = "";
                string InputId = "";
                string ReturnRegisterChallanID = "";
                string DocumentType = "";
                string Period = "";
                string MonthId = "";
                string Year = "";
                string ComplianceType = "";
                string ActID = "";
                int CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                hdnOutCustID.Value = Convert.ToString(AuthenticationHelper.CustomerID);
                LoginUserID = Convert.ToInt32(AuthenticationHelper.UserID);
                hdnLoginUserID.Value = Convert.ToString(LoginUserID);

                string BranchID = "";
                hdnOutBranchId.Value = "";
                hdnOutMonth.Value = "";
                hdnOutComplianceID.Value = "0";
                hdnOutYear.Value = "0";
                hdnOutComplianceType.Value = "";
                locationList.Clear();
                for (int i = 0; i < this.tvOutputBranch.Nodes.Count; i++)
                {
                    RetrieveNodes(this.tvOutputBranch.Nodes[i]);
                }
                if (locationList.Count > 0)
                {
                    foreach (var item in locationList)
                    {
                        BranchID += Convert.ToString(item) + ",";
                    }
                }
                if (!string.IsNullOrEmpty(BranchID))
                {
                    BranchID = BranchID.Remove(BranchID.Length - 1);
                    hdnOutBranchId.Value = Convert.ToString(BranchID);
                }

                foreach (RepeaterItem Item in rptPeriod1.Items)
                {
                    CheckBox chkPeriod1 = (CheckBox)Item.FindControl("chkPeriod1");

                    if (chkPeriod1 != null && chkPeriod1.Checked)
                    {
                        Label lblPeriodID1 = (Label)Item.FindControl("lblPeriodID1");
                        MonthId += (lblPeriodID1.Text) + ",";
                    }
                }
                if (MonthId.Length > 0)
                {
                    MonthId = MonthId.Remove(MonthId.Length - 1);
                    hdnOutMonth.Value = MonthId;
                }
                string ComplianceID = "0";
                if (ddlComplianceName.SelectedValue != "Compliance")
                {
                    ComplianceID = ddlComplianceName.SelectedValue;
                    hdnOutComplianceID.Value = ComplianceID;

                }

                if (ddlOutYear.SelectedValue != "0")
                {
                    Year = ddlOutYear.SelectedValue;
                    hdnOutYear.Value = Year;

                }
                if (ddlOutputComplianceType.SelectedValue=="Compliance Type")
                {
                    ComplianceType ="";
                    hdnOutComplianceType.Value = "";
                }
                else
                {
                    ComplianceType = ddlOutputComplianceType.SelectedValue;
                    hdnOutComplianceType.Value = ComplianceType;
                }
             
                hdnSubmitbtn.Value = "Apply";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "GetDocumentGenerateListData('" + CustomerID + "','" + BranchID + "','" + ComplianceType + "','" + ComplianceID + "','" + MonthId + "','" + Year + "','" + LoginUserID + "');", true);

            }
            catch (Exception ex)
            {
            }
        }

        protected void RetrieveNodes(TreeNode node)
        {
            try
            {
                if (node.Checked)
                {
                    if (!locationList.Contains(Convert.ToInt32(node.Value)))
                    {
                        locationList.Add(Convert.ToInt32(node.Value));
                    }
                    if (node.ChildNodes.Count != 0)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            RetrieveNodes(node.ChildNodes[i]);
                        }
                    }
                }
                else
                {
                    foreach (TreeNode tn in node.ChildNodes)
                    {
                        if (tn.Checked)
                        {
                            if (!locationList.Contains(Convert.ToInt32(tn.Value)))
                            {
                                locationList.Add(Convert.ToInt32(tn.Value));
                            }
                        }
                        if (tn.ChildNodes.Count != 0)
                        {
                            for (int i = 0; i < tn.ChildNodes.Count; i++)
                            {
                                RetrieveNodes(tn.ChildNodes[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

    }
}