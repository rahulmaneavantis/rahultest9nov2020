﻿<%@ Page Title="Masters :: State-City Master" Language="C#" AutoEventWireup="true" MasterPageFile="~/HRPlusCompliance.Master" CodeBehind="RLCS_Location_Master.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_Location_Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script src="../js/google1113_jquery.min.js"></script>

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>
    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />
    <title></title>

    <style>
        .k-grid-content {
            min-height: auto;
        }

        .k-grid td {
            line-height: 2.0em;
        }
    </style>

    <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

        .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

        .panel-heading .nav > li {
            margin-left: 5px !important;
            margin-right: 5px !important;
        }

        .panel-heading .nav > li:hover {
            color: white;
            background-color: #1fd9e1;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script>
        $(document).ready(function () {
            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "<% =Path%>GetAll_StateMaster",
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                    }
                },
                schema: {
                    data: function (response) {
                        return response.Result;
                    }
                }
            });

            $("#ddlStates").kendoDropDownList({
                optionLabel: "State",
                dataTextField: "SM_Name",
                dataValueField: "AVACOM_StateID",
                dataSource: dataSource,
                change: ApplyFilter
            });
        });
       </script>

    <script type="text/javascript">
        function BindFirstGrid() {            
            $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    serverFiltering: false,
                    pageSize: 10,
                    transport: {
                        read: {
                            url: '<% =Path%>GetAll_StateLocationMaster?CustID=<% =CustId%>',
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 50,
                    schema: {
                        data: function (response) {                         
                            return response.Result;
                        },
                        total: function (response) {                           
                            return response.Result.length;
                        }
                    }
                },
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    // pageSizes: true,
                    //pageSizes: [ 10 , 25, 50 ],
                    change: function (e) {
                        //$('#chkAll').removeAttr('checked');
                        //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "AVACOM_CityID", title: "AVACOM_CityID" },
                    { hidden: true, field: "AVACOM_StateID", title: "AVACOM_StateID" },
                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: "10%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "SM_Code", title: 'State Code',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "StateName", title: 'State',
                        width: "30%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "LM_Code", title: 'Location Code',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'

                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "LM_Name", title: 'Location',
                        width: "30%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    }
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });
        }

        $(document).ready(function () {
            fhead('Masters/ State-City');
            $('#btnClearFilter').css('display', 'none');
            BindFirstGrid();
        });

        function ApplyFilter() {           

            var finalSelectedfilter = { logic: "and", filters: [] };
            var txtSearch = $("#txtSearch").val();
            if (txtSearch) {
                var textFilter = { logic: "or", filters: [] };

                textFilter.filters.push({
                    field: "LM_Name", operator: "contains", value: txtSearch
                });

                textFilter.filters.push({
                    field: "StateName", operator: "contains", value: txtSearch
                });

                textFilter.filters.push({
                    field: "SM_Code", operator: "contains", value: txtSearch
                });

                textFilter.filters.push({
                    field: "LM_Code", operator: "contains", value: txtSearch
                });

                finalSelectedfilter.filters.push(textFilter);
            }

            var selectedStateIDs = $("#ddlStates").val();

            if (selectedStateIDs != '' && selectedStateIDs != null && selectedStateIDs != undefined && selectedStateIDs != "-1") {
                var stateFilter = { logic: "or", filters: [] };

                stateFilter.filters.push({
                    field: "AVACOM_StateID", operator: "eq", value: selectedStateIDs
                });

                finalSelectedfilter.filters.push(stateFilter);
            }            

            if (finalSelectedfilter.filters.length > 0) {
                if ($("#grid").data("kendoGrid") != null && $("#grid").data("kendoGrid") != undefined) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                    $('#btnClearFilter').css('display', 'block');
                }
            } else {
                if ($("#grid").data("kendoGrid") != null && $("#grid").data("kendoGrid") != undefined) {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    $('#btnClearFilter').css('display', 'none');
                }
            }

            return false;
        }

        function ClearFilter(e) {
            debugger;
            e.preventDefault();
            $("#txtSearch").val("");
            $("#ddlStates").data("kendoDropDownList").value("");
            $('#btnClearFilter').css('display', 'none');
            ApplyFilter();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">                   
                    <li class="active">
                        <asp:LinkButton ID="lnkTabStateCity" runat="server" PostBackUrl="~/RLCS/RLCS_Location_Master.aspx">State-City</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabDesignation" runat="server" PostBackUrl="~/RLCS/RLCS_Designation_Master.aspx">Designation</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabLeaveType" runat="server" PostBackUrl="~/RLCS/RLCS_LeaveType_Master.aspx">Leave Type</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>

        <div class="col-md-12 colpadding0 toolbar" style="margin-top: 10px;">
            <div class="col-md-3 colpadding0">
                <input id="ddlStates" style="width: 98%" data-placeholder="State" />
            </div>
            <div class="col-md-6">
                <div class="col-md-8">
                    <input class="k-textbox" type="text" id="txtSearch" placeholder="Type Text to Search..." style="width: 100%;" />
                </div>
                <div class="col-md-2">
                    <button id="btnSearch" class="btn btn-primary" onclick="ApplyFilter(); return false;">Search</button>
                </div>
                <div class="col-md-2">
                    <button id="btnClearFilter" class="btn btn-primary" onclick="ClearFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid" style="border: none;">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

