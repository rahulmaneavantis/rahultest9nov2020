﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Configuration;
using System.Threading;
using System.Reflection;
using com.VirtuosoITech.ComplianceManagement.Portal.Users;
using System.IO;
using System.Globalization;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.AppCode.Controllers;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_UserDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            vsUserDetails.CssClass = "alert alert-danger";

            if (!IsPostBack)
            {
                int userID = 0;
                string strCustID = "";

                rblAuditRole.Enabled = true;
                BindCustomers();

                if (!string.IsNullOrEmpty(Request.QueryString["UserID"]))
                {
                    userID = Convert.ToInt32(Request.QueryString["UserID"]);

                    if (!string.IsNullOrEmpty(Request.QueryString["CustomerID"]))
                    {
                        strCustID = Request.QueryString["CustomerID"];

                        if (ddlCustomer.Items.FindByValue(strCustID) != null)
                        {
                            ddlCustomer.ClearSelection();
                            ddlCustomer.Items.FindByValue(strCustID).Selected = true;
                        }
                        ddlCustomer.Enabled = false;
                        BindRoles(Convert.ToInt32(strCustID));
                        BindDepartment();
                        ComplianceBindRoles();
                        BindRolesSEC();

                        if (userID > 0)
                        {
                            ViewState["Mode"] = 1;
                            ViewState["UserID"] = userID;

                            EditUserInformation(userID);
                        }
                        else
                        {
                            ViewState["Mode"] = 0;
                            AddNewUser();
                        }
                    }
                   int customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    EnableDisableRole(customerID);
                }
            }
        }

        protected void Upload(object sender, EventArgs e)
        {
            if (UserImageUpload.HasFile)
            {
                string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg" };

                string ext = System.IO.Path.GetExtension(UserImageUpload.PostedFile.FileName);
                bool isValidFile = false;
                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidFile = true;
                        break;
                    }
                }
                if (!isValidFile)
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes);
                }
            }
        }

        protected void upUsers_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "InitializeJQueryUI", "initializeJQueryUI();", true);
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        private void BindDepartment()
        {
            try
            {
                if (ddlCustomer.SelectedValue != "" && ddlCustomer.SelectedValue != null)
                {
                    int customerID = -1;
                    //customerID = Convert.ToInt32(Portal.Common.AuthenticationHelper.CustomerID);

                    ddlDepartment.DataTextField = "Name";
                    ddlDepartment.DataValueField = "ID";
                    ddlDepartment.DataSource = CompDeptManagement.FillDepartment(customerID);
                    ddlDepartment.DataBind();
                    ddlDepartment.Items.Insert(0, new ListItem("< Select Department >", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindRoles(int customerID)
        {
            try
            {
                ddlRole.ClearSelection();
                ddlRole.Items.Clear();

                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "ID";

                List<Role> roles = new List<Role>();

                //var roles = RoleManagement.GetAll(false);
                //roles = RoleManagement.GetAll_Compliance(false);
                if (customerID != 0)
                    roles = RoleManagement.GetAll_HRCompliance_Roles(customerID);
                else
                    roles = RoleManagement.GetAll_HRCompliance_Roles();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN") && !entry.Code.Equals("IMPT")).ToList();
                }

                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("IMPT")).ToList();
                }

                ddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlRole.DataBind();
                ddlRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
              
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        private void ComplianceBindRoles()
        {
            try
            {
                CddlRole.DataTextField = "Name";
                CddlRole.DataValueField = "ID";

                var roles = GetAll(false);
                CddlRole.DataSource = roles.OrderBy(entry => entry.Name);
                CddlRole.DataBind();
                CddlRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Role> GetAll(bool? onlyForCompliance = null)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID == 2 || row.ID == 7 || row.ID == 8
                             select row);

                if (onlyForCompliance.HasValue)
                {
                    roles = roles.Where(entry => entry.IsForCompliance == onlyForCompliance.Value && entry.Code != "APPR");
                }
                return roles.ToList();
            }
        }
        private void BindRolesSEC()
        {
            try
            {
                ddlSECRole.DataTextField = "Name";
                ddlSECRole.DataValueField = "ID";

                var roles = GetSECLimitedRole();
                if (AuthenticationHelper.Role == "CADMN" || AuthenticationHelper.Role == "MGMT")
                {
                    roles = roles.Where(entry => !entry.Code.Equals("SADMN")).ToList();
                }

                if (roles.Count > 0)
                {
                    List<string> secRoleCodes = new List<string> { "CEXCT", "DADMN", "CSMGR" };

                    roles = roles.Where(row => secRoleCodes.Contains(row.Code)).ToList();
                }

                ddlSECRole.DataSource = roles.OrderBy(entry => entry.Name);
                ddlSECRole.DataBind();

                ddlSECRole.Items.Insert(0, new ListItem("< Select Role >", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<Role> GetSECLimitedRole()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.IsForSecretarial == true
                             select row);

                return roles.ToList();
            }
        }

        public static List<Role> GetHRLimitedRole()
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var roles = (from row in entities.Roles
                             where row.ID == 14 || row.ID == 15 || row.ID == 16 || row.ID == 17 || row.ID == 18
                             select row);

                return roles.ToList();
            }
        }
        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string roleCode = string.Empty;
                var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                if (role != null)
                {
                    roleCode = role.Code;
                }
                //divReportingTo.Visible = divCustomer.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                //divReportingTo.Visible = !(roleCode.Equals("SADMN") || string.IsNullOrEmpty(roleCode));
                divReportingTo.Visible = false;

                if (divReportingTo.Visible)
                    BindReportingTo();

                divCustomerBranch.Visible = roleCode.Equals("EXCT");
                divCustomerBranch.Visible = roleCode.Equals("VAUDT");

                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewRoleChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }

                reqAudit1.Visible = false;
                reqAudit2.Visible = false;
                reqAudit3.Visible = false;
                reqAudit4.Visible = false;

                if (roleCode.Equals("AUDT"))
                {
                    Auditor1.Visible = true;
                    Auditor2.Visible = true;

                    reqAudit1.Visible = true;
                    reqAudit2.Visible = true;
                    reqAudit3.Visible = true;
                    reqAudit4.Visible = true;
                }
                else
                {
                    Auditor1.Visible = false;
                    Auditor2.Visible = false;
                    reqAudit1.Visible = true;
                    reqAudit2.Visible = true;
                    reqAudit3.Visible = true;
                    reqAudit4.Visible = true;
                }
                //if (divReportingTo.Visible)
                //{
                //    BindReportingTo();
                //}

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomers()
        {
            try
            {
                ddlCustomer.DataTextField = "Name";
                ddlCustomer.DataValueField = "ID";

                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                //ddlCustomer.DataSource = CustomerManagement.GetAll(customerID);
                //ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomers_IncludesServiceProvider(customerID, serviceProviderID, 2);

                ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2, true);
                ddlCustomer.DataBind();

                //if (AuthenticationHelper.Role != "CADMN")
                ddlCustomer.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = string.Empty;
                BindCustomerBranches();
                //BindReportingTo();

                //if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && ddlCustomer.SelectedValue != "-1")
                //    BindRoles(Convert.ToInt32(ddlCustomer.SelectedValue));
                //else
                //    BindRoles(0);

                if (divCustomerBranch.Visible)
                {
                    ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewCustomerChange", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindReportingTo()
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue) && !string.IsNullOrEmpty(ddlRole.SelectedValue))
                {
                    ddlReportingTo.DataTextField = "Name";
                    ddlReportingTo.DataValueField = "ID";

                    string roleCode = string.Empty;
                    var role = RoleManagement.GetByID(Convert.ToInt32(ddlRole.SelectedValue));
                    if (role != null)
                    {
                        roleCode = role.Code;
                    }

                    ddlReportingTo.DataSource = UserManagement.GetAllByCustomerID(Convert.ToInt32(ddlCustomer.SelectedValue), roleCode);
                    ddlReportingTo.DataBind();

                    ddlReportingTo.Items.Insert(0, new ListItem("Select", "-1"));
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void tvBranches_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxBranch.Text = tvBranches.SelectedNode != null ? tvBranches.SelectedNode.Text : "< Select Branch >";
                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeView", "$(\"#divBranches\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindCustomerBranches()
        {
            try
            {
                tvBranches.Nodes.Clear();
                NameValueHierarchy branch = null;
                var branchs = CustomerBranchManagement.GetAllHierarchy(Convert.ToInt32(ddlCustomer.SelectedValue));
                if (branchs.Count > 0)
                {
                    branch = branchs[0];
                }
                tbxBranch.Text = "Select";
                List<TreeNode> nodes = new List<TreeNode>();
                BindBranchesHierarchy(null, branch, nodes);
                foreach (TreeNode item in nodes)
                {
                    tvBranches.Nodes.Add(item);
                }

                tvBranches.CollapseAll();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        {
            try
            {
                if (nvp != null)
                {
                    foreach (var item in nvp.Children)
                    {
                        TreeNode node = new TreeNode(item.Name, item.ID.ToString());
                        BindBranchesHierarchy(node, item, nodes);
                        if (parent == null)
                        {
                            nodes.Add(node);
                        }
                        else
                        {
                            parent.ChildNodes.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        private void BindParameters(List<UserParameterValueInfo> userParameterValues = null)
        {
            try
            {
                if (userParameterValues == null)
                {
                    userParameterValues = new List<UserParameterValueInfo>();
                    var userParameters = UserManagement.GetAllUserParameters();
                    userParameters.ForEach(entry =>
                    {
                        userParameterValues.Add(new UserParameterValueInfo()
                        {
                            UserID = -1,
                            ParameterID = entry.ID,
                            ValueID = -1,
                            Name = entry.Name,
                            DataType = (DataType)entry.DataType,
                            Length = (int)entry.Length,
                            Value = string.Empty
                        });
                    });
                }

                repParameters.DataSource = userParameterValues;
                repParameters.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        public static List<long> GetByProductID(int customerID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                //List<long> plist = new List<long>();
                //var ServiceproviderId = (from cust in entities.Customers
                //                         where cust.ID == customerID
                //                         select cust.ServiceProviderID).FirstOrDefault();

                //if (ServiceproviderId != null)
                //{
                //    plist = (from row in entities.ProductMappings
                //             where row.CustomerID == ServiceproviderId && row.IsActive == false
                //             select (long)row.ProductID).ToList();
                //}

                //return plist;
                var productmapping = (from row in entities.ProductMappings
                                      where row.CustomerID == customerID && row.IsActive == false
                                      select (long)row.ProductID).ToList();

                return productmapping;
            }
        }
        public void EnableDisableRole(int CustomerID)
        {

            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                bool ab = false;
                int customerID = -1;
                if (AuthenticationHelper.Role == "SADMN" || AuthenticationHelper.Role == "IMPT" || AuthenticationHelper.Role == "MGMT")
                {
                    customerID = CustomerID;
                }
                else
                {
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                }

                var Listofproduct = GetByProductID(customerID);
                if (Listofproduct.Count > 0)
                {
                    divComplianceRole.Visible = false;
                    CddlRole.Enabled = false;
                    divAuditRole.Visible = false;
                    ddlAuditRole.Enabled = false;
                    divSECRole.Visible = false;
                    ddlSECRole.Enabled = false;
                    divHRRole.Visible = false;
                    ddlRole.Enabled = false;

                    if (Listofproduct.Contains(1))
                    {
                        divComplianceRole.Visible = true;
                        CddlRole.Enabled = true;
                    }
                    if (Listofproduct.Contains(4))
                    {
                        divAuditRole.Visible = true;
                        ddlAuditRole.Enabled = true;
                    }
                    if (Listofproduct.Contains(8))
                    {
                        divSECRole.Visible = true;
                        ddlSECRole.Enabled = true;
                    }
                    if (Listofproduct.Contains(9))
                    {
                        divHRRole.Visible = true;
                        ddlRole.Enabled = true;
                    }
                }

            }
        }


        public void AddNewUser(int CustomerID)
        {
            try
            {
                ViewState["Mode"] = 0;
                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                //tbxUsername.Enabled = true;
                tbxEmail.Enabled = true;
                //ddlCustomer.Enabled = true;
                ddlCustomer.Enabled = false;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);
                txtEndDate.Text = "";
                txtStartDate.Text = "";
                txtperiodStartDate.Text = "";
                txtperiodEndDate.Text = "";
                BindParameters();
                EnableDisableRole(CustomerID);
                upUsers.Update();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void AddNewUser()
        {
            try
            {
                ViewState["Mode"] = 0;

                tbxFirstName.Text = tbxLastName.Text = tbxDesignation.Text = tbxEmail.Text = tbxContactNo.Text = tbxAddress.Text = string.Empty;
                //tbxUsername.Enabled = true;
                tbxEmail.Enabled = true;
                //ddlCustomer.Enabled = true;
                ddlCustomer.Enabled = false;
                rblAuditRole.Enabled = true;
                rblAuditRole.ClearSelection();
                ddlDepartment.ClearSelection();
                ddlRole.ClearSelection();
                ddlRole.SelectedValue = "-1";
                ddlRole_SelectedIndexChanged(null, null);

                BindParameters();

                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    bool ab = false;
                    customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                  
                    //bool a = UserManagementRisk.CheckProductmapping(customerID);
                    //if (a == true)
                    //{
                    //    divRiskRole.Visible = true;
                    //}
                    //else
                    //{
                    //    divRiskRole.Visible = false;
                    //}
                }

                upUsers.Update();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        public void EditUserInformation(int userID)
        {
            try
            {
                ViewState["Mode"] = 1;
                ViewState["UserID"] = userID;

                User user = UserManagement.GetByID(userID);

                List<UserParameterValueInfo> userParameterValues = UserManagement.GetParameterValuesByUserID(userID);

                if (user.RoleID == 1 || user.RoleID == 2 || user.RoleID == 8 || user.RoleID == 12)
                {
                    rblAuditRole.Enabled = false;
                }

                tbxFirstName.Text = user.FirstName;
                tbxLastName.Text = user.LastName;
                tbxDesignation.Text = user.Designation;
                tbxEmail.Text = user.Email;
                tbxContactNo.Text = user.ContactNumber;
                tbxAddress.Text = user.Address;

                if (user.RoleID == 9)
                {
                    txtStartDate.Text = Convert.ToString(Convert.ToDateTime(user.Startdate).ToString("dd-MM-yyyy"));
                    txtEndDate.Text = Convert.ToString(Convert.ToDateTime(user.Enddate).ToString("dd-MM-yyyy"));
                    txtperiodStartDate.Text = Convert.ToString(Convert.ToDateTime(user.AuditStartPeriod).ToString("dd-MM-yyyy"));
                    txtperiodEndDate.Text = Convert.ToString(Convert.ToDateTime(user.AuditEndPeriod).ToString("dd-MM-yyyy"));
                }

                var Productdetails = GetByProductID(Convert.ToInt32(user.CustomerID));
                //var Productdetails = UserManagement.GetByProductIDList(UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0);
                //var Productdetails = UserManagement.GetByProductIDList(Convert.ToInt32(AuthenticationHelper.CustomerID));

                if (Productdetails.Contains(3))
                {
                    divAuditRole.Visible = true;
                }
                else if (Productdetails.Contains(4))
                {
                    divAuditRole.Visible = true;
                }
                else
                {
                    divAuditRole.Visible = false;
                }

                if (Productdetails.Contains(8))
                {
                    divSECRole.Visible = true;
                }
                else
                {
                    divSECRole.Visible = false;
                }

                if (Productdetails.Contains(9))
                {
                    divHRRole.Visible = true;
                }
                else
                {
                    divHRRole.Visible = false;
                }

                if (divCustomer.Visible && user.CustomerID.HasValue)
                {
                    ddlCustomer.SelectedValue = user.CustomerID.Value.ToString();
                    ddlCustomer_SelectedIndexChanged(null, null);
                }

                //ddlRole.Enabled = false;
                ddlDepartment.SelectedValue = user.DepartmentID != null ? user.DepartmentID.ToString() : "-1";

                //ADD
                CddlRole.SelectedValue = user.RoleID != null ? user.RoleID.ToString() : "-1";
                ddlRole.SelectedValue = user.HRRoleID != null ? user.HRRoleID.ToString() : "-1";
                ddlRole_SelectedIndexChanged(null, null);
                ddlSECRole.SelectedValue = user.SecretarialRoleID != null ? user.SecretarialRoleID.ToString() : "-1";
                Business.DataRisk.mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);

                if (mstuser.IsAuditHeadOrMgr == "AM")
                {
                    ddlAuditRole.SelectedValue = "2";

                }
                else if (mstuser.RoleID == 2)
                {
                    ddlAuditRole.SelectedValue = "4";
                }
                else if (mstuser.RoleID == 7)
                {
                    ddlAuditRole.SelectedValue = "1";
                }
                else
                {
                    ddlAuditRole.SelectedValue = "-1";
                }
                //if (mstuser.IsAuditHeadOrMgr == "AH")
                //{
                //    ddlAuditRole.SelectedValue = "3";
                //}
                //else if (mstuser.IsAuditHeadOrMgr == "AM")
                //{
                //    ddlAuditRole.SelectedValue = "2";

                //}
                //else if (mstuser.RoleID == 2)
                //{
                //    ddlAuditRole.SelectedValue = "4";
                //}
                //else
                //{
                //    ddlAuditRole.SelectedValue = "1";
                //}
                //END

                //ddlRole.SelectedValue = user.RoleID != null ? user.RoleID.ToString() : "-1";
                //ddlRole_SelectedIndexChanged(null, null);

                if (user.IsHead == true)
                    chkHead.Checked = true;
                else
                    chkHead.Checked = false;

                //risk role
                if (user.IsAuditHeadOrMgr == "AH")
                    rblAuditRole.Items.FindByValue("IAH").Selected = true;

                if (user.IsAuditHeadOrMgr == "AM")
                    rblAuditRole.Items.FindByValue("IAM").Selected = true;

                //com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstuser = UserManagementRisk.GetByID_OnlyEditOption(userID);
                //ddlRiskRole.Enabled = false;
                //ddlRiskRole.SelectedValue = mstuser.RoleID != null ? mstuser.RoleID.ToString() : "-1"; // mstuser.RoleID.ToString();
                //ddlRiskRole_SelectedIndexChanged(null, null);


                ddlReportingTo.SelectedValue = user.ReportingToID != null ? user.ReportingToID.ToString() : "-1";

                if (divCustomerBranch.Visible && user.CustomerBranchID.HasValue)
                {
                    Queue<TreeNode> queue = new Queue<TreeNode>();
                    foreach (TreeNode node in tvBranches.Nodes)
                    {
                        queue.Enqueue(node);
                    }
                    while (queue.Count > 0)
                    {
                        TreeNode node = queue.Dequeue();
                        if (node.Value == user.CustomerBranchID.Value.ToString())
                        {
                            node.Selected = true;
                            break;
                        }
                        else
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                queue.Enqueue(child);
                            }
                        }
                    }
                    tvBranches_SelectedNodeChanged(null, null);
                }

                if (user.IsActive)
                {
                    if (ddlStatus.Items.FindByValue("1") != null)
                    {
                        ddlStatus.ClearSelection();
                        ddlStatus.Items.FindByValue("1").Selected = true;
                    }
                }
                else
                {
                    if (ddlStatus.Items.FindByValue("0") != null)
                    {
                        ddlStatus.ClearSelection();
                        ddlStatus.Items.FindByValue("0").Selected = true;
                    }
                }

                BindParameters(userParameterValues);
                upUsers.Update();

                ScriptManager.RegisterStartupScript(this.upUsers, this.upUsers.GetType(), "HideTreeViewEdit", "$(\"#divBranches\").hide(\"blind\", null, 5, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(ddlCustomer.SelectedValue))
                {
                    if (!string.IsNullOrEmpty(ddlRole.SelectedValue))
                    {
                        string PrimaryRoleAudit = "";
                        bool a = false;
                        bool saveSuccess = false;
                        int avacom_CustomerID = 0;
                        long avacom_UserID = 0;
                        int getproductCOMPLIANCE = -1;
                        int getproductAudit = -1;
                        int getproductHR = -1;
                        int getproductSec = -1;
                        string Isauditheadormgr = null;
                        int customerID = -1;
                        customerID = Convert.ToInt32(ddlCustomer.SelectedValue);

                        var Productdetails = GetByProductID(Convert.ToInt32(customerID));
                        if (Productdetails.Contains(1) && Convert.ToString(CddlRole.SelectedValue)!="")
                        {
                            getproductCOMPLIANCE = Convert.ToInt32(CddlRole.SelectedValue);
                        }
                        else
                        {
                            getproductCOMPLIANCE = -1;
                        }
                        if (Productdetails.Contains(3) || Productdetails.Contains(4))
                        {

                            if (ddlAuditRole.SelectedItem.Text == "Manager")
                            {
                                Isauditheadormgr = "AM";
                                getproductAudit = 7;
                                PrimaryRoleAudit = "Manager";
                            }                           
                            else if (ddlAuditRole.SelectedItem.Text == "Partner")
                            {
                                getproductAudit = 2;
                                Isauditheadormgr = "AH";
                                PrimaryRoleAudit = "Partner";
                            }
                            else if (ddlAuditRole.SelectedItem.Text == "Executive")
                            {
                                getproductAudit = 7;
                                Isauditheadormgr = null;
                                PrimaryRoleAudit = "Executive";
                            }
                            else
                            {
                                getproductAudit = -1;
                                //PrimaryRoleAudit = "Executive";
                            }
                        }
                        else
                        {
                            getproductAudit = -1;
                        }
                        if (Productdetails.Contains(8) && Convert.ToString(ddlSECRole.SelectedValue)!="")
                        {
                            getproductSec = Convert.ToInt32(ddlSECRole.SelectedValue);
                        }
                        if (Productdetails.Contains(9))
                        {
                            getproductHR = Convert.ToInt32(ddlRole.SelectedValue);
                        }

                        //if (ddlRiskRole.SelectedValue == "")//check added
                        //{
                        //    ddlRiskRole.SelectedValue = "-1";
                        //}

                        //a = UserManagementRisk.CheckProductmapping(customerID);
                        //int getproductcOMPLIANCE = -1;
                        //bool ac = UserManagementRisk.CheckProductmapping(customerID, 1);
                        //if (ac == true)
                        //{
                        //    getproductcOMPLIANCE = Convert.ToInt32(CddlRole.SelectedValue);
                        //}
                        //else
                        //{
                        //    getproductcOMPLIANCE = Convert.ToInt32(ddlRiskRole.SelectedValue);
                        //    if (getproductcOMPLIANCE == -1 && Convert.ToString(CddlRole.SelectedValue)!="")
                        //    {
                        //        getproductcOMPLIANCE = Convert.ToInt32(CddlRole.SelectedValue);
                        //    }
                        //}

                        //int getproductrisk = -1;
                        //if (a == true)
                        //{
                        //    getproductrisk = Convert.ToInt32(ddlRiskRole.SelectedValue);
                        //}
                        //else
                        //{
                        //    bool ab = UserManagementRisk.CheckProductmapping(customerID, 4);
                        //    if (ab == true)
                        //    {
                        //        getproductrisk = Convert.ToInt32(ddlRiskRole.SelectedValue);
                        //    }
                        //    else
                        //    {
                        //        if (Convert.ToString(CddlRole.SelectedValue) != "")
                        //        {
                        //            getproductrisk = Convert.ToInt32(CddlRole.SelectedValue);
                        //        }

                        //    }
                        //}
                        //if (ddlAuditRole.SelectedItem.Text == "Audit Head")
                        //{
                        //    Isauditheadormgr = "AH";
                        //    getproductAudit = 7;
                        //}
                        //else if (ddlAuditRole.SelectedItem.Text == "Audit Manager")
                        //{
                        //    Isauditheadormgr = "AM";
                        //    getproductAudit = 7;
                        //}
                        //else if (ddlAuditRole.SelectedItem.Text == "Admin")
                        //{
                        //    getproductAudit = 2;
                        //}
                        //else
                        //{
                        //    getproductAudit = 7;
                        //}
                        //if (Convert.ToString(ddlSECRole.SelectedValue) != "")
                        //{
                        //    getproductSec = Convert.ToInt32(ddlSECRole.SelectedValue);
                        //}
                        //if (Convert.ToString(ddlRole.SelectedValue) != "")
                        //{
                        //    getproductHR = Convert.ToInt32(ddlRole.SelectedValue);
                        //}
                        //if (getproductHR == 28)
                        //{
                        //    Isauditheadormgr = "AH";
                        //}
                        //if (getproductcOMPLIANCE == -1)
                        //{
                        //    getproductcOMPLIANCE = 7;
                        //}

                        //int complianceProdType = RLCS_Master_Management.GetComplianceProductType(Convert.ToInt32(ddlCustomer.SelectedValue));

                        //if (Convert.ToInt32(ddlRole.SelectedValue) >= 14 && Convert.ToInt32(ddlRole.SelectedValue) <= 18)
                        //{
                        //    if (complianceProdType == 0)
                        //    {
                        //        cvEmailError.IsValid = false;
                        //        cvEmailError.ErrorMessage = "Selected role can only assigned to HR Compliance Product Customer only";
                        //        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "hideLoader();", true);
                        //        return;
                        //    }
                        //}
                       
                        #region Compliance User
                        User user = new User()
                        {
                            FirstName = tbxFirstName.Text.Trim(),
                            LastName = tbxLastName.Text.Trim(),
                            Designation = tbxDesignation.Text.Trim(),
                            Email = tbxEmail.Text.Trim(),
                            ContactNumber = tbxContactNo.Text.Trim(),
                            Address = tbxAddress.Text.Trim(),
                            RoleID = getproductCOMPLIANCE,
                            HRRoleID= getproductHR,                           
                            //LitigationRoleID = getproductcOMPLIANCE,
                            IsExternal = false,
                            IsAuditHeadOrMgr = Isauditheadormgr,
                            PrimaryRoleAudit = PrimaryRoleAudit
                        };
                        if (getproductSec==-1)
                        {
                            user.SecretarialRoleID = null;
                        }       
                        else
                        {
                            user.SecretarialRoleID = getproductSec;
                        }                 
                        if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                            user.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);

                        if (chkHead.Checked)
                            user.IsHead = true;
                        else
                            user.IsHead = false;

                        if (divCustomer.Visible)
                        {
                            user.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }

                        if (divCustomerBranch.Visible)
                        {
                            user.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                        }

                        if (ddlReportingTo.SelectedValue != "-1" && ddlReportingTo.SelectedValue != "")
                        {
                            user.ReportingToID = Convert.ToInt64(ddlReportingTo.SelectedValue);
                        }

                        if (ddlRole.SelectedValue == "9" && Auditor1.Visible)
                        {
                            try
                            {
                                user.Startdate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                user.Enddate = DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            catch { }
                        }

                        if (ddlRole.SelectedValue == "9" && Auditor2.Visible)
                        {
                            try
                            {
                                user.AuditStartPeriod = DateTime.ParseExact(txtperiodStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                user.AuditEndPeriod = DateTime.ParseExact(txtperiodEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            catch { }
                        }

                        List<UserParameterValue> parameters = new List<UserParameterValue>();
                        foreach (var item in repParameters.Items)
                        {
                            RepeaterItem entry = item as RepeaterItem;
                            TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                            HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                            parameters.Add(new UserParameterValue()
                            {
                                ID = Convert.ToInt32(hdnID.Value),
                                UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                                Value = tbxValue.Text
                            });
                        }

                        if (rblAuditRole.SelectedIndex != -1)
                        {
                            if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                            {
                                user.IsAuditHeadOrMgr = "AH";
                            }

                            if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                            {
                                user.IsAuditHeadOrMgr = "AM";
                            }
                        }
                        #endregion

                        #region Audit User

                        com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User mstUser = new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.mst_User()
                        {
                            FirstName = tbxFirstName.Text,
                            LastName = tbxLastName.Text,
                            Designation = tbxDesignation.Text,
                            Email = tbxEmail.Text,
                            ContactNumber = tbxContactNo.Text,
                            Address = tbxAddress.Text,
                            RoleID = getproductAudit,
                            //LitigationRoleID = getproductrisk,
                            IsExternal = false,
                            IsAuditHeadOrMgr = Isauditheadormgr,
                            HRRoleID = getproductHR,
                            PrimaryRoleAudit = PrimaryRoleAudit
                        };
                        if (getproductSec == -1)
                        {
                            mstUser.SecretarialRoleID = null;
                        }
                        else
                        {
                            mstUser.SecretarialRoleID = getproductSec;
                        }
                        if (!string.IsNullOrEmpty(ddlDepartment.SelectedValue))
                            mstUser.DepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);

                        if (ddlRole.SelectedValue == "9" && Auditor1.Visible)
                        {
                            try
                            {
                                mstUser.Startdate = DateTime.ParseExact(txtStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                mstUser.Enddate = DateTime.ParseExact(txtEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            catch { }
                        }

                        if (ddlRole.SelectedValue == "9" && Auditor2.Visible)
                        {
                            try
                            {
                                mstUser.AuditStartPeriod = DateTime.ParseExact(txtperiodStartDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                mstUser.AuditEndPeriod = DateTime.ParseExact(txtperiodEndDate.Text, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                            }
                            catch { }
                        }

                        if (chkHead.Checked)
                            mstUser.IsHead = true;
                        else
                            mstUser.IsHead = false;

                        if (divCustomer.Visible)
                        {
                            mstUser.CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        }

                        if (divCustomerBranch.Visible)
                        {
                            mstUser.CustomerBranchID = Convert.ToInt32(tvBranches.SelectedNode.Value);
                        }

                        List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk> parametersRisk = new List<com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk>();
                        foreach (var item in repParameters.Items)
                        {
                            RepeaterItem entry = item as RepeaterItem;
                            TextBox tbxValue = ((TextBox)entry.FindControl("tbxValue"));
                            HiddenField hdnEntityParameterID = ((HiddenField)entry.FindControl("hdnEntityParameterID"));
                            HiddenField hdnID = ((HiddenField)entry.FindControl("hdnID"));

                            parametersRisk.Add(new com.VirtuosoITech.ComplianceManagement.Business.DataRisk.UserParameterValue_Risk()
                            {
                                ID = Convert.ToInt32(hdnID.Value),
                                UserParameterId = Convert.ToInt32(hdnEntityParameterID.Value),
                                Value = tbxValue.Text
                            });
                        }

                        if (rblAuditRole.SelectedIndex != -1)
                        {
                            if (rblAuditRole.SelectedItem.Text == "Is Audit Head")
                            {
                                mstUser.IsAuditHeadOrMgr = "AH";
                            }

                            else if (rblAuditRole.SelectedItem.Text == "Is Audit Manager")
                            {
                                mstUser.IsAuditHeadOrMgr = "AM";
                            }
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                        {
                            user.IsActive = Convert.ToBoolean(Convert.ToInt32(ddlStatus.SelectedValue));
                            mstUser.IsActive = Convert.ToBoolean(Convert.ToInt32(ddlStatus.SelectedValue));
                        }

                        //if role is company admin
                        if (getproductCOMPLIANCE == 2)
                        {
                            var ProductMappingDetails = UserManagement.GetByProductIDList(Convert.ToInt32(customerID));
                            //Litigation Management
                            if (ProductMappingDetails.Contains(2))
                            {
                                if (user.LitigationRoleID == null)
                                {
                                    user.LitigationRoleID = 2;
                                    mstUser.LitigationRoleID = 2;
                                }
                            }
                            //Contract Management
                            if (ProductMappingDetails.Contains(5))
                            {
                                if (user.ContractRoleID == null)
                                {
                                    user.ContractRoleID = 2;
                                    mstUser.ContractRoleID = 2;
                                }
                            }
                            //License Management
                            if (ProductMappingDetails.Contains(6))
                            {
                                if (user.LicenseRoleID == null)
                                {
                                    user.LicenseRoleID = 2;
                                    mstUser.LicenseRoleID = 2;
                                }
                            }
                        }

                        if ((int)ViewState["Mode"] == 1)
                        {
                            user.ID = Convert.ToInt32(ViewState["UserID"]);
                            mstUser.ID = Convert.ToInt32(ViewState["UserID"]);
                        }

                        bool emailExists;
                        UserManagement.Exists(user, out emailExists);
                        if (emailExists)
                        {
                            cvEmailError.IsValid = false;
                            cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "hideLoader();", true);
                            return;
                        }

                        UserManagementRisk.Exists(mstUser, out emailExists);
                        if (emailExists)
                        {
                            cvEmailError.IsValid = false;
                            cvEmailError.ErrorMessage = "User with Same Email Already Exists.";
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "hideLoader();", true);
                            return;
                        }

                        bool result = false;
                        int resultValue = 0;
                        string passwordText = string.Empty;

                        ////GG ADD 02Aug2020
                        int CustomerID = Convert.ToInt32(ddlCustomer.SelectedValue);
                        //END

                        if ((int)ViewState["Mode"] == 0)
                        {
                            ////GG ADD 30July2020
                            bool Valid = false;
                            
                            int NewRecord = 1;
                            Valid = ICIAManagement.CheckForUserValidation(NewRecord, CustomerID);
                            if (Valid)
                            {

                                user.CreatedBy = AuthenticationHelper.UserID;
                                user.CreatedByText = AuthenticationHelper.User;

                                passwordText = Util.CreateRandomPassword(10);
                                user.Password = Util.CalculateAESHash(passwordText);
                                user.EnType = "A";

                                string message = SendNotificationEmail(user, passwordText);

                                mstUser.CreatedBy = AuthenticationHelper.UserID;
                                mstUser.CreatedByText = AuthenticationHelper.User;
                                mstUser.Password = Util.CalculateAESHash(passwordText);
                                mstUser.EnType = "A";

                                resultValue = UserManagement.CreateNew(user, parameters, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                                if (resultValue > 0)
                                {
                                    result = UserManagementRisk.Create(mstUser, parametersRisk, ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), message);
                                    if (result == false)
                                    {
                                        UserManagement.deleteUser(resultValue);
                                    }
                                    else
                                    {
                                        if (user.CustomerID != null)
                                        {
                                            avacom_CustomerID = Convert.ToInt32(user.CustomerID);
                                            avacom_UserID = user.ID;
                                            saveSuccess = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvEmailError.ErrorMessage = "Facility to create more users is not available in the Free version. Kindly contact Avantis to activate the same.";
                                cvEmailError.IsValid = false;
                                return;
                            }
                            ///END
                        }
                        else if ((int)ViewState["Mode"] == 1)
                        {
                            User User = UserManagement.GetByID(Convert.ToInt32(user.ID));

                            if (User != null)
                            {
                                avacom_UserID = Convert.ToInt32(user.ID);
                                passwordText = User.Password;

                                user.IsActive = User.IsActive;
                                mstUser.IsActive = User.IsActive;

                                if (tbxEmail.Text.Trim() != User.Email)
                                {
                                    string message = SendNotificationEmailChanged(user);
                                    string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                                    EmailManager.SendMail(ConfigurationManager.AppSettings["SenderEmailAddress"].ToString(), new List<String>(new String[] { user.Email }), null, null, "AVACOM Email ID Changed.", message);
                                }

                                result = UserManagement.Update(user, parameters);
                                result = UserManagementRisk.Update(mstUser, parametersRisk);

                                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                                {
                                    RLCS_Master_Management.ToggleStatus_ComplianceDB(Convert.ToInt32(avacom_UserID), Convert.ToBoolean(Convert.ToInt32(ddlStatus.SelectedValue)));
                                    RLCS_Master_Management.ToggleStatus_AuditDB(Convert.ToInt32(avacom_UserID), Convert.ToBoolean(Convert.ToInt32(ddlStatus.SelectedValue)));
                                }

                                if (result)
                                {
                                    if (user.CustomerID != null)
                                    {
                                        avacom_CustomerID = Convert.ToInt32(user.CustomerID);
                                        avacom_UserID = user.ID;
                                        saveSuccess = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            cvEmailError.IsValid = false;
                            cvEmailError.ErrorMessage = "Something went wrong, Please try again";
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "hideLoader();", true);
                        }

                        if (result)
                        {
                            if (UserImageUpload.HasFile)
                            {
                                string fileName = user.ID + "-" + user.FirstName + " " + user.LastName + Path.GetExtension(UserImageUpload.PostedFile.FileName);
                                UserImageUpload.PostedFile.SaveAs(Server.MapPath("~/UserPhotos/") + fileName);

                                string filepath = "~/UserPhotos/" + fileName;

                                UserManagement.UpdateUserPhoto(Convert.ToInt32(user.ID), filepath, fileName);
                                UserManagementRisk.UpdateUserPhoto(Convert.ToInt32(mstUser.ID), filepath, fileName);
                            }
                        }

                        if (saveSuccess && avacom_CustomerID != 0 && avacom_UserID != 0)   ////&& complianceProdType > 0
                        {
                            string avacomUserRole = string.Empty;
                            string TLUserRole = string.Empty;

                            RLCS_User_Mapping newRecord = new RLCS_User_Mapping()
                            {
                                AVACOM_UserID = avacom_UserID,
                                ProfileID = avacom_UserID.ToString(),
                                UserType = "A",
                                FirstName = user.FirstName,
                                LastName = user.LastName,
                                Email = user.Email,
                                UserID = user.Email,
                                Password = passwordText,
                                EnType = "A",
                                ContactNumber = user.ContactNumber,
                                Address = user.Address,
                                Designation = user.Designation,
                                IsActive = user.IsActive
                            };

                            if (user.RoleID != 0)
                            {
                                var roleDetails = RoleManagement.GetByID(user.RoleID);

                                if (roleDetails != null)
                                {
                                    avacomUserRole = roleDetails.Code;
                                }

                                if (!string.IsNullOrEmpty(avacomUserRole))
                                {
                                    newRecord.AVACOM_UserRole = avacomUserRole;

                                    if (avacomUserRole.Trim().ToUpper().Equals("HMGMT"))
                                    {
                                        TLUserRole = "HRMGR";
                                    }
                                    else if (avacomUserRole.Trim().ToUpper().Equals("HMGR"))
                                    {
                                        TLUserRole = "MGR";
                                    }
                                    else if (avacomUserRole.Trim().ToUpper().Equals("LSPOC"))
                                    {
                                        TLUserRole = "LSPOC";
                                    }
                                    else if (avacomUserRole.Trim().ToUpper().Equals("HAPPR"))
                                    {
                                        TLUserRole = "APPR";
                                    }
                                    else
                                    {
                                        TLUserRole = "MGR";
                                    }

                                    if (!string.IsNullOrEmpty(TLUserRole))
                                        newRecord.Role = TLUserRole;

                                    if (user.DepartmentID != null)
                                        newRecord.Department = user.DepartmentID.ToString();

                                    newRecord.CustomerID = RLCS_Master_Management.GetCorporateIDByCustID(avacom_CustomerID);

                                    if (user.IsActive)
                                        newRecord.Status = "A";
                                    else if (!user.IsActive)
                                        newRecord.Status = "I";

                                    saveSuccess = RLCS_Master_Management.CreateUpdate_UserMapping(newRecord);

                                    if (saveSuccess)
                                    {
                                        if (avacom_UserID != 0 && avacom_CustomerID != 0)
                                        {
                                            UserCustomerMapping newMappingRecord = new UserCustomerMapping()
                                            {
                                                UserID = Convert.ToInt32(avacom_UserID),
                                                CustomerID = Convert.ToInt32(avacom_CustomerID),
                                                IsActive = true,
                                            };

                                            UserCustomerMappingManagement.CreateUpdate_UserCustomerMapping(newMappingRecord);

                                            //Added New - Auto EntityAssignment in case of CADMN or DADMN
                                            if (avacomUserRole.Equals("CADMN") || avacomUserRole.Equals("DADMN"))
                                            {
                                                var selectedBranchList = CustomerBranchManagement.GetAll_Branches(avacom_CustomerID);

                                                if (selectedBranchList.Count > 0)
                                                {
                                                    List<int> lstComplianceCategory = new List<int>();
                                                    lstComplianceCategory.Add(2);
                                                    lstComplianceCategory.Add(5);

                                                    bool recordActive = true;

                                                    if (user.IsActive)
                                                        recordActive = true;
                                                    else if (!user.IsActive)
                                                        recordActive = false;

                                                    RLCS_Master_Management.CreateUpdate_RLCS_EntityAssignment(Convert.ToInt32(avacom_UserID), selectedBranchList, recordActive, lstComplianceCategory);
                                                }
                                            }
                                        }
                                    }

                                    if (!saveSuccess)
                                    {
                                        LoggerMessage.InsertErrorMsg_DBLog("AVACOM_UserID-" + avacom_UserID + "",
                                                        MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                                    }
                                    else
                                    {
                                        cvEmailError.IsValid = false;
                                        cvEmailError.ErrorMessage = "User Details Save Successfully";
                                        vsUserDetails.CssClass = "alert alert-success";
                                        btnSave.Enabled = false;

                                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "ClosePopupWindowAfterSave();", true);

                                        ViewState["Mode"] = 1;
                                        ViewState["UserID"] = avacom_UserID;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        cvEmailError.IsValid = false;
                        cvEmailError.ErrorMessage = "Select Role";
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "hideLoader();", true);
                    }
                }
                else
                {
                    cvEmailError.IsValid = false;
                    cvEmailError.ErrorMessage = "Select Customer";
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "hideLoader();", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "script", "hideLoader();", true);
            }
        }

        private string SendNotificationEmail(User user, string passwordText)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserRegistration
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@Password", passwordText)
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                    ;

                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        private string SendNotificationEmailChanged(User user)
        {
            try
            {
                int customerID = -1;
                string ReplyEmailAddressName = "";
                if (Convert.ToString(Session["CurrentRole"]).Equals("SADMN"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                if (Convert.ToString(Session["CurrentRole"]).Equals("IMPT"))
                {
                    ReplyEmailAddressName = "Avantis";
                }
                else
                {
                    customerID = UserManagement.GetByID(Convert.ToInt32(Session["CurrentUserId"])).CustomerID ?? 0;
                    ReplyEmailAddressName = CustomerManagement.GetByID(customerID).Name;
                }

                string username = string.Format("{0} {1}", user.FirstName, user.LastName);
                string message = com.VirtuosoITech.ComplianceManagement.Portal.Properties.Settings.Default.EmailTemplate_UserEdit
                                        .Replace("@Username", user.Email)
                                        .Replace("@User", username)
                                        .Replace("@PortalURL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]))
                                        .Replace("@From", ReplyEmailAddressName)
                                        .Replace("@URL", Convert.ToString(ConfigurationManager.AppSettings["PortalURL"]));
                return message;

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvEmailError.IsValid = false;
                cvEmailError.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return null;
        }

        public event EventHandler OnSaved;

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

            int UserID = 0;
            UserID = Convert.ToInt32(Request.QueryString["UserID"]);
            if (UserID != 0)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var query = (from CA in entities.ComplianceAssignments
                                 join CI in entities.ComplianceInstances on CA.ComplianceInstanceID equals CI.ID
                                 where CA.UserID == UserID && CI.IsDeleted == false
                                 select CA).FirstOrDefault();
                    if (query != null)
                    {
                        cvEmailError.IsValid = false;
                        cvEmailError.ErrorMessage = "Compliance already assigned for this user hence can not Inactive";
                        ddlStatus.SelectedValue = "1";

                    }
                    else
                    {

                    }
                }
            }
        }
    }
}