﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_OverdueCompliances : System.Web.UI.Page
    {
        protected string pointname;
        protected string attrubute;
        protected int customerid;
        protected int branchid;
        protected DateTime FromDate;
        protected DateTime Enddate;
        protected string Filter;
        protected int functionid;
        protected string satutoryinternal;
        protected string BranchId;
        protected string chartname;
        protected string listcategoryid;
        protected static string CompDocReviewPath = "";
        protected static int Userid;
        protected static bool IsApprover = false;
        public static List<long> Branchlist = new List<long>();
        protected string Performername;
        protected string Reviewername;
        protected bool flag;

        protected void Page_Load(object sender, EventArgs e)
        {
            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            BranchId = Request.QueryString["BranchID"];

            if (!IsPostBack)
            {
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();


                                ViewState["SortOrder"] = "Desc";
                                ViewState["SortExpression"] = "OverdueBy";
                                flag = false;

                                BindLocationFilter(assignedbranchIDs);
                                if(BranchId != "-1")
                                {
                                    GetAllHierarchy(customerID, Convert.ToInt32(BranchId));
                                    
                                }
                                BindActList(Branchlist);
                                BindCategories();

                                BindDetailView();

                                TableCell tableCell = grdSummaryDetails.HeaderRow.Cells[3];
                                Image img = new Image();
                                img.ImageUrl = "../Images/up_arrow1.png";
                                tableCell.Controls.Add(new LiteralControl("&nbsp;"));
                                tableCell.Controls.Add(img);

                                //btnDownload.Visible = false;
                                GetPageDisplaySummary();

                                if (tvFilterLocation.SelectedValue != "-1")
                                    Session["LocationName"] = tvFilterLocation.SelectedNode.Text;
                                else
                                    Session["LocationName"] = tvFilterLocation.Nodes[1].Text;

                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        private void BindCategories()
        {
            try
            {
                ddlFunctions.DataSource = null;
                ddlFunctions.DataBind();

                ddlFunctions.DataTextField = "Name";
                ddlFunctions.DataValueField = "ID";

                var lstCategories = ComplianceCategoryManagement.GetAll();

                if (lstCategories.Count > 0)
                    lstCategories = lstCategories.Where(row => row.ID == 2 || row.ID == 5).ToList();

                ddlFunctions.DataSource = lstCategories;
                ddlFunctions.DataBind();

                ddlFunctions.Items.Insert(0, new ListItem("Select Category", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected string GetPerformer(long complianceinstanceid)
        {
            try
            {
                string result = "";
                result = DashboardManagement.GetUserName(complianceinstanceid, 3);
                Performername = result;
                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return "";
            }
        }
        
        private void BindActList(List<long> blist)
        {
            bool IsAvantisFlag = false;
            try
            {
                if (!string.IsNullOrEmpty(Request.QueryString["IsAvantisFlag"]))
                {
                    IsAvantisFlag = Convert.ToBoolean(Request.QueryString["IsAvantisFlag"]);
                }
                int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                ddlAct.Items.Clear();
                var ActList = ActManagement.GetAllAssignedActs_RLCS(customerID, AuthenticationHelper.UserID, -1, AuthenticationHelper.ProfileID, blist, IsAvantisFlag);//Query string Same As Compliance Details
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();
                ddlAct.Items.Insert(0, new ListItem("Act", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation.Nodes.Clear();

                    string isstatutoryinternal = "";
                    //if (ddlStatus.SelectedValue == "Statutory")
                    //{
                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        satutoryinternal = Request.QueryString["Internalsatutory"];
                    }
                    if (satutoryinternal == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (satutoryinternal == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/Sub-Entity/Location", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected bool ViewDownload(int statusid)
        {
            if (statusid != 1)
                return true;
            else
                return false;
        }
        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }
        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindDetailView()
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                int CustomerBranchId = Convert.ToInt32(tvFilterLocation.SelectedValue);
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    ddlAct.ClearSelection();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    Branchlist.ToList();
                }
                else if(BranchId!="-1")
                {
                 // Branchlist.Clear();
                }
                else
                {
                    Branchlist.Clear();
                }
                if (ddlAct.SelectedValue == "-1" || ddlAct.SelectedValue == "")
                {
                    BindActList(Branchlist);
                }

                int category = -1;
                int ActID = -1; 
                int risk = -1;

                if (ddlFunctions.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlFunctions.SelectedValue))
                    category = Convert.ToInt32(ddlFunctions.SelectedValue);

                if (ddlAct.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlAct.SelectedValue))
                    ActID = Convert.ToInt32(ddlAct.SelectedValue);

                if (ddlRisk.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlRisk.SelectedValue))
                    risk = Convert.ToInt32(ddlRisk.SelectedValue);

                var detailView = RLCSManagement.GetOverdueComplianceListByRisk(customerid, AuthenticationHelper.ProfileID, UserID, risk).ToList()
                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                if (Branchlist.Count > 0)
                {
                    detailView = detailView.Where(row => Branchlist.Contains((long)row.CustomerBranchID)).Distinct().ToList();
                }
                else
                {
                    if (CustomerBranchId > 0)
                    {
                        detailView = detailView.Where(x => x.CustomerBranchID.Equals(CustomerBranchId)).ToList();
                    }
                }
              
                if (ActID != -1)
                {
                    detailView = detailView.Where(entry => (entry.ActID == ActID)).ToList();
                }

                if (category != -1)
                {
                    detailView = detailView.Where(entry => (entry.ComplianceCategoryId == Convert.ToByte(category))).ToList();
                }

                if (risk != -1)
                {
                    detailView = detailView.Where(entry => (entry.Risk == Convert.ToByte(risk))).ToList();
                }

                if (ViewState["SortOrder"].ToString() == "Asc")
                {
                    if (ViewState["SortExpression"].ToString() == "Branch")
                    {
                        detailView = detailView.OrderBy(entry => entry.Branch).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        detailView = detailView.OrderBy(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ScheduledOn")
                    {
                        detailView = detailView.OrderBy(entry => entry.ScheduledOn).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ForMonth")
                    {
                        detailView = detailView.OrderBy(entry => entry.ForMonth).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "OverdueBy")
                    {
                        detailView = detailView.OrderBy(entry => entry.OverdueBy).ToList();
                    }
                    direction = SortDirection.Descending;
                }
                else
                {
                    if (ViewState["SortExpression"].ToString() == "Branch")
                    {
                        detailView = detailView.OrderByDescending(entry => entry.Branch).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ShortDescription")
                    {
                        detailView = detailView.OrderByDescending(entry => entry.ShortDescription).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ScheduledOn")
                    {
                        detailView = detailView.OrderByDescending(entry => entry.ScheduledOn).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "ForMonth")
                    {
                        detailView = detailView.OrderByDescending(entry => entry.ForMonth).ToList();
                    }
                    if (ViewState["SortExpression"].ToString() == "OverdueBy")
                    {
                        detailView = detailView.OrderByDescending(entry => entry.OverdueBy).ToList();
                    }
                    direction = SortDirection.Ascending;
                }

                grdSummaryDetails.DataSource = detailView;
                grdSummaryDetails.DataBind();
                Session["TotalRows"] = detailView.Count;
                GetPageDisplaySummary();
                UpDetailView.Update();
                Session["grdDetailData"] = (grdSummaryDetails.DataSource as List<SP_RLCS_GetOverDueHighCompliance_Result>).ToDataTable();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }


        protected void grdSummaryDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
                int UserID = Convert.ToInt32(AuthenticationHelper.UserID);

                int location = Convert.ToInt32(tvFilterLocation.SelectedValue);

                int category = -1;
                int ActID = -1;
                int risk = -1;

                if (ddlFunctions.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlFunctions.SelectedValue))
                    category = Convert.ToInt32(ddlFunctions.SelectedValue);

                if (ddlAct.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlAct.SelectedValue))
                    ActID = Convert.ToInt32(ddlAct.SelectedValue);

                if (ddlRisk.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlRisk.SelectedValue))
                    risk = Convert.ToInt32(ddlRisk.SelectedValue);

                //int category = Convert.ToInt32(ddlFunctions.SelectedValue);
                //int ActID = Convert.ToInt32(ddlAct.SelectedValue);
                //int risk = Convert.ToInt32(ddlRisk.SelectedValue);

                var detailView = RLCSManagement.GetOverdueComplianceListByRisk(customerid,AuthenticationHelper.ProfileID, UserID, risk).ToList()
                                   .GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();

                List<object> dataSource = new List<object>();
                foreach (var complianceInfo in detailView)
                {
                    dataSource.Add(new
                    {
                        complianceInfo.Branch,
                        complianceInfo.ShortDescription,
                        complianceInfo.ScheduledOn,
                        complianceInfo.ForMonth,
                        complianceInfo.Status,
                        complianceInfo.RiskCategory,
                        complianceInfo.Risk,
                        complianceInfo.RoleID,
                        complianceInfo.ScheduledOnID,
                        complianceInfo.Role,
                        complianceInfo.User,
                        complianceInfo.UserID,
                        complianceInfo.ActID,
                        complianceInfo.ActName,
                        complianceInfo.ComplianceCategoryId,
                        complianceInfo.ComplianceID,
                        complianceInfo.ComplianceInstanceID,
                        complianceInfo.ComplianceStatusID,
                        complianceInfo.ComplianceTransactionID,
                        complianceInfo.ComplianceTypeId,
                        complianceInfo.CustomerBranchID,
                        complianceInfo.CustomerID,
                        complianceInfo.Description,
                        complianceInfo.OverdueBy,
                    });
                }

                if (direction == SortDirection.Ascending)
                {
                    dataSource = dataSource.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Descending;
                    ViewState["SortOrder"] = "Asc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                else
                {
                    dataSource = dataSource.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    direction = SortDirection.Ascending;
                    ViewState["SortOrder"] = "Desc";
                    ViewState["SortExpression"] = e.SortExpression.ToString();
                }
                foreach (DataControlField field in grdSummaryDetails.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdSummaryDetails.Columns.IndexOf(field);
                    }
                }

                grdSummaryDetails.DataSource = dataSource;
                grdSummaryDetails.DataBind();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdCompliances_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSummaryDetails.PageIndex = e.NewPageIndex;
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdSummaryDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
            sortImage.ImageAlign = ImageAlign.AbsMiddle;

            if (flag == true)
            {
                if (direction == SortDirection.Ascending)
                {
                    sortImage.ImageUrl = "../Images/up_arrow1.png";
                    sortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    sortImage.ImageUrl = "../Images/down_arrow1.png";
                    sortImage.AlternateText = "Descending Order";
                }
                headerRow.Cells[columnIndex].Controls.Add(sortImage);
            }
            flag = true;
        }

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                ViewState["checkedCompliances"] = null;

                SelectedPageNo.Text = "1";

                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;

                int location = 0;
                int Category = 0;
                int Act = 0;

                if (tvFilterLocation.SelectedValue != "-1")
                {
                    location = tvFilterLocation.SelectedNode.Text.Length;

                    if (location >= 50)
                        lblAdvanceSearchScrum.Text = "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 30) + "...";
                    else
                        lblAdvanceSearchScrum.Text = "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                }

                if (ddlFunctions.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlFunctions.SelectedValue))
                {
                    Category = ddlFunctions.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Category: </b>" + ddlFunctions.SelectedItem.Text;
                    }
                    else
                    {
                        if (Category >= 20)
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlFunctions.SelectedItem.Text.Substring(0, 20) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Category: </b>" + ddlFunctions.SelectedItem.Text;
                    }
                }

                if (ddlAct.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlAct.SelectedValue))
                {
                    Act = ddlAct.SelectedItem.Text.Length;

                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (Act >= 30)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 30) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }


                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                    BindDetailView();
                }
                else
                {
                    divAdvSearch.Visible = false;
                    BindDetailView();
                }

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0];
                node.Selected = true;

                lblAdvanceSearchScrum.Text = String.Empty;
                divAdvSearch.Visible = false;

                tvFilterLocation_SelectedNodeChanged(sender, e);

                //Rebind Grid
                BindDetailView();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        protected void grdSummaryDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");

                    if (lblStatus != null)
                    {
                        if (lblStatus.Text.Trim() == "Open")
                            lblStatus.Text = "Overdue";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    try
                    {
                        ExcelWorksheet exWorkSheet = exportPackge.Workbook.Worksheets.Add("Report");
                        DataTable ExcelData = null;
                        String LocationName = String.Empty;

                        DateTime CurrentDate = DateTime.Today.Date;

                        if (Session["LocationName"].ToString() != "")
                            LocationName = Session["LocationName"].ToString();
                        DataView view = new System.Data.DataView((DataTable)Session["grdDetailData"]);
                        ExcelData = view.ToTable("Selected", false, "Branch", "ActName", "ShortForm", "RequiredForms", "Frequency", "ScheduledOn", "ForMonth", "Status", "RiskCategory", "ComplianceStatusID", "ScheduledOnID", "ComplianceInstanceID", "OverdueBy");

                        //foreach (DataRow item in ExcelData.Rows)
                        //{
                        //    item["User"] = GetPerformer(Convert.ToInt32(item["ComplianceInstanceID"].ToString()));
                        //}
                        ExcelData.Columns.Remove("ComplianceStatusID");
                        ExcelData.Columns.Remove("ScheduledOnID");
                        ExcelData.Columns.Remove("ComplianceInstanceID");

                        exWorkSheet.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A1"].Value = "Entity/ Location:";

                        exWorkSheet.Cells["B1:C1"].Merge = true;
                        exWorkSheet.Cells["B1"].Value = LocationName;

                        exWorkSheet.Cells["A2"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A2"].Value = "Report Name:";

                        exWorkSheet.Cells["B2:C2"].Merge = true;


                        exWorkSheet.Cells["B2"].Value = "Report of Overdue Compliances";

                        exWorkSheet.Cells["A3"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A3"].Value = "Report Generated On:";

                        exWorkSheet.Cells["B3"].Value = DateTime.Today.Date.ToString("dd/MMM/yyyy");

                        exWorkSheet.Cells["A5"].LoadFromDataTable(ExcelData, true);


                        exWorkSheet.Cells["A5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["A5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["A5"].Value = "Entity / Branch";
                        exWorkSheet.Cells["A5"].AutoFitColumns(20);

                        exWorkSheet.Cells["B5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["B5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["B5"].Value = "Act Name";
                        exWorkSheet.Cells["B5"].AutoFitColumns(75);

                        exWorkSheet.Cells["C5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["C5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["C5"].Value = "Compliance";
                        exWorkSheet.Cells["C5"].AutoFitColumns(40);

                        //exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        //exWorkSheet.Cells["D5"].Value = "Performer";
                        //exWorkSheet.Cells["D5"].AutoFitColumns(30);

                        //exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        //exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        //exWorkSheet.Cells["E5"].Value = "Reviewer";
                        //exWorkSheet.Cells["E5"].AutoFitColumns(30);

                        exWorkSheet.Cells["D5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["D5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["D5"].Value = "Form";
                        exWorkSheet.Cells["D5"].AutoFitColumns(15);

                        exWorkSheet.Cells["E5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["E5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["E5"].Value = "Frequency";
                        exWorkSheet.Cells["E5"].AutoFitColumns(10);
                        //////////////End
                        exWorkSheet.Cells["F5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["F5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["F5"].Value = "Due Date";
                        exWorkSheet.Cells["F5"].AutoFitColumns(12);

                        exWorkSheet.Cells["G5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["G5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["G5"].Value = "Period";
                        exWorkSheet.Cells["G5"].AutoFitColumns(10);

                        exWorkSheet.Cells["H5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["H5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["H5"].AutoFitColumns(10);

                        exWorkSheet.Cells["I5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["I5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["I5"].Value = "Risk";
                        exWorkSheet.Cells["I5"].AutoFitColumns(8);

                        exWorkSheet.Cells["J5"].Style.Font.Bold = true;
                        exWorkSheet.Cells["J5"].Style.Font.Size = 12;
                        exWorkSheet.Cells["J5"].Value = "Overdue By(Days)";
                        exWorkSheet.Cells["J5"].AutoFitColumns(15);

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 9])
                        {
                            col.Style.WrapText = true;
                            col.Style.Numberformat.Format = "dd/MMM/yyyy";
                            //col.AutoFitColumns();
                        }

                        using (ExcelRange col = exWorkSheet.Cells[5, 1, 5 + ExcelData.Rows.Count, 10])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }

                        Byte[] fileBytes = exportPackge.GetAsByteArray();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment;filename=Summary_Of_Overdue_Compliance.xlsx");
                        Response.Charset = "";
                        Response.ContentType = "application/vnd.ms-excel";
                        StringWriter sw = new StringWriter();
                        Response.BinaryWrite(fileBytes);
                        HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                        HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                        HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void chkCompliancesHeader_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)grdSummaryDetails.HeaderRow.FindControl("chkCompliancesHeader");

                foreach (GridViewRow row in grdSummaryDetails.Rows)
                {
                    CheckBox chkCompliances = (CheckBox)row.FindControl("chkCompliances");

                    if (ChkBoxHeader.Checked)
                        chkCompliances.Checked = true;
                    else
                        chkCompliances.Checked = false;
                }

                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }


        }

        protected void chkCompliances_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                int Count = 0;

                CheckBox ChkBoxHeader = (CheckBox)grdSummaryDetails.HeaderRow.FindControl("chkCompliancesHeader");

                foreach (GridViewRow row in grdSummaryDetails.Rows)
                {
                    CheckBox chkCompliances = (CheckBox)row.FindControl("chkCompliances");

                    if (chkCompliances.Checked)
                        Count++;
                }

                if (Count == grdSummaryDetails.Rows.Count)
                    ChkBoxHeader.Checked = true;
                else
                    ChkBoxHeader.Checked = false;

                ShowSelectedRecords(sender, e);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }


        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindDetailView();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ShowSelectedRecords(object sender, EventArgs e)
        {
            try
            {
                lblTotalSelected.Text = "";
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }

        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindDetailView();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdSummaryDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdSummaryDetails.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                //Reload the Grid
                BindDetailView();

            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}