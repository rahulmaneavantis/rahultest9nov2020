﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System.Reflection;
using System.Web.Services;
using System.Net.Http;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class EntitiesDetails : System.Web.UI.Page
    {
        protected static string userProfileID;
        protected static string userProfileID_Encrypted;
        protected static string authKey;

        protected static string avacomAPIUrl;
        protected static string tlConnectAPIUrl;

        protected void Page_Load(object sender, EventArgs e)
        {
            avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            tlConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

            if (!IsPostBack)
            {
                try
                {
                    string userProfileID_Encrypted = string.Empty;
                    string authKey = string.Empty;

                    string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];

                    if (!string.IsNullOrEmpty(TLConnectKey))
                    {
                        userProfileID = string.Empty;
                        userProfileID = AuthenticationHelper.ProfileID;

                        if(!string.IsNullOrEmpty(userProfileID))
                        {
                            userProfileID_Encrypted = CryptographyHandler.encrypt(userProfileID.Trim(), TLConnectKey);
                        }

                        authKey = AuthenticationHelper.AuthKey;
                        hdnAuthKey.Value = authKey;
                        hdnProfileID.Value = userProfileID_Encrypted;
                        if (!string.IsNullOrEmpty(userProfileID)
                           && !string.IsNullOrEmpty(userProfileID_Encrypted)
                           && !string.IsNullOrEmpty(authKey))
                        {
                            BindGrid(tlConnectAPIUrl, userProfileID, userProfileID_Encrypted, authKey);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        public void BindGrid(string requestUrl, string profileID, string encryptedProfileID, string authenticationKey)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    //
                    requestUrl += "Dashboard/GetProfile_BasicInformationByProfileID?profileID=" + profileID;

                    string responseData = RLCSAPIClasses.InvokeWithAuthKey("GET", requestUrl, authenticationKey, encryptedProfileID, "");

                    if (!string.IsNullOrEmpty(responseData))
                    {
                        var _objProfileInfo = JsonConvert.DeserializeObject<Profile_BasicInfo>(responseData);

                        if (_objProfileInfo != null)
                        {
                            if (_objProfileInfo.lstBasicInfrmation != null)
                            {
                                grdEntityDetails.DataSource = _objProfileInfo.lstBasicInfrmation;
                                grdEntityDetails.DataBind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}