﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_Location_Master : System.Web.UI.Page
    {
        protected static int CustId;
        protected int UserId;
        protected static string Path;
        protected void Page_Load(object sender, EventArgs e)
        {
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

            //WebRequest req = WebRequest.Create("http://c77de55b.ngrok.io/rlcs/rlcstl/public/");
            //WebResponse res = req.GetResponse();
            //StreamReader sr = new StreamReader(res.GetResponseStream());
            //string html = sr.ReadToEnd();

            //if (!string.IsNullOrEmpty(html))
            //{
            //    Literal ltrDyn = new Literal();
            //    ltrDyn.Text = html;
            //    //pnlDynamic.Controls.Add(ltrDyn);
            //}
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            string masterpage = Convert.ToString(Session["masterpage"]);
            if (masterpage == "HRPlusSPOCMGR")
            {
                this.Page.MasterPageFile = "~/HRPlusSPOCMGR.Master";
            }
            else if (masterpage == "HRPlusCompliance")
            {
                this.Page.MasterPageFile = "~/HRPlusCompliance.Master";
            }
        }
    }
}