﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RegistrationsRepository : System.Web.UI.Page
    {
        public int CustId, UserID;
        protected static string avacomRLCSAPI_URL;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                UserID = Convert.ToInt32(AuthenticationHelper.UserID);
                ViewState["CustomerID"] = CustId;
                avacomRLCSAPI_URL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

            }

        }
    }
}