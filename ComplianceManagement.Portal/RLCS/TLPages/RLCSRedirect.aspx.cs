﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS.TLPages
{

    public class RLCSRedirectMenu
    {
        public int Id { get; set; }

        public string Area { get; set; }
        public string Controller { get; set; }

        public string Action { get; set; }
    }

    public static class RLCSEncryption
    {

        public static string encrypt(string encryptString)
        {
            string EncryptionKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"].ToString();
            byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    encryptString = Convert.ToBase64String(ms.ToArray());
                }
            }
            return encryptString;
        }
    }



    public partial class RLCSRedirect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<RLCSRedirectMenu> Lst = new List<RLCSRedirectMenu>();
            //Client_Setup/ClientPODetails/GetListOfPODetails
            Lst.Add(new RLCSRedirectMenu { Id = 1, Area = "MastersSetup", Controller = "AuthorityAddresses", Action = "AuthorityAddresses" });
            Lst.Add(new RLCSRedirectMenu { Id = 2, Area = "Client_Setup", Controller = "ClientPODetails", Action = "GetListOfPODetails" });
            Lst.Add(new RLCSRedirectMenu { Id = 3, Area = "Client_Setup", Controller = "Commercial_Setup", Action = "ClientCommercialSetup" });
            Lst.Add(new RLCSRedirectMenu { Id = 4, Area = "MastersSetup", Controller = "RegulatoryKnowledge", Action = "RegulatoryKnowledge" });
            Lst.Add(new RLCSRedirectMenu { Id = 5, Area = "Client_Setup", Controller = "ScopeofWork", Action = "Scope_of_Works" });
            Lst.Add(new RLCSRedirectMenu { Id = 6, Area = "OneTimeActivity", Controller = "Registrations", Action = "RegistrationsRepository" });
            Lst.Add(new RLCSRedirectMenu { Id = 7, Area = "Downloads", Controller = "SummaryDetailsReport", Action = "EPFESICSummaryDetailsReport" });
            Lst.Add(new RLCSRedirectMenu { Id = 8, Area = "MastersSetup", Controller = "MinimumWagesReckoner", Action = "MinimumWagesReckoner" });
            Lst.Add(new RLCSRedirectMenu { Id = 9, Area = "Client_Setup", Controller = "ClientComplianceCertificate", Action = "ComplianceCertificateDetails" });
            Lst.Add(new RLCSRedirectMenu { Id = 10, Area = "OneTimeActivity", Controller = "Liasoning", Action = "LiasoningActivity" });
            Lst.Add(new RLCSRedirectMenu { Id = 11, Area = "MastersSetup", Controller = "DSC", Action = "DigitalSignatureList" });
            Lst.Add(new RLCSRedirectMenu { Id = 12, Area = "RLCSReports", Controller = "ClientMasterReport", Action = "ClientMasterReport" });
            Lst.Add(new RLCSRedirectMenu { Id = 13, Area = "OneTimeActivity", Controller = "InspectionNotices", Action = "InspectionNoticeCreation" });
            Lst.Add(new RLCSRedirectMenu { Id = 14, Area = "HealthCheck", Controller = "HealthCheckCreation", Action = "HealthCheckCreation" });
            Lst.Add(new RLCSRedirectMenu { Id = 15, Area = "Downloads", Controller = "SummaryDetailsReport", Action = "KYCPMRPYSummaryReportDetails" });
            Lst.Add(new RLCSRedirectMenu { Id = 16, Area = "MonthlyInputs", Controller = "CPOEsicUpload", Action = "CPOEsicUpload" });
            Lst.Add(new RLCSRedirectMenu { Id = 17, Area = "MIS", Controller = "MISCompliance", Action = "MISComplianceUpload" });
            Lst.Add(new RLCSRedirectMenu { Id = 18, Area = "MastersSetup", Controller = "RegulatoryAbstracts", Action = "RegulatoryAbstracts" });
            Lst.Add(new RLCSRedirectMenu { Id = 19, Area = "Leave", Controller = "Leaves", Action = "LeaveSummaryUploadProcess" });
            Lst.Add(new RLCSRedirectMenu { Id = 20, Area = "MastersSetup", Controller = "DocumentActivityMapping", Action = "DocumentActivityMapping" });
            Lst.Add(new RLCSRedirectMenu { Id = 21, Area = "Client_Setup", Controller = "ManageClientLocation", Action = "ClientLocationMasterList" });

            Lst.Add(new RLCSRedirectMenu { Id = 22, Area = "Invoicing", Controller = "InvoiceCreation", Action = "InvoiceCreation" });
            Lst.Add(new RLCSRedirectMenu { Id = 23, Area = "Invoicing", Controller = "InvoiceUpdation", Action = "InvoiceUpdation" });
            Lst.Add(new RLCSRedirectMenu { Id = 24, Area = "Invoicing", Controller = "InvoiceApproval", Action = "InvoiceApproval" });
            Lst.Add(new RLCSRedirectMenu { Id = 25, Area = "Invoicing", Controller = "InvoiceFaceSheet", Action = "InvoiceFaceSheet" });
            Lst.Add(new RLCSRedirectMenu { Id = 26, Area = "Invoicing", Controller = "InvoiceReport", Action = "InvoiceReport" });
            Lst.Add(new RLCSRedirectMenu { Id = 27, Area = "Invoicing", Controller = "InvoiceLock", Action = "InvoiceLock" });
            Lst.Add(new RLCSRedirectMenu { Id = 28, Area = "Invoicing", Controller = "InvoiceGovtFee", Action = "InvoiceGovtFee" });
            Lst.Add(new RLCSRedirectMenu { Id = 29, Area = "Invoicing", Controller = "InvoiceGSTMaster", Action = "InvoiceGSTMaster" });
            Lst.Add(new RLCSRedirectMenu { Id = 30, Area = "Invoicing", Controller = "InvoiceGSTMaster", Action = "InvoiceGstUploadApproval" });

            Lst.Add(new RLCSRedirectMenu { Id = 31, Area = "RLCSReports", Controller = "WomenWorkingNightShift", Action = "WomenWorkingNightShiftReport" });
            Lst.Add(new RLCSRedirectMenu { Id = 32, Area = "RLCSReports", Controller = "ER1Applicability", Action = "ER1ApplicabilityReport" });
            Lst.Add(new RLCSRedirectMenu { Id = 33, Area = "MastersSetup", Controller = "EmployeeMinimumWageReckoner", Action = "EmployeeMinimumWageReckoner" });
            Lst.Add(new RLCSRedirectMenu { Id = 34, Area = "RLCSReports", Controller = "MonthlyEmpReport", Action = "MonthlyEmpReport" });

            Lst.Add(new RLCSRedirectMenu { Id = 35, Area = "Client_Setup", Controller = "ManageTracker", Action = "Index" });
            Lst.Add(new RLCSRedirectMenu { Id = 36, Area = "RLCSReports", Controller = "SalaryReport", Action = "SalaryReport" });
            Lst.Add(new RLCSRedirectMenu { Id = 37, Area = "RLCSReports", Controller = "MastersReport", Action = "MastersReport" });
            Lst.Add(new RLCSRedirectMenu { Id = 38, Area = "RLCSReports", Controller = "MISSummary", Action = "DownloadMISSummary" });
            Lst.Add(new RLCSRedirectMenu { Id = 39, Area = "Invoicing", Controller = "InvoiceDownloadZonewise", Action = "InvoiceDownloadZonewise" });
            Lst.Add(new RLCSRedirectMenu { Id = 40, Area = "RLCSReports", Controller = "MISReport", Action = "MISReport" });

            var MenuId = Request.QueryString["id"];

            RLCSRedirectMenu menudetails = Lst.FirstOrDefault(a => a.Id.ToString() == MenuId);  //from x in Lst where x.Id == nwId select x;

            
            var RLCSURL = ConfigurationManager.AppSettings["RLCS_WEB_URL"].ToString() + "/" + menudetails.Area + "/" + menudetails.Controller + "/" + menudetails.Action + "?AvacomUserId=" + RLCSEncryption.encrypt(User.Identity.Name.Split(';')[0]);


            RLCSIFrame.Attributes["src"] = RLCSURL;

            //Response.Redirect(RLCSURL, false);
        }
    }


}