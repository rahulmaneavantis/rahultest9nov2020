﻿
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class TempTable
    {
        public long ComplianceId { get; set; }
        public int CustomerBranchID { get; set; }
    }

    public partial class HRPlus_ComplianceAssignmentUpload : System.Web.UI.Page
    {
        bool suucess = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindCustomers();

                    if (!string.IsNullOrEmpty(Request.QueryString["CustID"]))
                    {
                        var custID = Request.QueryString["CustID"];

                        if (!string.IsNullOrEmpty(custID))
                        {
                            int customerID = Convert.ToInt32(custID);

                            if (customerID != 0)
                            {
                                if (ddlCustomerList.Items.FindByValue(customerID.ToString()) != null)
                                {
                                    ddlCustomerList.ClearSelection();
                                    ddlCustomerList.Items.FindByValue(customerID.ToString()).Selected = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadAssignment.IsValid = false;
                cvUploadAssignment.ErrorMessage = "Something went wrong, Please try again";
            }
        }
        
        private void BindCustomers()
        {
            try
            {
                ddlCustomerList.DataTextField = "Name";
                ddlCustomerList.DataValueField = "ID";

                int customerID = -1;
                int serviceProviderID = -1;
                int distributorID = -1;
                string loggedUserRole = string.Empty;

                if (AuthenticationHelper.Role == "CADMN")
                {
                    //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    loggedUserRole = AuthenticationHelper.Role;
                }
                else if (AuthenticationHelper.Role == "SPADM")
                {
                    serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    loggedUserRole = AuthenticationHelper.Role;
                }
                else if (AuthenticationHelper.Role == "DADMN")
                {
                    distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    loggedUserRole = AuthenticationHelper.Role;
                }
                else if (AuthenticationHelper.Role == "IMPT")
                {
                    loggedUserRole = "CADMN";
                    if (!string.IsNullOrEmpty(Request.QueryString["CustID"]))
                    {
                        var custID = Request.QueryString["CustID"];

                        if (!string.IsNullOrEmpty(custID))
                        {
                            customerID = Convert.ToInt32(custID);
                        }
                    }
                }

                //ddlCustomer.DataSource = CustomerManagement.GetAll_HRComplianceCustomersByServiceProviderOrDistributor(customerID, serviceProviderID, distributorID, 2);
                ddlCustomerList.DataSource = CustomerManagement.GetAll_HRComplianceCustomers(AuthenticationHelper.UserID, customerID, serviceProviderID, distributorID, loggedUserRole, false);
                ddlCustomerList.DataBind();

                if (AuthenticationHelper.Role == "CADMN")
                {
                    if (ddlCustomerList.Items.FindByValue(customerID.ToString()) != null)
                    {
                        ddlCustomerList.ClearSelection();
                        ddlCustomerList.Items.FindByValue(customerID.ToString()).Selected = true;
                    }
                }

                ddlCustomerList.Items.Insert(0, new ListItem("Select", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadAssignment.IsValid = false;
                cvUploadAssignment.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            if (MasterFileUpload.HasFile)
            {
                try
                {
                    if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                    {
                        if (ddlCustomerList.SelectedValue != "-1")
                        {
                            string filename = Path.GetFileName(MasterFileUpload.FileName);
                            MasterFileUpload.SaveAs(Server.MapPath("~/RLCS/Uploaded/") + filename.Trim());
                            FileInfo excelfile = new FileInfo(Server.MapPath("~/RLCS/Uploaded/") + filename.Trim());
                            if (excelfile != null)
                            {
                                using (ExcelPackage xlWorkbook = new ExcelPackage(excelfile))
                                {
                                    if (rdoAssignCompliance.Checked)
                                    {
                                        bool flag = ComplianceAssignmentSheetsExitsts(xlWorkbook, "UploadComplianceAssignment");
                                        if (flag == true)
                                        {
                                            int customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);

                                            if (HttpContext.Current.Cache.Get("ComplianceListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("ComplianceListData");
                                            }

                                            if (HttpContext.Current.Cache.Get("UserListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("UserListData");
                                            }

                                            if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                            }

                                            if (HttpContext.Current.Cache.Get("DepartmentListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("DepartmentListData");
                                            }

                                            if (HttpContext.Current.Cache.Get("ComplianceInstanceAssignmentListData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("ComplianceInstanceAssignmentListData");
                                            }

                                            if (HttpContext.Current.Cache.Get("RegisterComplianceMappingData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("RegisterComplianceMappingData");
                                            }

                                            if (HttpContext.Current.Cache.Get("ReturnComplianceMappingData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("ReturnComplianceMappingData");
                                            }

                                            if (HttpContext.Current.Cache.Get("ChallanComplianceMappingData") != null)
                                            {
                                                HttpContext.Current.Cache.Remove("ChallanComplianceMappingData");
                                            }   

                                            GetCompliance();
                                            GetUser();
                                            GetCustomerBranch(customerID);
                                            GetDepartment(customerID);

                                            GetComplianceInstanceAssignment(customerID);

                                            GetRegisterComplianceMapping();
                                            GetReturnComplianceMapping();
                                            GetChallanComplianceMapping();

                                            ComplianceAssignmentData(xlWorkbook);

                                            if (suucess == true)
                                            {
                                                cvUploadAssignment.IsValid = false;
                                                cvUploadAssignment.ErrorMessage = "Data Uploaded Successfully";

                                                if (HttpContext.Current.Cache.Get("ComplianceListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("ComplianceListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("UserListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("UserListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("CustomerBranchListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("CustomerBranchListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("DepartmentListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("DepartmentListData");
                                                }
                                                if (HttpContext.Current.Cache.Get("ComplianceInstanceAssignmentListData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("ComplianceInstanceAssignmentListData");
                                                }

                                                if (HttpContext.Current.Cache.Get("RegisterComplianceMappingData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("RegisterComplianceMappingData");
                                                }

                                                if (HttpContext.Current.Cache.Get("ReturnComplianceMappingData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("ReturnComplianceMappingData");
                                                }

                                                if (HttpContext.Current.Cache.Get("ChallanComplianceMappingData") != null)
                                                {
                                                    HttpContext.Current.Cache.Remove("ChallanComplianceMappingData");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            cvUploadAssignment.IsValid = false;
                                            cvUploadAssignment.ErrorMessage = "No sheet exists with name-UploadComplianceAssignment";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                cvUploadAssignment.IsValid = false;
                                cvUploadAssignment.ErrorMessage = "Error uploading file, Please try again";
                            }
                        }
                        else
                        {
                            cvUploadAssignment.IsValid = false;
                            cvUploadAssignment.ErrorMessage = "Select Customer";
                        }
                    }
                    else
                    {
                        cvUploadAssignment.IsValid = false;
                        cvUploadAssignment.ErrorMessage = "Select Customer";
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                    cvUploadAssignment.IsValid = false;
                    cvUploadAssignment.ErrorMessage = "Something went wrong, Please try again";
                }
            }
        }

        private bool ComplianceAssignmentSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadComplianceAssignment"))
                    {
                        if (sheet.Name.Trim().Equals("UploadComplianceAssignment"))
                        {
                            flag = true;
                            break;
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadAssignment.IsValid = false;
                cvUploadAssignment.ErrorMessage = "Something went wrong, Please try again";
            }
            return false;
        }

        private bool ComplianceAssignmentCheckListSheetsExitsts(ExcelPackage xlWorkbook, string data)
        {
            try
            {
                bool flag = false;
                foreach (ExcelWorksheet sheet in xlWorkbook.Workbook.Worksheets)
                {
                    if (data.Equals("UploadComplianceAssignmentCheckList"))
                    {
                        if (sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList") || sheet.Name.Trim().Equals("UploadComplianceAssignmentCheckList"))
                        {
                            flag = true;
                            break;//added by Manisha
                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                return flag;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadAssignment.IsValid = false;
                cvUploadAssignment.ErrorMessage = "Something went wrong, Please try again";
            }
            return false;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                MasterFileUpload = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadAssignment.IsValid = false;
                cvUploadAssignment.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public void ErrorMessages(List<string> emsg)
        {
            string finalErrMsg = string.Empty;

            finalErrMsg += "<ol type='1'>";

            if (emsg.Count > 0)
            {
                emsg.ForEach(eachErrMsg =>
                {
                    finalErrMsg += "<li>" + eachErrMsg + "</li>";
                });

                finalErrMsg += "</ol>";
            }

            cvUploadAssignment.IsValid = false;
            cvUploadAssignment.ErrorMessage = finalErrMsg;
        }

        private bool CheckInt(string val)
        {
            try
            {
                int i = Convert.ToInt32(val);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void ComplianceAssignmentData(ExcelPackage xlWorkbook)
        {
            try
            {
                ExcelWorksheet xlWorksheet = xlWorkbook.Workbook.Worksheets["UploadComplianceAssignment"];
                if (xlWorksheet != null)
                {
                    int count = 1;
                    int xlrow2 = xlWorksheet.Dimension.End.Row;
                    List<TempAssignmentTable> TempassignmentTableList = new List<TempAssignmentTable>();
                    List<TempAssignmentTableCheckList> TempassignmentTableCheckList = new List<TempAssignmentTableCheckList>();
                    List<string> errorMessage = new List<string>();
                    List<TempTable> lstTemptable = new List<TempTable>();

                    #region Validations

                    int valComplianceID = -1;
                    int valPerformerID = -1;
                    int valReviewerID = -1;
                    int valApproverID = -1;
                    int valCustomerBranchID = -1;

                    string valIsCheckList = string.Empty;
                    //int valDepartmentID = -1;
                    string valSequence = string.Empty;

                    int custBranchID = -1;
                    int complianceID = -1;
                    string scopeType = string.Empty;
                    string establishmentType = string.Empty;
                    string branchState = string.Empty;

                    List<string> lstCodes = new List<string>();

                    if (xlrow2 >= 2)
                    {
                        for (int rowNum = 2; rowNum <= xlrow2; rowNum++)
                        {
                            valComplianceID = -1;
                            valPerformerID = -1;
                            valReviewerID = -1;
                            valApproverID = -1;
                            valCustomerBranchID = -1;
                            //valDepartmentID = -1;

                            scopeType = string.Empty;
                            custBranchID = -1;

                            #region 1 ComplianceID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString().Trim()))
                            {
                                if (CheckInt(xlWorksheet.Cells[rowNum, 1].Text.ToString()))
                                {
                                    valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                                }
                                else
                                    errorMessage.Add("ComplianceID-" + xlWorksheet.Cells[rowNum, 1].Text.ToString() + " can be number only, check at row number-" + rowNum);                                                                
                            }
                            if (valComplianceID == -1 || valComplianceID == 0)
                            {
                                errorMessage.Add("Required ComplianceID at row number-" + rowNum);
                            }
                            else
                            {
                                if (ExistsCompliance(valComplianceID) == false)
                                {
                                    errorMessage.Add("ComplianceID-" + valComplianceID + " not defined in the Compliance Master or " + valComplianceID + "-it is not tagged to HR Product, check at row number-" + rowNum);
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                                    {
                                        valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 5].Text);
                                        if (!BranchIDExists(valCustomerBranchID) == false)
                                        {
                                            if (!lstTemptable.Any(x => x.ComplianceId == valComplianceID && x.CustomerBranchID == valCustomerBranchID))
                                            {
                                                TempTable tt = new TempTable();
                                                tt.ComplianceId = valComplianceID;
                                                tt.CustomerBranchID = valCustomerBranchID;
                                                lstTemptable.Add(tt);

                                                scopeType = GetComplianceType(valComplianceID);
                                                complianceID = valComplianceID;
                                            }
                                            else
                                            {
                                                errorMessage.Add("Compliance with this ComplianceID (" + valComplianceID + ") , Exists Multiple Times in the Uploaded Excel Document at Row - " + rowNum + "");
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Check Compliance Schedule               
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 1].Text.ToString()))
                            {
                                if (valComplianceID != 0 || valComplianceID != -1)
                                {
                                    valComplianceID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 1].Text.Trim());
                                    Boolean checkflag = CheckComplianceSchedulePresent(valComplianceID);
                                    if (checkflag == false)
                                    {
                                        errorMessage.Add("No Compliance Schedule Present, Please Correct the Display Schedule Information of ComplianceID-" + valComplianceID + ", check at row number-" + rowNum);
                                    }
                                }
                            }
                            #endregion

                            #region 2 PerformerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                            {
                                if (CheckInt(xlWorksheet.Cells[rowNum, 2].Text.ToString()))
                                {
                                    valPerformerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 2].Text);
                                }
                                else
                                    errorMessage.Add("PerformerID-" + xlWorksheet.Cells[rowNum, 2].Text.ToString() + " can be number only, check at row number-" + rowNum);
                                
                            }
                            if (valPerformerID == -1 || valPerformerID == 0)
                            {
                                errorMessage.Add("Required PerformerID at row number-" + rowNum);
                            }
                            else
                            {
                                if (UserExists(valPerformerID) == false)
                                {
                                    errorMessage.Add("PerformerID-" + valPerformerID + " not defined in the User Master, Check at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 3 ReviewerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 3].Text.ToString()))
                            {
                                if (CheckInt(xlWorksheet.Cells[rowNum, 3].Text.ToString()))
                                {
                                    valReviewerID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 3].Text);
                                }
                                else
                                    errorMessage.Add("ReviewerID-" + xlWorksheet.Cells[rowNum, 3].Text.ToString() + " can be number only, check at row number-" + rowNum);
                            }
                            if (valReviewerID == -1 || valReviewerID == 0)
                            {
                                errorMessage.Add("Required ReviewerID at row number-" + rowNum);
                            }
                            else
                            {
                                if (UserExists(valReviewerID) == false)
                                {
                                    errorMessage.Add("ReviewerID-" + valReviewerID + " not defined in the User Master, Check at row number-" + rowNum);
                                }
                            }
                            #endregion

                            #region 4 ApproverID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 4].Text.ToString()))
                            {
                                if (CheckInt(xlWorksheet.Cells[rowNum, 4].Text.ToString()))
                                {
                                    valApproverID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 4].Text);
                                }
                                else
                                    errorMessage.Add("ApproverID-" + xlWorksheet.Cells[rowNum, 4].Text.ToString() + " can be number only, check at row number-" + rowNum);
                                

                                if (valApproverID != -1 || valApproverID != 0)
                                {
                                    if (UserExists(valApproverID) == false)
                                    {
                                        errorMessage.Add("ApproverID-" + valApproverID + " not defined in the User Master, Check at row number-" + rowNum);
                                    }
                                }
                            }

                            #endregion

                            #region 5 CustomerBranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                            {
                                if (CheckInt(xlWorksheet.Cells[rowNum, 5].Text.ToString()))
                                {
                                    valCustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 5].Text);
                                }
                                else
                                    errorMessage.Add("CustomerBranchID-" + xlWorksheet.Cells[rowNum, 5].Text.ToString() + " can be number only, check at row number-" + rowNum);                                
                            }
                            if (valCustomerBranchID == -1 || valCustomerBranchID == 0)
                            {
                                errorMessage.Add("Required CustomerBranchID at row number-" + rowNum);
                            }
                            else
                            {
                                if (BranchIDExists(valCustomerBranchID) == false)
                                {
                                    errorMessage.Add("CustomerBranchID-" + valCustomerBranchID + " not exists, check at row number-" + rowNum);
                                }
                                else
                                    custBranchID = valCustomerBranchID;
                            }
                            #endregion

                            #region 6 Department
                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[rowNum, 6].Text.ToString()))
                            //{
                            //    if (CheckInt(xlWorksheet.Cells[rowNum, 6].Text.ToString()))
                            //    {
                            //        valDepartmentID = Convert.ToInt32(xlWorksheet.Cells[rowNum, 6].Text);
                            //    }
                            //    else
                            //        errorMessage.Add("DepartmentID-" + xlWorksheet.Cells[rowNum, 6].Text.ToString() + " can be number only, check at row number-" + rowNum);
                            //}
                            //if (valDepartmentID != -1 && valDepartmentID != 0)
                            //{
                            //    if (DepartmentExists(valDepartmentID) == false)
                            //    {
                            //        errorMessage.Add("DepartmentID-" + valDepartmentID + " not Defined in the System at row number-" + rowNum);
                            //    }
                            //}
                            #endregion

                            #region 7 Check Already Activation
                            if (AssignmentExists(valCustomerBranchID, valComplianceID, valSequence))
                            {
                                errorMessage.Add("Compliance already activated, check at row number-" + rowNum);
                            }
                            #endregion

                            #region 8 - Type Wise Compliance Check

                            lstCodes.Clear();

                            if (!string.IsNullOrEmpty(scopeType))
                            {
                                if (complianceID != -1 && custBranchID != -1 && custBranchID != 0)
                                {
                                    #region Register

                                    if (scopeType.Trim().ToUpper().Equals("SOW03"))
                                    {
                                        var custBranchDetails = GetEntityBranchDetails(custBranchID, "B");

                                        if (custBranchDetails != null)
                                        {
                                            establishmentType = string.Empty;
                                            branchState = string.Empty;

                                            if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                                            {
                                                establishmentType = custBranchDetails.CM_EstablishmentType;
                                                branchState = custBranchDetails.CM_State;
                                            }

                                            if (!string.IsNullOrEmpty(establishmentType) && !string.IsNullOrEmpty(branchState))
                                            {
                                                bool canMap = Check_Registers_Compliance_StateWise(complianceID, branchState, establishmentType);

                                                if (!canMap)
                                                    errorMessage.Add("ComplianceID-" + complianceID + " can not be mapped to BranchID-" + custBranchID + ", Please check at row-" + rowNum);
                                            }
                                            else
                                                errorMessage.Add("Please specify Establishment Type and State for  BranchID-" + custBranchID + ", prior to Register Compliance Assignment, Please check at row-" + rowNum);
                                        }
                                        else
                                            errorMessage.Add("Entity-Branch Details not found for BranchID-" + custBranchID + ", or BranchID-" + custBranchID + " should be Branch, Please check at row-" + rowNum);
                                    }

                                    #endregion

                                    #region Return
                                    else if (scopeType.Trim().ToUpper().Equals("SOW05"))
                                    {
                                        var custBranchDetails = GetEntityBranchDetails(custBranchID, "B");

                                        if (custBranchDetails != null)
                                        {
                                            establishmentType = string.Empty;
                                            branchState = string.Empty;

                                            if (!string.IsNullOrEmpty(custBranchDetails.CM_EstablishmentType) && !string.IsNullOrEmpty(custBranchDetails.CM_State))
                                            {
                                                establishmentType = custBranchDetails.CM_EstablishmentType;
                                                branchState = custBranchDetails.CM_State;
                                            }

                                            if (!string.IsNullOrEmpty(branchState))
                                            {
                                                bool canMap = Check_Returns_Compliance_StateWise(complianceID, branchState);

                                                if (!canMap)
                                                    errorMessage.Add("ComplianceID-" + complianceID + " can not be mapped to BranchID-" + custBranchID + ", Please check at row-" + rowNum);
                                            }
                                        }
                                    }

                                    #endregion

                                    #region PF Challan
                                    else if (scopeType.Trim().ToUpper().Equals("SOW13"))
                                    {
                                        var entityDetails = GetEntityBranchDetails(custBranchID, "E");

                                        if (entityDetails != null)
                                        {
                                            string clientID = entityDetails.CM_ClientID;
                                            if (!string.IsNullOrEmpty(clientID))
                                            {
                                                lstCodes = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "EPF");
                                                if (lstCodes.Count == 0)
                                                {
                                                    errorMessage.Add("No PF Code Assigned for Selected Entity/Client-" + custBranchID + ", Please check at row - " + rowNum);
                                                }
                                            }
                                        }
                                        else
                                            errorMessage.Add("Entity/Branch Details not found, or Select Entity for PF Compliance Assignment, Please check at row - " + rowNum);
                                    }

                                    #endregion

                                    #region ESI Challan

                                    else if (scopeType.Trim().ToUpper().Equals("SOW14"))
                                    {
                                        var custBranchDetails = GetEntityBranchDetails(custBranchID, "B");

                                        if (custBranchDetails != null)
                                        {
                                            string clientID = custBranchDetails.CM_ClientID;
                                            if (!string.IsNullOrEmpty(clientID))
                                            {
                                                lstCodes = RLCS_Master_Management.GetPForESIorPT_Codes(clientID, "ESI");

                                                if (lstCodes.Count == 0)
                                                {
                                                    errorMessage.Add("No ESI Code Assigned to Employee, Update ESI Code in Employee Master Before ESI Compliance Assignment, Please check at row - " + rowNum);
                                                }
                                            }
                                        }
                                    }

                                    #endregion

                                    #region PT Challan
                                    else if (scopeType.Trim().ToUpper().Equals("SOW17"))
                                    {
                                        var custBranchDetails = GetEntityBranchDetails(custBranchID, "B");

                                        if (custBranchDetails != null)
                                        {
                                            string clientID = custBranchDetails.CM_ClientID;
                                            if (!string.IsNullOrEmpty(clientID))
                                            {
                                                lstCodes = RLCS_Master_Management.GetPTStates(custBranchID, clientID, "PT");

                                                if (lstCodes.Count == 0)
                                                {
                                                    errorMessage.Add("No PT State Assigned to Employee, Update PT State in Employee Master Before ESI Compliance Assignment, Please check at row - " + rowNum);
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                    errorMessage.Add("Customer Branch Detail for-" + custBranchID + " not found, Check in Entity/Branch Master, Please check at row-" + rowNum);

                            }
                            #endregion
                        }
                    }
                    else
                    {
                        errorMessage.Add("No Data available to upload, Please check excel document carefully and try again");
                    }
                    #endregion

                    if (errorMessage.Count > 0)
                    {
                        ErrorMessages(errorMessage);
                    }
                    else
                    {
                        #region Save
                        for (int i = 2; i <= xlrow2; i++)
                        {
                            count = count + 1;

                            int ComplianceID = -1;
                            int PerformerID = -1;
                            int ReviewerID = -1;
                            int ApproverID = -1;
                            int CustomerBranchID = -1;
                            //int DepartmentID = -1;

                            #region ComplianceID                     
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 1].Text.ToString()))
                            {
                                ComplianceID = Convert.ToInt32(xlWorksheet.Cells[i, 1].Text.Trim());
                            }
                            #endregion

                            #region PerformerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 2].Text.ToString()))
                            {
                                PerformerID = Convert.ToInt32(xlWorksheet.Cells[i, 2].Text);
                            }
                            #endregion

                            #region ReviewerID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 3].Text.ToString()))
                            {
                                ReviewerID = Convert.ToInt32(xlWorksheet.Cells[i, 3].Text);
                            }
                            #endregion

                            #region ApproverID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 4].Text.ToString()))
                            {
                                ApproverID = Convert.ToInt32(xlWorksheet.Cells[i, 4].Text);
                            }
                            #endregion

                            #region CustomerBranchID
                            if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 5].Text.ToString()))
                            {
                                CustomerBranchID = Convert.ToInt32(xlWorksheet.Cells[i, 5].Text);
                            }
                            #endregion

                            #region Department
                            //if (!String.IsNullOrEmpty(xlWorksheet.Cells[i, 6].Text.ToString()))
                            //{
                            //    DepartmentID = Convert.ToInt32(xlWorksheet.Cells[i, 6].Text);
                            //}
                            #endregion

                            if (Business.ComplianceManagement.Exists(ComplianceID, CustomerBranchID, null))
                            {
                                TempAssignmentTable TempAssP = new TempAssignmentTable();
                                TempAssP.ComplianceId = ComplianceID;
                                TempAssP.CustomerBranchID = CustomerBranchID;
                                TempAssP.RoleID = 3;
                                TempAssP.UserID = PerformerID;
                                TempAssP.IsActive = true;
                                TempAssP.CreatedOn = DateTime.Now;
                                //TempAssP.DepartmentID = DepartmentID;
                                //if (DepartmentID == -1 || DepartmentID == 0)
                                //{
                                //    TempAssP.DepartmentID = null;
                                //}
                                //else
                                //{
                                //    TempAssP.DepartmentID = DepartmentID;
                                //}
                                TempassignmentTableList.Add(TempAssP);

                                TempAssignmentTable TempAssR = new TempAssignmentTable();
                                TempAssR.ComplianceId = ComplianceID;
                                TempAssR.CustomerBranchID = CustomerBranchID;
                                TempAssR.RoleID = 4;
                                TempAssR.UserID = ReviewerID;
                                TempAssR.IsActive = true;
                                TempAssR.CreatedOn = DateTime.Now;
                                //if (DepartmentID == -1 || DepartmentID == 0)
                                //{
                                //    TempAssR.DepartmentID = null;
                                //}
                                //else
                                //{
                                //    TempAssR.DepartmentID = DepartmentID;
                                //}
                                TempassignmentTableList.Add(TempAssR);
                                if (ApproverID != -1)
                                {
                                    TempAssignmentTable TempAssA = new TempAssignmentTable();
                                    TempAssA.ComplianceId = ComplianceID;
                                    TempAssA.CustomerBranchID = CustomerBranchID;
                                    TempAssA.RoleID = 6;
                                    TempAssA.UserID = ApproverID;
                                    TempAssA.IsActive = true;
                                    TempAssA.CreatedOn = DateTime.Now;
                                    //if (DepartmentID == -1 || DepartmentID == 0)
                                    //{
                                    //    TempAssA.DepartmentID = null;
                                    //}
                                    //else
                                    //{
                                    //    TempAssA.DepartmentID = DepartmentID;
                                    //}
                                    TempassignmentTableList.Add(TempAssA);
                                }
                            }//exists end

                        }
                        #endregion

                        if (TempassignmentTableList.Count > 0)
                        {
                            suucess = CreateExcelTempAssignmentTable(TempassignmentTableList);
                        }
                        if (suucess)
                        {
                            if (TempassignmentTableCheckList.Count > 0)
                            {
                                suucess = CreateExcelTempAssignmentTableCheckList(TempassignmentTableCheckList);
                            }
                        }
                        else if (suucess == false && TempassignmentTableCheckList.Count > 0)
                        {
                            suucess = CreateExcelTempAssignmentTableCheckList(TempassignmentTableCheckList);
                        }
                        else
                        {
                            suucess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                suucess = false;
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvUploadAssignment.IsValid = false;
                cvUploadAssignment.ErrorMessage = "Something went wrong, Please try again";
            }
        }

        public static void GetDepartment(int customerID)
        {
            List<ComplianceManagement.Business.Data.Department> Records = new List<ComplianceManagement.Business.Data.Department>();

            var DepartmentList = HttpContext.Current.Cache["DepartmentListData"];

            if (DepartmentList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.Departments
                               where row.IsDeleted == false
                               && row.CustomerID == customerID
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("DepartmentListData", Records); // add it to cache
                }
            }
        }

        public bool DepartmentExists(int deptid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.Department>)Cache["DepartmentListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == deptid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public static void GetCompliance()
        {
            List<SP_RLCS_RegisterReturnChallanCompliance_Result> Records = new List<SP_RLCS_RegisterReturnChallanCompliance_Result>();

            var ComplianceList = HttpContext.Current.Cache["ComplianceListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.SP_RLCS_RegisterReturnChallanCompliance()
                               where row.IsDeleted == false
                               && row.EventFlag == null
                               && row.Status == null
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("ComplianceListData", Records); // add it to cache
                }
            }
        }

        public static void GetRegisterComplianceMapping()
        {
            List<RLCS_Register_Compliance_Mapping> lstRegisters = new List<RLCS_Register_Compliance_Mapping>();

            var lstRecords = HttpContext.Current.Cache["RegisterComplianceMappingData"];

            if (lstRecords == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstRegisters = (from row in entities.RLCS_Register_Compliance_Mapping
                                    where row.AVACOM_ComplianceID != null
                                    && row.RegisterID != null
                                    && row.Register_Status == "A"
                                    select row).ToList();

                    HttpContext.Current.Cache.Insert("RegisterComplianceMappingData", lstRegisters); // add it to cache
                }
            }
        }

        public static void GetReturnComplianceMapping()
        {
            List<RLCS_Returns_Compliance_Mapping> lstReturns = new List<RLCS_Returns_Compliance_Mapping>();

            var lstRecords = HttpContext.Current.Cache["ReturnComplianceMappingData"];

            if (lstRecords == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstReturns = (from row in entities.RLCS_Returns_Compliance_Mapping
                                  where
                                  //&&  row.RM_StateID.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())   &&                                 
                                  row.AVACOM_ComplianceID != null
                                  && row.ReturnID != null
                                  && row.RM_Status == "A"
                                  select row).ToList();

                    HttpContext.Current.Cache.Insert("ReturnComplianceMappingData", lstReturns); // add it to cache
                }
            }
        }

        public static void GetChallanComplianceMapping()
        {
            List<RLCS_Returns_Compliance_Mapping> lstReturns = new List<RLCS_Returns_Compliance_Mapping>();

            var lstRecords = HttpContext.Current.Cache["ChallanComplianceMappingData"];

            if (lstRecords == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    lstRecords = (from row in entities.RLCS_Challan_Compliance_Mapping
                                  where row.AVACOM_ComplianceID != null
                                  && row.IsActive == true
                                  && row.AVACOM_ComplianceID != null
                                  select row).ToList();

                    HttpContext.Current.Cache.Insert("ChallanComplianceMappingData", lstRecords); // add it to cache
                }
            }
        }

        public void GetComplianceInstanceAssignment(int CustomerID)
        {
            List<ComplianceManagement.Business.Data.SP_CheckSequence_Result> Records = new List<ComplianceManagement.Business.Data.SP_CheckSequence_Result>();

            var ComplianceList = HttpContext.Current.Cache["ComplianceInstanceAssignmentListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    Records = (from row in entities.SP_CheckSequence(CustomerID, "S")
                               select row).ToList();

                    HttpContext.Current.Cache.Insert("ComplianceInstanceAssignmentListData", Records); // add it to cache
                }
            }
        }

        public static bool CheckComplianceSchedulePresent(long ComplianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                Boolean flag = true;
                var Frequency = (from row in entities.Compliances
                                 where row.ID == ComplianceID
                                 select row.Frequency).FirstOrDefault();

                var RecordCount = (from row in entities.ComplianceSchedules
                                   where row.ComplianceID == ComplianceID
                                   select row).ToList().Count();

                if (Frequency != null)
                {
                    //int frequencyId = Enumerations.GetEnumByName<Frequency>(Convert.ToString(Frequency));

                    int frequencyId = Convert.ToInt32(Frequency);

                    if (frequencyId == 0)  //Monthly
                    {
                        if (RecordCount != 12)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 1)  //Quarterly
                    {
                        if (RecordCount != 4)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 2)  //HalfYearly
                    {
                        if (RecordCount != 2)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 3)  //Annual
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 4)  //FourMonthly
                    {
                        if (RecordCount != 3)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 5)  //TwoYearly
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 6)  //SevenYearly
                    {
                        if (RecordCount != 1)
                        {
                            flag = false;
                        }
                    }
                    if (frequencyId == 7)  //Daily
                    {
                        flag = true;
                    }
                    if (frequencyId == 8)  //Weekly
                    {
                        flag = true;
                    }
                }
                else
                {
                    flag = true;
                }
                return flag;
            }
        }

        public void GetUser()
        {
            int customerID = -1;
            int serviceProviderID = -1;
            int distributorID = -1;

            if (AuthenticationHelper.Role == "CADMN")
            {
                //customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;
                customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else if (AuthenticationHelper.Role == "SPADM")
            {
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                distributorID = RLCS_Master_Management.Get_DistributorID(customerID);
                //serviceProviderID = Convert.ToInt32(AuthenticationHelper.CustomerID);                    
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
                distributorID = Convert.ToInt32(AuthenticationHelper.CustomerID);
            }
            else
            {
                if (!string.IsNullOrEmpty(ddlCustomerList.SelectedValue))
                {
                    customerID = Convert.ToInt32(ddlCustomerList.SelectedValue);
                }
            }

            var ComplianceList = HttpContext.Current.Cache["UserListData"];

            if (ComplianceList == null)
            {
                if (customerID != -1)
                {
                    var users = UserCustomerMappingManagement.GetAll_Users(customerID, serviceProviderID, distributorID);
                    HttpContext.Current.Cache.Insert("UserListData", users); // add it to cache
                }
            }
        }

        public void GetCustomerBranch(int customerID)
        {
            List<RLCS_CustomerBranchView> customerBranches = new List<RLCS_CustomerBranchView>();

            var ComplianceList = HttpContext.Current.Cache["CustomerBranchListData"];

            if (ComplianceList == null)
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    customerBranches = (from row in entities.RLCS_CustomerBranchView
                                        where row.IsDeleted == false
                                        && row.CustomerID == customerID
                                        select row).ToList();

                    HttpContext.Current.Cache.Insert("CustomerBranchListData", customerBranches); // add it to cache
                }
            }
        }

        public bool CreateExcelTempAssignmentTableCheckList(List<TempAssignmentTableCheckList> TempsssignmentTableCheckList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempsssignmentTableCheckList.ForEach(entry =>
                    {
                        entities.TempAssignmentTableCheckLists.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool CreateExcelTempAssignmentTable(List<TempAssignmentTable> TempassignmentTable)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    TempassignmentTable.ForEach(entry =>
                    {
                        entities.TempAssignmentTables.Add(entry);
                    });
                    entities.SaveChanges();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ExistsCompliance(int complianceID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<SP_RLCS_RegisterReturnChallanCompliance_Result>)Cache["ComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == complianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public string GetComplianceType(int complianceID) //SOW03-Register, SOW05-Return, SOW13-  ,SOW14- ,SOW17
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<SP_RLCS_RegisterReturnChallanCompliance_Result>)Cache["ComplianceListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == complianceID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return query.ScopeID;
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public bool UserExists(int userID)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<SP_RLCS_GetAllUser_ServiceProviderDistributor_Result>)Cache["UserListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == userID
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool BranchIDExists(int Branchid)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<RLCS_CustomerBranchView>)Cache["CustomerBranchListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.ID == Branchid
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool AssignmentExists(long Branchid, long ComplianceiD, string Sequence)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var Records = (List<ComplianceManagement.Business.Data.SP_CheckSequence_Result>)Cache["ComplianceInstanceAssignmentListData"];
                if (Records.Count > 0)
                {
                    var query = (from row in Records
                                 where row.CustomerBranchID == Branchid
                                 && row.ComplianceId == ComplianceiD
                                 && row.SequenceID == Sequence
                                 select row).FirstOrDefault();
                    if (query != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public bool Check_Registers_Compliance_StateWise(int complianceID, string stateName, string establishmentType)
        {
            bool canMap = false;
            var lstRegisters = (List<RLCS_Register_Compliance_Mapping>)Cache["RegisterComplianceMappingData"];
            if (lstRegisters.Count > 0)
            {
                if (!string.IsNullOrEmpty(stateName))
                {
                    lstRegisters = lstRegisters.Where(row => row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper())).ToList();
                }

                if (!string.IsNullOrEmpty(establishmentType))
                {
                    if (establishmentType.Equals("FACT"))
                    {
                        lstRegisters = (from row in lstRegisters
                                        where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("FACT"))
                                        && row.Act_Type != null
                                        && row.Register_Status == "A"
                                        && row.AVACOM_ComplianceID != null
                                        select row).ToList();
                    }
                    else if (establishmentType.Equals("SEA"))
                    {
                        lstRegisters = (from row in lstRegisters
                                        where (row.Act_Type.Trim().ToUpper().Equals("SF") || row.Act_Type.Trim().ToUpper().Equals("SEA"))
                                        && row.Act_Type != null
                                        && row.Register_Status == "A"
                                        && row.AVACOM_ComplianceID != null
                                        select row).ToList();
                    }
                    else if (establishmentType.Equals("SF"))
                    {
                        lstRegisters = (from row in lstRegisters
                                        where row.Register_Status == "A"
                                        && row.AVACOM_ComplianceID != null
                                        select row).ToList();
                    }
                }

                int count = 0;
                if (lstRegisters.Count > 0)
                    count = lstRegisters.Where(row => row.AVACOM_ComplianceID == complianceID).Count();

                if (count > 0)
                    canMap = true;
                else
                    canMap = false;

            }

            return canMap;
        }

        public bool Check_Returns_Compliance_StateWise(int complianceID, string stateName)
        {
            bool canMap = false;
            int count = 0;

            var lstReturns = (List<RLCS_Register_Compliance_Mapping>)Cache["ReturnComplianceMappingData"];

            if (lstReturns.Count > 0)
            {
                if (!string.IsNullOrEmpty(stateName))
                {
                    lstReturns = lstReturns.Where(row => row.StateID.Trim().ToUpper().Equals(stateName.Trim().ToUpper()) || row.StateID == "Central").ToList();

                    if (lstReturns.Count > 0)
                        count = lstReturns.Where(row => row.AVACOM_ComplianceID == complianceID).Count();

                    if (count > 0)
                        canMap = true;
                    else
                        canMap = false;
                }
            }

            return canMap;
        }

        public bool Check_ChallanRelatedCompliance(int complianceID, string stateCode, string challanType)
        {
            bool canMap = false;
            int count = 0;

            var lstChallans = (List<RLCS_Challan_Compliance_Mapping>)Cache["ChallanComplianceMappingData"];

            if (lstChallans.Count > 0)
            {
                if (!string.IsNullOrEmpty(stateCode))
                    lstChallans = lstChallans.Where(row => row.State_Code.Trim().ToUpper().Equals(stateCode.Trim().ToUpper())).ToList();

                if (!string.IsNullOrEmpty(challanType))
                    lstChallans = lstChallans.Where(row => row.ChallanType.Trim().ToUpper().Equals(challanType.Trim().ToUpper())).ToList();

                if (lstChallans.Count > 0)
                    count = lstChallans.Where(row => row.AVACOM_ComplianceID == complianceID).Count();

                if (count > 0)
                    canMap = true;
                else
                    canMap = false;
            }

            return canMap;
        }


        public RLCS_CustomerBranchView GetEntityBranchDetails(int custBranchID, string branchType)
        {
            try
            {
                RLCS_CustomerBranchView branchRecord = new RLCS_CustomerBranchView();
                var customerBranches = (List<RLCS_CustomerBranchView>)Cache["CustomerBranchListData"];
                if (customerBranches.Count > 0)
                {
                    branchRecord = (from row in customerBranches
                                    where row.ID == custBranchID
                                    && row.BranchType.Trim().ToUpper().Equals(branchType)
                                    select row).FirstOrDefault();
                }

                return branchRecord;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        protected void lbtnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                using (ExcelPackage exportPackge = new ExcelPackage())
                {
                    #region Sheet1 - UploadComplianceAssignment
                    ExcelWorksheet exWorkSheet1 = exportPackge.Workbook.Worksheets.Add("UploadComplianceAssignment");

                    exWorkSheet1.Cells["A1"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["A1"].Value = "ComplianceID";
                    exWorkSheet1.Cells["A1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["A1"].Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet1.Cells["B1"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["B1"].Value = "Performer";
                    exWorkSheet1.Cells["B1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["B1"].Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet1.Cells["C1"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["C1"].Value = "Reviewer";
                    exWorkSheet1.Cells["C1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["C1"].Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet1.Cells["D1"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["D1"].Value = "Approver";

                    exWorkSheet1.Cells["E1"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["E1"].Value = "BranchID";
                    exWorkSheet1.Cells["E1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    exWorkSheet1.Cells["E1"].Style.Fill.BackgroundColor.SetColor(Color.Red);

                    exWorkSheet1.Cells["F1"].Style.Font.Bold = true;
                    exWorkSheet1.Cells["F1"].Value = "Dept_ID";

                    using (ExcelRange col = exWorkSheet1.Cells[1, 1, 1, 6])
                    {
                        col.Style.WrapText = true;
                        col.Style.Numberformat.Format = "dd/MMM/yyyy";
                        col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        col.Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        //col.AutoFitColumns();

                        // Assign borders
                        col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }

                    #endregion

                    #region Sheet2 - List of HR Compliances

                    var masterRecords = (List<SP_RLCS_RegisterReturnChallanCompliance_Result>)Cache["ComplianceListData"];

                    if (masterRecords == null)
                        GetCompliance();

                    if (masterRecords.Count > 0)
                    {
                        ExcelWorksheet exWorkSheet2 = exportPackge.Workbook.Worksheets.Add("HRComplianceList");
                        DataTable ExcelData = null;
                        DataView view = new System.Data.DataView(masterRecords.ToDataTable());
                        ExcelData = view.ToTable("Selected", false, "ScopeName", "ActID", "ActName", "SM_Name", "ID", "ShortForm", "RequiredForms", "AVACOM_Frequency");

                        exWorkSheet2.Cells["A1"].Value = "Type";
                        exWorkSheet2.Cells["A1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["A1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["A1"].AutoFitColumns(10);

                        exWorkSheet2.Cells["B1"].Value = "ActID";
                        exWorkSheet2.Cells["B1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["B1"].Style.Font.Bold = true;

                        exWorkSheet2.Cells["B1"].AutoFitColumns(10);

                        exWorkSheet2.Cells["C1"].Value = "Act";
                        exWorkSheet2.Cells["C1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["C1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["C1"].AutoFitColumns(50);

                        exWorkSheet2.Cells["D1"].Value = "State";
                        exWorkSheet2.Cells["D1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["D1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["D1"].AutoFitColumns(25);

                        exWorkSheet2.Cells["E1"].Value = "ComplianceID";
                        exWorkSheet2.Cells["E1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["E1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["E1"].AutoFitColumns(10);

                        exWorkSheet2.Cells["F1"].Value = "Compliance";
                        exWorkSheet2.Cells["F1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["F1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["F1"].AutoFitColumns(50);

                        exWorkSheet2.Cells["G1"].Value = "Form";
                        exWorkSheet2.Cells["G1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["G1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["G1"].AutoFitColumns(25);

                        exWorkSheet2.Cells["H1"].Value = "Frequency";
                        exWorkSheet2.Cells["H1"].Style.Font.Size = 12;
                        exWorkSheet2.Cells["H1"].Style.Font.Bold = true;
                        exWorkSheet2.Cells["H1"].AutoFitColumns(25);

                        exWorkSheet2.Cells["A2"].LoadFromDataTable(ExcelData, false);

                        using (ExcelRange col = exWorkSheet2.Cells[2, 1, 2 + ExcelData.Rows.Count, 8])
                        {
                            col.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                            col.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //col.AutoFitColumns();

                            // Assign borders
                            col.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                            col.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        }
                    }

                    #endregion

                    Byte[] fileBytes = exportPackge.GetAsByteArray();
                    Response.ClearContent();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + "UploadHRComplianceAssignment.xlsx");
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.ms-excel";
                    StringWriter sw = new StringWriter();
                    Response.BinaryWrite(fileBytes);
                    HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                    HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                    HttpContext.Current.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.                    
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}