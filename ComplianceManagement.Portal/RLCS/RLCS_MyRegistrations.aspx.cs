﻿using com.VirtuosoITech.ComplianceManagement.Business.Contract;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Configuration;
using System.Reflection;
using System.Web.UI;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyRegistrations : System.Web.UI.Page
    {
        public static string RLCSDocPath = "";
        public static string msg;
        protected static int CustId;
        protected static int UserId;
        protected static string Path;
        protected static string ProfileID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
                UserId = Convert.ToInt32(AuthenticationHelper.UserID);
                ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);

                if (!string.IsNullOrEmpty(Request.QueryString["RLCSRecordID"]) && !string.IsNullOrEmpty(Request.QueryString["RLCSDockType"]))
                {                   
                    string[] RecordID = Request.QueryString["RLCSRecordID"].ToString().Split(',');
                    //long RecordID = Convert.ToInt64(Request.QueryString["RLCSRecordID"].ToString());
                    string DocType = Request.QueryString["RLCSDockType"].ToString();
                    string Event = Request.QueryString["Event"].ToString();

                    if (Event == "Download")
                    {
                        bool _objRegval = RLCS_DocumentDownload(RecordID, DocType);
                        //bool _objRegval = RLCS_DocumentDownload(RecordID, DocType);
                        if (!_objRegval)
                        {
                            //divErrormsgReg.Visible = true;
                            //lblerroemsg.Text = "No Document To Download";
                            // msg = "No Document To Download";
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document Available to Download')", true);
                        }
                        else
                            lblerroemsg.Text = "";
                    }
                }
            }
        }

        private bool RLCS_DocumentDownload(string[] recordID, string docType)// private bool RLCS_DocumentDownload(long recordID, string docType)
        {
            try
            {
                string fileName = string.Empty;

                if (!string.IsNullOrEmpty(docType))
                {
                    if (docType.Trim().Equals("SOW03"))
                        fileName = "Register";
                    else if (docType.Trim().Equals("SOW04"))
                        fileName = "Challan";
                    else if (docType.Trim().Equals("SOW05"))
                        fileName = "Return";
                    else if (docType.Trim().Equals("REN"))
                        fileName = "Registration";
                    else if (docType.Trim().Equals("REG"))
                        fileName = "Registration";
                }

                if (recordID.Length > 0 && docType != null)
                {
                    bool getDocumentDownloadval = RLCS_DocumentManagement.GetRLCSDocuments_Historical_Multiple(recordID, docType, fileName);
                    if (!getDocumentDownloadval)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }

            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
    }
}
