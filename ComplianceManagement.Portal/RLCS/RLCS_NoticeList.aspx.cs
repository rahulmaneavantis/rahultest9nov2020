﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_NoticeList : System.Web.UI.Page
    {
        private long CustomerID = AuthenticationHelper.CustomerID;
        protected bool flag;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

                    flag = false;

                    BindParty();
                    BindCaseCategoryType();

                    //Bind Tree Views
                    //var branchList = CustomerBranchManagement.GetAllHierarchy(customerID);
                    //bindOwner();
                    //BindCustomerBranches(tvFilterLocation, tbxFilterLocation, branchList);

                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                                BindLocationFilter(assignedbranchIDs);

                            }
                        }
                    }

                    BindDepartment();

                    if (!String.IsNullOrEmpty(Request.QueryString["Status"]))
                    {
                        if (Request.QueryString["Status"] == "Open")
                        {
                            ViewState["Status"] = 1;

                            if (ddlStatus.Items.FindByValue("1") != null)
                            {
                                ddlStatus.ClearSelection();
                                ddlStatus.Items.FindByValue("1").Selected = true;
                            }
                        }

                        else if (Request.QueryString["Status"] == "Closed")
                        {
                            ViewState["Status"] = 3;

                            if (ddlStatus.Items.FindByValue("3") != null)
                            {
                                ddlStatus.ClearSelection();
                                ddlStatus.Items.FindByValue("3").Selected = true;
                            }
                        }
                    }

                    BindGrid(); bindPageNumber();
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void bindOwner()
        {
            if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
            {
                int noticeStatus = Convert.ToInt32(ddlStatus.SelectedValue);

                var Ownerlist = LitigationUserManagement.GetOwnerListforNotice(CustomerID, noticeStatus);
                ddlOwnerlist.DataValueField = "ID";
                ddlOwnerlist.DataTextField = "Name";
                ddlOwnerlist.DataSource = Ownerlist;
                ddlOwnerlist.DataBind();
                ddlOwnerlist.Items.Insert(0, new ListItem("All", "-1"));
            }
        }

        private void BindCaseCategoryType()
        {
            try
            {
                var lstCaseCaseType = LitigationCourtAndCaseType.GetAllLegalCaseTypeData(CustomerID);

                ddlType.DataTextField = "CaseType";
                ddlType.DataValueField = "ID";

                ddlType.DataSource = lstCaseCaseType;
                ddlType.DataBind();
                ddlType.Items.Insert(0, new ListItem("All", "-1"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //private void BindCustomerBranches(TreeView treetoBind, TextBox treeTxtBox, List<NameValueHierarchy> branchList)
        //{
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            treetoBind.Nodes.Clear();
        //            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //            NameValueHierarchy branch = null;

        //            if (branchList.Count > 0)
        //            {
        //                branch = branchList[0];
        //            }
        //            var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID)).ToList();
        //            treeTxtBox.Text = "Select Entity/Branch/Location";

        //            if (userAssignedBranchList != null)
        //            {
        //                if (userAssignedBranchList.Count > 0)
        //                {
        //                    List<int> assignedbranchIDs = new List<int>();
        //                    assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
        //                    List<TreeNode> nodes = new List<TreeNode>();

        //                    BindLocationFilter(assignedbranchIDs);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvErrorNoticePage.IsValid = false;
        //        cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
        //    }
        //}

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation.Nodes.Clear();

                    string isstatutoryinternal = "";
                    //if (ddlStatus.SelectedValue == "Statutory")
                    //{
                    if (ddlStatus.SelectedValue == "0")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ddlStatus.SelectedValue == "1")
                    {
                        isstatutoryinternal = "I";
                    }

                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                    upDivLocation_Load(null, null);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        //private void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<TreeNode> nodes)
        //{
        //    try
        //    {
        //        if (nvp != null)
        //        {
        //            foreach (var item in nvp.Children)
        //            {
        //                TreeNode node = new TreeNode(item.Name, item.ID.ToString());
        //                BindBranchesHierarchy(node, item, nodes);
        //                if (parent == null)
        //                {
        //                    nodes.Add(node);
        //                }
        //                else
        //                {
        //                    parent.ChildNodes.Add(node);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
        //        cvErrorNoticePage.IsValid = false;
        //        cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
        //    }
        //}

        public void BindDepartment()
        {
            int customerID = -1;
            customerID = Convert.ToInt32(com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID);

            var obj = CompDeptManagement.GetAllDepartmentMasterList(customerID);

            //Page DropDown

            ddlDeptPage.DataTextField = "Name";
            ddlDeptPage.DataValueField = "ID";

            ddlDeptPage.DataSource = obj;
            ddlDeptPage.DataBind();

            ddlDeptPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindParty()
        {
            var obj = LitigationLaw.GetLCPartyDetails(CustomerID);

            //Drop-Down at Page
            ddlPartyPage.DataTextField = "Name";
            ddlPartyPage.DataValueField = "ID";

            ddlPartyPage.DataSource = obj;
            ddlPartyPage.DataBind();

            ddlPartyPage.Items.Insert(0, new ListItem("All", "-1"));
        }

        public void BindGrid()
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                string noticeStatus = string.Empty;
                string noticeType = string.Empty;
                int CategoryType = -1;
                int OwnerID = -1;

                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    CategoryType = Convert.ToInt32(ddlType.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    noticeStatus = ddlStatus.SelectedValue;
                }
                //if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                //{
                //    noticeStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                //}
                if (!string.IsNullOrEmpty(ddlOwnerlist.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(ddlOwnerlist.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                {
                    noticeType = ddlNoticeTypePage.SelectedValue;
                }

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                {
                    partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                //var lstNoticeDetails = RLCSManagement.GetAssignedNoticeList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, noticeStatus, noticeType);
                var lstNoticeDetails = RLCSManagement.GetInspectionNoticeList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID,  branchList, noticeStatus, noticeType);

                if (lstNoticeDetails.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                                if (assignedbranchIDs.Count > 0)
                                    lstNoticeDetails = lstNoticeDetails.Where(Entry => assignedbranchIDs.Contains(Entry.CustomerBranchID)).ToList();

                            }
                        }
                    }
                }

                if (CategoryType != -1)
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeCategoryID == CategoryType).ToList();
                }
                //if (OwnerID != -1)
                //{
                //    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.OwnerID == OwnerID).ToList();
                //}
                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.RefNo.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                string CheckDirection = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["Direction"])))
                    {
                        CheckDirection = Convert.ToString(ViewState["Direction"]);

                        SortExpr = Convert.ToString(ViewState["SortExpression"]);
                        if (CheckDirection == "Ascending")
                        {
                            lstNoticeDetails = lstNoticeDetails.OrderBy(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                        else
                        {
                            CheckDirection = "Descending";
                            lstNoticeDetails = lstNoticeDetails.OrderByDescending(entry => entry.GetType().GetProperty(SortExpr).GetValue(entry, null)).ToList();
                        }
                    }
                }

                flag = true;
                Session["TotalRows"] = null;
                if (lstNoticeDetails.Count > 0)
                {
                    grdNoticeDetails.DataSource = lstNoticeDetails;
                    grdNoticeDetails.DataBind();
                    Session["TotalRows"] = lstNoticeDetails.Count;
                }
                else
                {
                    grdNoticeDetails.DataSource = lstNoticeDetails;
                    grdNoticeDetails.DataBind();
                }

                lstNoticeDetails.Clear();
                lstNoticeDetails = null;
                upDivLocation_Load(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                if (btn != null)
                {
                    int noticeInstanceID = Convert.ToInt32(btn.CommandArgument);

                    if (noticeInstanceID != 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + noticeInstanceID + ");", true);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                grdNoticeDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

                BindGrid();

                int count = Convert.ToInt32(GetTotalPagesCount());
                if (count > 0)
                {
                    int gridindex = grdNoticeDetails.PageIndex;
                    string chkcindition = (gridindex + 1).ToString();
                    DropDownListPageNo.SelectedValue = (chkcindition).ToString();
                }
                bindPageNumber();
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnApplyFilter_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkBtnBindGrid_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
                bindPageNumber();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnAddNotice_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + 0 + ");", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void grdNoticeDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("DELETE_Notice"))
                {
                    int noticeInstanceID = Convert.ToInt32(e.CommandArgument);
                    int userID = AuthenticationHelper.UserID;
                    //NoticeManagement.DeleteNoticeByID(noticeInstanceID);
                    bool saveSuccess = NoticeManagement.DeleteNoticeByID(noticeInstanceID, userID);

                    if (saveSuccess)
                    {
                        BindGrid();
                        bindPageNumber();
                        LitigationManagement.CreateAuditLog("N", noticeInstanceID, "tbl_LegalNoticeInstance", "Delete", Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "Notice Deleted", true);

                        cvErrorNoticePage.IsValid = false;
                        cvErrorNoticePage.ErrorMessage = "Notice Deleted Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvErrorNoticePage.IsValid = false;
                cvErrorNoticePage.ErrorMessage = "Server Error Occurred. Please try again.";
            }
        }

        #region Page Number-Bottom

        protected void DropDownListPageNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            int chkSelectedPage = Convert.ToInt32(DropDownListPageNo.SelectedItem.ToString());
            grdNoticeDetails.PageIndex = chkSelectedPage - 1;

            grdNoticeDetails.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);

            BindGrid(); ShowGridDetail();
        }

        private void bindPageNumber()
        {
            try
            {
                int count = Convert.ToInt32(GetTotalPagesCount());

                if (DropDownListPageNo.Items.Count > 0)
                {
                    DropDownListPageNo.Items.Clear();
                    DropDownListPageNo.SelectedValue = null;
                }

                DropDownListPageNo.DataTextField = "ID";
                DropDownListPageNo.DataValueField = "ID";

                DropDownListPageNo.DataBind();
                for (int i = 1; i <= count; i++)
                {
                    string chkPageID = i.ToString();
                    DropDownListPageNo.Items.Add(chkPageID);
                }
                if (count > 0)
                {
                    DropDownListPageNo.SelectedValue = ("1").ToString();
                }
                else if (count == 0)
                {
                    DropDownListPageNo.Items.Add("0");
                    DropDownListPageNo.SelectedValue = ("0").ToString();
                }
                ShowGridDetail();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ShowGridDetail()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])) && Convert.ToString(Session["TotalRows"]) != "0")
            {
                var PageSize = Convert.ToInt32(ddlPageSize.SelectedItem.Text);
                var PageNumber = Convert.ToInt32(DropDownListPageNo.SelectedValue);
                var EndRecord = 0;
                var TotalRecord = 0;
                var TotalValue = PageSize * PageNumber;

                TotalRecord = Convert.ToInt32(Session["TotalRows"]);
                if (TotalRecord < TotalValue)
                {
                    EndRecord = TotalRecord;
                }
                else
                {
                    EndRecord = TotalValue;
                }
                lblStartRecord.Text = Convert.ToString(PageSize * PageNumber - PageSize + 1);
                lblEndRecord.Text = Convert.ToString(EndRecord) + " ";
                lblTotalRecord.Text = TotalRecord.ToString();
            }
            else
            {
                lblStartRecord.Text = "0 ";
                lblEndRecord.Text = "0 ";
                lblTotalRecord.Text = "0";
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = "0";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["TotalRows"])))
                {
                    TotalRows.Value = Convert.ToString(Session["TotalRows"]);
                }

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion

        public SortDirection direction
        {
            get
            {
                if (ViewState["dirState"] == null)
                {
                    ViewState["dirState"] = SortDirection.Ascending;
                }
                return (SortDirection)ViewState["dirState"];
            }
            set
            {
                ViewState["dirState"] = value;
            }
        }

        protected void grdNoticeDetails_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                long customerID = -1;
                customerID = com.VirtuosoITech.ComplianceManagement.Portal.Common.AuthenticationHelper.CustomerID;
                int branchID = -1;
                int partyID = -1;
                int deptID = -1;
                string noticeStatus = string.Empty;
                string noticeType = string.Empty;
                int CategoryType = -1;
                int OwnerID = -1;

                if (!string.IsNullOrEmpty(ddlType.SelectedValue))
                {
                    CategoryType = Convert.ToInt32(ddlType.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                {
                    noticeStatus = ddlStatus.SelectedValue;
                }
                //if (!string.IsNullOrEmpty(ddlStatus.SelectedValue))
                //{
                //    noticeStatus = Convert.ToInt32(ddlStatus.SelectedValue);
                //}
                if (!string.IsNullOrEmpty(ddlOwnerlist.SelectedValue))
                {
                    OwnerID = Convert.ToInt32(ddlOwnerlist.SelectedValue);
                }
                if (!string.IsNullOrEmpty(ddlNoticeTypePage.SelectedValue))
                {
                    noticeType = ddlNoticeTypePage.SelectedValue;
                }

                if (!string.IsNullOrEmpty(tvFilterLocation.SelectedValue) && tvFilterLocation.SelectedValue != "-1")
                {
                    branchID = Convert.ToInt32(tvFilterLocation.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlPartyPage.SelectedValue))
                {
                    partyID = Convert.ToInt32(ddlPartyPage.SelectedValue);
                }

                if (!string.IsNullOrEmpty(ddlDeptPage.SelectedValue))
                {
                    deptID = Convert.ToInt32(ddlDeptPage.SelectedValue);
                }

                var branchList = LitigationManagement.GetAllHierarchy(Convert.ToInt32(customerID), branchID);

                //var lstNoticeDetails = RLCSManagement.GetAssignedNoticeList(customerID, AuthenticationHelper.UserID, AuthenticationHelper.Role, 3, branchList, partyID, deptID, noticeStatus, noticeType);
                var lstNoticeDetails = RLCSManagement.GetInspectionNoticeList(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, branchList, noticeStatus, noticeType);

                if (lstNoticeDetails.Count > 0)
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(Convert.ToInt32(customerID), AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();

                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();

                                if (assignedbranchIDs.Count > 0)
                                    lstNoticeDetails = lstNoticeDetails.Where(Entry => assignedbranchIDs.Contains(Entry.CustomerBranchID)).ToList();

                            }
                        }
                    }
                }

                if (CategoryType != -1)
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeCategoryID == CategoryType).ToList();
                }
                //if (OwnerID != -1)
                //{
                //    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.OwnerID == OwnerID).ToList();
                //}
                if (!string.IsNullOrEmpty(tbxtypeTofilter.Text))
                {
                    lstNoticeDetails = lstNoticeDetails.Where(entry => entry.NoticeTitle.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim()) || entry.RefNo.ToLower().Contains(((tbxtypeTofilter.Text).ToLower()).Trim())).ToList();
                }

                string SortExpr = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
                {
                    SortExpr = Convert.ToString(ViewState["SortExpression"]);
                    if (SortExpr == e.SortExpression)
                    {
                        if (direction == SortDirection.Ascending)
                        {
                            direction = SortDirection.Descending;
                        }
                        else
                        {
                            direction = SortDirection.Ascending;
                        }
                    }
                    else
                    {
                        direction = SortDirection.Ascending;
                    }
                }

                if (direction == SortDirection.Ascending)
                {
                    ViewState["Direction"] = "Ascending";
                    lstNoticeDetails = lstNoticeDetails.OrderBy(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    //direction = SortDirection.Descending;
                }
                else
                {
                    ViewState["Direction"] = "Descending";
                    lstNoticeDetails = lstNoticeDetails.OrderByDescending(entry => entry.GetType().GetProperty(e.SortExpression).GetValue(entry, null)).ToList();
                    //direction = SortDirection.Ascending;
                }

                foreach (DataControlField field in grdNoticeDetails.Columns)
                {
                    if (field.SortExpression == e.SortExpression)
                    {
                        ViewState["SortIndex"] = grdNoticeDetails.Columns.IndexOf(field);
                    }
                }
                ViewState["SortExpression"] = e.SortExpression;
                Session["TotalRows"] = null;
                flag = true;
                if (lstNoticeDetails.Count > 0)
                {
                    grdNoticeDetails.DataSource = lstNoticeDetails;
                    grdNoticeDetails.DataBind();
                    Session["TotalRows"] = lstNoticeDetails.Count;
                }
                else
                {
                    grdNoticeDetails.DataSource = lstNoticeDetails;
                    grdNoticeDetails.DataBind();
                }

                lstNoticeDetails.Clear();
                lstNoticeDetails = null;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void AddSortImage(int columnIndex, GridViewRow headerRow)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["SortExpression"])))
            {
                System.Web.UI.WebControls.Image sortImage = new System.Web.UI.WebControls.Image();
                sortImage.ImageAlign = ImageAlign.AbsMiddle;

                if (flag == true)
                {
                    if (direction == SortDirection.Ascending)
                    {
                        sortImage.ImageUrl = "../../Images/down_arrow1.png";
                        sortImage.AlternateText = "Ascending Order";
                    }
                    else
                    {
                        sortImage.ImageUrl = "../../Images/up_arrow1.png";
                        sortImage.AlternateText = "Descending Order";
                    }
                    headerRow.Cells[columnIndex].Controls.Add(sortImage);
                }
            }
        }

        protected void grdNoticeDetails_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int sortColumnIndex = Convert.ToInt32(ViewState["SortIndex"]);
                if (sortColumnIndex != -1)
                {
                    AddSortImage(sortColumnIndex, e.Row);
                }
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bindOwner();
        }
    }
}