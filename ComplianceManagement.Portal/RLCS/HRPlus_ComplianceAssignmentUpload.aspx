﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_ComplianceAssignmentUpload.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_ComplianceAssignmentUpload" %>

<%@ Register Assembly="DropDownListChosen" Namespace="Saplin.Controls" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="background: white;">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <link href="/NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="/NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="/Newjs/kendo.all.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("body").tooltip({ selector: '[data-toggle=tooltip]' });

            if ($(window.parent.document).find("#divReAssignCompliance").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#divReAssignCompliance").children().first().hide();
        });
    </script>

    <style>
        input[type=file]{
            color:#666;
        }
    </style>
</head>
<body style="background: white;">
    <form id="form1" runat="server">
        <div class="chart-loading" id="divLoader"></div>
        <asp:ScriptManager ID="smComplianceDetails" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="upComplianceDetails" runat="server">
            <ContentTemplate>
                <div class="col-lg-12 col-md-12" style="min-height: 0px; max-height: 140px; overflow-y: auto;">
                    <asp:ValidationSummary ID="vsUploadAssignment" runat="server" DisplayMode="BulletList" CssClass="alert alert-block alert-danger fade in" ValidationGroup="oplValidationGroup" />
                    <asp:CustomValidator ID="cvUploadAssignment" runat="server" EnableClientScript="False"
                        ValidationGroup="oplValidationGroup" Display="None" Enabled="true" ShowSummary="true" />
                </div>

                <div class="clearfix"></div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <asp:LinkButton runat="server" CssClass="btn" ID="lbtnExportExcel" OnClick="lbtnExportExcel_Click"
                        style="background: url('../images/Excel _icon.png'); float: right; width: 47px; height: 33px;"
                        data-toggle="tooltip" data-placement="left" Title="Export to Excel" ToolTip="Download Format">                                                  
                    </asp:LinkButton>
                </div>

                <div class="clearfix"></div>

                <div class=" col-xs-12 col-sm-12 col-md-12 colpadding0">
                    <div class="col-xs-4 col-sm-4 col-md-4 text-right">
                        <label for="ddlCustomer" class="control-label">Customer</label>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8">
                        <asp:DropDownListChosen runat="server" ID="ddlCustomerList" CssClass="form-control" Width="95%"
                            AllowSingleDeselect="false" DataPlaceHolder="Select Customer">
                        </asp:DropDownListChosen>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ErrorMessage="Select Customer" ControlToValidate="ddlCustomerList"
                            runat="server" Display="None" ValidationGroup="oplValidationGroup" InitialValue="-1" />
                    </div>
                </div>

                <div class="form-group clearfix"></div>

                <div class="col-xs-12 col-sm-12 col-md-12 colpadding0">
                    <div class="col-xs-4 col-sm-4 col-md-4 text-right">
                        <label for="tbxFilterLocation" class="control-label">File to Upload</label>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8">
                        <asp:RadioButton ID="rdoAssignCompliance" runat="server" Checked="true" AutoPostBack="false" Text="Assign Compliance" GroupName="uploadContentGroup" Visible="false" />
                        <asp:FileUpload ID="MasterFileUpload" runat="server" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Select File to Upload" ControlToValidate="MasterFileUpload"
                            runat="server" Display="None" ValidationGroup="oplValidationGroup" />
                    </div>
                </div>

                <div class="form-group clearfix"></div>

                <div class=" col-xs-12 col-sm-12 col-md-12 text-center">
                    <asp:Button ID="btnUploadFile" runat="server" Text="Upload" ValidationGroup="oplValidationGroup" CssClass="btn btn-primary"
                        OnClick="btnUploadFile_Click" />
                </div>

                <div class="clearfix"></div>

                <div class=" col-xs-12 col-sm-12 col-md-12 colpadding0">
                    <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUploadFile" />
                <asp:PostBackTrigger ControlID="lbtnExportExcel" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
