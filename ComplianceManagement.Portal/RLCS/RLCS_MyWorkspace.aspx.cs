﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using com.VirtuosoITech.ComplianceManagement.Portal.ProductMapping;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyWorkspace : System.Web.UI.Page
    {
        public string Role;
        protected static List<Int32> roles;       
        protected string Reviewername;
        protected string Performername;     
        protected int CustomerID = 0;
        static int UserRoleID;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!this.IsPostBack)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    //UserRoleID = ActManagement.GetUserRoleID(AuthenticationHelper.UserID);
                    
                    BindLocationFilter();

                    string type = Request.QueryString["type"];
                    string filter = Request.QueryString["filter"];
                    string role = Request.QueryString["role"];
                    //roles = CustomerBranchManagement.GetDashboardAssignedRoleid(AuthenticationHelper.UserID);
                    roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);
                    
                    //BindStatusPerformer();
                    BindStatusReviewer();
                    
                    BindComplianceTransactions();
                    
                    if (SelectedPageNo.Text == "")
                    {
                        SelectedPageNo.Text = "1";
                    }

                    if (Session["TotalComplianceRows"].ToString() != "")
                        TotalRows.Value = Session["TotalComplianceRows"].ToString();

                    GetPageDisplaySummary();

                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                if (CustomerID == 0)
                {
                    CustomerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                }

                tvFilterLocation.Nodes.Clear();
                var bracnhes = CustomerBranchManagement.GetAllHierarchySatutory(CustomerID);

                string isstatutoryinternal = "S";
                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, CustomerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/Branch", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    // BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();
                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public void BindBranchesHierarchy(TreeNode parent, NameValueHierarchy nvp, List<int> LocationList)
        {
            foreach (var item in nvp.Children)
            {
                TreeNode node = new TreeNode(item.Name, item.ID.ToString());

                if (FindNodeExists(item, LocationList))
                {
                    BindBranchesHierarchy(node, item, LocationList);
                    parent.ChildNodes.Add(node);
                }
            }
        }

        public bool FindNodeExists(NameValueHierarchy item, List<int> ListIDs)
        {
            bool result = false;
            try
            {
                if (ListIDs.Contains(item.ID))
                {
                    result = true;
                    return result;
                }
                else
                {
                    foreach (var childNode in item.Children)
                    {
                        if (ListIDs.Contains(childNode.ID))
                        {
                            result = true;
                        }
                        else
                        {
                            result = FindNodeExists(childNode, ListIDs);
                        }

                        if (result)
                        {
                            break;
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        protected void upComplianceDetails_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeFilterLocation", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
               
        #region Search

        private void BindStatusPerformer()
        {
            try
            {
                ddlStatus.Items.Clear();
                foreach (Performer r in Enum.GetValues(typeof(Performer)))
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(Performer), r), r.ToString());
                    ddlStatus.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindStatusReviewer()
        { 
            try
            {
                ddlStatus.Items.Clear();

                foreach (RLCSReviewerStatus r in Enum.GetValues(typeof(RLCSReviewerStatus)))
                {
                    string description = GetDescription(r); 

                    //ListItem item = new ListItem(Enum.GetName(typeof(RLCSReviewerStatus), r), r.ToString());
                    ListItem item = new ListItem(description, r.ToString());
                    ddlStatus.Items.Add(item);
                }

                ddlStatus.Items.Insert(0, new ListItem("All", "0"));
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        public string GetDescription(Enum GenericEnum)
        {
            Type genericEnumType = GenericEnum.GetType();
            MemberInfo[] memberInfo = genericEnumType.GetMember(GenericEnum.ToString());
            if ((memberInfo != null && memberInfo.Length > 0))
            {
                try
                {
                    return memberInfo[0].GetCustomAttributesData()[0].ConstructorArguments[0].Value.ToString();
                }
                catch(Exception)
                {
                    return GenericEnum.ToString();
                }
                //var _Attribs = memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
                //if ((_Attribs != null && _Attribs.Count() > 0))
                //{
                //    return ((System.ComponentModel.DescriptionAttribute)_Attribs.ElementAt(0)).Description;
                //}
            }
            return GenericEnum.ToString();
        }

        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            { 
                SelectedPageNo.Text = "1";

                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);
                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                BindComplianceTransactions();
                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalComplianceRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalComplianceRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalComplianceRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
               
                BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalComplianceRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalComplianceRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                grdComplianceTransactions.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                grdComplianceTransactions.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                
                BindComplianceTransactions();
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                //DivRecordsScrum.Attributes.Remove("disabled");
                DivRecordsScrum.Visible = true;

                lblTotalRecord.Text = " " + Session["TotalComplianceRows"].ToString();

                lTotalCount.Text = GetTotalPagesCount().ToString();

                if (lTotalCount.Text != "0")
                {
                    if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                    {
                        SelectedPageNo.Text = "1";
                        lblStartRecord.Text = "1";

                        if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalComplianceRows"].ToString())))
                            lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                        else
                            lblEndRecord.Text = Session["TotalComplianceRows"].ToString() + " ";
                    }
                }
                else if (lTotalCount.Text == "0")
                {
                    SelectedPageNo.Text = "0";
                    DivRecordsScrum.Visible = false;
                }
            }

            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        
        private int GetTotalPagesCount()
        {
            try
            {

                TotalRows.Value = Session["TotalComplianceRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);
                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);
                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void grdComplianceTransactions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdComplianceTransactions.PageIndex = e.NewPageIndex;               
                BindComplianceTransactions();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void grdComplianceTransactions_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                    //Label lblScheduledOn = (Label)e.Row.FindControl("lblScheduledOn");
                    Label lblScheduledPerformerOn = (Label)e.Row.FindControl("lblScheduledOn");
                    Label lblDays = (Label)e.Row.FindControl("lblInterimdays");
                    Button btnChangeStatus = (Button)e.Row.FindControl("btnChangeStatus");
                    DateTime CurrentDate = DateTime.Today.Date;

                    //if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date > CurrentDate || lblStatus.Text.Trim() == "Submitted For Interim Review" || lblStatus.Text.Trim() == "Interim Review Approved" || lblStatus.Text.Trim() == "Interim Rejected")
                    //{
                    //    if (!string.IsNullOrEmpty(lblDays.Text))
                    //    {
                    //        e.Row.ForeColor = System.Drawing.Color.Blue;
                    //        e.Row.CssClass = "Inforamative";
                    //    }
                    //}

                    //btnChangeStatus.Enabled = true;
                    //if (lblStatus.Text.Trim() == "Submitted For Interim Review")
                    //{
                    //    btnChangeStatus.Enabled = false;
                    //}

                    //if (lblStatus.Text.Trim() == "Interim Review Approved")
                    //{
                    //    btnChangeStatus.Enabled = true;
                    //    btnChangeStatus.Visible = true;
                    //}
                    //if (lblStatus.Text.Trim() == "Interim Rejected")
                    //{
                    //    btnChangeStatus.Enabled = true;
                    //    btnChangeStatus.Visible = true;
                    //}

                    if (lblStatus != null)
                    {
                        if (lblScheduledPerformerOn != null)
                        {
                            //DateTime CurrentDate = DateTime.Today.Date;
                            if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "Open" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                            else if (lblStatus.Text.Trim() == "Complied but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "Complied Delayed but pending review")
                                lblStatus.Text = "Pending For Review";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date >= CurrentDate)
                                lblStatus.Text = "Upcoming";
                            else if (lblStatus.Text.Trim() == "In Progress" && Convert.ToDateTime(lblScheduledPerformerOn.Text).Date <= CurrentDate)
                                lblStatus.Text = "Overdue";
                        }
                    }

                    Label lblCanChangeStatus = (Label)e.Row.FindControl("lblCanChangeStatus");

                    if (lblCanChangeStatus != null)
                    {
                        if (!string.IsNullOrEmpty(lblCanChangeStatus.Text) && (lblCanChangeStatus.Text == "0" || lblCanChangeStatus.Text == "1"))
                        {
                            if (Convert.ToBoolean(Convert.ToInt32(lblCanChangeStatus.Text)))
                            {
                                btnChangeStatus.Visible = true;

                                LinkButton lnkbtn_GotoIOM = (LinkButton)e.Row.FindControl("lnkbtn_GotoIOM");
                                Label lblStatusID = (Label)e.Row.FindControl("lblStatusID");

                                if (lnkbtn_GotoIOM != null && lblStatusID != null)
                                {
                                    if (lblStatus.Text.Trim() == "Upcoming" || lblStatus.Text.Trim() == "Overdue")
                                        lnkbtn_GotoIOM.Visible = true;
                                    else
                                        lnkbtn_GotoIOM.Visible = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void grdComplianceTransactions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (!(e.CommandName.Equals("Sort")))
                {
                    if (e.CommandName.Equals("CHANGE_STATUS"))
                    {
                        string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
                        int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                        string Interimdays = Convert.ToString(commandArgs[2]);
                        string Status = Convert.ToString(commandArgs[3]);

                        Session["Days"] = "";
                        if (!string.IsNullOrEmpty(Interimdays))
                        {
                            Session["Days"] = Interimdays;
                            Session["Status"] = Interimdays;
                        }

                        //udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);
                    }
                    if (e.CommandName.Equals("GOTO_IO"))
                    {
                        if (e.CommandArgument != null)
                        {
                            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });

                            if (commandArgs.Length > 0)
                            {
                                string complianceType = string.Empty;
                                int returnRegisterChallanID = 0;

                                long scheduleOnID = Convert.ToInt64(commandArgs[0]);
                                int customerID = Convert.ToInt32(commandArgs[1]);
                                int branchID = Convert.ToInt32(commandArgs[2]);
                                int actID = Convert.ToInt32(commandArgs[3]);
                                int complianceID = Convert.ToInt32(commandArgs[4]);
                                string month = commandArgs[5];
                                string year = commandArgs[6];

                                if (customerID != 0)
                                {
                                    ProductMappingStructure _obj = new ProductMappingStructure();

                                    //customerID --- User's CustomerID not Selected CustomerID
                                    _obj.ReAuthenticate_User(customerID);

                                    //string[] userDetails = HttpContext.Current.User.Identity.Name.Split(';');

                                    //if (userDetails.Length == 9)
                                    //{
                                    //    //profileID = userDetails[7];
                                    //    FormsAuthentication.SetAuthCookie(string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
                                    //      userDetails[0], userDetails[1], userDetails[2], userDetails[3], userDetails[4], customerID, userDetails[6], userDetails[7], userDetails[8]), false);
                                    //}

                                    var masterRecord = RLCS_ComplianceManagement.GetRegisterReturnIDComType(customerID, branchID, actID, complianceID);

                                    if (masterRecord != null)
                                    {
                                        string masterID = masterRecord.MasterID;
                                        if (!string.IsNullOrEmpty(masterID))
                                        {
                                            string[] comTypeRegRetID = masterID.Split(new char[] { '-' });

                                            if (comTypeRegRetID.Length > 0)
                                            {
                                                complianceType = comTypeRegRetID[0];
                                                returnRegisterChallanID = Convert.ToInt32(comTypeRegRetID[1]);

                                                if (!string.IsNullOrEmpty(complianceType) && returnRegisterChallanID != 0)
                                                {
                                                    if (complianceType.Equals("REG"))
                                                        complianceType = "Register";
                                                    else if (complianceType.Equals("RET"))
                                                        complianceType = "Return";
                                                    else
                                                        complianceType = "Challan";
                                                                                                        
                                                    ScriptManager.RegisterStartupScript(this, Page.GetType(), "CallMyFunction", "GotoComplianceInputOutputPage('" + complianceType + "','" + returnRegisterChallanID + "','" + actID + "','" + complianceID + "','" + branchID + "','" + month + "','" + year + "');", true);

                                                    //Response.Redirect("~/RLCS/ComplianceInputOutput.aspx?ComplianceType=" + complianceType
                                                    //                                                + "&ReturnRegisterChallanID=" + returnRegisterChallanID
                                                    //                                                + "&ActID=" + actID
                                                    //                                                + "&ComplianceID=" + complianceID
                                                    //                                                + "&BranchID=" + branchID
                                                    //                                                + "&Month=" + month
                                                    //                                                + "&Year=" + year, false);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
               
        protected void BindComplianceTransactions()
        {
            try
            {
                Session["TotalComplianceRows"] = 0;

                int risk = Convert.ToInt32(ddlRiskType.SelectedValue);
                string month = ddlMonth.SelectedValue;
                int year = Convert.ToInt32(ddlYear.SelectedValue);
                int location = Convert.ToInt32(tvFilterLocation.SelectedValue);
                string status = ddlStatus.SelectedValue;
                string complianceType = ddlScopeType.SelectedValue;

                DateTime ScheduleOnCalender = DateTime.ParseExact("01-01-1900", "dd-MM-yyyy", CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(Request.QueryString["ScheduleOn"]))
                {
                    ScheduleOnCalender = Convert.ToDateTime(Request.QueryString["ScheduleOn"]);
                }
                
                string stringSearchText = "";               
                grdComplianceTransactions.Visible = false;

                var lstCompliances = RLCS_ComplianceManagement.GetRLCSMyWorkspace(Convert.ToInt32(AuthenticationHelper.CustomerID), AuthenticationHelper.UserID, "C", risk,month,year, status, location, stringSearchText, complianceType);

                grdComplianceTransactions.DataSource = lstCompliances;
                Session["TotalComplianceRows"] = lstCompliances.Count();
                grdComplianceTransactions.DataBind();
                grdComplianceTransactions.Visible = true;

                GetPageDisplaySummary();
                
                performerdocuments.Visible = true;
                performerdocuments.Attributes.Remove("class");
                performerdocuments.Attributes.Add("class", "tab-pane active");               
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
            }
        }
        
        /// <summary>
        /// this function return true false value for status image.
        /// </summary>
        /// <param name="userID">long</param>
        /// <param name="roleID">int</param>
        /// <param name="statusID">int</param>
        /// <returns></returns>
        /// 

        protected bool CanChangeStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;
                //roles = CustomerBranchManagement.GetDashboardAssignedRoleid(AuthenticationHelper.UserID);
                //roles = CustomerBranchManagement.GetAssignedroleid(AuthenticationHelper.UserID);

                result = CanChangeReviewerStatus(userID, roleID, statusID);
                //if (roles.Contains(3))
                //{
                //    result = CanChangePerformerStatus(userID, roleID, statusID);
                //}
                //else if (roles.Contains(4))
                //{
                //    result = CanChangeReviewerStatus(userID, roleID, statusID);
                //}                

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";

                return false;
            }
        }

        protected bool CanChangePerformerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1 || statusID == 6 || statusID == 10 || statusID == 12;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }

        protected bool CanChangeReviewerStatus(long userID, int roleID, int statusID)
        {
            try
            {
                bool result = false;

                if (userID == AuthenticationHelper.UserID)
                {
                    if (roleID == 3)
                    {
                        result = statusID == 1;
                    }
                    else if (roleID == 4)
                    {
                        result = statusID == 2 || statusID == 3 || statusID == 11 || statusID == 12;
                    }
                    else if (roleID == 5)
                    {
                        result = statusID == 2 || statusID == 3;
                    }
                    else if (roleID == 6)
                    {
                        result = statusID == 4 || statusID == 5 || statusID == 6;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
            return false;
        }
      
        protected void btnChangeStatusReviewer_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)(sender);
                string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });
                int ScheduledOnID = Convert.ToInt32(commandArgs[0]);
                int complianceInstanceID = Convert.ToInt32(commandArgs[1]);

                string Interimdays = Convert.ToString(commandArgs[2]);
                string lblStatusReviewer = Convert.ToString(commandArgs[3]);

                Session["InterimdaysReview"] = "";
                Session["ComplianceStatus"] = "";
                if (!string.IsNullOrEmpty(Interimdays) && lblStatusReviewer == "Submitted For Interim Review")
                {
                    Session["InterimdaysReview"] = Interimdays;
                    Session["ComplianceStatus"] = lblStatusReviewer;
                }
                              
                BindComplianceTransactions();
                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Server Error Occured. Please try again.";
            }
        }
        
        protected void btnChangeStatus_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)(sender);

                if (btn != null)
                {
                    string[] commandArgs = btn.CommandArgument.ToString().Split(new char[] { ',' });

                    if (commandArgs.Length > 0)
                    {
                        int scheduledOnID = Convert.ToInt32(commandArgs[0]);
                        int complianceInstanceID = Convert.ToInt32(commandArgs[1]);
                        string Interimdays = Convert.ToString(commandArgs[2]);
                        string Status = Convert.ToString(commandArgs[3]);
                        string lblScheduledOn = Convert.ToString(commandArgs[4]);

                        Session["Interimdays"] = "";
                        Session["Status"] = "";

                        DateTime CurrentDate = DateTime.Today.Date;

                        if (Status == "Open" && Convert.ToDateTime(lblScheduledOn).Date > CurrentDate || Status == "Submitted For Interim Review" || Status == "Interim Review Approved" || Status == "Interim Rejected")
                        {

                            if (!string.IsNullOrEmpty(Interimdays))
                            {
                                Session["Interimdays"] = Interimdays;
                                Session["Status"] = Status;
                            }
                        }

                        if (complianceInstanceID != 0 && scheduledOnID != 0)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ShowDialog(" + complianceInstanceID + "," + scheduledOnID + ");", true);
                        }
                    }
                }

                //udcStatusTranscatopn.OpenTransactionPage(ScheduledOnID, complianceInstanceID);

                //BindComplianceTransactions();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                cvDuplicateEntry.IsValid = false;
                cvDuplicateEntry.ErrorMessage = "Something went wrong. Please try again.";
            }
        }

    }    
}