﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_VendorAuditDashborad_Client : System.Web.UI.Page
    {
        protected static string seriesData_GraphMonthlyCompliancePie;
        protected static string seriesData_GraphFrequancyPie;
        protected static string perRiskChart;
        protected static string perFunctionPieChart;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAllNewDetail();
            }            
        }
        private void BindAllNewDetail()
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    ViewState["vrole"] = "HVADM";

                    int customerID = -1;
                    int branchID = -1;
                    long vendorid = -1;

                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    long userID = -1;
                    var role = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
                    {
                        role = Convert.ToString(ViewState["vrole"]);
                    }
                    else
                    {
                        User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
                        if (LoggedUser != null)
                        {
                            role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
                            ViewState["vrole"] = Convert.ToString(role);
                        }
                    }
                    if (role.Trim().Contains("HVADM"))
                    {
                        userID = -1;
                    }
                    else
                    {
                        userID = AuthenticationHelper.UserID;
                    }

                    var lstChecklistDetails = entities.SP_RLCS_VendorAudit_CheckListReport(vendorid, branchID, customerID, role, userID).ToList();
                    if (lstChecklistDetails.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(AuthenticationHelper.Role))
                        {                             
                            if (AuthenticationHelper.Role.Equals("HMGR"))
                            {
                                string DeptId = Convert.ToString(RLCSVendorMastersManagement.GetDeptID("HR"));
                                lstChecklistDetails = lstChecklistDetails.Where(entry => (entry.DepartmentType != null && entry.DepartmentType.Trim() == DeptId.Trim())).ToList();                                
                            }
                            if (AuthenticationHelper.Role.Equals("VNDADM"))
                            {
                                string DeptId = Convert.ToString(RLCSVendorMastersManagement.GetDeptID("Admin"));
                                lstChecklistDetails = lstChecklistDetails.Where(entry => (entry.DepartmentType != null && entry.DepartmentType.Trim() == DeptId.Trim())).ToList();
                            }
                        }
                    }
                    lstChecklistDetails = lstChecklistDetails.Distinct().ToList();
                    
                    List<int> openstatus = new List<int>();
                    openstatus.Add(1);
                    openstatus.Add(2);
                    openstatus.Add(3);

                    divOpenAuditCount.InnerText = Convert.ToString(lstChecklistDetails.Where(x => openstatus.Contains(x.AuditStatusID)).Distinct().ToList().Count);
                    divClosedAuditCount.InnerText = Convert.ToString(lstChecklistDetails.Where(x => x.AuditStatusID == 4).Distinct().ToList().Count);
                    divCancelledAuditCount.InnerText = Convert.ToString(lstChecklistDetails.Where(x => x.AuditStatusID == 5).Distinct().ToList().Count);
                    divAuditorCount.InnerText = Convert.ToString(lstChecklistDetails.Select(x => x.AVACOM_AuditorID).Distinct().ToList().Count);
                    divVendorCount.InnerText = Convert.ToString(lstChecklistDetails.Select(x => x.Avacom_VendorID).Distinct().ToList().Count);


                    int MonthlyFreqAudit = lstChecklistDetails.Where(x => x.Frequency.Equals("M")).Distinct().ToList().Count;
                    int QuarterlyFreqAudit = lstChecklistDetails.Where(x => x.Frequency.Equals("Q")).Distinct().ToList().Count;
                    int AnnuallyFreqAudit = lstChecklistDetails.Where(x => x.Frequency.Equals("A")).Distinct().ToList().Count;
                    int HalfYearlyFreqAudit = lstChecklistDetails.Where(x => x.Frequency.Equals("H")).Distinct().ToList().Count;

                    int compliedCount = 0;
                    int NotcompliedCount = 0;
                    int AppliANDNotApplicableCount = 0;
                    int totalNotCompliedHIGH = 0;
                    int totalNotCompliedMEDIUM = 0;
                    int totalNotCompliedLOW = 0;
                    int totalCompliedHIGH = 0;
                    int totalCompliedMEDIUM = 0;
                    int totalCompliedLOW = 0;
                    int totalNotApplicableHIGH = 0;
                    int totalNotApplicableMEDIUM = 0;
                    int totalNotApplicableLOW = 0;
                    if (lstChecklistDetails.Count > 0)
                    {                       
                        foreach (var item in lstChecklistDetails)
                        {
                            var table = (from row in entities.SP_RLCS_VendorAudit_DetailsCheckListReport(item.ScheduleonID, item.AuditID)
                                         select row).ToList();
                            table = table.Where(row => row.ChecklistStatus.Equals("Closed")).ToList(); 
                            int complied = table.Where(entry => entry.ResultStatusID == 1).ToList().Count;
                            int Notcomplied = table.Where(a => a.ResultStatusID == 2).ToList().Count;
                            int AppliANDNotApplicable = table.Where(b => b.ResultStatusID == 3 || b.ResultStatusID == 4).ToList().Count;

                            compliedCount = compliedCount + complied;
                            NotcompliedCount = NotcompliedCount + Notcomplied;
                            AppliANDNotApplicableCount = AppliANDNotApplicableCount + AppliANDNotApplicable;
                            
                            //Risk Summary
                            int compliedHIGH = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("High")).ToList().Count;
                            int compliedMEDIUM = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Medium")).ToList().Count;
                            int compliedLOW = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Low")).ToList().Count;

                            int NotcompliedHIGH = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("High")).ToList().Count;
                            int NotcompliedMEDIUM = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Medium")).ToList().Count;
                            int NotcompliedLOW = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Low")).ToList().Count;

                            int NotApplicableHIGH = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("High")).ToList().Count;
                            int NotApplicableMEDIUM = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Medium")).ToList().Count;
                            int NotApplicableLOW = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Low")).ToList().Count;

                            totalNotCompliedHIGH = totalNotCompliedHIGH + NotcompliedHIGH;
                            totalNotCompliedMEDIUM = totalNotCompliedMEDIUM + NotcompliedMEDIUM;
                            totalNotCompliedLOW = totalNotCompliedLOW + NotcompliedLOW;

                            totalCompliedHIGH = totalCompliedHIGH + compliedHIGH;
                            totalCompliedMEDIUM = totalCompliedMEDIUM + compliedMEDIUM;
                            totalCompliedLOW = totalCompliedLOW + compliedLOW;

                            totalNotApplicableHIGH = totalNotApplicableHIGH + NotApplicableHIGH;
                            totalNotApplicableMEDIUM = totalNotApplicableMEDIUM + NotApplicableMEDIUM;
                            totalNotApplicableLOW = totalNotApplicableLOW + NotApplicableLOW;
                        }
                    }
               

                    seriesData_GraphMonthlyCompliancePie = string.Empty;

                    var pieComplied = compliedCount;
                    var pieNotComplied = NotcompliedCount;
                    var pieNotApplicable = AppliANDNotApplicableCount;

                    #region OverAll Summary
                    seriesData_GraphMonthlyCompliancePie += "{name:'Complied', color:'#64B973',  y:" + pieComplied + " },";/* color: '#FF7473'*/
                    seriesData_GraphMonthlyCompliancePie += "{name:'Not Complied', color:'#FF0000', y:" + pieNotComplied + " },"; /*color: '#FFC952'*/
                    seriesData_GraphMonthlyCompliancePie += "{name:'Not Applicable', color:'#FFD320', y:" + pieNotApplicable + " },"; /*color: '#FFC952'*/
                    #endregion

                    #region Frequency
                    seriesData_GraphFrequancyPie = string.Empty;

                    seriesData_GraphFrequancyPie += "{name:'Annually', color:'#64B973',  y:" + AnnuallyFreqAudit + " },";/* color: '#FF7473'*/
                    seriesData_GraphFrequancyPie += "{name:'Half Yearly', color:'#FF0000', y:" + HalfYearlyFreqAudit + " },"; /*color: '#FFC952'*/
                    seriesData_GraphFrequancyPie += "{name:'Quarterly', color:'#FFD320', y:" + QuarterlyFreqAudit + " },"; /*color: '#FFC952'*/
                    seriesData_GraphFrequancyPie += "{name:'Monthly', color:'#FF7473', y:" + MonthlyFreqAudit + " },"; /*color: '#FFC952'*/

                    #endregion

                    #region Risk Summary

                    perRiskChart = "series: [{name: 'Not Complied', color: '#FF0000',data: [{" + /*perRiskStackedColumnChartColorScheme.high*/
                                                                                                 // Not Completed - High
                               "y: " + totalNotCompliedHIGH + "},{" +

                               // Not Completed - Medium
                               " y: " + totalNotCompliedMEDIUM + "},{  " +

                               // Not Completed - Low
                               "y: " + totalNotCompliedLOW + "}]},{name: 'Complied',color:'#64B973',data: [{" + /*color: perRiskStackedColumnChartColorScheme.medium*/

                               // After Due Date - High
                               "y: " + totalCompliedHIGH + "},{   " +

                               // After Due Date - Medium
                               "y: " + totalCompliedMEDIUM + "},{   " +
                               // After Due Date - Low
                               "y: " + totalCompliedLOW + "}]},{name: 'Not Applicable',color:'#FFD320',data: [{" + /*perRiskStackedColumnChartColorScheme.low*/
                                                                                                                   // In Time - High
                               "y: " + totalNotApplicableHIGH + "},{  " +

                               // In Time - Medium
                               "y: " + totalNotApplicableMEDIUM + "},{  " +
                               // In Time - Low
                               "y: " + totalNotApplicableLOW + "}]}]";
                    #endregion

                    #region Performance Summary                

                    perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Complied',y: " + pieComplied + ",color: '#64B973',drilldown: 'Complied',},{name: 'Not Complied',y: " + pieNotComplied + ",color: '#FF0000',drilldown: 'NotComplied',},{name: 'Not Applicable',y: " + pieNotApplicable + ",color: '#FFD320',drilldown: 'NotApplicable',}],}],";

                    perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
                                       " series: [" +

                                        " {id: 'NotComplied',name: 'Not Complied',cursor: 'pointer',data: [{name: 'High',y: " + totalNotCompliedHIGH + ",color: '#FF0000',},{name: 'Medium',y: " + totalNotCompliedMEDIUM + ",color: '#FFD320',},{name: 'Low',y: " + totalNotCompliedLOW + ",color: '#64B973',}],}," +
                                        " {id: 'NotApplicable',name: 'Not Applicable',cursor: 'pointer',data: [{name: 'High',y: " + totalNotApplicableHIGH + ",color: '#FF0000',},{name: 'Medium',y: " + totalNotApplicableMEDIUM + ",color: '#FFD320',},{name: 'Low',y: " + totalNotApplicableLOW + ",color: '#64B973',}],}," +
                                        " {id: 'Complied',name: 'Complied',cursor: 'pointer',data: [{name: 'High',y: " + totalCompliedHIGH + ",color: '#FF0000',},{name: 'Medium',y: " + totalCompliedMEDIUM + ",color: '#FFD320',},{name: 'Low',y: " + totalCompliedLOW + ",color: '#64B973',}],}],},";

                    #endregion
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        //private void BindAllDetail()
        //{
        //    try
        //    {
        //        using (ComplianceDBEntities entities = new ComplianceDBEntities())
        //        {
        //            ViewState["vrole"] = "HVADM";

        //            int MonthlyFreqAudit = 0;
        //            int QuarterlyFreqAudit = 0;
        //            int AnnuallyFreqAudit = 0;
        //            int HalfYearlyFreqAudit = 0;

        //            int compliedCount = 0;
        //            int NotcompliedCount = 0;
        //            int AppliANDNotApplicableCount = 0;

        //            int HighCount = 0;
        //            int MediumCount = 0;
        //            int LowCount = 0;

        //            int customerID = -1;
        //            int branchID = -1;
        //            long vendorid = -1;

        //            int totalNotCompliedHIGH = 0;
        //            int totalNotCompliedMEDIUM = 0;
        //            int totalNotCompliedLOW = 0;

        //            int totalCompliedHIGH = 0;
        //            int totalCompliedMEDIUM = 0;
        //            int totalCompliedLOW = 0;

        //            int totalNotApplicableHIGH = 0;
        //            int totalNotApplicableMEDIUM = 0;
        //            int totalNotApplicableLOW = 0;


        //            customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
        //            long userID = -1;
        //            var role = string.Empty;
        //            if (!string.IsNullOrEmpty(Convert.ToString(ViewState["vrole"])))
        //            {
        //                role = Convert.ToString(ViewState["vrole"]);
        //            }
        //            else
        //            {
        //                User LoggedUser = UserManagement.GetByID(AuthenticationHelper.UserID);
        //                if (LoggedUser != null)
        //                {
        //                    role = RoleManagement.GetByID((int)LoggedUser.VendorRoleID).Code;
        //                    ViewState["vrole"] = Convert.ToString(role);
        //                }
        //            }
        //            if (role.Trim().Contains("HVADM"))
        //            {
        //                userID = -1;
        //            }
        //            else
        //            {
        //                userID = AuthenticationHelper.UserID;
        //            }

        //            var lstChecklistDetails = entities.SP_RLCS_VendorAudit_CheckListReport(vendorid, branchID, customerID, role, userID).ToList();
        //            if (lstChecklistDetails.Count > 0)
        //            {
        //                if (!string.IsNullOrEmpty(AuthenticationHelper.Role))
        //                {
        //                    if (AuthenticationHelper.Role.Equals("HMGR"))
        //                    {
        //                        lstChecklistDetails = lstChecklistDetails.Where(entry => entry.DepartmentType.Equals("3")).ToList();
        //                    }
        //                }
        //            }
        //            lstChecklistDetails = lstChecklistDetails.Distinct().ToList();
        //            //lstChecklistDetails = lstChecklistDetails.Where(x => x.AuditStatusID == 4).ToList();

        //            List<int> openstatus = new List<int>();
        //            openstatus.Add(1);
        //            openstatus.Add(2);
        //            openstatus.Add(3);

        //            divOpenAuditCount.InnerText = Convert.ToString(lstChecklistDetails.Where(x => openstatus.Contains(x.AuditStatusID)).Distinct().ToList().Count);
        //            divClosedAuditCount.InnerText = Convert.ToString(lstChecklistDetails.Where(x => x.AuditStatusID == 4).Distinct().ToList().Count);
        //            divCancelledAuditCount.InnerText = Convert.ToString(lstChecklistDetails.Where(x => x.AuditStatusID == 5).Distinct().ToList().Count);
        //            divAuditorCount.InnerText = Convert.ToString(lstChecklistDetails.Select(x => x.AVACOM_AuditorID).Distinct().ToList().Count);
        //            divVendorCount.InnerText = Convert.ToString(lstChecklistDetails.Select(x => x.Avacom_VendorID).Distinct().ToList().Count);


        //            MonthlyFreqAudit = lstChecklistDetails.Where(x => x.Frequency.Equals("M")).Distinct().ToList().Count;
        //            QuarterlyFreqAudit = lstChecklistDetails.Where(x => x.Frequency.Equals("Q")).Distinct().ToList().Count;
        //            AnnuallyFreqAudit = lstChecklistDetails.Where(x => x.Frequency.Equals("A")).Distinct().ToList().Count;
        //            HalfYearlyFreqAudit = lstChecklistDetails.Where(x => x.Frequency.Equals("H")).Distinct().ToList().Count;

        //            if (lstChecklistDetails.Count > 0)
        //            {
        //                compliedCount = 0;
        //                NotcompliedCount = 0;
        //                AppliANDNotApplicableCount = 0;
        //                HighCount = 0;
        //                MediumCount = 0;
        //                LowCount = 0;

        //                totalNotCompliedHIGH = 0;
        //                totalNotCompliedMEDIUM = 0;
        //                totalNotCompliedLOW = 0;

        //                totalCompliedHIGH = 0;
        //                totalCompliedMEDIUM = 0;
        //                totalCompliedLOW = 0;

        //                totalNotApplicableHIGH = 0;
        //                totalNotApplicableMEDIUM = 0;
        //                totalNotApplicableLOW = 0;
        //                foreach (var item in lstChecklistDetails)
        //                {
        //                    var table = (from row in entities.SP_RLCS_VendorAudit_DetailsCheckListReport(item.ScheduleonID, item.AuditID)
        //                                 select row).ToList();

        //                    int complied = table.Where(entry => entry.ResultStatusID == 1).ToList().Count;
        //                    int Notcomplied = table.Where(a => a.ResultStatusID == 2).ToList().Count;
        //                    int AppliANDNotApplicable = table.Where(b => b.ResultStatusID == 3 || b.ResultStatusID == 4).ToList().Count;

        //                    compliedCount = compliedCount + complied;
        //                    NotcompliedCount = NotcompliedCount + Notcomplied;
        //                    AppliANDNotApplicableCount = AppliANDNotApplicableCount + AppliANDNotApplicable;

        //                    int High = table.Where(entry => entry.Risk.Equals("High")).ToList().Count;
        //                    int Low = table.Where(entry => entry.Risk.Equals("Low")).ToList().Count;
        //                    int Medium = table.Where(entry => entry.Risk.Equals("Medium")).ToList().Count;

        //                    HighCount = HighCount + High;
        //                    LowCount = LowCount + Low;
        //                    MediumCount = MediumCount + Medium;

        //                    //Risk Summary
        //                    int compliedHIGH = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("High")).ToList().Count;
        //                    int compliedMEDIUM = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Medium")).ToList().Count;
        //                    int compliedLOW = table.Where(entry => entry.ResultStatusID == 1 && entry.Risk.Equals("Low")).ToList().Count;

        //                    int NotcompliedHIGH = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("High")).ToList().Count;
        //                    int NotcompliedMEDIUM = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Medium")).ToList().Count;
        //                    int NotcompliedLOW = table.Where(entry => entry.ResultStatusID == 2 && entry.Risk.Equals("Low")).ToList().Count;

        //                    int NotApplicableHIGH = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("High")).ToList().Count;
        //                    int NotApplicableMEDIUM = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Medium")).ToList().Count;
        //                    int NotApplicableLOW = table.Where(entry => (entry.ResultStatusID == 3 || entry.ResultStatusID == 4) && entry.Risk.Equals("Low")).ToList().Count;

        //                    totalNotCompliedHIGH = totalNotCompliedHIGH + NotcompliedHIGH;
        //                    totalNotCompliedMEDIUM = totalNotCompliedMEDIUM + NotcompliedMEDIUM;
        //                    totalNotCompliedLOW = totalNotCompliedLOW + NotcompliedLOW;

        //                    totalCompliedHIGH = totalCompliedHIGH + compliedHIGH;
        //                    totalCompliedMEDIUM = totalCompliedMEDIUM + compliedMEDIUM;
        //                    totalCompliedLOW = totalCompliedLOW + compliedLOW;

        //                    totalNotApplicableHIGH = totalNotApplicableHIGH + NotApplicableHIGH;
        //                    totalNotApplicableMEDIUM = totalNotApplicableMEDIUM + NotApplicableMEDIUM;
        //                    totalNotApplicableLOW = totalNotApplicableLOW + NotApplicableLOW;
        //                }
        //            }
        //            seriesData_GraphMonthlyCompliancePie = string.Empty;

        //            var pieComplied = compliedCount;
        //            var pieNotComplied = NotcompliedCount;
        //            var pieNotApplicable = AppliANDNotApplicableCount;

        //            #region Checklist Summary
        //            seriesData_GraphMonthlyCompliancePie += "{name:'Complied', color:'#64B973',  y:" + pieComplied + " },";/* color: '#FF7473'*/
        //            seriesData_GraphMonthlyCompliancePie += "{name:'Not Complied', color:'#FF0000', y:" + pieNotComplied + " },"; /*color: '#FFC952'*/
        //            seriesData_GraphMonthlyCompliancePie += "{name:'Not Applicable', color:'#FFD320', y:" + pieNotApplicable + " },"; /*color: '#FFC952'*/
        //            #endregion

        //            #region Frequency
        //            seriesData_GraphFrequancyPie = string.Empty;

        //            seriesData_GraphFrequancyPie += "{name:'Annually', color:'#64B973',  y:" + AnnuallyFreqAudit + " },";/* color: '#FF7473'*/
        //            seriesData_GraphFrequancyPie += "{name:'Half Yearly', color:'#FF0000', y:" + HalfYearlyFreqAudit + " },"; /*color: '#FFC952'*/
        //            seriesData_GraphFrequancyPie += "{name:'Quarterly', color:'#FFD320', y:" + QuarterlyFreqAudit + " },"; /*color: '#FFC952'*/
        //            seriesData_GraphFrequancyPie += "{name:'Monthly', color:'#FF7473', y:" + MonthlyFreqAudit + " },"; /*color: '#FFC952'*/

        //            #endregion

        //            #region Risk Summary

        //            perRiskChart = "series: [{name: 'Not Complied', color: '#FF0000',data: [{" + /*perRiskStackedColumnChartColorScheme.high*/
        //                                                                                         // Not Completed - High
        //                       "y: " + totalNotCompliedHIGH + "},{" +

        //                       // Not Completed - Medium
        //                       " y: " + totalNotCompliedMEDIUM + "},{  " +

        //                       // Not Completed - Low
        //                       "y: " + totalNotCompliedLOW + "}]},{name: 'Complied',color:'#64B973',data: [{" + /*color: perRiskStackedColumnChartColorScheme.medium*/

        //                       // After Due Date - High
        //                       "y: " + totalCompliedHIGH + "},{   " +

        //                       // After Due Date - Medium
        //                       "y: " + totalCompliedMEDIUM + "},{   " +
        //                       // After Due Date - Low
        //                       "y: " + totalCompliedLOW + "}]},{name: 'Not Applicable',color:'#FFD320',data: [{" + /*perRiskStackedColumnChartColorScheme.low*/
        //                                                                                                           // In Time - High
        //                       "y: " + totalNotApplicableHIGH + "},{  " +

        //                       // In Time - Medium
        //                       "y: " + totalNotApplicableMEDIUM + "},{  " +
        //                       // In Time - Low
        //                       "y: " + totalNotApplicableLOW + "}]}]";
        //            #endregion

        //            #region Performance Summary                

        //            perFunctionPieChart = "series: [{name: 'Status',tooltip:{pointFormat: 'hello',},data: [{name: 'Complied',y: " + pieComplied + ",color: '#64B973',drilldown: 'Complied',},{name: 'Not Complied',y: " + pieNotComplied + ",color: '#FF0000',drilldown: 'NotComplied',},{name: 'Not Applicable',y: " + pieNotApplicable + ",color: '#FFD320',drilldown: 'NotApplicable',}],}],";

        //            perFunctionPieChart += "drilldown:{activeDataLabelStyle: {textDecoration: 'none',color: 'gray',}, " +
        //                               " series: [" +

        //                                " {id: 'NotComplied',name: 'Not Complied',cursor: 'pointer',data: [{name: 'High',y: " + totalNotCompliedHIGH + ",color: '#FF0000',},{name: 'Medium',y: " + totalNotCompliedMEDIUM + ",color: '#FFD320',},{name: 'Low',y: " + totalNotCompliedLOW + ",color: '#64B973',}],}," +
        //                                " {id: 'NotApplicable',name: 'Not Applicable',cursor: 'pointer',data: [{name: 'High',y: " + totalNotApplicableHIGH + ",color: '#FF0000',},{name: 'Medium',y: " + totalNotApplicableMEDIUM + ",color: '#FFD320',},{name: 'Low',y: " + totalNotApplicableLOW + ",color: '#64B973',}],}," +
        //                                " {id: 'Complied',name: 'Complied',cursor: 'pointer',data: [{name: 'High',y: " + totalCompliedHIGH + ",color: '#FF0000',},{name: 'Medium',y: " + totalCompliedMEDIUM + ",color: '#FFD320',},{name: 'Low',y: " + totalCompliedLOW + ",color: '#64B973',}],}],},";

        //            #endregion
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);              
        //    }
        //}
    }
}