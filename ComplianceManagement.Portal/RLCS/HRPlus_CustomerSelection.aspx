﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_CustomerSelection.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_CustomerSelection" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="AVANTIS - Products that simplify" />
    <meta name="author" content="Avantis - Development Team" />

    <title>Customer :: AVANTIS - Products that simplify</title>

    <link href="../Style/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../Style/css/bootstrap-theme.css" rel="stylesheet" />
    <link href="../Style/css/elegant-icons-style.css" rel="stylesheet" />
    <link href="../Style/css/font-awesome.css" rel="stylesheet" />

    <link href="../NewCSS/stylenew.css" rel="stylesheet" />
    <link href="../Style/css/style-responsive.css" rel="stylesheet" />
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-92029518-1', 'auto');
        ga('send', 'pageview');

        function settracknew(e, t, n, r) {
            try {
                ga('send', 'event', e, t, n + "#" + r)
            } catch (t) { } return !0
        }

        function settracknewnonInteraction(e, t, n, r) {
            ga('send', 'event', e, t, n, 0, { 'nonInteraction': 1 })
        }

        function CallSelectIndexEvent(CustomerId) {
            $("#hfCustId").val(CustomerId);
            document.getElementById('<%= btnClick.ClientID %>').click();
            return false;
        }

        function fclearcookie() {
            window.location.href = "/logout.aspx"
            return false;
        }
    </script>

    <style>
        .s2n8Rd .WBW9sf {
            overflow: hidden;
        }

        .d2laFc .WBW9sf {
            -webkit-box-flex: 1;
            box-flex: 1;
            -webkit-flex-grow: 1;
            flex-grow: 1;
        }

        .JDAKTe {
            margin-top: 8px;
            margin-left: 25px;
            padding-left: 15px;
        }

            .JDAKTe.W7Aapd,
            .JDAKTe.SmR8,
            .JDAKTe.cd29Sd {
                margin: 0 -24px;
                list-style: none;
                padding: 0;
                position: relative;
            }

        @media all and (min-width:450px) {
            .JDAKTe.cd29Sd,
            .JDAKTe.SmR8 {
                margin: 0 -40px;
            }
        }

        .JDAKTe.zpCp3 {
            margin: auto -24px;
        }

        @media all and (min-width:450px) {
            .JDAKTe.zpCp3 {
                margin: auto -40px;
            }
        }

        .zeRELc .ibdqA {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
        }

            .zeRELc .ibdqA .lCoei {
                -webkit-box-flex: 1;
                box-flex: 1;
                -webkit-flex-grow: 1;
                flex-grow: 1;
                padding-bottom: 12px;
                padding-top: 12px;
            }

        .lCoei {
            display: -webkit-box;
            display: -webkit-flex;
            display: flex;
            padding-bottom: 16px;
            padding-top: 16px;
        }



        .d2laFc .WBW9sf {
            -webkit-box-flex: 1;
            box-flex: 1;
            -webkit-flex-grow: 1;
            flex-grow: 1;
        }

        .d2laFc .r78aae,
        .d2laFc .stUf5b,
        .d2laFc .G5XIyb {
            max-height: 100%;
            max-width: 100%;
        }

        .d2laFc .w1I7fb {
            color: #3c4043;
            font-size: 14px;
            font-weight: 500;
        }

        .d2laFc {
            width: 100%;
        }

            .d2laFc .qQWzTd {
                height: 28px;
                margin-right: 12px;
                /* width:28px */
            }

            .d2laFc .tgnCOd,
            .s2n8Rd .tgnCOd {
                display: -webkit-box;
                display: -webkit-flex;
                display: flex;
                -webkit-align-items: center;
                align-items: center;
            }


        .qQWzTd {
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }

        .d2laFc .qQWzTd {
            height: 28px;
            margin-right: 12px;
            /* width:28px */
        }

        .w1I7fb {
            font-family: 'Google Sans', arial, sans-serif;
            line-height: 1.4286;
        }

        .image-parent {
            border-radius: 50%;
            max-width: 80px;
            height: 65px;
            border:1px solid;
        }

        .login-form {
            max-width: 500px !important;
        }

        .login-form-head {
            max-width: 500px !important;
        }

        img.img-fluid {
            
            max-width: 100%;
            max-height: 100%;
            
        }

        li:hover {
            background-color: #e8f0fe;
        }

        .custLogo:hover{
            background-color:#fff;
            color:#515967;
        }
    </style>
</head>
<body>
    <div class="container">
        <form runat="server" class="login-form" name="login" id="Form1" autocomplete="off">
            <asp:Panel ID="Panel1" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="col-md-12 login-form-head">
                            <p class="login-img">
                                <img src="../Images/avantil-logo.png" />
                            </p>
                        </div>
                        <div class="login-wrap">
                            <h1 id="headingText" style="text-align: center; font-weight: 400;"><span>Choose a customer</span></h1>
                            <div class="container">
                                <div class="row">
                                    <ul class="list-group">
                                        <asp:HiddenField ID="hfCustId" runat="server" />
                                        <li style="cursor: pointer">
                                            <div class="col-md-12 colpadding0">
                                                <%foreach (var eachCustomer in lstUserAssignedCustomers)
                                                    { %>
                                                <div class="col-md-4 colpadding0 custLogo" id="<%= eachCustomer.custID %>" onclick=" return CallSelectIndexEvent('<%= eachCustomer.custID %>')" style="text-align: center">
                                                    <%if (eachCustomer.logoPath != null)
                                                        {%>
                                                    <div class="col-md-12 colpadding0" style="padding: 10px 10px!important;">
                                                        <img src="<%= eachCustomer.logoPath %>" class="image-parent" />
                                                    </div>
                                                    <% } %>
                                                    <% else
                                                    {%>
                                                    <div class="col-md-12 colpadding0" style="padding: 10px 10px!important;">
                                                        <img src="../img/icon-th-profile.png" class="image-parent" />
                                                    </div>
                                                    <% } %>
                                                    <div class="col-md-12 colpadding0">
                                                        <span style="color: #666; display: inline-block; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; -o-text-overflow: ellipsis; max-width: 100%;"
                                                            data-toggle="tooltip" data-original-title="<%= eachCustomer.custName %>"><%= eachCustomer.custName %></span>
                                                    </div>
                                                </div>
                                                <%}%>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div style="text-align: center; margin-top: 3%;">
                                <asp:LinkButton ID="lnkLogOut" OnClientClick="javascript:return fclearcookie();" runat="server">Or sign in as a different user</asp:LinkButton>
                            </div>
                        </div>

                        <asp:Button runat="server" ID="btnClick" Style="display: none" OnClick="btnClick_Click"></asp:Button>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </form>
    </div>

    <div class="clearfix" style="height: 10px"></div>
    <!--js-->

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

</body>
</html>
