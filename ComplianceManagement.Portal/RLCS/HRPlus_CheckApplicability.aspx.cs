﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class HRPlus_CheckApplicability : System.Web.UI.Page
    {
        protected static int custID = -1;

        protected static int IsSPDist;
        protected static int spID = -1;
        protected static int distID = -1;
        protected int userID;

        protected static string avacomRLCSAPIURL;
        protected static string ProfileID;

        protected void Page_Load(object sender, EventArgs e)
        {
            userID = Convert.ToInt32(AuthenticationHelper.UserID);
            avacomRLCSAPIURL = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];

            if (!string.IsNullOrEmpty(AuthenticationHelper.ProfileID))
                ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);
            else
                ProfileID = Convert.ToString(AuthenticationHelper.UserID);

            if (AuthenticationHelper.Role == "SPADM")
            {
                spID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 0;
            }
            else if (AuthenticationHelper.Role == "DADMN")
            {
                distID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 1;
            }
            else if (AuthenticationHelper.Role == "IMPT")
            {
                IsSPDist = 2;
            }
            else if (AuthenticationHelper.Role == "CADMN")
            {
                custID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                IsSPDist = 3;
            }
        }
    }
}