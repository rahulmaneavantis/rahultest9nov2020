﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Configuration;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class MyComplianceCalendar : System.Web.UI.Page
    {
        public static string CalendarDate;
        public static DateTime CalendarTodayOrNextDate;
        public static string date = "";
        protected static string CalenderDateString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["type"] != "undefined")
                {
                    try
                    {
                        bool isAvantis = false;
                        if (!string.IsNullOrEmpty(Request.QueryString["IsAvantis"]))
                        {
                            isAvantis = Convert.ToBoolean(Convert.ToInt32(Request.QueryString["IsAvantis"]));
                        }

                        string type = Convert.ToString(Request.QueryString["type"]);
                        GetCalender(isAvantis);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "fcal", "fcal('" + CalendarTodayOrNextDate.ToString("yyyy-MM-dd") + "');", true);
                    }
                    catch (Exception ex) { }
                }
            }
        }

        public DataTable fetchData(int customerid, int userid, bool isAvantis)
        {
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                DataTable mgmtstatutorydatatable = new DataTable();

                #region Statutory
                entities.Database.CommandTimeout = 180;
                var mgmtQueryStatutory = (from row in entities.SP_RLCS_ComplianceInstanceTransactionViewCustomerWiseManagement(customerid, userid, AuthenticationHelper.ProfileID)
                                          where row.IsAvantis == isAvantis
                                          select row).ToList();
                if (mgmtQueryStatutory.Count > 0)
                {
                    mgmtQueryStatutory = mgmtQueryStatutory.GroupBy(entity => entity.ScheduledOnID).Select(entity => entity.FirstOrDefault()).ToList();
                }
                if (mgmtQueryStatutory.Count > 0)
                {
                    var Statutory = (from row in mgmtQueryStatutory
                                     select new
                                     {
                                         ScheduledOn = row.PerformerScheduledOn,
                                         ComplianceStatusID = row.ComplianceStatusID,
                                         RoleID = row.RoleID,
                                         ScheduledOnID = row.ScheduledOnID,
                                         CustomerBranchID = row.CustomerBranchID,
                                     }).Distinct().ToList();
                    mgmtstatutorydatatable = Statutory.ToDataTable();
                }
                #endregion

                DataTable newtable = new DataTable();

                //management
                if (mgmtstatutorydatatable.Rows.Count > 0)
                {
                    newtable.Merge(mgmtstatutorydatatable);
                }

                return newtable;
            }
        }

        public void GetCalender(bool isAvantis)
        {
            DateTime dt1 = DateTime.Now.Date;

            string month = dt1.Month.ToString();
            if (Convert.ToInt32(month) < 10)
            {
                month = "0" + month;
            }

            string year = dt1.Year.ToString();
            CalendarDate = year + "-" + month;
            List<Compliancecalendar> events = new List<Compliancecalendar>();
            int customerid = Convert.ToInt32(AuthenticationHelper.CustomerID);
            int userid = AuthenticationHelper.UserID;
            DataTable dt = fetchData(customerid, userid, isAvantis);
            var list = (from r in dt.AsEnumerable()
                        select r["ScheduledOn"]).Distinct().ToList();

            var datetodayornext = (from r in dt.AsEnumerable()
                                   orderby r.Field<DateTime>("ScheduledOn")
                                   where r.Field<DateTime>("ScheduledOn") >= DateTime.Now.AddDays(-1)
                                   select r["ScheduledOn"]).Distinct().FirstOrDefault();

            if (datetodayornext != null)
                CalendarTodayOrNextDate = Convert.ToDateTime(datetodayornext);
            else
                CalendarTodayOrNextDate = DateTime.Now;

            CalenderDateString = "";
            for (int i = 0; i < list.Count; i++)
            {
                Compliancecalendar _Event = new Compliancecalendar();
                string clor = "";
                int assignedCount = 0;
                DateTime date = Convert.ToDateTime(list[i]);
                //DateTime date = DateTime.Today.AddDays(7);
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    var Assigned = (from dts in dt.AsEnumerable()
                                    where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                    && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                    && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                    select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                    var ClosedDelaycount = (from dts in dt.AsEnumerable()
                                            where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                           && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                           && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                           && dts.Field<int>("ComplianceStatusID") == 5
                                            select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                    var Completed = (from dts in dt.AsEnumerable()
                                     where dts.Field<DateTime>("ScheduledOn").Year == date.Year
                                    && dts.Field<DateTime>("ScheduledOn").Month == date.Month
                                    && dts.Field<DateTime>("ScheduledOn").Day == date.Day
                                     && (dts.Field<int>("ComplianceStatusID") == 4
                                     || dts.Field<int>("ComplianceStatusID") == 5)
                                     select dts.Field<long>("ScheduledOnID")).Distinct().ToList();

                    assignedCount = Assigned.Count;
                    if (date > DateTime.Now)
                    {
                        clor = "";
                        if (Assigned.Count > Completed.Count)
                        {
                            clor = "upcoming";
                        }
                        else if ((Assigned.Count) == (Completed.Count))
                        {
                            if (ClosedDelaycount.Count > 0)
                            {
                                clor = "delayed";
                            }
                            else
                            {
                                clor = "complete";
                            }
                        }
                    }
                    else
                    {
                        if (Assigned.Count > Completed.Count)
                        {
                            clor = "overdue";
                        }
                        else if ((Assigned.Count) == (Completed.Count))
                        {
                            if (ClosedDelaycount.Count > 0)
                            {
                                clor = "delayed";
                            }
                            else
                            {
                                clor = "complete";
                            }
                        }
                    }
                }
                DateTime dtdate = Convert.ToDateTime(list[i]);
                var Date = dtdate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                CalenderDateString += "\"" + Date.ToString() + "\": { \"number\" :" + assignedCount + ", \"badgeClass\": \"badge-warning " + clor + "\",\"url\": \"javascript: fcal('" + Date.ToString() + "');\"},";
            }
            CalenderDateString = CalenderDateString.Trim(',');
        }
    }
}