﻿using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using com.VirtuosoITech.ComplianceManagement.Business;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_InvoiceList : System.Web.UI.Page
    {
        protected static string CId;
        protected static string UId;
        protected static string avacomAPIUrl;
        protected static string TLConnectAPIUrl;

        protected static string userProfileID;
        protected static string userProfileID_Encrypted;
        protected static string authKey;
        protected static string ProfileID;
        protected static string rlcsAPIURL;
        protected static string commonPath;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                avacomAPIUrl = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
                TLConnectAPIUrl = ConfigurationManager.AppSettings["TLConnect_API_URL"];

                CId = Convert.ToString(AuthenticationHelper.CustomerID);
                UId = Convert.ToString(AuthenticationHelper.UserID);

                string rlcsAPIURL = ConfigurationManager.AppSettings["RLCSAPIURL"];
                commonPath = rlcsAPIURL + "Masters/GenerateInvoicePath?ClientId=";

                string TLConnectKey = ConfigurationManager.AppSettings["TLConnect_Encrypt_Decrypt_Key"];
                ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);

                if (!string.IsNullOrEmpty(TLConnectKey))
                {
                    userProfileID = string.Empty;
                    userProfileID = AuthenticationHelper.ProfileID;

                    if (!string.IsNullOrEmpty(userProfileID))
                    {
                        userProfileID_Encrypted = CryptographyHandler.encrypt(userProfileID.Trim(), TLConnectKey);
                    }

                    authKey = AuthenticationHelper.AuthKey;
                }
            }
        }
    }
}