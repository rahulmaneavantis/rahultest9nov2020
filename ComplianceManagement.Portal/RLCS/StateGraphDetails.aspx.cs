﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class StateGraphDetails : System.Web.UI.Page
    {
        protected string Performername;
        protected string Reviewername;
        protected static int customerid;
        protected static string ComplianceTypeFlag;
        protected static bool IsApprover = false;       
        public static List<long> Branchlist = new List<long>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        int customerBranchID = 0;
                        if (!string.IsNullOrEmpty(Request.QueryString["branchid"]))
                        {
                            customerBranchID = Convert.ToInt32(Request.QueryString["branchid"]);                           
                        }
                        var userAssignedBranchList = (entities.SP_RLCS_GetAssignedLocationBranches(customerID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID)).ToList();
                        if (userAssignedBranchList != null)
                        {
                            if (userAssignedBranchList.Count > 0)
                            {
                                List<int> assignedbranchIDs = new List<int>();
                                assignedbranchIDs = userAssignedBranchList.Select(row => row.ID).ToList();
                                BindLocationFilter(assignedbranchIDs);
                                string rlcsStateID = string.Empty;
                                if (!string.IsNullOrEmpty(Request.QueryString["stateid"]))
                                {
                                    rlcsStateID = Request.QueryString["stateid"];
                                }
                                if (!string.IsNullOrEmpty(rlcsStateID))
                                {
                                    customerBranchID = GetCustomerBranchIDbyPanIndiaStateCode(rlcsStateID, customerID);
                                }
                                if (customerBranchID != 0)
                                {
                                    TraverseTree(tvFilterLocation.Nodes, customerBranchID);
                                    tvFilterLocation_SelectedNodeChanged(sender, e);
                                }                                                                
                                BindActList(Branchlist);
                                BindDetailView();

                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                }
            }
        }

        void TraverseTree(TreeNodeCollection nodes, int customerBranchID)
        {
            foreach (TreeNode child in nodes)
            {
                if (child.Value == customerBranchID.ToString())
                {
                    child.Selected = true;
                }
                else
                    TraverseTree(child.ChildNodes, customerBranchID);
            }
        }

        protected int GetCustomerBranchIDbyPanIndiaStateCode(string PanIndiaCode, int customerID)
        {
            int customerBranchID = 0;
            try
            {
                if (!string.IsNullOrEmpty(PanIndiaCode))
                {
                    using (ComplianceDBEntities entities = new ComplianceDBEntities())
                    {
                        var stateName = (from row in entities.RLCS_State_Mapping
                                         where row.SM_PanIndia_ID.Trim().ToUpper().Equals(PanIndiaCode.Trim().ToUpper())
                                         select row.SM_Name).FirstOrDefault();

                        if (!string.IsNullOrEmpty(stateName))
                        {
                            var AVACOM_BranchID = (from row in entities.RLCS_CustomerBranch_ClientsLocation_Mapping
                                                   join row2 in entities.CustomerBranches
                                                   on row.AVACOM_BranchID equals row2.ID
                                                   where row2.Name.Trim().ToUpper().Equals(stateName.Trim().ToUpper())
                                                   && row2.CustomerID == customerID
                                                   select row.AVACOM_BranchID).FirstOrDefault();

                            if (AVACOM_BranchID != null)
                                customerBranchID = Convert.ToInt32(AVACOM_BranchID);
                        }
                    }
                }

                return customerBranchID;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return customerBranchID;
            }
        }

        public static List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> GetAllHierarchy(int customerID, int customerbranchid)
        {
            List<com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy> hierarchy = null;
            using (ComplianceDBEntities entities = new ComplianceDBEntities())
            {
                var query = (from row in entities.CustomerBranches
                             where row.IsDeleted == false && row.CustomerID == customerID
                             && row.ID == customerbranchid
                             select row);
                hierarchy = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
                foreach (var item in hierarchy)
                {
                    Branchlist.Add(item.ID);
                    LoadSubEntities(customerID, item, true, entities);
                }
            }
            return hierarchy;
        }

        public static void LoadSubEntities(int customerid, com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy nvp, bool isClient, ComplianceDBEntities entities)
        {


            IQueryable<CustomerBranch> query = (from row in entities.CustomerBranches
                                                where row.IsDeleted == false && row.CustomerID == customerid
                                                     && row.ParentID == nvp.ID
                                                select row);
            var subEntities = query.ToList().Select(entry => new com.VirtuosoITech.ComplianceManagement.Business.Data.NameValueHierarchy() { ID = entry.ID }).OrderBy(entry => entry.ID).ToList();
            foreach (var item in subEntities)
            {
                Branchlist.Add(item.ID);
                LoadSubEntities(customerid, item, false, entities);
            }
        }

        private void BindActList(List<long> blist)
        {
            try
            {
                int CategoryID = -1;
                bool IsAvantisFlag = false;
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsAvantisFlag"]))
                {
                    IsAvantisFlag = Convert.ToBoolean(Request.QueryString["IsAvantisFlag"]);
                }
                ddlAct.Items.Clear();
                var ActList = ActManagement.GetAllAssignedActs_RLCS(customerid, AuthenticationHelper.UserID, CategoryID, AuthenticationHelper.ProfileID, Branchlist, IsAvantisFlag);//Query String 
                ddlAct.DataTextField = "Name";
                ddlAct.DataValueField = "ID";
                ddlAct.DataSource = ActList;
                ddlAct.DataBind();                
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter()
        {
            try
            {
                int customerID = -1;
                customerID = UserManagement.GetByID(AuthenticationHelper.UserID).CustomerID ?? 0;

                var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                tvFilterLocation.Nodes.Clear();
                string isstatutoryinternal = "";

                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    isstatutoryinternal = "S";
                }
                else if (ComplianceTypeFlag == "Internal")
                {
                    isstatutoryinternal = "I";
                }

                var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                node.Selected = true;
                tvFilterLocation.Nodes.Add(node);

                foreach (var item in bracnhes)
                {
                    node = new TreeNode(item.Name, item.ID.ToString());
                    node.SelectAction = TreeNodeSelectAction.Expand;                    
                    CustomerBranchManagement.BindBranchesHierarchy(node, item, LocationList);
                    tvFilterLocation.Nodes.Add(node);
                }

                tvFilterLocation.CollapseAll();

                tvFilterLocation_SelectedNodeChanged(null, null);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void BindLocationFilter(List<int> assignedBranchList)
        {
            try
            {
                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                {
                    int customerID = -1;
                    customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                    var bracnhes = CustomerBranchManagement.GetAllHierarchyManagementSatutory(customerID);

                    tvFilterLocation.Nodes.Clear();

                    string isstatutoryinternal = "";

                    if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                    {
                        ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                    }

                    if (ComplianceTypeFlag == "Statutory")
                    {
                        isstatutoryinternal = "S";
                    }
                    else if (ComplianceTypeFlag == "Internal")
                    {
                        isstatutoryinternal = "I";
                    }
                    
                    //var LocationList = CustomerBranchManagement.GetAssignedLocationList(AuthenticationHelper.UserID, customerID, AuthenticationHelper.Role, isstatutoryinternal);

                    TreeNode node = new TreeNode("Entity/State/Location/Branch", "-1");
                    node.Selected = true;
                    tvFilterLocation.Nodes.Add(node);

                    foreach (var item in bracnhes)
                    {
                        node = new TreeNode(item.Name, item.ID.ToString());
                        node.SelectAction = TreeNodeSelectAction.Expand;
                        CustomerBranchManagement.BindBranchesHierarchy(node, item, assignedBranchList);
                        tvFilterLocation.Nodes.Add(node);
                    }

                    tvFilterLocation.CollapseAll();
                    tvFilterLocation_SelectedNodeChanged(null, null);
                    
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
        private void BindDetailView()
        {
            try
            {
                lblAdvanceSearchScrum.Text = String.Empty;
                int RoleID = 3;
                int CustomerBranchId = -1;
                int ActId = -1;
                int CategoryID = -1;
                int UserID = -1;
                if (!string.IsNullOrEmpty(Request.QueryString["customerid"]))
                {
                    customerid = Convert.ToInt32(Request.QueryString["customerid"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["UserID"]))
                {
                    UserID = Convert.ToInt32(Request.QueryString["UserID"]);
                }
                if (!string.IsNullOrEmpty(Request.QueryString["Internalsatutory"]))
                {
                    ComplianceTypeFlag = Request.QueryString["Internalsatutory"];
                }               
                if (!string.IsNullOrEmpty(Request.QueryString["ActID"]))
                {
                    ActId = Convert.ToInt32(Request.QueryString["ActID"]);
                    if (ActId != 0)
                        ddlAct.Items.FindByValue(Request.QueryString["ActID"]).Selected = true;
                }
                if (!string.IsNullOrEmpty(Request.QueryString["IsApprover"]))
                {
                    IsApprover = Convert.ToBoolean(Request.QueryString["IsApprover"]);
                }

                if (tvFilterLocation.SelectedValue != "-1")
                {
                    //ddlAct.ClearSelection();
                    int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                    Branchlist.Clear();
                    GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                    Branchlist.ToList();
                }
                else
                {
                    Branchlist.Clear();
                }
                
                if (ddlAct.SelectedValue == "-1" || ddlAct.SelectedValue == "")
                {
                    BindActList(Branchlist);
                }

                if (!string.IsNullOrEmpty(Request.QueryString["Branchid"]))
                {
                    CustomerBranchId = Convert.ToInt32(Request.QueryString["Branchid"]);                    
                    tvFilterLocation_SelectedNodeChanged(null, null);
                    foreach (TreeNode node in tvFilterLocation.Nodes)
                    {
                        if (node.ChildNodes.Count > 0)
                        {
                            foreach (TreeNode child in node.ChildNodes)
                            {
                                if (child.Value == Convert.ToString(CustomerBranchId))
                                {
                                    child.Selected = true;
                                }
                            }
                        }
                        else if (node.Value == Convert.ToString(CustomerBranchId))
                        {
                            node.Selected = true;
                        }
                    }
                }
                if (tvFilterLocation.SelectedValue != "-1")
                {
                    int CustomerBranchlength = tvFilterLocation.SelectedNode.Text.Length;
                    CustomerBranchId = Convert.ToInt32(tvFilterLocation.SelectedValue);

                    if (CustomerBranchlength >= 20)
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text.Substring(0, 20) + "...";
                    else
                        lblAdvanceSearchScrum.Text += "     " + "<b>Location: </b>" + tvFilterLocation.SelectedNode.Text;
                }               
                if (ddlRole.SelectedValue != "-1")
                {
                    RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                    if (lblAdvanceSearchScrum.Text != "")
                    {

                        lblAdvanceSearchScrum.Text += "     " + "<b>Role: </b>" + ddlRole.SelectedItem.Text;
                    }
                    else
                    {
                        lblAdvanceSearchScrum.Text += "<b>Role: </b>" + ddlRole.SelectedItem.Text;
                    }
                }

                if (!string.IsNullOrEmpty(ddlAct.SelectedValue) && ddlAct.SelectedValue != "-1")
                {
                    int actlength = ddlAct.SelectedItem.Text.Length;
                    ActId = Convert.ToInt32(ddlAct.SelectedValue);
                    if (lblAdvanceSearchScrum.Text != "")
                    {
                        if (actlength >= 75)
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 75) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "     " + "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                    else
                    {
                        if (actlength >= 75)
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text.Substring(0, 75) + "...";
                        else
                            lblAdvanceSearchScrum.Text += "<b>Act: </b>" + ddlAct.SelectedItem.Text;
                    }
                }
                if (lblAdvanceSearchScrum.Text != "")
                {
                    divAdvSearch.Visible = true;
                }
                else
                {
                    divAdvSearch.Visible = false;
                }
                if (ComplianceTypeFlag == "Statutory")
                {
                    DivAct.Visible = true;
                    bool isAvantis = false;
                    var detailsview = RLCSManagement.GetComplianceDetailsDashboard(customerid, Branchlist, RoleID, CustomerBranchId, ActId, CategoryID, AuthenticationHelper.UserID, AuthenticationHelper.ProfileID, isAvantis);

                    if (UserID != -1)
                        detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                    Session["TotalRows"] = detailsview.Count;

                    GridStatutory.DataSource = detailsview;
                    GridStatutory.DataBind();

                    GridStatutory.Visible = true;
                    GridInternalCompliance.Visible = false;
                }
                else if (ComplianceTypeFlag == "Internal")
                {
                    ddlAct.ClearSelection();
                    DivAct.Visible = false;
                    var detailsview = Business.InternalComplianceManagement.GetInternalComplianceDetailsDashboard(customerid, Branchlist, RoleID, CustomerBranchId, CategoryID, AuthenticationHelper.UserID, IsApprover);
                    if (UserID != -1)
                        detailsview = detailsview.Where(Entry => Entry.UserID == UserID).ToList();

                    Session["TotalRows"] = detailsview.Count;
                    GridInternalCompliance.DataSource = detailsview;
                    GridInternalCompliance.DataBind();
                    GridStatutory.Visible = false;
                    GridInternalCompliance.Visible = true;
                }
                UpDetailView.Update();
                GetPageDisplaySummary();
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Script1", "HideLoaderDiv();", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void lnkClearAdvanceSearch_Click(object sender, EventArgs e)
        {
            try
            {
                TreeNode node = tvFilterLocation.Nodes[0];
                node.Selected = true;                
                ddlRole.ClearSelection();
                ddlAct.ClearSelection();
                lblAdvanceSearchScrum.Text = String.Empty;
                divAdvSearch.Visible = false;
                tvFilterLocation_SelectedNodeChanged(sender, e);
                var nvc = HttpUtility.ParseQueryString(Request.Url.Query);
                nvc.Remove("customerid");
                nvc.Remove("Category");
                nvc.Remove("ActID");
                nvc.Remove("Branchid");
                nvc.Remove("UserID");
                nvc.Remove("Internalsatutory");
                string url = Request.Url.AbsolutePath + "?" + nvc.ToString();
               // Response.Redirect("~/RLCS/StateGraphDetails.aspx", false);
                Context.ApplicationInstance.CompleteRequest();
                //Rebind Grid
                BindDetailView();
             
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";

                if (ComplianceTypeFlag == "Statutory")
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                else if (ComplianceTypeFlag == "Internal")
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;

                lblAdvanceSearchScrum.Text = String.Empty;
                               
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SelectedPageNo.Text = "1";
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo).ToString();
                }

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();

                GetPageDisplaySummary();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        private void GetPageDisplaySummary()
        {
            try
            {
                DivRecordsScrum.Visible = true;
                if (Convert.ToString(Session["TotalRows"]) != null && Convert.ToString(Session["TotalRows"]) != "")
                {
                    lblTotalRecord.Text = " " + Session["TotalRows"].ToString();
                    lTotalCount.Text = GetTotalPagesCount().ToString();

                    if (lTotalCount.Text != "0")
                    {
                        if (SelectedPageNo.Text == "" || SelectedPageNo.Text == "0" || SelectedPageNo.Text == "1")
                        {
                            SelectedPageNo.Text = "1";
                            lblStartRecord.Text = "1";

                            if (!(Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"].ToString())))
                                lblEndRecord.Text = ddlPageSize.SelectedValue + " ";
                            else
                                lblEndRecord.Text = Session["TotalRows"].ToString() + " ";
                        }
                    }
                    else if (lTotalCount.Text == "0")
                    {
                        SelectedPageNo.Text = "0";
                        DivRecordsScrum.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                throw ex;
            }
        }

        private int GetTotalPagesCount()
        {
            try
            {
                TotalRows.Value = Session["TotalRows"].ToString();

                int totalPages = Convert.ToInt32(TotalRows.Value) / Convert.ToInt32(ddlPageSize.SelectedValue);

                // total page item to be displyed
                int pageItemRemain = Convert.ToInt32(TotalRows.Value) % Convert.ToInt32(ddlPageSize.SelectedValue);

                // remaing no of pages
                if (pageItemRemain > 0)// set total No of pages
                {
                    totalPages = totalPages + 1;
                }
                else
                {
                    totalPages = totalPages + 0;
                }
                return totalPages;
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        protected void Next_Click(object sender, EventArgs e)
        {
            try
            {
                int currentPageNo = Convert.ToInt32(SelectedPageNo.Text);

                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (currentPageNo < GetTotalPagesCount())
                {
                    SelectedPageNo.Text = (currentPageNo + 1).ToString();
                }
                else
                {

                }

                if (!(StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) > Convert.ToInt32(Session["TotalRows"])))
                    StartRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue);

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //  ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void Previous_Click(object sender, EventArgs e)
        {
            try
            {
                int StartRecord = Convert.ToInt32(lblStartRecord.Text);
                int EndRecord = 0;

                if (Convert.ToInt32(SelectedPageNo.Text) > 1)
                {
                    SelectedPageNo.Text = (Convert.ToInt32(SelectedPageNo.Text) - 1).ToString();
                }

                StartRecord = StartRecord - Convert.ToInt32(ddlPageSize.SelectedValue);

                if (StartRecord < 1)
                    StartRecord = 1;

                EndRecord = StartRecord + Convert.ToInt32(ddlPageSize.SelectedValue) - 1;

                if (EndRecord > Convert.ToInt32(Session["TotalRows"]))
                    EndRecord = Convert.ToInt32(Session["TotalRows"]);

                lblStartRecord.Text = StartRecord.ToString();
                lblEndRecord.Text = EndRecord.ToString() + " ";

                if (ComplianceTypeFlag == "Statutory")
                {
                    GridStatutory.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridStatutory.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                else if (ComplianceTypeFlag == "Internal")
                {
                    GridInternalCompliance.PageSize = Convert.ToInt32(ddlPageSize.SelectedValue);
                    GridInternalCompliance.PageIndex = Convert.ToInt32(SelectedPageNo.Text) - 1;
                }

                //Reload the Grid
                BindDetailView();
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                //ShowGridViewPagingErrorMessage(ex.Message.ToString());
            }
        }

        protected void tvFilterLocation_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                tbxFilterLocation.Text = tvFilterLocation.SelectedNode.Text;

                if (sender != null)
                {
                    if (tvFilterLocation.SelectedValue != "-1")
                    {
                        ddlAct.ClearSelection();
                        int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);
                        Branchlist.Clear();
                        GetAllHierarchy(customerID, Convert.ToInt32(tvFilterLocation.SelectedValue));
                        Branchlist.ToList();
                    }
                    else
                    {
                        Branchlist.Clear();
                    }
                    if (ddlAct.SelectedValue == "-1" || ddlAct.SelectedValue == "")
                    {
                        BindActList(Branchlist);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void upDivLocation_Load(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "InitializeBranchesFilter", string.Format("initializeJQueryUI('{0}', 'divFilterLocation');", tbxFilterLocation.ClientID), true);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "HideTreeViewFilter", "$(\"#divFilterLocation\").hide(\"blind\", null, 500, function () { });", true);
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}