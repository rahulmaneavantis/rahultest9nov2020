﻿<%@ Page Title="Status Report" Language="C#" MasterPageFile="~/HRPlusSPOCMGR.Master" AutoEventWireup="true" CodeBehind="HRPlus_ComplianceStatusReport.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_ComplianceStatusReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>

    <%--<link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />--%>
    <link href="../NewCSS/contract_custom_style.css" rel="stylesheet" />

    <script src="../Newjs/moment.min.js"></script>

    <style>
        .k-grid-content {
            min-height: 100px;
            /*max-height: 350px;
            overflow-y: visible;*/
        }

        .k-btn {
            /*// float: right;*/
            margin-right: 10px;
        }

        .k-btnUpload {
            float: right;
            margin-right: 10px;
        }

        div.k-window-content {
            padding: 0;
        }

        .k-grid-toolbar {
            text-align: right;
        }

        .mt10 {
            margin-top: 10px;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
           <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script>
        $(document).ready(function () {
            fhead('My Reports/Status Report');
            Bind_Customer();
            Bind_SPOC();
            Bind_ComplianceType();
            Bind_FilterType();

            Bind_Frequency();

            Bind_Period();

            BindGrid();

            $("#btnExcel").click(function (e) {
                ExportToExcel(e);
            });

            $('#btnClearFilter').css('display', 'none');
        });

        function ExportToExcel(e) {
            e.preventDefault();
            kendo.ui.progress($("#gridCompliances").data("kendoGrid").element, true);

            // trigger export of the products grid
            $("#gridCompliances").data("kendoGrid").saveAsExcel();

            kendo.ui.progress($("#gridCompliances").data("kendoGrid").element, false);
            return false;
        }

        $(document).ready(function () {
            function startChange() {
                var startDate = start.value(),
                endDate = end.value();

                if (startDate) {
                    startDate = new Date(startDate);
                    startDate.setDate(startDate.getDate());
                    end.min(startDate);
                } else if (endDate) {
                    start.max(new Date(endDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter();
            }

            function endChange() {
                var endDate = end.value(),
                startDate = start.value();

                if (endDate) {
                    endDate = new Date(endDate);
                    endDate.setDate(endDate.getDate());
                    start.max(endDate);
                } else if (startDate) {
                    end.min(new Date(startDate));
                } else {
                    endDate = new Date();
                    start.max(endDate);
                    end.min(endDate);
                }

                ApplyFilter();
            }

            var start = $("#txtStartPeriod").kendoDatePicker({
                change: startChange,
                format: "dd-MMM-yyyy",
                dateInput: false
            }).data("kendoDatePicker");

            var end = $("#txtEndPeriod").kendoDatePicker({
                change: endChange,
                format: "dd-MMM-yyyy",
                dateInput: false
            }).data("kendoDatePicker");

            start.max(end.value());
            end.min(start.value());
        });

        function Bind_Customer() {
            $("#ddlCustomers").kendoDropDownList({
                optionLabel: "Customers",
                filter: "contains",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "CustomerName",
                dataValueField: "CustomerID",
                change: ChangeCustomer,
                dataSource: {
                    transport: {
                        read: {
                            url: '<% =AVACOM_RLCS_API_URL%>GetMyAssignedCustomers?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    },
                },
            });
        }

        function Bind_SPOC() {

            var customerID = -1;
            debugger;
            if ($('#ddlSPOCs').data('kendoDropDownTree')) {
                $('#ddlSPOCs').data('kendoDropDownTree').destroy();
                $('#divSPOCs').empty();
                $('#divSPOCs').append('<input id="ddlSPOCs" data-placeholder="User" style="width: 100%;" />');
            }
            var customerID = -1;
            if ($("#ddlCustomers").val() != '' && $("#ddlCustomers").val() != null && $("#ddlCustomers").val() != undefined) {
                customerID = $("#ddlCustomers").val();
            }

            var apiURL = '';

            if (customerID !== -1) {
                apiURL = '<%=AVACOM_RLCS_API_URL%>GetUsers?custID=' + customerID + '&distID=<% =distID%>&spID=<% =spID%>';
            } else {                
                apiURL = '<% =AVACOM_RLCS_API_URL%>GetAssignedSPOCs?userID=<% =loggedInUserID%>&distID=<% =distID%>&roleCode=<% =loggedUserRole%>';
            }

            $("#ddlSPOCs").kendoDropDownTree({
                placeholder: "Users",
                filter: "contains",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                dataTextField: "UserName",
                dataValueField: "UserID",
                change: ApplyFilter,
                dataSource: {
                    transport: {
                        read: {
                            url:apiURL, 
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            return response.Result;
                        }
                    },
                },
            });
        }

        function Bind_ComplianceType() {
            $("#ddlComType").kendoDropDownTree({
                placeholder: "Types",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                //change: ApplyFilter,
                dataSource: [
                    //{ text: "All", value: "-1" },
                    { text: "Register", value: "REG" },
                    { text: "Return", value: "RET" },
                    { text: "Challan", value: "CHA" },
                ]
            });
        }

        function Bind_FilterType() {
            $("#ddlFType").kendoDropDownTree({
                placeholder: "Status",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter,
                dataSource: [
                    //{ text: "All", value: "-1" },
                    { text: "Upcoming", value: "Upcoming" },
                    { text: "Overdue", value: "Overdue" },
                    { text: "Pending Review", value: "Pending Review" },
                    { text: "Closed-Timely", value: "Closed-Timely" },
                    { text: "Closed-Delayed", value: "Closed-Delayed" },
                ]
            });
        }

        function Bind_Frequency() {
            $("#ddlFreq").kendoDropDownTree({
                placeholder: "Frequency",
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: Bind_Period,
                dataSource: [
                    { text: "Frequency-All", value: "-1" },
                    { text: "Monthly", value: "Monthly" },
                    { text: "Quarterly", value: "Quarterly" },
                    { text: "Half Yearly", value: "HalfYearly" },
                    { text: "Annual", value: "Annual" },
                    { text: "Biennial", value: "TwoYearly" },
                ]
            });
        }

        function Bind_Period() {
            if ($('#ddlPeriod').data('kendoDropDownTree')) {
                $('#ddlPeriod').data('kendoDropDownTree').destroy();
                $('#divPeriod').empty();
                $('#divPeriod').append('<input id="ddlPeriod" data-placeholder="Period" style="width: 100%;" />')
            }

            $("#ddlPeriod").kendoDropDownTree({
                placeholder: "Period",
                autoClose: false,
                tagMode: "single",
                checkboxes: true,
                checkAll: true,
                checkAllTemplate: "Select All",
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter,
                dataSource: [
                        { freq: "Annual", text: "Annual", value: "Annual" },
                        { freq: "Biennial", text: "Biennial", value: "BiAnnual" },

                        { freq: "Half Yearly", text: "First Half", value: "HY1" },
                        { freq: "Half Yearly", text: "Second Half", value: "HY2" },

                        { freq: "Quarterly", text: "Q1", value: "Q1" },
                        { freq: "Quarterly", text: "Q2", value: "Q2" },
                        { freq: "Quarterly", text: "Q3", value: "Q3" },
                        { freq: "Quarterly", text: "Q4", value: "Q4" },

                        { freq: "Monthly", text: "January", value: "01" },
                        { freq: "Monthly", text: "February", value: "02" },
                        { freq: "Monthly", text: "March", value: "03" },
                        { freq: "Monthly", text: "April", value: "04" },
                        { freq: "Monthly", text: "May", value: "05" },
                        { freq: "Monthly", text: "June", value: "06" },
                        { freq: "Monthly", text: "July", value: "07" },
                        { freq: "Monthly", text: "August", value: "08" },
                        { freq: "Monthly", text: "September", value: "09" },
                        { freq: "Monthly", text: "Octomber", value: "10" },
                        { freq: "Monthly", text: "November", value: "11" },
                        { freq: "Monthly", text: "December", value: "12" },
                ]
            });

            var dropDownPeriod = $("#ddlPeriod").data("kendoDropDownTree");
            if (dropDownPeriod != null && dropDownPeriod != undefined) {
                if ($("#ddlFreq") != null && $("#ddlFreq") != undefined) {
                    if ($("#ddlFreq").val() != "-1") {
                        if ($("#ddlFreq").val() != null && $("#ddlFreq").val() != undefined) {
                            dropDownPeriod.enable(true);
                            dropDownPeriod.dataSource.filter({ field: "freq", operator: "eq", value: $("#ddlFreq").val() });
                        }
                    } else {
                        dropDownPeriod.enable(false);
                    }
                }
            }

            ApplyFilter();
        }

        function Bind_Year(DataSource) {
            $("#ddlYear").kendoDropDownTree({
                placeholder: "Year",
                autoClose: true,
                autoWidth: true,
                dataTextField: "text",
                dataValueField: "value",
                change: ApplyFilter,
                dataSource: DataSource
            });

            if ($("#ddlYear").data("kendoDropDownTree")) {
                $("#ddlYear").data("kendoDropDownTree").dataSource.sort({ field: "text", dir: "desc" });
            }
        }

        function BindGrid() {
            var fileName = '';
            var exportExcelName = fileName.concat('ComplianceStatusReport-', '<%=DateTime.Now.ToString("ddMMyyyy")%>', ".xlsx");

            var dataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: '<%=AVACOM_RLCS_API_URL%>GetComplianceStatusReport?userID=<% =loggedInUserID%>&profileID=<% =loggedUserProfileID%>&custID=<% =customerID%>&distID=<% =distID%>&spID=<% =spID%>&roleCode=<% =loggedUserRole%>',
                        dataType: "json",
                        beforeSend: function (request) {
                            request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                            request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                            request.setRequestHeader('Content-Type', 'application/json');
                        },
                    }
                },
                schema: {
                    data: function (response) {
                        if (response != null && response != undefined) {
                            return response.Result;
                        }
                    },
                    total: function (response) {
                        if (response.Result != null && response.Result != undefined) {
                            if (response.Result.length > 0) {

                            }
                            return response.Result.length;
                        }
                    }
                },
                pageSize: 50,
            });

            var initialLoad = true;

            $("#gridCompliances").kendoGrid({
                dataSource: dataSource,
                //pageable: {
                //    pageSizes: [50, 100, 500],
                //},
                //pageable: true,

                persistSelection: true,
                //scrollable: true,
                //scrollable: {
                //    virtual: true,
                //    endless: true
                //},
                pageable: {
                    numeric: true,
                    previousNext: true,
                    messages: {
                        display: "Showing {2} data items"
                    },
                    pageSizes: [50, 100, 500],
                },
                sortable: true,
                columns: [
                    {
                        field: "CustomerID",
                        sortable: {
                            initialDirection: "asc"
                        },
                        hidden: true,
                    },
                    {
                        field: "CustomerName", title: 'Customer', width: "18%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        //sortable: {
                        //    initialDirection: "asc"
                        //}
                    },
                    {
                        field: "CustomerBranchName", title: 'Branch', width: "15%",
                        attributes: { style: 'white-space: nowrap;' },
                    },
                    { field: "ActID", hidden: true, },
                    { field: "ShortForm", title: 'Compliance', width: "15%", attributes: { style: 'white-space: nowrap;' }, },
                    { field: "RequiredForms", title: 'Form', width: "7%", attributes: { style: 'white-space: nowrap;' }, },
                    { field: "Frequency", title: 'Frequency', width: "7%", attributes: { style: 'white-space: nowrap;' }, },
                    {
                        field: "ForMonth", title: 'Period', width: "10%", attributes: { style: 'white-space: nowrap; text-align: center;' },
                        headerAttributes: { style: 'white-space: nowrap; text-align: center;' },
                    },
                    {
                        field: "ScheduledOn", title: 'Due Date', template: "#= dateConverter(data)#", width: "7%", type: "date",
                        attributes: { style: 'white-space: nowrap;text-align: center;' },
                        headerAttributes: { style: 'white-space: nowrap; text-align: center;' },
                    },
                    { field: "ComplianceStatusID", hidden: true, },
                    {
                        field: "FilterStatus", title: 'Status', width: "8%", attributes: { style: 'white-space: nowrap;text-align: center;' },
                        headerAttributes: { style: 'white-space: nowrap; text-align: center;' },
                    },
                    { field: "ComplianceStatusName", title: 'Status', hidden: true, },
                    { field: "RLCS_PayrollMonth", title: 'PayrollMonth', type: "string", hidden: true, },
                    { field: "RLCS_PayrollYear", title: 'PayrollYear', hidden: true, },
                ],
                dataBound: onDataBound,
                //dataBound: function () {

                //},
                excelExport: function (e) {
                    var sheet = e.workbook.sheets[0];

                    for (var rowIndex = 1; rowIndex < sheet.rows.length; rowIndex++) {                        
                        var row = sheet.rows[rowIndex];
                        var cellIndex = 6;

                        var value = row.cells[cellIndex].value;
                        var newValue = DateFormatConverter(value);

                        row.cells[cellIndex].value = newValue;
                        row.cells[cellIndex].format = "dd-MMM-yyyy";
                        row.cells[cellIndex].autoWidth = true;
                    }

                    var columns = e.workbook.sheets[0].columns;
                    columns.forEach(function (column) {
                        delete column.width;
                        column.autoWidth = true;
                    });
                },
                //toolbar: ["excel"],
                excel: {
                    fileName: exportExcelName,
                    allPages: true,
                },
            });

            function onDataBound(arg) {
                
                var cnt = $('#gridCompliances').data('kendoGrid').dataSource.total();
                //alert(cnt);

                var dataSource = $('#gridCompliances').data('kendoGrid').dataSource;
                var data = dataSource.data();

                var ddlSource = [];
                var dataSourceToBind = [];
                for (var i = 0; i < data.length; i++) {
                    var currentPayrollYear = data[i].RLCS_PayrollYear;
                    if (!ddlSource.includes(currentPayrollYear)) {
                        ddlSource.push(currentPayrollYear);
                        dataSourceToBind.push({ text: currentPayrollYear, value: currentPayrollYear });
                    }
                }

                if (dataSourceToBind.length > 0) {
                    if ($('#ddlYear').data('kendoDropDownTree')) {
                        $('#ddlYear').data('kendoDropDownTree').destroy();
                        $('#divYear').empty();
                        $('#divYear').append('<input id="ddlYear" data-placeholder="Year" style="width: 100%;" />');

                        Bind_Year(dataSourceToBind);
                    } else {
                        Bind_Year(dataSourceToBind);
                    }
                }
            }

            //var grid = $("#gridCompliances").data("kendoGrid");
            //grid.dataSource.page(1);

            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(5)",
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.ShortForm;
                    return content;
                }
            }).data("kendoTooltip");

            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(6)", 
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = dataItem.RequiredForms;
                    return content;
                }
            }).data("kendoTooltip");

            $("#gridCompliances").kendoTooltip({
                filter: "td:nth-child(9)", 
                position: "bottom",
                content: function (e) {
                    var dataItem = $("#gridCompliances").data("kendoGrid").dataItem(e.target.closest("tr"));

                    var content = DateFormatConverter(dataItem.ScheduledOn);
                    return content;
                }
            }).data("kendoTooltip");
        }

        function getStatusName(data) {

            var strStatus = '';
            var todayDate = new Date(); // convert to actual date

            var year = todayDate.getFullYear();
            var month = todayDate.getMonth();

            if (month == 12) {
                year = year + 1;
                month = 1;
            } else {
                month = month + 1;
            }

            var nextMonth = new Date(year, month, todayDate.getDate());

            kendo.culture("en-IN");
            if (data.ScheduledOn != null && data.ScheduledOn != undefined && data.ComplianceStatusID != null && data.ComplianceStatusID != undefined) {
                if ((data.ComplianceStatusID == 1 || data.ComplianceStatusID == 12 || data.ComplianceStatusID == 13)
                    && (kendo.parseDate(data.ScheduledOn) > todayDate && kendo.parseDate(data.ScheduledOn) <= nextMonth)) {
                    strStatus = 'Upcoming';
                } else if ((data.ComplianceStatusID == 1 || data.ComplianceStatusID == 12 || data.ComplianceStatusID == 13)
                    && (kendo.parseDate(data.ScheduledOn) < todayDate)) {
                    strStatus = 'Overdue';
                } else if (data.ComplianceStatusID == 2 || data.ComplianceStatusID == 3 || data.ComplianceStatusID == 12) {
                    strStatus = 'Pending Review';
                } else if (data.ComplianceStatusID == 4) {
                    strStatus = 'Closed-Timely';
                } else if (data.ComplianceStatusID == 5) {
                    strStatus = 'Closed-Delayed';
                }
            }
            return strStatus;
        }

        function DateFormatConverter(providedDate) {

            var strDate = '';
            kendo.culture("en-IN");
            if (providedDate != null && providedDate != undefined) {
                strDate = kendo.toString(kendo.parseDate(providedDate), "dd-MMM-yyyy")
            }
            return strDate;
        }

        function dateConverter(data) {

            var strDate = '';
            kendo.culture("en-IN");
            if (data.ScheduledOn != null && data.ScheduledOn != undefined) {
                strDate = kendo.toString(kendo.parseDate(data.ScheduledOn), "dd-MMM-yyyy")
            }
            return strDate;
        }

        function ChangeCustomer() {
            
            Bind_SPOC();
            ApplyFilter();
        }

        function ApplyFilter() {
            
            //Customer
            var selectedCustIDs = $("#ddlCustomers").val();
            //var selectedCustIDs = $("#ddlCustomers").data("kendoDropDownTree")._values;

            //User/SPOC
            var selectedSPOCs = $("#ddlSPOCs").data("kendoDropDownTree")._values;

            //ComType           
            var selectedComTypes = $("#ddlComType").data("kendoDropDownTree")._values;

            //Filter Type - Upcoming, Overdue 
            var selectedFTypes = $("#ddlFType").data("kendoDropDownTree")._values;
            //var checkedFTypes = $("#ddlFType").data("kendoDropDownTree")._allCheckedItems;

            //var selectedFTypes = [];

            //if (checkedFTypes.length > 0) {
            //    for (var i = 0; i < checkedFTypes.length; i += 1) {
            //        var currentProduct = checkedFTypes[i];

            //        selectedFTypes.push({
            //            Name: currentProduct.text
            //        });
            //    }
            //}  

            var finalSelectedfilter = { logic: "and", filters: [] };

            if (selectedCustIDs != "" || selectedSPOCs.length > 0 || selectedComTypes.length > 0 || selectedFTypes.length > 0) {

                if (selectedCustIDs != '' && selectedCustIDs != null && selectedCustIDs != undefined && selectedCustIDs != "-1") {
                    var custFilter = { logic: "or", filters: [] };

                    custFilter.filters.push({
                        field: "CustomerID", operator: "eq", value: selectedCustIDs
                    });

                    finalSelectedfilter.filters.push(custFilter);
                }

                if (selectedSPOCs.length > 0) {
                    var spocFilter = { logic: "or", filters: [] };

                    $.each(selectedSPOCs, function (i, v) {
                        spocFilter.filters.push({
                            field: "UserID", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(spocFilter);
                }

                if (selectedComTypes.length > 0) {
                    var stateFilter = { logic: "or", filters: [] };

                    $.each(selectedStates, function (i, v) {
                        stateFilter.filters.push({
                            field: "StateID", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(stateFilter);
                }

                if (selectedFTypes.length > 0) {
                    var fTypeStatusFilter = { logic: "or", filters: [] };

                    $.each(selectedFTypes, function (i, v) {
                        fTypeStatusFilter.filters.push({
                            field: "FilterStatus", operator: "eq", value: v
                        });
                    });

                    finalSelectedfilter.filters.push(fTypeStatusFilter);
                }
            }

            //datefilter 
            var datefilter = { logic: "or", filters: [] };

            if ($("#txtStartPeriod").data("kendoDatePicker") != null && $("#txtStartPeriod").data("kendoDatePicker") != undefined) {
                if ($("#txtStartPeriod").data("kendoDatePicker").value() != null && $("#txtStartPeriod").data("kendoDatePicker").value() != "") {
                    //var startFilterDate = new Date($("#FromMonth_Deduction").data("kendoDatePicker").value());
                    var startFilterDate = new Date($("#txtStartPeriod").data("kendoDatePicker").value().getFullYear(),
                                                   $("#txtStartPeriod").data("kendoDatePicker").value().getMonth(),
                                                   1);
                    datefilter.filters.push({ logic: "or", field: "ScheduledOn", operator: "gte", value: startFilterDate });
                }
            }

            if ($("#txtEndPeriod").data("kendoDatePicker") != null && $("#txtEndPeriod").data("kendoDatePicker") != undefined) {
                if ($("#txtEndPeriod").data("kendoDatePicker").value() != null && $("#txtEndPeriod").data("kendoDatePicker").value() != "") {
                    //var endFilterDate = moment($("#txnDateTo").data("kendoDatePicker").value());

                    var year = $("#txtEndPeriod").data("kendoDatePicker").value().getFullYear();
                    var month = $("#txtEndPeriod").data("kendoDatePicker").value().getMonth();

                    if (month == 12) {
                        year = year + 1;
                        month = 1;
                    } else {
                        month = month + 1;
                    }

                    var endFilterDate = new Date(year, month, 0, 23, 59, 59);
                    datefilter.filters.push({ logic: "or", field: "ScheduledOn", operator: "lte", value: endFilterDate });
                }
            }

            if (datefilter.filters.length > 0)
                finalSelectedfilter.filters.push(datefilter);
           
            //Frequency       
            if ($("#ddlFreq").val() != null && $("#ddlFreq").val() != undefined) {
                if ($("#ddlFreq").val() != "-1" && $("#ddlFreq").val() != "") {
                    finalSelectedfilter.filters.push({ logic: "or", field: "Frequency", operator: "eq", value: $("#ddlFreq").val() });

                    //Period                      
                    if ($("#ddlPeriod").val() != null && $("#ddlPeriod").val() != undefined) {
                        if ($("#ddlPeriod").val() != "-1") {
                            var selectedPeriods = $("#ddlPeriod").data("kendoDropDownTree")._values;
                            if (selectedPeriods.length > 0) {
                                var periodFilter = { logic: "or", filters: [] };

                                if ($("#ddlFreq").val() == "Monthly") {
                                    $.each(selectedPeriods, function (i, v) {
                                        periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: v });
                                    });
                                } else if ($("#ddlFreq").val() == "Quarterly") {
                                    $.each(selectedPeriods, function (i, v) {                                        
                                        if (v == "Q1") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "06" });
                                        } else if (v == "Q2") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "06" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "09" });
                                        } else if (v == "Q3") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "09" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                        } else if (v == "Q4") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                        }
                                    });
                                } else if ($("#ddlFreq").val() == "HalfYearly") {
                                    $.each(selectedPeriods, function (i, v) {                                        
                                        if (v == "HY1") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "06" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "09" });
                                        } else if (v == "HY2") {
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                        }
                                    });
                                } else if ($("#ddlFreq").val() == "Annual") {
                                    $.each(selectedPeriods, function (i, v) {
                                        if (v == "Annual") {                                            
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                            periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                        }
                                    });
                                } else if ($("#ddlFreq").val() == "BiAnnual" || $("#ddlFreq").val() == "TwoYearly") {
                                    $.each(selectedPeriods, function (i, v) {
                                        periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "12" });
                                        periodFilter.filters.push({ logic: "or", field: "RLCS_PayrollMonth", operator: "eq", value: "03" });
                                    });
                                }

                                if (periodFilter.filters.length > 0)
                                    finalSelectedfilter.filters.push(periodFilter);
                            }
                        }
                    }
                }
            }

            //Year      
            var yearFilter = { logic: "or", filters: [] };
            if ($("#ddlYear").val() != null && $("#ddlYear").val() != undefined) {
                if ($("#ddlYear").val() != "") {
                    yearFilter.filters.push({ logic: "or", field: "RLCS_PayrollYear", operator: "eq", value: $("#ddlYear").val() });
                }
            }

            if (yearFilter.filters.length > 0)
                finalSelectedfilter.filters.push(yearFilter);

            if (finalSelectedfilter.filters.length > 0) {
                if ($("#gridCompliances").data("kendoGrid") != null && $("#gridCompliances").data("kendoGrid") != undefined) {
                    var dataSource = $("#gridCompliances").data("kendoGrid").dataSource;
                    dataSource.filter(finalSelectedfilter);
                    $('#btnClearFilter').css('display', 'block');
                }
            } else {
                if ($("#gridCompliances").data("kendoGrid") != null && $("#gridCompliances").data("kendoGrid") != undefined) {
                    var dataSource = $("#gridCompliances").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    $('#btnClearFilter').css('display', 'none');
                }
            }
        }

        function ClearFilter(e) {
            e.preventDefault();
            //e.stopPropogation();
            //$("#ddlCustomers").data("kendoDropDownTree").value([]);
            $("#ddlCustomers").data("kendoDropDownList").value("-1");
            $("#ddlSPOCs").data("kendoDropDownTree").value([]);
            $("#ddlComType").data("kendoDropDownTree").value([]);
            $("#ddlFType").data("kendoDropDownTree").value([]);
            $("#ddlFreq").data("kendoDropDownTree").value("-1");
            $("#ddlPeriod").data("kendoDropDownTree").value([]);
            $("#ddlYear").data("kendoDropDownTree").value("-1");

            var txtStartPeriod = $("#txtStartPeriod").data("kendoDatePicker");

            if (txtStartPeriod != null && txtStartPeriod != undefined) {
                txtStartPeriod.value(null);
                txtStartPeriod.trigger("change");
            }

            var txtEndPeriod = $("#txtEndPeriod").data("kendoDatePicker");

            if (txtEndPeriod != null && txtEndPeriod != undefined) {
                txtEndPeriod.value(null);
                txtEndPeriod.trigger("change");
            }

            $('#btnClearFilter').css('display', 'none');

            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container colpadding0 plr0">
        <div class="row col-md-12 colpadding0 mt10 mb10">
            <div class="col-md-1 pl0" style="width: 20%">
                <%--<label for="ddlCustomers" class="control-label">Customer</label>--%>
                <input id="ddlCustomers" data-placeholder="type" style="width: 100%;" />
            </div>

            <div id="divSPOCs" class="col-md-1 pl0" style="width: 20%">
                <%--<label for="ddlSPOCs" class="control-label">SPOC</label>--%>
                <input id="ddlSPOCs" data-placeholder="type" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0 d-none" style="width: 15%">
                <%--<label for="ddlComType" class="control-label">Compliance Type</label>--%>
                <input id="ddlComType" data-placeholder="State" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0" style="width: 15%">
                <%--<label for="ddlFType" class="control-label">Filter Type</label>--%>
                <input id="ddlFType" data-placeholder="Type" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0" style="width: 15%">
                <input id="ddlFreq" data-placeholder="Frequency" style="width: 100%;" />
            </div>

            <div id="divPeriod" class="col-md-1 pl0" style="width: 15%">
                <input id="ddlPeriod" data-placeholder="Period" style="width: 100%;" />
            </div>

            <div id="divYear" class="col-md-1 pl0" style="width: 15%">
                <input id="ddlYear" data-placeholder="Year" style="width: 100%;" />
            </div>
        </div>

        <div class="row col-md-12 colpadding0 mt10 mb10">
            <div class="col-md-1 pl0" style="width: 20%">
                <input id="hdnInput1" class="d-none" data-placeholder="Year" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0" style="width: 20%">
                <input id="hdnInput2" class="d-none" data-placeholder="Year" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0" style="width: 15%">
                <input id="hdnInput3" class="d-none" data-placeholder="Year" style="width: 100%;" />
            </div>

            <div class="col-md-1 pl0" style="width: 15%">
                <%--<label for="txtStartPeriod" class="control-label">From</label>--%>
                <input id="txtStartPeriod" placeholder="From-Date" style="width: 100%" />
            </div>

            <div class="col-md-1 pl0" style="width: 15%">
                <%--<label for="txtEndPeriod" class="control-label">To</label>--%>
                <input id="txtEndPeriod" placeholder="To-Date" style="width: 100%" />
            </div>

            <div class="col-md-1 pl0 text-right" style="width: 15%">
                <div class="col-md-6 colpadding0">
                    <button id="btnClearFilter" class="btn btn-primary" onclick="ClearFilter(event)"><span class="k-icon k-i-filter-clear" onclick="javascript:return false;"></span>Clear</button>
                </div>
                <div class="col-md-6 colpadding0 text-right">
                    <button id="btnExcel" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Export to Excel"><span class="k-icon k-i-excel"></span>Export</button>
                </div>
            </div>
        </div>

        <div class="row col-md-12 colpadding0 mb10">
            <div id="gridCompliances" style="border: none;"></div>
        </div>
    </div>
</asp:Content>
