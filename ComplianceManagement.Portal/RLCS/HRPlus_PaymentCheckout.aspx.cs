﻿using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class PaymentCheckout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnProceedPayment);

                if (!IsPostBack)
                {
                    int custID = 0;
                    if (!string.IsNullOrEmpty(Request.QueryString["CustID"]))
                    {
                        custID = Convert.ToInt32(Request.QueryString["CustID"]);
                        hdnCustID.Value = custID.ToString();

                        txtOrderID.Text = PaymentManagement.Get_UniqueOrderID(custID, "Order");
                        Session["OrderID"] = txtOrderID.Text;
                        GetCustomerDetails(custID);

                        rbMode_CheckedChanged(sender, e);
                    }
                    else
                    {
                        cvPayment.IsValid = false;
                        cvPayment.ErrorMessage = "Something went wrong, Please try again!!";
                    }
                }

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "selClass", "setRadioButtonClass();", true);
            }
            else
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                FormsAuthentication.RedirectToLoginPage();
            }
        }

        public void GetCustomerDetails(int CustomerID)
        {
            var cust = CustomerManagement.GetByID(CustomerID);
            if (cust != null)
            {
                txtPaymentEmail.Text = cust.BuyerEmail;
                txtPaymentMobile.Text = cust.BuyerContactNumber;

                txtPaymentDate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
        }

        protected void btnProceedPayment_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnCustID.Value != null && !string.IsNullOrEmpty(hdnCustID.Value))
                {
                    if (!string.IsNullOrEmpty(txtOrderID.Text))
                    {
                        if (!string.IsNullOrEmpty(txtPaymentAmount.Text))
                        {
                            if (Session["OrderID"] != null)
                            {
                                if (Session["OrderID"].ToString().ToUpper().Equals(txtOrderID.Text.Trim().ToString().ToUpper()))
                                {
                                    //CustID, PGID, OrderID, Amount, Phone, Email
                                    Session["PaymentDetails"] = hdnCustID.Value + "," + 2 + "," + txtOrderID.Text.Trim() + "," + txtPaymentAmount.Text.Trim() + "," + txtPaymentMobile.Text.Trim() + "," + txtPaymentEmail.Text.Trim();

                                    PaymentManagement.PaywithPayTM(Convert.ToInt32(hdnCustID.Value), Convert.ToInt32(rbMode.SelectedValue), txtOrderID.Text.Trim(), txtPaymentAmount.Text.Trim(), txtPaymentMobile.Text, txtPaymentEmail.Text, AuthenticationHelper.UserID);
                                }
                                else
                                {
                                    cvPayment.IsValid = false;
                                    cvPayment.ErrorMessage = "Something went wrong, Please refresh page and try again";
                                }
                            }
                        }
                        else
                        {
                            cvPayment.IsValid = false;
                            cvPayment.ErrorMessage = "Amount can not be empty, Please try again later or refresh the page";
                        }
                    }
                    else
                    {
                        cvPayment.IsValid = false;
                        cvPayment.ErrorMessage = "OrderID can not be empty, Please try again later or refresh the page";
                    }
                }
                else
                {
                    cvPayment.IsValid = false;
                    cvPayment.ErrorMessage = "Something went wrong, Please try again";
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (rbMode.SelectedValue == "2") //Offline
                {
                    if (!string.IsNullOrEmpty(rbOfflineType.SelectedValue))
                    {
                        #region Verification
                        if (!string.IsNullOrEmpty(txtTxnNo.Text) && !string.IsNullOrEmpty(txtOrderID.Text) && !string.IsNullOrEmpty(txtPaymentDate.Text))
                        {
                            int loggedInUserID = AuthenticationHelper.UserID;
                            int customerID = Convert.ToInt32(AuthenticationHelper.CustomerID);

                            bool saveSuccess = PaymentManagement.CreatePaymentLog(customerID, 0, 1, txtOrderID.Text.Trim(), txtPaymentAmount.Text.Trim(), 1, loggedInUserID); //PGID--2--OFFLINE, TxnType--1--PaymentRequest, StatusID--0--Initiated

                            if (saveSuccess)
                            {
                                using (ComplianceDBEntities entities = new ComplianceDBEntities())
                                {
                                    PaymentTransactionLog newPaymentLog = new PaymentTransactionLog();
                                    newPaymentLog.MODE = Convert.ToInt32(rbMode.SelectedValue);
                                    newPaymentLog.PGID = 0;
                                    newPaymentLog.OrderID = txtOrderID.Text.Trim();
                                    newPaymentLog.CustomerID = customerID;
                                    newPaymentLog.Phone = txtPaymentMobile.Text.Trim();
                                    newPaymentLog.Email = txtPaymentEmail.Text.Trim();
                                    newPaymentLog.StatusID = 1;
                                    newPaymentLog.Amount = Convert.ToDecimal(txtPaymentAmount.Text.Trim());
                                    newPaymentLog.TxnDate = DateTime.Now;

                                    newPaymentLog.BankTxnID = txtTxnNo.Text.Trim(); //UTR-Cheque No
                                    newPaymentLog.TxnID = txtBank.Text.Trim(); //Bank

                                    if (!string.IsNullOrEmpty(rbOfflineType.SelectedValue))
                                    {
                                        newPaymentLog.Payment_Mode = rbOfflineType.SelectedValue;
                                    }

                                    newPaymentLog.IsDeleted = false;
                                    newPaymentLog.CreatedOn = DateTime.Now;
                                    newPaymentLog.CreatedBy = loggedInUserID;

                                    saveSuccess = PaymentManagement.CreateUpdate_PaymentTransactionLog(newPaymentLog);
                                }
                            }
                        }
                        else
                        {
                            cvOfflinepayment.IsValid = false;
                            cvOfflinepayment.ErrorMessage = "Required UTR/Cheque No., OrderID, Transaction Date";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }

        protected void rbMode_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(rbMode.SelectedValue))
                {
                    if (rbMode.SelectedValue == "2") //Offline
                    {
                        divUTR.Visible = true;
                        divBank.Visible = true;
                        divOfflineType.Visible = true;

                        txtPaymentDate.Enabled = true;
                        txtPaymentDate.Text = string.Empty;

                        btnProceedPayment.Visible = false;
                        btnSubmit.Visible = true;

                        rfvPaymentDate.ValidationGroup = "OfflinePayment";
                        rfvOrderID.ValidationGroup = "OfflinePayment";
                        rfvPaymentAmount.ValidationGroup = "OfflinePayment";
                        rfvPaymentMobile.ValidationGroup = "OfflinePayment";
                        rfvPaymentEmail.ValidationGroup = "OfflinePayment";

                        cvAmount1.ValidationGroup = "OfflinePayment";
                        cvAmount2.ValidationGroup = "OfflinePayment";
                    }
                    else if (rbMode.SelectedValue == "1") //Online
                    {
                        divUTR.Visible = false;
                        divBank.Visible = false;
                        divOfflineType.Visible = false;

                        txtPaymentDate.Enabled = false;
                        txtPaymentDate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                        btnProceedPayment.Visible = true;
                        btnSubmit.Visible = false;

                        rfvPaymentDate.ValidationGroup = "OnlinePayment";
                        rfvOrderID.ValidationGroup = "OnlinePayment";
                        rfvPaymentAmount.ValidationGroup = "OnlinePayment";
                        rfvPaymentMobile.ValidationGroup = "OnlinePayment";
                        rfvPaymentEmail.ValidationGroup = "OnlinePayment";

                        cvAmount1.ValidationGroup = "OnlinePayment";
                        cvAmount2.ValidationGroup = "OnlinePayment";
                    }

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "selClass", "setRadioButtonClass();", true);

                }
            }
            catch (Exception ex)
            {
                LoggerMessage.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
            }
        }
    }
}