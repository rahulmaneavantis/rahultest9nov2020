﻿<%@ Page Title="User :: Setup HR+" Language="C#" MasterPageFile="~/HRPlusCompliance.Master" AutoEventWireup="true" CodeBehind="RLCS_User_MasterList_New.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.RLCS_User_MasterList_New" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../NewCSS/kendo.common.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.rtl.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.silver.min.css" rel="stylesheet" />
    <link href="../NewCSS/kendo.mobile.all.min.css" rel="stylesheet" />

    <script type="text/javascript" src="../Newjs/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="../Newjs/kendo.all.min.js"></script>
    <script type="text/javascript" src="../Newjs/jszip.min.js"></script>

    <link href="../NewCSS/Kendouicss.css" rel="stylesheet" />

    <title></title>

    <style>
        .k-grid-content {
            min-height: 50px;
        }

        .k-grid td {
            line-height: 2.0em;
        }

        .k-grid tbody button.k-button {
            min-width: 32px;
            min-height: 32px;
        }

        .k-window div.k-window-content {
            overflow: hidden;
        }
    </style>

    <style>
        .panel-heading .nav > li > a {
            font-size: 17px;
        }

            .panel-heading .nav .active, .panel-heading .nav > li.active > a, .panel-heading .nav > li > a:hover {
                color: white;
                background-color: #1fd9e1;
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
                margin-left: 0.5em;
                margin-right: 0.5em;
            }

        .panel-heading .nav > li > a {
            font-size: 16px;
            margin-left: 0.5em;
            margin-right: 0.5em;
        }

        .panel-heading .nav {
            background-color: #f8f8f8;
            border: none;
            font-size: 11px;
            margin: 0px 0px 0px 0;
            border-radius: 10px;
        }

            .panel-heading .nav > li {
                margin-left: 5px !important;
                margin-right: 5px !important;
            }

                .panel-heading .nav > li:hover {
                    color: white;
                    background-color: #1fd9e1;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    margin-left: 0.5em;
                    margin-right: 0.5em;
                }

        .btnAdd {
            height: 33px;
            width: 71px;
        }
    </style>

    <script id="templateTooltip" type="text/x-kendo-template">
        <div>
        <div> #:value ? value : "N/A" #</div>
        </div>
    </script>

    <script type="text/javascript">
        function Bind_Customers() {
            $("#ddlCustomer").kendoDropDownList({
                placeholder: "Select",
                autoWidth: true,                
                dataTextField: "Name",
                dataValueField: "ID",
                index: 0,
                change: function (e) {
                    $('#btnAddUser').show();
                    BindUserGrid();
                },
                dataSource: {
                    severFiltering: true,
                    transport: {
                        read: {
                            <%--url: '<%=avacomRLCSAPIURL%>GetCustomers?custID=' + <%=custID%> +'&SPID='+<%=spID%>+'&distID='+<%=distID%>,--%>
                             url: '<%=avacomRLCSAPIURL%>GetHRCustomers?userID=<%=userID%>&custID=<%=custID%>&SPID=<%=spID%>&distID=<%=distID%>&roleCode=<%=RoleCode%>&prodType=2',
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                            dataType: 'json',
                        }
                    },
                    schema: {
                        data: function (response) {
                            debugger;
                            return response.Result;
                        }
                    }
                }
            });
        }
                
        function checkUserActive(value) {                        
            if (value) {
                return "Active";
            } else if (!value) {
                return "In-Active";
            }
        }

        $(document).on("click", "#grid tbody tr .k-grid-edit", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            var userID = item.ID;
            OpenAddEditUserPopup(userID);
        });

        $(document).on("click", "#grid tbody tr .k-grid-delete", function (e) {
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            if(item!=null && item!=undefined){
                if(!item.IsActive){
                    var selectedUserID = item.ID;
                  
                    if(selectedUserID!=null && selectedUserID!=undefined){
                        window.kendoCustomConfirm("Are you sure! You want to delete this User?","Confirm").then(function () {
                        
                            $.ajax({
                                type: 'POST',
                                url: '<%=avacomRLCSAPIURL%>DeleteUser?UserID=' + selectedUserID,
                                dataType: "json",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                    xhr.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                    xhr.setRequestHeader('Content-Type', 'application/json');
                                },
                                success: function (result) {
                                    if(result.Result=="NOTDELETE")
                                        window.kendoCustomAlert("Compliance(s) assigned to this user, hence can not be delete","Message");
                                    else
                                        window.kendoCustomAlert("User Deleted Successfully","Message");                               
                                    
                                    $("#grid").data("kendoGrid").dataSource.read();
                                    //var popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
                                    //popupNotification.show("Deleted Successfully", "error");
                                },
                                error: function (result) {
                                    // notify the data source that the request failed
                                    console.log(result);
                                }
                            });
                           
                        }, function () {
                            // For Cancel
                        });  
                    }
                }
                else{
                    window.kendoCustomAlert("Active User can not Deleted","Message");  
                }
            }

            return false;
        });
        
        $(document).on("click", "#grid tbody tr .k-grid-chngpassword", function (e) {
            debugger;                        
            var item = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
            if(item!=null && item!=undefined){
                var UserID = item.ID;
                var UserRole="<%= userRole%>";
                if(UserID!=null && UserID!=undefined){
                    window.kendoCustomConfirm("Do you want to Reset Password of this User?","Confirm").then(function () {
                        kendo.ui.progress($("#grid").data("kendoGrid").element,true);
                        $.ajax({
                            type: 'POST',
                            url: "/Setup/ResetPasswordUser",
                            data: { UserID,UserRole},
                            dataType: "json",
                            success: function (result) {
                                kendo.ui.progress($("#grid").data("kendoGrid").element,false);
                                if(result === true)
                                {
                                    window.kendoCustomAlert("User Password Reset Successfully","Message");    
                                }
                                else {
                                    window.kendoCustomAlert("Something went wrong...!","Message");    
                                }
                            },
                            error: function (result) {
                                // notify the data source that the request failed
                                kendo.ui.progress($("#grid").data("kendoGrid").element,false);                                
                                console.log(result);
                            }
                        });
                           
                    });
                }
            }
        });

        function BindUserGrid() {
            
            var customerID=-1;

            if($("#ddlCustomer").val()!=''&&$("#ddlCustomer").val()!=null&&$("#ddlCustomer").val()!=undefined){
                customerID=$("#ddlCustomer").val();
            } else {
                customerID=<% =loggedInUserCustID%>;
                var a=$("#ddlCustomer").data("kendoDropDownList");a.value(customerID); 
				$("#ddlCustomer").data("kendoDropDownList").enable(false); 
            }
          
            var gridview = $("#grid").kendoGrid({
                dataSource: {
                    serverPaging: false,
                    pageSize: 50,
                    transport: {
                        read: {
                            url: '<%=avacomRLCSAPIURL%>GetAll_UserMaster?customerID='+customerID,
                            dataType: "json",
                            beforeSend: function (request) {
                                request.setRequestHeader('Authorization', '5E8E3D45-172D-4D58-8DF8-EB0B0E');
                                request.setRequestHeader('X-User-Id-1', 'f2UKmAm0KlI98bEYGGxEHQ==');
                                request.setRequestHeader('Content-Type', 'application/json');
                            },
                        }
                    },
                    batch: true,
                    pageSize: 10,
                    schema: {
                        data: function (response) {
                            if (response != undefined)
                                return response.Result;
                        },
                        total: function (response) {
                            if (response != undefined)
                                return response.Result.length;
                        }
                    }
                },                
                sortable: true,
                filterable: true,
                columnMenu: true,
                pageable: {
                    refresh: true,
                    buttonCount: 3,
                    // pageSizes: true,
                    //pageSizes: [ 10 , 25, 50 ],
                    change: function (e) {
                        //$('#chkAll').removeAttr('checked');
                        //$('#dvbtndownloadDocumentMain').css('display', 'none');
                    }
                },
                reorderable: true,
                resizable: true,
                multi: true,
                selectable: true,
                columns: [
                    { hidden: true, field: "ID", title: "UserID" },
                    {
                        title: "Sr.No",
                        field: "rowNumber",
                        template: "<span class='row-number'></span>",
                        width: "5%;",
                        attributes: {
                            style: 'white-space: nowrap;text-align:center;'
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "FirstName", title: 'First Name',
                        width: "15%;",
                        attributes: {
                            style: 'white-space: nowrap;'
                        }, filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "LastName", title: 'Last Name',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "Email", title: 'Email',
                        width: "15%",
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "ContactNumber", title: 'Contact No',
                        width: "15%",
                        attributes: {
                            style: 'white-space: nowrap;text-align:center; '
                        },
                        filterable: {
                            extra: false,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "HR_Role", title: 'Role',
                        width: "13%",
                        attributes: {
                            style: 'white-space: nowrap;text-align:center;'
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        field: "IsActive", title: 'Status',
                        width: "5%",
                        template: '#:checkUserActive(IsActive)#',
                        attributes: {
                            style: 'white-space: nowrap '
                        },
                        filterable: {
                            multi: true,
                            extra: false,
                            search: true,
                            operators: {
                                string: {
                                    eq: "Is equal to",
                                    neq: "Is not equal to",
                                    contains: "Contains"
                                }
                            }
                        }
                    },
                    {
                        command: 
                            [
                                { name: "edit", text: "", imageClass: "k-i-edit", iconClass: "k-icon k-i-edit", className: "ob-download" },
                                { name: "delete", text: "", iconClass: "k-icon k-i-delete", className: "ob-delete" },
                                { name: "chngpassword", text: "", iconClass: "k-icon k-i-reset", className: "ob-changepassword" },
                            ], title: "Action", lock: true, width: "12%;"
                    }
                ],
                dataBound: function () {
                    var rows = this.items();
                    $(rows).each(function () {
                        var index = $(this).index() + 1
                            + ($("#grid").data("kendoGrid").dataSource.pageSize() * ($("#grid").data("kendoGrid").dataSource.page() - 1));;
                        var rowLabel = $(this).find(".row-number");
                        $(rowLabel).html(index);
                    });
                }
            });
        }

        function SetGridTooltip(){
            $("#grid").kendoTooltip({
                filter: ".k-grid-edit",
                content: function(e){
                    return "View/Edit";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-delete",
                content: function(e){
                    return "Delete";
                }
            });

            $("#grid").kendoTooltip({
                filter: ".k-grid-chngpassword",
                content: function(e){
                    return "Reset Password";
                }
            });
        }

        $(document).ready(function () { 
            fhead('Masters/User');
            $('#divRLCSUserAddDialog').hide();
            $("#btnAddUser").kendoTooltip({ content: "Add New-User" });
            
            Bind_Customers();

            BindUserGrid();

            SetGridTooltip();

            $("#btnSearch").click(function (e) {
                e.preventDefault();

                var filter = [];
                $x = $("#txtSearch").val();
                if ($x) {
                    var gridview = $("#grid").data("kendoGrid");
                    gridview.dataSource.query({
                        page: 1,
                        pageSize: 50,
                        filter: {
                            logic: "or",
                            filters: [
                                { field: "FirstName", operator: "contains", value: $x },
                              { field: "LastName", operator: "contains", value: $x },                              
                              { field: "Email", operator: "contains", value: $x },
                              { field: "ContactNumber", operator: "contains", value: $x },
                              { field: "HR_Role", operator: "contains", value: $x }
                            ]
                        }
                    });

                    return false;
                }
                else {
                    var dataSource = $("#grid").data("kendoGrid").dataSource;
                    dataSource.filter({});
                    //BindUserGrid();
                }
                // return false;
            });
        });

    </script>

    <script>
         ///GG ADD 30July2020
        function UserValidateCheck()
        {
            debugger;
            var CustomerID = $("#ddlCustomer").val();
            var  NewRecord=1;  ///ADD
            event.preventDefault();
            if (CustomerID != '') {

                $.ajax({
                    url: "/Setup/CheckForUserValidation",
                    data: { NewRecord,CustomerID},
                    type: "POST",
                    content: 'application/json;charset=utf-8',
                    dataType: 'json',
                    success: function (data) {
                        debugger;
                        if(data === true)
                        {
                            OpenAddEditUserPopup(0);   ///ADD
                        }
                        else
                        {
                            alert("Facility to create more users is not available in the Free version. Kindly contact Avantis to activate the same.");
                        }
                    }

                });
            }
        }
        ///END
        function OpenAddEditUserPopup(UserID){
            
            $('#divRLCSUserAddDialog').show();            
            var myWindowAdv = $("#divRLCSUserAddDialog");

            var ddlcustomer = $("#ddlCustomer").val();

            myWindowAdv.kendoWindow({
                width: "50%",
                height: '80%',
                content: "../RLCS/RLCS_UserDetails.aspx?UserID=" + UserID + '&CustomerID=' + ddlcustomer,
                iframe: true,
                title: "User Details",
                visible: false,
                actions: [
                    //"Pin",
                    "Close"
                ],
                close: onClose,
                open:onOpen
            });

            myWindowAdv.data("kendoWindow").center().open();
            return false;
        }

        function onOpen(e) {
            kendo.ui.progress(e.sender.element, true);
        }

        function onClose() {
            $('#grid').data('kendoGrid').dataSource.read();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row colpadding0">
        <div class="col-md-12 colpadding0">
            <header class="panel-heading tab-bg-primary" style="background: none !important; padding: 0px 0px;">
                <ul class="nav nav-tabs">
                    <li>
                        <asp:LinkButton ID="lnkTabCustomer" runat="server" PostBackUrl="~/RLCS/RLCS_CustomerCorporateList_New.aspx">Customer</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEntityBranch" runat="server">Entity-Branch</asp:LinkButton>
                        <%--PostBackUrl="~/RLCS/RLCS_EntityLocation_Master_New.aspx"--%>
                    </li>
                    <li class="active">
                        <asp:LinkButton ID="lnkTabUser" runat="server" PostBackUrl="~/RLCS/RLCS_User_MasterList_New.aspx">User</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkTabEmployee" runat="server" PostBackUrl="~/RLCS/RLCS_EmployeeMaster_New.aspx">Employee</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnEntityAssign" PostBackUrl="~/RLCS/RLCS_EntityAssignment_New.aspx" runat="server">Entity-Branch Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAssign" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Assign_New.aspx">Compliance Assignment</asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkBtnComAct" runat="server" PostBackUrl="~/RLCS/RLCS_HRCompliance_Activate_New.aspx">Compliance Activation</asp:LinkButton>
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <div class="row colpadding0" style="margin-top: 1%">
        <div class="col-md-12 colpadding0 toolbar">
            <div class="col-md-4" style="padding-left: 0px;">
                <input id="ddlCustomer" style="width: 100%" />
            </div>
            <div class="col-md-4 colpadding0">
                <input class="k-textbox" type="text" id="txtSearch" style="width: 70%" placeholder="Type Text to Search..." />
                <button id="btnSearch" class="btn btn-primary">Search</button>
            </div>

             <div class="col-md-4" style="text-align: right">
                 <asp:LinkButton ID="lnkbtnUserCustomerMapping" class="btn btn-primary" runat="server" PostBackUrl="~/RLCS/HRPlus_UserCustomerMapping_New.aspx">User-Customer-Mapping</asp:LinkButton>
                <%--<button id="btnUserCustomerMapping" class="btn btn-primary"><span class="k-icon k-i-plus-outline"></span>User-Customer-Mapping</button> class="k-button btnAdd"--%>
                 <button id="btnAddUser" class="btn btn-primary" onclick="return UserValidateCheck()"><span class="k-icon k-i-plus-outline"></span>Add New</button> <%--class="k-button btnAdd"--%>
            </div>            
        </div>
    </div>

    <div class="row colpadding0 " style="margin-top: 1%">
        <div class="col-md-12 colpadding0">
            <div id="grid" style="border: none;">
                <div class="k-header k-grid-toolbar">
                </div>
            </div>
        </div>
    </div>

    <div id="divRLCSUserAddDialog">
        <iframe id="iframeAdd" style="width: 100%; height: 100%; border: none"></iframe>
    </div>
</asp:Content>
