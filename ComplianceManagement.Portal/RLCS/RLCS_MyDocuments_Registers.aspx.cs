﻿using System;
using com.VirtuosoITech.ComplianceManagement.Business;
using com.VirtuosoITech.ComplianceManagement.Business.Data;
using com.VirtuosoITech.ComplianceManagement.Portal.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using com.VirtuosoITech.ComplianceManagement.Business.RLCS;
using Ionic.Zip;
using System.Configuration;
using System.IO;
using com.VirtuosoITech.ComplianceManagement.Business.Contract;

namespace com.VirtuosoITech.ComplianceManagement.Portal.RLCS
{
    public partial class RLCS_MyDocuments_Registers : System.Web.UI.Page
    {
        protected int CustId;
        protected int UserId;
        protected static string Path;
        protected static int newRegistersMonth;
        protected static int newRegistersYear;
        protected static string ProfileID;

        protected void Page_Load(object sender, EventArgs e)
        {
            //divShowErrorMsg.Visible = false;
            CustId = Convert.ToInt32(AuthenticationHelper.CustomerID);
            UserId = Convert.ToInt32(AuthenticationHelper.UserID);
            Path = ConfigurationManager.AppSettings["AVACOM_RLCS_API_URL"];
            newRegistersMonth = Convert.ToInt32(ConfigurationSettings.AppSettings["RLCS_RegisterChallan_Month"]);
            newRegistersYear = Convert.ToInt32(ConfigurationSettings.AppSettings["RLCS_RegisterChallan_Year"]);
            ProfileID = Convert.ToString(AuthenticationHelper.ProfileID);


            if (!string.IsNullOrEmpty(Request.QueryString["Customer_BranchID"]) && !string.IsNullOrEmpty(Request.QueryString["EstablishmentType"]) && !string.IsNullOrEmpty(Request.QueryString["State"]) && !string.IsNullOrEmpty(Request.QueryString["Month"]) && !string.IsNullOrEmpty(Request.QueryString["Year"]))
            {
                int CustomerBranchID = Convert.ToInt32(Request.QueryString["Customer_BranchID"].ToString());
                string EstablishmentType = Request.QueryString["EstablishmentType"];
                string State = Request.QueryString["State"];
                string Month = Request.QueryString["Month"];
                string Year = Request.QueryString["Year"];

                if (CustomerBranchID > 0 && EstablishmentType != null && State != null && Month != null && Year != null)
                {
                    bool _objRegData = getDocumentRegDtls(CustomerBranchID, EstablishmentType, State, Month, Year);

                    if(!_objRegData)
                    {
                        //divErrormsgReg.Visible = true;
                        //lblregDocumenterror.Text = "No Document to Download";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Document to Download')", true);

                    }                   
                }
            }
        }

        private bool getDocumentRegDtls(int customerBranchID, string establishmentType, string state, string month, string year)
        {
            try
            {
                List<long> lstComplianceIDs = RLCSManagement.GetAll_Registers_Compliance_StateWise(state, establishmentType);

                if (lstComplianceIDs.Count > 0 && lstComplianceIDs != null)
                {
                    List<long> lstScheduleOnIDs = RLCSManagement.GetAll_Registers_ScheduleOnIDs(lstComplianceIDs, customerBranchID, year, month);

                    if (lstScheduleOnIDs.Count > 0)
                    {
                        bool _objDocument_RegDownloadval = RLCS_DocumentManagement.GetRLCSDocuments("Register", lstScheduleOnIDs);
                        if(!_objDocument_RegDownloadval)
                        {
                            return false;
                        }
                        else
                        {
                            return true ;
                        }                       
                    }
                    else
                    {
                        return false;  
                    }                    
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ContractManagement.InsertLog(ex, MethodBase.GetCurrentMethod().DeclaringType.ToString(), MethodBase.GetCurrentMethod().Name);
                return false;
            }
        }
    }
}