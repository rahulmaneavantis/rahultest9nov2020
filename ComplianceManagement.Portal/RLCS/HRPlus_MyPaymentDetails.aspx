﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HRPlus_MyPaymentDetails.aspx.cs" Inherits="com.VirtuosoITech.ComplianceManagement.Portal.RLCS.HRPlus_MyPaymentDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS -->
    <link href="~/NewCSS/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap theme -->
    <link href="~/NewCSS/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <!-- font icon -->
    <link href="~/NewCSS/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!--external css-->
    <link href="~/NewCSS/stylenew.css" rel="stylesheet" type="text/css" />
    <link href="~/NewCSS/contract_custom_style.css" rel="stylesheet" />
    <link href="~/NewCSS/jquery-ui-1.10.4.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="/Newjs/jquery.js"></script>
    <script type="text/javascript" src="/Newjs/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/Newjs/bootstrap.min.js"></script>

    <script type="text/javascript">
        function setRadioButtonClass() {
            debugger;
            var rbl = document.getElementById("<%=rbMode.ClientID %>");
            if (rbl != null && rbl != undefined) {
                var radio = rbl.getElementsByTagName("INPUT");
                if (radio != null && radio != undefined) {
                    for (var i = 0; i < radio.length; i++) {
                        var radiobutton = radio[i];
                        var label = radiobutton.parentNode.getElementsByTagName("LABEL")[0];
                        if (label != null && label != undefined) {
                            label.setAttribute('class', 'radio-inline');
                        }
                    }
                }
            }

            rbl = document.getElementById("<%=rbOfflineType.ClientID %>");
            if (rbl != null && rbl != undefined) {
                var radio = rbl.getElementsByTagName("INPUT");
                if (radio != null && radio != undefined) {
                    for (var i = 0; i < radio.length; i++) {
                        var radiobutton = radio[i];
                        var label = radiobutton.parentNode.getElementsByTagName("LABEL")[0];
                        if (label != null && label != undefined) {
                            label.setAttribute('class', 'radio-inline');
                        }
                    }
                }
            }

            rbl = document.getElementById("<%=rbStatus.ClientID %>");
            if (rbl != null && rbl != undefined) {
                var radio = rbl.getElementsByTagName("INPUT");
                if (radio != null && radio != undefined) {
                    for (var i = 0; i < radio.length; i++) {
                        var radiobutton = radio[i];
                        var label = radiobutton.parentNode.getElementsByTagName("LABEL")[0];
                        if (label != null && label != undefined) {
                            label.setAttribute('class', 'radio-inline');
                        }
                    }
                }
            }
        }

        function initializeDatePicker() {            
            $('#<%= txtPaymentDate.ClientID %>').datepicker({
                dateFormat: 'dd-mm-yy',                
                numberOfMonths: 1,
                changeMonth: true,
                changeYear: true,
            });
        }

        $(document).ready(function () {            
            setRadioButtonClass();
            initializeDatePicker();

            if ($(window.parent.document).find("#PaymentDetails").children().first().hasClass("k-loading-mask"))
                $(window.parent.document).find("#PaymentDetails").children().first().hide();
        });
    </script>
    <style>
        .radio-inline {
            padding-left: 0px;
            margin-left: 0em;
            margin-right: 0.5em;
        }
        .h2 {
            margin-top: 0px !important;
        }
    </style>
</head>
<body style="background-color: white;">
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="hdnPaymentID" runat="server" />
        <asp:HiddenField ID="hdnCustomerID" runat="server" />
    <%-- <asp:UpdatePanel ID="upPayment" runat="server">
            <ContentTemplate>--%>
                <div class="row">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Display="none" class="alert alert-block alert-danger fade in"
                        ValidationGroup="OfflinePayment" />
                    <asp:CustomValidator ID="cvPayment" runat="server" EnableClientScript="False"
                        ValidationGroup="OfflinePayment" Display="none" class="alert alert-block alert-danger fade in" />
                </div>

                <div class="row col-md-12 col-sm-12 col-xs-12 colpadding0 mb10">
                    <div class="col-md-12 col-sm-12 col-xs-12 text-center form-group row">
                        <h2>Order Summary</h2>
                    </div>
                    <div class="form-group row" id="divrbMode">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">Mode</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:RadioButtonList ID="rbMode" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" Enabled="false">
                                <asp:ListItem Text="Online" Value="1" Selected="True" CssClass="radio-inline"></asp:ListItem>
                                <asp:ListItem Text="Offline" Value="2" CssClass="radio-inline"></asp:ListItem>
                            </asp:RadioButtonList>
                            <%--OnSelectedIndexChanged="rbMode_CheckedChanged"--%>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row" id="divOfflineType" runat="server">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">Select</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:RadioButtonList ID="rbOfflineType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="NEFT/IMPS" Value="NEFT/IMPS" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Cheque" Value="Cheque"></asp:ListItem>
                                <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                            </asp:RadioButtonList>
                            <%--AutoPostBack="true" OnSelectedIndexChanged="rbMode_CheckedChanged"--%>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row required" id="divUTR" runat="server">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">UTR/Cheque No</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:TextBox ID="txtTxnNo" runat="server" CssClass="form-control" data-toggle="tooltip" data-placement="bottom" ToolTip="Unique Transaction Reference or Cheque Number"></asp:TextBox>
                            <asp:RequiredFieldValidator ErrorMessage="Required UTR/Cheque No" ControlToValidate="txtTxnNo" ID="rfvTxnNo"
                                runat="server" ValidationGroup="OfflinePayment" Display="None" />
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row" id="divBank" runat="server">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">Bank</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:TextBox ID="txtBank" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row required">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">Transaction Date</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ErrorMessage="Required Transaction Date" ControlToValidate="txtPaymentDate" ID="rfvPaymentDate"
                                runat="server" ValidationGroup="OfflinePayment" Display="None" />
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row required">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">OrderID</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:TextBox ID="txtOrderID" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                            <asp:RequiredFieldValidator ErrorMessage="Required OrderID" ControlToValidate="txtOrderID" ID="rfvOrderID"
                                runat="server" ValidationGroup="OfflinePayment" Display="None" />
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row required">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">Amount</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:TextBox ID="txtPaymentAmount" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ErrorMessage="Required Amount" ControlToValidate="txtPaymentAmount" ID="rfvPaymentAmount"
                                runat="server" ValidationGroup="OnlinePayment" Display="None" />
                            <asp:RegularExpressionValidator
                                ID="revAmount" Display="None"
                                runat="server" ValidationGroup="OfflinePayment"
                                ControlToValidate="txtPaymentAmount"
                                ErrorMessage="Allowed Numbers only in Amount"
                                ValidationExpression="^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$">
                            </asp:RegularExpressionValidator>
                            <%--<asp:CompareValidator runat="server" ID="cvAmount1"
                                ControlToValidate="txtPaymentAmount"
                                Operator="DataTypeCheck"
                                Type="Integer"
                                ErrorMessage="Enter Numbers only in Amount"
                                Display="None"
                                ValidationGroup="OfflinePayment"
                                Text="*" />--%>

                            <asp:CompareValidator runat="server" ID="cvAmount2"
                                ControlToValidate="txtPaymentAmount"
                                Operator="GreaterThanEqual"
                                ValueToCompare="0"
                                Type="Double"                                
                                ErrorMessage="Amount can not be less than 0"
                                Display="None" ValidationGroup="OfflinePayment"
                                Text="*" />
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row required">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">Mobile</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:TextBox ID="txtPaymentMobile" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ErrorMessage="Required Mobile Number" ControlToValidate="txtPaymentMobile" ID="rfvPaymentMobile"
                                runat="server" ValidationGroup="OfflinePayment" Display="None" />
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row required">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">Email</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:TextBox ID="txtPaymentEmail" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ErrorMessage="Required Email" ControlToValidate="txtPaymentEmail" ID="rfvPaymentEmail"
                                runat="server" ValidationGroup="OfflinePayment" Display="None" />
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row" id="div1" runat="server">
                        <div class="col-md-4 col-sm-4 col-xs-4 text-right vertical-align-center">
                            <label class="control-label">Status</label>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <asp:RadioButtonList ID="rbStatus" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Confirm" Value="2" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Reject/Failed" Value="3"></asp:ListItem>
                            </asp:RadioButtonList>                           
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                        </div>
                    </div>

                    <div class="form-group row" style="text-align: center;">
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary" Text="Submit" 
                            CausesValidation="true" OnClick="btnSubmit_Click" ValidationGroup="OfflinePayment"></asp:Button>
                        <%----%>
                    </div>

                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
    </form>
</body>
</html>
